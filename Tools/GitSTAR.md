# GitSTAR 恒星

```
# 效仿 SCrot 这一最经典应用的名字，SIGerot 是为 SIGer 期刊提供打印排版服务的，通过控制浏览器生成可以用于打印输出的分页版式，并沿用 Gitee 的UI风格。  
# 本需求源自 第2期 拥抱开源，改变世界 的打印需求 :pray: 而且该期推荐的两期期刊中 出了《宇宙》，《中文化 PYTHON》中的网页爬虫能力，也是 SIGerot 的核心技能。
```

这是 Tools 目录新建的 Python 爬虫程序 [SIGerot.py](SIGerot.py)，虽然没有任何代码，但他是一个开端，开启了 SIGer 的分享新模式 —— 纸质印刷。受到《宇宙》的启发，星云是诞生恒星的摇篮，我希望 SIGer 学习社群（即将发布）能如星云一般，为未来之星们提供创新的摇篮，能量之源就是 “分享”。说来奇妙，开启真正的分享之旅来自火焰棋实验室社区中的一个小游戏 LuckyStar 我们叫她福星游戏，这个发愿为我们的分享打下了坚实的基础，恒星的诞生是偶然的，也是必然的，地球智慧生命的诞生，是宇宙华章中的重要一笔，而未来的 SIGer 之星们，也必将见证历史。让我们尽情地分享，尽情的拥抱彼此吧。

[![GitSTAR](https://images.gitee.com/uploads/images/2022/0110/111939_70a67c39_5631341.jpeg "GitSTAR369.jpg")](https://images.gitee.com/uploads/images/2022/0110/111923_5c69e085_5631341.jpeg)

这是我送给 GitSTAR 们的礼物，它源自《宇宙》! :pray: 

<p title="STEM 在上海">
<img src="stemINshanghai2-p01.png" height="199px"> 
<img src="stemINshanghai2-p02.png" height="199px"> 
<img src="stemINshanghai2-p03.png" height="199px"> 
<img src="stemINshanghai2-p04.png" height="199px"> 
<img src="stemINshanghai2-p05.png" height="199px"> 
<img src="stemINshanghai2-p06.png" height="199px"> 
<img src="stemINshanghai2-p07.png" height="199px"> 
<img src="stemINshanghai2-p08.png" height="199px"> 
<img src="stemINshanghai2-p09.png" height="199px"> 
<img src="stemINshanghai2-p10.png" height="199px"> 
<img src="stemINshanghai2-p11.png" height="199px"> 
<img src="stemINshanghai2-p12.png" height="199px"> 
</p>

### GitSTAR 星云

星云，在 GitSTAR 中就是创意汇集的地方，而它区别于 Idea 的特征就是经过数字化工具，将创意具象化，作为需求的最好补充，必定不是所有人都可以像 特斯拉 一样所有作品的细节都能在脑海中重构，我们需要汇集大家的灵感，这不只是为了创新而创新，这是生活本身，学习即生活，生活即求知。那么，首先能称为星云的作品，就是这一期一期的封面啦 :pray:

这是一份美妙的感觉，一年前的打印画质的封面，终于可以用上了，这是一份惊喜，Surprise 是我最喜欢的 火种寄语，我们也是这样分享的，它为 SIGer 注入了最初的基因和灵魂。就在刚刚，一份捐赠意向达成，有捐赠人愿意助印 SIGer 期刊了。尽管这是作者自身的需求，我叫它 星系叠加效应，这是 SIGer 的价值。它是一份 开源精神的明证 :pray: 。在获得了作者本人同意后，我分享了这两期 打印版 SIGer 期刊，正好是第1期和第2期。时隔一年，年初的发愿《[拥抱开源](../第1期%20迎接我们未来的生活方式.md)》，轮回一年中的开源实践开花结果，美妙至极。临 PR 前，我不忘调侃下我们的基地 Gitee ，吃水不忘挖井人。我此时此刻认为，鸡啼真的不好听，“基地” 多好啊 :D 

![输入图片说明](https://images.gitee.com/uploads/images/2022/0112/124928_61f32f3d_5631341.png "屏幕截图.png")

两本正刊，欢迎助印，这个形式也将成为感谢 SIGer 志愿者最好的 伴手礼啦 :pray: 新年快乐！

<p><a href="../专访/【谢凯年】开源将成为我们未来的生活方式.pdf" target="_blank"><img src="https://images.gitee.com/uploads/images/2022/0112/125235_87e6d720_5631341.jpeg" width="299px"></a> <a href="stemINshanghai2.pdf" target="_blank"><img src="https://images.gitee.com/uploads/images/2022/0112/125249_3f588356_5631341.jpeg" width="299px"></a></p>

> 当一字排开，展示在我面前的99寄语（他们是作为 SIGer [README](../README.md) 的背景装饰设计的），突然有一种经文的感觉，这次无限宽屏的喜好，竖立起来，如同上朝的笏板，恭恭敬敬地擎在手中，上面记录的是 SIGer 作为平台征集的 [石榴派国际漂流的寄语英文版](https://gitee.com/blesschess/luckystar/issues/I3YL4C#note_6755058_link)，没有换行，首尾相接。在一篇 MD 文章不足以撑起一册期刊时，SIGer README.md 是最自然的选择，其下面的评论区则是包含热情的99寄语，原本顺序排列，篇幅过长才精简为只有英文版，正好 [README.md](../README.md) 首页展示也是收窄的形式（右侧 1/3是最近更新的空间。）排版印刷的非白处，放置这份祝福，再合适不过了。

<p title="1partOF5wishes99">
<img src="1partOF5wishes99-02.png" width="99px"> 
<img src="1partOF5wishes99-03.png" width="99px"> 
<img src="1partOF5wishes99-04.png" width="99px"> 
<img src="1partOF5wishes99-05.png" width="99px"> 
<img src="1partOF5wishes99-06.png" width="99px"> 
</p>

列在上面，只为请出之后的 [SIGer 周年宣传海报为封面](https://images.gitee.com/uploads/images/2021/1230/154825_ea5b1875_5631341.jpeg)、[封底](https://images.gitee.com/uploads/images/2021/1230/154811_d647eb1e_5631341.jpeg)的 SIGer 宣传单。起初骑马订，只有6页太少了，加上原先的海报 8 页也有些单薄，两张纸再骑马订装（浪费啦），突然闪现灵光，有了下面的 <a href="https://images.gitee.com/uploads/images/2022/0113/011704_5e8c3f23_5631341.png" title="查找资料中发现这是一门古老的印后宣传单的工艺——十字折!">立体经折版</a> ，缓缓地打开，最大限度地竖向阅读，构思印刷形式的时候，我已经满心欢喜了，就如同抄经为自己带来力量一样。这就是 2022 最好的起势，祝 SIGer 志愿者 新年顺顺利利，2022 更上一层楼，我们的队伍更壮大，我们的队伍向太阳 :pray: