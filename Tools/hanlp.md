<p><img width="706px" src="https://images.gitee.com/uploads/images/2022/0607/165642_2092fc11_5631341.jpeg" title="HANLP副本.jpg"></p>

> 该书的学习笔记见文末，包括对作者的致谢！比起学习笔记，本篇的试用攻略，更能表达。

# 浪漫诗语评测始末 —— 浪漫指数的诞生

Hanlp 是最 Nice 的开源 NLP 项目，用 NICE 这个评语是为了扣题 “浪漫”。因为本期的主题缘起来自一位 宇宙诗人，用 Nice 评价女孩也是再好不过的啦。一上来咬文嚼字这么些词，估计 Hanlp 会有自己的学术方法。今天我将自己的实用心得分享给大家。

上文章：《[和唯一知道星星为什么会发光的人一起散步](https://mp.weixin.qq.com/s?__biz=MzIyNjY0MjE1NQ==&mid=2247483983&idx=1&sn=a2224d18b877c1dc543207bab9f44f61)》这是一个宇宙主题的公号作者的代表作，被收录在 [SIGer](https://gitee.com/flame-ai/siger) [天文频道](https://gitee.com/flame-ai/siger/tree/master/sig/%E5%A4%A9%E6%96%87) 内，这是这个假期最值得期待的内容频道了，这不马上就要上天的巡天星空望远镜，已经和胖五合体了。前不久的韦伯望远镜，也把人们的星辰大海情节调足了胃口。我们 天文社的同学，直接发刊《[没有韦伯，也能星辰大海](https://gitee.com/flame-ai/siger/blob/master/sig/%E5%A4%A9%E6%96%87/%E9%81%A5%E6%9C%9B%E6%98%9F%E8%BE%B0%E5%A4%A7%E6%B5%B7.md)》，准备DIY望远镜，比肩不了镜头，后期凑。按下不表，直奔 NLP 主题。

- 对公号进行 NLP 处理，建立一个关键词库，主要是星体名称，专家名录，天文事件等
- 对文章形成关键字索引和摘要

这都是典型的 NLP 应用，由于微信公号的特殊性，爬虫基本无效，只能手工下载，小小试水 Hanlp 的 功力！测试分两个部分：

- Hanlp 1.8
- Hanlp 2.1 RESTFUL 在线 API

## Hanlp 1.8 初体验

为什么说它 Nice ，比起其他 NLP 开源项目，构建出可以运行版本本身对于小白就是困难的，很多开源项目并没有可用的语料库，就别说只管体会了。Hanlp 最初是通过《自然语言处理入门》找到的，BBS，各种文档，开源社区完备，尤其是作者的观点，从工程的角度入手 NLP 真的是最适合 SIGer 场景的学习环境了。这里补一些 SIGer 的介绍：

> SIGer 最初是为青少年学习开源社区而设置的开源项目，通过共同编辑一本科普自媒体在线期刊，体验开源协作的工作过程，SIGer 仓中主要是文本 MD，辅以一些简单的 PY 脚本处理一些网页的信息提取，曰数字化工具。随着参与的同学越来越多，分享的内容也与日俱增，经过 2021年 一年的发展，2022年初确定了 科普自媒体的定位，内容聚合加速增长。这时参与分享的同学的工作量就下来了。原本随意的分享，在得到其他同学的呼应后，就对分享内容的丰富度和质素有个新的要求，参考的资料更多，编纂整理的工作量也加大了。从 2022年初开始，增设了频道建设的概念，通过聚合相同兴趣的同学，升级为内容聚合的需求。现在 SIGer 的编委们（分享的同学的身份名称）主要还是手工方式 通过搜索引擎检索网上的内容，作为素材，或转载，或编辑，当然原创是现在的主要抓手，但学习方式的重要形式，就是对转载和编辑的内容进行提炼，曰学习笔记，成为 当下 SIGer 的主要内容源。

介绍到这里，大家就知道 SIGer 上 NLP 的作用啦吧。

为什么用 初体验，是因为 Hanlp 1.8 主要是配合 《自然语言处理入门》的学习配备的。为了兼顾入门的学习需求，工程需求的满足上多少还是无法实现，第一个就是语料库的规模还有 1.8 版本的功能局限，至少它让我成为 Hanlp 的忠粉起到了决定的作用，别说小白啦，没有一定的计算机基础是别想摸到 NLP 的大门的。这里对 Hanlp 的转化率捏一把汗啊。这已经是被评为 最 NICE 的 NLP 啦。下面的测试，已经是摸索过一段时间的成果了，一看就是轻车熟路的。

### 运行 Hanlp

运行环境是 树莓派4B+ 8g版，32 bit Raspbian OS，  
主要遇到的问题是 JAVA 虚拟机无法访问4G以上内存问题，这个 declare 是经过 Hanlp 开发者社区问答实践出的。  
本文，作为基本了解 Hanlp 开发者社区的读者。

```
declare -x HANLP_JVM_XMX="2g"

hanlp -h
usage: hanlp [-h] [-v] {segment,parse,serve,update} ...

HanLP: Han Language Processing v1.8.3

positional arguments:
  {segment,parse,serve,update}
                        which task to perform?
    segment             word segmentation
    parse               dependency parsing
    serve               start http server
    update              update jar and data of HanLP

optional arguments:
  -h, --help            show this help message and exit
  -v, --version         show installed versions of HanLP
```

### [hanlp parse](https://gitee.com/MadNug/siger/issues/I5H96I#note_11656526_link)


```
1	韦布	韦布	nh	nrf	_	2	主谓关系	_	_
2	拍摄	拍摄	v	v	_	5	定中关系	_	_
3	的	的	u	u	_	2	右附加关系	_	_
4	图像	图像	n	n	_	5	定中关系	_	_
5	分辨率	分辨率	n	n	_	29	主谓关系	_	_
6	比	比	p	p	_	29	状中结构	_	_
7	前辈	前辈	n	n	_	8	定中关系	_	_
8	哈勃	哈勃	nh	nrf	_	10	主谓关系	_	_
9	清晰	清晰	nt	t	_	10	状中结构	_	_
10	很多	很多	m	m	_	6	介宾关系	_	_
11	，	，	wp	w	_	10	标点符号	_	_
12	哈勃	哈勃	nh	nrf	_	14	主谓关系	_	_
13	主要	主要	b	b	_	14	状中结构	_	_
14	观测	观测	v	vn	_	10	并列关系	_	_
15	可见光	可见光	n	n	_	14	动宾关系	_	_
16	、	、	wp	w	_	17	标点符号	_	_
17	紫外线	紫外线	n	n	_	15	并列关系	_	_
18	和	和	c	c	_	21	左附加关系	_	_
19	少量	少量	m	m	_	21	定中关系	_	_
20	的	的	u	u	_	19	右附加关系	_	_
21	红外线	红外线	n	n	_	15	并列关系	_	_
22	，	，	wp	w	_	14	标点符号	_	_
23	由于	由于	c	c	_	29	状中结构	_	_
24	高红	高红	ns	ns	_	25	主谓关系	_	_
25	移	移	v	v	_	28	定中关系	_	_
26	的	的	u	u	_	25	右附加关系	_	_
27	星系	星系	n	n	_	28	定中关系	_	_
28	能量	能量	n	n	_	29	主谓关系	_	_
29	分布	分布	v	v	_	0	核心关系	_	_
30	在	在	p	p	_	29	动补结构	_	_
31	红外	红外	b	b	_	32	定中关系	_	_
32	波段	波段	n	n	_	30	介宾关系	_	_
33	，	，	wp	w	_	29	标点符号	_	_
34	因此	因此	c	c	_	37	状中结构	_	_
35	韦布	韦布	nh	nrf	_	37	主谓关系	_	_
36	更	更	d	d	_	37	状中结构	_	_
37	适合	适合	v	v	_	29	并列关系	_	_
38	探测	探测	v	v	_	37	动宾关系	_	_
39	宇宙	宇宙	n	n	_	40	定中关系	_	_
40	早期	早期	nd	f	_	42	定中关系	_	_
41	的	的	u	u	_	40	右附加关系	_	_
42	天体	天体	n	n	_	38	动宾关系	_	_
43	。	。	wp	w	_	29	标点符号	_	_
44	望远镜	望远镜	n	n	_	46	主谓关系	_	_
45	一	一	d	d	_	46	状中结构	_	_
46	共有	共有	v	v	_	29	并列关系	_	_
47	4	4	m	m	_	48	定中关系	_	_
48	个	个	q	q	_	49	定中关系	_	_
49	仪器	仪器	n	n	_	46	动宾关系	_	_
50	，	，	wp	w	_	46	标点符号	_	_
51	其中	其中	r	r	_	52	定中关系	_	_
52	一台	一台	m	m	_	53	主谓关系	_	_
53	是	是	v	v	_	46	并列关系	_	_
54	近红外	近红外	i	l	_	55	定中关系	_	_
55	光谱仪	光谱仪	n	n	_	53	动宾关系	_	_
56	，	，	wp	w	_	53	标点符号	_	_
57	这	这	r	r	_	58	定中关系	_	_
58	台	台	q	q	_	59	定中关系	_	_
59	仪器	仪器	n	n	_	65	主谓关系	_	_
60	可以	可以	v	v	_	65	状中结构	_	_
61	把	把	p	p	_	65	状中结构	_	_
62	恒星	恒星	n	n	_	64	定中关系	_	_
63	的	的	u	u	_	62	右附加关系	_	_
64	光	光	d	d	_	61	介宾关系	_	_
65	转换	转换	v	v	_	53	并列关系	_	_
66	为	为	v	v	_	65	动补结构	_	_
67	光谱	光谱	n	n	_	66	动宾关系	_	_
68	，	，	wp	w	_	53	标点符号	_	_
69	数据	数据	n	n	_	70	主谓关系	_	_
70	显示	显示	v	v	_	53	并列关系	_	_
71	，	，	wp	w	_	70	标点符号	_	_
72	韦布	韦布	nh	nrf	_	73	定中关系	_	_
73	望远镜	望远镜	n	n	_	74	主谓关系	_	_
74	捕捉到	捕捉到	i	i	_	70	动宾关系	_	_
75	了	了	u	u	_	74	右附加关系	_	_
76	131亿年前	131亿年前	nt	t	_	79	状中结构	_	_
77	早期	早期	nd	f	_	79	状中结构	_	_
78	宇宙	宇宙	n	n	_	79	主谓关系	_	_
79	发出	发出	v	v	_	81	定中关系	_	_
80	的	的	u	u	_	79	右附加关系	_	_
81	光	光	n	n	_	74	动宾关系	_	_
82	。	。	wp	w	_	29	标点符号	_	_

```


### EXCEL 截图

![输入图片说明](https://images.gitee.com/uploads/images/2022/0715/130635_0ebe3114_5631341.png "2022-07-15-130526_1680x1050_scrot.png")

### 摘要

超大质量星系

船底座星云（NGC 3324）

斯蒂芬的五重奏
- 飞马座


NGC 7319  
超大质量黑洞

（NGC 7320）

南环星云

WASP  
气态巨行星


凌日法

气态巨行星

詹姆斯·韦伯太空望远镜

> 以上摘要，是人工从 PARSE 中挑选出来的，这是 Hanlp 1.8 现有的能力展示，看着丰富的注释和语义的层次关系，都认识，但是不知道怎么用。看来，我这个学生的学习还不够精良，还没学出个所以然，就想操刀上阵了。这个时候，最需要的就是老师的鼓励啦。

  - 上面的尝试是另一篇文章《[詹姆斯·韦布壁纸收藏 | 人类文明追寻星光的里程碑](https://mp.weixin.qq.com/s?__biz=MzIyNjY0MjE1NQ==&mid=2247487177&idx=1&sn=c5a0db055d65bf3477db2004c53ee0bb)》

### [hanlp segment](https://gitee.com/MadNug/siger/issues/I5H96I#note_11692248_link)

关注该公众号你/rr 说/v 你/rr 喜欢/vi 黑夜/n ，/w 我/rr 想/v 是因为/c 白天/t 只能/v 看到/v 1.5/m 亿/m 公里/q 外/f 的/ude1 太阳/n ，/w 而/cc 夜晚/n 却/d 可以/v 看到/v 数/n 亿/m 光年/qt 外/f 的/ude1 星系/n 。/w

你/rr 带/v 我/rr 见过/v 宇宙/n ，/w 现在/t ，/w 我/rr 来/vf 帮/v 你/rr 造/v 一/m 片/q 星空/n 。/w

> ... 此处省略 1000字 ...

> 同样的是我认识每一个字母，但是不知道怎么用这些丰富的语义信息。

### hanlp parse 的执行效率

![输入图片说明](https://images.gitee.com/uploads/images/2022/0718/095637_69a583fb_5631341.png "屏幕截图.png")

> 通过 MobaXterm 的终端信息，我们可以看到每次运行的 内存使用情况，如果不在开始 Dclare 估计，能拉多满，就怎么拉满啦。Hanlp 的引擎是用 JAVA 实现的。保证其跨平台性。

### hanlp parse 全文，信息是海量的。

NGC4302（1      和      和      c       c       _       3       左附加关系      _     _
2       唯一    唯一    b       b       _       3       状中结构        _       _
3       知道    知道    v       v       _       0       核心关系        _       _
4       星星    星星    n       n       _       9       定中关系        _       _
5       为什么  为什么  r       r       _       9       定中关系        _       _
6       会      会      v       v       _       7       状中结构        _       _
7       发光    发光    v       v       _       9       定中关系        _       _
8       的      的      u       u       _       7       右附加关系      _       _
9       人      人      n       n       _       11      主谓关系        _       _
10      一起    一起    d       d       _       11      状中结构        _       _
11      散步    散步    v       v       _       3       动宾关系        _       _

> 这下面是真的要省略 一万字了。分两个帖子才贴出来，每个帖子 60000字起

  - https://gitee.com/MadNug/siger/issues/I5H96I#note_11692532_link
  - https://gitee.com/MadNug/siger/issues/I5H96I#note_11692555_link

> 这是把一个句子的信息，扩展了好几倍，用来机器学习和研究啊。这那时人眼能识别的呢？知难而退啦。只能求助 老师啊！

自此，膜拜之情，不言而喻啦吧！ :pray:

## Hanlp 2.1 RESTFUL 在线 API

### [HanLP2.1 RESTful API开源社区免费申请](https://bbs.hanlp.com/t/hanlp2-1-restful-api/53)

HanLP-API 20年12月

经过一年的打磨，HanLP2.1已经于元旦为大家贺岁。这次2.1为全新设计，带来了许多新功能与准确率提升。HanLP2.1支持中英日法等在内的104种语言的十多种任务：

- 分词 153
  - 粗分、细分2个标准
  - 强制、合并、校正3种词典模式
- 词性标注 55
  - PKU、863、CTB三套词性规范
- 命名实体识别 108
  - 人民日报PKU、微软MSRA、OntoNotes三套规范
- 依存句法分析 50
  - CTB规范
- 短语句法分析 33
  - CTB规范
- 语义依存分析 40
  - 北语规范
- 语义角色标注 27
  - CPB规范
- 语义文本相似度 121
- 文本风格转换 48
- 指代消解 55
- 词干提取 59
- 词法语法特征提取 59
- 抽象意义表示 8（Abstract Meaning Representation）
- 关键词提取 45
- 抽取式自动摘要 17

其中，RESTful API 可能最受大家期待。我们决定上线社区API方便大家快速体验HanLP2.1，RESTful API的调用演示如下：

### Python
安装客户端：

- pip install hanlp_restful

调用：


```
from hanlp_restful import HanLPClient
HanLP = HanLPClient('https://www.hanlp.com/api', auth='你申请到的auth')  # auth需要申请
doc = HanLP.parse('2021年HanLPv2.1为生产环境带来次世代最先进的多语种NLP技术。阿婆主来到北京立方庭参观自然语义科技公司。')

```

同时，Python HanLP提供基于等宽字体的可视化：

- doc.pretty_print(ner='ner/msra')

能够直接将语言学结构在你的控制台内可视化出来：

![输入图片说明](https://images.gitee.com/uploads/images/2022/0718/101016_642119cd_5631341.png "屏幕截图.png")

为了降低安装和部署的成本，也为了回馈开源社区，我们专门拿出服务器运行社区版API做公益。

社区版API面向学术界NLP研究者和公司技术选型人员免费提供，不收取任何费用亦不提供任何担保。社区版API通过本社区进行身份认证，并根据用户对开源社区的贡献分配调用额度。调用额度以每分钟的请求数度量，针对parse接口，每个请求最大可分析256个句子，约为1.5万字。

### 开源贡献	调用额度

- 论坛信用评级Lv1	4
- 论坛信用评级Lv2	20
- 论坛信用评级Lv3	30
- 论坛每获得10个赞	+10
- 论坛绑定edu邮箱	+10
- GitHub上为HanLP加星	+40
- GitHub上fork HanLP	+6
- GitHub上每找出HanLP的一个bug	+20
- GitHub上每为HanLP提交一次代码	+20

例如，你在论坛上信用评级为Lv1，同时在GitHub上为HanLP加了星并fork的话，你的调用额度总计为每分钟4+40+6=50个请求，等于12800个句子或75万个字符。虽然请求数看上去不高，但由于一个请求可以分析多个句子，最终吞吐量还是比较可观。由于庞大的用户数和有限且昂贵的GPU资源，我们暂时约定这个额度，但未来极有可能等比扩大调用频率。

我们也将根据你的开源贡献，定期更新你的调用额度。请在论坛上绑定GitHub账户，以便我们统计你的开源贡献。

### 文档

HanLP的在线文档地址为：[HanLP: Han Language Processing — HanLP Documentation](https://hanlp.hankcs.com/docs/) 487

标注集的说明文档位于：[Annotations — HanLP Documentation](https://hanlp.hankcs.com/docs/annotations/index.html) 102

### pip install hanlp_restful

```
Looking in indexes: https://pypi.org/simple, https://www.piwheels.org/simple
Collecting hanlp_restful
  Downloading https://www.piwheels.org/simple/hanlp-restful/hanlp_restful-0.0.17-py3-none-any.whl
Collecting hanlp-common (from hanlp_restful)
  Downloading https://www.piwheels.org/simple/hanlp-common/hanlp_common-0.0.18-py3-none-any.whl
Collecting phrasetree (from hanlp-common->hanlp_restful)
  Downloading https://www.piwheels.org/simple/phrasetree/phrasetree-0.0.8-py3-none-any.whl (45kB)
    100% |████████████████████████████████| 51kB 5.4kB/s
Installing collected packages: phrasetree, hanlp-common, hanlp-restful
Successfully installed hanlp-common-0.0.18 hanlp-restful-0.0.17 phrasetree-0.0.8

```

> 和 Hanlp 的安装一样 Nice 

- [UnicodeEncodeError: 'latin-1' codec can't encode characters in position 6-10: ordinal not in range(256)](https://gitee.com/MadNug/siger/issues/I5H96I#note_11693534_link)

  > [UnicodeEncodeError: 'latin-1' codec can't encode characters in position 41-50: ordinal not in range(256)](https://blog.csdn.net/aaronthon/article/details/87261924)

实际是虚惊一场，不能在 PYTHON3 命令行中执行，必须在 PY 文件中执行。

### 运行 hanlp_restful

- nano tmp.py


```
from hanlp_restful import HanLPClient
HanLP = HanLPClient('https://www.hanlp.com/api', auth='auth')  # auth需要申请

```


- python3 tmp.py

```
Traceback (most recent call last):
  File "tmp.py", line 3, in <module>
    doc = HanLP.parse('2021年HanLPv2.1为生产环境带来次世代最先进的多语种NLP技术。阿婆主来到北京立方庭参观自然语义科技公司。')
  File "/home/pi/.local/lib/python3.7/site-packages/hanlp_restful/__init__.py", line 91, in parse
    'language': language or self._language
  File "/home/pi/.local/lib/python3.7/site-packages/hanlp_restful/__init__.py", line 129, in _send_post_json
    return json.loads(_post(url, form, headers, self._timeout))
  File "/home/pi/.local/lib/python3.7/site-packages/hanlp_restful/__init__.py", line 19, in _post
    raise HTTPError(url, response.status_code, response.text, response.headers, None)
urllib.error.HTTPError: HTTP Error 401: {"detail":"Invalid authentication credentials"}
```

> 在线 api 没有 授权码，怎么可能运行呢？

### [注册不了 ＨＡＮＬＰ 收不到注册邮件！](https://bbs.hankcs.com/t/topic/4838)

最后通过联系到 HANLP 托管的服务商的客服，终于拿到了 18h 的体验机会。

```
## 姓名

## 单位

## 联系电话
<!-- 请填写手机号以便接收秘钥auth -->

## 专业/行业

## 申请下列API
<!-- 括号内输入x勾选，可多选 -->

- [√] 分词
- [√] 词性标注
- [√] 命名实体识别
- [√] 依存句法分析
- [√] 短语结构分析
- [√] 语义角色标注
- [√] 语义依存分析
- [√] 语义文本相似度
- [√] 文本风格转换
- [√] 指代消解
- [√] 抽象意义表示
- [√] 关键词提取
- [√] 抽取式自动摘要

## 免责声明
我已同意：HanLP中文社区提供的社区版API仅供技术选型和学术研究使用，不得用于其他目的。该API不提供任何担保，包括但不限于准确率、稳定性和响应速度。该API的调用额度的解释权归API运营方所有，且运营方有权随时进行调整而不另行通知。使用API时必须遵守当地法律法规，API运营方有权将使用记录提供给司法机构。

#### 申请人签名:
```

- pi@raspberrypi:~/SIGerot $ python3 tmp.py

```
Dep Tree        Token           Relation        PoS     Token           NER Type               Token           SRL PA1         Token           SRL PA2         Token         PoS    3       4       5       6       7       8       9       10      11      12
─────────────   ─────────       ───────────     ───     ─────────       ────────────────       ─────────       ────────────    ─────────       ────────────    ─────────     ─────────────────────────────────────────────────────────────────────────────────
    ┌───────►   2021年          nmod:tmod       NT      2021年          ───►DATE               2021年          ───►ARGM-TMP    2021年                          2021年        NT ───────────────────────────────────────────────────────────────────►NP ───┐
    │┌──────►   HanLPv2.1       nsubj           NR      HanLPv2.1       ───►ORGANIZATION       HanLPv2.1       ───►ARG0        HanLPv2.1                       HanLPv2.1     NR ───────────────────────────────────────────────────────────────────►NP────┤
    ││   ┌──►   为              case            P       为                                     为              ◄─┐             为                              为            P ───────────┐                                                               │
    ││   │┌─►   生产            compound:nn     NN      生产                                   生产              ├►ARG2        生产                            生产          NN ──┐       ├────────────────────────────────────────────────►PP ───┐       │
    ││┌─►└┴──   环境            nmod:prep       NN      环境                                   环境            ◄─┘             环境                            环境          NN ──┴►NP ───┘                                                       │       │
┌┬──┴┴┴──────   带来            root            VV      带来                                   带来            ╟──►PRED        带来                            带来          VV ──────────────────────────────────────────────────────────┐       │       │
││        ┌─►   次              amod            JJ      次                                     次              ◄─┐             次              ◄─┐             次            JJ ───►ADJP──┐                                               │       ├►VP────┤
││     ┌─►└──   世代            dep             NN      世代                                   世代              │             世代            ◄─┴►ARGM-TMP    世代          NN ───►NP ───┴►NP ───┐                                       │       │       │
││     │  ┌─►   最              advmod          AD      最                                     最                │             最              ───►ARGM-ADV    最            AD ───────────►ADVP──┼►VP ────►IP ───┐                       │       │       ├►IP
││  ┌─►└──┼──   先进            amod            VA      先进                                   先进              │             先进            ╟──►PRED        先进          VA ───────────►VP ───┘               ├►CP ────►CP ───┐       ├►VP ───┘       │
││  │     └─►   的              mark            DEC     的                                     的                ├►ARG1        的                              的            DEC──────────────────────────────────┘               │       │               │
││  │     ┌─►   多              dep             CD      多                                     多                │             多                              多            CD ───►QP ───┐                                       │       │               │
││  │  ┌─►└──   语种            compound:nn     NN      语种                                   语种              │             语种                            语种          NN ───►NP ───┴────────────────────────────────►NP────┼►NP ───┘               │
││  │  │  ┌─►   NLP             compound:nn     NN      NLP                                    NLP               │             NLP             ◄─┐             NLP           NN ──┐                                               │                       │
│└─►└──┴──┴──   技术            dobj            NN      技术                                   技术            ◄─┘             技术            ◄─┴►ARG0        技术          NN ──┴────────────────────────────────────────►NP ───┘                       │
└───────────►   。              punct           PU      。                                     。                              。                              。            PU ──────────────────────────────────────────────────────────────────────────┘

Dep Tree        Tok     Relation        Po      Tok     NER Type                Tok   SRL PA1  Tok     SRL PA2         Tok     Po    3       4       5       6
────────────    ───     ───────────     ──      ───     ────────────────        ───   ──────── ───     ────────        ───     ────────────────────────────────
         ┌─►    阿婆主  nsubj           NN      阿婆主                          阿婆主───►ARG0 阿婆主  ───►ARG0        阿婆主  NN───────────────────►NP ───┐
┌┬────┬──┴──    来到    root            VV      来到                            来到  ╟──►PRED 来到                    来到    VV──────────┐               │
││    │  ┌─►    北京    name            NR      北京    ◄─┐                     北京  ◄─┐      北京                    北京    NR──┐       ├►VP ───┐       │
││    └─►└──    立方庭  dobj            NR      立方庭  ◄─┴►LOCATION            立方庭◄─┴►ARG1 立方庭                  立方庭  NR──┴►NP ───┘       │       │
│└─►┌───────    参观    conj            VV      参观                            参观           参观    ╟──►PRED        参观    VV──────────┐       ├►VP────┤
│   │  ┌───►    自然    compound:nn     NN      自然    ◄─┐                     自然  ◄─┐      自然    ◄─┐             自然    NN──┐       │       │       ├►IP
│   │  │┌──►    语义    compound:nn     NN      语义      │                     语义    │      语义      │             语义    NN  │       ├►VP ───┘       │
│   │  ││┌─►    科技    compound:nn     NN      科技      ├►ORGANIZATION        科技    ├►ARG1 科技      ├►ARG1        科技    NN  ├►NP ───┘               │
│   └─►└┴┴──    公司    dobj            NN      公司    ◄─┘                     公司  ◄─┘      公司    ◄─┘             公司    NN──┘                       │
└──────────►    。      punct           PU      。                              。             。                      。      PU──────────────────────────┘

```

### 自动摘要：extractive_summarization

https://hanlp.hankcs.com/docs/api/restful.html
 
- extractive_summarization(text: str, topk: int = 3, language: Optional[str] = None)→ Dict[str, float]

[source]

```
    def extractive_summarization(
            self,
            text: str,
            topk: int = 3,
            language: str = None,
    ) -> Dict[str, float]:
        """ Single document summarization is the task of selecting a subset of the sentences which best
        represents a summary of the document, with a balance of salience and redundancy.

        Args:
            text: The text content of the document.
            topk: The maximum number of top-K ranked sentences. Note that due to Trigram Blocking tricks, the actual
                number of returned sentences could be less than ``topk``.
            language: The language of input text or tokens. ``None`` to use the default language on server.

        Returns:
            A dictionary containing each sentence and its ranking score :math:`s \in [0, 1]`.

        Examples::

            HanLP.extractive_summarization('''
            据DigiTimes报道，在上海疫情趋缓，防疫管控开始放松后，苹果供应商广达正在逐步恢复其中国工厂的MacBook产品生产。
            据供应链消息人士称，生产厂的订单拉动情况正在慢慢转强，这会提高MacBook Pro机型的供应量，并缩短苹果客户在过去几周所经历的延长交货时间。
            仍有许多苹果笔记本用户在等待3月和4月订购的MacBook Pro机型到货，由于苹果的供应问题，他们的发货时间被大大推迟了。
            据分析师郭明錤表示，广达是高端MacBook Pro的唯一供应商，自防疫封控依赖，MacBook Pro大部分型号交货时间增加了三到五周，
            一些高端定制型号的MacBook Pro配置要到6月底到7月初才能交货。
            尽管MacBook Pro的生产逐渐恢复，但供应问题预计依然影响2022年第三季度的产品销售。
            苹果上周表示，防疫措施和元部件短缺将继续使其难以生产足够的产品来满足消费者的强劲需求，这最终将影响苹果6月份的收入。
            ''')
            # Output:
            {'据DigiTimes报道，在上海疫情趋缓，防疫管控开始放松后，苹果供应商广达正在逐步恢复其中国工厂的MacBook产品生产。': 0.9999,
             '仍有许多苹果笔记本用户在等待3月和4月订购的MacBook Pro机型到货，由于苹果的供应问题，他们的发货时间被大大推迟了。': 0.5800,
             '尽管MacBook Pro的生产逐渐恢复，但供应问题预计依然影响2022年第三季度的产品销售。': 0.5422}
        """
        assert text, 'Text has to be non-empty.'
        return self._send_post_json(self._url + '/extractive_summarization', {
            'text': text,
            'language': language or self._language,
            'topk': topk,
        })
```


> Single document summarization is the task of selecting a subset of the sentences which best represents a summary of the document, with a balance of salience and redundancy.

- Parameters
  -  **text**  – The text content of the document.

  -  **topk**  – The maximum number of top-K ranked sentences. Note that due to Trigram Blocking tricks, the actual number of returned sentences could be less than topk.

  -  **language**  – The language of input text or tokens. None to use the default language on server.

- Returns

  A dictionary containing each sentence and its ranking score $s \in [0, 1]$ .

- Examples:


```
HanLP.extractive_summarization('''
据DigiTimes报道，在上海疫情趋缓，防疫管控开始放松后，苹果供应商广达正在逐步恢复其中国工厂的MacBook产品生产。
据供应链消息人士称，生产厂的订单拉动情况正在慢慢转强，这会提高MacBook Pro机型的供应量，并缩短苹果客户在过去几周所经历的延长交货时间。
仍有许多苹果笔记本用户在等待3月和4月订购的MacBook Pro机型到货，由于苹果的供应问题，他们的发货时间被大大推迟了。
据分析师郭明錤表示，广达是高端MacBook Pro的唯一供应商，自防疫封控依赖，MacBook Pro大部分型号交货时间增加了三到五周，
一些高端定制型号的MacBook Pro配置要到6月底到7月初才能交货。
尽管MacBook Pro的生产逐渐恢复，但供应问题预计依然影响2022年第三季度的产品销售。
苹果上周表示，防疫措施和元部件短缺将继续使其难以生产足够的产品来满足消费者的强劲需求，这最终将影响苹果6月份的收入。
''')
```

- Output:

```
{'据DigiTimes报道，在上海疫情趋缓，防疫管控开始放松后，苹果供应商广达正在逐步恢复其中国工厂的MacBook产品生产。': 0.9999,
 '仍有许多苹果笔记本用户在等待3月和4月订购的MacBook Pro机型到货，由于苹果的供应问题，他们的发货时间被大大推迟了。': 0.5800,
 '尽管MacBook Pro的生产逐渐恢复，但供应问题预计依然影响2022年第三季度的产品销售。': 0.5422}
```

- python3 zhaiyao.py

```
from hanlp_restful import HanLPClient
HanLP = HanLPClient('https://www.hanlp.com/api', auth='MjNAYmJzLmhhbmxwLmNvbTpuckRoRDV$
doc = HanLP.extractive_summarization('''
据DigiTimes报道，在上海疫情趋缓，防疫管控开始放松后，苹果供应商广达正在逐步恢复其中国>$
据供应链消息人士称，生产厂的订单拉动情况正在慢慢转强，这会提高MacBook Pro机型的供应量>$
仍有许多苹果笔记本用户在等待3月和4月订购的MacBook Pro机型到货，由于苹果的供应问题，他>$
据分析师郭明錤表示，广达是高端MacBook Pro的唯一供应商，自防疫封控依赖，MacBook Pro大部$
一些高端定制型号的MacBook Pro配置要到6月底到7月初才能交货。
尽管MacBook Pro的生产逐渐恢复，但供应问题预计依然影响2022年第三季度的产品销售。
苹果上周表示，防疫措施和元部件短缺将继续使其难以生产足够的产品来满足消费者的强劲需求，$
''')

print( doc );

```

- output:


```
{'据DigiTimes报道，在上海疫情趋缓，防疫管控开始放松后，苹果供应商广达正在逐步恢复其中国工厂的MacBook产品生产。': 0.9999580383300781, '仍有许多苹果笔记本用户在等待3月和4月订购的MacBook Pro机型到货，由于苹果的供应问题，他们的发货时间被大大推迟了。': 0.6589649319648743, '苹果上周表示，防疫措施和元部件短缺将继续使其难以生产足够的产品来满足消费者的强劲需求，这最终将影响苹果6月份的收入。': 0.09875578433275223}

```

### 自动关键字：keyphrase_extraction

https://hanlp.hankcs.com/docs/api/restful.html

- keyphrase_extraction(text: str, topk: int = 10, language: Optional[str] = None)→ Dict[str, float]

[source]


```
    def keyphrase_extraction(
            self,
            text: str,
            topk: int = 10,
            language: str = None,
    ) -> Dict[str, float]:
        """ Keyphrase extraction aims to identify keywords or phrases reflecting the main topics of a document.

        Args:
            text: The text content of the document. Preferably the concatenation of the title and the content.
            topk: The number of top-K ranked keywords or keyphrases.
            language: The language of input text or tokens. ``None`` to use the default language on server.

        Returns:
            A dictionary containing each keyword or keyphrase and its ranking score :math:`s`, :math:`s \in [0, 1]`.

        Examples::

            HanLP.keyphrase_extraction(
                '自然语言处理是一门博大精深的学科，掌握理论才能发挥出HanLP的全部性能。 '
                '《自然语言处理入门》是一本配套HanLP的NLP入门书，助你零起点上手自然语言处理。', topk=3)
            # Output:
            {'自然语言处理': 0.800000011920929,
             'HanLP的全部性能': 0.5258446335792542,
             '一门博大精深的学科': 0.421421080827713}
        """
        assert text, 'Text has to be specified.'
        return self._send_post_json(self._url + '/keyphrase_extraction', {
            'text': text,
            'language': language or self._language,
            'topk': topk,
        })
```


> Keyphrase extraction aims to identify keywords or phrases reflecting the main topics of a document.

- Parameters

  - text – The text content of the document. Preferably the concatenation of the title and the content.

  - topk – The number of top-K ranked keywords or keyphrases.

  - language – The language of input text or tokens. None to use the default language on server.

- Returns

  A dictionary containing each keyword or keyphrase and its ranking score $s$, $s \in [0, 1]$.

- Examples:


```
HanLP.keyphrase_extraction(
    '自然语言处理是一门博大精深的学科，掌握理论才能发挥出HanLP的全部性能。 '
    '《自然语言处理入门》是一本配套HanLP的NLP入门书，助你零起点上手自然语言处理。', topk=3)
```

- Output:

```
{'自然语言处理': 0.800000011920929,
 'HanLP的全部性能': 0.5258446335792542,
 '一门博大精深的学科': 0.421421080827713}
```

- python3 key.py

```
from hanlp_restful import HanLPClient
HanLP = HanLPClient('https://www.hanlp.com/api', auth='MjNAYmJzLmhhbmxwLmNvbTpuckRoRDV$
doc = HanLP.keyphrase_extraction(
    '自然语言处理是一门博大精深的学科，掌握理论才能发挥出HanLP的全部性能。 '
    '《自然语言处理入门》是一本配套HanLP的NLP入门书，助你零起点上手自然语言处理。', to$

print( doc );
```
- output:


```
{'自然语言处理': 0.800000011920929, '自然语言': 0.5408431887626648, 'HanLP的全部性能': 0.5258457660675049}
```

## 真刀真枪

### 整理文本，可不能 CTRL-C CTRL-V 啦。

```
'和唯一知道星星为什么会发光的人一起散步'
'宇宙浩瀚，岁月悠长，我始终愿意和你讲关于这个宇宙的故事。'
'如今，宇宙中存在着大量的星系、恒星与行星，而且至少在其中的一颗星球上，还生存着可赞可叹的生命。如此这般，怎能依然将时间视作破坏者？它明明花一百五十亿年创造了自己的杰作——你。'
'宇宙中的能量不会被制造出来，也不会被毁灭。'
'这意味着我们体内蕴含的所有能量，每一个粒子，都会成为别的事物的一份子，也许是海蛾鱼，也许是微生物，也许在百亿年之后被超新星燃烧掉。'
'而现在构成我们身体的每个粒子，都曾经是别的事物的一份子，可能来自月亮，积雨云，或者来自猛犸，或者是猴子，成千上万的美丽生物，就像我们一样惧怕死亡，我们赋予他们新生，在几十年前的今天，他们重新拥有了生命，那个生命就是你。'
'你说你喜欢黑夜，我想是因为白天只能看到1.5亿公里外的太阳，而夜晚却可以看到数亿光年外的星系。'
'你带我见过宇宙，现在，我来帮你造一片星空。'
'我们与那些遥远星系息息相关，无论它们是如何与我们天各一方，那些经过数十亿年旅行到达地球的光线，最终会把我们联系在一起。'
'所以为什么喜欢星辰呢，因为你就源自于他。'
'此篇文章仅送给一位叫「冥王星」的人，我想祝他：生日快乐。'
'宇宙中的海底世界 宇宙礁'
'巨大的红色星云（NGC 2014）和较小的蓝色邻居（NGC 2020）是大麦哲伦星系中一个巨大恒星形成区域的一部分。'
'这个星系被命名为宇宙礁，因为他类似于地球上的海底世界。'
'一个由大约3000颗恒星组成的巨大星团'
'一个距离地球2万光年的巨大星团，包含了我们银河系中最热、最亮、质量最大的恒星。 它的一些最重的恒星释放出大量的紫外线和飓风般的带电粒子风，腐蚀着包裹着的氢气云。'
'或许这是你喜欢的宇宙的模样'
'M17，这里是银河系中最大的恒星形成区之一，包含银河系中年轻的星团，丰富的宇宙气体和尘埃形成了许多炽热的年轻大质量恒星。'
'喧闹的恒星繁殖场，NGC2070'
'这张照片揭示了恒星诞生的各个阶段，从几千年前仍被黑暗气体包裹的胚胎恒星，到在超新星爆炸中英年早逝的庞然大物。图中还包含了一些旋转速度很快近乎疯狂的失控恒星。'
'在中央偏左闪耀的区域是一个巨大的年轻星团，命名为NGC2070，只有200万年的历史。'
'长达几十亿年的合并, M82'
'图中被命名为M82的星系其实距离我们有1200万光年，由于他的邻居M81星系的引力过于强大，使他成为了一种不规则星系，在今后的几十亿年中，持续不断的引力交汇将使他们最终走向合并。'
'在2009年，科学家接受到来自M82星系的无线电波信号，但至今无法确定是生物还是物质所放出来的。'
'有趣的对数螺旋, 图片 M51'
'M51是最早被发现的旋涡星云之一，右上角的伴星系NGC5195似乎改变了他面前旋臂的走向。'
'就像我遇见你，从此改变了我的人生轨迹。'
'奇怪的环状星系'
'这究竟是一个星系还是两个？'
'在这个天体的外围由明亮的蓝色恒星组成的圆环，内侧中心的地方是由红色的恒星组成的圆球，这些恒星可能更古老，在圆环和球体之间是几乎完美的黑色间隙，但是这个星系到底如何形成，至今没有答案。'
'蝴蝶星云中正在死亡的恒星'
'在蝴蝶星云的中央有一颗垂死的恒星，曾经是太阳质量的五倍，他原本是一颗红巨星，由于蝴蝶星云的不断喷溅，只剩下他的核心部分，现在已接近生命的终点。'
'由一颗红巨星和一颗白矮星组成的星云, 沙漏星云'
'沙漏星云是由双星系统中一对旋转的恒星形成，这两颗恒星由一颗老化的红巨星和一颗烧毁的白矮星组成。'
'隔空对话的旋涡星系，有着彼此的陪伴不孤单'
'NGC4302（左）与NGC4298（右）'
'这两个星系类似于银河系的旋涡星系，只是以不同的角度对着地球，所以看起来才差异巨大，研究星系如何演化的科学家Raja Guhathakurta说这些东西如何形成的物理机制还不完全清楚或没有定论。'
'在浩瀚星空下，吹个泡泡做梦'
'这个年轻的大质量恒星是太阳质量的45倍，猛烈的恒星风如同吹气球一般，将气泡星云不断吹大，目前气泡星云的直径已高达7光年，并且还在继续膨胀。'
```

### KEY 不能超过 1000字，分4词提取

1. https://gitee.com/MadNug/siger/issues/I5H96I#note_11716883_link
2. https://gitee.com/MadNug/siger/issues/I5H96I#note_11716930_link
3. https://gitee.com/MadNug/siger/issues/I5H96I#note_11716972_link
4. https://gitee.com/MadNug/siger/issues/I5H96I#note_11716993_link

最后合并4次提取的关键字：

```
'别的事物的一份子'
'成千上万的美丽生物'
'别的事物'
'可赞可叹的生命'
'1.5亿公里外的太阳'
'数亿光年外的星系'
'宇宙'
'能量'
'月亮，积雨云'
'星系'
'每一个粒子'
'几十年前的今天'
'宇宙中的能量'
'份子'
'宇宙暗能量宇宙'
'那些遥远星系'
'其中的一颗星球'
'数十亿年旅行'
'生命'
'恒星'
'星团'
'银河系中年轻的星团'
'喧闹的恒星繁殖场'
'宇宙中的海底世界'
'宇宙'
'NGC2070这张照片'
'包裹着的氢气云'
'大麦哲伦星系'
'海底世界'
'宇宙礁'
'NGC2070'
'银河系'
'这个星系'
'麦哲伦星系'
'宇宙气体和尘埃'
'星系'
'大量的紫外线'
'「冥王星」'
'星系'
'星云'
'右上角的伴星系NGC5195'
'恒星'
'几乎完美的黑色间隙'
'他的邻居M81星系'
'蝴蝶星云'
'他面前旋臂的走向'
'持续不断的引力交汇'
'这个天体的外围'
'蝴蝶星云的不断喷溅'
'一颗红巨星'
'一颗烧毁的白矮星'
'有趣的对数螺旋'
'奇怪的环状星系'
'沙漏星云'
'蝴蝶星云的中央'
'一种不规则星系'
'沙漏星云沙漏星云'
'隔空对话的旋涡星系'
'气泡星云'
'气泡星云的直径'
'旋涡星系'
'猛烈的恒星风'
'银河系的旋涡星系'
'星系'
'彼此的陪伴'
'这些东西'
'RajaGuhathakurta'
'个泡泡'
'不同的角度'
'恒星'
'东西'
'质量'
'物理机制'
'陪伴'
'恒星风'
'大质量'
```

成果是：

```
{'星系': 0.8821908831596375, 
'这些东西RajaGuhathakurta个泡泡': 0.6305766105651855, 
'持续不断的引力交汇': 0.44387656450271606, 
'他面前旋臂的走向': 0.4437222480773926, 
'星系猛烈的恒星风': 0.4351731538772583, 
'银河系这个星系麦哲伦星系': 0.412289559841156, 
'RajaGuhathakurta个泡泡': 0.40790414810180664, 
'物理机制陪伴恒星风': 0.39706093072891235, 
'有趣的对数螺旋': 0.39545655250549316, 
'别的事物': 0.3946351706981659, 
'宇宙中的能量份子': 0.3883710503578186, 
'蝴蝶星云的不断喷溅': 0.3857060670852661, 
'成千上万的美丽生物': 0.3833876848220825, 
'这个天体的外围': 0.3779415488243103, 
'别的事物的一份子': 0.36451882123947144, 
'宇宙气体和尘埃星系': 0.34705400466918945, 
'持续不断的引力': 0.34465837478637695, 
'冥王星」': 0.3417441248893738, 
'一颗烧毁的白矮星': 0.34019944071769714}
```

一眼看过去，还是不知道咋用。

### ZHAIYAO 可以全文上...

```
{'和唯一知道星星为什么会发光的人一起散步': 0.9999552369117737, 
'宇宙浩瀚，岁月悠长，我始终愿意和你讲关于这个宇宙的故事。': 0.7902040481567383, 
'如今，宇宙中存在着大量的星系、恒星与行星，而且至少在其中的一颗星球上，还生存着可赞可叹的生命。': 0.5382537245750427, 
'它明明花一百五十亿年创造了自己的杰作——你。': 0.6438832879066467, 
'宇宙中的能量不会被制造出来，也不会被毁灭。': 0.7187421917915344, 
'你说你喜欢黑夜，我想是因为白天只能看到1.5亿公里外的太阳，而夜晚却可以看到数亿光年外的星系。': 0.5711492896080017, 
'你带我见过宇宙，现在，我来帮你造一片星空。': 0.8556545376777649, 
'我们与那些遥远星系息息相关，无论它们是如何与我们天各一方，那些经过数十亿年旅行到达地球的光线，最终会把我们联系在一起。': 0.2378644049167633, 
'所以为什么喜欢星辰呢，因为你就源自于他。': 0.7564247846603394}

```

### 原来还需要排序

```
{'和唯一知道星星为什么会发光的人一起散步'	0.9999552369117737, 
你带我见过宇宙，现在，我来帮你造一片星空。'	0.8556545376777649, 
宇宙浩瀚，岁月悠长，我始终愿意和你讲关于这个宇宙的故事。'	0.7902040481567383, 
所以为什么喜欢星辰呢，因为你就源自于他。'	0.7564247846603394}
宇宙中的能量不会被制造出来，也不会被毁灭。'	0.7187421917915344, 
它明明花一百五十亿年创造了自己的杰作——你。'	0.6438832879066467, 
你说你喜欢黑夜，我想是因为白天只能看到1.5亿公里外的太阳，而夜晚却可以看到数亿光年外的星系。'	0.5711492896080017, 
如今，宇宙中存在着大量的星系、恒星与行星，而且至少在其中的一颗星球上，还生存着可赞可叹的生命。'	0.5382537245750427, 
我们与那些遥远星系息息相关，无论它们是如何与我们天各一方，那些经过数十亿年旅行到达地球的光线，最终会把我们联系在一起。'	0.2378644049167633, 
```

### 浪漫诗语的浪漫指数

| 浪漫诗语 | 浪漫指数 |
|---|---|
| 和唯一知道星星为什么会发光的人一起散步 | 0.9999552369117737 |
| 你带我见过宇宙，现在，我来帮你造一片星空。 | 0.8556545376777649 |
| 宇宙浩瀚，岁月悠长，我始终愿意和你讲关于这个宇宙的故事。 | 0.7902040481567383 |
| 所以为什么喜欢星辰呢，因为你就源自于他。 | 0.7564247846603394 |
| 宇宙中的能量不会被制造出来，也不会被毁灭。 | 0.7187421917915344 |
| 它明明花一百五十亿年创造了自己的杰作——你。 | 0.6438832879066467 |
| 你说你喜欢黑夜，我想是因为白天只能看到1.5亿公里外的太阳，而夜晚却可以看到数亿光年外的星系。 | 0.5711492896080017 |
| 如今，宇宙中存在着大量的星系、恒星与行星，而且至少在其中的一颗星球上，还生存着可赞可叹的生命。 | 0.5382537245750427 |
| 我们与那些遥远星系息息相关，无论它们是如何与我们天各一方，那些经过数十亿年旅行到达地球的光线，最终会把我们联系在一起。 | 0.2378644049167633 |

这是一份别样的收获，我用它打动了原作者，来担任我们天文社团的星语星言的导师。耕耘有收获，就是最开心的了。NLP的学研才刚刚开始！

最后祝 Hanlp 越来越壮大，发展越来越好，如果 GITHUB 能正常访问，一定号召同学们去集体点 Star.

<p><img width="369px" src="https://images.gitee.com/uploads/images/2022/0720/093837_6adf7773_5631341.jpeg" title="宇宙级浪漫副本.jpg"></p>

sig/天文/[宇宙级浪漫](https://gitee.com/flame-ai/siger/blob/master/sig/%E5%A4%A9%E6%96%87/%E5%AE%87%E5%AE%99%E7%BA%A7%E6%B5%AA%E6%BC%AB.md).md

和我一起再重温一遍，宇宙级浪漫吧 :pray:

---

- NLP 调研笔记：《[自动提取关键字和摘要的 编辑器](https://gitee.com/g5pik/siger/issues/I5AYAH#note_10776084)》

  <p><img width="706px" src="https://images.gitee.com/uploads/images/2022/0607/165725_c8277277_5631341.jpeg"></p>

- https://bbs.hankcs.com/ 读书笔记：《[Tell me and I forget. Teach me and I remember. Involve me and I learn. ](https://bbs.hankcs.com/t/tell-me-and-i-forget-teach-me-and-i-remember-involve-me-and-i-learn-chinese-proverb-from-xun-zuang/4817)》

  ![输入图片说明](https://images.gitee.com/uploads/images/2022/0721/071429_40aeddd2_5631341.png "屏幕截图.png")

  感谢 何老师的签名赠语 :pray: 对同学们的鼓励！从工程实践的角度将 nlp 用好。

- SIGer 校园版：《[西浦学子故事精要](../sig/xiaoyuan/西浦学子故事精要.md)》 hanlp_restful 实录数据

  <p><img width="706px" src="https://images.gitee.com/uploads/images/2022/0721/061848_28e08683_5631341.jpeg" title="西浦nlp副本.jpg"></p>

  