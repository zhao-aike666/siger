import sys  # 导入sys模块
sys.setrecursionlimit(30000)  # 将默认的递归深度修改为3000

from bs4 import BeautifulSoup

# 7、爬取数据保存到文件
fileOb = open('sigerot22.html','r',encoding='utf-8')
s = fileOb.read()
fileOb.close()

soup = BeautifulSoup(s, 'html.parser')

# print( soup.title )

# print( soup.get_text() )

lastnum = ""

for txid in soup.find_all() :
    if txid.name == 'li' :
        #if txid.get('class') == "album__list-item js_album_item js_wx_tap_highlight wx_tap_cell" :
        titles = '[' + txid['data-title'] + '](' + txid['data-link'] + ')'
        if txid.get('data-pos_num') :
            print( txid['data-pos_num']+'.\t'+titles )
        else :
            print( lastnum + titles )
    if txid.name == 'span' :
        txt = txid.get_text()
        if txt.find( '. ' ) > 0 :
            # print( txt )
            i = int(txt[0:len(txt)-2])
            lastnum = str(i-1) + '.\t'
