import sys  # 导入sys模块
sys.setrecursionlimit(30000)  # 将默认的递归深度修改为3000

from bs4 import BeautifulSoup

# 7、爬取数据保存到文件
fileOb = open('sigerot22.html','r',encoding='utf-8')
s = fileOb.read()
fileOb.close()

soup = BeautifulSoup(s, 'html.parser')

print( soup.title )

print( soup.get_text() )

for txid in soup.find_all( class_="content-picture" ) :
    if txid.name == 'img' :
        print( "http:" + txid.get('src') )

