[![输入图片说明](https://images.gitee.com/uploads/images/2021/1122/112359_7d8f56f1_5631341.jpeg "封面副本999.jpg")](https://images.gitee.com/uploads/images/2021/1122/112344_7c70bb4a_5631341.jpeg)

The COMPUTER that made BRITAIN
- the home computer revolution of the 1980s

### 封面

英国人制造的计算机
- 家庭电脑革命在1980年代

### [目录](https://gitee.com/blesschess/luckystar/issues/I3YL4C#note_7443929_link)和[简介](https://gitee.com/blesschess/luckystar/issues/I3YL4C#note_7443946_link)

[The Computers That Made Britain (Raspberry Pi Press)](https://www.i-programmer.info/book-watch-archive/14638-the-computers-that-made-britain-raspberry-pi-press.html)
Friday, 11 June 2021
This book tells the story of the computers that would go on to inspire a generation, such as the ZX Spectrum, BBC Micro, and Commodore 64, and what happened behind the scenes during their creation. With dozens of interviews Tim Danton looks at the tales of missed deadlines, technical faults, business interference, and the unheralded geniuses behind all of it.

<ASIN:1912047853>

Author: Tim Danton
Publisher: Raspberry Pi Press
Date: May 2021
Pages: 300
ISBN: 978-1912047857
Print: 1912047853
Audience: General
Level: Introductory
Category: [History](https://www.i-programmer.info/bookreviews/24-history.html)

[![输入图片说明](https://images.gitee.com/uploads/images/2021/1118/002838_28c47305_5631341.png "屏幕截图.png")](https://images.gitee.com/uploads/images/2021/1119/175240_90dd0a25_5631341.png)

The full list covered is:

1. Acorn Archimedes
1. Acorn Electron
1. Apple II
1. Apple Macintosh
1. Amstrad CPC 464
1. Amstrad PCW 8256
1. Atari 520ST
1. BBC Micro
1. Commodore 64
1. Commodore Amiga
1. Commodore PET 2001
1. Commodore VIC-20
1. Dragon 32
1. IBM Personal Computer (5150)
1. Research Machines 380Z
1. Sinclair QL
1. Sinclair ZX80 
1. Sinclair ZX81
1. Sinclair ZX Spectrum

-  **介绍** P6-7

由树莓派贸易公司，2021年首次出版，
于 Maurice Wilkes Building, St. John’s Innovation Park, Cowley Road, Cambridge, CB4 0DS


| 出版总监 | 编辑 |
|---|---|
| 拉塞尔·巴恩斯 | 菲尔·金，西蒙·布鲁 |
|  **编辑助理**  |  **设计**  |
| 尼古拉·金 | Critical Media |
|  **插图**  |  **首席执行官**  |
| Sam Alder 和 Brian O Halloran | 埃本·厄普顿 |

 **ISBN**  978-1-912047-90-1	

这本书中提及或广告的有关商品，产品或服务的任何遗漏或错误，出版商，和贡献者不承担任何责任。除非另有说明，本书的内容按照 Creative Commons attribution - noncommercial - alike 3.0 (CC BY-NC-SA 3.0) 进行授权。

 **目录** 

- 介绍. . . . . . . . . . . . . . . . . . . . . . . . . . . . .6

1. Research Machines (研究机器) 380z. . . . . . . . . . . . . . . . . . . . . .8 【[初译](https://gitee.com/blesschess/luckystar/issues/I4IYBI/#note_7480120_link)】[图](https://images.gitee.com/uploads/images/2021/1120/155241_54d4c4be_5631341.png)
1. Commodore (指挥官) PET 2001. . . . . . . . . . . . . . . . . . . . . . .18 【[初译](https://gitee.com/blesschess/luckystar/issues/I4IYBI#note_7481869_link)】[图](https://images.gitee.com/uploads/images/2021/1120/182103_8fc200cd_5631341.png)
1. Apple II (苹果二代) . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .36 【[初译](https://gitee.com/blesschess/luckystar/issues/I4IYBI#note_7487049_link)】[图](https://images.gitee.com/uploads/images/2021/1122/005250_41655134_5631341.png)
1. Sinclair (辛克莱) ZX80 和 ZX81. . . . . . . . . . . . . . . . . . . . . .46
1. Commodore (指挥官) VIC-20 . . . . . . . . . . . . . . . . . . . . . . . .60
1. IBM Personal Computer (个人电脑) (5150). . . . . . . . . . . . . . . . . . .78
1. BBC (英国广播公司) Micro . . . . . . . . . . . . . . . . . . . . . . . . . . . . .90
1. Sinclair (辛克莱) ZX Spectrum. . . . . . . . . . . . . . . . . . . . . . .114
1. Dragon (龙) 32 . . . . . . . . . . . . . . . . . . . . . . . . . . . . .138
1. Commodore (指挥官) 64. . . . . . . . . . . . . . . . . . . . . . . . . .150
1. Acorn Electron (电子). . . . . . . . . . . . . . . . . . . . . . . . . . .166
1. Apple (苹果) Macintosh. . . . . . . . . . . . . . . . . . . . . . . . . .176
1. Amstrad CPC 464. . . . . . . . . . . . . . . . . . . . . . . . .194
1. Sinclair (辛克莱) QL . . . . . . . . . . . . . . . . . . . . . . . . . . . . .210
1. Atari (雅达利) 520ST . . . . . . . . . . . . . . . . . . . . . . . . . . . .222
1. Commodore (指挥官) Amiga . . . . . . . . . . . . . . . . . . . . . . . .234
1. Amstrad (阿姆斯特拉德) PCW 8256. . . . . . . . . . . . . . . . . . . . . . . .256
1. Acorn Archimedes (阿基米德) . . . . . . . . . . . . . . . . . . . . . . . . .268

- 尾声:英国的PC到底发生了什么?. . . . . . . . .280 【[初译](https://gitee.com/blesschess/luckystar/issues/I4IYBI#note_7471318_link)】
- 鸣谢. . . . . . . . . . . . . . . . . . . . . . . .281 【[初译](https://gitee.com/blesschess/luckystar/issues/I4IYBI#note_7473030_link)】
- 进一步阅读，进一步查看，和论坛. . . . . . . . . . .283 【[初译](https://gitee.com/blesschess/luckystar/issues/I4IYBI#note_7474218_link)】
- 索引  . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .286 【[初译](https://gitee.com/blesschess/luckystar/issues/I4IYBI#note_7476087_link)】
  [A B C D E F](https://gitee.com/blesschess/luckystar/issues/I4IYBI#note_7476087_link) | [G H I](https://gitee.com/blesschess/luckystar/issues/I4IYBI#note_7477047_link) | [J K L M O](https://gitee.com/blesschess/luckystar/issues/I4IYBI#note_7477203_link) | [P Q R](https://gitee.com/blesschess/luckystar/issues/I4IYBI#note_7477583_link) | [S T U V W](https://gitee.com/blesschess/luckystar/issues/I4IYBI#note_7477929_link) | [X Y Z](https://gitee.com/blesschess/luckystar/issues/I4IYBI#note_7477938_link) 

本章是按照每台电脑在英国的呈现顺序排列的，就像每个型号在《个人电脑世界》(Personal Computer World) 杂志上的评论日期所反映的那样。

- 【[封面](https://gitee.com/blesschess/luckystar/issues/I4IYBI#note_7474046_link)】【[封底](https://gitee.com/blesschess/luckystar/issues/I4IYBI#note_7473802_link)】：初译

 **介绍** 

　　毫无疑问，20世纪80年代是有史以来最好的十年。不仅仅是因为它给了我们 Duran Duran 和 E.T., 甚至不是因为 Sony Walkman(索尼随身听)。那是因为在20世纪80年代，个人电脑开始兴起。

　　在政府大幅折扣的帮助下，计算机进入了全国各地的中小学，尽管教师并不总是知道如何使用它们。数百万台电脑出现在全国各地的客厅和卧室里。这一次，英国走在了世界的前面，帮助创造了英国程序员的黄金一代。当然，我们中更多的人注定要花时间玩《Elite》和《Chuckie Egg》，而不是自己创造游戏，但 C64s, Spectrum 48Ks 和 BBC Micros 的结合直接催生了英国蓬勃发展的软件产业，并一直延续到今天。

　　然而，激发这本书灵感的问题是，这些计算机是如何产生的? 没有像 cookie-cutter (饼干模具) 样可以效仿的模版。公司真的是在随风而去编造事情，而且往往效果很好。你在这里读到的每一台电脑都有一个令人惊讶的故事，而这几乎总是与涉及的人有关。这就是为什么，这本书既是一个关于每台电脑的创造的故事，也是一个关于创造它们的人的故事。很多是英国人，很多是天才。

　　在这个新兴行业中，数十亿英镑的财富有待争夺，并不是每个人都能做到最好。在追求名利的过程中，自我与自我斗争，导致了背叛、失去了财富和无数破碎的梦想。想想 J.R. 和 Dallas (达拉斯)，不过用的是硅而不是石油。还有英格兰的沼泽，而不是德克萨斯晒黑的平原。

　　这本书讲述了19台计算机的故事，每一台都对英国产生了影响。如果你最喜欢的电脑不见了，我很抱歉——我很想覆盖 Apricot(杏子)'s machines(的机器)，比如：NewBrain 、 Oric-1 、 Jupiter Ace 和 Cambridge (剑桥) Z88，但这些都得等轮到他们了。

　　虽然我完全认为人们会直接跳到他们拥有的电脑上，但这本书的阅读顺序是任意的。你可以选择开始你的旅程，从 20世纪70年代末的 Commodore PET 到 1987年 的 Acorn Archimedes 结束。你还可以追溯到 Amstrad 令人震惊的崛起，它最终吞并了 Sinclair (辛克莱) 的 Spectrum 业务。这完全取决于你。

　　我最后要说的是准确性。这些电脑是两代人以前制造的，这给了谣言和半真半假的说法足够的时间来获得尊重。只要有可能，我都会直奔源头:这意味着听口述历史，阅读采访，钻研历史文件，参考以前写的书。但最重要的是，只要有可能，我都会和相关人员交谈。

　　结果是尽可能接近我所能得到的“真相”，如果这意味着消灭那些没有现实根源的神话，那就最好了。

　　公平地说，这些故事不需要夸张就能让它们引人入胜。我希望你喜欢阅读它们。

 **Tim Danton**  蒂姆·丹顿
