- [OpenFPGA](https://space.bilibili.com/1551161745)：[简谈FPGA比特流结构](https://blog.csdn.net/Pieces_thinking/article/details/119792644)
- [在FPGA上完美复刻Windows 95](https://www.bilibili.com/video/BV1944y1p7BH/)
- [ao486](https://gitee.com/shiliupi/ao486) :: [OpenCores](https://opencores.org/projects/ao486) | [DE10-Nano](https://www.cnblogs.com/DoreenLiu/p/14841667.html)
- [使用FPGA和MiSTer进行复古计算](https://blog.csdn.net/cumo7370/article/details/107410131)
- [开源FPGA硬件模拟游戏机](https://www.qbitai.com/2021/04/22927.html) | [MiSTer FPGA](https://www.bilibili.com/video/BV1T44y1G7fJ)

<p><img width="706px" src="https://foruda.gitee.com/images/1675311184838122808/e8f0ab72_5631341.jpeg" title="519openfpga副本.jpg"></p>

> 本期专题完整记录了一个探宝的过程，缘起自一个A-Z80的复古项目，为了验证 Hello,world 字串是否加载到了 bitstream 中，找到了 OpenFPGA 的一篇比特流文件格式的文章，这位自称碎碎念的博主竟然是去年 IC技术圈推荐的一位优秀博主，找到了他的 B站大本营，牵出啦 MiSTer 复古计算。这就叫殊途同归啦。让我们一起玩耍起来吧 :+1: 

`ram.v` [笔记](https://gitee.com/shiliupi/A-Z80/issues/I6B7IJ#note_15915374_link)

```
	wire [15:0] doa;

	wire [15:0] dia;
	wire [10:0] addra;
	wire wea;
	wire cea;
	wire clka;

	ram ram_(
		.doa(doa), 
		.dia(dia), 
		.addra(addra), 
		.cea(cea), 
		.clka(clka), 
		.wea(wea)
	);
```

- 为什么在 BITSTREAM 文件里 找不到 HELLO ASM 中的字符串“明码” ？

  bing `fpga 的 bitstream 是怎么生成的？`

# [【Vivado那些事】简谈FPGA比特流结构](https://blog.csdn.net/Pieces_thinking/article/details/119792644)

### Xilinx的BITGEN实用程序

BITGEN是Xilinx的实用程序，利用本地电路描述(NCD)格式的布局布线后文件，创建用于FPGA配置的比特流。BITGEN是一个高度可配置的工具，具有100多个命令行选项(在命令行工具用户指南（Xilinx Command Line Tools User Guide，http://www.xilinx.com/support/documentation/sw_manuals/xilinxl 2_2/devref.pdf）中描述）。有些选项用于确定比特流输出格式、启用压缩处理减少比特流大小、提高FPGA配置速度、使用CRC来确保数据完整性、对比特流加密等。

 **示例** 

以下示例用于根据差异部分配置的短比特流， 通过脚本语言描述比特流命令。


```
#!/devl/perl/bin/perl
 
use Switch;
 
# -----------------------------------------------------------------------------
#  Copyright (C) 2011 OutputLogic.com 
#  This source file may be used and distributed without restriction 
#  provided that this copyright statement is not removed from the file 
#  and that any derivative work contains the original copyright notice 
#  and the associated disclaimer. 
#  
#  THIS SOURCE FILE IS PROVIDED "AS IS" AND WITHOUT ANY EXPRESS 
#  OR IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED 
#  WARRANTIES OF MERCHANTIBILITY AND FITNESS FOR A PARTICULAR PURPOSE. 
# -----------------------------------------------------------------------------
#
# A script to parse Xilinx FPGA bitstream in .RBT format
#
if ($#ARGV < 0)
{
    die "Usage: xilinx_bitstream_parser <rbt file>\n";
}
 
$verbose = 2;
 
open(RBT_FILE,"<$ARGV[0]")          || die "Error: can't open $ARGV[0] for input\n";
 
# Bus OP codes
$opcode{"00"} = "no op";
$opcode{"01"} = "read";
$opcode{"10"} = "write";
$opcode{"11"} = "decrypt";
 
# Regster Addresses
$reg{"00000"} = "CRC ";
$reg{"00001"} = "FAR ";
$reg{"00010"} = "FDRI";
$reg{"00011"} = "FDRO";
$reg{"00100"} = "CMD ";
$reg{"00101"} = "CTL ";
$reg{"00110"} = "MASK";
$reg{"00111"} = "STAT";
$reg{"01000"} = "LOUT";
$reg{"01001"} = "COR ";
$reg{"01010"} = "MFWR";
$reg{"01011"} = "CBC ";
$reg{"01100"} = "ID  ";
$reg{"01101"} = "AXSS";
$reg{"01110"} = "COR1";
$reg{"01111"} = "CSOB";
$reg{"10000"} = "WBSTAR";
$reg{"10001"} = "TIMER";
$reg{"10010"} = "RBCRC0";
$reg{"10011"} = "RBCRC1";
$reg{"10100"} = "RBCRC2";
$reg{"10101"} = "EFAR";
$reg{"10110"} = "BOOTSTS";
$reg{"10111"} = "TESTMODE";
$reg{"11000"} = "CTL1";
 
 
# Configuration Commands
$command{"00000"} = "NULL";
$command{"00001"} = "WCFG";
$command{"00010"} = "MFW";
$command{"00011"} = "LFRM";
$command{"00100"} = "RCFG";
$command{"00101"} = "START";
$command{"00110"} = "RCAP";
$command{"00111"} = "RCRC";
$command{"01000"} = "AGHIGH";
$command{"01001"} = "SWITCH";
$command{"01010"} = "GRESTORE";
$command{"01011"} = "SHUTDOWN";
$command{"01100"} = "GCAPTURE";
$command{"01101"} = "DESYNCH";
$command{"01110"} = "DRTEST";
$command{"01111"} = "IPROG";
$command{"10000"} = "CRCC";
 
$fa = -1;
$last_fa = -1;
$in_LOUT = 0;
@LOUT_wordcnt = ();
 
while (<RBT_FILE>)
{
   $type = "";
   $op   = "";
   $reg  = "";
   chop;
   next if (! /^[01]/);   
   
   if (/^10101010100110010101010101100110/) {
       $type = "Sync word";
   }
   elsif (/^11111111111111111111111111111111/) {
       $type = "Dummy word";
   }
   elsif (/^00000000000000000000000010111011/) {
       $type = "Bus Width word ";
   }
   elsif (/^00010001001000100000000001000100/) {
       $type = "8/16/32 BusWidth";
   }
   elsif (/^001/) {
       $type = "Type 1";
   }
   elsif (/^010/) {
       $type = "Type 2";
   }
   elsif (/^00000000000000000000000000000000/) {
       $type = "NO OP";
       $wordcnt = 0;
   }
   else {
       if (($register eq "FDRI") || ($register eq "FDRO") || ($last_command eq "LFRM")) {
          $type = "Partial CRC word";
       }else{
          $type = "Type Unknown";
       }
   }
   
   if ($type eq "Type 1" || $type eq "Type 2")    {
      $s = substr($_,3,2);
      if ($opcode{$s} ne "") 
      {
          $op = $opcode{$s};
      }
      else 
      {
          $op = "Unknown";
      }
   }
   
   if ($type eq "Type 1") {
      $s = substr($_,14,5);
      if ($reg{$s} ne "") {
       $register = $reg{$s};
      }
      else {
          $register = "Unknown";
      }
      $wordcnt = &bin2dec(substr($_,21,11));
   }
   
   if ($type eq "Type 2"){
      $wordcnt = &bin2dec(substr($_,5,27));
   }
   
   $text = "$type";
   if ($op eq "read") 
   {
      $text .= " $op " . "$wordcnt words from " . "$register";
   }
   elsif (($op eq "write") || ($op eq "decrypt"))
   {
      $text .= " $op " . "$wordcnt words to " . "$register";
   }
   elsif ($op eq "no op") 
   {
      $text .= " NO OP";
   }
   printf ("%10s %s\n", &bin2hex($_), $text);
 
 
   # A little hack so we can see the next stream also
   if (($register eq "LOUT") && ($wordcnt > 1)) 
   {
      $in_LOUT++;
      push @LOUT_wordcnt, $wordcnt;
      $wordcnt = 0;
   }
    
    
    # start printing out everything
    for ($i=0; $i<$wordcnt && (($op eq "write") || ($op eq "decrypt") || ($op eq "read")) && ($_ = <RBT_FILE>); $i++) 
    {
        chop;
 
        if (($op eq "write") && ($register eq "FAR "))
        {
         $fa = $_;
        }
        
        if (($register eq "FDRI") || ($register eq "FDRO")) 
        {
            # printf ("%s %10s\n", "data word " . $i, $_ );
 
            # don't print all frame words
            
            if ($i == 0) 
            {
             printf ("%s %d...%d \n", "data words " , $i , $wordcnt-1);
            }
       }
       elsif ($register eq "LOUT") 
       {
          $this_fa = &bin2dec($_);
          $BlkType = &bin2dec(substr($_,8,3));
          $TopBot  = (&bin2dec(substr($_,11,1)) == 0) ? "Top" : "Bot";
          $MajRow  = &bin2dec(substr($_,12,5));
          $MajCol  = &bin2dec(substr($_,17,8));
          $MinCol  = &bin2dec(substr($_,25,7));
        
           printf ("%10s %s [Block %d %s Row %d Col %d Minor %d] \n", &bin2hex($_) , "Frame Address", $BlkType, $TopBot, $MajRow, $MajCol, $MinCol);        
                
                
          if ($this_fa < $last_fa) {
             printf ("ERROR: Frame Address going down (%d < %d)?\n", $this_fa, $last_fa);
          }
          $last_fa = $this_fa;
       }
        elsif ($register eq "FAR ") 
        {
            $BlkType = &bin2dec(substr($_,8,3));
            $TopBot  = (&bin2dec(substr($_,11,1)) == 0) ? "Top" : "Bot";
            $MajRow  = &bin2dec(substr($_,12,5));
            $MajCol  = &bin2dec(substr($_,17,8));
            $MinCol  = &bin2dec(substr($_,25,7));
           
            printf ("%10s %s [Block %d %s Row %d Col %d Minor %d] \n", &bin2hex($_) , $register . " data word " . $i, $BlkType, $TopBot, $MajRow, $MajCol, $MinCol);            
            
        }
        elsif ($register ne "CMD ") 
        {
            printf ("%10s %s\n", &bin2hex($_) , $register . " data word " . $i);
            
        } 
        else 
        {
            $s = substr($_,27,5);
         
            if ($command{$s} ne "") 
            {
                $last_command = $command{$s};
             printf ("%10s %s\n", &bin2hex($_), $command{$s} . " command",  );
         }
            else
            {
             printf ("%10s %s\n", &bin2hex($_), "Unknown command" );
         }
        }
    }
} # while
close(RBT_FILE);
sub bin2dec
{
    local($num) = @_;
    $retval = 0;
    while ($num ne "")
    {
        $retval = $retval * 2;
        if (substr($num,0, 1) eq "1") 
        {
            $retval++;
     }
        $num = substr($num,1);
    }
    return $retval;
}
# convert 32-digit bin number to hex
sub bin2hex
{
    local($num) = @_;
    my $str_hex = "";
    
    for ($i=0; $i<8; $i++) 
    {
        # left to right
        my $chunk = substr($num, 4 * $i ,4); 
        my $retval = "0";
        switch ($chunk) 
        {
         case "0000" { $retval = "0" }
         case "0001" { $retval = "1" }        
         case "0010" { $retval = "2" }
         case "0011" { $retval = "3" }        
         case "0100" { $retval = "4" }
         case "0101" { $retval = "5" }        
         case "0110" { $retval = "6" }
         case "0111" { $retval = "7" }        
         case "1000" { $retval = "8" }
         case "1001" { $retval = "9" }        
         case "1010" { $retval = "A" }
         case "1011" { $retval = "B" }        
         case "1100" { $retval = "C" }
         case "1101" { $retval = "D" }        
         case "1110" { $retval = "E" }
         case "1111" { $retval = "F" }        
        }
      #  print $chunk. " : " . $retval . "\n";
        $str_hex .= $retval;
    }
    
  #  print $str_hex;    
    return $str_hex;
}
```

关于脚本的使用，可以查看《[Verilog数字系统基础设计-CRC](http://mp.weixin.qq.com/s?__biz=Mzg4ODA5NzM1Nw%3D%3D)》。

仔细观察比特流，能区分出同步和解同步命令、属于Virtex-6 LX240T FPGA的IDCODE，以及两个405个和243个字的帧。

上诉脚本及比特流文件如下：

** 链接：https://pan.baidu.com/s/1nXknXYGbiDGx6e4YOb3zrg 提取码：open  **

### 参考资料

Xilinx UG360

Xilinx Command Line Tools User Guide

![输入图片说明](https://foruda.gitee.com/images/1674963994197230669/7690dd67_5631341.png "屏幕截图")

 **推荐阅读** 

![输入图片说明](https://foruda.gitee.com/images/1674965843937484416/eb46ab6b_5631341.png "屏幕截图")

- 微信公众号：OpenFPGA

  > FPGA新闻、技术、设计思想 ，FPGA深度学习

1. [【Vivado那些事】如何查找官网例程及如何使用官网例程 ](http://mp.weixin.qq.com/s?__biz=Mzg4ODA5NzM1Nw%3D%3D&amp;chksm=cf810c0cf8f6851a6f0aedd45ab75ac2884142cc3a96f0838a548b5e6fdecaec367ebc096fbd&amp;idx=1&amp;mid=2247488302&amp;scene=21&amp;sn=4150ed28a939971134cfd2788bcd75fc)
2. [【Vivado使用误区与进阶】总结篇 ](http://mp.weixin.qq.com/s?__biz=Mzg4ODA5NzM1Nw%3D%3D&amp;chksm=cf810496f8f68d80bd7b5e14ae9922c3262d2c8c633dffe6d89293398d3ca0e505cfd0cfd646&amp;idx=1&amp;mid=2247490484&amp;scene=21&amp;sn=77dbdec9db195c656eede1c73a25b1a3)
3. [【Vivado那些事】Vivado中常用的快捷键（二）其他常用快捷键 ](http://mp.weixin.qq.com/s?__biz=Mzg4ODA5NzM1Nw%3D%3D&amp;chksm=cf810338f8f68a2e5dee390ae292c5a4ee813fe6b7e850250003ed79f65bf7a4d69e6ab5f6a0&amp;idx=1&amp;mid=2247490586&amp;scene=21&amp;sn=95fa0c94748dbe3e06e874c5bafa1928)
4. [SystemVerilog数字系统设计_夏宇闻 PDF ](http://mp.weixin.qq.com/s?__biz=Mzg4ODA5NzM1Nw%3D%3D&amp;chksm=cf810319f8f68a0f3429410c0e76d9e4da26f5bd359d2386c86b1e2329b9fc1a8ec148e1317c&amp;idx=1&amp;mid=2247490619&amp;scene=21&amp;sn=e3d7cb164e4534753b87e31859ae525a)
5. [图书推荐|ARM Cortex-M0 全可编程SoC原理及实现 ](http://mp.weixin.qq.com/s?__biz=Mzg4ODA5NzM1Nw%3D%3D&amp;chksm=cf82fa63f8f57375ae0b83c28eb8afb47cad4acd4388a0241b58f593cc0a184a1e2d93d9ccc3&amp;idx=1&amp;mid=2247492929&amp;scene=21&amp;sn=bfe4075373850b4c11f06667edf541a3)
6. [1202年了，还在使用虚拟机吗？Win10安装Ubuntu子系统及图形化界面详细教程 ](http://mp.weixin.qq.com/s?__biz=Mzg4ODA5NzM1Nw%3D%3D&amp;chksm=cf82ec02f8f56514e53d1f1b7e51198f4bd5cde3633f855a253ec3547a79be55b727116bc463&amp;idx=1&amp;mid=2247496480&amp;scene=21&amp;sn=5b7c7a436876ffe601c550124b0bcd73)
7. [Github 上有哪些优秀的 VHDL/Verilog/FPGA 项目 ](http://mp.weixin.qq.com/s?__biz=Mzg4ODA5NzM1Nw%3D%3D&amp;chksm=cf82eb51f8f5624794b37889e1e7c4efb9f693a4a139e5a56d6feedda4bf7e45baf68893e2fc&amp;idx=1&amp;mid=2247496819&amp;scene=21&amp;sn=653f6b3407fbf6d81e7cf782064457a8)
8. [AD936x+ZYNQ搭建OpenWIFI ](http://mp.weixin.qq.com/s?__biz=Mzg4ODA5NzM1Nw%3D%3D&amp;chksm=cf82e97cf8f5606aef3fee16597f7490abce63ab7537cb62a3a9802c5309fe88b4280c0e6f8b&amp;idx=1&amp;mid=2247497310&amp;scene=21&amp;sn=aec12b961218f3305861630b25efe05b)
9. [面试中经常会遇到的FPGA基本概念，你会几个？ ](http://mp.weixin.qq.com/s?__biz=Mzg4ODA5NzM1Nw%3D%3D&amp;chksm=cf82e698f8f56f8e2a912bd40912564b44eb1074cab6717e93b5fbc52da964181bf876eaa11c&amp;idx=1&amp;mid=2247498170&amp;scene=21&amp;sn=ae7eacf294c2b7bb096f4c5b13b87977)
10. [Xilinx FPGA MIPI 接口简单说明 ](http://mp.weixin.qq.com/s?__biz=Mzg4ODA5NzM1Nw%3D%3D&amp;chksm=cf82e6d0f8f56fc6d99174b89573923ae1d5d0eb59b4d22fb90e74e04ec010c71e27a8c3a768&amp;idx=1&amp;mid=2247498226&amp;scene=21&amp;sn=84859ab879480194fc6aeb046ce3dcce)
11. [介绍一些新手入门FPGA的优秀网站 ](http://mp.weixin.qq.com/s?__biz=Mzg4ODA5NzM1Nw%3D%3D&amp;chksm=cf82e568f8f56c7efb82361217ae6fe99ad5183c4c74bc7cd4ce6c0bf6f9827e5b4a345bc60d&amp;idx=1&amp;mid=2247498314&amp;scene=21&amp;sn=9ef5021f2a805b08d13e9d1aa37eba9c)
12. [Vivado ML（机器学习） 2021尝鲜 ](http://mp.weixin.qq.com/s?__biz=Mzg4ODA5NzM1Nw%3D%3D&amp;chksm=cf82e5edf8f56cfbc7e104691a7d8b00d634e530985569fd3d2e0d538df58fa9cc75f3c8e1c5&amp;idx=2&amp;mid=2247498447&amp;scene=21&amp;sn=e72a8f3e3f3b96422ee72d43e86e2b4c)
13. [推荐一些可以获取免费的国外的原版书籍（电子版）网站 ](http://mp.weixin.qq.com/s?__biz=Mzg4ODA5NzM1Nw%3D%3D&amp;chksm=cf82e413f8f56d05bbf2aa1381c8f618707b9a8dd133e4e203e3a87633440950e554ce5c18d6&amp;idx=1&amp;mid=2247498545&amp;scene=21&amp;sn=053f69f5d4b028468d059a54b04b45ca)
14. [FPGA 的重构 ](http://mp.weixin.qq.com/s?__biz=Mzg4ODA5NzM1Nw%3D%3D&amp;chksm=cf82e477f8f56d61570eecf2dcf6d86fc43bda3c9fdfe31cf7b3913bbcd96d0c0975de41007e&amp;idx=1&amp;mid=2247498581&amp;scene=21&amp;sn=2c25946507503ddd3fb522ed1a577ace)
15. [浅析FPGA局部动态可重构技术 ](http://mp.weixin.qq.com/s?__biz=Mzg4ODA5NzM1Nw%3D%3D&amp;chksm=cf82e450f8f56d46b0158746556ab9e1030c71bd916b86134400aa7d4e43596bfd761ef4b377&amp;idx=1&amp;mid=2247498610&amp;scene=21&amp;sn=8d1dc664e817cee19accfccf63b78ff6)
16. [ISP（图像信号处理）算法概述、工作原理、架构、处理流程 ](http://mp.weixin.qq.com/s?__biz=Mzg4ODA5NzM1Nw%3D%3D&amp;chksm=cf82e488f8f56d9e06b832b55a43edb118b14778764ba4723bca6c6a8d527f206549e4488a59&amp;idx=1&amp;mid=2247498666&amp;scene=21&amp;sn=eefa9fbd271b36e8f5d521cb8fca2cff)
17. [国产CPU概括 ](http://mp.weixin.qq.com/s?__biz=Mzg4ODA5NzM1Nw%3D%3D&amp;chksm=cf82e4eaf8f56dfc2187262a734af2ba5bfeeb508f3942bb123f5076a06381ac2b05ee447011&amp;idx=1&amp;mid=2247498696&amp;scene=21&amp;sn=3989731d7fb986611b873727f87c96bc)
18. [从电子游戏历史看IC发展的助推剂 ](http://mp.weixin.qq.com/s?__biz=Mzg4ODA5NzM1Nw%3D%3D&amp;chksm=cf82e20cf8f56b1ac4d2636f78c0ecbd1bc3c5e51ed5efa8fed088d94a161272bd1a12ddf0dc&amp;idx=1&amp;mid=2247499054&amp;scene=21&amp;sn=6453fff3fd52c14de37086092865ca90)
19. [80年代电子游戏及电脑游戏的发展历史 ](http://mp.weixin.qq.com/s?__biz=Mzg4ODA5NzM1Nw%3D%3D&amp;chksm=cf82e2f9f8f56bef5cba564dbdf01633473508bd9b5a1f47ddb753977ecb6ceb3331ca55f465&amp;idx=1&amp;mid=2247499227&amp;scene=21&amp;sn=6a7c68a429e0bb10d322368b42ad3519)
20. [PCIe总线的基础知识 ](http://mp.weixin.qq.com/s?__biz=Mzg4ODA5NzM1Nw%3D%3D&amp;chksm=cf82e2c8f8f56bde3c114cb966b70e6de12ac89beab660058cb0521d34c2041cecd6b6459616&amp;idx=1&amp;mid=2247499242&amp;scene=21&amp;sn=94da13557b02e5b0ab28eec080e1217e)
21. [万字长文带你回顾电子游戏的七十多年历史（完整版） ](http://mp.weixin.qq.com/s?__biz=Mzg4ODA5NzM1Nw%3D%3D&amp;chksm=cf82e10af8f5681c0c4e744acdf7937ccd4c33771c138333f69a11ec8505249e36e70f59a0e6&amp;idx=1&amp;mid=2247499304&amp;scene=21&amp;sn=237464dd2efbac2fee7ee517e757d9ab)
22. [FPGA中异步复位，同步释放的理解 ](http://mp.weixin.qq.com/s?__biz=Mzg4ODA5NzM1Nw%3D%3D&amp;chksm=cf82e141f8f5685740cc915425cb9b560d66e1428b7ab4a49909cd4f40cf2a4f5875383128c9&amp;idx=1&amp;mid=2247499363&amp;scene=21&amp;sn=4e0e4b770c15f7ec836dd45841feebda)
23. [OpenFPGA系列文章总结 ](http://mp.weixin.qq.com/s?__biz=Mzg4ODA5NzM1Nw%3D%3D&amp;chksm=cf82e1b8f8f568ae7fbf427d4de34993c3c0eb79c17cd74b72afcbd8fdba8d4921af43f78609&amp;idx=1&amp;mid=2247499418&amp;scene=21&amp;sn=0302862b61ee9d4210ea5f8bc823649a)
24. [用Verilog设计一个16 位 RISC 处理器 ](http://mp.weixin.qq.com/s?__biz=Mzg4ODA5NzM1Nw%3D%3D&amp;chksm=cf82e186f8f5689084b5335485659c76633aa3c0640a9528faeb2d64f8a61bfc03d26ed8c17c&amp;idx=1&amp;mid=2247499428&amp;scene=21&amp;sn=01154bf313d5981a71ff4bf5a310529f)
25. [介绍一些新手入门FPGA的优秀网站（新增） ](http://mp.weixin.qq.com/s?__biz=Mzg4ODA5NzM1Nw%3D%3D&amp;chksm=cf82e1c2f8f568d496e51b108efbd1feb09f0eedb193cce90139d966e79dbe5b18980afb6270&amp;idx=1&amp;mid=2247499488&amp;scene=21&amp;sn=fcdc0fcb9eaf3841cd50683747e62db3)
26. [如何提高FPGA的工作频率 ](http://mp.weixin.qq.com/s?__biz=Mzg4ODA5NzM1Nw%3D%3D&amp;chksm=cf82e1dff8f568c924a6683e2a95e8ea72596a00f0ba698b9f4b5214d13aa949285ccb5142db&amp;idx=1&amp;mid=2247499517&amp;scene=21&amp;sn=a9720f31e7d35dbbe66eeb88f051c193)
27. [【Verilog】深入理解阻塞和非阻塞赋值的不同 ](http://mp.weixin.qq.com/s?__biz=Mzg4ODA5NzM1Nw%3D%3D&amp;chksm=cf82e001f8f56917aac412dc347b5fa2339f05cea2195e6d7c5e6bb118e15b8a91d366ff8c40&amp;idx=1&amp;mid=2247499555&amp;scene=21&amp;sn=0d1a85a442a7a700d52205036f12afb3)
28. [简谈 Xilinx FPGA 原理及结构 ](http://mp.weixin.qq.com/s?__biz=Mzg4ODA5NzM1Nw%3D%3D&amp;chksm=cf82e050f8f569465c1730f46b98100bfb5ecb1f3f632395851dba42ed96e4e3895a19e1198b&amp;idx=1&amp;mid=2247499634&amp;scene=21&amp;sn=7802d277f7f992181661f037708f9861)
29. [【官方推荐】学习Zynq-7000的入门书单 ](http://mp.weixin.qq.com/s?__biz=Mzg4ODA5NzM1Nw%3D%3D&amp;chksm=cf82e05af8f5694cd98f56b73379f9d7f580985c7744733a8eedc7ea4b3d7ed49401d227cebf&amp;idx=1&amp;mid=2247499640&amp;scene=21&amp;sn=30dd42d2fd707292de8473c5b503398b)
30. [【Vivado那些事】Xilinx 7系列时钟结构详解 ](http://mp.weixin.qq.com/s?__biz=Mzg4ODA5NzM1Nw%3D%3D&amp;chksm=cf82e0a4f8f569b2a26ac48890e0ab3ad1ecdbf0adb89964b1868187d6d0e05f614c68da61e8&amp;idx=1&amp;mid=2247499654&amp;scene=21&amp;sn=b1fcb2d3af7cb991603d415b6944c28d)
31. [Verilog数字系统基础设计-CRC ](http://mp.weixin.qq.com/s?__biz=Mzg4ODA5NzM1Nw%3D%3D&amp;chksm=cf82e081f8f56997715e235b8a61244368bcac5221f5196de5aaf6611fad3277dee3e156b17b&amp;idx=1&amp;mid=2247499683&amp;scene=21&amp;sn=308d960d1eb77e31126a6eb8ed1fe677)


# [干货：Vivado 直接修改RAM初始化文件，避免重新综合、实现的方法](https://cloud.tencent.com/developer/article/1529565)

发布于2019-10-29 17:52:25 阅读 2.6K0

### 1、引言

以交换机设计为例。在交换机设计前期，转发表项是固化在交换机内部的（给FPGA片内BRAM初始值），但是在测试过程中，往往需要对表项进行修改，如果直接修改BRAM的coe文件，则需要重新综合、实现、生成bit文件，其中，综合与实现耗时十分严重，设计规模越大，消耗的时间越长，而生成bit文件消耗的时间则相对固定。针对上述问题，本文探究一种避免综合与实现，直接修改BRAM初始化值的方法，可以避免综合、实现两个步骤，修改BRAM初始值后，直接生成bit文件，可节约大量的时间。

### 2、 操作方法

1. 完成初始设计，综合、实现（但是不生成bit文件） ；
2. 打开布线后的dcp文件（使用Vivado直接打开），如下图所示:

   ![输入图片说明](https://foruda.gitee.com/images/1674969079618550942/36bbf4e6_5631341.png "屏幕截图")

3. 找到想修改的目标BRAM：

   1. ctrl+f ；

      ![输入图片说明](https://foruda.gitee.com/images/1674969134303519041/0e7a5aae_5631341.png "屏幕截图")

   2. 选择需要修改的BRAM ；

      ![输入图片说明](https://foruda.gitee.com/images/1674969149313530378/1a2ef50d_5631341.png "屏幕截图")

   3. 在properties界面，可以直接修改初始化文件（需要熟悉原语，其初始化方式与RAMB18E1相同）；

      ![输入图片说明](https://foruda.gitee.com/images/1674969165787039247/101bd85f_5631341.png "屏幕截图")

   4. 保存，退出；
   5. 在主工程中打开实现设计，按照相同的方式可以找到对应的RAM与初始化值，可以发现初始化值已经改变了 ；
   6. 生成bitstream文件。

### 3、 上板实测

1. coe文件：

   ![输入图片说明](https://foruda.gitee.com/images/1674969213244868000/d1fc842c_5631341.png "屏幕截图")

2. 修改后的RAM初始化值

   ![输入图片说明](https://foruda.gitee.com/images/1674969228302880385/d172f046_5631341.png "屏幕截图")

3. 上板实测结果：

   ![输入图片说明](https://foruda.gitee.com/images/1674969243815349625/ab457d28_5631341.png "屏幕截图")

### 4、 结论

在仅修改BRAM初始化值的场景下，该方法可以大大减小从修改BRAM初始值到重新生成bitstream的时间，在交换机调试过程中，具有一定实用价值。

 **作者：西电ISN国家重点实验室网络与交换团队博士生刘欢。** 

# [在FPGA上完美复刻Windows 95](https://www.bilibili.com/video/BV1944y1p7BH/)

https://space.bilibili.com/1551161745

![输入图片说明](https://foruda.gitee.com/images/1675130854411228577/c912885c_5631341.png "屏幕截图")

https://www.bilibili.com/video/BV1944y1p7BH/

> P1 演示
P2 详细制作过程
文字版本详见置顶评论
欢迎交流
感谢一键三连

- 2023年1月29日 18:48

  > 转载WIN95复刻这期，推荐下FPGA项目和 OPENFPGA ， SIGER 青少年开源科普 https://gitee.com/flame-ai/siger 是 2021刊例， siger99.gitee.io 是新首页，500+ 期 专题，150+编委的科普自媒体，为 UP主点赞！

- 2023年1月29日 20:37

  > 嗯，可以

---

评论 75

- 十万_伏特 2022-02-03 17:23 | 93 :+1: 

  > 好家伙fpga实现了x86架构

- 你怎么这么有才 2022-02-02 08:32 :+1: 37

  > 太强了，都是怎么做到的，我为什么觉得FPGA的编程非常僵硬呢[辣眼睛]

  - OpenFPGA 2022-02-02 13:37 :+1: 22

    > 从根本上看，就是先实现486处理器，然后再安装系统

  - bili_1791334683 回复 @OpenFPGA 2022-02-02 19:14 :+1: 1

    > 有没有95蓝屏的画面？多少年了，都忘了

  - 鹤舞长空1337 2022-02-02 21:30 回复 @OpenFPGA : microblaze？

    - OpenFPGA 回复 @鹤舞长空1337 :+1: 1  2022-02-03 02:07 就是硬件486处理器

      - 去码头整点金条 回复 @OpenFPGA : 2022-02-03 23:24

        > 486是开源的吗，还是有半开源design项目，还是根据指令集自己撸的rtl呀？

  - 你怎么这么有才 回复 @OpenFPGA : 2022-02-03 00:47

    > 老哥厉害，我关注你微信公众号了[doge]

  - CYTZyz 2022-02-03 15:18 :+1: 2

    > 为什么你们可以实现486，英特尔486是开源的吗

- 最后一根飞线 2022-02-01 23:01 :+1: 29

  > 太强了吧，为所欲为的为所欲为[辣眼睛]

- 炸春卷 2022-02-16 02:49 :+1: 27

  > 该马桶执行了非法操作，即将关闭，如有问题，请向马桶供应商联系

- 我在B站做视频 2022-02-03 18:57 :+1: 21

  > 我还看到有大佬用esp32跑win3.1的[笑哭]

  - bili_60722796585 2022-02-06 14:51 :+1: 1

    > 而且我还在下面回复能不能跑95.

    - 风景与他 回复 @bili_60722796585 

      > 2022-08-14 17:06 : 我看见了。  
      > 2022-08-14 17:06 : Windows95，最低需要4兆内存。

  - 不稳定海洋 2022-02-11 08:37 :+1: 1

    > 快进到用三极管跑。

- 兰星工控 2022-02-03 19:25 :+1: 17

  > 不懂就问，可以用FPGA实现12900K么[doge]

  - 英特尔中国intel 2022-02-03 22:24 :+1: 2

    > 理论上可以，但是制程差很多

    - 小宝____ 回复 @英特尔中国intel 2022-02-04 19:27 : 理论上不可能，规模 散热都搞不定

  - 一路人甲一 2022-02-04 01:05 :+1: 5

    > 买es版的，开盖，旁边就有颗fpga

    - 兰星工控 回复 @一路人甲一 2022-02-04 07:16 :功能是什么😳

      - 一路人甲一 回复 @兰星工控 2022-02-04 09:52 : 就是cpu，另一个是显卡

        - 无情冲塔 回复 @一路人甲一 2022-02-18 08:58 :+1: 3

          > 一般Intel的fpga是用来内存寻址的，amd收购xilinx之后也做了这样的计划

      - 超前进位加法器 回复 @兰星工控 2022-02-04 09:54 : 硬件加速某些功能

        - 限位符 回复 @超前进位加法器 2022-02-04 11:37 : 可以当缓存，也可以当成扩展IO

  - TLDLHS 2022-02-10 10:56 : 我超。撞头像了

    - 兰星工控 回复 @TLDLHS 2022-02-10 16:23 : 握个爪[doge]

- _凌晨_ 2022-02-07 14:06 :+1: 15

  > 卧槽，可编程晶体管的力量[支持]

- 名字好难啊555 2022-02-09 10:32 :+1: 12

  > 单片机我都搞不定，我这是有多勇才敢点开这视频[捂眼]

- bili-7208165143 2022-02-04 12:22 :+1: 8

  > 北桥南桥都是模拟的？这些芯片的细节可以在那里弄到？

  - 托卡马克何时能发电 2022-02-05 00:30 :+1: 9

    > 不需要，北桥是显示核心随便搞个图像驱动器就行，南桥没有复杂的电源这些就更不用了。

  - bili-7208165143 回复 @RICS架构微机用户 : 2022-02-05 02:14

    > 但是要运行在Windows上，如果不跟现有的产品相同，驱动就得另外写了

  - 托卡马克何时能发电 回复 @1m93onl 2022-02-06 16:00 :

    > 接口协议这些标准和当时的产品兼容就行，不一定要百分百相似吧

  - bili-7208165143 回复 @RICS架构微机用户 : 2022-02-06 22:53

    > 我说的就是register map，我最近也打算做类似的东西

- sven_1234 2022-02-03 14:19 :+1: 6

  > 实现了一个x86[支持]

- mango7720 2022-02-01 22:29 :+1: 3

  > [脸红][星星眼]太强了吧

- lizhirui 2022-02-05 17:16 :+1: 3

  > ao486项目?

  - 豆子Doge 2022-02-05 22:52 : 是的

  - OpenFPGA 2022-02-05 23:57 : 嗯，是的

  - abiaozsh 2022-05-16 18:33 : ao486资源占用太厉害了，我的开发板跑不起来

- iroek 2022-02-02 20:57 :+1: 2

  > 使用多少资源

  - OpenFPGA 2022-02-03 02:06 :+1: 2

    > 总共大约100k 486差不多37k 其他包括显示 ps2 声音

- 快去买酒 2022-02-03 15:54 :+1: 1

  > 牛逼的一比

- 咕噜鸡蛋饼干 2022-02-20 11:39 :+1: UP主觉得很赞

  > 背景声是不是拨号上网

  - OpenFPGA 2022-02-21 10:23 : 背景声音其实是win95的开机音乐，只不过有点抽象[保卫萝卜_哭哭]
  - Rywkr 回复 @OpenFPGA 2022-03-01 00:28 : 里面混了一段拨号上网的声音

- luckywwb :+1: 2022-02-03 16:13

  > 这么强[囧][囧][囧]

- 忘：忧 2022-02-03 13:39 :+1:  用的什么开发板啊

  - OpenFPGA 回复 @33201307962_bili 2022-06-17 07:56 : 友晶官网开源

    - 33201307962_bili 回复 @OpenFPGA 2022-06-15 21:17 : 大佬有de10的开发板资料吗

  - OpenFPGA 2022-02-03 13:43 : De10 nano

- lan_shen 2022-02-12 19:48 : 这也太强了吧！

- 春天的十七个瞬间 2022-07-11 22:24 : 386有开源的vhdl代码

- abiaozsh 2022-05-16 18:40 : 有普通开发板（10k） 能跑的系统吗？DE10太贵了

  - OpenFPGA 2022-05-17 12:53 : 跑不了呢

- 信封tegami 2022-02-22 02:40 : 卧槽

- donsum123 2022-02-09 17:48 : 油管上有个大神用分立元件搭了一个8086

  - mingtiannihao123 2022-05-19 21:25 : 我记得有一个6502的分体式元件视频

- klinxun 2022-02-09 10:39 : 划时代的95啊，童年记忆。

- er0s10n 2022-02-09 10:22 : 那这么说是不是可以刷成arm架构

  - maebari 2022-02-11 18:04 :

    > 理论上可以，不过需要arm的ip授权。另外xilinx的zynq系列就是arm+fpga的结合体

- 不白吃CrE 2022-02-09 09:44 : 装2k不好吗？

- 不白吃CrE 2022-02-09 09:42

  > 你下的是osr2.5。ie是4.0，但装好后感觉是osr2.1，用的ie3.0

  - 1drv-3 2022-02-17 11:52 :+1:  OSR25的IE4就是需要另装的。

- 甜蜜小猪窝窝窝 2022-02-09 03:05 : 想问问散热风扇是什么名字啊

- 社会易姐QwQ <img align="right" src="https://foruda.gitee.com/images/1675133015455621196/417a5452_5631341.png"> 2022-02-07 04:07 : bgm里有猫叫声

- 太空回声 2022-02-05 12:43 : doom、暗黑、war好评

- 技艺高超电子技术 2022-02-04 23:10 : CPLD可以实现这个功能吗

  - 林北请你挑塞 2022-02-05 22:44 : 资源不够吧

- E丶N 2022-02-02 15:17 : DE10 Nano？

  - OpenFPGA 2022-02-03 13:43 : 是的


# [ao486](https://gitee.com/shiliupi/ao486)/[README.md](https://gitee.com/shiliupi/ao486/blob/master/README.md)

### Description

The ao486 is an x86 compatible Verilog core implementing all features of a 486 SX.
The core was modeled and tested based on the Bochs software x86 implementation.
Together with the 486 core, the ao486 project also contains a SoC capable of
booting the Linux kernel version 3.13 and Microsoft Windows 95.

### Current status
- 31 March 2014  - initial version 1.0.
- 19 August 2014 - driver_sd update, ps2 fix.

### Features

The ao486 processor model has the following features:
- pipeline architecture with 4 main stages: decode, read, execute and write,
- all 486 instructions are implemented, together with CPUID,
- 16 kB instruction cache,
- 16 kB write-back data cache,
- TLB for 32 entries,
- Altera Avalon interfaces for memory and io access.

The ao486 SoC consists of the following components:
- ao486 processor,
- IDE hard drive that redirects to a HDL SD card driver,
- floppy controller that also redirects to the SD card driver,
- 8259 PIC,
- 8237 DMA,
- Sound Blaster 2.0 with DSP and OPL2 (FM synthesis not fully working).
  Sound output redirected to a WM8731 audio codec,
- 8254 PIT,
- 8042 keyboard and mouse controller,
- RTC
- standard VGA.

All components are modeled as Altera Qsys components. Altera Qsys connects all
parts together, and supplies the SDRAM controller.

The ao486 project is currently only running on the Terasic DE2-115 board.

### Resource usage

The project is synthesised for the Altera Cyclone IV E EP4CE115F29C7 device.
Resource utilization is as follows:

| Unit               | Logic cells | M9K memory blocks |
|--------------------|-------------|-------------------|
| ao486 processor    | 36517       | 47                |
| floppy             | 1514        | 2                 |
| hdd                | 2071        | 17                |
| nios2              | 1056        | 3                 |
| onchip for nios2   | 0           | 32                |
| pc_dma             | 848         | 0                 |
| pic                | 388         | 0                 |
| pit                | 667         | 0                 |
| ps2                | 742         | 2                 |
| rtc                | 783         | 1                 |
| sound              | 37131       | 29                |
| vga                | 2534        | 260               |

The fitter raport after compiling all components of the ao486 project is as
follows:

```
Fitter Status : Successful - Sun Mar 30 21:00:13 2014
Quartus II 64-Bit Version : 13.1.0 Build 162 10/23/2013 SJ Web Edition
Revision Name : soc
Top-level Entity Name : soc
Family : Cyclone IV E
Device : EP4CE115F29C7
Timing Models : Final
Total logic elements : 91,256 / 114,480 ( 80 % )
    Total combinational functions : 86,811 / 114,480 ( 76 % )
    Dedicated logic registers : 26,746 / 114,480 ( 23 % )
Total registers : 26865
Total pins : 108 / 529 ( 20 % )
Total virtual pins : 0
Total memory bits : 2,993,408 / 3,981,312 ( 75 % )
Embedded Multiplier 9-bit elements : 44 / 532 ( 8 % )
Total PLLs : 1 / 4 ( 25 % )
```

The maximum frequency is 39 MHz. The project uses a 30 MHz clock.

### CPU benchmarks

The package DosTests.zip from
http://www.roylongbottom.org.uk/dhrystone%20results.htm
was used to benchmark the ao486.

| Test                               | Result        |
-------------------------------------|---------------|
| Dhryston 1 Benchmark Non-Optimised | 1.00 VAX MIPS |
| Dhryston 1 Benchmark Optimised     | 4.58 VAX MIPS |
| Dhryston 2 Benchmark Non-Optimised | 1.01 VAX MIPS | 
| Dhryston 2 Benchmark Optimised     | 3.84 VAX MIPS |


### Running software

The ao486 successfuly runs the following software:
- Microsoft MS-DOS version 6.22,
- Microsoft Windows for Workgroups 3.11,
- Microsoft Windows 95,
- Linux 3.13.1.

### BIOS

The ao486 project uses the BIOS from the Bochs project
(http://bochs.sourceforge.net, version 2.6.2). Some minor changes
were required to support the hard drive.

The VGA BIOS is from the VGABIOS project (http://www.nongnu.org/vgabios,
version 0.7a). No changes were required. The VGA model does not have VBE
extensions, so the extensions were disabled.

### NIOS2 controller

The ao486 SoC uses a Altera NIOS2 processor for managing all components and
displaying the contents of the On Screen Display.

The OSD allows the user to insert and remove floppy disks.

### License

All files in the following directories:
- rtl,
- ao486_tool,
- sim

are licensed under the BSD license:

All files in the following directories:
- bochs486,
- bochsDevs

are taken from the Bochs Project and are licensed under the LGPL license.

The binary file sd/fd_1_44m/fdboot.img is taken from the FreeDOS project.

The binary file sd/bios/bochs_legacy is a compiled BIOS from the Bochs project.

The binary file sd/vgabios/vgabios-lgpl is a compiled VGA BIOS from the vgabios
project.

### Compiling
To compile the SoC, which contains the NIOS II microcontroller,  Altera Quartus II software is required.
The Verilog components of the SoC, in particular the ao486 processor, should be possible to compile
in any Verilog compiler. Currently synthesis project files are prepared only for Altera Quartus II.

NOTE: In the current version some synthesis project files -- especially the paths in those files, could be
broken.

#### ao486 processor
To compile the ao486 processor load the project file from syn/components/ao486/ao486.qpf.

#### SoC
To compile the ao486 SoC load the project file from syn/soc/soc.qpf.

Before compiling in Altera Quartus II, the Qsys system must be generated.

#### BIOS
To compile the BIOS do the following:
- extract the bochs-2.6.2 source archive,
- apply the patch from the directory bios/bochs-2.6.2 by running in the extracted directory:
  patch -p1 < (path to patch file)
- run ./configure in bochs
- run make in bochs
- cd bios
- make
- the binary file BIOS-bochs-legacy works with ao486 SoC.

#### VGABIOS
To compile the VGABIOS do the following:
- extract the vgabios-0.7a source archive,
- apply the patch form the directory bios/vgabios-0.7a by running in the extracted directory:
  patch -p1 < (path to patch file)
- run make in vgabios,
- the binary file VGABIOS-lgpl-latest.bin works with ao486 SoC.

### Running the SoC on Terasic DE2-115

- compile the soc Altera Quartus II project in syn/soc/soc.qpf
- compile the firmware for the NIOS II by:
    - opening the Nios II Software Build Tools for Eclipse,
    - creating a workspace in the directory syn/soc/firmware,
    - importing the two projects 'exe' and 'exe_bsp',
    - genrating BSP on the 'exe_bsp' project,
    - compiling the 'exe' project.
- compile the BIOS and copy the binary to the directory sd/bios,
- compile the VGABIOS and copy the binary to the directory sd/vgabios,
- compile the ao486_tool by running 'ant jar' in the directory ao486_tool,
- edit the files in the directory sd/hdd. They contain the position of the virtual hard disk located on
  the SD card. The start entry must be a multiplicity of 512. The values are in bytes from the begining
  of the SD card,
- run 'java -cp ./dist/ao486_tool.jar ao486.SDGenerator' in the directory ao486_tool,
- copy the file ao486_tool/sd.dat to the first sectors of the SD card by using 'dd if=sd.dat of=/dev/sdXXX'.
- insert the SD card to the Terasic DE2-115 board,
- program the FPGA using the SOF file,
- load and run the firmware of the NIOS II controller,
- select the BIOS file on the On Screen Display by using KEY0 for down, KEY1 for up and KEY2 for select,
- select the VGABIOS file on the OSD,
- select the hard drive on the OSD,
- select the floppy on the OSD. Use the KEYs to select the floppy image. Use KEY3 to cancel.
- after selecting the floppy or pressing cancel, ao486 boots,
- to activate the OSD press KEY2.

# [ao486](https://opencores.org/projects/ao486) :: [OpenCores](https://opencores.org)

<a class="UnderlineNav__Link-sc-14g1rj0-1 UnderlineNav-item selected iSdXyy" href="https://opencores.org/projects/ao486">Overview</a> | <a class="UnderlineNav__Link-sc-14g1rj0-1 UnderlineNav-item iSdXyy" href="https://opencores.org/projects/ao486/news">News</a> | <a class="UnderlineNav__Link-sc-14g1rj0-1 UnderlineNav-item iSdXyy" href="https://opencores.org/projects/ao486/downloads">Downloads</a> | <a class="UnderlineNav__Link-sc-14g1rj0-1 UnderlineNav-item iSdXyy" href="https://opencores.org/projects/ao486/bugtracker">Bugtracker</a>

</div><div class="overview__Maintainers-sc-1t8xrxd-0 jPDRDc"><h2 class="_common__H2-sc-1oqitgj-1 jVlvaY">Project maintainers</h2><ul><li><a href="/users/alfik/profile">Osman, Aleksander</a></li></ul></div><h2 class="_common__H2-sc-1oqitgj-1 jVlvaY">Details</h2><div class="_common__P-sc-1oqitgj-2 kIUTVe">Name: ao486<br>Created: Mar 30, 2014<br>Updated: Aug 20, 2014<br>SVN Updated: Aug 20, 2014<br>SVN: <a href="/websvn/listing/ao486">Browse</a><br>Latest version: <a href="/download/ao486">download</a> (might take a bit to start...)<br>Statistics: <a href="/stats/ao486">View</a><br><a href="/projects/ao486/bugtracker">Bugs:</a> 0 reported / 0 solved</div><div class="_common__P-sc-1oqitgj-2 kIUTVe"><span class="overview__StarContainer-sc-1t8xrxd-3 dUYjZl"><span><svg aria-hidden="true" role="img" class="octicon" viewBox="0 0 16 16" width="16" height="16" fill="currentColor" style="display: inline-block; user-select: none; vertical-align: text-bottom;"><path fill-rule="evenodd" d="M8 .25a.75.75 0 01.673.418l1.882 3.815 4.21.612a.75.75 0 01.416 1.279l-3.046 2.97.719 4.192a.75.75 0 01-1.088.791L8 12.347l-3.766 1.98a.75.75 0 01-1.088-.79l.72-4.194L.818 6.374a.75.75 0 01.416-1.28l4.21-.611L7.327.668A.75.75 0 018 .25z"></path></svg> Star</span><a href="/projects/ao486/stargazers">7</a></span><span class="overview__StarTagline-sc-1t8xrxd-4 sYJTO">you like it: star it!</span></div><h2 class="_common__H2-sc-1oqitgj-1 jVlvaY">Other project properties</h2><div class="_common__P-sc-1oqitgj-2 kIUTVe">Category:<a href="/projects?category=Processor">Processor</a><br>Language:<a href="/projects?language=Verilog">Verilog</a><br>Development status:<a href="/projects?stage=beta">Beta</a><br>Additional info:<a href="/projects?fpgaProven=true">FPGA proven</a><br>WishBone compliant: No<br>WishBone version: n/a<br>License: BSD</div><div class="_body__Content-euzcac-0 fuwsYa"><h2 id="description">Description</h2>
<p>The ao486 is an x86 compatible Verilog core implementing all features of a 486 SX.<br>
The core was modeled and tested based on the Bochs software x86 implementation.<br>
Together with the 486 core, the ao486 project also contains a SoC capable of<br>
booting the Linux kernel version 3.13 and Microsoft Windows 95.</p>
<h2 id="current-status">Current status</h2>
<p></p><ul>
<li>31 March 2014 - initial version 1.0.</li>
<li>19 August 2014 - driver_sd update, ps2 fix.</li>
</ul>
<p></p>
<h2 id="links">Links</h2>
<p></p><ul>
<li>ao486 project on github.com: <a href="http://github.com/alfikpl/ao486">http://github.com/alfikpl/ao486</a>.
</li></ul>
<p></p>
<h2 id="features">Features</h2>
<p>The ao486 processor model has the following features:
</p><ul>
<li>pipeline architecture with 4 main stages: decode, read, execute and write,</li>
<li>all 486 instructions are implemented, together with CPUID,</li>
<li>16 kB instruction cache,</li>
<li>16 kB write-back data cache,</li>
<li>TLB for 32 entries,</li>
<li>Altera Avalon interfaces for memory and io access.</li>
</ul>

The ao486 SoC consists of the following components:
<ul>
<li>ao486 processor,</li>
<li>IDE hard drive that redirects to a HDL SD card driver,</li>
<li>floppy controller that also redirects to the SD card driver,</li>
<li>8259 PIC,</li>
<li>8237 DMA,</li>
<li>Sound Blaster 2.0 with DSP and OPL2 (FM synthesis not fully working).
  Sound output redirected to a WM8731 audio codec,</li>
<li>8254 PIT,</li>
<li>8042 keyboard and mouse controller,</li>
<li>RTC,</li>
<li>standard VGA.</li>
</ul>

<p>
All components are modeled as Altera Qsys components. Altera Qsys connects all
parts together, and supplies the SDRAM controller.
</p>

<p>
The ao486 project is currently only running on the Terasic DE2-115 board.
</p>
<p></p>
<h2 id="resource-usage">Resource usage</h2>
<p>The project is synthesised for the Altera Cyclone IV E EP4CE115F29C7 device.
Resource utilization is as follows:

</p><table border="1">
<tbody><tr><th>Unit</th><th>Logic cells</th><th>M9K memory blocks</th></tr>

<tr><td>ao486 processor </td><td>36517</td><td>47</td></tr>
<tr><td>floppy          </td><td>1514 </td><td>2 </td></tr>
<tr><td>hdd             </td><td>2071 </td><td>17</td></tr>
<tr><td>nios2           </td><td>1056 </td><td>3 </td></tr>
<tr><td>onchip for nios2</td><td>0    </td><td>32</td></tr>
<tr><td>pc_dma          </td><td>848  </td><td>0 </td></tr>
<tr><td>pic             </td><td>388  </td><td>0 </td></tr>
<tr><td>pit             </td><td>667  </td><td>0 </td></tr>
<tr><td>ps2             </td><td>742  </td><td>2 </td></tr>
<tr><td>rtc             </td><td>783  </td><td>1 </td></tr>
<tr><td>sound           </td><td>37131</td><td>29</td></tr>
<tr><td>vga             </td><td>2534 </td><td>260</td></tr>
</tbody></table>

<p>
The fitter raport after compiling all components of the ao486 project is as
follows:
</p>

<code>
Fitter Status : Successful - Sun Mar 30 21:00:13 2014                 <br>
Quartus II 64-Bit Version : 13.1.0 Build 162 10/23/2013 SJ Web Edition<br>
Revision Name : soc                                                   <br>
Top-level Entity Name : soc                                           <br>
Family : Cyclone IV E                                                 <br>
Device : EP4CE115F29C7                                                <br>
Timing Models : Final                                                 <br>
Total logic elements : 91,256 / 114,480 ( 80 % )                      <br>
    Total combinational functions : 86,811 / 114,480 ( 76 % )         <br>
    Dedicated logic registers : 26,746 / 114,480 ( 23 % )             <br>
Total registers : 26865                                               <br>
Total pins : 108 / 529 ( 20 % )                                       <br>
Total virtual pins : 0                                                <br>
Total memory bits : 2,993,408 / 3,981,312 ( 75 % )                    <br>
Embedded Multiplier 9-bit elements : 44 / 532 ( 8 % )                 <br>
Total PLLs : 1 / 4 ( 25 % )                                           <br>
</code>

<p>
The maximum frequency is 39 MHz. The project uses a 30 MHz clock.
</p>
<p></p>
<h2 id="cpu-benchmarks">CPU benchmarks</h2>
<p>The package DosTests.zip from
<a href="http://www.roylongbottom.org.uk/dhrystone%20results.htm">http://www.roylongbottom.org.uk/dhrystone%20results.htm</a>
was used to benchmark the ao486.

</p><table border="1">
<tbody><tr><th>Test</th><th>Result</th></tr>
<tr><td>Dhryston 1 Benchmark Non-Optimised</td><td>1.00 VAX MIPS</td></tr>
<tr><td>Dhryston 1 Benchmark Optimised    </td><td>4.58 VAX MIPS</td></tr>
<tr><td>Dhryston 2 Benchmark Non-Optimised</td><td>1.01 VAX MIPS</td></tr>
<tr><td>Dhryston 2 Benchmark Optimised    </td><td>3.84 VAX MIPS</td></tr>
</tbody></table>
<p></p>
<h2 id="running-software">Running software</h2>
<p>The ao486 successfuly runs the following software:
</p><ul>
<li>Microsoft MS-DOS version 6.22,</li>
<li>Microsoft Windows for Workgroups 3.11,</li>
<li>Microsoft Windows 95,</li>
<li>Linux 3.13.1.</li>
</ul>
<p></p>
<h2 id="bios">BIOS</h2>
<p></p><p>
The ao486 project uses the BIOS from the Bochs project
(http://bochs.sourceforge.net, version 2.6.2). Some minor changes
were required to support the hard drive.
</p>

<p>
The VGA BIOS is from the VGABIOS project (http://www.nongnu.org/vgabios,
version 0.7a). No changes were required. The VGA model does not have VBE
extensions, so the extensions were disabled.
</p>
<p></p>
<h2 id="nio-s2-controller">NIOS2 controller</h2>
<p></p><p>
The ao486 SoC uses a Altera NIOS2 processor for managing all components and
displaying the contents of the On Screen Display.
</p>

<p>
The OSD allows the user to insert and remove floppy disks.
</p>
<p></p>
<h2 id="license">License</h2>
<p>All files in the following directories:
</p><ul>
<li>rtl,</li>
<li>ao486_tool,</li>
<li>sim.</li>
</ul>
are licensed under the BSD license:

All files in the following directories:
<ul>
<li>bochs486,</li>
<li>bochsDevs.</li>
</ul>
are taken from the Bochs Project and are licensed under the LGPL license.

<p>
The binary file sd/fd_1_44m/fdboot.img is taken from the FreeDOS project.
</p>

<p>
The binary file sd/bios/bochs_legacy is a compiled BIOS from the Bochs project.
</p>

<p>
The binary file sd/vgabios/vgabios-lgpl is a compiled VGA BIOS from the vgabios
project.
</p>
<p></p>

---

- [de10-nano中文资料,数据手册,替代选型,技术支持](https://www.sekorm.com/Web/Search/keyword/DE10-NANO%20%E5%BC%80%E5%8F%91%E6%9D%BF)  
  www.sekorm.com

  > 来自 360de10-nano数据手册下载,BOM替换,研发经验支持,国产化替代,应用方案设计,专业技术支持,免费研发服务,定制加工,测试测量,开放实验 

    - [DE10-Nano Cyclone V SoC with Dual-core ARM Cortex-A9 User Manual](https://www.sekorm.com/doc/3103474.html) 用户指南  -  友晶科技  - October 19, 2021  中文 下载

      > 型号- DE10-NANO

    - [DE10-Nano FPGA development board QT Control Panel](https://www.sekorm.com/doc/3670373.html) 用户指南  -  友晶科技  - July 19, 2017  英文 下载

      > 型号- DE10-NANO

    - [DE10-Nano User Manual](https://www.sekorm.com/doc/3707188.html) 用户指南  -  友晶科技  - V2.2  - December 31, 2019  英文 下载

      > 型号- DE10-NANO

    - [World-Leading FPGA Design Services Product Catalog](https://www.sekorm.com/doc/3103468.html) 选型指南  -  友晶科技  - 2020/12/10  英文 下载

      > 型号- DE10-NANO,DE1-SOC-MTL2,TR5-F40W,DE5A-NET-DDR4,DE4,TR5-LITE,VEEK-MT2,DE10-PRO,VEEK-MT2S,TR10A-LP,TR10A-HL,DE0-CV,MAX 10 NEEK,TR10A-LPQ,DE5-NET,DE2-115

- 【资料】[2021年最网红的FPGA开发板之一——DE10-Nano](https://www.cnblogs.com/DoreenLiu/p/14841667.html) ...  
  https://www.cnblogs.com/DoreenLiu/p/14841667.html

  - FPGA设计资料1.FPGA基础案例（my_first_fpga）2.外设自检案例（Default …
  - FPGA与HPS协同工作的开发资料。HPS SoC 设计示例(硬件工程，Verilog)1.基于QT的开发 …
  - OpenCL 资料1.BSP(Board Support Package) for Intel FPGA SDK OpenCL 14.0及使用手册。
  - 与友晶科技子卡搭配的demo1.与D8M摄像头搭配的案例2.与蓝牙WiFi子卡RFS搭配的案例3.与 …

  请在 cnblogs.com 查看完整列表

# 【资料】[2021年最网红的FPGA开发板之一——DE10-Nano （SOC FPGA入门推荐！）](https://www.cnblogs.com/DoreenLiu/p/14841667.html)
    
<div class="post">
            <div class="postcontent">
                <div id="cnblogs_post_body" class="blogpost-body blogpost-body-html">
<div>DE10-Nano开发板可谓2021年最网红的FPGA开发板之一，除了广泛用于物联网，边缘计算，硬件加速，AI 和 EDA教育课程外，还有不少发烧友将其用于网络上日渐风靡的开源复古游戏项目——Mister。可谓是跨越了学术界，工业界，科研界，无所不及。</div>
<p></p>
<p>友晶科技对板子提供的技术支持也是十分给力，不仅24小时官方邮件答复， 论坛值守，还不断不断的开发出配套的参考案例。下面让我们来看看DE10-Nano 都提供哪些资料吧：</p>
<h1><span id=".E5.8F.8B.E6.99.B6.E5.AE.98.E7.BD.91.E7.9A.84.E8.B5.84.E6.96.99" class="mw-headline">友晶官网的资料</span><span id=".E5.8F.8B.E6.99.B6.E5.AE.98.E7.BD.91.E7.9A.84.E8.B5.84.E6.96.99" class="mw-headline" style="background-color: rgba(255, 204, 0, 1)">（提供中文手册啦！！！）</span><button class="cnblogs-toc-button" title="显示目录导航" aria-expanded="false"></button></h1>
<p>提供中英文手册，电路图（PDF版）， 各种案例教程等。官网：<a class="external free" href="http://www.terasic.com.cn/cgi-bin/page/archive.pl?Language=China&amp;CategoryNo=182&amp;No=1048&amp;PartNo=4" rel="noopener">http://www.terasic.com.cn/cgi-bin/page/archive.pl?Language=China&amp;CategoryNo=182&amp;No=1048&amp;PartNo=4</a></p>
<h2><span id=".E4.B8.80._FPGA.E8.AE.BE.E8.AE.A1.E8.B5.84.E6.96.99" class="mw-headline">一. FPGA设计资料</span><button class="cnblogs-toc-button" title="显示目录导航" aria-expanded="false"></button></h2>
<p>1. FPGA基础案例（my_first_fpga）</p>
<p>2. 外设自检案例（Default code）</p>
<p>3. Golden_top</p>
<p>4. ADC信号读取案例（ADC）</p>
<p>5. 基于RTL代码设计的DDR3测试案例（DDR3_RTL）</p>
<p>6. 基于Nios设计的DDR3 VIP图像输出案例（DDR3_VIP）</p>
<p>7. HDMI TX显示视频图像案例（HDMI_TX）</p>
<h2><span id=".E4.BA.8C._FPGA.E4.B8.8EHPS.E5.8D.8F.E5.90.8C.E5.B7.A5.E4.BD.9C.E7.9A.84.E5.BC.80.E5.8F.91.E8.B5.84.E6.96.99" class="mw-headline">二. FPGA与HPS协同工作的开发资料</span><button class="cnblogs-toc-button" title="显示目录导航" aria-expanded="false"></button></h2>
<p><strong>HPS SoC 设计示例(硬件工程，Verilog)</strong></p>
<p>1. 基于QT的开发板控制台案例（ControlPanel）</p>
<p>2. HPS控制FPGA端LED的案例（HPS_FPGA_LED）</p>
<p>3. GHRD工程（DE10_NANO_SoC_GHRD）</p>
<p>4. LXDE 桌面BSP工程（DE10_NANO_SoC_FB）</p>
<p>5. Nios II访问HPS端DDR3案例（Nios_Access_DDR3）</p>
<p><br><strong>HPS SoC 设计示例(C代码)</strong> </p>
<p>1. HPS基础案例(my_first_hps) </p>
<p>2. HPS GPIO应用案例(hps_gpio) </p>
<p>3. HPS 传感器应用案例(hps_gsensor) </p>
<p>4. HPS USB应用案例(USB_Gadget) </p>
<p><br><strong>SoC_Advanced 进阶案例</strong> </p>
<p>1. 查询互联网时间（NET_Time） </p>
<p>2. Bluetooth SPP (Serial Port Profile)案例 </p>
<p>3. 基于OpenCV的设计案例 </p>
<p><br><strong>Linux内核为3.12的镜像</strong> </p>
<p>1. Linux Console镜像 </p>
<p>2. Linux Console with framebuffer </p>
<p>3. Linux LXDE Desktop </p>
<p>4. Linux Ubuntu Desktop </p>
<p><br><strong>Linux内核为4.5的镜像文件</strong> </p>
<p>1. Linux LXDE Desktop </p>
<p>2. Linux Ubuntu Desktop </p>
<h2><span id=".E4.B8.89._OpenCL_.E8.B5.84.E6.96.99" class="mw-headline">三. OpenCL 资料</span><button class="cnblogs-toc-button" title="显示目录导航" aria-expanded="false"></button></h2>
<p>1. BSP(Board Support Package) for Intel FPGA SDK OpenCL 14.0及使用手册 </p>
<p>2. BSP(Board Support Package) for Intel FPGA SDK OpenCL 16.0及使用手册 </p>
<p>3. BSP(Board Support Package) for Intel FPGA SDK OpenCL 18.1及使用手册 </p>
<p>4.支持OpenCV的OpenCL VNC 镜像文件</p>
<h2><span id=".E5.9B.9B._.E4.B8.8E.E5.8F.8B.E6.99.B6.E7.A7.91.E6.8A.80.E5.AD.90.E5.8D.A1.E6.90.AD.E9.85.8D.E7.9A.84demo" class="mw-headline">四. 与友晶科技子卡搭配的demo</span><button class="cnblogs-toc-button" title="显示目录导航" aria-expanded="false"></button></h2>
<p>1. 与D8M摄像头搭配的案例 </p>
<p>2. 与蓝牙WiFi子卡RFS搭配的案例 </p>
<p>3. 与电机驱动子卡SMK搭配的案例 </p>
<p>4. 与Arduino Shield子卡搭配的案例 </p>
<p>5. 与LT24搭配的案例 </p>
<p>6. 与MTL2触摸屏搭配的案例 </p>
<h2><span id=".E4.BA.94._.E5.88.9B.E6.96.B0.E8.AE.BE.E8.AE.A1.E5.A4.A7.E8.B5.9B.E6.88.96Intel.E5.8F.82.E8.80.83.E8.AE.BE.E8.AE.A1" class="mw-headline">五. 创新设计大赛或Intel参考设计</span><button class="cnblogs-toc-button" title="显示目录导航" aria-expanded="false"></button></h2>
<p><strong>1. 创新设计大赛优秀设计</strong> </p>
<p>总冠军&nbsp;: Flex Force Smart Glove </p>
<p>银奖&nbsp;: Real-time HDR video </p>
<p>铜奖&nbsp;: PipeCNN </p>
<p><strong>2.Intel参考设计</strong> </p>
<p>AWS Greengrass认证设计资源</p>
<h2><span id=".E5.85.AD.E3.80.81.E8.A7.86.E9.A2.91.E8.B5.84.E6.96.99" class="mw-headline">六、视频资料</span><button class="cnblogs-toc-button" title="显示目录导航" aria-expanded="false"></button></h2>
<p>1. DE10-Nano产品介绍（包括简介、硬件介绍和对应外设demo演示、主子卡搭配demo 演示、可提供的案例资源等） </p>
<p>2. DE10-Nano Kit: Training Materials and Support Packages （DE10-Nano所提供的的案例资源） </p>
<p>3. DE10-Nano Linux BSP 案例演示视频 </p>
<p>4. DE10-Nano 扩展子卡展示视频 </p>
<p>5. DE10-Nano OpenCL 案例演示视频 </p>
<p>观看： </p>
<p><a class="external free" href="http://www.terasic.com.cn/cgi-bin/page/archive.pl?Language=China&amp;CategoryNo=182&amp;No=1048&amp;PartNo=6" rel="noopener">http://www.terasic.com.cn/cgi-bin/page/archive.pl?Language=China&amp;CategoryNo=182&amp;No=1048&amp;PartNo=6</a> </p>
<p>以上2、3、4、5的合集完整视频：<a class="external free" href="https://v.youku.com/v_show/id_XMzA1NzYyMzQ1Mg==.html?spm=a2hbt.13141534.app.5~5!2~5!2~5~5~5!2~5~5!2~5!2~5!2~5~5!23~A" rel="noopener">https://v.youku.com/v_show/id_XMzA1NzYyMzQ1Mg==.html?spm=a2hbt.13141534.app.5~5!2~5!2~5~5~5!2~5~5!2~5!2~5!2~5~5!23~A</a> </p>
<h1><span id=".E8.AF.BE.E7.A8.8B.E5.9F.B9.E8.AE.AD.E8.B5.84.E6.96.99" class="mw-headline">课程培训资料</span><button class="cnblogs-toc-button" title="显示目录导航" aria-expanded="false"></button></h1>
<h2><span id="2018.E5.B9.B4.E4.BA.A7.E5.AD.A6.E5.90.88.E4.BD.9C.E5.9F.B9.E8.AE.AD.E8.B5.84.E6.96.99" class="mw-headline">2018年产学合作培训资料</span><button class="cnblogs-toc-button" title="显示目录导航" aria-expanded="false"></button></h2>
<p>基于2018年产学合作培训资料：<a class="external free" href="http://mail.terasic.com.cn/~dongliu/2020training.rar%EF%BC%88%E6%9B%B4%E6%96%B0%E8%87%AA2018%E4%BA%A7%E5%AD%A6%E5%90%88%E4%BD%9C%E8%B5%84%E6%96%99%EF%BC%89" rel="noopener">http://mail.terasic.com.cn/~dongliu/2020training.rar（更新自2018产学合作资料）</a> </p>
<p>其中录播课正在筹备中</p>
<h2><span id="2020.E5.B9.B4DE10-Nano.E7.B3.BB.E5.88.97.E8.AF.BE.E7.A8.8B" class="mw-headline">2020年DE10-Nano系列课程</span><button class="cnblogs-toc-button" title="显示目录导航" aria-expanded="false"></button></h2>
<p>公开链接：<a class="external free" href="http://www.myfpga.org/discuz/forum.php?mod=viewthread&amp;tid=197176&amp;extra=" rel="noopener">http://www.myfpga.org/discuz/forum.php?mod=viewthread&amp;tid=197176&amp;extra=</a></p>
<h2><span id="Intel.E5.AE.98.E7.BD.91.E5.A4.A7.E5.AD.A6.E8.AE.A1.E5.88.92.E8.AF.BE.E7.A8.8B.EF.BC.88.E8.8B.B1.E6.96.87.EF.BC.8C.E5.8F.AF.E7.A7.BB.E6.A4.8D.E5.88.B0DE10-Nano.EF.BC.89" class="mw-headline">Intel官网大学计划课程（英文，可移植到DE10-Nano）</span><button class="cnblogs-toc-button" title="显示目录导航" aria-expanded="false"></button></h2>
<p>课程一： 数字逻辑 </p>
<p>课程二： 计算机组成 </p>
<p>课程三：嵌入式系统 </p>
<p>课程四：人工智能 </p>
<p>链接：<a class="external free" href="https://software.intel.com/content/www/us/en/develop/topics/fpga-academic/teach.html" rel="noopener">https://software.intel.com/content/www/us/en/develop/topics/fpga-academic/teach.html</a></p>
<h2><span id="Intel_FPGA_.E5.85.A5.E9.97.A8.E6.95.99.E7.A8.8B" class="mw-headline">Intel FPGA 入门教程</span><button class="cnblogs-toc-button" title="显示目录导航" aria-expanded="false"></button></h2>
<p>链接： <a class="external free" href="https://software.intel.com/content/www/cn/zh/develop/topics/fpga-academic/learn/tutorials.html" rel="noopener">https://software.intel.com/content/www/cn/zh/develop/topics/fpga-academic/learn/tutorials.html</a></p>
<h1><span id="Mister.E9.A1.B9.E7.9B.AE" class="mw-headline">Mister 复古游戏</span><button class="cnblogs-toc-button" title="显示目录导航" aria-expanded="false"></button></h1>
<p>github网站：&nbsp;<a href="https://github.com/MiSTer-devel/Main_MiSTer/wiki/Setup-Guide" target="_blank" rel="noopener">https://github.com/MiSTer-devel/Main_MiSTer/wiki/Setup-Guide</a></p>
<p>百度贴吧：&nbsp;<a href="https://tieba.baidu.com/f?kw=mister_fpga&amp;ie=utf-8" target="_blank" rel="noopener">https://tieba.baidu.com/f?kw=mister_fpga&amp;ie=utf-8</a></p>
<h1><span id=".E5.B8.B8.E8.A7.81.E9.97.AE.E9.A2.98.E8.A7.A3.E7.AD.94FAQ" class="mw-headline">常见问题解答FAQ</span><button class="cnblogs-toc-button" title="显示目录导航" aria-expanded="false"></button></h1>
<p>My FPGA 论坛的 DE10-Nano FAQ:<a class="external free" href="http://www.myfpga.org/discuz/forum.php?mod=forumdisplay&amp;fid=127" rel="noopener">http://www.myfpga.org/discuz/forum.php?mod=forumdisplay&amp;fid=127</a></p>
<p>&nbsp;</p>
<p><span style="background-color: rgba(255, 204, 0, 1); font-size: 18px">更多资料请联系support@terasic.com.cn</span></p>
<p>&nbsp;</p>
</div>

![输入图片说明](https://foruda.gitee.com/images/1675149842383151450/e1cb0fc0_5631341.png "屏幕截图")

- [Terasic DE10-Nano](https://www.intel.com/content/www/us/en/developer/topic-technology/edge-5g/hardware/fpga-de10-nano.html) en
  https://www.intel.cn/.../hardware/fpga-de10-nano.html

- [Terasic DE10-Nano - Intel](https://www.intel.cn/content/www/cn/zh/developer/topic-technology/edge-5g/hardware/fpga-de10-nano.html) cn
  https://www.intel.com/.../developer/topic-technology/edge-5g/hardware/fpga-de10-nano.html
  > 2023年1月19日 · Overview. Based on a Cyclone® V SoC FPGA, this kit provides a …

# Overview 概述

Based on a Cyclone® V SoC FPGA, this kit provides a reconfigurable hardware design platform for makers, educators, and IoT system developers. 此套件基于 Cyclone® V SoC FPGA，为创客、教育工作者和物联网系统开发人员提供可重新配置的硬件设计平台。

- Equipped with high-speed DDR3 memory 配备高速 DDR3 内存
- Includes 2 GPIO expansion headers 包括 2 个 GPIO 扩展接头
- Provides analog-to-digital capabilities 提供模拟转数字功能

Get Started 开始 [Where to Buy](https://marketplace.intel.com/s/pmp-partner-program/a723b0000008PICAA2/distributor?language=en_US) [何处购买](https://marketplace.intel.com/s/pmp-partner-program/a723b0000008PICAA2/distributor?language=en_US)

- 分销商分类，根本就没有中国！ :-1: 

> Contact an Intel® Authorized Distributor today. 立即联系英特尔® 授权分销商。

### 套件详情

![输入图片说明](https://foruda.gitee.com/images/1675150112112641494/0b1ecfb6_5631341.png "屏幕截图")

 **硬件** 

- DE10-Nano 主板
- DE10-Nano 快速入门指南
- A 型 USB 至 Mini-B 缆线
- A 型 USB 至 Micro-B 缆线
- 直流电源适配器 (5 V)
- microSD* 卡（已安装）

 **预装软件** 

- 片上系统 (SoC) 引导至 Linux 操作系统
- 互联网和虚拟网络计算 (VNC) 服务器
- 参考设计
- 开发工具

---

- Mister 复古游戏

  - [开源FPGA硬件模拟游戏机，原汁原味的复古游戏体验带你回童年](https://www.qbitai.com/2021/04/22927.html)
    https://www.qbitai.com/2021/04/22927.html

    > 2021年4月14日 · 近几年，游戏界兴起了一股复古风潮。 玩腻了追求极致画面表现，玩法上却千

  - [使用FPGA和MiSTer进行复古计算_cumo7370的博](https://blog.csdn.net/cumo7370/article/details/107410131) …
    https://blog.csdn.net/cumo7370/article/details/107410131

    > 2020年7月12日 · 在我的MiSTer板上，即使我的Pi有更多的可用内存，我也可以运行不在Raspberry Pi上运行的Amiga演示。 仿真更加准确，运行效率更高。 通过扩展板，我什至可以使用旧硬件，例如原始的Commodore显示器和Amiga游戏 …  

  - [我爱玩游戏 篇一：复古游戏与开源掌机的基础知识 - 知乎](https://zhuanlan.zhihu.com/p/482019157)
    https://zhuanlan.zhihu.com/p/482019157

    > 2022年3月16日 · 玩过斐讯N1刷陈复古游戏机的肯定都知道【人中日月】这位大佬，【人中日月 …

      缺失: Mister必须包含: Mister

  - 免费下载的最佳复古游戏 - Retrolorian
    https://www.retrolorian.com/cn

    > 准备好深入了解复古PC游戏，超过21317个游戏可供选择，并等待再次播放。发现罕见的80年代 …

      缺失: Mister必须包含: Mister

# [使用FPGA和MiSTer进行复古计算](https://blog.csdn.net/cumo7370/article/details/107410131)

cumo7370 于 2020-07-12 21:12:49 发布 | 1698

另一个周末即将来临，我可以花一些时间从事我的激情项目，包括使用单板计算机，使用模拟器以及使用烙铁进行一般修补。 今年早些时候，我写过关于 [在Raspberry Pi上复活Commodore Amiga的文章](https://opensource.com/article/19/3/amiga-raspberry-pi) 。 一位同事将我们对旧技术的痴迷称为“ [对保存我们的数字文化的热情](https://www.linkedin.com/pulse/passion-preserving-digital-culture-%C3%B8ivind-ekeberg/) ”。

在“数字考古学”世界中，我听说了一种通过使用 [现场可编程门阵列](https://en.wikipedia.org/wiki/Field-programmable_gate_array) （FPGA）来模拟旧系统的新方法。 这个概念让我很感兴趣，因此我花了一个周末来学习更多。 具体来说，我想知道是否可以使用FPGA模拟Commodore Amiga。

### 什么是FPGA？

当您构建电路板时，所有内容实际上都是在硅中蚀刻的。 您可以更改在其上运行的软件，但是物理电路是不可变的。 因此，如果要向其添加新组件或稍后对其进行修改，则受到硬件物理特性的限制。 使用FPGA，您可以对硬件进行编程以模拟新组件或更改现有组件。 这是通过可编程逻辑门实现的（因此得名）。 这为物联网（IoT）设备提供了很大的灵活性，因为以后可以对其进行更改以满足新的要求。

![Terasic DE10-Nano](https://foruda.gitee.com/images/1675151314033870059/fc0e2fd4_5631341.png "屏幕截图")

今天，FPGA被用于许多设备，包括智能手机，医疗设备，机动车辆和飞机。 由于FPGA可以轻松修改并且通常具有低功耗要求，因此这些器件无处不在！ 它们的制造成本也不高，并且可以配置为多种用途。

Commodore Amiga设计的芯片具有特定用途和有趣的名称。 例如，“加里”是一个门阵列，后来在A3000和A4000上升级“他”时变成了“胖加里”。 “ Bridgette”是一个集成的总线缓冲区，而令人愉悦的“ Amber”是A3000上的一个“闪烁固定器”。 利用可编程门模拟这些芯片的能力为Amiga仿真提供了理想的平台。

### 介绍MiSTer项目

我一直在使用的板是 [Terasic](https://www.terasic.com.tw/en/) 的 [DE10-Nano](https://www.terasic.com.tw/cgi-bin/page/archive.pl?Language=English&CategoryNo=165&No=1046) 。 该设备开箱即用，非常适合学习FPGA的工作原理，使您可以使用各种工具来入门。

![Terasic DE10-Nano](https://foruda.gitee.com/images/1675152124335627517/3c328fbc_5631341.png "屏幕截图")

[MiSTer项目](https://github.com/MiSTer-devel/Main_MiSTer/wiki) 建立在该板的顶部，并采用子板来提供内存扩展，SDRAM和改进的I / O，所有这些均建立在基于Linux的发行版上。 为了将其用作仿真平台，它通过使用“核心”进行了扩展，这些“核心”定义了电路板将要仿真的架构。

用MiSTer发行版刷新设备后，您可以加载“核心”，该核心是要使用的芯片的定义以及用于管理仿真系统的关联菜单的组合。

![Terasic DE10-Nano](https://foruda.gitee.com/images/1675151961822662621/f24c9f3c_5631341.png "屏幕截图")

与运行仿真软件的Raspberry Pi相比，这些内核为仿真提供了更多的本机体验，通常，在基于软件的仿真器上不能完美运行的应用程序在MiSTer上可以正常运行。

### 如何开始

在线上有大量资源可以帮助您入门。 第一站是MiSTer的 [GitHub](https://github.com/MiSTer-devel) 页面上的[文档](https://github.com/MiSTer-devel/Main_MiSTer/wiki/Setup-Guide) ，其中包含有关将所有内容组合在一起的分步说明。 如果您希望从视觉上看板，请从 [Retro Man Cave](https://www.youtube.com/channel/UCLEoyoOKZK0idGqSc6Pi23w) YouTube频道观看[此视频](https://www.youtube.com/watch?v=e5yPbzD-W-I&t=2s) 。 有关配置 [Minimig](https://github.com/MiSTer-devel/Minimig-AGA_MiSTer) （mini Amiga的缩写）内核以加载磁盘或使用Amiga的经典[Workbench](https://en.wikipedia.org/wiki/Workbench_%28AmigaOS%29)和 [WHDLoad](https://en.wikipedia.org/wiki/WHDLoad) 的更多信息 ，请查看YouTube上 [Phil的计算机实验室](https://www.youtube.com/channel/UCj9IJ2QvygoBJKSOnUgXIRA) 的精彩[教程](https://www.youtube.com/watch?v=VFespp1adI0) 。

### 核心数

MiSTer具有可用于多种系统的内核。 我的主要兴趣是由Minimig核心提供的Amiga仿真。 我还对在大学期间使用过的Commodore 64和PET以及BBC微型计算机感兴趣。 我对[在Commodore PET上](https://www.youtube.com/watch?v=hqs6gIZbpxo)玩《 [太空侵略者](https://www.youtube.com/watch?v=hqs6gIZbpxo)》也情有独钟，这是我承认的（很多年后！）是我本周末在大学计算机实验室预订时间的真正原因。

加载内核后，您可以通过连接的键盘并通过按F12来访问“内核”菜单来与之交互。 要访问外壳程序，可以使用F9键登录，该键会为您显示一个登录提示。 您将需要一个[kickstart ROM](https://en.wikipedia.org/wiki/Kickstart_%28Amiga%29) （相当于PC的BIOS），以使Amiga运行。 您可以从[Cloanto](https://cloanto.com/) 获得这些文件， [Cloanto](https://cloanto.com/) 出售 [Amiga Forever](https://www.amigaforever.com/) kickstart，其中包含启动系统所需的ROM以及可以在MiSTer上使用的游戏，演示和硬盘文件。 将kickstart ROM存储在SD卡的根目录中，并将其命名为“ KICK.ROM”。

在我的MiSTer板上，即使我的Pi有更多的可用内存，我也可以运行不在Raspberry Pi上运行的Amiga演示。 仿真更加准确，运行效率更高。 通过扩展板，我什至可以使用旧硬件，例如原始的Commodore显示器和Amiga游戏杆。

### 源代码

MiSTer项目的所有代码均可在其 [GitHub存储库中找到](https://github.com/MiSTer-devel) 。 您可以访问核心以及主要的MiSTer设置，关联的脚本和菜单文件。 这些会被积极更新，并且有一个坚实的社区正在积极开发，错误修复和改善所有贡献，因此请定期检查更新。 该仓库具有大量可用信息，可帮助您启动和运行。

### 安全注意事项

定制的灵活性带来了潜在的 [安全漏洞](https://www.helpnetsecurity.com/2019/06/03/vulnerability-in-fpgas/) 。 所有MiSTer安装都在根帐户上附带了预设密码，因此，您要做的第一件事就是更改密码。 如果您使用该设备为游戏搭建机柜，并且已授予该设备访问网络的权限，则可以使用默认的登录凭据来利用该设备，从而可能导致第三方访问您的网络。

对于非MiSTer项目，FPGA公开了一个过程能够监听另一个过程的能力，因此限制对设备的访问应该是您要做的第一件事。 在构建应用程序时，应隔离进程以防止不必要的访问。 如果您打算将开发板部署在其他用户或共享应用程序可以访问的地方，则这一点尤其重要。

### 查找更多信息

在线上有很多有关此类项目的信息。 以下是一些您可能会有所帮助的资源。

 **社区** 
- [MiSTer维基](https://github.com/MiSTer-devel/Main_MiSTer/wiki)
- [设定指南](https://github.com/MiSTer-devel/Main_MiSTer/wiki/Setup-Guide)
- [支持核心上的Internet连接](https://github.com/MiSTer-devel/Main_MiSTer/wiki/Internet-and-console-connection-from-supported-cores)
- [讨论区](http://www.atari-forum.com/viewforum.php?f=117)
- [MiSTer附加组件](https://www.facebook.com/groups/251655042432052/) （公共Facebook组）

 **子板** 
- [SDRAM板](https://github.com/MiSTer-devel/Main_MiSTer/wiki/SDRAM-Board)
- [I / O板](https://github.com/MiSTer-devel/Main_MiSTer/wiki/IO-Board)
- [RTC板](https://github.com/MiSTer-devel/Main_MiSTer/wiki/RTC-board)
- [USB集线器](https://github.com/MiSTer-devel/Main_MiSTer/wiki/USB-Hub-daughter-board)

 **视频和演练** 
- [探索MiSTer和DE-10 Nano FPGA](https://www.youtube.com/watch?v=e5yPbzD-W-I) ：这是复古的未来吗？
- [Terasic DE10-Nano上的FPGA仿真MiSTer项目](https://www.youtube.com/watch?v=1jb8YPXc8DA)
- [FPGA上的Amiga OS 3.1-运行MisTer的DE10-Nano](https://www.youtube.com/watch?v=tAz8VRAv7ig)

 **在哪里购买硬件** 

### MiSTer项目

- [DE10-纳米](https://www.amazon.com/Terasic-Technologies-P0496-DE10-Nano-Kit/dp/B07B89YHSB/) （亚马逊）
- [终极先生](https://ultimatemister.com/)
- [MiSTer附加组件](https://misteraddons.com/)

### 其他FPGA

- [TinyFPGA BX —带USB的ICE40 FPGA开发板](https://www.adafruit.com/product/4038) （Adafruit）
- [Terasic](https://www.terasic.com.tw/en/) ，DE10-Nano和其他高性能FPGA的制造商

> 翻译自:  https://opensource.com/article/19/11/fpga-mister

# [开源FPGA硬件模拟游戏机，原汁原味的复古游戏体验带你回童年](https://www.qbitai.com/2021/04/22927.html)

梦晨 2021-04-14 13:50:00 来源：量子位

> 雅达利、红白机、世嘉MD、GBA，你能想到的都支持。

  - 梦晨 发自 凹非寺
  - 量子位 报道 | 公众号 QbitAI

近几年，游戏界兴起了一股复古风潮。

玩腻了追求极致画面表现，玩法上却千篇一律的“罐头大作”的人们，开始怀念童年记忆中那些简单的美好。

这款基于FPGA的硬件模拟游戏机MiSTer可以带你穿越到过去，找回童年的感觉。

![输入图片说明](https://foruda.gitee.com/images/1675152849344821276/757d70ce_5631341.gif "23771532660e45f794fb3e1158146a76.compressed.gif")

目前已支持雅达利，红白机，世嘉MD和GB、GBA等17种游戏主机与掌机。

![输入图片说明](https://foruda.gitee.com/images/1675152864135127131/b6debe3f_5631341.png "屏幕截图")

还有Apple II，Commodore 64，MSX等四十多种古董个人电脑。

![输入图片说明](https://foruda.gitee.com/images/1675152873446158358/41bc9ba2_5631341.png "屏幕截图")

> △运行MSX上的初代《合金装备》

这些主机的配置文件全都可以放在一起，随时切换。一台巴掌大小的机器，把你的童年全装进去。

与常见的各种模拟器软件不同，MiSTer使用硬件模拟方法，在电路的层面还原各种老游戏机的运行方式。

MiSTer是开源项目，软件部分可以免费下载，在GitHub上已获得1800星。

![输入图片说明](https://foruda.gitee.com/images/1675152908034291897/8efff9b4_5631341.png "屏幕截图")

硬件部分基于友晶科技的DE-10 Nano FPGA开发板，淘宝可以买到。

![输入图片说明](https://foruda.gitee.com/images/1675152922657246795/88e8cfc1_5631341.png "屏幕截图")

### 硬件模拟好在哪？

一般来说现在想玩老游戏有三种方法。

 **第一种方法** 是在二手市场淘换当年的原机原卡带，但是非常稀缺、价格昂贵，品相难以保证。

年代久远的游戏机只能输出模拟视频信号，需要更换芯片或用采集卡才能连接现代的数字显示器，会带来 **显示延迟** 。

![输入图片说明](https://foruda.gitee.com/images/1675152947411313074/2545898c_5631341.png "屏幕截图")

 **第二种方法** 是软件模拟，虽然硬件性能今非昔比，手机上都可运行许多模拟器，但有兼容性问题，不是所有游戏都能稳定运行。

使用软件模拟还会出现操作 **输入延迟** ，和 **声音延迟** 。

对高难度的动作游戏来说，虽然延迟只有毫秒级别，但还是能感觉到手感不同，老游戏难度普遍又高，输入延迟使一些高级技巧难以操作。

![输入图片说明](https://foruda.gitee.com/images/1675152983817355112/020ce58c_5631341.png "屏幕截图")

> △经典超难红白机游戏《魔界村》

对于音游来说，那就根本没法玩了。

![输入图片说明](https://foruda.gitee.com/images/1675153064383213125/2acd34df_5631341.gif "4bc8208834e6413cac1e6d382922703e.compressed.gif")

> △GBA上的音游《节奏天国》

现在市面上有一些基于树莓派等环境的开源游戏机，任天堂、世嘉等也不断地推出官方迷你复刻版。

![输入图片说明](https://foruda.gitee.com/images/1675153097386251475/6871befd_5631341.png "屏幕截图")

但这些本质上还是使用现代硬件架构的软件模拟，不能解决软件模拟带来的问题。如任天堂迷你FC实际上是在ARM架构上运行Linux系统。

![输入图片说明](https://foruda.gitee.com/images/1675153115859895423/5b5e3665_5631341.png "屏幕截图")

 **第三种方法** 就是使用FPGA硬件模拟。

FPGA的全称是  **现场可编程门阵列** ，通过直接对芯片中的模块和逻辑单元编程来模拟老游戏机硬件的运行方式。

![输入图片说明](https://foruda.gitee.com/images/1675153137211606913/883a0db2_5631341.png "屏幕截图")

软件模拟器是用CPU做通用计算，按顺序执行代码，需要比被模拟的硬件运行频率快许多倍的CPU才能达到原硬件的运行速度。

FPGA通过编程重组生成专用电路，相当于“可变形的硬件”。

可以让被模拟硬件的不同芯片同时工作，耗费的资源更少，同时解决延迟问题。

还可以模拟大型游戏卡带中特制的增强芯片，解决游戏兼容性问题。

![输入图片说明](https://foruda.gitee.com/images/1675153141836862291/5be4360e_5631341.png "屏幕截图")

> △SFC星际火狐中的增强芯片负责渲染3D多边形

以及模拟老机种的音频芯片输出原汁原味的游戏音效。

![输入图片说明](https://foruda.gitee.com/images/1675153154813075557/6b46e18e_5631341.png "屏幕截图")

此外，在测试中MiSTer输出的画面比原机清晰度更高，色彩也更鲜明。

### 购买安装使用

组装一台MiSTer需要一块DE10-Nano开发板，USB OTG Hub或为MiSTer特制的USB I/O板，和SD卡，推荐安装风扇增强散热，以及一个合适的外壳。

这些都可以从淘宝买到，全套价格在 **1700** 元左右，成本较高。

![输入图片说明](https://foruda.gitee.com/images/1675153187500473734/78f140d0_5631341.png "屏幕截图")

从GitHub下载整合好的安装镜像文件，刷入SD卡后接入DE10-Nano就可以自动安装了。

![输入图片说明](https://foruda.gitee.com/images/1675153195733567818/e2832134_5631341.png "屏幕截图")

具体步骤请参考文章末尾处的项目Wiki。

懒得动手的玩家也可以直接购买第三方制作好的成品，搜索“ **MiSTer FPGA** ”即可。

MiSTer支持鼠标、键盘、手柄与摇杆输入，甚至老游戏机专用的光枪等独特外设。

![输入图片说明](https://foruda.gitee.com/images/1675153211090178733/528521d2_5631341.png "屏幕截图")

软件方面，MiSTer运行的是定制的Linux系统，只加载了游戏机需要的模块，可以像老机一样瞬间开机。

游戏ROM直接拷在SD卡里。像MSX这种存档存在磁带里的老机也可以用软件方式解决。

MiSTer系统里提供作弊选项，截图，联网更新等功能。

还可以为液晶显示器添加模仿CRT的扫描线等滤镜，体验最正统的复古风格。

![输入图片说明](https://foruda.gitee.com/images/1675153224085631995/112b98aa_5631341.png "屏幕截图")

最后总结一下，玩老游戏用软件模拟器最方便省钱，官方复刻机适合买来收藏，注重游戏体验选择FPGA硬件模拟。

MiSTer项目Wiki： https://github.com/MiSTer-devel/Main_MiSTer/wiki

参考链接： [1] https://www.youtube.com/watch?v=y8bV0fOMYX0  
[2] https://en.wikipedia.org/wiki/List_of_Super_NES_enhancement_chips

---

![输入图片说明](https://foruda.gitee.com/images/1675311089687636398/ec4ba288_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1675311132940262733/9ba4e2b1_5631341.jpeg "7-77507_windows-95.jpg")

---

- [在MiSTer-FPGA运行 ao486从而引导Windows98启动](https://www.bilibili.com/video/BV1vv411y7P5)  
  地址：https://github.com/MiSTer-devel/Main_MiSTer/wiki

- [FPGA MiSTer + MT32-PI - AO486 内核 "SILPHEED" midi 音效](https://www.bilibili.com/video/av629954140)

- [MiSTer FPGA硬件模拟器初学者完全指南2022 第一节 概述 Overview 中英双语字幕](https://www.bilibili.com/video/BV1T44y1G7fJ)  
  https://www.youtube.com/watch?v=rhT6YYRH1EI&t=405s

  > MY LIFE IN GAMING的这一期真的太重磅了，对我的MiSTer入门给予了很大帮助。发布许久B站还没有人搬运，只能用我这渣翻来做个双语视频了。让我们一起享受复古游戏的乐趣。

# [MiSTer FPGA硬件模拟器初学者完全指南2022 第一节 概述 Overview 中英双语字幕](https://www.bilibili.com/video/BV1T44y1G7fJ)

The classic gaming landscape is just as much about new products, new devices, new ways
尽管在景点游戏领域，新产品、新设备以及针对老游戏的新玩法繁多到

to play as it is about old ones. But perhaps the one thing that has energized the core  
可以和经典游戏的数量相提并论。但是，在过去几年中，

enthuslasts of the classic gaming scene the most in the past several years has been the  
最能振奋经典游戏领域核心玩家的一件事可能就是 MiSTer FPGA 项目了。

MiSTer FPGA project. Thanks to an ever-growing group of insanely talented and passionate  
多亏了不断增长的一群才华横溢、激情四射的人，

（TRY @backloggery.com/Try4ce）

people, what began as a heavily Do-It-Yourself endeavor with a rather intimidating barrier  
把经典游戏玩法从具有巨大技术壁垒的 DIY 模式

of technical know-how has evolved into not only a relatively easy way to play games from  
转变成为了可以使用相对简单且稳定的硬件

a huge number of consoles, handhelds, classic computers, and arcade machines in a stable  
来玩大量的主机、掌上设备、老式电脑以及街机上的游戏，

hardware-driven processing environment, but may also be our best shot at preserving as  
并且这也可能是我们在未来几十年

accurate of information as possible about the functionality of these old platforms for  
乃至几百年内尽可能准确地保存

decades and centuries yet to come.  
这些旧平台游戏的最佳途径。

Yikes, though... this video is kinda long, isn't it? But don't worry - it's not  
不过，这段视频确实有点长了，但你不要担心，这并不是因为 MiSTer 很难，

because MiSTer is difficult, it's because there is so much you can do in MiSTer and  
而是因为在 MiSTer 上你可以做很多很多事情，并且我们也有很多需要向你展示。

there's so much to show. So if you've ever felt a bit afraid of jumping into MiSTer,  
如果你曾经对 MiSTer 是否值得入手而感到犹豫不决；

trust me, I get it, we've been there too, and we think you'll see that it's totally  
相信我，我也曾经有过那种想法。但是如果你一旦尝试它，

something you can use too if you can get into a can-do mindset. In this episode we'll  
你就会发现你完全可以得心应手的使用它。

be taking a look at the parts that make up MiSTer, how to get it initialized, get the  
在这一集中，我们将讲述 MiSTer 的组建，

output looking great, and what's exciting about the current slate of platforms that  
如何初始化，如何输出更好的画面，

are available to play on it as of right now in early 2022.  
以及 2022 年初可以在这个平台上使用的一系列令人兴奋的新功能。

[Music: Principle by Matt McCheskey]

PART 1 MiSTer Overview

Fans of vintage video games are truly living in an incredible era. We've seen some amazing  
复古视频游戏的粉丝们真的生活在一个不可思议的时代。

revivals of classic series on new systems, while the original hardware these games  
我们看到了经典游戏硬件系统的复兴，

were played on have been getting their own power-ups in the form of HDMI mods, flash  
复古游戏主机已经通过 HDMI 外设、烧录卡、

carts, wireless controllers and more accessories.  
无线手柄、以及更多配件的形式获得了强化。

And then we have emulation. Those looking for a dose of nostalgia can grab a mini console  
随后我们有了模拟器。那些想要怀旧的玩家

from their local big box store to get a quick fix or dive a little deeper using cycle accurate  
可以在他们家附近的大型超市里面买一台迷你复刻主机，

PC emulators or an all-in-one mini computer like a Raspberry Pi.  
也可以用个人电脑运行模拟器，甚至可以使用一些小型的一体机比如树莓派。

But thanks to the commercial success of the AVS from RetroUSB and the Nt Mini, Super Nt  
多亏了 RetroUSB 的 AVS、Nt Mini、Super Nt

（COURY @Backloggery.com/couryc）

and Mega Sg from Analogue, consoles that are built around a Field Programmable Gate Array
和 Mega Sg 在商业上的成功，基于可编程门阵列（FPGA）

or FPGA, have been making the most noise among hardcore enthusiasts while also breaking  
的复古主机系统获得了核心玩家的关注并且也吸引了普通玩家

to casual fans.  
的兴趣。

Using a schematic called a "core," an FPGA can be reprogrammed to emulate another  
使用一个称为 “核心” 的硬件原理图，可以对FPGA进行重新编程，让它来以模拟另一块硬件。

piece of hardware. This differs from the emulators you see on mini consoles or a Raspberry Pi  
这与我们在迷你复刻主机以及

which creates a software environment for a game to run in. Software emulation can more  
树莓派上运行软件模拟环境完全不同。

easily replicate relatively recent systems but a key distinction is that hardware-based  
软件模拟器可以比较容易的完成对游戏主机的近似模拟，但是与硬件模拟的最大区别在于

emulation can perform tasks in parallel like original hardware, rather than sequentially.  
硬件模拟可以使游戏进程像原始硬件系统一样并行执行，而不是软件模拟时的顺序执行。

This makes it easier to minimize latency in all aspects and to match original hardware  
这使得硬件模拟可以从控制器输入、视频帧率输出以及声音效果等

behaviors. from audio processing to frame delivery to controller inputs. It might be  
各个层面上尽量降低延迟，使其更加接近实机表现。

tempting to say that one approach is better than the other, but the fact is they're  
可以说这种硬件模拟方式比软件模拟要优秀

different avenues to achieve the same goal... and how successful they are comes down to  
但事实上，他们只是实现同一目标的不同途径。

the talents and dedication of the programmers working with them, and the resources they  
它们的成功与否取决于程序员的灵感、奉献精神、

have availabel to them... as well as the capabilities of the machines running the emulation.  
投入的资源以及运行它们的具体硬件都有一定的关系。

The MiSTer FPGA project began its rise to prominence in the classic gaming scene in  
MiSTer FPGA 项目于 2017 年6月开始在经典游戏领域崭露头角。

June of 2017. Alexey Melnikov, known to most as Sorglig or Sorg, was a key figure in the  
Alexey Melnikov 是 MiST FPGA 项目长期开发的关键人物，

（ALEXEY MELNIKOV @github.com/sorgelig）

long running development of the MiST FPGA Computer project. Frustrated by it's analog  
他被大多数人成为 Sorglig 或 Sorg。

only video output, Sorg sought to port many of the cores to more modern hardware which  
Sorg 对经典主机仅能运行模拟视频输出感到失望，于是试图将许多内核移植到

wouldn't be hindered by this limitation. He selected the DE-10 nano as the vehicle  
没有这种限制的现代硬件上。他选择了DE-10 nano 作为该项目的载体，

for the project, a development board which utilized a Cyclone V FPGA in tandem with a   
这是一个开发板，它板载了 Cyclone V FPGA,

number of inputs and outputs like HDMI and USB ports along with plenty of room to expand  
以及HDMI和USB 等大量输入和输出接口，并提供了充足的扩展插槽。

as needed. Because the DE-10 is primarily aimed at engineering students or developers  
DE-10 主要面向低级设备的工程专业学生

looking to implement some sort of low level device, it's been mass produced - bringing  
或开发人员，它已经批量生产，

the cost down significantly. Although chip shortages due to global pandemics have caused  
尽管全球疫情的大流行导致芯片短缺，致使价格波动较大，但这种开发板的获取成本

the price to fulctuate.  
还是比较低廉的。

Development on the MiSTer was steadily on the rise before hitting a fever pitch in 2020  
MiSTer 的开发平稳发展直至2020年和2021年之间达到高峰

and 2021. Developers from all walks of Life have reverse engineered and ported a staggering  
各行各业的开发者开始进行了逆向工程，

number of consoles, handhelds, microcomputers, and arcade games to the MiSTer. If it came  
将数量惊人的主机、手持设备、微型计算机和街机游戏移植到 MiSTer 上。

out before 1995, cances are it's availabel or at least in the works to some extent.  
1995年以前发布的游戏基本上都是可运行的，或者是至少在某种程度上正在开发之中。

Before we really get down to business, I need to be blunt. The MiSTer is an open source  
在我们开始谈正事之前，我要直言不讳一下。 MiSTer 是一个开源的 DIY 项目。

DIY project with a super active scene that has innumerable moving parts. Straight up,  
有一个超级活跃的应用场景以及大量的可更换部件。

things can rapidly change on a day to day basis which could render some information  
坦白地说，这个项目可以说每天都在迅速发展变化，这可能会使视频中的一些信息

in this video out of date within minutes of posting it. The MiSTer is capable of so much  
在发布后的几分钟内就过时了。MiSTer 有很多很酷的应用场景，

cool stuff, and there's no way we can cover everything in this video if we value our sanity.
我们不可能在这个视频中涵盖方方面面。

So, In this video we'll be going over the hardware and essential processes you'll  
所以，在本视频中，我们将展示 

need to know in order to get going with MiSTer. But in order to avoid overwhelming anyone  
你需要了解的 MiSTer 硬件使用的基本流程。但是为了避免一下子给大家带来过多的

with technical information, we'll be taking periodic breaks to shine a spotlight on the  
技术情报，我们将定期插入一些 MiSTer 目前支持的

various platforms that the MiSTer currently supports. By the time you reach the end, you'll  
各种平台的游戏信息作为放松缓解。当你看到最后的时候，

hopefully have a good idea if MiSTer is for you, and how to wrangle it. Because, that's  
你会知道 MiSTer 是否适合你， 以及如何理解这项计划。 因为，这才是

really the key here - while I'm willing to bet the MiSTer is much easier to build  
我要说的关键点——虽然我敢打赌， MiSTer 比您想象的更容易构建和使用，

and use than you think, you'll still need to be an active participant if you want to   
但如果你想要获得更大的收益，你仍然需要投入更大的精力

get the most out of it.
去研究它。

And listen... there are a LOT of cores, so we'd be here for at least another 15 hours  
MiSTer 有很多核心，所以如果我们对每个核心进行全面的介绍，

if we gave each and every one the full deep dive treatment. This is just an overview of  
至少还要花费 15 个小时。 我们做的是对每个核心的概述，

each core that will hopefully answer some of your questions in terms of what kinds of  
希望能够解答你关于哪些类型的游戏是可玩的疑问

games are playable and some of the interesting features on offer. But the cores are always  
以及进行一些有趣功能介绍。但是核心更新的很频繁，

（OFFICAL DOCUMENTATION @[mister-devel.github.io/MkDocs_MiSTer](https://mister-devel.github.io/MkDocs_MiSTer/)）

evolving, so I highly recommend checking out each core's GitHub page to get the latest  
所以我强烈建议你查看每个核心的 GitHub 页面，

information on features and setup.  
以获得关于功能和设置的最新信息。

[![输入图片说明](https://foruda.gitee.com/images/1675394599389079833/4b7018f3_5631341.png "logo_small.png")](https://mister-devel.github.io/MkDocs_MiSTer/)
