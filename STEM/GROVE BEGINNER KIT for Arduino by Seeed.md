- [GROVE BEGINNER KIT for Arduino](https://wiki.seeedstudio.com/cn/Grove-Beginner-Kit-For-Arduino/) by [Seeed](https://www.seeedstudio.com/about-us/)
- Seeeduino XIAO | [Starter Kit for Seeed XIAO](https://gitee.com/siger99/Wifi-Oximiter-using-MAX30102-and-ESP32/issues/I6I7T5#note_17581240_link)  | [课程](https://gitee.com/siger99/Wifi-Oximiter-using-MAX30102-and-ESP32/issues/I6I7T5#note_17581251_link)
- [用 Codecraft 玩转 Grove 极简 Arduino 入门套件](https://gitee.com/siger99/Wifi-Oximiter-using-MAX30102-and-ESP32/issues/I6I7T5#note_17581283_link)
- [【评测】Seeed为Arduino推出的新Grove初学者套件：可以说是最简单的入门方式](https://techexplorations.com/blog/arduino/seeeds-grove-beginner-kit-for-arduino-percect-for-beginners/)
- [【开箱】拼装式开发板到底是营销概念还是真香？Grove Beginner Kit For Arduino 开箱初印象](https://www.bilibili.com/video/BV1JV411s74u)

<p><img width="706px" src="https://foruda.gitee.com/images/1677529136433874353/6d38dfaf_5631341.jpeg" title="567GROVE.jpg"></p>

> 当我还焦虑给学生上课时，各种连线故障，元件缺失的问题时，早在2年前 GROVE BEGINNER KIT for Arduino by Seeed 就已经创生了，通过其他老师推荐，经过考古，原来它与风靡全国的创客运动的发源地之一 “柴火空间” 联系起来的。这就是轮回，我在被拉入创客行列的2015年还造访过，而《人工智能与开源》的蓝本也是创客，今天显然不是追忆故事的，当创新被赋能给每一个人的时候，“电子也可以是艺术的！”，这里借用 [中国深圳成立Seeed工作室](#里程碑) 的引语，向这件艺术品的作者致敬 :pray: 更多引语参见 [About](#关于我们).

![输入图片说明](https://foruda.gitee.com/images/1677523080399400154/d32b911b_5631341.png "屏幕截图")

https://wiki.seeedstudio.com/cn/Grove-Beginner-Kit-For-Arduino/

太帅了，没有之一！ 

<h2 id="_8">制作自己的模块和开发板<a class="headerlink" href="#_8" title="Permanent link">¶</a></h2>
<p>在这段学习之后，您已经对Arduino和开源硬件有了系统的了解，那么为什么不继续尝试制作自己的模块或开发板呢？</p>
<h3 id="eda">EDA<a class="headerlink" href="#eda" title="Permanent link">¶</a></h3>
<p>要设计自己的电路板，您将需要设计自己的模块原理图，这需要使用EDA工具。这里推荐一个开源的EDA软件。</p>
<ul>
<li><strong>KiCAD</strong></li>
</ul>
<p><a href="https://www.kicad-pcb.org/">KiCad</a>是用于电子设计自动化的免费软件套件. 它促进了电子电路原理图的设计及其到PCB设计的转换。它拥有用于原理图捕获和PCB布局设计的集成环境。程序通过输出Gerber格式来处理原理图捕获和PCB布局。 该套件可在Windows、Linux和macOS上运行，且获得了GNU GPL v3的许可。</p>
<ul>
<li><strong>Geppetto</strong></li>
</ul>
<p>如果您不想自己设计原理图或布局，但是想将基于Seeed模块的原型转换为集成产品，我们强烈建议您尝试使用Geppetto。</p>
<p><a href="www.seeedstudio.com/geppetto">Geppetto</a>)是目前生产高质量电子产品最简单最划算的方法。 您无需了解电阻器、电容器、电感器、路由路径或任何电子工程的相关知识即可设计自己的模块。 Geppetto简单易用，任何人都可以创建专业的IoT设备。为了使您更方便，Geppeto拥有一个Seeed库，因此您可以轻松设计自己的Grove模块！</p>
<h3 id="pcb">PCB服务<a class="headerlink" href="#pcb" title="Permanent link">¶</a></h3>
<p>在完成你的设计后，访问<a href="https://www.seeedstudio.com/fusion_pcb.html">Seeed Fusion PCB Assembly (PCBA) Service</a>来将你的设计转化为有实际功能的设备吧！</p>
<p><img alt="" src="https://files.seeedstudio.com/wiki/Grove-Beginner-Kit-For-Arduino/img/Fusion.png"></p>
<p>Seeed Studio拥有自己的<a href="https://www.seeedstudio.com/opl.html">Open Parts Library (OPL)</a>，这个库是专门为Seeed Fusion PCBA service采购的10,000多个常用组件的集合。 为了加快PCB设计过程，Seeed正在为KiCad和Eagle建立组件库。当所有组件均来自Seeed的PCBA OPL并搭配<a href="https://www.seeedstudio.com/fusion_pcb.html">Seeed Fusion PCBA service</a> ，整个PCBA的生产时间可以从20个工作日减少到7天。</p>
<h2 id="_9">在线原理图<a class="headerlink" href="#_9" title="Permanent link">¶</a></h2>

![输入图片说明](https://foruda.gitee.com/images/1677527268000579736/e51134ea_5631341.jpeg "Grove Beginner Kit for Arduino SCH.jpg")

<h2 id="_10">相关资料<a class="headerlink" href="#_10" title="Permanent link">¶</a></h2>
<ol>
<li>
<p><a href="https://files.seeedstudio.com/wiki/Grove-Beginner-Kit-For-Arduino/res/Grove-Beginner-Kit-For-Arduino(CN).pdf"><strong>Grove极简入门套件Wiki [PDF]</strong></a></p>
</li>
<li>
<p><a href="https://files.seeedstudio.com/wiki/Grove-Beginner-Kit-For-Arduino/res/Grove-Beginner-Kit-for-Arduino-SCH-PCB.zip"><strong>Grove极简入门套件原理图设计文件</strong></a></p>
</li>
<li>
<p><strong>Github模块库:</strong></p>
<ul>
<li><a href="https://github.com/olikraus/U8g2_Arduino">OLED显示</a></li>
<li><a href="https://github.com/Seeed-Studio/Grove_Temperature_And_Humidity_Sensor">温度&amp;湿度传感器</a></li>
<li><a href="https://github.com/Seeed-Studio/Grove_BMP280">气压传感器</a></li>
<li><a href="https://github.com/Seeed-Studio/Seeed_Arduino_LIS3DHTR">3轴加速度计</a></li>
</ul>
</li>
<li>
<p><a href="https://files.seeedstudio.com/wiki/Grove-Beginner-Kit-For-Arduino/res/Grove-beginner-kit-for-arduino-datasheet.zip"><strong>传感器数据手册</strong></a></p>
</li>
<li>
<p><a href="https://files.seeedstudio.com/wiki/Grove-Beginner-Kit-For-Arduino/res/GroveBeginnerKitFirmwareFINAL.zip"><strong>初始Arduino固件</strong></a></p>
</li>
<li>
<p><a href="https://files.seeedstudio.com/wiki/Grove-Beginner-Kit-For-Arduino/res/Grove-Beginner-Kit-For-Arduino-Resources-in-one(20200401).7z"><strong>Grove极简入门套件USB资料(20200401)[7z]</strong></a></p>
</li>
</ol>
<h2 id="_11">其他学习资料<a class="headerlink" href="#_11" title="Permanent link">¶</a></h2>
<ul>
<li><a href="https://github.com/256ericpan/LSTM_IoT">LSTM用于实时物联网数据预测</a></li>
<li><a href="https://wiki.seeedstudio.com/cn/Mixly-Grove-Beginner-Kit">Grove Beginner Kit 在 Mlxly 平台上使用</a></li>
</ul>
<h2 id="_12">技术支持<a class="headerlink" href="#_12" title="Permanent link">¶</a></h2>
<p>请您不要犹豫，来我们的<a href="http://forum.seeedstudio.com/">论坛</a>提出问题吧！<br></p><p style="text-align:center"><a href="https://www.seeedstudio.com/act-4.html?utm_source=wiki&amp;utm_medium=wikibanner&amp;utm_campaign=newproducts" target="_blank"><img src="https://files.seeedstudio.com/wiki/Wiki_Banner/new_product.jpg"></a></p><p></p>

# [关于我们](https://www.seeedstudio.com/about-us/)

<img align="right" src="https://foruda.gitee.com/images/1682157608752402337/8cd54f64_5631341.png">

“柴火創客空間則是深圳第1大創客空間。Seeed Studio推動的是讓科技可以隨手可得，創意良好生長。幾個月就能產品迭代，甚至12天就可以做出一個樣品，過程比以前縮短很多，這就是開源的力量，因為共用了設計跟供應鏈。 ”
- BUSINESS NEXT

<img align="right" src="https://foruda.gitee.com/images/1682157628147977287/2508d4d9_5631341.png">

“Seeed Studio, a startup based in Shenzhen, specialises in open-source hardware. But Seeed goes one step further, supporting a whole ecosystem of open-source production. People pitch ideas on its website, and if they garner enough community support, Seeed will manufacture them.”
- The Economist

<img align="right" src="https://foruda.gitee.com/images/1682157647577463958/939ede34_5631341.png">

Cool Vendors in Asia/Pacific Cool Vendors in IoT ‘Thingification’—— Few IT organizations are equipped to develop the hardware and software for low-level sense and control devices. Gartner provides guidelines for IT leaders looking to seed and accelerate IoT plans. Maker movement suppliers enable organizations to implement IoT concepts in demonstrable form.
- Gartner

<img align="right" src="https://foruda.gitee.com/images/1682157659296902357/65d4fce9_5631341.png">

“Wer ein neues Gerät oder ein digitales Gadget, ein besonderes Design im Sinn hat, dem stellt „Seeed“ die Ausrüstung be- reit, um einen Prototypen zu bauen. Wenn er weiter vorankommt, vermittelt ihn das Start-up an einen Betrieb, der eine Klein- serie herstellen kann, wenn die erfolgreich ist, an eine Fabrik, die das Gerät in Massen produziert. ”
- DER SPIEGEL

<img align="right" src="https://foruda.gitee.com/images/1682157675881216848/ba5d0637_5631341.png">

“Seeed（深圳矽递科技有限公司）は、プリント基板や電子パーツを顧客の依頼に応じて小規模から生産している。この企業のユニークなところは、自社製品のデータを外部に公開する、いわゆるオープンソース化を進めていることだ。オリジナルの技術を特許などで保護せず、自由にコピー・改良することを認めることでイノベーションを進めていこう、という先進的な考えを反映している。 ”
- Nippon

<img align="right" src="https://foruda.gitee.com/images/1682157686030686852/1d695cda_5631341.png">

“这家2008年在深圳创立的企业迅速发展。目前矽递科技已经是全球前三大开源硬件制造商之一（其他两家为美国企业），服务的客户95%来自海外。 ”
- Nanfang Daily

<img align="right" src="https://foruda.gitee.com/images/1682157696739109571/a4376bd2_5631341.png">

“柴火創客空間則是深圳第1大創客空間。Seeed Studio推動的是讓科技可以隨手可得，創意良好生長。幾個月就能產品迭代，甚至12天就可以做出一個樣品，過程比以前縮短很多，這就是開源的力量，因為共用了設計跟供應鏈。 ”
- BUSINESS NEXT

<img align="right" src="https://foruda.gitee.com/images/1682157707748286083/a8b06740_5631341.png">

“Seeed Studio, a startup based in Shenzhen, specialises in open-source hardware. But Seeed goes one step further, supporting a whole ecosystem of open-source production. People pitch ideas on its website, and if they garner enough community support, Seeed will manufacture them.”
- The Economist


### 里程碑

| date | img | title | contents |
|---|---|---|---|
| 2008.6.18 | ![输入图片说明](https://foruda.gitee.com/images/1682152923324549913/19a7cf9c_5631341.png "屏幕截图") | 在中国深圳成立Seeed工作室 | 相信“电子也可以是艺术”
| 2008.9.15 | ![输入图片说明](https://foruda.gitee.com/images/1682152924493628369/fd4b2fe6_5631341.png "屏幕截图") | 发布“Seeeduino” | 微控制器兼容Arduino
| 2009.8.13 | ![输入图片说明](https://foruda.gitee.com/images/1682152935617597818/78b853d3_5631341.png "屏幕截图") | 推出了“传播” | 在线原型和定制服务
| 2009.9.10 | ![输入图片说明](https://foruda.gitee.com/images/1682152946829980809/2d6bae3b_5631341.png "屏幕截图") | 发布“DSO” | 第一个与Maker Pro合作的产品
| 2010.7.20 | ![输入图片说明](https://foruda.gitee.com/images/1682152953383780410/5fa6e8d7_5631341.png "屏幕截图") | 与“RF Explore”合作 | 提供制造和分销服务
| 2010.10.16 | ![输入图片说明](https://foruda.gitee.com/images/1682152960941520757/dcd37503_5631341.png "屏幕截图") | 发布“Grove System” | 开放式模块化系统设计，可轻松连接任何输入/输出模块到微处理器
| 2010.12.24 | ![输入图片说明](https://foruda.gitee.com/images/1682152970048417259/d604132f_5631341.png "屏幕截图") | 推出“开放零件图书馆” | 常用的、具有成本效益的和dfm友好的组件的集合
| 2011.5.4 | ![输入图片说明](https://foruda.gitee.com/images/1682152976103618828/2968bb21_5631341.png "屏幕截图") | 推出“融合原型” | PCB制造、PCB组装、其他电子、机械定制一条龙原型服务
| 2011.11.30 | ![输入图片说明](https://foruda.gitee.com/images/1682152982158957696/4bf42d17_5631341.png "屏幕截图") | 创立柴火创客空间 | 深圳首个创客空间
| 2012.4.8 | ![输入图片说明](https://foruda.gitee.com/images/1682152988232898176/a4d8f185_5631341.png "屏幕截图") | 推出“创客嘉年华深圳” | 中国首届创客大会
| 2012.5.29 | ![输入图片说明](https://foruda.gitee.com/images/1682152994090025889/fd36fe7b_5631341.png "屏幕截图") | 发布第一套“Grove入门套件” | 教育套件与格罗夫模块初学者
| 2013.3.20 | ![输入图片说明](https://foruda.gitee.com/images/1682153010573926402/d396376d_5631341.png "屏幕截图") | 《福布斯》中国版封面文章 | Seeed创始人兼首席执行官潘立宏先生作为封面故事
| 2013.4.25 | ![输入图片说明](https://foruda.gitee.com/images/1682153007341370017/2a86f01e_5631341.png "屏幕截图") | 与“bitfrenzy”合作推出“crazyfly” | 用于研究和教育的小型和多功能四轴飞行器
| 2013.9.6 | ![输入图片说明](https://foruda.gitee.com/images/1682153059328166603/6b1bcf2d_5631341.png "屏幕截图") | 主办“2013开放硬件峰会” | 作为Marquee赞助商，支持不断增长的开源社区
| 2014.9.8 | ![输入图片说明](https://foruda.gitee.com/images/1682153026559021531/93f3c15b_5631341.png "屏幕截图") | 联发科发布LinkIt ONE | 用于可穿戴设备和物联网设备原型的开源高性能板
| 2014.10.31 | ![输入图片说明](https://foruda.gitee.com/images/1682161812246580612/3597d44c_5631341.png "屏幕截图") | 发布Intel®Edison Kit for Arduino与Intel合作 | 一个工具包允许制造商应用英特尔爱迪生与Arduino屏蔽接口
| 2014.11.11 | ![输入图片说明](https://foruda.gitee.com/images/1682153040290786010/5e9c7ed6_5631341.png "屏幕截图") | 推出Fusion Gallery | 一个平台，Fusion客户可以共享和开源他们的PCB设计
| 2015.1.5 | ![输入图片说明](https://foruda.gitee.com/images/1682153049806042492/29002118_5631341.png "屏幕截图") | 双创领导视察柴火创客空间 | 激励中国官方在全国范围内扩大创客运动
| 2015.5.19 | ![输入图片说明](https://foruda.gitee.com/images/1682153059057469858/561870ac_5631341.png "屏幕截图") | 成立Seeed Studio美国分公司 | 为不断增长的美国市场提供本地支持
| 2015.8.14 | ![输入图片说明](https://foruda.gitee.com/images/1682153069039610982/5c76c7e4_5631341.png "屏幕截图") | 与TI和BeagleBoard.org合作发布“BeagleBone Green” | 基于BeagleBone®Black的低成本、开源、社区支持的开发平台
| 2015.9.23 | ![输入图片说明](https://foruda.gitee.com/images/1682153082951923341/889565ed_5631341.png "屏幕截图") | 发布“RePhone” | 世界上第一个开源模块化手机工具包
| 2015.11.24 | ![输入图片说明](https://foruda.gitee.com/images/1682161783452533225/abaffbdc_5631341.png "屏幕截图") | 成立“TinkerGen” | Seeed Studio的教育品牌
| 2015.12.24 | ![输入图片说明](https://foruda.gitee.com/images/1682153107719877019/dc805247_5631341.png "屏幕截图") | 与“Piper”合作 | 提供制造和分销服务
| 2016.5.24 | ![输入图片说明](https://foruda.gitee.com/images/1682153096834830238/edfc1f4c_5631341.png "屏幕截图") | 发布“Wio Link” | Wi-fi解决方案将物联网开发简化为物联网
| 2016.6.7 | ![输入图片说明](https://foruda.gitee.com/images/1682153104623052455/7ba07e65_5631341.png "屏幕截图") | 在《连线》的纪录片中出现 | “深圳:硬件的硅谷”
| 2016.8.24 | ![输入图片说明](https://foruda.gitee.com/images/1682153115940341073/ad6db36b_5631341.png "屏幕截图") | 发布“ReSpeaker” | 物联网应用的首个开放语音接口
| 2016.12.24 | ![输入图片说明](https://foruda.gitee.com/images/1682153119441853744/6927f665_5631341.png "屏幕截图") | 宣布与“微软”进行全球合作 | 产品开发、软件协作和创新生态系统
| 2017.8.3 | ![输入图片说明](https://foruda.gitee.com/images/1682153125610380491/8c5ec4f7_5631341.png "屏幕截图") | 在日本名古屋成立Seeed Studio分公司 | Seeed KK: https://www.seeed.co.jp/
| 2017.12.24 | ![输入图片说明](https://foruda.gitee.com/images/1682153132236674537/530776d0_5631341.png "屏幕截图") | ISO 9001:2015认证 | 最高标准的质量管理体系
| 2017.12.25 | ![输入图片说明](https://foruda.gitee.com/images/1682153138087466658/de6e22ae_5631341.png "屏幕截图") | 柴火创客空间升级 | 作为“产业转型平台”
| 2018.5.15 | ![输入图片说明](https://foruda.gitee.com/images/1682153144710391023/1f518c4e_5631341.png "屏幕截图") | 发起了一项名为“为地球而看”的在线竞赛 | 促进全球社区为实现可持续发展目标应用开放技术解决方案
| 2018.10.22 | ![输入图片说明](https://foruda.gitee.com/images/1682153152696523174/6c0d4f7e_5631341.png "屏幕截图") | 成立柴火创客空间中国河北省分公司 | 注重运用新兴技术解决当地生态问题
| 2018.11.23 | ![输入图片说明](https://foruda.gitee.com/images/1682167210284998814/4a270c71_5631341.png "屏幕截图") | 宣布与“ADI”合作 | 提供定制和制造服务
| 2018.12.24 | ![输入图片说明](https://foruda.gitee.com/images/1682153166670753391/d047006a_5631341.png "屏幕截图") | 发布“SenseCAP系列” | 工业物联网产品系列，包括硬件、软件和API，适用于各类无线环境传感应用:智慧农业、智慧畜牧业、智慧城市等。
| 2018.12.25 | ![输入图片说明](https://foruda.gitee.com/images/1682153173531716086/1b5967b9_5631341.png "屏幕截图") | 发布“Codecraft” | 图形化编程平台
| 2018.12.26 | ![输入图片说明](https://foruda.gitee.com/images/1682153183682989327/0890eebe_5631341.png "屏幕截图") | 与美国印刷厂合作“跳码者”项目 | 为有特殊需要的儿童提供包容性编码和编程教育
| 2019.1.29 | ![输入图片说明](https://foruda.gitee.com/images/1682153187223803518/ac7eeb7d_5631341.png "屏幕截图") | 发布“Wio终端” | 完整的嵌入式机器学习和物联网人工智能平台
| 2020.1.8 | ![输入图片说明](https://foruda.gitee.com/images/1682161759557345103/b049ea7e_5631341.png "屏幕截图") | 发行《Seeeduino XIAO》 | Seeeduino家族中最小的arduino兼容微控制器，由Microchip SAMD21驱动
| 2020.2.24 | ![输入图片说明](https://foruda.gitee.com/images/1682153203441268157/fedc18af_5631341.png "屏幕截图") | 发布“验算” | 一系列用于边缘计算应用的单板计算机
| 2020.4.9 | ![输入图片说明](https://foruda.gitee.com/images/1682153207241291746/5f47c1e6_5631341.png "屏幕截图") | 发布“Arduino全能Grove初学者工具包” | arduino兼容套件，带有10个Grove传感器
| 2021.3.16 | ![输入图片说明](https://foruda.gitee.com/images/1682153213373093173/c4f76edc_5631341.png "屏幕截图") | 与微软和FFA合作推出“微软学生农场节拍套件” | 硬件、软件、免费课程和活动，让学生学习关于智能农业的人工智能、机器学习、物联网和数据科学
| 2021.4.27 | ![输入图片说明](https://foruda.gitee.com/images/1682153217924635271/adfd0ea3_5631341.png "屏幕截图") | 发布“reTerminal” | 由树莓派计算模块4驱动的下一代人机界面
| 2021.6.24 | ![输入图片说明](https://foruda.gitee.com/images/1682153225879386243/ccb8dfc1_5631341.png "屏幕截图") | 发布“SenseCAP M1” | 高性能，即用的LoRaWAN室内网关由Helium Network供电
| 2021.6.3 | ![输入图片说明](https://foruda.gitee.com/images/1682153231607428955/89db03ef_5631341.png "屏幕截图") | 发布的“储备” | 紧凑而强大的边缘计算系统，适用于各种应用
| 2021.7.1 | ![输入图片说明](https://foruda.gitee.com/images/1682153237571732007/8f09cb7a_5631341.png "屏幕截图") | 被授权为树莓派认可的设计合作伙伴 | 可支持工业应用的精选硬件和软件设计公司
| 2021.7.23 | ![输入图片说明](https://foruda.gitee.com/images/1682153243665732542/1a4e164c_5631341.png "屏幕截图") | 与tinyML重新推出“Codecraft” | 嵌入式机器学习图形化编程平台

<h1>Seeeduino XIAO</h1>
                
<p><img alt="" src="https://files.seeedstudio.com/wiki/Seeeduino-XIAO/img/Seeeduino-XIAO-preview-1.jpg"></p>
<p>Seeeduino XIAO是Seeeduino家族中的最小成员。它使用了功能强大却低功耗的微控制器——ATSAMD21G18A-MU。这意味该小板在处理方面具有良好的性能，但需要的功率更少。由于它的设计尺寸很小，它可以被灵活运用于多种场景，特别是可穿戴设备和小型项目。</p>
<p>Seeeduino XIAO具有14个通用输入输出接口（GPIO），可用作11个数字接口，11个模拟接口，10个PWM接口（d1-d10），1个DAC输出引脚D0、1个SWD焊盘接口，1个I2C接口，1个SPI接口，1个UART 接口，串行通信指示灯（T/R），闪烁指示灯（L）。LED的颜色为绿色、黄色、蓝色和蓝色分别对应电源、L、RX和TX。此外，Seeeduino XIAO具有Type-C接口，用于提供电源和数据传输。板上有两个重置按钮，您可以短接它们来重置板子。</p>
<p style=":center"><a href="https://detail.tmall.com/item.htm?spm=a21ag.12100459.0.0.6b4050a5T3bfrc&amp;id=612336208350" target="_blank"><img src="https://files.seeedstudio.com/wiki/wiki_chinese/docs/images/click_to_buy.PNG"></a></p>

<h2 id="_1">产品特性<a class="headerlink" href="#_1" title="Permanent link">¶</a></h2>
<ul>
<li>强大的CPU: ARM® Cortex®-M0+ 32bit 48MHz微控制器(SAMD21G18)，256KB Flash,32KB SRAM.</li>
<li>灵活的兼容性：与Arduino IDE兼容。</li>
<li>易于项目操作：可与面包板兼容。</li>
<li>体积小：适用于可穿戴设备和小型项目，小至拇指大小（20x17.5mm）。</li>
<li>多种开发接口：11个数字/模拟引脚，10个PWM引脚，1个DAC输出，1个SWD焊盘接口，1个I2C接口，1个UART接口，1个SPI接口。</li>
</ul>
<h2 id="_2">规格参数<a class="headerlink" href="#_2" title="Permanent link">¶</a></h2>
<div class="md-typeset__scrollwrap"><div class="md-typeset__table"><table>
<thead>
<tr>
<th>项目</th>
<th>数值</th>
</tr>
</thead>
<tbody>
<tr>
<td>CPU</td>
<td>ARM Cortex-M0+ CPU(SAMD21G18)，运行频率高达48MHz</td>
</tr>
<tr>
<td>Flash存储</td>
<td>256KB</td>
</tr>
<tr>
<td>SRAM</td>
<td>32KB</td>
</tr>
<tr>
<td>数字I/O引脚</td>
<td>11</td>
</tr>
<tr>
<td>模拟I/O引脚</td>
<td>11</td>
</tr>
<tr>
<td>I2C接口</td>
<td>1</td>
</tr>
<tr>
<td>SPI接口</td>
<td>1</td>
</tr>
<tr>
<td>UART接口</td>
<td>1</td>
</tr>
<tr>
<td>电源和数据传输接口</td>
<td>Type-C</td>
</tr>
<tr>
<td>电压</td>
<td>3.3V/5V 直流</td>
</tr>
<tr>
<td>尺寸</td>
<td>20×17.5×3.5mm</td>
</tr>
</tbody>
</table></div></div>
<h2 id="_3">硬件概述<a class="headerlink" href="#_3" title="Permanent link">¶</a></h2>
<p><img alt="" src="https://files.seeedstudio.com/wiki/Seeeduino-XIAO/img/Seeeduino-XIAO-pinout.jpg"></p>
<p><img alt="" src="https://files.seeedstudio.com/wiki/Seeeduino-XIAO/img/Seeeduino%20XIAO%20pinout%202.png"></p>
<p><img alt="" src="https://files.seeedstudio.com/wiki/Seeeduino-XIAO/img/regulator_to_3.3v.png"></p>
<div class="admonition note">
<p class="admonition-title">Note</p>
<p>对于通用输入输出引脚：</p>
</div>
<p>微控制器的工作电压为3.3V。如果连接到通用输入输出引脚的电压输入高于3.3V，则可能会损坏芯片。</p>
<table class="codehilitetable"><tbody><tr><td class="linenos">1</td><td class="code">对于电源引脚：</div>
</td></tr></tbody></table>
<p>内置的DC-DC转换电路能够将5V电压转换为3.3V，从而可以通过VIN引脚和5V引脚用5V电源为设备供电。</p>
<table class="codehilitetable"><tbody><tr><td class="linenos">1</td><td class="code">请注意使用，请勿提起防护罩。</div>
</td></tr></tbody></table>
<h3 id="_4">重置<a class="headerlink" href="#_4" title="Permanent link">¶</a></h3>
<p>有时，在用户编程过程失败时，Seeeduino XIAO端口可能会消失。 我们可以通过以下操作解决此问题：</p>
<ul>
<li>将Seeeduino XIAO连接到你的电脑。</li>
<li>使用镊子或短线将图中的RST引脚短路两次。</li>
<li>等至橙色LED灯闪烁并点亮。</li>
</ul>
<p>此时，芯片进入Bootloader模式，烧录端口再次出现。由于samd21芯片具有两个分区，一个分区是Bootloader，另一个分区是用户程序。 产品出厂时将在系统内存中烧录引导程序代码。因此，我们可以通过快速重置来切换模式。</p>
<p><img alt="" src="https://files.seeedstudio.com/wiki/Seeeduino-XIAO/img/XIAO-reset.gif"></p>
<h3 id="_5">中断<a class="headerlink" href="#_5" title="Permanent link">¶</a></h3>
<p>Seeeduino XIAO上的所有引脚都支持中断，但是不能同时使用两个引脚：5引脚和7引脚。有关中断的更多详细信息，请查看<a href="https://github.com/Seeed-Studio/ArduinoCore-samd/blob/master/variants/XIAO_m0/variant.cpp">此处</a>.</p>
<h3 id="_6">引脚多路复用<a class="headerlink" href="#_6" title="Permanent link">¶</a></h3>
<p>我们不需要自己手动配置引脚，使用引脚后，您可以直接调用函数。</p>
<ul>
<li>将引脚6用作数字引脚：</li>
</ul>


```
constintbuttonPin=6;// the number of the pushbutton pin
constintledPin=13;// the number of the LED pin
intbuttonState=0;// variable for reading the pushbutton status
voidsetup(){
// initialize the LED pin as an output:
pinMode(ledPin,OUTPUT);
// initialize the pushbutton pin as an input:
pinMode(buttonPin,INPUT);
}

voidloop(){
// read the state of the pushbutton value:
buttonState=digitalRead(buttonPin);
// check if the pushbutton is pressed. If it is, the buttonState is HIGH:
if(buttonState==HIGH){
// turn LED on:
digitalWrite(ledPin,HIGH);
}else{
// turn LED off:
digitalWrite(ledPin,LOW);
}
}
```

<ul>
<li>将引脚6用作模拟引脚：</li>
</ul>

```
voidsetup(){
// declare the ledPin as an OUTPUT:
pinMode(ledPin,OUTPUT);
}

voidloop(){
// read the value from the sensor:
sensorValue=analogRead(sensorPin);
// turn the ledPin on
digitalWrite(ledPin,HIGH);

// stop the program for <sensorValue> milliseconds:
delay(sensorValue);

// turn the ledPin off:
digitalWrite(ledPin,LOW);

// stop the program for for <sensorValue> milliseconds:
delay(sensorValue);
}
```

<ul>
<li>将引脚6用作UART的TX引脚（UART的RX引脚为7引脚）:</li>
</ul>

```
voidsetup(){
Serial1.begin(115200);
while(!Serial);
}

voidloop(){
Serial1.println("Hello,World");
delay(1000);
}
```

<ul>
<li>将引脚5用作I2C的SCL引脚（I2C的SDA引脚为4引脚）：</li>
</ul>

```
// Wire Master Writer
// by Nicholas Zambetti <http://www.zambetti.com>
// Demonstrates use of the Wire library
// Writes data to an I2C/TWI slave device
// Refer to the "Wire Slave Receiver" example for use with this
// Created 29 March 2006
// This example code is in the public domain.

#include<Wire.h>

voidsetup()
{
Wire.begin();// join i2c bus (address optional for master)
}

bytex=0;

voidloop()
{
Wire.beginTransmission(4);// transmit to device #4
Wire.write("x is ");// sends five bytes
Wire.write(x);// sends one byte
Wire.endTransmission();// stop transmitting
x++;
delay(500);
}
```
<ul>
<li>将引脚8用作SPI的SCK引脚（SPI的MISO引脚为9引脚，SPI的MOSI引脚为10引脚）：</li>
</ul>

```
#include<SPI.h>
constintSS=7;
voidsetup(void){
digitalWrite(SS,HIGH);// disable Slave Select
SPI.begin();
SPI.setClockDivider(SPI_CLOCK_DIV8);//divide the clock by 8
}

voidloop(void){
charc;
digitalWrite(SS,LOW);// enable Slave Select
// send test string
for(constchar*p="Hello, world!\r";c=*p;p++){
SPI.transfer(c);
}
digitalWrite(SS,HIGH);// disable Slave Select
delay(2000);
}
```

<h2 id="_7">入门指南<a class="headerlink" href="#_7" title="Permanent link">¶</a></h2>
<h3 id="_8">硬件<a class="headerlink" href="#_8" title="Permanent link">¶</a></h3>
<p><strong>所需部件</strong></p>
<ul>
<li>Seeeduino XIAO x1</li>
<li>电脑 x1</li>
<li>USB Type-C数据线 x1</li>
</ul>
<div class="admonition tip">
<p class="admonition-title">Tip</p>
<p>某些USB电缆只能供电，不能传输数据。如果您没有USB电缆或不知道您的USB电缆是否可以传输数据，你可以购买<a href="https://www.seeedstudio.com/USB-Type-C-to-A-Cable-1Meter-p-4085.html">seeed USB type C support USB 3.1</a>.</p>
</div>
<ul>
<li>
<p>第一步：准备Seeeduino XIAO和Type-C数据线。</p>
</li>
<li>
<p>第二步：将Seeeduino XIAO连接到您的计算机。此时，黄色电源LED指示灯应点亮。</p>
</li>
</ul>
<h3 id="_9">软件<a class="headerlink" href="#_9" title="Permanent link">¶</a></h3>
<div class="admonition note">
<p class="admonition-title">Note</p>
<p>如果这是您第一次使用Arduino，我们强烈建议您参考<a href="http://wiki.seeed.cc/Getting_Started_with_Arduino">Arduino入门教程</a></p>
</div>
<ul>
<li><strong>第一步：安装Arduino IDE</strong></li>
</ul>
<p><a href="https://www.arduino.cc/en/Main/Software"><img alt="" src="https://files.seeedstudio.com/wiki/Seeeduino_Stalker_V3_1/images/Download_IDE.png"></a></p>
<p><strong>启动Arduino IDE</strong></p>
<p>双击您下载好的Arduino应用程序（arduino.exe）。</p>
<div class="admonition note">
<p class="admonition-title">Note</p>
<p>如果您的Arduino软件为其他语言，您可以在"Peference"中进行更改。请访问<a href="https://www.arduino.cc/en/Guide/Environment#languages">Arduino软件(IDE)</a> for details.</p>
</div>
<ul>
<li><strong>第二步：打开Blink示例</strong></li>
</ul>
<p>打开LED闪烁示例项目:<strong>File（文件） &gt; Examples（示例） &gt;01.Basics &gt; Blink</strong></p>
<p><img alt="" src="https://files.seeedstudio.com/wiki/Seeeduino_GPRS/img/select_blink.png"></p>
<ul>
<li><strong>第二步： 将Seeeduino添加到您的Arduino IDE</strong></li>
</ul>
<p>点击<strong>File（文件） &gt; Preference（偏好）</strong> ，并将以下网址复制到“Additional Boards Manager URLs”中：
<em><a href="https://files.seeedstudio.com/arduino/package_seeeduino_boards_index.json">https://files.seeedstudio.com/arduino/package_seeeduino_boards_index.json</a></em></p>
<p><img alt="" src="https://files.seeedstudio.com/wiki/Wio-Terminal/img/Boardurl.png"></p>
<p>点击<strong>Tools（工具） &gt; Board（开发板）&gt; Boards Manager...</strong> ，在搜索栏中搜索关键字 "<strong>Seeeduino XIAO</strong> "后会出现"Seeed SAMD Boards"。点击并安装它。</p>
<p><img alt="" src="https://files.seeedstudio.com/wiki/Seeeduino-XIAO/img/XIAO-board.png"></p>
<ul>
<li><strong>第四步：选择您的板和端口</strong></li>
</ul>
<p>添加了Seeeduino XIAO后，打开目录菜单<strong>Tools（工具） &gt; Board（开发板）</strong>  ， 查找并选择 "<strong>Seeeduino XIAO M0</strong> " 。现在，您已经在Arduino IDE中完成了对Seeeduino XIAO设置。</p>
<p><img alt="" src="https://files.seeedstudio.com/wiki/Seeeduino-XIAO/img/board.png"></p>
<p>从<strong>Tools（工具） | Serial Port（端口）</strong> 中选择Seeeduino XIAO的串口号。 这应该COM3或数字更大的串口（<strong>COM1</strong> 和<strong>COM2</strong> 通常为硬件串行端口保留）。如果您不知道是哪个，您可以断开Seeeduino XIAO并重新打开菜单; 消失的条目应该是Seeeduino XIAO。重新连接电路板并选择该串行端口。</p>
<p><img alt="" src="https://files.seeedstudio.com/wiki/Seeeduino-XIAO/img/port.png"></p>
<ul>
<li><strong>第五步：上传程序</strong></li>
</ul>
<p>现在，只需单击工具栏中的“上传（Upload）”按钮。 等待几秒钟，如果上传成功，则状态栏中将显示“完成上传”的信息。</p>
<p><img alt="" src="https://files.seeedstudio.com/wiki/Seeeduino_GPRS/img/upload_image.png"></p>
<p>上传完成几秒钟后，您应该可以看到板上的引脚13（L）LED开始闪烁（橙色）。如果您能看到正确的闪烁，那么恭喜，您已经启动并运行了Arduino。 如果遇到问题，请参阅故障排除建议。</p>
<h2 id="_10">示例应用<a class="headerlink" href="#_10" title="Permanent link">¶</a></h2>
<ul>
<li>
<p><a href="https://files.seeedstudio.com/wiki/Seeeduino-XIAO/How-to-use-Seeeduino-XIAO-to-log-in-to-your-Raspberry-PI.md">借助Seeeduino XIAO访问Raspberry Pi教程</a></p>
</li>
<li>
<p><a href="https://files.seeedstudio.com/wiki/Seeeduino-XIAO/SPI-Communication-Interface.md">串行外设接口（SPI）</a></p>
</li>
<li>
<p><a href="https://files.seeedstudio.com/wiki/Seeeduino-XIAO/res/Seeeduino-XIAO-smart-car-project.pdf">Seeeduino XIAO智能车 -- 张通</a></p>
</li>
</ul>
<h2 id="_11">资源<a class="headerlink" href="#_11" title="Permanent link">¶</a></h2>
<ul>
<li><strong>[PDF]</strong><a href="https://files.seeedstudio.com/wiki/Seeeduino-XIAO/res/ATSAMD21G18A-MU-Datasheet.pdf">ATSAMD218A-MU数据手册</a></li>
</ul>
<h2 id="_12">技术支持<a class="headerlink" href="#_12" title="Permanent link">¶</a></h2>
<p>如果您有任何技术问题，请提交到我们的<a href="https://forum.seeedstudio.com/">论坛</a>。</p>

# [Starter Kit for Seeed XIAO](https://gitee.com/siger99/Wifi-Oximiter-using-MAX30102-and-ESP32/issues/I6I7T5#note_17581240_link)

![输入图片说明](https://foruda.gitee.com/images/1681924482873286722/21bab33c_5631341.png "屏幕截图")

| Seeed Studio XIAO 套件 | x |
|---|---|
| Seeed Studio XIAO 扩展板 | x1 |
| Grove - WS2813 RGB LED 防 水 灯 带 - 30 LED/m - 1m， A.1 （设计）| x1 |
| Grove LED 模块 | x1 |
| Grove 旋钮模块 | x1 |
| Grove 红外接收器模块 | x1 |
| Grove - 光传感器模块 | x1 |
| Grove - 迷你 PIR 运动传感器模块 | x1 |
| Grove - 3 轴加速度计 (LIS3DHTR) 模块 | x1 |
| Grove - 温湿度传感器模块 V2.0(DHT20) | x1 |
| Grove - 舵机 | x1 |
| USB 数据线； A 转 C 口 | x1 |
| Gorve 电缆 | x7 |
| 公对公跳线 | x4 |
| 20 键迷你遥控器 | x1 |
| cr2032 纽扣电池 | x1 |
| Grove 超声波距离传感器 | x1 |

### [课程](https://gitee.com/siger99/Wifi-Oximiter-using-MAX30102-and-ESP32/issues/I6I7T5#note_17581251_link)

第一单元 硬件及编程入门（XIAO 系列均可用）

| # | 课程标题 | 学习目标 |
|---|---|---|
| 1 | Seeed Studio XIAO 的第一个 Arduino 程序：Blink | 了然 Arduino IDE 以及如何用 XIAO 点亮 LED。 |
| 2 | 用 XIAO 扩展板上的按钮开关 LED | 认识 XIAO 扩展板，用编程实现按 XIAO 扩展板按钮控制 LED 的开关。 |
| 3 | XIAO 加扩展板变身莫尔斯电码发报机 | 学习通过 XIAO 来控制扩展板的板载蜂鸣器以及通过扩展板的按钮来控制蜂鸣器发声。 |
| 4 | 用串口监视器查看旋钮的数值变化 | 在 XIAO 扩展板上外接 Grove 旋钮模块，并学习通过串口监视器查看旋钮的数值变化。 |
| 5 | 用旋钮控制 LED 和舵机 | 进一步学习使用旋钮来控制 LED 的亮度变化，以及舵机的转动角度。 |
| 6 | 让 OLED 显示 Hello World ！ | 学习控制扩展板的 OLED 显示屏显示文本信息和图案。 |

第二单元 项目实践初级 —— 原型设计入门（XIAO 系列均可用）

| # | 课程标题 | 学习目标 |
|---|---|---|
| 7 | 产品原型设计入门 | 学习产品原型设计的基本知识，并尝试提出自己的产品方案。 |
| 8 | 智能温湿度仪 | 读取温湿度传感器的读数并展示在扩展板的 OLED 屏幕上。 |
| 9 | 基于光传感器的惊喜礼盒 | 学习使用光传感器，实现当礼盒打开时，RGB LED 灯带亮起的效果。 |
| 10 | 借助三轴加速度计的律动炫舞 | 了解 3 轴加速度计，并用此传感器控制 RGB LED 灯带变换灯效 |

第三单元 项目实践中极 —— 复杂项目（ 第 11-13 课 XIAO 系列均可用，第 14、15 课需要 XIAO ESP32C3）

| # | 课程标题 | 学习目标 |
|---|---|---|
| 11 | 智能遥控门 | 了解和学习使用红外接收器模块，以实现用遥控器控制舵机转动开关门。 |
| 12 | 智能手表 | 了解和学习 XIAO 扩展板上 RTC 时钟的用法，并用它制作一个能显示当前时间和温湿度的智能手表。 |
| 13 | 空气琴 | 了解和学习超声波距离传感器的用法，并用它制作一个空气琴。 |
| 14 | 用 XIAO ESP32C3 实现 Wi-Fi 连接和应用 | 让 XIAO ESP32C3 通过 Wi-Fi 连接局域网的本机发送 HTTP GET 或 POST 请求。 |
| 15 | 用 XIAO ESP32C3 通过 MQTT 协议实现遥测与命令 | 在本课中，我们将逐步介绍：通信协议，消息队列遥测传输 (MQTT)，遥测（从传感器收集并发送到云端的数据）和命令（由云向设备发送的指示它做一些事情的消息）。 | 

第四单元 项目实践高级 —— TinyML 应用（仅针对 XIAO nRF52840Sense）

| # | 课程标题 | 学习目标 |
|---|---|---|
| 16 | 认识 TinyML 与 Edge Impulse | 了解 TinyML 的基本概念，以及 Edge Impulse 工具的基本介绍。 |
| 17 | TinyML 变得简单：用 XIAO 实现异常检测和运动分类 | 使用 XIAO nRF52840 Sense 上的 6 轴加速度计和 Edge Impulse 进行运动识别，以学习机器学习的数据采集、训练、测试、部署到推理的整个过程。 |
| 18 | TinyML 变得简单：用 XIAO 实现语音关键词识别 (KWS) | 使用 XIAO nRF52840 Sense 上的麦克风和 Edge Impulse 对声音分类，实现语音关键字检测的功能。 |

第五单元 自研项目（XIAO 系列均可用）

| # | 课程标题 | 学习目标 | 
|---|---|---|
| 19 | 挑战改造项目 | 学会在简单项目的基础上改进，以实现更复杂需求的方法。 |
| 20 | 自研项目策划 | 学会尝试规划设计自己的项目。 |
| 21 | 自研项目分享与展示 | 学习并尝试为自己的项目准备一场发布会。 | 
| 22 | 自由探索——有用与有趣的 XIAO 项目集锦 | 提供了一些用户使用 XIAO 做的项目案例，以帮助读者看到用 XIAO 做创意的更多可能性。 | 

# [用 Codecraft 玩转 Grove 极简 Arduino 入门套件](https://gitee.com/siger99/Wifi-Oximiter-using-MAX30102-and-ESP32/issues/I6I7T5#note_17581283_link)

| 模块 | 端口 | 引脚 / 地址 |
|---|---|---|
| LED | 数字 | D4 | 
| 蜂鸣器 | 数字 | D5 | 
| OLED.显示屏 | I2C | I2C,.0x78( 默认 ) | 
| 按钮 | 数字 | D6 | 
| 旋转式电位器 | 模拟 | A0 | 
| 光传感器 | 模拟 | A6 | 
| 声音传感器 | 模拟 | A2 | 
| 温度 & 湿度传感器 | 数字 | D3 | 
| 气压传感器 | I2C | I2C,.0x77( 默认 )./.0x76( 可选 ) | 
| 3 轴数字加速度计 | I2C | I2C,.0x19( 默认 | 

### 入门套件 + 教育扩展包的课程

1. 我的第一个.Arduino.程序：Blink
1. 控制.LED.灯的亮度
1. 循环与变量——.LED.“呼吸灯”
1. 条件语句——按钮控制开关灯
1. 用旋钮调节.LED.亮度
1. 摩尔斯电码发报机和音乐盒
1. 点亮.OLED.显示屏，开启可视化交互
1. 那些默不作声的硬件真的在工作吗？
1. “看见”声音与制作声控灯
1. 光控灯
1. 气压计与高度计
1. 小小气象站
1. 三轴加速度计——运动与平衡
1. 扩展项目 1：智能加湿器
1. 扩展项目 2：可转头的遥控电风扇
1. 扩展项目 3：自动报警宝箱.

### Grove 极简 Arduino 入门套件教育扩展包

Grove 极简 Arduino 入门套件教育扩展包（英文 Grove Beginner Kit for Arduino Education Add-on Pack Grove，课程内简称“扩展包”）。使用入门套件和扩展包结合，就能做一些小规模且有一定复杂度的项目。

![背景知识](https://foruda.gitee.com/images/1681953628142214080/c9725f42_5631341.png "屏幕截图")

- 执行器 
- 传感器
- Grove - 加湿器.v1.0.x1 
- Grove - 超声波测距传感器.x1
- Grove - 迷你风扇.v1.1.x1 
- Grove - IR 红外接收器.x1
- Grove - 舵机.x1 
- Grove - 迷你无源红外运动传感器.x1

![输入图片说明](https://foruda.gitee.com/images/1681953247162175394/0574d3ac_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1681953284702425748/e37b5631_5631341.png "屏幕截图")

# [Seeed为Arduino推出的新Grove初学者套件：可以说是最简单的入门方式](https://techexplorations.com/blog/arduino/seeeds-grove-beginner-kit-for-arduino-percect-for-beginners/)

7月 2020， <> 作者：彼得

<p _msttexthash="119863354" _msthash="25">我最近收到了Seeed Studio的新套件：用于Arduino的<a aria-label="未定义（在新选项卡中打开）" href="https://amzn.to/31x6NJ5" target="_blank" rel="noreferrer noopener sponsored nofollow" _mstaria-label="616733" _msthash="24" _istranslated="1">Grove Beginner Kit</a>。</p>
<p _msttexthash="1387863477" _msthash="28"><a href="https://techexplorations.com/guides/arduino/grove/what-is-grove/" target="_blank" aria-label="未定义（在新选项卡中打开）" rel="noreferrer noopener" _mstaria-label="616733" _msthash="26" _istranslated="1">Grove</a>是一个连接器和组件系统，使创建电路变得非常容易。只需将特殊的 Grove 电缆插入连接器，然后继续处理草图，知道接线牢固可靠。因此，Grove系统非常适合<a href="https://techexplorations.com/guides/arduino/begin/lss1/" target="_blank" aria-label="未定义（在新选项卡中打开）" rel="noreferrer noopener" _mstaria-label="616733" _msthash="27" _istranslated="1">刚接触Arduino的人</a>。</p>
<p _msttexthash="1604395845" _msthash="29">但是，有了Arduino的Grove初学者套件，Seeed更进一步：他们摆脱了电线。尽管该套件包含诸如OLED屏幕，五个传感器以及按钮和电位计等各种执行器之类的模块，但您无需进行任何接线。</p>
<hr class="wp-block-separator">
<p class="has-very-light-gray-background-color has-background" _msttexthash="587255032" _msthash="30">我录制了一个 90 秒的视频，向您展示 Grove Beginner Kit for Arduino 的工作原理，开箱即用。<br _istranslated="1"><br _istranslated="1"><a href="#video" _istranslated="1">您可以在这篇文章中找到它</a>。</p>
<hr class="wp-block-separator">
<p _msttexthash="76133304" _msthash="31">看看格罗夫初学者套件的照片：</p>
<p><img src="https://foruda.gitee.com/images/1681058569793867075/f2d05dd6_5631341.png"></p>
<p>用于Arduino的Grove初学者套件由包含Arduino和各种组件的单个PCB组成。很酷的事实：组件已经连接到Arduino。它开箱即用。</p>
<p _msttexthash="20170358" _msthash="33">该套件是单个PCB。</p>
<p _msttexthash="309469732" _msthash="34">PCB包含一个带有Grove连接器的Arduino克隆和一组标准针座，可以容纳Arduino屏蔽。</p>
<p _msttexthash="782985840" _msthash="35">在Arduino周围，PCB包含带有传感器（湿度，光线，声音等）和执行器（LED，按钮，电位计，蜂鸣器）的模块。</p>
<div class="wp-block-cover alignwide has-vivid-red-background-color has-background-dim is-position-center-center" style="min-height:50px"><div class="wp-block-cover__inner-container">
<h2 class="wp-block-heading" _msttexthash="788710" _msthash="36">Arduino for Beginners with Grove</h2>
<div class="wp-block-columns alignwide are-vertically-aligned-center is-layout-flex wp-container-4">
<div class="wp-block-column is-vertically-aligned-center is-layout-flow" style="flex-basis:66.67%">
<p class="has-text-align-left has-text-color" style="line-height:1.2;font-size:21px;color:#fffffa"><strong _msttexthash="324197471" _msthash="37">本课程将通过教您如何使用创新的 Arduino Grove 初学者套件附带的所有硬件来向您介绍 Arduino。</strong></p>
<div class="wp-block-buttons is-layout-flex">
<div class="wp-block-button"><a class="wp-block-button__link has-text-color has-background" href="https://techexplorations.com/so/arduino-for-beginners-with-grove/" style="border-radius:3px;background-color:#fffffa;color:#00000a" rel="https://techexplorations.com/so/arduino-for-beginners-with-grove/" _msttexthash="51646595" _msthash="38">了解有关本课程的更多信息</a></div>
</div>
</div>
<p><img src="https://foruda.gitee.com/images/1681058606270471442/48f99334_5631341.png"></p>
<p></p>
</div></div>
<h2 class="wp-block-heading" _msttexthash="4620057" _msthash="39">创新</h2>
<p _msttexthash="1355430999" _msthash="40">Seeed的创新之处在于，他们通过PCB本身将所有这些模块连接到Arduino。在PCB上，有迹线将套件左上角的LED模块连接到Arduino数字引脚4。蜂鸣器连接到数字引脚 5。OLED显示屏到Arduino的I2C接口。</p>
<p _msttexthash="29568591" _msthash="41">这确实非常聪明。</p>
<p _msttexthash="753381954" _msthash="42">您只需将套件从包装盒中取出，将其插入任何USB电源（或您的计算机），它将执行工厂上传的草图，该草图循环遍历各个组件。</p>
<p><img src="https://foruda.gitee.com/images/1681058638440664448/3b7f0bf2_5631341.png"></p>
<p>我的 Arduino Grove 初学者工具包运行演示草图。</p>
<p _msttexthash="113107943" _msthash="44">PCB设计非常聪明（可以重复我自己！</p>
<p _msttexthash="725517377" _msthash="45">如果要从PCB上卸下模块，可以使用切割工具卡住接头（不要简单地在接头上施加压力，因为这可能会导致电路板和模块损坏）。</p>
<p _msttexthash="547332097" _msthash="46">您还可以通过 Arduino 上的 Grove 连接器或通过跳线连接其他 Grove 模块。当然，您可以连接任何兼容的Arduino扩展板。</p>
<p _msttexthash="961759591" _msthash="47">一旦你玩够了演示草图，你可以启动Arduino IDE，将开发板设置为“Arduino Uno”，然后开始编写自己的草图以使用套件的任何模块......无需进行任何布线。</p>

- https://player.vimeo.com/video/433855800

这是一个90秒的视频，其中包含我对套件的第一印象。</em></figcaption></figure>
<h2 class="wp-block-heading" _msttexthash="4481139" _msthash="49">影响</h2>
<p _msttexthash="295908301" _msthash="50">这对于教室来说改变了游戏规则，大部分时间都用于修复松动或不正确的接线。</p>
<p _msttexthash="693567303" _msthash="52">实际上，我开始重新制作我的 10 个 Arduino 精通项目，以使用 <a aria-label="未定义（在新选项卡中打开）" href="https://app.techexplorations.com/courses/arduino-mastery-projects/" target="_blank" rel="noreferrer noopener" _mstaria-label="616733" _msthash="51" _istranslated="1">Arduino 的 Grove 初学者工具包</a>，并添加了一些。当然，没有跳线。</p>
<p _msttexthash="3384000113" _msthash="54">Seeed过去曾提供过Arduino套件：用于Arduino的Grove入门套件。这是一个经典的套件，具有基本的模块集合，但没有Arduino来降低成本。我<a aria-label="未定义（在新选项卡中打开）" href="https://app.techexplorations.com/courses/grove-for-busy-people/" target="_blank" rel="noreferrer noopener" _mstaria-label="616733" _msthash="53" _istranslated="1">什至基于此套件创建了一门课程</a>。这很好，但与其他选择相比没有太大的不同或创新。你仍然必须把东西连接在一起，尽量不要丢失任何东西。</p>
<p _msttexthash="275298868" _msthash="56">还有一个用于<a href="https://www.seeedstudio.com/Grove-Beginner-Kit-for-Arduino-p-2895.html" target="_blank" aria-label="未定义（在新选项卡中打开）" rel="noreferrer noopener sponsored nofollow" _mstaria-label="616733" _msthash="55" _istranslated="1">Arduino（旧）的Grove Beginner Kit</a>的旧版本，其中包括Arduino克隆。</p>
<p _msttexthash="1577268966" _msthash="57">作为一名教育工作者，当我帮助新学习者开始使用电子产品和 Arduino 时，我的主要困难是使他们的学习曲线尽可能温和。大多数学习者似乎很早就放弃了，因为必须吸收大量的信息和必须获得的技能。</p>
<p _msttexthash="1320306351" _msthash="58">学习是一项长期的努力，因此，作为教育工作者，我们能做的任何事情来降低早期辍学的风险都会增加学习者坚持足够长的时间以获得好东西（即成为独立创造者）的可能性。</p>
<p _msttexthash="274581203" _msthash="60"><a href="https://scratch.mit.edu/" target="_blank" aria-label="未定义（在新选项卡中打开）" rel="noreferrer noopener nofollow" _mstaria-label="616733" _msthash="59" _istranslated="1">诸如Scratch</a>（或其他图形）编程语言之类的创新有助于实现这一目标。</p>
<p _msttexthash="246074985" _msthash="61">Seeed使用Arduino的Grove Beginner Kit，在硬件方面也做了同样的事情。</p>
<h2 class="wp-block-heading" _msttexthash="33939763" _msthash="62">学习者的软硬件困境</h2>
<p _msttexthash="181616825" _msthash="63">我花了很多时间试图弄清楚我应该首先教给新学生什么：</p>
<ul><li _msttexthash="115274510" _msthash="64">我应该从向他们展示如何连接电路开始吗？</li><li _msttexthash="35025068" _msthash="65">也许从草图开始？</li><li _msttexthash="110207955" _msthash="66">也许通过一个简单的项目来修复这一切？</li><li _msttexthash="126460074" _msthash="67">或者，专注于解释如何使用单个组件或模块？</li></ul>
<p _msttexthash="236183584" _msthash="68">我对此做了很多思考和阅读，也花了很多时间与其他教育工作者交谈。</p>
<p _msttexthash="64472239" _msthash="69">毕竟，我意识到这<strong _istranslated="1">并不重要</strong>。</p>
<p _msttexthash="195997854" _msthash="70">学习以许多不同的方式发生，这对我们所有人都是独一无二的。</p>
<p _msttexthash="360717604" _msthash="71">规定学生应该先从这个开始，然后从那个开始，然后再从另一个开始，这是行不通的。</p>
<p _msttexthash="140469537" _msthash="72">根据我的经验，在学习方面需要考虑四件事：</p>
<ol><li _msttexthash="42123263" _msthash="73">我们想学习和教什么？</li><li _msttexthash="50576695" _msthash="74">我们有哪些可用的工具？</li><li _msttexthash="573552993" _msthash="75">谁是学生（以及他们的独特情况，例如他们已经知道什么，他们带来了什么样的技能，他们自己的愿望是什么）</li><li _msttexthash="82964310" _msthash="76">谁是老师（以及他们独特的...你知道...</li></ol>
<p _msttexthash="308778847" _msthash="77">总之，重要的是结果（“什么”）。“如何”取决于老师、学生和他们可以使用的工具。</p>
<p _msttexthash="87737832" _msthash="78">您可以通过多种方式实现理想的结果。</p>
<p _msttexthash="130115518" _msthash="79">但是当涉及到指标时，一个重要的指标是时间。</p>
<p _msttexthash="149388356" _msthash="80">学生需要多长时间才能体验到他们的第一次成功？</p>
<p _msttexthash="114882092" _msthash="81">学生需要多长时间才能体验第二次成功？</p>
<p _msttexthash="141975886" _msthash="82">学生需要多长时间才能体验到源源不断的成功？</p>
<p _msttexthash="363427077" _msthash="83">每一次成功都会增加学生继续学习的意愿。他们的奖励是多巴胺激增，这让他们渴望更多。</p>
<p _msttexthash="637369720" _msthash="84">如果我们能够构建我们的教学，以增加学习过程中的良好感觉（成功而不是痛苦），那么我们就走在正确的轨道上。</p>
<p _msttexthash="534970332" _msthash="85">当涉及到电子和编程时，诸如松动的电线和损坏的Arduinos之类的东西会产生痛苦，并导致继续的愿望降低。</p>
<p _msttexthash="1085276946" _msthash="86">借助Scratch和Arduino的Grove初学者工具包等技术，我们可以提高成功与失败的比例，从而提高学生“坚持”足够长的时间以成为独立创作者的可能性。</p>
<h2 class="wp-block-heading" _msttexthash="19493162" _msthash="87">一些技术说明</h2>
<p _msttexthash="222425827" _msthash="88">在我写完这篇文章之前，我想提一下我在玩 Grove 套件时发现的一些事情。</p>
<p _msttexthash="2077852530" _msthash="89">所有 Grove 组件（套件中和单独出售的组件）均为库存标准。Grove气压计传感器模块（包含BMP280）和通用BMP280分线之间的唯一区别，您可以在eBay上找到，以及Grove连接器。要在草图中使用它们，请使用完全相同的代码和库。</p>
<p _msttexthash="1096448197" _msthash="91">屏幕是标准的0.96英寸OLED模块，具有SSD1315驱动芯片和128×64点分辨率。在草图中，导入 <a aria-label="未定义（在新选项卡中打开）" href="https://github.com/olikraus/u8g2/wiki" target="_blank" rel="noreferrer noopener nofollow" _mstaria-label="616733" _msthash="90" _istranslated="1">U8g2lib 库</a>。这个库有很好的文档，也许是它所有替代方案中最好的。</p>
<p _msttexthash="1460347681" _msthash="92">您可以使用它来创建包含字母、数字和图形的用户界面。可以使用图形基元（线条、像素、框、圆等）以编程方式创建图形，也可以创建自己的位图图形。我已经记录了创建自定义位图的简单过程。</p>
<p _msttexthash="2230822828" _msthash="93">每个模块都通过PCB上的走线连接到Arduino。使用的引脚在丝网印刷上标记，以便您可以在草图中设置它们。显示器、压力传感器和加速度计通过 I2C 连接。其余组件连接到唯一的引脚。这意味着您可以同时使用所有模块，因为没有共享引脚。</p>
<p _msttexthash="1083226924" _msthash="95">为了降低成本，该套件的Arduino板使用<a href="https://www.silabs.com/products/development-tools/software/usb-to-uart-bridge-vcp-drivers" target="_blank" aria-label="未定义（在新选项卡中打开）" rel="noreferrer noopener nofollow" _mstaria-label="616733" _msthash="94" _istranslated="1">CP1202 USB驱动程序</a>。您可能需要为您的 PC 安装适当的驱动程序才能完成此操作。我在运行Mac OS Catalina的iMac上这样做没有问题。</p>
<p _msttexthash="120912142" _msthash="97">您可以在 <a aria-label="未定义（在新选项卡中打开）" href="https://wiki.seeedstudio.com/Grove-Beginner-Kit-For-Arduino/" target="_blank" rel="noreferrer noopener sponsored nofollow" _mstaria-label="616733" _msthash="96" _istranslated="1">Seeed Studio wiki</a> 中找到有关此套件的信息。</p>

# [【开箱】拼装式开发板到底是营销概念还是真香？Grove Beginner Kit For Arduino 开箱初印象](https://www.bilibili.com/video/BV1JV411s74u)

- https://www.bilibili.com/video/BV1JV411s74u

  > 我是龚克的，孙老师，又分享硬件知识大板，今天，我想要来评测一块，迪哥的开发板，来自seed的FU5也是应该替的。声明一下，本期视频绝无插板成分，开发板都是up主，我自己掏钱买的，所以各位观众，也可以放心观看。这块开发板最大的特点，就是它采用了一种很独特的设计，就像这种拼装玩具一样，每一个模块都可以单独的摘下来，而且它整体的这个设计，配色，我觉得都挺漂亮的，外观和创意上我觉得都可以给满分，拿到手的话，也千万别急着把这些模块一个个都拆下来，仔细看的话可以发现，每一个模块其实跟主控板是连接的，也就是说不需要任何的连线，直接编程就可以使用了。这对于没有基础的同学来说是非常友好的，因为对于很多新手来说，一开始既要学习硬件的连接，又要学习编程，那是一个非常困难的事情。你可能会注意到这个板上有很多种白色的小插口，如果你把这些模块掰下来以后，就可以通过这个白色的接口，把这个模块和主控板连接在一起，就成这个样子。那这种白色的接口，是一种叫做group的通用接口。使用group接口的优点，就是说你完全不用担心插错，因为它有防粘的功能，只要你能插进去就肯定是对的。那么缺点，就是说，目前市面上兼容这个group接口的模块并不是很多，而且价格也普遍比较高。

  > 我们来看一看这个开发板上都有些什么东西，中间是一块鱼龙乌龙的触控板，边上四周一共分布着十个输入输出模块，分别是led灯、分明器、oled显示屏、硬件旋钮、光敏传感器、麦克风、温室度传感器、气压计、加速度计，那这些模块虽然不算多，但是完全可以满足一个初学者学习使用了。当然，我觉得如果能加上像WS2812的珍珠极光测距模块，那就更好了。另外的话，这个开发板默认是自带了一套测试代码的，涵盖了以上所有的模块，拿到手的话插上电就可以直接使用了。你可以用它来测温度、气压、环境、光线、噪音，甚至还可以当一个水平仪来使用。除了硬件本身，B磊也很贴心的给这个开发板准备了。做的教程基本上跟着这些教程走一遍，云的就算是初步入门了，最后说一下大家比较关心的价格问题，价格的话是175块钱，这比自己的家出品的其他产品，我觉得还是比较良心的了。如果和淘宝上一些没有品牌的模块相比，还是稍微有些贵的，我大致估算了一下，差价应该是在100块钱左右，不过我觉得冲着这个独特的设计，以及它贴心的教程就挣多钱了，毕竟比起其他开发版，可以避免初学者少走很多的弯路，节约很多的时间。如果你是一个初学者，想学习ig运动，那么我觉得这个套件非常的合适，甚至可以说是首选，但是如果你是想做项目，那么我就不是那么推荐。

  > 店里去买这个开发版的，因为毕竟你不可能用到这上面所有的模块，而且这个格录模块的通用性也不是很强。好，那本期视频就到这里，我是龚克山孙老师，我会定期给大家分享一些开源以及相关的知识，记得关注我三连，拜拜。

  ![输入图片说明](https://foruda.gitee.com/images/1681056526408739694/fd0b6d7b_5631341.png "屏幕截图")

  ![输入图片说明](https://foruda.gitee.com/images/1681056583070993429/da770777_5631341.png "屏幕截图")

  ![输入图片说明](https://foruda.gitee.com/images/1681056607118858782/09376b9e_5631341.png "屏幕截图")

  ![输入图片说明](https://foruda.gitee.com/images/1681056637790822561/4fffe95d_5631341.png "屏幕截图")

  ![输入图片说明](https://foruda.gitee.com/images/1681056686565766136/513344f0_5631341.png "屏幕截图")

  ![输入图片说明](https://foruda.gitee.com/images/1681056720001923015/025b9c1f_5631341.png "屏幕截图")

  小白友好，防呆功能（防止插错）

  ![输入图片说明](https://foruda.gitee.com/images/1681056820457981242/d34ca1a0_5631341.png "屏幕截图")

---

【笔记】[GROVE BEGINNER KIT for Arduino by Seeed](https://gitee.com/siger99/Wifi-Oximiter-using-MAX30102-and-ESP32/issues/I6I7T5)

- [RP2040](https://gitee.com/siger99/Wifi-Oximiter-using-MAX30102-and-ESP32/issues/I6TYQC) | 26 | 13天前

  树莓派四百是你们家待产的?  

  - seeed矽递科技:果子 2021-12-07 13:39:12

    > 这个是外采的哦

  - [arduino开发板nano/uno主板 Seeed Studio XIAO 微控制器蓝牙主控](https://item.taobao.com/item.htm?id=607624979977)

    - [RP2040](https://wiki.seeedstudio.com/XIAO-RP2040/ ) （[转载](https://gitee.com/siger99/Wifi-Oximiter-using-MAX30102-and-ESP32/issues/I6TYQC#note_17355504_link)）
    - 这个是另一个型号的 大体上是差不多的
      链接：https://pan.baidu.com/s/1m6EWovPADnJ1xBIMTM2GPw  提取码：tg4f

  - [seeed矽递科技:服务助手 2023-04-06 20:12:37](https://gitee.com/siger99/Wifi-Oximiter-using-MAX30102-and-ESP32/issues/I6TYQC#note_17355325_link)

    ReSpeaker系列为矽递科技自主研发产品，专注于AI智能语音交互，智能语音服务类应用领域；
    1. 该系列各版本参数对比详见图片；
    2. 技术及教程资料详见：http://wiki.seeedstudio.com/cn/ReSpeaker/

    <p><img height="199px" src="https://foruda.gitee.com/images/1681008925977198380/3178e423_5631341.jpeg"></p>

  new Bing:

  - [do you know arduino module, named Grove? and there're the develepment board kit suit for it. pls reply me both in english and chinese.](https://gitee.com/siger99/Wifi-Oximiter-using-MAX30102-and-ESP32/issues/I6TYQC#note_17359819_link)

    - [Is there the Grove registed mark for business? which company or group ownes it? do you know which companies in china to produce the GROVE kit for arduino?](https://gitee.com/siger99/Wifi-Oximiter-using-MAX30102-and-ESP32/issues/I6TYQC#note_17359837_link)
    - [dose the standard grove beginner kit have how many sensors? could you give me the sensor name list?](https://gitee.com/siger99/Wifi-Oximiter-using-MAX30102-and-ESP32/issues/I6TYQC#note_17359844_link)

      The Grove Beginner Kit for Arduino comes with **10 sensors** including : 
      - a temperature & humidity sensor, 
      - air pressure sensor, 
      - 3-axis accelerometer, 
      - sound sensor, 
      - light sensor, 
      - rotary potentiometer, 
      - LED bar, 
      - buzzer, 
      - button and 
      - a touch sensor

    - [pls give the chinese name of the 10 sensors ?](https://gitee.com/siger99/Wifi-Oximiter-using-MAX30102-and-ESP32/issues/I6TYQC#note_17359850_link)
    - [pls give me office document about the grove beginner kit for arduino.](https://gitee.com/siger99/Wifi-Oximiter-using-MAX30102-and-ESP32/issues/I6TYQC#note_17359853_link)
    - [one Arduino compatible Board and 10 additional Arduino sensors and all in one-piece of PCB design. it's so exciting, could you give me more information about that? only seeeduino to provide the founction?](https://gitee.com/siger99/Wifi-Oximiter-using-MAX30102-and-ESP32/issues/I6TYQC#note_17359860_link)
    - [that's nice. could you give me the 12 step-by-step project tutorials for me? in details for each.](https://gitee.com/siger99/Wifi-Oximiter-using-MAX30102-and-ESP32/issues/I6TYQC#note_17359862_link)

      1. Blinking an LED
      1. Button Pressed LED
      1. Temperature Sensor
      1. Light Sensor
      1. Sound Sensor
      1. Rotary Angle Sensor
      1. Relay
      1. LCD Backlight
      1. Buzzer
      1. Vibration Motor
      1. Servo
      1. Ultrasonic Ranger

    - [someone said the grove interface not be used by generaly only few company used for there module. how do you think about it?](https://gitee.com/siger99/Wifi-Oximiter-using-MAX30102-and-ESP32/issues/I6TYQC#note_17359977_link)
    - [could you give the offical arduino extensition board intro, and how diffrent with grove.](https://gitee.com/siger99/Wifi-Oximiter-using-MAX30102-and-ESP32/issues/I6TYQC#note_17359979_link)

  评测：

  - [Seeed为Arduino推出的新Grove初学者套件：可以说是最简单的入门方式](https://techexplorations.com/blog/arduino/seeeds-grove-beginner-kit-for-arduino-percect-for-beginners/) （[转载](https://gitee.com/siger99/Wifi-Oximiter-using-MAX30102-and-ESP32/issues/I6TYQC#note_17359897_link)）

    - [CODING ARRAY Arduino学习板开箱](https://www.bilibili.com/video/BV1nk4y167eC/) （[摘要](https://gitee.com/siger99/Wifi-Oximiter-using-MAX30102-and-ESP32/issues/I6TYQC#note_17359965_link)）
      - 大而全
      - 有 ARDUINO 开发板
      - 底板有一块亚克力，更厚实，更稳。
    - [Arduino初学者神器Grove Beginner Kit For Arduino开箱视频](https://www.bilibili.com/video/BV13g4y167Hq/) （[摘要](https://gitee.com/siger99/Wifi-Oximiter-using-MAX30102-and-ESP32/issues/I6TYQC#note_17359950_link)）
    - [【开箱】拼装式开发板到底是营销概念还是真香？Grove Beginner Kit For Arduino 开箱初印象](https://www.bilibili.com/video/BV1JV411s74u) （音转字）
    - [【开箱】Seeed Grove Beginner KIT for Arduino](https://www.bilibili.com/video/BV1dv411y7He/) （[摘要](https://gitee.com/siger99/Wifi-Oximiter-using-MAX30102-and-ESP32/issues/I6TYQC#note_17359912_link)）
      - 假如有人打算购买套件但不清楚里头有什么的话，可以参考一下这个视频。
  - 用于Arduino评论的Seeed Grove初学者套件（Tom's Hardware 论坛评测，中译文）
  - [WeDesign第2期，一起来设计体验树莓派RP2040吧！](https://www.bilibili.com/video/BV1xh411E7BX/)
    https://www.eetree.cn/project/detail/1692

- [do you know microduino?](https://gitee.com/siger99/Wifi-Oximiter-using-MAX30102-and-ESP32/issues/I6TYQC#note_17360161_link)

  - [did microduino used grove interface on their module?](https://gitee.com/siger99/Wifi-Oximiter-using-MAX30102-and-ESP32/issues/I6TYQC#note_17360171_link)
  - [give me the details of microduino sensor extionsion module.](https://gitee.com/siger99/Wifi-Oximiter-using-MAX30102-and-ESP32/issues/I6TYQC#note_17360172_link)
  - [the micorduino-sensorhub using trinket connector are 4 pin, 1.25mm pitch JST connectors. which connector are used by grove interface?](https://gitee.com/siger99/Wifi-Oximiter-using-MAX30102-and-ESP32/issues/I6TYQC#note_17360179_link)
  - https://wiki.microduinoinc.com/Microduino-Module_Sensor_Hub （[转载](https://gitee.com/siger99/Wifi-Oximiter-using-MAX30102-and-ESP32/issues/I6TYQC#note_17360177_link)）
  - [are there diffrent between IIC and I2C ?](https://gitee.com/siger99/Wifi-Oximiter-using-MAX30102-and-ESP32/issues/I6TYQC#note_17360184_link)
  - https://www.instructables.com/Microduino-a-small-and-stackable-Arduino-Uno/ （[转载](https://gitee.com/siger99/Wifi-Oximiter-using-MAX30102-and-ESP32/issues/I6TYQC#note_17365400_link)）

- [all to ARDUINO and all to python](https://gitee.com/siger99/Wifi-Oximiter-using-MAX30102-and-ESP32/issues/I6IVZS) |  14  | 2个月前

  all to ARDUINO

  - 树莓派 跨 ARDUINO
  - FPGA to arduino
  - any MCU 开发板 to arduino

  all to python

  - pygames
  - pynq
  - py porting 

  <img align="right" width="169px" src="https://foruda.gitee.com/images/1679549066229214914/38f4ec90_5631341.png">

  Grove Beginner Kit 缘起：[用梁山派做一个ARDUINO的配套器材，一个扩展板全部实验](https://gitee.com/siger99/Wifi-Oximiter-using-MAX30102-and-ESP32/issues/I6IVZS#note_17010583_link)
  - [#629探秘梁山派的全栈开发能力](https://gitee.com/siger99/Wifi-Oximiter-using-MAX30102-and-ESP32/issues/I6IVZS#note_17029650_link) 封面故事
    - [10分钟快速入门](https://lckfb.com/docs/lckfb_lspi/#/) （[转载](https://gitee.com/siger99/Wifi-Oximiter-using-MAX30102-and-ESP32/issues/I6IVZS#note_17029655_link)）
    - [立创·梁山派入门学习引导手册](https://dri8c0qdfb.feishu.cn/wiki/wikcn3yhk3CjNBK9mESfbXlrpSC) （[转载](https://gitee.com/siger99/Wifi-Oximiter-using-MAX30102-and-ESP32/issues/I6IVZS#note_17029686_link)）
    - [立创·梁山派Arduino开发](https://dri8c0qdfb.feishu.cn/docx/IX9jd4ku3oq9HPxHqeFcDxEcnyg) （[转载](https://gitee.com/siger99/Wifi-Oximiter-using-MAX30102-and-ESP32/issues/I6IVZS#note_17029696_link)）
    - [梁山派开发板 MicroPython](https://dri8c0qdfb.feishu.cn/docx/FBa2djfSdopfOoxolVucpYi5nDv) （[转载](https://gitee.com/siger99/Wifi-Oximiter-using-MAX30102-and-ESP32/issues/I6IVZS#note_17029703_link)）
    - [绿垫板](https://gitee.com/siger99/Wifi-Oximiter-using-MAX30102-and-ESP32/issues/I6IVZS#note_17029716_link)  [图](https://foruda.gitee.com/images/1679594788106835405/b3224766_5631341.jpeg)

  <img align="right" width="136px" src="https://foruda.gitee.com/images/1679650389466727227/25c84e2c_5631341.jpeg">

  - [根据：《人工智能与开源》的硬件篇的三个部分，得出以下硬件清单。](https://gitee.com/siger99/Wifi-Oximiter-using-MAX30102-and-ESP32/issues/I6IVZS#note_17029964_link)

    - [v0.1 原始清单罗列（重复多）](https://gitee.com/siger99/Wifi-Oximiter-using-MAX30102-and-ESP32/issues/I6IVZS#note_17029965_link)
    - [v0.2 简单重复计数 （个数和次数合并啦）](https://gitee.com/siger99/Wifi-Oximiter-using-MAX30102-and-ESP32/issues/I6IVZS#note_17029988_link)
    - [v0.3 删除传感器列表，和电阻（板子上不再需要）简单聚合了4个扩展应用](https://gitee.com/siger99/Wifi-Oximiter-using-MAX30102-and-ESP32/issues/I6IVZS#note_17030027_link)
    - [v0.4 聚合声光电基础（去掉连线，PCB预置啦），将麦克和摄像头，归到智能家居（智能生活），新增机器人扩展](https://gitee.com/siger99/Wifi-Oximiter-using-MAX30102-and-ESP32/issues/I6IVZS#note_17030058_link)
    - [v0.5 明确声光电扩展版（青少年更容易理解）](https://gitee.com/siger99/Wifi-Oximiter-using-MAX30102-and-ESP32/issues/I6IVZS#note_17055152_link)
    - [v0.6 聚焦机器人扩展，增加细分（多足、轮式），增加视觉传感+FPGA学习扩展](https://gitee.com/siger99/Wifi-Oximiter-using-MAX30102-and-ESP32/issues/I6IVZS#note_17055194_link)

- [GROVE BEGINNER KIT for Arduino by Seeed](https://gitee.com/siger99/Wifi-Oximiter-using-MAX30102-and-ESP32/issues/I6I7T5) | 15 | 2个月前

  - [关于我们](https://www.seeedstudio.com/about-us/) （[转发](https://gitee.com/siger99/Wifi-Oximiter-using-MAX30102-and-ESP32/issues/I6I7T5#note_16510427_link)） 2个月前
  - [Home  Microcontroller Unit  FPGA  Spartan Edge Accelerator Board - Arduino FPGA Shield with ESP32](https://www.seeedstudio.com/Spartan-Edge-Accelerator-Board-p-4261.html?queryID=e315198e4a69219887923dc026c56e8c&objectID=4261&indexName=bazaar_retailer_products) （[转发](https://gitee.com/siger99/Wifi-Oximiter-using-MAX30102-and-ESP32/issues/I6I7T5#note_16510438_link)） 2个月前
  - [Home  Microcontroller Unit  Sipeed  Sipeed Tang Nano FPGA Board Powered by GW1N-1 FPGA](https://www.seeedstudio.com/Sipeed-Tang-Nano-FPGA-board-powered-by-GW1N-1-FPGA-p-4304.html?queryID=e315198e4a69219887923dc026c56e8c&objectID=4304&indexName=bazaar_retailer_products) （[转发](https://gitee.com/siger99/Wifi-Oximiter-using-MAX30102-and-ESP32/issues/I6I7T5#note_16510439_link)） 2个月前
  - [Seeeduino XIAO](https://gitee.com/siger99/Wifi-Oximiter-using-MAX30102-and-ESP32/issues/I6I7T5#note_16510450_link) 2个月前
  - [傻傻分不清](https://gitee.com/siger99/Wifi-Oximiter-using-MAX30102-and-ESP32/issues/I6I7T5#note_16510451_link)： www.sipeed.com 矽速科技 & www.seeedstudio.com 矽递科技 & www.uctechip.com 优矽科技

- [Seeed Studio XIAO 套件](https://gitee.com/siger99/Wifi-Oximiter-using-MAX30102-and-ESP32/issues/I6I7T5#note_17581240_link) (XIAO+材大用.pdf) 2天前

  - [第一单元 硬件及编程入门（XIAO 系列均可用）](https://gitee.com/siger99/Wifi-Oximiter-using-MAX30102-and-ESP32/issues/I6I7T5#note_17581251_link)
  - [第二单元 项目实践初级 —— 原型设计入门（XIAO 系列均可用）](https://gitee.com/siger99/Wifi-Oximiter-using-MAX30102-and-ESP32/issues/I6I7T5#note_17581256_link)
  - [第三单元 项目实践中极 —— 复杂项目（ 第 11-13 课 XIAO 系列均可用，第 14、15 课需要 XIAO ESP32C3）](https://gitee.com/siger99/Wifi-Oximiter-using-MAX30102-and-ESP32/issues/I6I7T5#note_17581263_link)
  - [第四单元 项目实践高级 —— TinyML 应用（仅针对 XIAO nRF52840Sense）](https://gitee.com/siger99/Wifi-Oximiter-using-MAX30102-and-ESP32/issues/I6I7T5#note_17581266_link)
  - [第五单元 自研项目（XIAO 系列均可用）](https://gitee.com/siger99/Wifi-Oximiter-using-MAX30102-and-ESP32/issues/I6I7T5#note_17581270_link)

- [Arduino+图形化编程入门-用+Codecraft+玩转+Grove+极简+Arduino+入门套件-+V4+1203](https://gitee.com/siger99/Wifi-Oximiter-using-MAX30102-and-ESP32/issues/I6I7T5#note_17581283_link)

  入门套件课程

  1. 我的第一个.Arduino.程序：Blink
  2. 控制.LED.灯的亮度
  3. 循环与变量——.LED.“呼吸灯”
  4. 条件语句——按钮控制开关灯
  5. 用旋钮调节.LED.亮度
  6. 摩尔斯电码发报机和音乐盒
  7. 点亮.OLED.显示屏，开启可视化交互
  8. 那些默不作声的硬件真的在工作吗？
  9. “看见”声音与制作声控灯
  10. 光控灯
  11. 气压计与高度计
  12. 小小气象站
  13. 三轴加速度计——运动与平衡

  - [入门套件 + 教育扩展包的课程](https://gitee.com/siger99/Wifi-Oximiter-using-MAX30102-and-ESP32/issues/I6I7T5#note_17582400_link)

    14. 扩展项目 1：智能加湿器
    15. 扩展项目 2：可转头的遥控电风扇
    16. 扩展项目 3：自动报警宝箱.

  - [OLED 不需要！其他合并，接ARDUINO标准 UNO 接口，以及 XIAO 扩展槽。](https://gitee.com/siger99/Wifi-Oximiter-using-MAX30102-and-ESP32/issues/I6I7T5#note_17582620_link)

- [NRE成本: NRE是Non-Recurring Engineering的缩写，NRE费用即一次性工程费用](https://gitee.com/siger99/Wifi-Oximiter-using-MAX30102-and-ESP32/issues/I6I7T5#note_17585079_link)