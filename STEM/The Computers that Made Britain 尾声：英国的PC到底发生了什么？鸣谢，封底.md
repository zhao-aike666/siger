 **尾声: 英国的PC到底发生了什么?**

Acorn Archimedes (阿基米德) 是20世纪英国最后一台伟大的计算机。是的，当时有很多不错的 基于 视窗 (Windows) 的电脑 —— 即使在今天，作为《PC Pro》杂志的编辑，我在日常工作中还是建议，人们购买台式电脑的首选是这个国家最好的定制PC制造商之一。但他们使用的是现成的中国制造的箱子，里面装满了来自遥远国家的零部件。无论你如何努力，都无法回避这样一个事实:现在的主流是由美国、中国和台湾的公司主导的。

　　要完全理解其中的原因需要一本全新的书，但这一转变背后的两个关键因素是规模和标准化。1981年，当IBM进入个人电脑市场时，文字就出现在 磷酸盐涂层 (phosphate-coated) 的屏幕上。为了竞争，你要么需要创造出一个令人惊叹的平台，让人们争相为其编写软件和游戏，要么你需要跳到与IBM相同的平台上，以某种方式将你的计算机从所有其他计算机中区分出来。

　　让我们考虑第一个可能性: 创建自己的平台。事实证明，这是相当棘手的。即使是苹果公司在与微软的竞争中也举步维艰，直到1996年史蒂夫·乔布斯从流放中归来。你可以认为, 谷歌的成功与 chrome 上网本 显示了 第三方玩家有关, 但这给了我们一个提示: 谷歌是拥有全球化实力的，远远超出他们的规模，包括： Acorn, Amstrad, and Sinclair 可能是所有前面提到的四个一起。

　　很明显，只有 Amstrad 和它的企业家老板在美国留下了印象。试图把 BBC Micro 带到美国几乎让 Acorn 破产，而 Sinclair 在跨大西洋的冒险中采取了更谨慎的方式，与 Timex 合作。事实上，在这个充斥着巨头的市场中，没有一家英国公司是只小角色。就连 1984年 拥有数十亿美元业务的 Commodore 也很快被 IBM 复制品的重压压垮了。

　　在 IBM 个人电脑兼容的阴霾中，只有一个英国的反叛者: Acorn。尽管当时该公司为 Olivetti (奥利维) 所有，但它还是说服 BBC 将其革命性的 ARM 处理器 押在了 Acorn Archimedes（阿基米德橡果）上。Archimedes 战斗了十多年，最终屈服于不可避免的结果。

　　这就是为什么英国人在 20世纪80年代末 都尝试开发 IBM 兼容的个人电脑 —— 包括 Amstrad。但当每台电脑本质上都一样时，你如何区分自己呢? 同样，英国公司无法与之匹敌，事实证明，戴尔激进的定价和 “及时” 的交货模式是许多英国公司垮台的决定性因素。

　　有一些成功的故事，大部分都是建立在教育上的。Viglen 是 Amstrad 在 1994年 买下的一家公司，20年后将其出售。Research Machines 一直在生产自己的电脑，直到几年前，他们的 “附加值” 是为学校提供支持和建议。他们提供的是问题的解决方案，而不是包含零件的盒子。并且仍然会这样做。

　　然后，在 2012 年，在这片贫瘠的英国荒原上，一个绿色的萌芽。它再次出现在剑桥，这一次是研究生 Eben Christopher Upton 艾本·克里斯托弗·厄普顿 的想法。在 杰克·朗 (Jack Lang) 的大力鼓励下，他与人共同创立了树莓派基金会 (Raspberry Pi Foundation)，以开发低成本的单板计算机。杰克·朗 本人是一位连续创业家，但关键是他长期在剑桥大学计算机实验室 (University of Cambridge’s Computer Laboratory) 担任讲师。

　　第一款树莓派于2012年2月以22英镑的价格发售，八年内它成为英国有史以来最畅销的电脑: 2019年12月14日，Upton （厄普顿）在推特上随意地写道: “我们上周 (我们认为是周二) 卖出了第三千万部。”

　　这意味着树莓派现在正式成为有史以来最畅销的电脑。那么，英国的个人电脑怎么样了? 它就在这里，而且比以往更成功。让我们希望它能激励另一代英国程序员，这正是树莓派基金会成立的目的。

### P280 - P281

 **鸣谢** 

说这本书是一个团队的努力是一个轻描淡写的说法，大致相当于 “BBC Micro 是相当重要的英国计算机”。如果您不介意这么计算比喻，您之前看到的成果大概需要数百人的码字才可能实现的。

　　首先，我必须感谢树莓派出版社的团队，是他们让这一切成为现实。排名第一的是 Simon Brew 西蒙·布瑞。他是那个不经意间问我是否对一本书感兴趣的人，在当时它还只是个标题。然后，他热情地把这个想法卖给了树莓派出版社的负责人 Russell Barnes。和 Simon 和 Russell 一起，我们把这个想法变成了一个大纲，它最终变成了一本书。

p281

　　我还要感谢国家计算机博物馆的所有工作人员，尤其要感谢 Jacqui Garrard，当我坐在 café 和图书馆研读过期的《个人计算机世界》(人们亲切地称其为 PCW )时，她给予了我无限的能量和包容。

　　然后是确定哪些计算机应该包括的工作。20世纪80年代英国电脑的销售数字很难确定。在推特上数百人的帮助下，我们从 100 个左右 挤到了这本书的 19个。所以感谢所有花时间填写我们最初调查的人。

　　在确定了简短的名单后，我渴望与尽可能多的参与其中的人交谈。引荐我进入这个稀有领域的是 杰克·朗 (Jack Lang)，他是树莓派基金会 (Raspberry Pi Foundation) 的联合创始人、连续创业家，也是剑桥大学 (University of Cambridge) 计算机实验室 (Computer Lab) 的长期讲师。他让我直接接触了这些文章中引用的一些人，这就足够让事情开始了。这有助于一个人认识另一个人，认识另一个人，你懂的。我还要感谢 Peter Rennison 彼得·雷尼森，是他让我接触到了连 杰克 都联系不上的人。

　　我对我的受访者感激不尽。他们无一例外地愿意放弃自己的时间来重温他们创造的计算机的诞生。对有些人来说，这只是 20分钟 的对话。另一个极端是广泛讨论长达 4个小时到 扩展成几天的时间里。

　　这些受访者是: Tudor Brown 都铎·布朗，John Caswell 约翰·卡斯韦尔，Charles Cotton 查尔斯·科顿，Christopher Curry 克里斯托弗·库里，Mike Fischer 迈克·费舍尔，Steve Furber 史蒂夫·弗伯，Richard Harding 理查德·哈丁，Neil Harris 尼尔·哈里斯，Hermann Hauser 赫尔曼·豪泽，Andrew Herbert 安德鲁·赫伯特，Andy Hopper 安迪·霍珀，Jan Jones 简·琼斯，Mark-Eric Jones 马克-埃里克·琼斯，David Karlin 大卫·卡林，Tim King 蒂姆·金，John Mathieson 约翰·马西森，Richard Miller 理查德·米勒，Mike O’Regan 迈克·奥里根，Roland Perry 罗兰·佩里，Nigel Searle 奈杰尔·塞尔，Kit Spencer 基特·斯宾塞，Ian Thompson-Bell 伊恩·汤普森-贝尔，Michael Tomczyk 迈克尔·汤姆奇克，Leonard Tramiel 伦纳德·特拉梅尔，Chris Turner 克里斯·特纳，还有(最后一个，但不是任何可测量的最少的) Sophie Wilson 索菲·威尔逊。

　　不仅感谢您们的受访时间，也感谢您们足够耐心地检查了这本书的终稿。

　　我还采访了两位记者，当我阅读当代报纸报道和杂志评论时，他们的名字不断出现。Dick Pountain 迪克·庞顿，从他在 Byte 杂志和 PCW 的时候就已经是一个传奇人物了，他后来和别人一起创办了 PC Pro 杂志。谢谢你，迪克，感谢你在电话里怎么那么有耐心，给我们回顾你广泛的职业生涯的最初几年。

p282

　　我还与《卫报》长期从事计算机编辑工作的 杰克·斯科菲尔德 (Jack Schofield) 进行了详细交谈，他于 2020年3月去世。在过去的几个月里，我读了他从 20世纪80年代 以来的无数文章，他的知识和绝对的智慧交织在每一篇文章中。他继续在每周的 “问杰克” 专栏中为他的大批读者提供明智的建议。说人们会非常想念他，还远远不够。

　　特别感谢 鲁伯特·古德温 (Rupert Goodwins)。鲁珀特是世界上最罕见的生物: 他不仅是一个非常专业的作家，他还格外的 —— 我讨厌用这个词 —— NICE。为了让鲁伯特感兴趣，我给了他一本这本书的早期版本，让他检查我在哪些地方引用了他的话。然后他花自己的时间仔细地浏览了一遍，找出错误，并以一种我永远感激的方式要求我解释。虽然我要为漏掉的任何错误负全部责任，但鲁珀特发现了许多我从未发现的假设和错误。谢谢你,鲁珀特。

　　最后，我要感谢我的家人。Kim 金姆、Fraser 弗雷泽、Rowan 罗文和 Penelope 佩内洛普 几个月来一直忍受我工作到深夜，因为我试图把无数的资料整理成一本书。感谢你们的耐心和良好的幽默感。我还要感谢我的父亲 Stuart Danton斯图尔特·丹顿，没有他，我就不会写下这些话。毕竟是他拍摄了我六岁时在一台 MSI 6800 System One micro 前, 玩 《罗杰·康威的游戏生活》(Roger Conway’s Game of Life), 这是我人生第一次出现在电脑杂志（1979年6月个人电脑世界），关于 MSI 计算机 的回顾。

### P281 - P283



![输入图片说明](https://images.gitee.com/uploads/images/2021/1119/174547_c495d965_5631341.png "屏幕截图.png")

- The home computer boom of the1980s brought with it now iconicmachines such as the ZX Spectrum,BBC Micro, and Commodore 64. Thosemachines would inspire a generation.

- The Computers That Made Britain tells the story of 19 of those computers – and what happened behind the scenes.

- With dozens of new interviews, discover the tales of missed deadlines, technical faults, business interference, and the unheralded geniuses who brought to the UK everything from the Dragon 32 and ZX81, to the Amstrad CPC 464 and Commodore Amiga.

### 封底

- 20 世纪 80 年代的家用计算机热潮带来了现在标志性的机器，如 ZX Spectrum (频谱)、BBC Micro 和 Commodore (指挥官) 64。这些机器将激励一代人。

- 《英国人制造的计算机》讲述了其中 19 台计算机的故事 —— 以及他么背后发生的故事。

- 通过数十次新的采访，发现错过最后期限、技术故障、业务干扰的故事，以及那些将 Dragon 32 和 ZX81 , Amstrad CPC 464 和 Commodore Amiga 等一切带到英国的无名天才的故事。
