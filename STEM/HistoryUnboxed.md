<p><img width="706px" src="https://images.gitee.com/uploads/images/2022/0704/012306_7999e710_5631341.jpeg" title="HISUNBOX.jpg"></p>

> 当孩子们看到一大盒动手做的材料时，惊现在脸上的笑容。我回想起带着20盒火焰棋DIY材料去雅安灾区的场景，那已经是 多年前的事情了。这个标准的教育场景，原来是这么经久不衰，而下面推荐的一个网站的标题，出现了中国先哲的名字荀庄，让我无法拒绝：“实践出真知” 的道理我们不需要崇洋媚外，是我们思想懒惰啦啊！ :pray: 看老外用心地内置了一对兵马俑，各种零件一看就是某宝配货的，让我们多少有些无奈，我们的历史学的不认真啊！

---

![输入图片说明](https://images.gitee.com/uploads/images/2022/0702/011256_68690a93_5631341.png "屏幕截图.png")

historyunboxed.com

# Welcome to History Unboxed!

![输入图片说明](https://images.gitee.com/uploads/images/2022/0702/011311_6778ad68_5631341.png "屏幕截图.png")

We are passionate about bringing history to life for all students.  We are so glad you are joining us on this mission. As a thank you, here is a link to [download our mini-lesson](https://historyunboxed.us9.list-manage.com/track/click?u=04b75775ca08f3732cdf5bfc3&id=15cd4750c4&e=4e13c113d8) on the Islamic Empire. 

If you have questions, don't hesitate to reach out to hello@historyunboxed.com 

![输入图片说明](https://images.gitee.com/uploads/images/2022/0702/011349_5f101305_5631341.png "屏幕截图.png")

Hands-on history lessons from around the world, delivered to your door  
"Tell me and I forget. Teach me and I remember. Involve me and I learn."
- Chinese proverb from Xun Zuang

来自世界各地的实践历史课程，送到您家门口  
“告诉我，我就忘记了。教我，我记住。让我参与，我学习。”
- 来自荀庄的中国谚语

### [Mysteries of the Rubber People: The Olmecs](https://www.historyunboxed.com/shop/booksanddownloads/mysteries-of-the-rubber-people-the-olmecs/)

$4.99

Olmecs: The Mother Culture of Mesoamerica Deep in the jungle, among the rubber trees, the Olmec people lived thousands of years ago. Did you know they harvested latex into rubber? Did you know they were the first to drink chocolate? They loved to play sports. They believed in shape shifting...

![输入图片说明](https://images.gitee.com/uploads/images/2022/0702/012800_a0fcaf5f_5631341.png "屏幕截图.png")

### [Poetry Tea Time Mini Box – Limited Release](https://www.historyunboxed.com/shop/boxes/poetry-tea-time-mini-box-limited-release/)

From $12.99

Celebrate National Poetry Month with Historic Poetry Teatime Decorate your own functional tea service in a historic English style Enjoy two historic teas Read some of the most famous and exciting poems in history ...and more! Our high-quality hands-on history kits bring American...

![输入图片说明](https://images.gitee.com/uploads/images/2022/0702/012857_2e2a9fa3_5631341.png "屏幕截图.png")

### [Benin Unboxed](https://www.historyunboxed.com/shop/boxes/benin-unboxed/)

From $32.97

Discover the beautiful art and folklore of the kingdom of Benin in the Middle Ages. Inside this Box Discover how snail shells were used as currency and make your money cowrie jewelry Enjoy using a traditional Nigerian bar soap Learn to play traditional African and modern songs on a...

![输入图片说明](https://images.gitee.com/uploads/images/2022/0702/012945_9d75a523_5631341.png "屏幕截图.png")

### [Ancient History: A Secular Exploration of the World Book](https://www.historyunboxed.com/shop/booksanddownloads/ancient-history-a-secular-exploration-of-world-history-2/)

$29.99

Explore the ancient world on a tour of the most beautiful, vibrant, and fascinating places people have called home. History Unboxed takes you outside the box of a traditional history curriculum, exploring cultures around the globe on a journey across six continents in this ancient history...

![输入图片说明](https://images.gitee.com/uploads/images/2022/0702/013021_2cbc0890_5631341.png "屏幕截图.png")

### [American History Full Curriculum](https://www.historyunboxed.com/shop/full-curricula/american-history-full-curriculum/)

From $197.82

Learn the history of the United States of America and gain a new appreciation for the people, events, and cultures that came together in the rich history of our nation! Our complete, stand-alone American History Curriculum consists of 12 boxes with individual lesson plans, reading lists, a...

![输入图片说明](https://images.gitee.com/uploads/images/2022/0702/013101_c9a12687_5631341.png "屏幕截图.png")

### [Middle Ages Full Curriculum](https://www.historyunboxed.com/shop/full-curricula/middle-ages-full-curriculum/)

From $164.85

Adventure in the middle ages as we travel to some of the most amazing, fascinating, and thrilling times in history! Our middle ages timeline spans from 500 AD/CE to 1350 AD/C Our complete, stand-alone Middle Ages Curriculum consisting of 10 boxes with individual lesson plans, reading lists,...

![输入图片说明](https://images.gitee.com/uploads/images/2022/0702/013145_127c968e_5631341.png "屏幕截图.png")

### [Ancient History Full Curriculum](https://www.historyunboxed.com/shop/full-curricula/ancient-history-full-curriculum/)

From $296.73

Start exploring the ancient world as we take you on a tour of 18 of the most beautiful, influential, and fascinating places in history! Our Ancient History Timeline spans from 3500 BC/BCE to 500 AD/CE. Our complete, stand-alone Ancient History Curriculum consisting of 18 boxes with individual...

![输入图片说明](https://images.gitee.com/uploads/images/2022/0702/013225_3cc9db2a_5631341.png "屏幕截图.png")

### [Jamestown Unboxed](https://www.historyunboxed.com/shop/boxes/jamestown/)

From $32.97

Inside Jamestown Unboxed Find out why the colonists of Jamestown dined on rats and then try (a slightly more delicious) snack yourself Discover the truth about John Smith and Pocahontas and make a corn husk doll, just like Pocahontas would have played with Recreate an essential classroom...

![输入图片说明](https://images.gitee.com/uploads/images/2022/0702/013308_f8a3d1f7_5631341.png "屏幕截图.png")

### [Mongols Unboxed](https://www.historyunboxed.com/shop/boxes/mongols-unboxed/)

From $32.97

Inside this Box Decorate and play games with Mongolian Shagai Bones Learn about the importance of wool and try a felting project Taste a drink based on a traditional Mongolian yogurt Learn about the amazing life of a man who is still respected by some and despised by others: Genghis...

![输入图片说明](https://images.gitee.com/uploads/images/2022/0702/013401_cd307963_5631341.png "屏幕截图.png")

### [Egypt Unboxed](https://www.historyunboxed.com/shop/boxes/egypt-unboxed/)

From $32.97

Inside this Box Mummify an apple Make an ancient dessert Read a 4000 year old Cinderella story Play the world's oldest board game Decorate a sarcophagus Color a Pyramid ...and more! Our high-quality hands-on history kits bring ancient civilizations to life for learners of...

![输入图片说明](https://images.gitee.com/uploads/images/2022/0702/013448_76223981_5631341.png "屏幕截图.png")

[History Unboxed](https://www.historyunboxed.com/)

- Shop
- About History Unboxed
- FAQ
- Educators
- Affiliate Program
- Contact

Home / About History Unboxed
# [About History Unboxed](https://www.historyunboxed.com/history-box-subscriptions/)

Let us help you discover history in a whole new way.
> 让我们帮助您以全新的方式探索历史。

Nothing brings life to learning like a hands-on history box full of stories, crafts, and more, delivered to your door!
> 没有什么能像一个装满故事、手工艺品等的实践历史盒子，送到您家门口，让学习充满生机！

  ![输入图片说明](https://images.gitee.com/uploads/images/2022/0702/014016_af07bf1a_5631341.png "屏幕截图.png")

  - Stories from Around the World 来自世界各地的故事

    > Immerse your students in lessons that bring the cultures, peoples, and eras of human history to life! Stories, objects, recipes and more from far-off centuries and continents bring learning well beyond the classroom. 让您的学生沉浸在将人类历史的文化、民族和时代带入生活的课程中！  来自遥远世纪和大洲的故事、物品、食谱等使学习远远超出课堂。

  ![输入图片说明](https://images.gitee.com/uploads/images/2022/0702/014031_5925200d_5631341.png "屏幕截图.png")

  - Hands-On Learning 动手学习

    > Your student will benefit from hands-on activities that bring the lessons to life and greatly aid retention! Watch as your child paints, assembles, builds, flies, cooks, colors, and weaves their way into the eras and cultures of each box. 您的学生将从实践活动中受益，这些活动将课程带入生活并极大地帮助学生保留！  看着您的孩子在每个盒子的时代和文化中绘画、组装、建造、飞行、烹饪、着色和编织。

  ![输入图片说明](https://images.gitee.com/uploads/images/2022/0702/014046_592d90d1_5631341.png "屏幕截图.png")

  - Everything you need! 你需要的一切！

    > Each box contains everything you need to complete your crafts, plus pages of instructional material representing hundreds of hours of work from professional historians, artists, and educators 每个盒子都包含完成工艺所需的一切，以及代表专业历史学家、艺术家和教育工作者数百小时工作的教学材料页面。



Teaching history from a textbook? Whether you are a homeschool family, charter school parent or classroom teacher, there is a better way! 教科书上的历史？  无论您是在家上学的家庭、特许学校家长还是任课老师，都有更好的方法！

Students love learning history the Unboxed way! Make your curriculum come alive.学生们喜欢以 Unboxed 方式学习历史！  让您的课程生动起来。

The adventure begins when you order a single box, sign up for a subscription or purchase a complete curriculum! 当您订购一个盒子、注册订阅或购买完整的课程时，冒险就开始了！

If your order includes a welcome box, you receive:  **如果您的订单包含欢迎箱，您将收到：** 

- An interactive timeline poster
- A welcome letter and coloring sheet
- A time capsule kit to create your own historic memento
- A worksheet to help students understand how to read a timeline
- A Family Tree kit for exploring your student’s personal history
- 交互式时间线海报
- 欢迎信和彩页
- 一个时间胶囊套件，用于创建您自己的历史纪念品
- 帮助学生了解如何阅读时间线的工作表
- 用于探索学生个人历史的家谱套件

For subscriptions, each month you will receive a new box exploring a different place in time, according to the era you selected. For single boxes or a complete curriculum, everything arrives at once, for you to use when you are ready. Each box contains: 对于订阅，每个月您都会收到一个新盒子，根据您选择的时代及时探索不同的地方。  对于单个盒子或完整的课程，一切都会立即到达，供您在准备好时使用。  每个盒子包含：

- A age-specific coloring sheet
- 1-2 (very!) high quality crafts
- A sticker for your timeline
- 2-3 items of additional enrichment material
- 特定年龄的着色表
- 1-2（非常！）高品质工艺品
- 时间线贴纸
- 2-3项额外的浓缩材料

For the Ancient and Middle Ages subscriptions, the box you receive will depend on the month, and all subscribers will receive the same box that month—great for collaborating with friends who are also subscribers! 对于古代和中世纪订阅，您收到的盒子取决于月份，所有订阅者当月都会收到相同的盒子 - 非常适合与也是订阅者的朋友合作！

For the American History subscription, boxes arrive according to a set sequence, so as to build the story of our nation chronologically. 对于美国历史订阅，盒子按照设定的顺序到达，以便按时间顺序构建我们国家的故事。

---

- There are three timelines to explore with each month’s unique, immersive history box: 每个月独特的身临其境的历史盒子可以探索三个时间表：

  - Ancient History 古代历史

    ![Icon depicting Ancient History](https://images.gitee.com/uploads/images/2022/0702/015114_9e716c26_5631341.png " 描绘古代历史的图标.png")

    > Start exploring the ancient world as we take you on a tour of 18 of the most beautiful, influential, and fascinating places in history! 开始探索古代世界，我们将带您游览 18 个历史上最美丽、最有影响力和最迷人的地方！

    > Our Ancient History Timeline spans from 5000 BC/BCE to 400 AD/CE.
Middle Ages 我们的古代历史时间表跨越从 5000 BC/BCE 到 400 AD/CE。

  - Middle Ages 中世纪

    ![输入图片说明](https://images.gitee.com/uploads/images/2022/0702/015211_cf56cf07_5631341.png "屏幕截图.png")

    > Adventure in the middle ages as we travel to some of the most amazing, fascinating, and thrilling times in history! 当我们穿越到历史上最神奇、最迷人、最激动人心的时代时，在中世纪进行冒险！

    > Our middle ages timeline spans from 500 AD/CE to 1350 AD/CE. 我们的中世纪时间线跨度从公元 500 年到公元 1350 年。

  - American History (USA) 美国历史（美国）

    ![输入图片说明](https://images.gitee.com/uploads/images/2022/0702/015238_5a2c8ce5_5631341.png "屏幕截图.png")

    > Learn the History of the United States of America and gain a new appreciation for the people, events, and cultures that formed our great nation! 了解美利坚合众国的历史，对形成我们伟大国家的人民、事件和文化有新的认识！

    > Start with the three major cultures that came together in our beginnings (Africa, Europe, and Native American) and then learn about colonial life, the Revolutionary War, our Constitution, and more.  从我们最初融合的三种主要文化（非洲、欧洲和美洲原住民）开始，然后了解殖民生活、独立战争、我们的宪法等。

    > Our American History timeline runs through the Civil War. 我们的美国历史时间表贯穿内战。

---

- Each history box is available in three different versions, to keep the materials engaging for every age group! 每个历史记录框都提供三种不同的版本，以使材料对每个年龄段的人都有吸引力！

  ![输入图片说明](https://images.gitee.com/uploads/images/2022/0702/015816_52bf495f_5631341.png "屏幕截图.png")

  > Highly visual, approachable materials for younger students that can be explored with the help of an educator. In addition to craft materials, recipes, and souvenirs our monthly magazine helps give educators the background they need to teach about the topic of the month. 面向年轻学生的高度可视化、平易近人的材料，可以在教育工作者的帮助下进行探索。  除了工艺材料、食谱和纪念品，我们的月刊还帮助教育工作者了解他们需要教授本月主题的背景。

  ![输入图片说明](https://images.gitee.com/uploads/images/2022/0702/015830_308c01ca_5631341.png "屏幕截图.png")

  > An in-depth, relatable version of the subscription that can be completed mostly independently. Learn about the past through the eyes of a teenager and enjoy memorable crafts, recipes, stories, and mementos. 一个深入的、相关的订阅版本，可以大部分独立完成。  通过青少年的眼光了解过去，享受令人难忘的手工艺品、食谱、故事和纪念品。

  ![输入图片说明](https://images.gitee.com/uploads/images/2022/0702/015840_7469200d_5631341.png "屏幕截图.png")

  > Relax and become inspired with a monthly history fix as you try your hand at a wide variety of crafts, historic recipes, and history based games. Bite sized bits of history fit into your busy schedule and provide you with fun information you will want to share with your friends. 当您尝试各种手工艺品、历史食谱和基于历史的游戏时，您可以通过每月的历史修复来放松并获得灵感。  一口大小的历史片段适合您繁忙的日程安排，并为您提供您想与朋友分享的有趣信息。

en

- [Explore the Boxes](https://www.historyunboxed.com/shop/products/boxes/)
- [Try a Sample Lesson](https://www.historyunboxed.com/sample-lesson/)
- [Check Out the Subscriptions](https://www.historyunboxed.com/shop/products/subscriptions/)

cn

- 探索盒子
- 尝试示例课程
- 查看订阅

---

### Meet the Team 认识团队

- Educational Team 教育团队
 
  ![](https://images.gitee.com/uploads/images/2022/0702/020153_52a96bd8_5631341.png "屏幕截图.png")

  - Image of Elizabeth playing with her kids 伊丽莎白和她的孩子们玩耍的形象

    > Hi! I’m Elizabeth. Small business enthusiast, creative force, and mom to 7 excellent children. 你好！  我是伊丽莎白。  小企业爱好者、创造力和 7 个优秀孩子的妈妈。

    > As a child I volunteered at a living history museum and went on to co-direct the educational programs there as an adult. 小时候，我在一个活生生的历史博物馆做志愿者，成年后继续共同指导那里的教育项目。


    > I also have home educated my own children for the last eleven years and taught at a local co-op for two years… so the idea of bringing hands-on history enrichment activities to your family is both very natural and very exciting for me! As homeschooling families know, textbooks are great, but nothing beats hand-on learning activities! 在过去的 11 年里，我还让自己的孩子在家接受教育，并在当地的合作社任教了两年……因此，为您的家人带来亲身体验历史丰富活动的想法对我来说既自然又令人兴奋！  正如在家上学的家庭所知道的那样，教科书很棒，但没有什么比动手学习活动更好的了！


    > Thank you for supporting my family business and allowing me to share the learning that I love. 感谢您支持我的家族企业并让我分享我热爱的学习。

  ![输入图片说明](https://images.gitee.com/uploads/images/2022/0702/020141_aec9d8aa_5631341.png "屏幕截图.png")

  - A Caucasia woman wearing glasses and and orange and red dress stands in front of a blurry background of a forest on a sunny winter day. 在阳光明媚的冬日，一位戴着眼镜、身穿橙色和红色连衣裙的高加索妇女站在森林模糊的背景前。

    > Hi! I’m Stephanie. My passion for history began at an early age, sparked by the American Girl books. 你好！  我是斯蒂芬妮。  我对历史的热情从很小的时候就开始了，受到美国女孩书籍的启发。

    > In college, I majored in history, and then went back to school for a Master’s Degree in Elementary Education.  I spent eight years in various classrooms: a Montessori preschool, substitute teaching every grade K-12, and finally two years teaching fifth grade. I’ve always enjoyed finding just the right curriculum and resources for my students. Now, with History Unboxed, I develop learning resources that take students and teachers beyond textbooks, and I find that very exciting. 在大学，我主修历史，然后回到学校攻读基础教育硕士学位。  我在各种教室里呆了八年：蒙台梭利幼儿园，K-12 每个年级的代课老师，最后两年教五年级。  我一直很喜欢为我的学生找到合适的课程和资源。  现在，通过 History Unboxed，我开发了让学生和教师超越教科书的学习资源，我觉得这非常令人兴奋。


    > I have worked on the farm at a local historic home, educating the public about farm life in the eighteenth century. I also do living history as a hobby and dabble in spinning, weaving, and homesteading. 我曾在当地一座历史悠久的住宅的农场工作，向公众宣传 18 世纪的农场生活。  我还将生活历史作为一种爱好，并涉足纺纱、编织和宅基地。

    > I’m now a home educating mom of four kids who love hands-on activities as much as I do. 我现在是一个有四个孩子的家庭教育妈妈，他们和我一样喜欢动手活动。


- Artistic Team 艺术团队


  ![输入图片说明](https://images.gitee.com/uploads/images/2022/0702/020310_d0f6dbe8_5631341.png "屏幕截图.png")

  - Avatar image of David 大卫头像

    > My name’s David. I’m 49 and work as a computer programmer and freelance artist. 我叫大卫。  我今年 49 岁，是一名计算机程序员和自由艺术家。

    > I’ve always enjoyed comics and art in general, and history is one of the things I enjoy reading. It’s been really fun to learn about a new piece of history each month and watch as my two sons get excited over what I’m drawing.  总的来说，我一直很喜欢漫画和艺术，而历史是我喜欢阅读的东西之一。  每个月都能了解一段新的历史，看着我的两个儿子对我的画感到兴奋，这真的很有趣。

    > With History Unboxed, my art helps history come alive through textbook style resources as well as the fun coloring sheets and other learning materials. 随着 History Unboxed，我的艺术通过教科书风格的资源以及有趣的着色表和其他学习材料帮助历史变得生动。

  ![输入图片说明](https://images.gitee.com/uploads/images/2022/0702/020338_1d89fc88_5631341.png "屏幕截图.png")

  - Image of Matt in his office 马特在他办公室的照片

    > Matt Maley is a working artist with over twenty-five years in the fields of  Illustration, design and sculpture. He also teaches after-school cartooning classes, and incorporates his love of history, science and the environment into much of his work. Matt Maley 是一位在插画、设计和雕塑领域拥有超过 25 年经验的艺术家。  他还教授课后漫画课程，并将他对历史、科学和环境的热爱融入到他的大部分工作中。


    > Matt creates exceptional learning resources exclusively for History Unboxed, contributing to the complete, multi-sensory learning experience. Matt 专门为 History Unboxed 创建了卓越的学习资源，为完整的多感官学习体验做出了贡献。

    > Matt lives in New Paltz, NY with his wife, daughter, six chickens, one dog and a cat (when she’s around). [www.mattmaley.com](http://www.mattmaley.com) 马特和他的妻子、女儿、六只鸡、一只狗和一只猫（当她在附近时）住在纽约新帕尔茨。  www.mattmaley.com

### [Sample Lesson: Islamic Empire](https://www.historyunboxed.com/sample-lesson/)

Looking for a preview of a sample lesson with History Unboxed? Here you'll get a taste of what it's like to time travel with us. 

![输入图片说明](https://images.gitee.com/uploads/images/2022/0702/021434_ccea9858_5631341.png "屏幕截图.png")

When our boxes arrive, the first thing my kids do is open it up and take everything out as if it's a birthday present. 当我们的盒子到达时，我的孩子们做的第一件事就是打开它，把所有东西都拿出来，就好像它是生日礼物一样。

This box was Islamic Empire Unboxed for 10-15 year olds with two sibling add-ons. It included: 这个盒子是为 10 到 15 岁儿童拆箱的Islamic帝国，带有两个兄弟姐妹附加组件。  它包括：

- A prism pendant kit for each student
- A miswak (stick toothbrush) for each student
- Rock candy kit for each student
- Coloring page for each student
- Dear Friend letter from a time traveling penpal
- Open and Read summary sheet for younger students
- 8 page magazine with articles and instructions
- Timeline sticker
- 每个学生都有一个棱镜吊坠套件
- 为每个学生准备一个 miswak（棒状牙刷）
- 每个学生的冰糖套装
- 每个学生的彩页
- 来自时间旅行笔友的亲爱的朋友的信
- 为年轻学生打开并阅读摘要表
- 包含文章和说明的 8 页杂志
- 时间线贴纸

![输入图片说明](https://images.gitee.com/uploads/images/2022/0702/022547_1e3c6b95_5631341.png "屏幕截图.png")

After we look at the box contents, we start the lesson. Usually I read the first page of the magazine while they color. 看完盒子内容后，我们开始上课。  通常我会在杂志的第一页上色时阅读它们。

Sometimes, we work through the activities in the order they are presented in the magazine. Other times, I see what most sparks their interest from the materials in the box. Is it going to be growing rock candy? Or making a prism pendant? Maybe it would be a good idea to eat the rock candy and then clean our teeth with the miswak (a traditional natural toothbrush). We usually read the accompanying magazine article at the beginning of each project. 有时，我们会按照活动在杂志中出现的顺序进行工作。  其他时候，我从盒子里的材料中看到最能激发他们兴趣的东西。  会长成冰糖吗？  还是制作棱镜吊坠？  也许吃冰糖然后用 miswak（一种传统的天然牙刷）清洁牙齿是个好主意。  我们通常在每个项目开始时阅读随附的杂志文章。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0702/021523_b45eb3be_5631341.png "屏幕截图.png")

This time, the kids wanted to start with the prism. I read to them about science in the Islamic Empire while they threaded their beads. 这一次，孩子们想从棱镜开始。  当他们穿珠子时，我给他们读了有关伊斯兰帝国的科学知识。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0702/021534_447806d1_5631341.png "屏幕截图.png") ![输入图片说明](https://images.gitee.com/uploads/images/2022/0702/021541_5f680749_5631341.png "屏幕截图.png")

Then it was time to make rock candy! Each kid got a turn at the stove. We always do sibling add-ons so everyone can join the fun with a minimum of sibling squabbles. 然后是时候制作冰糖了！  每个孩子都在炉子上轮流。  我们总是做兄弟姐妹的附加组件，所以每个人都可以通过最少的兄弟争吵来加入乐趣。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0702/021553_459cefa0_5631341.png "屏幕截图.png")

Since it takes several days for the sugar crystals to grow, we decided to try out the miswak. It was a very different experience for them! 由于糖晶体需要几天的时间才能生长，我们决定尝试这种错误。  这对他们来说是一次非常不同的经历！
 
![输入图片说明](https://images.gitee.com/uploads/images/2022/0702/021559_e62fd3df_5631341.png "屏幕截图.png")

We had fun watching the crystals develop over time. The kids were fascinated by the amount of growth. 我们很高兴看到晶体随着时间的推移而发展。  孩子们对成长的数量着迷。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0702/022122_4bc84706_5631341.png "屏幕截图.png")

Finally, it was time to eat! 终于到了吃饭的时间了！

![输入图片说明](https://images.gitee.com/uploads/images/2022/0702/021612_427dbf64_5631341.png "屏幕截图.png")

We chat about the discussion questions as we go and sometimes I add in books from the further reading list while they work on projects. After we've done everything that comes in the box, we might try enrichment suggestions in the box or check out additional ideas on Pinterest.  We wrap up our study by placing our timeline sticker on the History Unboxed timeline. 我们边走边聊讨论问题，有时我会在他们从事项目时从进一步阅读列表中添加书籍。  在我们完成了包装盒中的所有内容后，我们可能会尝试包装盒中的丰富建议或查看 Pinterest 上的其他想法。  我们通过将时间线贴纸贴在未装箱的历史时间线上来结束我们的研究。

A Caucasian girl with long brown hair smiles at the camera as she places a sticker on a light blue timeline. 一个长着棕色长发的高加索女孩在淡蓝色的时间线上贴上贴纸时对着镜头微笑。

Here are some sample pages for a closer look: 以下是一些示例页面，供您仔细查看：

![输入图片说明](https://images.gitee.com/uploads/images/2022/0702/021712_b47cd929_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0702/021717_3b6ad43e_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0702/021725_59b2d095_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0702/021730_2efb2833_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0702/021736_5ea96e25_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0702/021741_b92832a7_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0702/021747_8c9b1562_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0702/021752_889d0150_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0702/021758_176b0558_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0702/021956_e25c68e7_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0702/021810_aa44453a_5631341.png "屏幕截图.png")



Here are samples of our lesson plans with discussion questions and a step-by-step walk through of our materials: 以下是我们的课程计划示例，其中包含讨论问题和逐步浏览我们的材料：

![输入图片说明](https://images.gitee.com/uploads/images/2022/0702/021648_de4def4b_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0702/021654_44a29811_5631341.png "屏幕截图.png")

---

![输入图片说明](https://images.gitee.com/uploads/images/2022/0702/025129_97ab2c43_5631341.jpeg "20171028_115040-01.jpeg")![输入图片说明](https://images.gitee.com/uploads/images/2022/0702/025141_16d7995b_5631341.jpeg "IMG_0347_2048x2048.jpg")![输入图片说明](https://images.gitee.com/uploads/images/2022/0702/025156_3d67594c_5631341.jpeg "32968174_1231362223667216_110095920355344384_o.jpg")![输入图片说明](https://images.gitee.com/uploads/images/2022/0702/025208_048abb24_5631341.jpeg "hornbook.jpg")![输入图片说明](https://images.gitee.com/uploads/images/2022/0702/025221_a93fc791_5631341.jpeg "IMG_0196_2048x2048.jpg")![输入图片说明](https://images.gitee.com/uploads/images/2022/0702/025232_cf9dbf48_5631341.jpeg "weaving_2048x2048.jpg")![输入图片说明](https://images.gitee.com/uploads/images/2022/0702/025245_63f281b0_5631341.jpeg "20171201_131845-01.jpg")![输入图片说明](https://images.gitee.com/uploads/images/2022/0702/025300_1ffb31b5_5631341.jpeg "new-babylon.jpg")

---

# [堆历 | 我最喜欢中国谚语：实践出真知](https://jiemodui.com/N/85979.html)

作者：芥末堆 发布时间： 2017-11-13 18:18

![输入图片说明](https://images.gitee.com/uploads/images/2022/0702/165733_5cb705a0_5631341.png "屏幕截图.png")

GET2017教育科技大会明早即将开幕，Penni也为大家带来几位参会嘉宾最喜欢的教育名言和他们的故事。今天的来信人是纽约 Scholars' Academy（学者学院）的校长 Brian O'Connell。

他最喜欢的教育名言是一句中国谚语：百闻不如一见，千看不如一练。（也可以直译为：告诉我，我会忘记；给我看，我能记得；让我参与，我才真正明白。）

网传这句话还有来自蒙特梭利女士和本杰明·富兰克林的相似版本，不过可查到的最早的出处应该是《荀子·修身》：“不闻不若闻之，闻之不若见之，见之不若知之，知之不若行之。学至于行而止矣。行之，明也。”大意是说：道的实践是学习和认知的必然归宿，不闻不见之道，不是真正的仁道，它的实行终将以失败而告终。我们应努力去践履正道，因为道就在我们身边，不去努力实践，仍然达不到目的，再小的事情，不去做永远也不会成功。

总之，都是强调“实践”的重要性。而 Brian O'Connell 在27年的从教生涯中，也一直在努力实践这句话的道理。

### 请读他的故事：

我第一次听到这句话是在1991年。那时候，我刚刚成为小学老师，艾德菲大学的 Michael Wagner 教授告诉我这句话。Wagner教授同时也是纽约布鲁克林22号街区的一名活跃的社会研究学教师。

这句话在我脑海中铭刻了很久，因为我自己成长中的大部分学习经历都没有什么参与感。所以，当我开始教学生涯后，我时不时会想到这句话，它总会提醒我要让自己的课堂更好、更加以学生为中心、参与感更强，并且注重创造出能够帮助学生真正理解知识、深度学习的体验。

那之后的27年，不论是在我之前做校长的小学，还是现在的纽约市6-12年级公立学校和高中 Scholars' Academy，这句谚语一直在我的学校中得到实行。Scholars' Academy 一直在努力扭转老师们的固有思维方式，希望通过适当指导、运用当下的科技工具，增加学生的参与度、自主性、决定权和创造力。

如今，Scholars' Academy 的老师们必须很巧妙地计划教学，不然谚语中所说的“学以致用”的情况就不会发生。因为我们人类的天性就是倾向于选择更容易的，或者我们自己成年以前被教育去相信的事。要想通过传播和评价来打破被动学习与教学的文化循环，我们必须准备好参与其中。因此，我们在 Scholars' Academy 有一个新的流行语：“教学隐藏于计划中。”

好好计划充满参与感的课堂吧——要充分考虑并且指出学生们可能有的思维过程和错误观念，用能让学生一起聪明地参与、相互联系的知识，创造出让学生成为更好的学习者和创造者的体验。

### 以下是他来信的原文：

Tell me, I forget. Show me, I remember. Involve me, I understand. -- Chinese Proverb

When I began my career as an elementary school teacher in 1991, this quote was taught to me by a Professor Michael Wagner at Adelphi University. At the time, Professor Wagner was also an active Social Studies teacher in District 22, Brooklyn, NY. The quotation stuck with me because most of my learning growing up was not engaging. As I evolved my practice as a new teacher, my mind retraced the steps back to this quote quite frequently which prompted my development and the evolution of my classroom to be student centered, engaging, and focused on creating experiences that developed conceptual understanding and deep learning. 

27 years later as a former elementary school principal, and current New York City Public School grade 6-12 middle and high school principal, this proverb still rings true as our school, the Scholars' Academy in Rockaway, NY, continues to grapple with the task of shifting the cultural mindsets of our teachers to leverage current day technology tools in order to plan for increased student engagement, autonomy, ownership, and innovation to take place during instruction. As a result, we at Scholars' Academy have learned that teachers must strategically plan for this to happen or it will not happen because our human tendency is to default to what is comfortable or how we were taught as children and young adults. To break the cultural cycle of passive learning and teaching via dissemination and evaluation, one must plan to engage. Hence, we have a new mantra at the Scholars' Academy: "In the planning is the teaching." Plan to engage, consider the thought processes and misconceptions that the students will encounter, surface them and create experiences wherein students learn how to be better learners and creators with the knowledge with which they are intelligently engaged and connected.
 
- Thank you,
- Brian O'Connell
- Principal, Scholars' Academy
- www.scholarsnyc.com    

听完 Brian O'Connell 的分享，你有什么想说吗？你关于教育的故事和观点是什么？你最喜欢的教育名言是哪一句？

欢迎邮件至 lei.peng@jmdedu.com 或打开收集入口投递给我们。