- [直播预告 | 国内首个软硬一体开放易用的机器人开发平台，到底有哪些亮点？](https://mp.weixin.qq.com/s?__biz=MzIzNjE3MDMzNQ==&mid=2247491998&idx=3&sn=3fd6e103ceb02378100c1501c29edf76)
- [共建机器人繁荣生态 地平线发布国内首个软硬一体机器人开发平台](https://mp.weixin.qq.com/s?__biz=Mzg2NDc2OTE5OQ==&mid=2247484255&idx=1&sn=71785b490f10ad200991502e02787d08)

<p><img width="706px" src="https://images.gitee.com/uploads/images/2022/0705/214403_873c9b3c_5631341.jpeg" src="刑天地平线官方副本2.jpg"></p>

( _扫码关注公号后，输入“070”，获得直播链接_ )

> ROS 名为：机器人操作系统，是 SIGer 编委较早的选题立项，由于内容庞大应用多元，一直没有能成刊。当看到这个[能跑能跳与波士顿动力同场PK](https://www.sohu.com/a/557738954_489960)的小家伙，我眼前一亮，“ **刑天机器人正向你跑来** ” 就是它了，满足了我所有的想象，对于轮式机器人的好感则源于发明家 迪安•卡门 的杰作（可抬升的双轮轮椅）他赞助的 FIRST 创新大赛，可是所有青少年的偶像啊。 :+1: 

> 关注机器人大讲堂有一段日子了，直驱方案，树莓派兼容，这些关键词都刺激到了我的神经，他们就是上手容易的代名词，看到 驱动刑天的开发板 名叫 [旭日X3派，隶属于地平线机器人](https://mp.weixin.qq.com/s?__biz=Mzg2NDc2OTE5OQ==&mid=2247484255&idx=1&sn=71785b490f10ad200991502e02787d08)，明天晚上，[周五 19:30 派员直播](https://mp.weixin.qq.com/s?__biz=MzIzNjE3MDMzNQ==&mid=2247491998&idx=3&sn=3fd6e103ceb02378100c1501c29edf76)，果断填写了资料，抢座沙发啦。带着种种期待，同学们随我一同围观吧 :pray: 

# [共建机器人繁荣生态 地平线发布国内首个软硬一体机器人开发平台](https://mp.weixin.qq.com/s?__biz=Mzg2NDc2OTE5OQ==&mid=2247484255&idx=1&sn=71785b490f10ad200991502e02787d08)

Horizon 地平线开发者 2022-06-14 22:21 发表于北京

![输入图片说明](https://images.gitee.com/uploads/images/2022/0616/132703_972f7903_5631341.png "屏幕截图.png")

- 微信号: gh_223f59310922 地平线开发者
- 功能介绍: 地平线开发者社区致力于连接地平线和开发者，为大家提供前沿技术内容和丰富的技术活动，打造更好的开发者文化和氛围，共建开源生态。


News 地平线要闻：

> 2022年6月14日，边缘人工智能计算平台全球领导者地平线于线上举办“Hello Hobot”地平线机器人开发平台发布会，推出国内首个软硬一体、开放易用的机器人开发平台——Horizon Hobot Platform。针对机器人开发的痛点，为开发者提供从底层计算、开发工具到算法案例的整套机器人开发服务，有效提升机器人开发效率，推动行业发展和产业繁荣。

本次发布会，地平线邀请到科沃斯机器人产品总监刘彬彬、本末科技创始人兼CEO张笛、舜宇智能光学技术有限公司副总经理徐士鑫、古月居创始人胡春旭分享了双方合作亮点，并表达了共建机器人开发生态的共同期待。此外，奥比中光、乐动LDROBOT等公司也公布了与地平线机器人开发平台的相关合作。



地平线创始人兼首席科学家余凯博士在发布会上宣布：“地平线成立AIoT&通用机器人事业部并推出Horizon Hobot Platform，面向广泛的机器人开发者，提供开源开放、软硬协同的系统方案，顺应了机器人行业的发展趋势。我们希望联合广泛的行业合作伙伴，共同开创机器人繁荣生态的美好未来。”



余凯博士表示：“地平线自创立伊始，便秉承着‘赋能机器，让人类生活更安全，更美好’的使命，旨在人工智能领域不断深耕，让每一辆车、每一个电器都具有环境感知、人机交互和决策控制的能力，从而实现万物智能。”


![输入图片说明](https://images.gitee.com/uploads/images/2022/0616/132747_a3b15e11_5631341.png "屏幕截图.png")

> 地平线创始人兼首席科学家余凯博士



作为中国唯一实现车规级汽车智能芯片前装量产的企业，地平线车载芯片征程系列出货量已经突破100万片。面向机器人领域，地平线希望为机器人提供开发平台，不仅仅是芯片，更是完整的软硬件系统。地平线本次发布国内首个软硬一体且开放易用的机器人开发平台，提供从底层计算、到开发工具、再到算法案例的整套机器人开发服务，能够有效提升开发效率，为机器人开发者提供全方位的开发保障。 



据悉，机器人产业一直面临着开发效率低、技术落地难、研发成本和产品价值不匹配的问题。软硬件厂商间“各自为战”，缺乏高效协作，导致重复造轮子的现状长期存在。地平线AIoT&通用机器人事业部总经理王丛表示：“随着复合机器人复杂度的攀升，对器件选型、软件系统、数据迭代等方面的要求也将更高。打破‘各自为战’局面，在基础设施级平台上共建开发生态，是行业降本增效、释放生产力的必由之路。”


![输入图片说明](https://images.gitee.com/uploads/images/2022/0616/132756_dd1a29f2_5631341.png "屏幕截图.png")

> 地平线AIoT&通用机器人事业部总经理王丛



因此，着眼于降本增效、释放生产力，适应未来复合机器人的趋势，地平线推出了国内首个软硬一体、开放易用的机器人开发平台。该平台由芯片（地平线旭日®）、机器人操作系统（地平线TogetherROS™）、机器人参考算法（Boxs）、机器人应用实例（Apps）、配套开发工具（Tools）组成，囊括底层计算、开发工具、算法案例在内的整套机器人开发服务，为赋能机器人开发者、提升机器人开发效率提供了全新的基础设施。


![输入图片说明](https://images.gitee.com/uploads/images/2022/0616/132805_8999bad6_5631341.png "屏幕截图.png")

> 地平线机器人开发平台——Horizon Hobot Platform



得益于前瞻性的软硬结合理念，地平线自主研发兼具极致效能与开放易用性的边缘人工智能芯片及解决方案，可面向智能驾驶以及更广泛的通用 AI 应用领域提供全面开放的赋能服务。地平线旭日®系列芯片为机器人开放平台构建了高性能、低功耗的算力基石。目前，旭日®芯片累计出货量已突破100万片，已在智能机器人、智能大屏、智能家居等领域实现规模化落地量产，广泛应用在科沃斯、小度、TCL等合作伙伴的多款产品上。地平线TogetherROS™从芯片指令集开始的调优，相比目前业内最受欢迎的机器人操作系统ROS2，无论单元测试还是场景测试，TogetherROS™的通讯延迟、CPU占用均大幅降低，且十分稳定。由于TogetherROS™系统集成了众多开源模型，借助底层芯片中的AI引擎BPU，提供充足的算力保障，开发者不再需要花费很多时间在模型的调教和数据的训练上，很快就可以部署人工智能应用。合作伙伴舜宇智能光学基于地平线旭日®3进行深度学习优化，做到了极致的性能优化。



地平线机器人开发平台的另一大特点就是“简单易用”。操作系统层面，TogetherROS™与ROS2的接口完全兼容一致，并为开发者提供了多种应用工具，包括Hobot DNN推理框架、Hobot Sensor传感器模块及多种常用的机器人开发工具；而参考算法层面，地平线将把自动驾驶场景的算法迁移到机器人场景，并在未来开源，加速行业整体水平提升；进而在应用实例层面上，地平线得以提供开发者小车、室内服务机器人、室外移动机器人等诸多实例，覆盖从简单到复杂的开发难度、工作环境，满足开发者多样化的需求；此外，在开发工具方面，地平线提供的AI开发工具地平线艾迪®，存储成本、GPU训练成本、数据标注成本均有不同程度的降低，数据挖掘、业务开发、场景迭代更高效，助力机器人应用的快速开发。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0616/132822_e8a14607_5631341.png "屏幕截图.png")

> 地平线旭日®️X3派



为了带给开发者一站式的开发体验，此次发布会上，地平线还宣布推出大算力、高兼容性的机器人开发板旭日®X3派，2G版499元，4G版549元。基于地平线旭日®X3派，本末科技与地平线打造的国内首个AI轮足机器人开发平台——刑天，也在本次活动中首次亮相。


![输入图片说明](https://images.gitee.com/uploads/images/2022/0616/132832_242ceebe_5631341.png "屏幕截图.png")

> 国内首个AI轮足机器人开发平台——刑天

此外，地平线还将提供一系列立体多元的开发者服务。包括，地平线开发者社区全面升级，地平线携手古月居推出ROS（机器人操作系统）精品课程，包括针对TogetherROS™的深度定制课程等内容现已上线；24小时内高效响应开发者的配套服务；为优秀的开发者作品提供传播支持，并为达到量产级别的合作，将给予特殊的商务优惠等。未来，地平线还将定期举办开发者互动活动，包括月度的开发者面对面、季度的加速营、年度的机器人竞赛及开发者大会等，与开发者群体保持深度沟通、同频共振。


近几年来随着技术进步、产业发展、需求提升，国内外科技巨头对机器人的关注也逐步增多：特斯拉公布了人形机器人Optimus擎天柱计划，英伟达也多次提到“机器人首先应用在自动驾驶领域，再逐步外延”的观点。在爆发前夜，地平线将锚定“赋能机器，让人类生活更安全，更美好”的使命，紧跟时代趋势投身机器人时代的基础设施建设，以丰富易用的开发组件、立体多元的服务助力开发者，携手更多合作伙伴共建繁荣生态，进一步打开机器人工业大门。



“在涌动的时代保持敬畏，对可期的未来不失初心，我们相信，我们终将携手迎来机器人行业更繁荣的明天。”正如地平线AIoT&通用机器人事业部总经理王丛所言，机器人行业的“明天”需要更多优秀伙伴的不断加入与携手同行。


![输入图片说明](https://images.gitee.com/uploads/images/2022/0616/132849_58419cf1_5631341.png "屏幕截图.png")

点击“阅读原文”，访问地平线开发者社区

# [直播预告 | 国内首个软硬一体开放易用的机器人开发平台，到底有哪些亮点？](https://mp.weixin.qq.com/s?__biz=MzIzNjE3MDMzNQ==&mid=2247491998&idx=3&sn=3fd6e103ceb02378100c1501c29edf76)

堂博士 leaderobot 2022-06-16 07:00 发表于浙江

机器人是一种多学科高度整合的产物，涉及机械、电子、计算机、材料、控制以及仿生学等，对人们的生产和生活产生了极大的影响。尽管机器人的种类多种多样，功能日趋复杂，但从设计角度来看，通常都是由软件和硬件组成的。软件部分包括传感器信息采集、控制算法、通信、计算控制算法等；硬件部分则包括计算控制核心、执行机构、驱动装置、控制系统和复杂机械等。



科技的快速发展推动着机器人不断向着智能化、自主化方向进化。近年来，随着机器人市场需求的增长、技术水平的进步，机器人产业开始进入快速发展期，越来越多的机器人走进人们的生产和生活中，人们对机器人智能化水平的要求也在不断提高。然而机器人产品开发的模式一直以来并未有太大变化，并且存在诸多问题，如涉及多技术领域、软硬件高度定制、技术迭代周期久等。


![输入图片说明](https://images.gitee.com/uploads/images/2022/0616/164457_758333ed_5631341.png "屏幕截图.png")



2022年6月14日，地平线推出国内首个软硬一体、开放易用的 **机器人开发平台——Horizon Hobot Platform** ，提供了从底层计算、到开发工具、再到算法案例的整套机器人开发服务，能够有效提升机器人开发销量，有效赋能开发者的开发工作。



![输入图片说明](https://images.gitee.com/uploads/images/2022/0616/164506_0ff7e975_5631341.png "屏幕截图.png")



为了让广大机器人开发者 **对软硬一体的机器人开发平台** 有更全面深入的了解， 我们有幸邀请到 **地平线通用机器人事业部专家、机器人开发平台项目负责人程飞先生做客机器人大讲堂** 直播间。程飞先生将逐一介绍机器人系统、算法仓库、应用示例、AIDI平台以及旭日X3派开发板套件等关键模块。该组成系列构成了软硬件一体的开发套件，依托地平线AI芯片，结合高效的机器人操作系统、丰富的算法仓库，并以场景化示例的形式呈现功能应用，可以简化并加速机器人产品开发的过程，并提升机器人的智能化水平。


本次直播面向广大机器人开发者，欢迎报名参加。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0616/164606_848ee63b_5631341.png "屏幕截图.png")


 **直播交流群** 

本次直播开设了“ **机器人开发平台** ”主题交流群，加入交流群的小伙伴，不仅可以及时获得机器人大讲堂每期活动直播信息，同时与讲师亲密互动，也会认识很多行业大牛，快来加入吧！

 **公司介绍** 




![输入图片说明](https://images.gitee.com/uploads/images/2022/0616/164632_c9fec078_5631341.png "屏幕截图.png")


地平线主要从事边缘人工智能芯片的研发，具有领先的人工智能算法和芯片设计能力，致力于通过底层技术赋能，推动汽车行业的创新发展。得益于前瞻性的软硬结合理念，地平线自主研发兼具极致效能与开放易用性的边缘人工智能芯片及解决方案，可面向智能驾驶以及更广泛的通用 AI 应用领域提供全面开放的赋能服务。在智能驾驶领域，地平线推出了征程系列车规级芯片，是目前国内唯一实现汽车智能芯片前装量产的企业。面向更广泛的通用AI应用领域，地平线推出旭日系列芯片，已在智能机器人、智能大屏、智能家居等领域实现规模化落地量产，赋能合作伙伴包括小米、科沃斯、TCL等行业头部企业。

地平线从创立伊始，一直秉承着“赋能机器，让人类生活更安全，更美好”的使命，旨在通过人工智能领域的不断深耕，让人类拥有更自由、更美好的生活。


- END -




 **征稿啦** 


> “leaderobot”公众号现长期招募有志青年/团体投稿，要求：

  1. 文章围绕机器人行业相关技术内容为主，字数不限。

  2. 投稿内容请以word形式发出，图片直接插入word中。另外，为保证图片清晰度，请将除文字外的所有素材打包作为附件一并发送。

  3. 一经采用，我们将会根据文章内容给您支付稿费。



> 稿件 **请投递至邮箱** ：robospeak@leaderobot.com

> 详情咨询可联系13810423387（同微信）



> 欢迎加入我们，一起为“机器人”们带来有趣、有用、有意义的分享~~


 **RECOMMEND 推荐阅读** 

[![输入图片说明](https://images.gitee.com/uploads/images/2022/0616/164749_4010ff53_5631341.png "屏幕截图.png")](https://mp.weixin.qq.com/s?__biz=MzIzNjE3MDMzNQ==&mid=2247487767&idx=1&sn=45be2c70560c7f76cf8714c7731d9343)

点击"在看"了解更多干货内容

---

【笔记】[ROS 自动驾驶](https://gitee.com/yuandj/siger/issues/I4UFAJ)

- [地平线机器人(Horizon Robotics)](https://www.zhihu.com/topic/20097340/hot) 知乎

  > 地平线具有领先的人工智能算法和芯片设计能力，通过软硬结合，设计开发高性能、低成本、低功耗的边缘人工智能芯片及解决方案，开放赋能合作伙伴。面向智能驾驶和 AIoT，地平线可提供超高性价比的边缘 AI 芯片、极致的功耗效率、开放的工具链、丰富的算法模型样例和全面的赋能服务。目前，基于创新的人工智能专用计算架构 BPU(Brain Processing Unit) ，地平线已成功流片量产了中国首款边缘人工智能处理器 -- 专注于智能驾驶的「征程(Journey)」系列处理器和专注于 AIoT 的「旭日(Sunrise)」系列处理器，并已大规模商用。

  > 依托行业领先的软硬结合产品，地平线向行业客户提供「芯片 + 算法 + 云」的完整解决方案。在智能驾驶领域，地平线同全球四大汽车市场(美国、德国、日本和中国)的业务联系不断加深，目前已赋能合作伙伴包括奥迪、博世、长安、比亚迪、上汽 、广汽等国内外的顶级 Tier1s，OEMs 厂商;而在 AIoT 领域，地平线携手合作伙伴已赋能多个国家级开发区、国内一线制造企业、现代购物中心及知名品牌店。[1]

  - [如何评价余凯创立的horizon robotics？](https://gitee.com/link?target=https%3A%2F%2Fwww.zhihu.com%2Fquestion%2F31943421%2Fanswer%2F120759325) 
    - 不看好。([邱彼特](https://www.zhihu.com/question/31943421/answer/120759325))
    - 辟谣。（[余凯](https://www.zhihu.com/question/31943421/answer/121176226)）
    - 阴暗。((科技圈投行狗)https://www.zhihu.com/question/31943421/answer/120741944)
    - 吐槽。（[鼎铉](https://www.zhihu.com/question/40950551/answer/115670287)）
    - 匡扶正义。（[宫一尘](https://www.zhihu.com/question/31943421/answer/120860719)）
    - 既然一切都没就绪，请还创业者安宁吧… ([董豪 帝国理工学院 计算研究博士](https://www.zhihu.com/question/31943421/answer/121291087))

  - [地平线机器人（Horizon Robotics）现在到底怎么样了？](https://www.zhihu.com/question/40950551/answer/115633854) 喜欢在地平线那段时间...

- [边缘人工智能芯片全球领导者](https://www.lagou.com/gongsi/83580.html) 【[笔记](https://gitee.com/yuandj/siger/issues/I4UFAJ#note_10969703_link)】拉钩 | 地平线发展历程 | [基本信息 | 管理团队](https://gitee.com/yuandj/siger/issues/I4UFAJ#note_10970140_link)  | [招聘职位](https://gitee.com/yuandj/siger/issues/I4UFAJ#note_10970301_link) | [微博](https://gitee.com/yuandj/siger/issues/I4UFAJ#note_10970393_link) |

  <p><img width="15.6%" src="https://images.gitee.com/uploads/images/2022/0616/112447_70bf8ad1_5631341.png"> <img width="15.6%" src="https://images.gitee.com/uploads/images/2022/0616/112441_8bc5ebd9_5631341.png"> <img width="15.6%" src="https://images.gitee.com/uploads/images/2022/0616/112510_ee9fdcd4_5631341.png"> <img width="15.6%" src="https://images.gitee.com/uploads/images/2022/0616/112516_bc64c7a1_5631341.png"> <img width="15.6%" src="https://images.gitee.com/uploads/images/2022/0616/112536_d8d985c1_5631341.png"> <img width="15.6%" src="https://images.gitee.com/uploads/images/2022/0616/112601_fb3fc056_5631341.png"></p>

- [地平线，已经不把Mobileye当竞争对手了](https://zhuanlan.zhihu.com/p/397291134) 【[转载](https://gitee.com/yuandj/siger/issues/I4UFAJ#note_10972052_link)】

  > 结语：争夺 “汽车产业的联发科”

    - 地平线的所谓开放软件生态，我认为是双刃剑。高通和英伟达深度捆绑了软件生态，但是软件平台的集成度相当好，能达到交钥匙的效果。地平线能否有这个能力去支持广大软件生态，我持保留意见。

    - 对于地平线进座舱，我还是持怀疑态度的。座舱对CPU和GPU要求很高，而且竞争已经非常充分了。地平线弯道超车的机会不大。

    - 忽悠不懂的人和资本还是可以的，按规矩进入更多大厂的供应链那还是。。。需要走一段路的吧？

- 【独家】[获Intel投资后，余凯自述：我越来越聚焦｜甲子光年](https://zhuanlan.zhihu.com/p/30294849) 【[转载](https://gitee.com/yuandj/siger/issues/I4UFAJ#note_10972306_link)】

  - [地平线机器人将完成10亿美元融资，估值或达40亿美元](https://mp.weixin.qq.com/s?__biz=MzA4MTE5OTQxOQ==&mid=2650022453&idx=3&sn=850edee0044f283679b5ae097a94489b) 【[转载](https://gitee.com/yuandj/siger/issues/I4UFAJ#note_10972421_link)】
  - [地平线机器人：资本力捧的自动驾驶芯片独角兽](https://mp.weixin.qq.com/s?__biz=MzA3MjEwMTQxNA==&mid=2650842529&idx=3&sn=0b51a3c3dfd36672b8aa59a9cd1c8187) 【[转载](https://gitee.com/yuandj/siger/issues/I4UFAJ#note_10972460_link)】
  - [别样时刻｜地平线余凯：智能汽车是机器人时代第一个大终端](https://mp.weixin.qq.com/s?__biz=MjM5MTM3NTMwNA==&mid=513552199&idx=1&sn=5a1e066254ee830d44591199cad6fc7a) 【[转载](https://gitee.com/yuandj/siger/issues/I4UFAJ#note_10972547_link)】

- [共建机器人繁荣生态 地平线发布国内首个软硬一体机器人开发平台](https://mp.weixin.qq.com/s?__biz=Mzg2NDc2OTE5OQ==&mid=2247484255&idx=1&sn=71785b490f10ad200991502e02787d08)

  - [征程宣言 | 高性能大算力整车智能计算平台暨战略发布会宣传片](https://mp.weixin.qq.com/s?t=pages/video_detail_new&fallback_from_env=not_support&scene=1&vid=wxv_2086071637833023490&__biz=MzA5MjM0MDQ1NA==&mid=502541979&idx=1&sn=0da2833752f10d8f40a17cdebc541430&vidsn=&exportkey=ASKJkJeUbRBs1BnhSplHX5g%3D&acctmode=0&pass_ticket=0J7YjZyCBrf32u5nIeG83ykEmBlNI%2B%2FSAXFgf7x2dRP54CaoDD42FXBN7N67h8kq&wx_header=0)

    > 从何时起，他们便踏上了征程，第一次高举火把长途奔袭带回胜利的消息，第一次将手掌离开熟悉的土地，第一次，告别恐惧挑战3000英尺岩壁，第一次，科学与艺术的融合，表达开放之文明，就像把黑夜，点亮的人 是为了照亮更多人的黑夜，借着光亮惰画蓝图的人，是为了让引擎隆隆作响。驱动引擎轰鸣的人是为奔赴征程的人能更远更快，更强，而今天我们驾驶车船振翅扬帆，我们接驳九天和环宇相连，我们用绵延不绝的代码，丈量征程路上的步伐，我们创造舞台，也陪你登上舞台。在这有限与无限的游戏之中，我们选择同行共同踏出的征程，变成为身后亿万人追寻的路，看吧目之所及的地平线，就是我们的宣言，路是时光族谱。你我互为征程。此刻，征程与共，一路同行。地平线，机器人。

      ![1](https://images.gitee.com/uploads/images/2022/0616/134322_1156db86_5631341.png "屏幕截图.png")

- [WHAT IS A DIRECT DRIVE MOTOR?](https://www.magneticinnovations.com/faq/direct-drive-motor/) 【[NOTE](https://gitee.com/yuandj/siger/issues/I4UFAJ#note_10977387_link)】  

  **A Direct Drive Motor, is a type of permanent-magnet synchronous motor which directly drives the load. When using a this kind of motor, the use of a transmission or gearbox is eliminated. Therefore, the amount of moving parts in the system is reduced tremendously. This increases the efficiency and creates a quiet and highly dynamic operation as well as a very high lifetime of the system. Examples  of Direct Drive Motors are [torque motors](https://www.magneticinnovations.com/faq/torque-motor-how-it-works), [linear motors](https://www.magneticinnovations.com/products/voice-coil-actuators/) and  certain types of [BLDC motors](https://www.magneticinnovations.com/faq/what-is-a-bldc-motor/).** 

  - [6D direct-drive technology for planar motion stages](https://www.sciencedirect.com/science/article/abs/pii/S0007850612001473)

- [直驱型精准动力专家](https://directdrive.com/) https://directdrive.com

  > 2022-3-22 · 本末科技面向扫地机器人、拖地机器人、家用服务机器人等小型轮式机器人行业，提供直驱型的行进轮和扫拖地功能模组。行进轮采用轮毂电机设计，扫拖地功能模组则大胆使用了扁

    - [直播预告 | 国内首个软硬一体开放易用的机器人开发平台，到底有哪些亮点？](http://mp.weixin.qq.com/s?__biz=MzIzNjE3MDMzNQ==&mid=2247491998&idx=3&sn=3fd6e103ceb02378100c1501c29edf76)
    - [能跑能跳能抻腿，这个和波士顿动力同台PK的机器人再升级！](https://www.sohu.com/a/557738954_489960)

  <p><img height="169px" src="https://images.gitee.com/uploads/images/2022/0616/154551_ba9b2187_5631341.jpeg"> <img height="169px" src="https://images.gitee.com/uploads/images/2022/0616/154911_2ecc83fb_5631341.png"></p>