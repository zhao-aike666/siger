- [【官网】zeal8bit.com](https://zeal8bit.com/)
- [【最新】自制8位计算机](https://www.bilibili.com/video/BV1Kr4y1L73E) [【支持C语言】自制8位操作系统](https://www.bilibili.com/video/BV1bv4y117Pu)
- [【开源】自制8位操作系统](https://www.bilibili.com/video/BV1Ze4y187ya)
- [Z80 GETS NEW OS](https://hackaday.com/2022/11/08/z80-gets-new-os/)

<p><img width="706px" src="https://foruda.gitee.com/images/1663599796140593325/d4d2554d_5631341.jpeg" title="290Zeal8bitII.jpg"></p>

> 复古计算机一直是一个让人着迷的领域，不只是学习，更是一种文化，所以 SIGer 推荐这个项目给同学们，尽管它还不能给你炫酷的展示和交互，但同样可以令你有 “YES” 的快感。如果可能，可以依照作者的提示，将它移植到自制的系统，这样就可以共享上面的应用，这会是一个非常好的连接！

# [Zeal8bit welcome](https://gitee.com/shiliupi/Zeal-8-bit-OS/issues/I5RTQ1)

> 我是Zeal的创作者奥马，我在自制一台真正的8位计算机。我会分享复古科技、FPGA、嵌入式软硬件知识等等。欢迎留言沟通（中英留言都可以） 网站：zeal8bit.com

  - 我正在翻译树莓派基金会出版的一本微型计算机的历史书，其中就有Z80的详细介绍。而且其中很多早起的计算机，非常适合您的创作，也会非常适合科普，尤其是青少年，以及热爱数电的家长，我就是其中之一。很高兴认识你，我正在翻译 BBC MICRO 这章，我是中学的科学老师，带学生一起完成这个任务。很高兴认识你。我的网名 石榴派， 也是一个 FPGA 开发板，BASED ZYNQ-7020，运行一个 RISCV 软核和 LINUX 操作系统，唯一的应用是 GNUCHESS。

  - 另外翻译工作是在一个开源的自媒体项目 SIGER，我分享样章给你，我是英文原版的正式用户，购买了PDF版用于翻译，我也是[树莓派基金会的粉丝](https://gitee.com/blesschess/luckystar/issues/I4IYBI)，通过阅读这本书，愈发喜欢那个时代，看到你的视频分享，让我如同穿越。专门为翻译这本书设立的 [STEM 频道](https://gitee.com/flame-ai/siger/tree/master/STEM)。记录了[我和树莓派基金会的故事](https://gitee.com/flame-ai/siger/blob/master/STEM/README.md)！希望你喜欢！

> 谢谢你喜欢我的视频！你有 The Computers that Made Britain 的英文版吗？ 我挺感兴趣的，我读英文比中文快

  - zeal8bit.com 可以制作一期专题，交个朋友。PDF英文版本您可以再树莓派基金会网站上下载，应该是可选捐赠模式，他们的MAGPI都是这个捐赠模式。真开源。所以我特地购买了PDF阅读，组织同学们免费翻译，希望在中国推荐。

  - 在 zeal8bit.com 的推荐的时候，我会先和同学们学习研究后，出一个大纲，如果没有特别的问题，直接出小样给您，看有没有谬误。如果，您可以接受我们的采访，可以一同把采访提纲发给您。可以中英文接受采访，我们可以请同学翻译。收录在 STEM 频道内。

    > it could be used by anyone who would like to learn low-level programming or learn more about how computers work 这个目标，和SIGER 的开源项目 石榴派的目标完全一致。 网页链接

    > 我和我的学生会成为 ZEAL8BIT 的粉丝，支持这个项目！包括购买和成为开发者。

    > The Zilog Z80 会是下一个发布的章节，可以配合 ZEAL8BIT 的专题，在 SIGER 社区发布。和我的同学们一同分享。 https://store.rpipress.cc/products/the-computers-that-made-britain 在我们的 ISSUES 头条正文里就有连接。

      - Acorn的选择要么是Zilog Z80，要么是6502。 在Zilog Z80芯片上运行CP/M 。bbc micro Proton可以在Z80和6502上运行。辛克莱的公司在基于Zilog-Z80的代号为NewBrain的计算机 。ZX81的基本规范与ZX80没有变化:仍然有1kB的RAM，仍然是Z80处理器 。在一台成本不到100英镑的现成电脑上安装一台3.25MHz的Z80处理器是相当了不起的。 。Apple II Plus 其最畅销的扩展包之一是 微软(Microsoft) 制造的 Z-80 软卡 (Z-80 SoftCard)，搭载同样的处理器，但内存可选择为16kB、32kB 或 48kB。由于集成了 Z80 处理器，Z-80 软卡 允许用户运行日益流行的 CP/M 操作系统。 TRS-80 (80指的是它其实是基于 Zilog Z80 处理器的) 。

      - RM 480Z 1982，括一个 4MHz 的 Z80A 处理器 和 最多 256kB 的内存 。Zilog Z80 被全书多次引用：13, 28, 45, 54, 55, 57, 95, 99, 107, 110, 119, 135, 143, 159, 166, 191, 202, 207, 216, 262, 274
网页链接 可以将该视频的字幕，作为专题的主要内容，再附加一些其他复古计算机相关的历史内容。

    所以，只需要您同意，SIGER 的转载和编辑，即可有一期 Zeal8bit 的专题奉献给您。祝一切顺利。

> 您好，谢谢来信。非常有意思，我觉得Z80非常适合教育，这是为什么我研发了Zeal8bit 计算机，非常适合用来学习了解嵌入式、底层系统学习。我还自研了8位操作系统，已经开源了。 https://github.com/Zeal8bit/Zeal-8-bit-OS ；欢迎查看、贡献。您可以加我微信。方便更多沟通。

### [SIGer 专访 2022-10-4](https://gitee.com/shiliupi/Zeal-8-bit-OS/issues/I5RTQ1#note_13157452_link)

1. 硬件开源吗？打版和采购元器件能DIY吗？还是必须购买成品，您交付的生产计划是怎样的？准备众筹还是什么计划？

Zeal8位计算机由两部分组成：主板加FPGA板。 目前硬件暂都不开源，后续可能会把主板可能会开源。

我接到一些私信说想要购买了。很快主板可以开始买了，现在可以先提前私信我需要数量、是想买已焊接的成品还是Kit。我们可以选择买已焊接的成品、也可以买Kit（Kit包含组合件）买好后需自己回去组装焊接。

但我建议先买已焊接好的成品，因为我会焊接好，测试无误再寄出。

至于众筹，我后续可能会去众筹。现在还没有计划。

2. 纯软件模拟器，是否能提前交付吗？有可能使用其他工具链完成模拟器的制作吗？比如RISCV最常用的QEMU 可以模拟大部分主流平台。

我已经用javascript写好了模拟器，我们可以在浏览器运行，而且代码已经开源在Github上来，https://github.com/Zeal8bit/Zeal-WebEmulator;

3. 对于外设的扩展，是怎样的计划？比如游戏手柄，游戏外设。8位机的游戏模拟器兼容性如何？

Zeal8bit计算机有8个可用GPIO（可以接游戏手柄）、可以外接PS2的键盘、还有外设Catridge接口可以接任何元件（比如接RAM、ROM、MCU、Wi-Fi等等），用户可以自己扩展和开发自己想要的功能。

Zeal8bit有两个操作系统：
- 1）CP/M，即支持CP/M的所有软件
- 2）Zeal8bitOS，我自制的操作系统，需要用户自己开发游戏。

4. SIGER对8位机的最大期待是80年代主流机的复刻功能复刻，优化。可以使用上其资源。其中BBCMICRO有一个非常有名的 计算机全民素养计划，有一个上手包（软件包）可以开机即玩，我们可以出这个复刻吗？还需要哪些努力？

我也是这样期待的。 我将自己的对计算机的软件、硬件知识都融入到了Zeal8位计算机，它不仅仅是对80年代的电脑的复刻，还有很有“现代”的优化功能。比如我用的Z80的CPU，我们现在还可以买的到，100%还原了复古，同时我开发了RTC、I2C、UART、PS2等优化功能。Zeal计算机可以开机即使用。


5. 您BASE是在法国？还是中国？是怎样的机缘让您可以法中融通，流畅交流？对于设计师的故事，开发者同样好奇，也重要，就好比LINUX，树莓派，背后的缔造者都是引人关注的

我是法国人，目前在上海。我大学时候就开始自学中文了，我非常喜欢中国的文化。我做了很多个Zeal的原型机，需要购买元器件、打板来验证和测试，在中国这些非常方便！要知道，在法国这些可能需要好几周时间！

6. 除了众筹，对8位机的发展，您还有哪些期待？除了自己热爱，不断雕刻打磨，对它的发展有哪些畅享？

我期待的Zeal8位计算机能吸引到开发者，能够在Zeal8位计算机开发软件或者游戏、移植软件、开发硬件外设，最终能形成一个热爱计算机的社区。

7. SIGER是复合型学习社区，以自媒体科普的方式连接青少年，中学生为起点社群，也是我的学生，不断辐射到北京其他学校和社团，现在100+编委，包括各社团骨干。也有大学生和小学生，还有家长。都是可以辐射的。另外教师群体也是可以辐射的。方式包括，分享内容（宣传）比如本次采访，还有协助加入应用包软硬结合，以石榴派为IP，发布全栈学习环境。再获得进一步内容后，可以由开发者社区提供应用期待的列表。这有关开发者（第一批）的素质要求？期待能有个全面的开发者发展的思路。

对于Zeal8位计算机的开发者：
- 1、首先会需要熟悉Z80的汇编语言。目前没有FPGA开售，还没有图形显示，所以想要开发游戏还需要等一会。我建议熟悉Z80的汇编语言，它更高效。
- 2、或者有C语言基础的人，因为有SDCC编译器，可以编译C语言到Z80汇编语言。

# [自制8位操作系统【支持C语言】](https://www.bilibili.com/video/BV1bv4y117Pu)

3239 45  2023-01-15 22:19:00

本期视频介绍了Zeal 8-bit操作系统是如何增加对SDCC的支持的。这意味着，Zeal 8-bit操作系统上可以用C语言编写程序了!
- 网站: https://zeal8bit.com；
- Zeal 8位操作系统开源：https://github.com/Zeal8bit/Zeal-8-bit-OS/；
- Zeal 8位OS的用户程序：https://github.com/Zeal8bit/Zeal-8-bit-OS/tree/main/kernel_headers/examples；
- Zeal 8位OS的C交互：https://github.com/Zeal8bit/Zeal-8-bit-OS/tree/main/kernel_headers/z88dk-z80asm；
- Zeal 8位OS的汇编交互：https://github.com/Zeal8bit/Zeal-8-bit-OS/tree/main/kernel_headers/z88dk-z80asm；

![输入图片说明](https://foruda.gitee.com/images/1673888169137934904/71f72ca9_5631341.jpeg "zeal80.jpg")

![输入图片说明](https://foruda.gitee.com/images/1673888155595344235/d8ba1433_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1673885440530210483/2b3b6742_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1673885485781802141/3abb9a91_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1673886069644575703/3011b9b4_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1673885522423444032/a060363c_5631341.png "屏幕截图")

# [Z80 GETS NEW OS](https://hackaday.com/2022/11/08/z80-gets-new-os/)

16 Commentsby: Al Williams | November 8, 2022    

![输入图片说明](https://foruda.gitee.com/images/1673882131865176146/ba805fd6_5631341.png "屏幕截图")

If you have a soft spot for a Z80 computer but want a new operating system experience, try [Zeal](https://github.com/Zeal8bit/Zeal-8-bit-OS). You can watch a demo of the open-source OS in the video below.

As you might expect, the whole system is written in Z80 assembly language. The features you expect are there: files, directories, device drivers, a clock, and even memory banking to support up to 16M of memory. The work isn’t totally done, nor is the initial target computer — Zeal — but it looks like a great piece of work so far and will be of interest to anyone who has a Z80.

You can also try it in [emulation mode](https://zeal8bit.github.io/Zeal-WebEmulator/) in your browser if you don’t have a Z80 CPU handy. The author wanted a simple OS that wasn’t multithreaded and could live in ROM. The kernel takes about 6K of ROM and 1K of RAM, depending on the configuration. The system calls and general operation will remind you of Linux.

There is a large list of things to do, so if you have a mind to pitch in, there’s no shortage of ideas to work on. Does the world need another Z80 OS? For that matter, does the world need any more Z80 systems? We don’t know about the world, but [we can always use them](https://hackaday.com/2022/07/19/modular-z80-really-racks-up-the-retrocomputer-cred/). They don’t even [have to cost very much](https://hackaday.com/2017/01/02/retrocomputing-for-4-with-a-z80/).



- Posted in [Retrocomputing](https://hackaday.com/category/retrocomputing/)
- Tagged [z80](https://hackaday.com/tag/z80/)

### 16 THOUGHTS ON “Z80 GETS NEW OS”

- Sanouage says: November 9, 2022 at 2:34 am

  > Very interesting and ambitious. Not only it has the Zeal 8-bit OS, but also the creator is making Z80 computer. I think the world still needs the Z80, especially in IT education. Why not teaching computer science begins with the Z80?

  - Joshua says: November 10, 2022 at 1:28 am

    > MP/M II is also an awesome OS. And it has an actual software library (can run multiple CP/M programs, has Turbo Pascal 3 etc).

    > And then there’s SymbOS, another lively OS with applications. It even has a RAD IDE, akin to Delphi or Visual Basic.

  - PPJ says: November 11, 2022 at 5:42 pm

    > There was a time not so long ago when a student could get sued by Microsoft as he could not possibly write operating system by himself. Today people write operating systems for computers they design. Impressive work.

- Etienne says: November 9, 2022 at 2:44 am

  > the first chip I started to learn assembly programming with on Texas Instruments calculator (TI-82)
and the chip used in the original gameboy.

  > Nice to see this chip stills getting interest after so many years. I guess it is part of the emblematic devices in the history of computers such as as commodore, amiga, motorola 68k…

  - Mog says: November 9, 2022 at 3:57 am

    > The chip used in the original Game Boy is a fun offshoot of computing history in and of itself.

    > The CPU used in the Game Boy is a Sharp LR35902, which has a number of features in common with the Zilog Z80, but lacks much of what people associate with the Z80 over the Intel 8080.

    > The opcodes prefixed with a ‘CB’ byte are present, but the remaining prefix instructions (DD, ED, FD) are not. The ‘shadow’ register file is not present. On-board support for DRAM refresh (the ‘R’ register) is not.

    > The fact that Sharp were tapped for the Game Boy’s CPU isn’t a coincidence, either, as by the time the Game Boy was in development, Nintendo had a 7-year history of working with Sharp, as the company’s 4-bit microcontroller line (the Sharp SM5x and SM51x) were the basis of all of Nintendo’s “Game & Watch” line of handhelds dating back to 1981. Offshoots of Sharp’s 4-bit SM5-series microcontrollers were also the dedicated key/lock chips known as “CIC” which were present on the NES, SNES, and N64 for the ostensible purposes of copy protection (though which were more realistically for vendor lock-in).

    - Daid says: November 9, 2022 at 5:44 am

      > Also, the gameboy CPU lacks the index registers, which makes it very much NOT a Z80. Any Z80 code will very likely not work on the gameboy due to it.

      > Everything points at that the gameboy CPU has a SM83 core, as documentation that was found on that core matches 1:1 with the gameboy CPU.

      > Both the Z80 and SM83 are 8080 based, and that’s where they share a lot of things from. And the assembly syntax is Z80 style, as the 8080 style is pretty confusing.

      > On a related note, those 4bit CPUs (SM51x for example) are really odd, if you want to be confused, lookup the instruction set of those, and how the program counter isn’t just counting upwards.

      - just passing says: November 9, 2022 at 6:58 am

        > In truth, I find the 8080 mnemonics more intuitive than the Z80 ones; they don’t seduce you into thinking things that are impossible might not be. Things that look like special cases really are.

        - just passing says: November 9, 2022 at 6:58 am

          > Sorry – I should have said “things that are special cases look like it”.

      - Joshua says: November 10, 2022 at 1:40 am

        > And let’s don’t forget the sound chip inside the DMG-01’s CPU core.. The GB is capable of stereo sound, has two pulse channels (square), a noisy channel, a square wave channel, 3-bit volume control..

        > The reason why most people think that the Gameboy has a Z80 is because of the history of emulation, I suppose. The Z80 was well documented, not only by official sources. So if you’re working on a Gameboy project, you’ll likely start with an existing Z80 emulation core and begin to build upon of that rather than implementing the strange SM83 architecture from scratch.

      - Pedro Lucas says: November 12, 2022 at 5:19 am

        > The Z80 actually matches 8085 machine code, i remember programming for the 8085 in Uni and then bringing the binary code home to run/test my code on weekends, using my old ZX Spectrum.

- ButtSoup Barnes says: November 9, 2022 at 2:49 am

  > Whelp, here’s something I can do with my TS/1000 . (have the RAM expander) Just need to hit the thrift stores and find a blank cassette.

- JRD says: November 9, 2022 at 8:57 am

  > Missed this detail: “Make sure you have an MMU first which divides the Z80’s 64KB address space into 4 pages of 16KB. That’s the main requirement.” So while it may have only a few K resident at any one time, it still requires more memory, just shoved off elsewhere.

  > DARN. There are those of us who like to play with vintage machines that don’t have the full 64K space available–like TRS-80s that start with 16K lost to ROM, and certainly no banked memory. The TRS-80 DOSes had a less than 3K resident kernel “nucleus”, with a 1K region for disk overlay “/SYS” files. (NewDOS got up to 21 SYS files.)

  > C’mon guys, even (very) early UNIX could run in small K. Can’t we squeeze an OS without needing 100+K memory on hand?

  - Joshua says: November 10, 2022 at 2:08 am

    > “Make sure you have an MMU first which divides the Z80’s 64KB address space into 4 pages of 16KB. That’s the main requirement.”

    > That’s the classic bank-switching model, as used by Expanded Memory Specification (EMS) in the mid-late 1980s. I had no idea this qualifies as a full-fledged MMU already.

    > On 16-Bit x86, 64KB is the size of a single segment (on Z80 its the complete address space).

    > A Small Page Frame of EMS has a size of 64KB, too. Inside this window, which usually is located between 640KB-1MB, four pages a 16KB reside.

    > Applications can use these four pages to store/read information. A memory manager is used for communication between bank-switching hardware and the EMS application. Simply said.

    - My Wetware is Dried Out says: November 15, 2022 at 9:42 pm

      > Quite a few of the early MMUs were nothing more than this, just done in silicon.

      > For example, the Motorola 68451 was often used more as a simple hardware segmentation system than anything more MMU-like (in the modern definition). It could be used for segment-style *or* page-style usage, but could only support 32 simultaneous segments/pages at a time; if you needed more simultaneous segments, you were expected to use multiple physical 68451s.

      > The 68000 had an error in its memory-access trapping that often prevented full restoration of state after the trap had been handled, but the 68010 had the fix, and thus could use a 68451 to do true virtual memory. With only 32 segments in the table, a 68010+68451 system often felt like an exercise in patience, since modifying the segment table wasn’t exactly instantaneous.

      > I once got to see a specialty system that used 12 or 16 68451s, back when it cost more than a small bizjet. It was great for problems that couldn’t fit into a smaller system, but if your problem would fit into a smaller system, *any* other system would have been faster :)

      > I think it ran on a UNOS variant, but they didn’t actually let us touch it. It was nearly obsolete, but the owners still had a couple of in-progress projects that depended on it. It was used for some kind of typesetting, and they were MUCH more careful of letting outsiders touch it than they were of the just-delivered fifteen million dollar process press.

- Mr D Kerr says: November 10, 2022 at 4:55 am

  > Would it not be better to target the ZILOG Z8000 CPU

  - Jaap van Ganswijk says: November 10, 2022 at 8:28 am

    > The Z8000 was quite convoluted. The Z800 was much nicer, but I don’t know if it ever materialized, there were also some other Z800-like designs in the making. I found some Olivetti Z8000 motherboards for minicomputers on a PC-fair, but never powered them up. Later Zilog also designed a Z80,000 I think.

### Z80获得新操作系统

由: 艾尔·威廉姆斯 | 16条评论 | 2022年11月8日    

如果你喜欢Z80电脑，但又想体验新的操作系统，可以试试Zeal。你可以在下面的视频中观看开源操作系统的演示。

正如您所期望的，整个系统是用Z80汇编语言编写的。您所期望的特性已经具备:文件、目录、设备驱动程序、时钟，甚至支持高达16M内存的内存库。这项工作还没有完全完成，也不是最初的目标计算机- Zeal -但它看起来像一个伟大的作品到目前为止，将会感兴趣的任何人谁有Z80。

如果手边没有Z80 CPU，也可以在浏览器的模拟模式下尝试。作者想要一个简单的操作系统，不是多线程的，可以在ROM中运行。内核大约需要6K的ROM和1K的RAM，这取决于配置。系统调用和一般操作会让你想起Linux。

有一大堆事情要做，所以如果你想参与进来，就不会缺少想法。世界还需要另一个Z80操作系统吗?就此而言，世界还需要更多的Z80系统吗?我们不了解这个世界，但我们总是可以利用它们。它们甚至不需要花费很多钱。

### 关于“Z80获得新操作系统”

- Sanouage 说: 2022年11月9日凌晨2:34

  > 非常有趣，雄心勃勃。它不仅有Zeal 8位操作系统，而且创作者正在制作Z80计算机。我认为这个世界仍然需要Z80，尤其是在IT教育领域。为什么不从Z80开始教授计算机科学呢?

  - 约书亚 说: 2022年11月10日凌晨1点28分

    > MP/M II也是一个很棒的操作系统。它有一个实际的软件库(可以运行多个CP/M程序，有Turbo Pascal 3等)。

    > 还有SymbOS，另一个充满活力的应用操作系统。它甚至有一个RAD IDE，类似于Delphi或Visual Basic。

  - PPJ 说: 2022年11月11日下午5:42

    > 不久以前，一个学生可能会被微软起诉，因为他不可能自己编写一个操作系统。今天，人们为自己设计的计算机编写操作系统。令人印象深刻的工作。

- 艾蒂安 说: 2022年11月9日凌晨2点44分

  > 我开始学习汇编编程的第一个芯片是在德州仪器计算器(TI-82)上，也是最初的gameboy中使用的芯片。很高兴看到这个芯片在这么多年后仍然受到关注。我猜它是计算机历史上的标志性设备的一部分，比如commodore、amiga、摩托罗拉68k……

  - Mog 说: 2022年11月9日凌晨3点57分

    > 最初的Game Boy使用的芯片本身就是计算史上一个有趣的分支。

    > Game Boy使用的CPU是夏普LR35902，它与Zilog Z80有许多共同的功能，但与英特尔8080相比，Z80缺乏很多人们联想到的东西。

    > 以' CB '字节为前缀的操作码存在，但其余的前缀指令(DD, ED, FD)不存在。' shadow '寄存器文件不存在。对DRAM刷新的板上支持(' R '寄存器)则不是。

    > 事实上，夏普被选为Game Boy的CPU也不是巧合，因为在Game Boy开发时，任天堂已经与夏普合作了7年，因为该公司的4位微控制器系列(夏普SM5x和SM51x)是任天堂所有“游戏和手表”系列的基础，可以追溯到1981年。夏普的4位sm5系列微控制器的分支也是专用的钥匙/锁芯片，称为“CIC”，出现在NES, SNES和N64上，表面上是为了保护拷贝(尽管更现实的是为了锁定供应商)。

    - 多 说: 2022年11月9日上午5:44

      > 此外，gameboy CPU缺乏索引寄存器，这使得它非常不是一个Z80。因此，任何Z80代码都很可能无法在gameboy上运行。

      > 一切都指向gameboy CPU有一个SM83核心，因为在该核心上发现的文档与gameboy CPU匹配1:1。

      > Z80和SM83都是基于8080的，这就是他们共享很多东西的地方。程序集语法是Z80风格，因为8080风格非常令人困惑。

      > 与此相关的是，那些4位cpu(例如SM51x)真的很奇怪，如果你想搞混，查找它们的指令集，以及程序计数器如何不只是向上计数。

      - 只是路过 说: 2022年11月9日早上6:58

        > 事实上，我发现8080的助记符比Z80的更直观;他们不会引诱你去想那些不可能的事情。看起来像特例的东西其实是特例。

        - 只是路过 说: 2022年11月9日早上6:58

          > 抱歉，我应该说“特殊情况下看起来像这样”。

      - 约书亚 说: 2022年11月10日凌晨1点40分

        > 让我们不要忘记DMG-01的CPU核心内部的声音芯片。GB能够立体声，有两个脉冲通道(方)，一个噪声通道，一个方波通道，3位音量控制。

        > 我想，大多数人认为Gameboy拥有Z80的原因是仿真的历史。Z80有很好的记录，不仅仅是官方来源。因此，如果你正在开发一个Gameboy项目，你可能会从现有的Z80模拟核心开始，并开始在此基础上构建，而不是从头开始执行奇怪的SM83架构。

      - 佩德罗·卢卡斯 说: 2022年11月12日早上5点19分

        > Z80实际上匹配8085的机器代码，我记得在Uni中为8085编程，然后在周末用我的旧ZX Spectrum把二进制代码带回家运行/测试我的代码。

- ButtSoup巴恩斯 说: 2022年11月9日凌晨2点49分

  > 小崽子，我可以用我的TS/1000做点什么。(有内存扩展器)只需要去旧货店找到一个空白磁带。

- JRD 说: 2022年11月9日上午8:57

  > 错过了这个细节:“首先确保你有一个MMU，它将Z80的64KB地址空间划分为4个16KB的页面。这是主要的要求。”因此，虽然它在任何时候可能只有几个K的内存，但它仍然需要更多的内存，只是被推到其他地方。

  > 该死的。我们中有些人喜欢玩那些没有完整的64K可用空间的老式机器——比如TRS-80s，它开始时ROM损失了16K，当然也没有存储内存。TRS-80 dose有一个不到3K的常驻内核“核”，有一个1K的区域用于磁盘覆盖“/SYS”文件。(NewDOS最多有21个SYS文件。)

  > 拜托，伙计们，即使是(非常)早期的UNIX也可以在小K的容量下运行，难道我们不能在不需要100+K内存的情况下压缩一个操作系统吗?

  - 约书亚 说: 2022年11月10日凌晨2:08

    > “首先确保你有一个MMU，它将Z80的64KB地址空间划分为4个16KB的页面。这是主要的要求。”

    > 这是典型的银行交换模型，在20世纪80年代中后期由扩展内存规范(EMS)使用。我不知道这已经是一个成熟的MMU了。

    > 在16位x86上，64KB是单个段的大小(在Z80上是完整的地址空间)。

    > EMS的小页帧的大小也为64KB。在这个通常位于640KB-1MB之间的窗口中，有4个16KB的页面。

    > 应用程序可以使用这四个页面来存储/读取信息。内存管理器用于银行交换硬件和EMS应用程序之间的通信。简单地说。

    - 我的湿巾干了 说: 2022年11月15日晚9:42

      > 相当多早期的mmu只不过是用硅做的。

      > 例如，摩托罗拉68451通常被用作一个简单的硬件分割系统，而不是任何更像mmu的东西(在现代定义中)。它可以用于段样式*或页样式的使用，但一次只能支持32个同时发生的段/页;如果需要更多同步段，则需要使用多个物理68451。

      > 68000在其内存访问陷阱中有一个错误，通常在陷阱被处理后阻止完全恢复状态，但68010已经修复了，因此可以使用68451来做真正的虚拟内存。由于表中只有32个段，68010+68451系统通常感觉像是一个耐心的练习，因为修改段表并不是完全立即的。

      > 我曾经看到一个专业系统使用12或16 68451，那时候它的价格比一架小型商务机还要贵。对于那些无法在较小的系统中解决的问题，它是非常棒的，但是如果你的问题能够在较小的系统中解决，那么*任何*其他系统都会更快:)

      > 我想它是在器官共享系统的变体上运行的，但他们不让我们碰它。它几乎已经过时了，但业主仍然有几个正在进行的项目依赖于它。它是用来排版的，他们非常小心，不让外人碰它，而不是刚刚交付的价值1500万美元的工艺印刷机。

- 科尔先生 说: 2022年11月10日凌晨4:55

  > 以ZILOG Z8000 CPU为目标不是更好吗

  - Jaap van Ganswijk 说: 2022年11月10日上午8:28

    > Z8000相当复杂。Z800要好得多，但我不知道它是否实现了，还有其他一些类似Z800的设计正在制作中。我在一个个人电脑展上找到了一些用于小型计算机的Olivetti Z8000主板，但从来没有给它们上过电。后来Zilog还设计了z80000。

### 纪念一下，Zeal 8位计算机上Hackaday了

@Zeal8bit [doge][微笑]（ https://hackaday.com/2022/11/08/z80-gets-new-os/ ）

![输入图片说明](https://foruda.gitee.com/images/1673884485218687068/7f4b53b7_5631341.png "屏幕截图")

# [我自研的Zeal8bit操作系统已经全部开源在GitHub上了。](https://gitee.com/shiliupi/Zeal-8-bit-OS/issues/I5RTSE)

![输入图片说明](https://foruda.gitee.com/images/1663574331777182878/387fa80b_5631341.png "屏幕截图")

我自研的Zeal8bit操作系统已经全部开源在GitHub上了。目前已有功能：

1. 单任务系统，整个CPU的计算专用于单个应用程序
2. 由两部分构成：内核+驱动
3. ROM-able系统
4. 全部由Z80汇编语言写成，保证最高效和紧凑
5. 有硬件抽象层HAL
6. 最多支持26个磁盘
7. 支持MMU，可以有大于64KB的地址空间
8. 支持文件、文件夹、多个文件系统、日期和时间
9. 可以动态加载二进制文件
10. 在不影响现有软件的情况下，完成升级/修改

全部开源，大家可以去Github观看：https://github.com/Zeal8bit/Zeal-8-bit-OS

另外，Zeal-WebEmulator网页模拟器也全部开源：https://github.com/Zeal8bit/Zeal-WebEmulator/

关于Zeal8bit操作系统解说、演示和使用，我正在做一期视频，敬请期待[脱单doge]  
最后祝各位中秋快乐[滑稽]

![输入图片说明](https://foruda.gitee.com/images/1663573240841811731/55129e46_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1663573251001978399/8432571d_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1663573270383888978/8af4577d_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1663573281426148973/7f6bd459_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1663573297097884968/6653c416_5631341.png "屏幕截图")

---

【笔记】

- 【最新原型】[自制8位计算机](https://www.bilibili.com/video/BV1Kr4y1L73E)

  https://www.bilibili.com/video/BV1Kr4y1L73E

  ![输入图片说明](https://foruda.gitee.com/images/1663573397130551029/229f57c9_5631341.png "屏幕截图")

  ![输入图片说明](https://foruda.gitee.com/images/1663574230353934080/935231b0_5631341.png "屏幕截图")

- [做了件T恤，你们觉得如何？](https://gitee.com/shiliupi/Zeal-8-bit-OS/issues/I5RTR7)[脱单doge]

  ![输入图片说明](https://foruda.gitee.com/images/1663573161682943382/7aef7194_5631341.jpeg "e6fdad056c858f4d7d224d4d0e232c3e1042658991.jpg")

- [Zeal8bit welcome](https://gitee.com/shiliupi/Zeal-8-bit-OS/issues/I5RTQ1)

- [Z80 GETS NEW OS](https://gitee.com/shiliupi/Zeal-8-bit-OS/issues/I6AT67)