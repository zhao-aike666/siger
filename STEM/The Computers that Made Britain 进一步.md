 **Further reading, further viewing and forums** 进一步阅读，进一步查看和论坛

如果你想读更多的书，你可以在网上找到很多优秀的资源和书籍。虽然这里的许多书已经不再印刷，但仍有一些以电子书、PDF下载和存档网站的形式存在。

p283

 **通用背景介绍** 

-  _The Mighty Micro: The Impact of the Computer Revolution_ 
克里斯托弗·埃文斯 (Christopher Evans)，《 _强大的 Micro: 计算机革命的影响_ 》，1979年，维克多·戈兰茨 (Victor Gollancz)，ISBN 0575027088
-  _Fire in the Valley: The Birth and Death of the Personal Computer_ 
Paul Freiberger 和 Michael Swaine，《 _山谷之火: 个人电脑的诞生和死亡_ 》，1984, McGraw-Hill, ISBN 1937785769
-  _Electronic Dreams: How 1980s Britain Learned to Love the Computer_ 
汤姆·里恩 (Tom Lean)，《 _电子梦:20世纪80年代的英国人如何学会爱上电脑_ 》，2016年，布鲁姆斯伯里世嘉出版社 (Bloomsbury Sega)，ISBN 9781472918338
-  _Digital Retro: The Evolution and Design of the Personal Computer_ 
戈登·莱恩 (Gordon Laing)，《 _数字复古:个人电脑的进化与设计_ 》，2004年，Sybex，ISBN 078214330X

 **Acorn/BBC Micro** 

-  _The Legacy of BBC Micro_ 
蒂莉·布莱斯 (Tilly Blyth)，《 _BBC Micro的遗产_ 》，Nesta, 2013年，
[ **nesta.org.uk/report/the-legacy-of-bbc-micro** ](http://nesta.org.uk/report/the-legacy-of-bbc-micro) (免费PDF下载)
- BBC 计算机素养项目 (1980-1999)
 **[clp.bbcrewind.co.uk](http://clp.bbcrewind.co.uk/)** 

 **Amstrad** 

-  _What You See Is What You Get: My Autobiography_ 
艾伦·休格 (Alan Sugar)，《 _所见即所得: 我的自传_ 》，Pan Books 2011, ISBN-13 9780330520478
-  _Alan Sugar: The Amstrad Story_ 
大卫·托马斯 (David Thomas)，《 _艾伦·休格: Amstrad 的故事_ 》，1990，ISBN 0712635181
Amstrad CPC 464 group on Facebook

 **Apple**  (苹果)

-  _Becoming Steve Jobs: The Evolution of a Reckless Upstart into a Visionary Leader_ 
布伦特·施伦德 (Brent Schlender) 和 里克·特策利 (Rick Tetzeli)，《 _成为史蒂夫·乔布斯: 从鲁莽的暴发家到有远见的领导者_ 》，2015年，皇冠商业 (Crown Business)，ISBN 0385347405
-  _Steve Jobs_ 
沃尔特·艾萨克森 (Walter Isaacson)，《 _史蒂夫·乔布斯_ 》，2011，西蒙与舒斯特出版社 (Simon & Schuster)，ISBN 1451648537
-  _Anecdotes about the development of Apple’s original Macintosh, and the people who made it_ 
安迪·赫茨菲尔德 (Andy Hertzfeld) 等人, Folklore.org, “ _苹果最初的麦金塔电脑开发的轶事，以及谁造的它_ ”

 **Commodore**  （海军准将）

-  _On the Edge: The Spectacular Rise and Fall of Commodore_ 
Brian Bagnall，《 _在边缘上: Commodore 电脑的盛与衰_ 》，2005年，变种 (Variant) 出版社，ISBN 0973864907
-  _Commodore: The Amiga Years_ 
Brian Bagnall，《 _Commodore 电脑: Amiga 的岁月_ 》，2012年，Variant (变种) 出版社，ISBN 0973864990

p284

 **IBM** 

-  _Blue Magic: The People, the Power and the Politics behind the IBM Personal Computer_ 
詹姆斯·切波斯基 (James Chposky) 和 泰德·莱昂西斯 (Ted Leonsis)，《 _蓝色魔法: IBM个人电脑背后的人民、权力和政治_ 》，ISBN 0246134453

 **Sinclair**  辛克莱

-  _The Sinclair Story_ 
罗德尼·戴尔 (Rodney Dale)，《 _辛克莱的故事_ 》，1985年，达克沃斯 (Duckworth)，ISBN 0715619012
-  _Sinclair and the Sunrise Technology_ 
Ian Adamson 和 Richard Kennedy, 《 _辛克莱与朝阳技术_ 》, 1986, Penguin, ISBN 0140087745

 **档案杂志** 

如果你想追溯计算机的早期历史，而且手头有很多时间，那么没有比阅读杂志的存档期刊更好的方法了。你可以通过互联网档案馆在网上读到很多东西，但要看 “真正的东西”，就去国家计算机博物馆 (the National Museum of Computing)，它与布莱切利公园 (Bletchley Park) 位于同一地点，就在米尔顿凯恩斯 (Milton Keynes) 外。

 **存档的报纸** 

这本书的创作离不开在线报纸档案。许多报纸可以通过你当地的图书馆找到 —— 只需登录其在线服务，查看有什么可用 —— 但其它报纸 (如英国《金融时报》) 基本上仅限于学术机构。这本书还依赖于 newspapers.com (付费服务) 上的国际档案。

 **YouTube** 油管

如果你想亲眼看到英国计算机界的英雄们被采访，计算历史中心 (The Centre for Computing History) 有一个 YouTube 频道，从 Steve Furber (Acorn 的创始人) 到 Rick Dickinson (Sinclair 最成功的计算机的已故设计师)。

要想了解美国人的观点，不妨去看看计算机历史博物馆 (Computer History Museum) 的一系列类似视频。也许最有价值的是它的一系列口传历史，包括 查克·佩德尔 Chuck Peddle (Commodore PET首席设计师) 和 帕特·盖尔辛格 Pat Gelsinger (英特尔现任首席执行官) 的口传历史。

 **电影** 

-  _The Commodore Story_ 《Commodore 电脑的故事》，2018年，史蒂文·弗莱彻 (Steven Fletcher) 导演
-  _Micro Men_ 《Micro 人》, 2009年，索尔·梅茨斯坦 (Saul Metzstein) 导演
-  _Steve Jobs: The Man in the Machine_ 《史蒂夫·乔布斯: 机器里的人》，2015年，亚历克斯·吉布尼 (Alex Gibney) 执导

### P283 - P285