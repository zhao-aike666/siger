【笔记】[Z80 fpga](https://gitee.com/shiliupi/Zeal-8-bit-OS/issues/I6AT9P)

<p><img width="706px" src="https://foruda.gitee.com/images/1673948506497832181/a79acb4d_5631341.jpeg" title="511Z80副本.jpg"></p>

> 这是一篇纯粹的博客考古，一个喜欢复古计算机的黑客学习计算机技术的过程，它会用清风表达自己对生活的热爱，还会还以爱心给SOS儿童村，这是同学们应有的学习态度。

本期专题要感谢树莓派基金会的计算机的80年代给我的线索，辛克莱计算机的名字要早于这篇博文进入我的脑海，还要感谢 Zeal 复古计算机项目的作者，对它的专访，确定了这个项目的教育意义，其开源的Z80操作系统对研究复古计算机具有现实意义，让我们的软核可以运行更多精彩的复古应用，重要的是新应用。这就是社区应该有的样子。我将把学习目标定位为制造一台可以运行 Zeal 操作系统的兼容机，使用上我一知半解的全栈知识。

> 新年快乐！2023 前兔无量！ :pray:

A Z80 From the Ground Up A-Z80 is a conceptual implementation of the venerable Zilog Z80 processor targeted to synthesize and run on a modern FPGA device. It differs from the existing (mostly Verilog) Z80 implementations in that it is designed from the ground up through the schematics and low-level gates.

- Zilog Z80 FPGA - Baltazar Studios  
  baltazarstudios.com/z80-ground/

  - What is a-z80?
  - What is the a-z80 implementation?
  - What is the speed of the Z80?
  - What is the difference between Verilog and Z80?

- Obijuan/Z80-FPGA: Z80 CPU for OpenFPGAs, with …  
  https://github.com/Obijuan/Z80-FPGA
  > 2019年9月12日 · Z80-FPGA. Z80 CPU for OpenFPGAs, with Icestudio. Based on the iceZ0mb1e project by abnoname and the TV80 CPU; Quick start. Open the z80-soc-16KB-Boot.ice file with …

- Laptop Like It’s 1979 With A 16-Core Z80 On An …  
  https://hackaday.com/2019/12/10/laptop-like-its...
  > 2019年12月10日 · The Stratix FPGA formed the heart of the design, surrounded by a few breakout boards for the 10.1″ VGA display and the keyboard, which …

- Verilog Implementation of a Z80 Compatible Processor Architecture
  https://github.com/Time0o/z80-verilog

- Overview :: A-Z80 CPU :: OpenCores  
  https://opencores.org/projects/a-z80

  - Description
    > Update: Rewritten in pure Verilog, the CPU can now be used on both Altera and Xilinx devices! A-Z80is a conceptual implementation of the venerable Zilog Z80 processor targeted to synthesize and run on a modern FPGA device. It differs from the existing (mostly Verilog) Z80 implementations in that it is designed from the gr…
  - Features
  - Documentation
  - Testbench
  - Status
  - Image

# [一台Z80从头开始](https://baltazarstudios.com/z80-ground/)

a -Z80是古老的Zilog Z80处理器的概念实现，旨在合成和运行在现代FPGA设备上。它与现有的(主要是Verilog) Z80实现的不同之处在于，它是通过原理图和低层门从头开始设计的。

这个设计能够模拟实际的Z80 CPU，它说明了它的内部工作原理，因为它是已知的在这个时候(*)。

### 简介

在现代FPGA上重新实现一个旧的NMOS和完全定制设计的电路是非常具有挑战性的，并为设计师提供了一个很好的学习经验，并且，希望设计对一个好奇的复古爱好者来说也是非常有信息和有趣的。

技术的差异设置了一些有趣的限制，并要求一定程度的抽象:

在最低水平上，Z80使用简单的双晶体管锁存器，这在fpga中是不实际的。ASIC触发器可能是双边触发，而FPGA只支持单边触发;内部Z80总线是反向的(与NMOS逻辑很好地工作)，而自然FPGA设计使用正逻辑;在现代FPGA设计中不鼓励使用内部三态总线;两种技术的底层时序特性不同;将时序传播延迟转换到FPGA是非常困难的，其中时序可能会根据所采用的路径拟合算法等而发生很大变化。

我所看到的所有现有Z80 FPGA实现都实现了行为模型并使用Verilog。最流行和最广泛使用的FPGA型号是T80。这样的模型不需要深入了解Z80设计的内部细节，因为它们实现了到更高级别芯片块的接口(例如，寄存器文件或ALU)。已知的未记录的行为只是硬编码在源代码中。

A-Z80(此项目)实现力求在内部结构上与原始Z80相同。使用这种方法，模型实现了一个完整的周期精度，并且对所有已记录和未记录的特性具有相同的行为，而不是通过显式地硬编码它们，而是通过模仿实际设计。

该图显示了主要功能块和总线大约映射到位置，因为它们出现在Z80模具上。

[![Z80 CPU top level partitions](https://foruda.gitee.com/images/1673892256986404933/5c38e8b8_5631341.png "z80-top-level-partitions.jpg")](https://foruda.gitee.com/images/1673892406924392106/6cbda27e_5631341.jpeg)

> Z80 CPU顶级分区(T34VM1克隆)

关于Z80内部架构的许多细节都没有公布，在这个项目开发的时候(2014春夏)也并不为人所知。Visual6502团队的一些人从Z80模具的各种微缩照片中逆向设计了几个块。A-Z80的实现主要由Ken Shirriff在他的Z80博客中记录的逆向工程工作指导;他提供了ALU、寄存器文件、地址增量器和PLA表的原始原理图。除此之外，人们可以通过阅读专利，参考手册，历史博物馆谈话，模拟器的来源，Z80设计师以前的作品等来挑选很多细节。本文的Library部分列出了其中一些文档。

### 顶级

从概念上讲，以下是Z80的主要架构块:

[![A-Z80 top level](https://foruda.gitee.com/images/1673892465871730380/c6f58088_5631341.png "屏幕截图")](https://foruda.gitee.com/images/1673892308246065386/225c79d9_5631341.png)

> A-Z80顶级

1. 控制单元，包括:

   - IR(指令寄存器)保存正在执行的当前指令。
   - PLA(可编程逻辑阵列)提供静态指令解码。
   - 排序器生成M (Machine)和t循环的单热编码，以循环方式触发，并为静态解码的指令提供定时信息。这是CPU的“心脏”。
   - 引脚控制基于当前确定外部CPU引脚的时间函数以及t周期。函数有:操作码获取;记忆读，写;IO读，写。
   - 中断块保存用于中断处理的锁存和逻辑。
   - 执行块-计时矩阵-将M/T计时与静态解码指令结合起来，并在精确的时间发射控制信号。

2. 算术和逻辑单元(ALU)进行数字运算。它包括:

   - 在Ken Shirriff的博客上有很好的描述，请阅读。
   - 标志包含临时标志寄存器和根据操作结果设置标志的逻辑。
   - 一组不同的ALU控制功能来处理操作，如DAA，奇偶校验，旋转选择器等。

3. 注册文件包含:

   - Z80系统和通用寄存器(AX, BC，…， sp, pc, ir)
   - 一种选择寄存器的控制单元。这是一个相当复杂的单元，因为它在各种寻址模式下处理多个寄存器交换触发器。

4. Bus是内部总线相关模块的松散集合:

   - 地址锁存器和增量器执行16位的递增/递减操作，并为内存/IO寻址提供地址。肯在他的博客上有很好的描述。
   - 标记为SW1, SW2, SW4)的数据总线交换机用于数据总线沿线的多个位置，以分隔段。它们允许芯片的多个部分在单个内部数据总线上同时操作。
   - 数据引脚和地址引脚实现连接内部总线与CPU引脚的端点垫逻辑。
   - 本博客之后的几篇文章更详细地描述了在这个项目中实现的那些组件。A-Z80设计作为开源发布，可以在GitHub上下载: https://github.com/gdevic/A-Z80

a - z80 CPU在这个阶段是一个完整的，完全工作的FPGA设计。它应用于这里描述的Sinclair ZX Spectrum计算机实现。人们可以在Altera甚至Xilinx设备中使用它，因为原理图被编译成通用的Verilog。

继续阅读关于这个设计的心，思想，灵魂…

---

(*)我不知道的是，Visual 6502团队已经在Z80的逆向工程中取得了一些重大进展，在此期间，我已经完成了大部分的设计，并通过“有意义”的设计即兴/替换了缺失的部分。这个设计;然而，随着6502团队越来越多地了解它，它提供了一个可以修改和对准实际Z80设计的工作基础。

### 9个关于“一台Z80从头开始”

- 艾德 2014年10月25日上午11:46

  > 嗨，戈兰，关注你的工作有一段时间了。恭喜你们取得了这样的成果，我对你们令人难以置信的工作表示钦佩。我太热衷于逆向工程了，这和你说的花一个周末的时间来解决问题有很大关系。呵呵

  > 顺便说一句，我今天在EDABOARD的论坛上发了一些东西，也许你可以看看并帮助我。链接: http://www.edaboard.com/thread325474.html

  > 致以最亲切的问候。

- 格兰德维克 2014年10月26日早上6:44

  > 嗨，Ed，我对CMOS不太熟悉，但它看起来确实是一个电容器。你有一套完整的高分辨率图像吗?如果你假设它是一个帽，那么电路的其余部分有意义吗?谢谢!

  - 艾德 2014年10月26日早上6:59

    > 嗨，Goran，这似乎是一个解耦帽。刚刚在这里发布了另一个挑战，看看: http://www.edaboard.com/thread325534.html

    > 的问候。艾德

    - 马科斯阻碍 2016年9月6日晚上10:39

      > 很棒的博客文章。我当然很欣赏这个网站。继续努力吧!

    - 马克 2018年3月11日下午5:03

      > 我想创建一个ZEMU的硬件版本，是否有可能使用这个FPGA版本来输出寄存器和内部总线的内容，这样你就可以一步通过ROM，看看....里面发生了什么对年轻人来说是一个很好的教学工具

      - gdevic 2018年3月16日上午10:54

        > 它可能需要一个JTAG或其他硬件块来充当接口。这将是一个有趣的练习。

# [关于](https://baltazarstudios.com/about-baltazar/)

这个网站是以巴尔塔扎尔教授的名字命名的，他是我小时候看过的一部动画片的主角。他是一个聪明而实际的发明家，他总是帮助他的同胞们用富有想象力的方法来解决他们奇怪的问题。

同样，我喜欢摆弄东西，寻找各种问题的解决方案——主要是为了学习新事物。

自从我11岁开始在杂志上HP-41的图片上输入想象的程序(在那之前我只是简单地看到了一个真实的HP-41)，然后在第二年夏天，我12岁的时候用手拆开了我的第一个ZX81的ROM，我一生都被硬件/软件的魔法迷住了。

希望你能在这些随意的闲逛中找到一些有价值的东西。如果你这样做了，请给我留言(gdevic @ yahoo.com)，在LinkedIn上连接，或发表评论，并包括你自己的黑客，修复和冒险的链接!

谢谢光临!

——戈兰·迪维克

![输入图片说明](https://foruda.gitee.com/images/1673922191776225226/47e625d9_5631341.png "屏幕截图")

# [气象站](https://baltazarstudios.com/home-mande-weather-station/) 

![Personal Weather Station](https://foruda.gitee.com/images/1673922320560093945/ae43bd0c_5631341.png "屏幕截图")

我使用的是现成的风速表、风向标和雨量计，我将它们连接到定制的ESP32板上，在将数据发送到家庭服务器(带有SSD的Rock64 SoC)之前，ESP32板进行数据收集和处理。服务器软件由CumulusMX、Emoncms和各种Python文件组成，用于将所有部分粘合在一起。

ESP32的固件遵循NOAA对天气收集观测标准的要求。

天气数据被发送到 [WeatherCloud](https://app.weathercloud.net/d0314729095#current)。

每隔15分钟，数据也会上传到这个网站。它部分显示在下面的框架中;点击这里在一个[新标签](https://baltazarstudios.com/cumulus)打开它。

- weathertop weather

  - Latitude N 30° 25' 04"  | Outside(*) Inside
  - Longitude W 97° 45' 50"    | Dew Point() Apparent(*) Wind Chill() Heat Index() Humidex()
  - Elevation 920 ft  | Outside(*) Inside

  | | | | | |
  |---|---|---|---|---|
  | Temperature: | (*)°C | °F |
  | Rainfall: | (*)mm | Inch | 
  | Pressure: | (*)hPa | inHg | mb | kPa
  | Wind Speed: | (*)km/h | m/s | mph | knots
  | CloudBase: | (*)m | ft |

scripts by mark crossley - version  
gauges drawn using gerrit grunwald's [steelseries](http://harmoniccode.blogspot.com/) [javascript library](https://github.com/HanSolo/SteelSeries-Canvas)  
wind rose drawn using rgraph

:[now](https://baltazarstudios.com/cumulus/index.htm)::[gauges](https://baltazarstudios.com/cumulus/gauges.htm)::[today](https://baltazarstudios.com/cumulus/today.htm)::[yesterday](https://baltazarstudios.com/cumulus/yesterday.htm)::[this month](https://baltazarstudios.com/cumulus/thismonth.htm)::[this year](https://baltazarstudios.com/cumulus/thisyear.htm)::[records](https://baltazarstudios.com/cumulus/record.htm)::[monthly records](https://baltazarstudios.com/cumulus/monthlyrecord.htm)::[trends](https://baltazarstudios.com/cumulus/trends.htm)::[historic](https://baltazarstudios.com/cumulus/historic.htm)::[forum](https://cumulus.hosiene.co.uk/):

page updated 01/16/2023 20:15:00
powered by v (b)

# [Software](https://baltazarstudios.com/software/)

### Windows PC

- GitForce is a visual front-end to git: https://sites.google.com/site/gitforcetool/homeOpens in a new window

- Z80 Explorer is a Zilog Z80 netlist-level simulator: https://baltazarstudios.com/z80explorerOpens in a new window

### Android

- PlayZX is a Sinclair retro games converter/player: https://baltazarstudios.com/playzxOpens in a new window

- Flight Sim Remote Player is an X-Plane's GA instrument panel: https://baltazarstudios.com/flight-sim-remote-panelOpens in a new window

### Linux

- LinICE is a source-level kernel debugger: https://sites.google.com/site/linicetool

# [Baltazar Studios](https://baltazarstudios.com/blog/)

Goran Devic's blog on hacks, fixes and adventures!

[<img width="199px" src="https://foruda.gitee.com/images/1673923296166012749/ad713273_5631341.png">](https://gglabs.us/)

> About Us

  All content on the website is copyrighted (C) 2015-2022 by Gabriele Gorla. All hardware designs/documentation/code on this website are released under the Gnu General Public License (GPL) V3.1

- [SDCC 4.2.0 Released](https://gglabs.us/node/2316)  
  Submitted by GG on Tue, 03/08/2022 - 13:47

  > SDCC logoA new release of SDCC, the portable optimizing compiler for STM8, MCS-51, DS390, HC08, S08, Z80, Z180, Rabbit, SM83, eZ80 in Z80 mode, Z80N, TLCS-90, 6502, Padauk and PIC microprocessors is now available. Sources, documentation and binaries for GNU/Linux amd64, Windows amd64, macOS amd64 and Windows x86 are available.

[<img width="199px" src="https://foruda.gitee.com/images/1673923301727730684/e40ec21c_5631341.png">](https://www.sosnepal.org.np/)

- [Ways to help](https://www.sosnepal.org.np/ways-to-help)

  You can help change a child's life

  **You can trust us** 

  > Having established 96 homes in 10 SOS Villages in Nepal in the last 50 years, we are one of the largest self-implementing NGOs in the country, providing for over 1890 children currently. SOS Children's Villages Cambodia is an approved social development organization.

  **Our Impact** 

  - children and youth lives improved
  - ongoing projects in Nepal
  - children presently under care
  - years of service in Sri Lanka

## CATEGORIES

- [ARDUINO](https://baltazarstudios.com/category/arduino/)
- [EAGLE](https://baltazarstudios.com/category/eagle/)
- [FIX](https://baltazarstudios.com/category/fix/)
- [FPGA](https://baltazarstudios.com/category/fpga/)
- [HACKS](https://baltazarstudios.com/category/hacks/)
- [PCB](https://baltazarstudios.com/category/pcb/)
- [RASPBERRY PI](https://baltazarstudios.com/category/raspberry-pi/)
- [RETRO](https://baltazarstudios.com/category/retro/)

  - [PlayZX](https://baltazarstudios.com/playzx/)

    > PlayZX is an Android application that lets you select from thousands of Sinclair ZX Spectrum games and play them through the headphone jack to load them onto your Speccy. You can also select your local (on the device) files, convert them to sound files, and then play them. This way you can load games for not only the ZX Spectrum micro but also a few other retro computers that have a compatible audio jack. 

    > PlayZX是一款Android应用程序，可以让你从数千款Sinclair ZX Spectrum游戏中进行选择，并通过耳机插孔将它们加载到你的Speccy上。您还可以选择您的本地(设备上)文件，将它们转换为声音文件，然后播放它们。这样你不仅可以为ZX Spectrum micro加载游戏，还可以为其他一些具有兼容音频插孔的复古电脑加载游戏。

  - [Z80 Explorer](https://baltazarstudios.com/z80explorer/)

    > Z80 Explorer is a Zilog Z80 netlist-level simulator capable of running Z80 machine code and also an educational tool with features that help reverse engineer and understand this chip better.

    > Z80 探险家 是Zilog Z80网列表级模拟器，能够运行Z80机器代码，也是一个具有帮助逆向工程和更好地理解该芯片功能的教育工具。

  - [ZX Spectrum ROM mods](https://baltazarstudios.com/zx-spectrum-rom-mods/)

    > In this blog, I will show you how to interface an Atari-style joystick to the Altera DE1 FPGA board running a Spectrum implementation, how to change the ROM to enable you to input some game-cheat pokes, and a few games I eventually completed using this setup.

    > 在这篇博客中，我将向您展示如何将雅达利风格的操纵杆连接到运行Spectrum实现的Altera DE1 FPGA板上，如何更改ROM以使您能够输入一些游戏作弊戳，以及我最终使用此设置完成的一些游戏。

  - [The A-Z80 CPU](https://baltazarstudios.com/z80-cpu/)

    > This article contains a brief overview and a background of the A-Z80 CPU created for FPGA boards and a ZX Spectrum implementation tied to it.

    > (You can find the Russian translation of this article here: https://howtorecover.me/z80-s-nulyaOpens in a new window)

    > 本文包含为FPGA板创建的a - z80 CPU的简要概述和背景，以及与之相关的ZX Spectrum实现。(你可以在这里找到本文的俄文翻译: https://howtorecover.me/z80-s-nulya )

  - [ZX Spectrum on FPGA using A-Z80 CPU](https://baltazarstudios.com/sinclair-zx-spectrum-z80/)

    > In the last articleOpens in a new window, I presented a different way of architecturally modeling a Zilog Z80 processor. It is time to do something really useful with it and what could be better than reliving the past for a moment? Let's recreate an old computer and load in and play some games!

    > Sinclair ZX Spectrum was my second computer, the first one being ZX81. As I was growing up in Croatia, I had a good friend whose brother, living in Germany, regularly sent him tapes with new games. Through this steady stream of games I've probably seen most of them (thank you, Krešo!) However, although I could, I had never played them through. Instead, I would load in DevPac Opens in a new window- a debugger - and disassemble them trying to decipher how they were made and occasionally find some pokes for infinite lives.

    > 在上一篇文章中，我介绍了一种从架构上建模Zilog Z80处理器的不同方法。是时候用它做一些真正有用的事情了，还有什么比重温过去更好的呢?让我们重新创造一台旧电脑，并加载和玩一些游戏!Sinclair ZX Spectrum是我的第二台电脑，第一台是ZX81。我在克罗地亚长大，我有一个好朋友，他的哥哥住在德国，经常给他寄新游戏的磁带。通过这种稳定的游戏流，我可能已经看到了其中的大部分(谢谢你，kreo !)然而，虽然我可以，但我从来没有玩过。相反，我将装入DevPac(一个调试器)，并将它们拆解，试图破译它们是如何生成的，偶尔会找到一些戳，以获得无限的生命。

  - [Z80 Library](https://baltazarstudios.com/z80-library/)

    > This is a list of some hard-to-find publications on the Z80 CPU.

    > Z80图书馆：这是Z80 CPU上一些很难找到的出版物的列表。

  - [A Z80 : References](https://baltazarstudios.com/z80-gratitude/)

    > This design would not be possible without Ken Shirriff who reverse-engineered some major portions of Z80 from the picture of a die. These are the portions of the A-Z80 design that are based on his work:

    > 参考资料 : 如果没有Ken Shirriff，这个设计是不可能的，他从一个模具的图片中逆向设计了Z80的一些主要部分。以下是A-Z80的部分设计是基于他的工作:

  - [A Z80 : The Soul](https://baltazarstudios.com/z80-soul/)

    > In the first post, I described the sequencer, a circuit that provided discrete timing signals to space operations apart. In the second post, I mentioned the Timing matrix that was run by these signals and orchestrated the dance of control signals in time.

    > This article is about making it all alive and kicking within an FPGA solution.

    > A Z80: 灵魂 : 在第一篇文章中，我描述了定序器，这是一种为空间操作提供离散定时信号的电路。在第二篇文章中，我提到了由这些信号运行的时间矩阵，并及时编排了控制信号的舞蹈。本文将介绍如何在FPGA解决方案中实现这一切。

  - [A Z80 : The Mind](https://baltazarstudios.com/z80-mind/)

    > In the last article, I described the sequencer, which is the heart of a CPU, and a few other blocks that perform various tasks. But how is it all orchestrated to perform useful work?

    > Enter the PLA and the Timing matrix - the mind of a CPU.

    > 心灵: 在上一篇文章中，我描述了排序器，它是CPU的核心，以及其他一些执行各种任务的块。但是，这一切是如何安排来执行有用的工作的呢? 进入PLA和时序矩阵- CPU的思想。

  - [A Z80 : The Heart](https://baltazarstudios.com/z80-the-heart/)

    > **Click on any image to open a higher-resolution version.** 

    > This is how it all works.

    > The sequencer is "the heart" of a CPU. It gets the external clock which in turn toggles two rows of flip-flops that generate machine cycles (M-cycles) and clock periods (T-states).

    > 心: 点击任何图片打开一个更高分辨率的版本。 这就是它的工作原理。 排序器是CPU的“心脏”。它获取外部时钟，然后切换生成机器周期(m周期)和时钟周期(t状态)的两行触发器。

      <p><img height="99px" src="https://foruda.gitee.com/images/1673925280300891383/f80467dd_5631341.png" title="A-Z80 CPU定序器"><img height="99px" src="https://foruda.gitee.com/images/1673925317780280653/7d96326d_5631341.png" title="A-Z80 CPU时钟延时"><img height="99px" src="https://foruda.gitee.com/images/1673925362211853527/11fd9332_5631341.png" title="A-Z80 CPU地址锁扣"><img height="99px" src="https://foruda.gitee.com/images/1673925423623658568/1e1130fb_5631341.png" title="A-Z80 CPU地址引脚"><img height="99px" src="https://foruda.gitee.com/images/1673925492424850393/7cf48bb4_5631341.png" title="A-Z80 CPU数据引脚"><img height="99px" src="https://foruda.gitee.com/images/1673925535063584386/1bac3877_5631341.png" title="A-Z80 CPU地址增减"><img height="99px" src="https://foruda.gitee.com/images/1673925566207501555/f3d0fb57_5631341.png" title="A-Z80 CPU 2位增减"><img height="99px" src="https://foruda.gitee.com/images/1673925598061686740/5fc0937f_5631341.png" title="A-Z80 CPU寄存器文件"><img height="99px" src="https://foruda.gitee.com/images/1673925624132106665/7266cd9e_5631341.png" title="A-Z80 CPU寄存器锁存"><img height="99px" src="https://foruda.gitee.com/images/1673925658834854222/7e1b3b48_5631341.png" title="A-Z80 CPU寄存器控制"></p>

  - [A Z80 From the Ground Up](https://baltazarstudios.com/z80-ground/)

    > A-Z80 is a conceptual implementation of the venerable Zilog Z80 processor targeted to synthesize and run on a modern FPGA device. It differs from the existing (mostly Verilog) Z80 implementations in that it is designed from the ground up through the schematics and low-level gates.

    > 一台Z80从头开始：a -Z80是古老的Zilog Z80处理器的概念实现，旨在合成和运行在现代FPGA设备上。它与现有的(主要是Verilog) Z80实现的不同之处在于，它是通过原理图和低层门从头开始设计的。

  - [The Anatomy of a Z80 Gate](https://baltazarstudios.com/anatomy-z80-gate/)

    > The data bus on the Z80 processor is 8 bits wide. Data bus wires carrying information within the chip itself do not simply connect to package pins and out to the world - the gate circuitry of each bit is quite complex. This article presents a transistor-level schematic of a data bit’s gate which I reverse-engineered from a die photograph.

    > Data pins (D0-D7) carry arguably the most complex signals on the Z80 since they are both bi-directional and capable of tri-stating. They are located around the +5V pin – four of them on each side. This is a microphotograph of a gate of one of the data pins which we will look at more closely today – a pin for a data line D6.

    > 门的解剖: Z80处理器上的数据总线是8位宽。在芯片内部携带信息的数据总线线并不是简单地连接到封装引脚并输出到外部——每个位的门电路是相当复杂的。这篇文章提出了一个晶体管级的数据位门的示意图，我从一个模具照片逆向工程。数据引脚(D0-D7)可以说是Z80上最复杂的信号，因为它们都是双向的，并且能够进行三态。它们位于+5V引脚周围-每边有四个。这是一个数据引脚的门的微缩照片，我们今天将更仔细地观察它-数据线D6的引脚。

  - [Z80 Instruction Register deciphered](https://baltazarstudios.com/z80-instruction-register-deciphered/)

    > After reading excellent Ken Shirriff's blog on reverse-engineering parts of the Z80 CPU, I decided to learn how to decipher some of the chip die-shots myself. It turns out not to be that difficult if you follow certain guidelines, which I will describe in this post.

    > Start with a good and clean die shot. Although the Visual 6502 team had a good one, it was somewhat grainy, and I've found a much cleaner version here. There is a slight difference in masks, but the functions are the same. In fact, it may even help to look at several versions when trying to decipher the layout.

    > 指令寄存器已破译: 在阅读了Ken Shirriff关于Z80 CPU逆向工程部分的优秀博客后，我决定自己学习如何破译一些芯片模射。事实证明，如果你遵循某些指导方针，这并不难，我将在这篇文章中描述。从一个好的干净的骰子开始。尽管Visual 6502团队有一个很好的版本，但它有点粗糙，我在这里找到了一个更干净的版本。遮罩略有不同，但功能是一样的。事实上，在尝试破译布局时，查看几个版本甚至会有所帮助。

  - [ZiLOG Z80 (un)documented behavior](https://baltazarstudios.com/zilog-z80-undocumented-behavior/)

    > In my previous post, I described an Arduino dongle and the software that can be used to clock a Z80 CPU and dump the states of its buses and pins while executing a controlled set of test cases.

    > Here I show a trace of every single Z80 instruction as run by that setup. I also outlined some of the tests created manually that clarified a few situations which were not too obvious (to me) after reading various pieces of documentation.

    >  (un)记录的行为：在我之前的文章中，我描述了一个Arduino加密狗和软件，可以用来为Z80 CPU时钟，并在执行一组受控测试用例时转储其总线和引脚的状态。在这里，我显示了由该设置运行的每一条Z80指令的跟踪。我还概述了一些手动创建的测试，在阅读了各种文档后，这些测试澄清了一些(对我来说)不太明显的情况。

  - [Arduino and ZiLOG Z80](https://baltazarstudios.com/arduino-zilog-z80/)

    > If you want to find out exactly what a venerable Z80 is doing on its bus while executing instructions, in this post I outlined a dongle and the software that will let you see that. Using just a few components and connecting them to an Arduino Mega, you can trace instructions clock by clock and observe what's happening on the bus.

    > 如果你想知道一台可敬的Z80在执行指令时到底在总线上做什么，在这篇文章中，我概述了一个加密狗和软件，它会让你看到这一点。只需使用几个组件，并将它们连接到Arduino Mega，您就可以逐时钟跟踪指令，并观察总线上正在发生的事情。

  - [Sinclair ZX81 lives on!](https://baltazarstudios.com/sinclair-zx81-lives-on/)

    > I got hold of an old Sinclair ZX81 in apparently good and working condition!

    > ZX81 was the first personal computer I owned. I was 13 years old. At that time, I copied its 8K ROM, byte by byte, into a notebook, and hand-disassembled it (a consequence of which I still suffer from: I still remember some Z80 opcodes), but I never opened it. Finally, now I can do what I missed 🙂

    > This post shows how it generates TV images and how its Z80 CPU boots. I instrumented it and captured scope and logic analyzer images as it was powering on.

    > What a beauty!

    > 辛克莱ZX81活了下来! : 我得到了一个旧辛克莱ZX81显然良好的工作条件! ZX81是我拥有的第一台个人电脑。那时我13岁。当时，我把它的8K ROM一个字节一个字节地复制到一个笔记本上，然后手工拆解它(我至今还在承受这个后果:我仍然记得一些Z80操作码)，但我从来没有打开过它。终于，现在我可以做我错过了🙂 这篇文章展示了它如何生成电视图像以及它的Z80 CPU如何启动。我对它进行了检测，并在它启动时捕获了范围和逻辑分析仪图像。真是个美人! 

      <p><img height="99px" src="https://foruda.gitee.com/images/1673926311572618533/6033d795_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1673926375262895723/e1dac070_5631341.jpeg"> <img height="99px" src="https://foruda.gitee.com/images/1673926390171355007/058d6a5d_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1673926419642871130/67976acf_5631341.jpeg"> <img height="99px" src="https://foruda.gitee.com/images/1673926450240055240/51707879_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1673926574098649670/be7a375a_5631341.jpeg"> <img height="99px" src="https://foruda.gitee.com/images/1673926604006813630/426b714e_5631341.jpeg"> <img height="99px" src="https://foruda.gitee.com/images/1673926614714350829/d624ef9b_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1673926620240776967/9477f52a_5631341.png"></p>

- [SOFTWARE](https://baltazarstudios.com/category/software/)
- [Z80](https://baltazarstudios.com/category/z80/)

  - [Z80 Explorer](https://baltazarstudios.com/z80explorer/) ([13](https://baltazarstudios.com/z80explorer/#comments))

    > Z80 Explorer is a Zilog Z80 netlist-level simulator capable of running Z80 machine code and also an educational tool with features that help reverse engineer and understand this chip better.

      ... [continue reading](https://baltazarstudios.com/z80explorer/#more-1568) | July 11, 2020 | [Retro](https://baltazarstudios.com/category/retro/), [Software](https://baltazarstudios.com/category/software/), [Z80](https://baltazarstudios.com/category/z80/) | [reverse-engineer](https://baltazarstudios.com/tag/reverse-engineer/), [Z80](https://baltazarstudios.com/tag/z80/), [Zilog](https://baltazarstudios.com/tag/zilog/) | [13 Comments](https://baltazarstudios.com/z80explorer/#comments)

# [笔记本电脑就像1979年的16核Z80在FPGA上](https://hackaday.com/2019/12/10/laptop-like-its-1979-with-a-16-core-z80-on-an-fpga/)

38岁的评论由: 丹·马洛尼 2019年12月10日    

![输入图片说明](https://foruda.gitee.com/images/1673892923219776515/37d50634_5631341.png "屏幕截图")

当生活给你一个极其昂贵和强大的FPGA开发板时，你的第一反应可能不是用它来构建一个16核Z80笔记本电脑。如果不是，也许你应该检查一下你的优先级，因为这就是克里斯·芬顿所做的，结果是非常不切实际的“ZedRipper”。

我们的第一印象是，我们必须开始在一个更好的实验室课堂上闲逛，因为[Chris]得到了这个6000美元的FPGA板，作为实验室清理的结果;我们得到的最好的结果是一些旧的Cat-5电缆和一些电源条。Stratix FPGA构成了设计的核心，周围是用于10.1″VGA显示器和键盘的几块分线板，键盘是从旧PS/2中回收的。在FPGA中运行的16个Z80核心通过环形拓扑网络连接，[Chris]称之为“Z-Ring”。Z80核心之一，即服务器核心，运行CP/M 2.2和一个名为CP/NET的文件服务器，而其他15台机器是运行CP/NOS的客户端。一个简单的窗口管理器可以同时为服务器和任意三个客户端显示80 x 25个字符的终端会话，整个设备，包括一个LiPo电池组，可以装入一个激光切割的胶合板盒中。它是复古的，是现代的，是过度的，我们绝对喜欢它。

阅读[Chris]的构建日志让我们有心情打破我们的2019超级会议徽章，并尝试旋转我们自己的Z80。如果你决定破解会议徽章的FPGA-est，你可能想看看[Sprite_TM]对此有什么看法。毕竟，这是他设计的。你肯定会想看看我们在Supercon上看到的一些很棒的徽章黑客。

谢谢[yNos]的提示。

发布在FPGA，逆向计算
标签客户端，CP/M, fpga, netowrk, retro，服务器，turbo pascal, verilog, z80

### LAPTOP LIKE IT’S 1979 WITH A 16-CORE Z80 ON AN FPGA
 38 Commentsby: Dan Maloney
December 10, 2019    

When life hands you a ridiculously expensive and massively powerful FPGA dev board, your first reaction may not be to build a 16-core Z80 laptop with it. If it’s not, perhaps you should examine your priorities, because that’s what [Chris Fenton] did, with the result being the wonderfully impractical “ZedRipper.”

Our first impression is that we’ve got to start hanging around a better class of lab, because [Chris] came by this $6000 FPGA board as the result of a lab cleanout; the best we ever scored was a few old Cat-5 cables and some power strips. The Stratix FPGA formed the heart of the design, surrounded by a few breakout boards for the 10.1″ VGA display and the keyboard, which was salvaged from an old PS/2. The 16 Z80 cores running in the FPGA are connected by a ring-topology network, which [Chris] dubs the “Z-Ring”. One of the Z80 cores, the server core, runs CP/M 2.2 and a file server called CP/NET, while the other fifteen machines are clients that run CP/NOS. A simple window manager shows 80 x 25 character terminal sessions for the server and any three of the clients at once, and the whole thing, including a LiPo battery pack, fits into a laser-cut plywood case. It’s retro, it’s modern, it’s overkill, and we absolutely love it.

Reading over [Chris]’s build log puts us in the mood to break out our 2019 Superconference badge and try spinning up a Z80 of our own. If you decide to hack the FPGA-est of conference badges, you might want to check out what [Sprite_TM] has to say about it. After all, he designed it. And you’ll certainly want to look at some of the awesome badge hacks we saw at Supercon.

Thanks to [yNos] for the tip.

Posted in FPGA, Retrocomputing
Tagged client, CP/M, fpga, netowrk, retro, server, turbo pascal, verilog, z80

# [A-Z80 CPU](https://opencores.org/projects/a-z80)

- 概述
- 新闻
- 下载
- Bugtracker

### 细节

- 名称: a-z80
- 创建: 2014年12月12日
- 更新: 2020年9月10日
- SVN更新: 2020年7月12日
- SVN: 浏览
- 最新版本: 下载(可能要花点时间才能开始……)
- 统计: 视图
- 错误: 4 报告/ 2 解决了

### 其他项目属性

- 类别: 处理器
- 语言: Verilog
- 发展现状: 稳定的
- 额外的信息: 设计完成的， FPGA验证， 规范做
- 叉骨兼容: 没有
- 叉骨版: N/A
- 许可: LGPL

### 描述

 **更新:在纯Verilog中重写，CPU现在可以在Altera和Xilinx设备上使用!** 

a -Z80是古老的Zilog Z80处理器的概念实现，旨在合成和运行在现代FPGA设备上。它与现有的(主要是Verilog) Z80实现的不同之处在于，它是通过原理图和低层门从头开始设计的。这是对Z80各个层次的研究和繁琐的逆向工程的结果，包括一个模具的微缩照片。

[![A-Z80 top level](https://foruda.gitee.com/images/1673892465871730380/c6f58088_5631341.png "屏幕截图")](https://foruda.gitee.com/images/1673892308246065386/225c79d9_5631341.png)

项目包括一个完全工作的辛克莱ZX频谱实现基于这个CPU。[BaltazarStudios](https://baltazarstudios.com/) 对此进行了更详细的描述。

### 特性

- 循环和总线准确，包括nWAIT和nBUSRQ的正确行为
- 所有已记录和未记录的操作码、标志和寄存器，包括R、WZ
- 根据实际的体系结构模型，深入到某些模块的各个门和寄存器
- 通过ZEXDOC和ZEXLL(除了IX,IY的古怪OTIR/LDIR)
- BIT n的正确行为，(HL)暴露WZ
- 所有中断模式(IM0,IM1,IM2)
- 慢模式fMax是18mhz，可以运行更快时，优化

在A-Z80基于ZX频谱实现上播放Mainc Miner:

### 文档

快速入门文档在这里:[视图](https://github.com/gdevic/A-Z80/tree/master/docs)

完整的用户指南在这里:[查看](https://github.com/gdevic/A-Z80/blob/master/docs/A-Z80_UsersGuide.pdf)

此外，项目文件包含许多自述文件，以帮助您理解，重新创建它和/或将a - z80添加到您自己的项目。

### Testbench
### RTL模拟

使用ModelSim模拟设计。

- 每个模块包含一个ModelSim项目
- 包含单独的SystemVerilog测试文件
- 测试wave (*.do)文件以快速设置视图

框架还围绕Fuse测试(低级Z80 CPU)开发，这些测试在ModelSim上运行每个Z80指令，并自动与预期的Fuse测试结果文件进行比较。不匹配会被标记。

有一个“快速”的心智测试，以及一个更长的综合测试。

### 顶级模拟

ZMAC组件用于生成Z80程序测试片段，然后在仿真和实际FPGA硬件上运行。结果文件应该匹配。此级别的测试将UART添加到ModelSim和FPGA实现中，以便可以运行测试并比较输出。

测试包括:

- 测试各种复杂指令，如DAA, NEG
- 经典的“Hello, World”应用程序
- 中断行为测试
- …并在*中嵌入了更多测试。Asm测试文件

### 实现

几个完整和工作FPGA设计说明实现和测试A-Z80在Altera和Xilinx设备上:

- 基本计算机使用键盘和UART运行Z80测试
- 完整实现辛克莱ZX谱

在基本计算机实现上运行ZEXDOC和ZEXALL测试:

![输入图片说明](https://foruda.gitee.com/images/1673893641882855252/9728a5ee_5631341.png "屏幕截图")

### 状态

本设计已完全完成、测试和工作。

基于Cyclone II的Altera DE1板实现使用了大约11%的LE。

![Compiled size](https://foruda.gitee.com/images/1673893576854136759/2eedf7c3_5631341.png "屏幕截图")

基于Spartan-6的Nexys3板使用了大约19%的切片。

![Compiled size](https://foruda.gitee.com/images/1673893589714280925/1843a728_5631341.png "屏幕截图")

这个实现使用了免费的Altera和Xilinx工具(Quartus II v13.0.1 Web Edition和Xilinx ISE Webpack)。它还使用Python 3.5构建一些组件和测试。虽然基于Altera和Xilinx设备，但该项目可以与其他供应商一起使用，因为(特定于quartus的)原理图文件被预编译为通用的Verilog文件。