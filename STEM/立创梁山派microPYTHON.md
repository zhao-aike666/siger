- [梁山派](https://lckfb.com/docs/lckfb_lspi/#/) [10分钟快速入门](https://lckfb.com/docs/lckfb_lspi/#/10分钟快速入门) [在线编译](https://lckfb.com/editor#id=8872374daf9d40c78086bd347fe23880)
- 立创·梁山派 [入门学习引导](https://dri8c0qdfb.feishu.cn/wiki/wikcn3yhk3CjNBK9mESfbXlrpSC)
- 立创·梁山派 [交通灯扩展板](https://lckfb.com/project/detail/lspi_tgx?param=baseInfo)
- 立创·梁山派 [Arduino开发](https://dri8c0qdfb.feishu.cn/docx/IX9jd4ku3oq9HPxHqeFcDxEcnyg)
- [梁山派开发板 MicroPython](https://dri8c0qdfb.feishu.cn/docx/FBa2djfSdopfOoxolVucpYi5nDv)

<p><img width="706px" src="https://foruda.gitee.com/images/1679591685057548434/d5466df0_5631341.jpeg" title="629梁山派.jpg"></p>

> 梁山派的探秘（考古）终于有了新的深度，当我写下 “全栈开发能力” 的时刻，也是我对梁山派的期许（同学们可以认真地研究下）加倍的时刻。选择吴工肝代码的视频作为封面，是对他的点赞，相信一定又是一个个连续奋战的夜晚，尽管版本号只有 0.1，我相信随着更多伙伴加入，小数点一定会快速跳变到1的后面的。期待这个时刻的到来。此时此刻，我已经构想出了，新扩展板的样貌啦，这几天刷屏的 “胸有成竹” 让我放弃了这个词，永远在路上是开源社区的标准配置，一起加油。 :pray:

# [10分钟快速入门](https://lckfb.com/docs/lckfb_lspi/#/)

<table>
<thead>
<tr>
<th>序列</th>
<th>教程</th>
<th>在线案例</th>
</tr>
</thead>
<tbody><tr>
<td>00</td>
<td><a href="https://lckfb.com/docs/lckfb_lspi/#/10分钟快速入门          ">10分钟快速入门        </a></td>
<td><a href="https://lckfb.com/editor#id=8872374daf9d40c78086bd347fe23880">10分钟快速入门案例</a></td>
</tr>
<tr>
<td>01</td>
<td><a href="https://lckfb.com/docs/lckfb_lspi/#/梁山Pi资料下载          ">梁山Pi资料下载       </a></td>
<td></td>
</tr>
<tr>
<td>02</td>
<td><a href="https://lckfb.com/docs/lckfb_lspi/#/004寄存器和库函数介绍   ">寄存器和库函数介绍    </a></td>
<td></td>
</tr>
<tr>
<td>03</td>
<td><a href="https://lckfb.com/docs/lckfb_lspi/#/005LED灯原理介绍        ">LED灯原理介绍        </a></td>
<td></td>
</tr>
<tr>
<td>04</td>
<td><a href="https://lckfb.com/docs/lckfb_lspi/#/006寄存器点亮LED        ">寄存器点亮LED         </a></td>
<td><a href="https://lckfb.com/editor#id=dae9045eba4b40ea81fe4db89f02ae7b">寄存器点亮LED案例</a></td>
</tr>
<tr>
<td>05</td>
<td><a href="https://lckfb.com/docs/lckfb_lspi/#/007库函数点亮LED        ">库函数点亮LED         </a></td>
<td><a href="https://lckfb.com/editor#id=d535bfa47d88466e9272e5e611c75a70">库函数点亮LED案例</a></td>
</tr>
<tr>
<td>06</td>
<td><a href="https://lckfb.com/docs/lckfb_lspi/#/008滴答定时器           ">滴答定时器           </a></td>
<td><a href="https://lckfb.com/editor#id=cc9329993f4e426da105b696d83f796f">滴答定时器案例</a></td>
</tr>
<tr>
<td>07</td>
<td><a href="https://lckfb.com/docs/lckfb_lspi/#/009位带操作             ">位带操作             </a></td>
<td><a href="https://lckfb.com/editor#id=71d5d1b0c5c74bab833aaf10a6016c92">位带操作案例</a></td>
</tr>
<tr>
<td>09</td>
<td><a href="https://lckfb.com/docs/lckfb_lspi/#/010串口通信原理介绍     ">串口通信原理介绍     </a></td>
<td></td>
</tr>
<tr>
<td>10</td>
<td><a href="https://lckfb.com/docs/lckfb_lspi/#/011串口打印信息         ">串口打印信息         </a></td>
<td><a href="https://lckfb.com/editor#id=4ea79ba319984cd493a5b485c64b5e40">串口打印信息案例</a></td>
</tr>
<tr>
<td>11</td>
<td><a href="https://lckfb.com/docs/lckfb_lspi/#/012独立按键原理介绍     ">独立按键原理介绍     </a></td>
<td></td>
</tr>
<tr>
<td>12</td>
<td><a href="https://lckfb.com/docs/lckfb_lspi/#/013按键点灯             ">按键点灯             </a></td>
<td><a href="https://lckfb.com/editor#id=a660306ec39d42c3829220a480bd6148">按键点灯案例</a></td>
</tr>
<tr>
<td>13</td>
<td><a href="https://lckfb.com/docs/lckfb_lspi/#/014中断原理介绍         ">中断原理介绍            </a></td>
<td></td>
</tr>
<tr>
<td>14</td>
<td><a href="https://lckfb.com/docs/lckfb_lspi/#/015外部中断按键点灯     ">外部中断按键点灯     </a></td>
<td><a href="https://lckfb.com/editor#id=ba3f6c7c204043e5bf247d7e85f7159d">外部中断按键点灯案例</a></td>
</tr>
<tr>
<td>15</td>
<td><a href="https://lckfb.com/docs/lckfb_lspi/#/016定时器原理介绍       ">定时器原理介绍       </a></td>
<td></td>
</tr>
<tr>
<td>16</td>
<td><a href="https://lckfb.com/docs/lckfb_lspi/#/017定时器灯闪烁         ">定时器灯闪烁         </a></td>
<td><a href="https://lckfb.com/editor#id=ddf8d6f2c99249af83b5e3c8372797c8">定时器灯闪烁案例</a></td>
</tr>
<tr>
<td>17</td>
<td><a href="https://lckfb.com/docs/lckfb_lspi/#/018PWM原理介绍          ">PWM原理介绍          </a></td>
<td></td>
</tr>
<tr>
<td>18</td>
<td><a href="https://lckfb.com/docs/lckfb_lspi/#/019PWM呼吸灯            ">PWM呼吸灯            </a></td>
<td><a href="https://lckfb.com/editor#id=09444005f46345429fb17a2936d7103d">PWM呼吸灯案例</a></td>
</tr>
<tr>
<td>19</td>
<td><a href="https://lckfb.com/docs/lckfb_lspi/#/020DMA原理介绍          ">DMA原理介绍          </a></td>
<td></td>
</tr>
<tr>
<td>20</td>
<td><a href="https://lckfb.com/docs/lckfb_lspi/#/021串口DMA接收和中断接收">串口DMA接收和中断接收</a></td>
<td><a href="https://lckfb.com/editor#id=78b6f1bb2edf4d238b6356d64aa538ef">在线案例</a></td>
</tr>
<tr>
<td>21</td>
<td><a href="https://lckfb.com/docs/lckfb_lspi/#/002开发环境介绍         ">MDK开发环境          </a></td>
<td></td>
</tr>
<tr>
<td>22</td>
<td><a href="https://lckfb.com/docs/lckfb_lspi/#/003工程模板创建         ">工程模板创建         </a></td>
<td></td>
</tr>
<tr>
<td>23</td>
<td><a href="https://lckfb.com/docs/lckfb_lspi/#/007VSCode安装           ">VSCode安装           </a></td>
<td></td>
</tr>
</tbody></table>

# [立创·梁山派入门学习引导手册](https://dri8c0qdfb.feishu.cn/wiki/wikcn3yhk3CjNBK9mESfbXlrpSC)

《版本修订详情表》

| 版本记录 | 编写人 | 修改日期 | 版本说明 | 
|---|---|---|---|
| 1.0 | 卓俊鸿 | 2022-10-19 | 立创·梁山派入门学习引导手册初版 |

## 1.0.前言

这份引导手册主要是为了帮助用户快速了解我们立创·梁山派开发板,以及给大家后续学习起到一个引导的作用。如果你在学习立创·梁山派的过程中遇到了困难或者解决不了的问题，可以到我们的立创开发板官方网站或是立创开源平台进行提问，我们的技术人员将会给你提供帮助。最后，如果你好的建议，欢迎你到立创开发板官方网站进行留言。

## 1.1.立创·梁山派开发板入门资料获取

- 点击立创开发板官方网址： https://lckfb.com
- 点击立创·梁山派开发板页面，点击了解详情，等待页面跳转；

![输入图片说明](https://foruda.gitee.com/images/1679592970273521752/4d6d6052_5631341.png "屏幕截图")

> 图1 立创·梁山派开放板页面图

- 完成跳转后点击教程资料，在左侧找到资料下载，点击下载链接即可进行下载；
- 百度网盘资料：https://pan.baidu.com/s/1O_2eZLAs25Kt0vKwWS6D8w?pwd=h2jd 提取码：h2jd
- 同时我们的教程资料也支持在线查阅；

![输入图片说明](https://foruda.gitee.com/images/1679592994538086099/109b63cb_5631341.png "屏幕截图")

> 图2 立创·梁山派开放板资料

## 1.2.立创·梁山派百度网盘资料文件夹说明

下载完资料包我们可以查看到以下目录结构，下面对各个文件夹进行详细的介绍。

![输入图片说明](https://foruda.gitee.com/images/1679593028435667510/815472b5_5631341.png "屏幕截图")

> 图3 立创·梁山派资源包

1.  **01官方资料文件夹** ：这个文件夹主要存放了GD官方提供的一些资料。
2.  **02开发工具文件夹** ：存放我们学习过程中可能使用到的各种软件安装包，包括了Keil，VScode，串口调试助手等。
3.  **03硬件资料文件夹** ：存放了一些硬件相关的资料，包括了各元器件的用户手册，立创·梁山派开发板的原理图，以及立创·梁山派的工程文件（JLCEDA专业版格式），该工程文件可以使用嘉立创EDA专业版导入后打开查看。
4.  **04软件资料文件夹** ：存放我们视频教程所讲解的各种外设代码例程，有视频版本和注释版本。
5.  **05配套参考文档文件夹** ：存放《立创·梁山派开发板学习指南》。
6.  **06配套ppt文件夹** ：存放我们配套教程视频中的所有PPT文件。
7.  **07教程视频文件夹** ：存放我们的配套的视频教程。
8.  **扩展驱动demo文件夹** ：存放我们基于立创·梁山派的扩展板的一些外设的驱动代码。

## 1.3.立创·梁山派硬件资源介绍

### 1.3.1.立创·梁山派开发板硬件资源

表1 立创·梁山派开发板硬件资源详情表

| 资源 | 分配 |
|---|---|
| 微控制器 | GD32F450ZGT6 |
| 按键 | 3个，RESET，BOOT0，KEY_UP | 
| LED | 4个 | 
| 电源指示灯 | 1个 |
| SWD下载接口 | 1个 |
| USB接口 | 1个 |
| 屏幕接口 | 2个，RGB565和8080并口 |
| SD卡槽 | 1个 | 
| P1引出IO口 | 40PIN |
| P2引出IO口 | 40PIN |
| FLASH | 1个，W25Q128 |
| SDRAM | 1个，W9825G6KH-6I |

### 1.3.2.硬件资源分布图

![输入图片说明](https://foruda.gitee.com/images/1679593182548455169/65e5a38d_5631341.png "屏幕截图")

> 图4 立创·梁山派硬件资源分布图

## 1.4.你接下来要学习的内容

### 1.4.1. 10分钟体验GD32流水灯

选择合适的开发工具，先完成一个流水灯的实验，提高自己的学习兴趣。俗话说，工欲善其事，必先利其器，所以为了让大家可以快速上手立创·梁山派开发板，我们为大家准备了在线编译网站，无需安装和配置各种环境，只需在线一键下载即可将我们的程序烧录到我们的开发板，从而看到我们想要看到的现象，这给新手朋友提供了极大的便利性，所以这里强烈推荐新手使用立创·梁山派开发板的在线编译器。此外，立创·梁山派开发板配套教程不仅适用新手入门的朋友，同样也适用于有单片机基础的同学，因为我们有基于立创·梁山派开发板的扩展板独立项目开发，通过项目实战帮助大家提高软硬件能力。

这里教大家一分钟快速点灯验证开发板，以及如何使用使用在线编译器烧写程序。

- （1）电脑配置：谷歌浏览器或微软自带Microsoft Edge浏览器。windows10不用装DAPLink驱动，windows7需要装DAPLink驱动。
- （2）硬件连接：将硬件按照以下图片进行连接。

![输入图片说明](https://foruda.gitee.com/images/1679593226749629787/05c5ac7c_5631341.png "屏幕截图")

> 图5 硬件连接示意图

- （3）点击链接https://lckfb.com/，editor/#id=8872374daf9d40c78086bd347fe23880跳转到浏览器，打开在线编译器工程，可以看到以下界面。

![输入图片说明](https://foruda.gitee.com/images/1679593245158661686/b33cbcf6_5631341.png "屏幕截图")

> 图6 在线编辑器界面图

- （4）按照下图步骤，一键下载，看现象：LED2反复闪烁。

![输入图片说明](https://foruda.gitee.com/images/1679593262669790453/eeeeafeb_5631341.png "屏幕截图")

> 图7 在线编辑器使用步骤图

- （5）进入ISP模式的操作步骤如下图。

![输入图片说明](https://foruda.gitee.com/images/1679593282433320307/7f09eae4_5631341.png "屏幕截图")

> 图8 进入ISP步骤图

- （6）[在线点灯视频教程链接](https://lckfb.com/docs/lckfb_lspi/#/10%E5%88%86%E9%92%9F%E5%BF%AB%E9%80%9F%E5%85%A5%E9%97%A8)
- （7）实验效果图：LED2闪缩，开发板正常。到此我们的板子验证完毕，你也成功点亮了属于你的第一个LED灯，赋予了立创·梁山派最初的心跳。

![输入图片说明](https://foruda.gitee.com/images/1679593311130007294/a3f0679d_5631341.png "屏幕截图")

> 图9 实验效果图

### 1.4.2.学习GD32基础入门篇教程

通过上述的流水灯实验，相信你已经被它的魅力所吸引。那么，你知道它的具体实现原理吗？不知道并没有关系，因为通过这个阶段的学习，你将彻底明白它的原理。所以在这个阶段，你将会学习到很多关于单片机底层原理的知识。

1. LED灯的原理：通过LED灯的原理介绍，以及原理图，再结合代码的理解，你将掌握流水灯的原理。
2. 寄存器和库函数：了解常用寄存器，通过对不同寄存器的操作，达到我们想要的效果。库函数则是将寄存器封装成一个个的函数接口给用户调用，使代码的可读性更高，也提高了我们的开发效率。
3. 定时器的工作原理：了解什么是定时器，以及使用定时器的步骤。
4. 串口通信的使用：了解什么是串口通信，串行通信的工作模式，以及串口通信协议。
5. 中断的原理与使用：了解什么是中断，中断的触发，以及中断优先级，如何使用外部中断。
6. PWM的使用：了解什么是PWM，以及如何配置PWM，PWM的应用完成呼吸灯实验。
7. DMA的使用：了解DMA的工作原理，以及DMA的传输模式。
8. 此外我们还可以学习其他的一些外设模块以及通信协议的使用，比如OLED屏，摄像头，IIC协议，SPI协议，CAN总线等。

### 1.4.3.学习GD32高级应用项目实战教程

当你掌握了基本的单片机知识后，就可以开始做一个简单的项目作为实践了。我们在立创开发板官网开放了交通灯和智能小车等案例供大家学习参考。我们后续也会带领大家学习GD32高级应用项目实战，从硬件原理到软件实现，再到实物演示，一步步给大家进行讲解。

1. 交通灯扩展板项目案例

- 立创·梁山派-交通灯扩展板项目链接：https://lckfb.com/project/detail/lspi_tgx?param=baseInfo
- 交通灯扩展板项目百度网盘资料：https://pan.baidu.com/s/11SZO-sYdlUXsM0gbNbMdSA?pwd=sm2p 提取码: sm2p

![输入图片说明](https://foruda.gitee.com/images/1679593369780745590/9453d4d6_5631341.png "屏幕截图")

> 图10 交通灯实物图

2. 智能小车扩展板项目案例

- 立创·梁山派-智能小车扩展板项目链接：https://lckfb.com/project/detail/lspi_car?param=baseInfo
- 智能小车扩展板项目百度网盘资料：https://pan.baidu.com/s/1eltn5Oivw5aFqYyT9b_Zaw?pwd=znxc 提取码：znxc

![输入图片说明](https://foruda.gitee.com/images/1679593389114324807/d3403682_5631341.png "屏幕截图")

> 图11 智能小车实物图

## 1.5.项目式学习

### 1.5.1.什么是项目式学习？

1. 定义：一站式全方位学习方式，从硬件电路设计，PCB设计，元器件选型以及采购，软件设计，软硬件调试，到达到项目完结标准。
2. 项目式学习的好处？

- （1）传统学习停留在理论上，学习成效较差；项目式学习通过理论实践结合，让学习更高效有趣，以及学习成果更明显；
- （2）全方位学习，让基础更扎实，让知识更系统。

3. 我们能提供什么？

- （1）资深工程师录制项目式学习视频教程，让知识更贴近实际工作应用；
- （2）立创EDA编辑器，让新手也可以快速入门设计电路板；
- （3）立创商城元器件一键下单，方便快捷；
- （4）官方答疑群，与广大学习者一起探讨学习进步，让学习不孤单。

### 1.5.2.学完后的你可以做什么？

相信看到这里的你已经迫不及待想知道自己学完上述内容后，自己究竟能做哪些有意思的项目呢？这里给大家推荐几个有趣的小项目进行参考。

- （1）游戏机：你可以基于梁山Pi用嘉立创EDA画一块扩展板，通过简单的LED点阵屏做一个简单的贪食蛇游戏。
- （2）智能小车：你可以使用循迹模块做一辆循迹小车，用蓝牙模块做一辆遥控小车，其它功能再根据你的需求添加。
- （3）还有更多有趣项目，等你去挑战。

## 1.6.一些学习上的建议

这里给大家分享一些学习上的建议以及一些常见的错误。

- （1）制定学习目标，长期坚持。

目标会让我们的学习变得明确和高效，合理制定学习目标是我们学习的第一步，也是成功的第一步。

- （2）实践出真知，多动手做实验。

光听不做假把式，很多东西你听的时候以为自己懂了，而实际操作起来还是各种问题，所以一定要多练，多动手敲代码，工科的东西光靠听和看是很难学会的。当你练多了，很多困扰你的东西自然就会消失。

- （3）学会查阅资料，不要闭门造车。

这里的查阅资料包括看数据手册以及元器件说明书，还有电路原理图。开发的过程尽量自己去查阅数据手册相关的内容，包括引脚的模式配置，复用功能，重映射等，以及时钟的分配。看元器件的说明书时注意其电气参数，额定电压等参数。电路原理图的设计会多都是可以在元器件数据手册说明书中查阅到。所以说学会查阅资料也是一项必备的技能。

- （4）寄存器还是库函数？

新手常见的错误就是只会用库函数，而不懂寄存器，或者说是害怕去查看寄存器。这里我给大家的建议就是平时开发使用库函数，但也要查阅一下相关寄存器，因为这对我们理解它的实现原理是有帮助的，当你深入学习时，对嵌入式内核的理解很有帮助。

- （5）遇到问题多调试，多思考。

开发的过程遇到问题是在所难免的，当我们遇到问题时一定要多思考，多调试自己的代码，不要急于寻找答案。这里推荐大家使用串口打印的方式进行调试，也可以使用DEBUG单步调试。

- （6）学会做总结，养成良好的编程习惯。

养成良好的编程习惯，代码勤写注释，课后善于总结，可以把遇到的问题以及解决方案汇总在一个文档里，积累学习经历。

最后，让我们一起出发，去探索立创·梁山派的奥秘和奇妙之处吧，祝你接下来的日子学习愉快。

## 1.7.关于我们

- 立创开发板官网：https://lckfb.com
- 嘉立创开发板在线编译器网站：https://lckfb.com/editor
- 嘉立创EDA官网：https://lceda.cn
- 嘉立创官网：https://www.jlc.com
- 立创商城官网：https://www.szlcsc.com
- 立创开源平台官网：https://oshwhub.com
- 立创EDA哔哩哔哩账号链接：https://space.bilibili.com/430536057
- 联系电话：152-1116-3466
- 微信：

![输入图片说明](https://foruda.gitee.com/images/1679593522325603242/5560ac36_5631341.png "屏幕截图")

# [立创·梁山派Arduino开发](https://dri8c0qdfb.feishu.cn/docx/IX9jd4ku3oq9HPxHqeFcDxEcnyg)

### 配置立创·梁山派支持

把下面网址填入附加开发板管理网址
https://github.com/stm32duino/BoardManagerFiles/raw/main/package_stmicroelectronics_index.json
文件-》首选项

![输入图片说明](https://foruda.gitee.com/images/1679593650454476757/ce06f2c4_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1679593656863670092/be23f924_5631341.png "屏幕截图")

打开开发板管理器面板，工具-》开发板-》开发板管理器

![输入图片说明](https://foruda.gitee.com/images/1679593665657926393/208e9d53_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1679593670414062396/80b4d20b_5631341.png "屏幕截图")

选择stm32 MCU based boards 进行安装，这里安装可能会等一段时间；

![输入图片说明](https://foruda.gitee.com/images/1679593680489412923/86b661f4_5631341.png "屏幕截图")

### 选择开发板

这里选STM32F4，因为GD32F4对应的兼容STM32F4

![输入图片说明](https://foruda.gitee.com/images/1679593697612068244/94d61b59_5631341.png "屏幕截图")

### 选择芯片型号

这里选F429ZGTx，因为GD32F450/GD32F470ZGT6,对应兼容STM32F429ZGT6

![输入图片说明](https://foruda.gitee.com/images/1679593714988732999/c93a6564_5631341.png "屏幕截图")

### 代码测试

代码效果，KEY_UP按键按下LED1点亮，松开灯关闭


```
int buttonState = 0;         // variable for reading the pushbutton status

void setup() {
  // initialize the LED pin as an output:
  pinMode(PE3, OUTPUT);
  // initialize the pushbutton pin as an input:
  pinMode(PA0, INPUT);
}

void loop() {
  // read the state of the pushbutton value:
  buttonState = digitalRead(PA0);

  // check if the pushbutton is pressed. If it is, the buttonState is HIGH:
  if (buttonState == HIGH) {
    // turn LED on:
    digitalWrite(PE3, HIGH);
  } else {
    // turn LED off:
    digitalWrite(PE3, LOW);
  }
}
```

新建

![输入图片说明](https://foruda.gitee.com/images/1679593744925280821/8eafea07_5631341.png "屏幕截图")

把上面代码粘贴进去

![输入图片说明](https://foruda.gitee.com/images/1679593754161269956/e838b8c1_5631341.png "屏幕截图")

ctrl+s保存，然后在ctrl+alt+s进行编译并导出已编译的二进制文件

![输入图片说明](https://foruda.gitee.com/images/1679593764481220132/1c598023_5631341.png "屏幕截图")

### 烧入

编译完成没有错误，显示项目文件夹

![输入图片说明](https://foruda.gitee.com/images/1679593777875284185/bfe736b0_5631341.png "屏幕截图")

这时候目录下就已经生成了我们的可执行文件bin

![输入图片说明](https://foruda.gitee.com/images/1679593787191737385/b1291a45_5631341.png "屏幕截图")

使用DFU把bin烧入开发板，DFU烧入代码请查看[梁山派使用DFU下载说明](https://dri8c0qdfb.feishu.cn/docx/KvOLdiwY1oe69WxCZ8ncXDGOnMh?from=from_copylink)

### 实验效果：

![输入图片说明](https://foruda.gitee.com/images/1679593867249943832/83657338_5631341.png "屏幕截图")

# [梁山派开发板 MicroPython](https://dri8c0qdfb.feishu.cn/docx/FBa2djfSdopfOoxolVucpYi5nDv)

## 烧入MicroPython并测试

### 镜像

[LCKFB_LSPI_MicroPython_V0.1.hex](https://dri8c0qdfb.feishu.cn/docx/FBa2djfSdopfOoxolVucpYi5nDv)

### 使用DFU方式下载

参考下载说明：[梁山派使用DFU下载说明](https://dri8c0qdfb.feishu.cn/docx/KvOLdiwY1oe69WxCZ8ncXDGOnMh?from=from_copylink) 

open打开LCKFB_LSPI_MicroPython_V0.1.hex所在目录

![输入图片说明](https://foruda.gitee.com/images/1679594013642485824/f3154233_5631341.png "屏幕截图")

 **注意：DFU下载时候把数据连接到了梁山派开发板板载USB上，测试的时候需要把数据线连接到DAPlink，因为我们是通过串口进行通讯** 

## 测试

### Micropython学习文档：

官方：http://docs.micropython.org/en/latest/
https://docs.singtown.com/micropython/zh/latest/moxingstm32f4/moxingstm32f4/tutorial/pin.html

### 立创开发板串口在线工具：（使用谷歌打开）

https://lckfb.com/tools/serial_assistant/

![输入图片说明](https://foruda.gitee.com/images/1679594056885358890/2c4e5541_5631341.png "屏幕截图")

首先输入help()让我们来看一下micropython简单使用，这里找到点灯的方法

![输入图片说明](https://foruda.gitee.com/images/1679594068644466461/8f33d406_5631341.png "屏幕截图")

### 点亮与关闭led1

//tab键可以补全


```
>>> import pyb
>>> pyb.LED(1).on()
>>> pyb.LED(1).off()
```

### 串口输出


```
>>> print("abcde")
abcde
```


### GPIO控制LED1


```
>>> from pyb import Pin
>>> myled = Pin("E3",Pin.OUT_PP)
>>> myled.value(1)
>>> myled.value(0)
```


### GPIO控制LED1闪烁


```
>>> from pyb import Pin
>>> myled = Pin("E3",Pin.OUT_PP)
>>> while True:
...     myled.value(1)
...     pyb.delay(1000)
...     myled.value(0)
...     pyb.delay(1000)
```

![输入图片说明](https://foruda.gitee.com/images/1679594133793273796/0ba0f267_5631341.png "屏幕截图")

更多玩法大家参考官方手册。。。。。。。

### 演示视频

![输入图片说明](https://foruda.gitee.com/images/1679594174552522227/b150e2f5_5631341.png "屏幕截图")

## 源码编译

### 编译工作环境


```
Distributor ID:        Ubuntu
Description:        Ubuntu 16.04.7 LTS
Release:        16.04
Codename:        xenial
```


### 安装编译环境


```
sudo apt-get update
sudo apt-get install git build-essential
sudo apt-get install gcc
sudo apt-get install gcc-arm-none-eabi
//查看是否安装成功
kerson@ubuntu:~/jlc/micropython/mpy-cross$ ar //tab
ar                        arm-none-eabi-g++         arm-none-eabi-ld.bfd
arch                      arm-none-eabi-gcc         arm-none-eabi-nm
arecord                   arm-none-eabi-gcc-7.3.1   arm-none-eabi-objcopy
arecordmidi               arm-none-eabi-gcc-ar      arm-none-eabi-objdump
arm2hpdl                  arm-none-eabi-gcc-nm      arm-none-eabi-ranlib
arm-none-eabi-addr2line   arm-none-eabi-gcc-ranlib  arm-none-eabi-readelf
arm-none-eabi-ar          arm-none-eabi-gcov        arm-none-eabi-size
arm-none-eabi-as          arm-none-eabi-gcov-dump   arm-none-eabi-strings
arm-none-eabi-c++         arm-none-eabi-gcov-tool   arm-none-eabi-strip
arm-none-eabi-c++filt     arm-none-eabi-gdb         arp
arm-none-eabi-cpp         arm-none-eabi-gprof       arpd
arm-none-eabi-elfedit     arm-none-eabi-ld          arping
```

### 源码

把文件解压到自己的ubuntu环境里去

[LSPI_micropython.zip](https://dri8c0qdfb.feishu.cn/docx/FBa2djfSdopfOoxolVucpYi5nDv) 1.9G

### 编译

编译mpy-cross


```
kerson@ubuntu:~/jlc/micropython/mpy-cross$ make -j32
//编译成功
CC ../shared/runtime/gchelper_generic.c
LINK build/mpy-cross
   text           data            bss            dec            hex        filename
 287744            776            840         289360          46a50        build/mpy-cross
```

编译梁山派开发板


```
//这里选 BOARD=GD32F450LSPI  
kerson@ubuntu:~/jlc/micropython/ports/stm32$ make BOARD=GD32F450LSPI PYTHON=python3
//编译完成
CC build-GD32F450LSPI/pins_GD32F450LSPI.c
CC build-GD32F450LSPI/frozen_content.c
LINK build-GD32F450LSPI/firmware.elf
   text           data            bss            dec            hex        filename
 326564             16          27612         354192          56790        build-GD32F450LSPI/firmware.elf
GEN build-GD32F450LSPI/firmware0.bin
GEN build-GD32F450LSPI/firmware1.bin
GEN build-GD32F450LSPI/firmware.hex //这个就是最终生成的目标文件，用DFU下载
GEN build-GD32F450LSPI/firmware.dfu
```

---

【笔记】[all to ARDUINO and all to python](https://gitee.com/siger99/Wifi-Oximiter-using-MAX30102-and-ESP32/issues/I6IVZS)

- all to ARDUINO

  - 树莓派 跨 ARDUINO
  - FPGA to arduino
  - any MCU 开发板 to arduino


- all to python

  - pygames
  - pynq
  - py porting 

- [用梁山派做一个ARDUINO的配套器材，一个扩展板全部实验](https://gitee.com/siger99/Wifi-Oximiter-using-MAX30102-and-ESP32/issues/I6IVZS#note_17010583_link)

- [切割板 A4双面雕刻垫板 介刀板 手工模型裁切护刀板](https://detail.1688.com/offer/541535308211.html)  
  detail.1688.comdetail.1688.com|1920 × 1920 jpeg|Image may be subject to copyright.

  ![输入图片说明](https://foruda.gitee.com/images/1679594788106835405/b3224766_5631341.jpeg "未标题-4.jpg")