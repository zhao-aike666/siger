- 有些事只有开源能做 —— 《开源观止》[第1期](https://oscimg.oschina.net/public_shard/opensource-guanzhi-0531.pdf) P5 （[第2期](https://oscimg.oschina.net/public_shard/opensource-guanzhi-20220707.pdf)、[第3期](https://oscimg.oschina.net/public_shard/opensource-guanzhi-20220810.pdf)）
- [ABOUT BRUCE PERENS](https://perens.com/about-bruce-perens/) —— https://perens.com/
- ["Homeworld" will be the default theme for Debian 11 - Bits from Debian](https://bits.debian.org/2020/11/homeworld-will-be-the-default-theme-for-debian-11.html) & [Debian. Desktop wallpaper](https://en.free-wallpapers.su/img135857.htm).

<p><img width="706px" src="https://foruda.gitee.com/images/1660271656383952168/230开源观止副本.jpeg" title="230开源观止副本.jpg"></p>

“让我们好好做一期！”，一晃一年多就这么飞过，这一期变成了200期，交在了一君老师面前。SIGer 的首页 2021年的刊例头条，就是开源治理的专题。我至今还怀念着 GNU99 的海报，说是致敬 RMS 的，也没能实现。今天看到啦另一位，是因为我正在使用的树莓派 RASPBIAN 就脱胎于 Debian。而看到标题，我有话要说，我们忘记开源精神了吗？或许我们只需要把他们放逐 “神坛” 做我们自己的事情，并冠以 “开源”。

> 这是一期致敬专题，选择我擅长的封面，和一组 Debian 桌面精华，表达对这位开源领袖的敬意，也同时感谢这份奇妙的缘分，难道这是老天让我给一君老师汇报的吗？除了开源治理专题的转载，去年表达投稿意愿的时候，正好是 RISCV 新专题的一篇原创 ACM 译文。当时 RVSC 2021 还没有召开，石榴派 RV版 也还没踪影呢，更别说和 SIGer 合并 RV少年 专题。昨天我刚把 RVSC 2022 的大会议程转发朋友圈，就看到了 一君老师分享的第三期《开源观止》，编辑署名大大地一君老师，我激动不已，记得第2期邮件列表收到时，我是看过的，当时也没有冲动去考古第一期。这回是派了一君老师来监督我干活儿啦。三期共172页，可要认真学习下，先发一期给同学们预热一下，之后，大家一起学习，一起向一君老师交作业。

# 有些事只有开源能做

《开源观止》是由 OSCHINA 策划的全新的开源期刊，昨天发布了第三期，编辑之一是尊敬的一君老师，同学们应该看过我转载的他的文章，关于开源治理。这本杂志的立刊之本同样是深度，如果开源能有五千年的历史，一定可以是上下五千年。在它的序言中突出了一点，开源的大佬们大多见在，这对同学们来说，是个好事情，没准哪天，就在社区里碰到啦。在考古的过程中，第一期创刊，杂志社就干脆 “请到了一位，谈了谈最近开源世界的一些变化。” 这位是谁？来看下面的标题和介绍吧。而且本期的标题就借用了对话这位大佬的题目。

 **《对话 Bruce Perens：有些事只有开源能》** 

> 写开源相关的文章，总是绕不开 Bruce Perens。写 Debian，他是 Debian 创始人钦点的第一任 Debian 领导人 ，起着关键性作用；写布道，他是 OSI（Open Source Initiative）的发起人之一，祖师爷级别；写协议，他是 OSD （Open Source Definition）的作者——就是他写下了十条定义，来定义“开源”是什么。

## 03 BSL 很好，但有些事只有开源能做

### 故事背景：

BSL（Business Source License）是 MariaDB 创建的一种商业许可证。一些开源企业采用 BSL，以阻止云厂商在没有获得授权的情况下将其产品以商业形式提供为云服务。

同样为了防止云厂商“搭便车”，2018 年，MongoDB 创 建 SSPL （ Server Side Public License），该协议规定：如果你将产品作为服务提供给他人，则需要公开发布任何修改以及管理层的源代码。

因为不符合“OSD”，该两个许可证均未通过OSI 认证，被认为不是真正的开源，而仅是源码可见（source-available）。

### OSCHINA:

您是如何看待源码可见许可证，例如 SSPL 和 BSL 的？似乎越来越多的开源创企开始倾向于源码可见，而不是地地道道、完全符合 OSD （Open Source Definition）的开源。您认为 OSD 会有过时的那一天吗？您是如何看待这种现象的？

### Bruce Perens：

关于 BSL，我曾经帮助过 MySQL&MariaDB 的创始人 Monty Widenius。BSL 保证，采用 BSL 的软件在经过一段时间后会自动地变成完全开源的状态。而 SSPL 就没有这样的承诺了。

开源并不意味着我们的开发者就必须过着“苦行僧”样的生活。AWS（Amazon Web Services）从 MariaDB 等公司“占到了便宜”，他们既不贡献也不资助，反而通过运行和售卖相关服务从开源软件中获利。我认为，BSL 是一个很好的折中方案，但其它的那些（非开源）许可证我就没那么高的评价了，有些根本不打算做出任何让步。

一些创业公司采用了那些不能保障一定会开源的许可证，我认为这就与开源不再相关了。开源社区要比他们壮大且有力量得多！开源社区每天发布的代码数量是 MongoDB 等公司的 1000 倍。

重点在于，一些大型且高技术含量的项目是大多数公司仅凭自身力量根本无法办到的，而开源社区就能。

以音视频编解码器为例，有哪家电话制造商愿意在实验室花十亿美元做高清音频，以作为他们下一款手机的主要卖点？没有一家。而 Jean-Marc Valin 的 Speex 在做，它是开源的。（译者注：Speex 是一个开源的编解码器，以 BSD 许可发布， 是 GNU 项目的一部分。）

有哪家手机芯片制造商在研发用于 5G 系统原型的软件吗？没有。但开源的 GNU Radio 在做。（译者注：GNU Radio 是免费开源的软件开发工具套件。它提供信号运行和处理的模块，被业余爱好者、学术机构和商业机构用来研究和构建无线通信系统。）

IBM 或者其他电脑公司有做出一款像 Linux 一样被运行在多种硬件上的操作系统吗？没有任何一家公司能做到，哪怕只是靠近一点。

开源观止 2022 年 6 月

# [ABOUT BRUCE PERENS](https://perens.com/about-bruce-perens/)

Mr. Perens can be reached via email to bruce at perens dot com.

Bruce Perens is one of the founders of the Open Source movement in software, and was the person to announce “Open Source” to the world. He created the Open Source Definition, the set of legal requirements for Open Source licensing which still stands today.

Mr. Perens is presently CEO of Algoram, a start-up business which is producing a 50-1000 MHz software-defined radio transceiver, and of Legal Engineering, a legal-technical consultancy.

Mr. Perens was Senior Global Strategist for Linux and Open Source with HP, and vice president of Sourcelabs. He represented Open Source at the U.N. Summit on the Information Society, at the invitation of the U.N. Development Project. Mr. Perens is the creator of Busybox, which is a component of Millions of commercial devices that use Linux. Busybox has the unfortunate feature of being the most-litigated Open Source program, although Mr. Perens was never associated with the plaintiffs. Mr. Perens eventually started assisting the defendants in these cases, which led to the formation of Legal Engineering.

Mr. Perens is a generalist, and feels that the most creative work is done at the intersections between fields rather than as a specialist in only one. Thus, he has worked on the junction of art and software at Pixar Animation Studios, the junction of intellectual property, economics, community, and programming in his work on Open Source, the junction of law and software for Legal Engineering, and the junction of electronics, communications, and software in his software-defined radio work for Algoram.

For his consultancy, Legal Engineering, Bruce Perens is the bridge between lawyers and engineers, helping one to understand the other. He instructs engineers in how to comply with legal requirements and how to deal with intellectual property issues in their own work, and produces clarity for attorneys who are working on issues of computer software.

Among his skills, Mr. Perens is an operating systems programmer, a microcode (a level lower than assembly language, used in CPU design) programmer, computer language designer, is knowledgeable in electronics and an innovator in wireless communications, and is an intellectual property specialist. He is well-known as a technology evangelist, has published 24 books as a series editor, and made his living for several years as a paid public speaker.

Mr. Perens was involved in the creation of the field of 3-D animated feature film, working for 19 years in total in the film industry as a software developer, the last 12 of those years at Pixar, where he interacted frequently with Steve Jobs, designed a computer language for image processing, produced some of the software that Pixar uses to create animation, and was a Unix kernel programmer. He is credited as a senior systems programmer on the films Toy Story II and A Bug’s Life, and had uncredited technical roles in the production of many other films.

Mr. Perens was expert for the plaintiff in the appeal of Jacobsen v. Katzer, which established the legality of Open Source licenses. He was a case strategy consultant for Google’s outside counsel in the lower court case of Oracle v. Google. He has taught Continuing Legal Education classes in many states, although he is not an attorney. Most recently, he was keynote speaker at the Baker and Mackenzie Tech Days 2015, a Silicon Valley event attracting over 250 attorneys.

Mr. Perens was an operating systems programmer at the Computer Graphics Laboratory of the New York Institute of Technology, and a visiting researcher at the Univesity of Agder under a 3-year grant from the Competence Fund of Southern Norway. He was a remote researcher with the Cyber Security Policy Research Institute of George Washington University.

As series editor of the Bruce Perens’ Open Source Series with Prentice Hall PTR publishers, Mr. Perens published 24 books on Open Source software under an Open Publications license (predecessor to the Creative Commons licenses). All but one of the books was profitable and several still sell well more than a decade after publication.

Mr. Perens was founder of No-Code International, which helped to convince the International Telecommunications Union, FCC and the telecommunications regulators of many nations to drop the Morse code requirement for Amateur Radio licensing. With the possible exception of Russia, all nations have now dropped that requirement. Mr. Perens is a Radio Amateur, and holds an holds an Amateur Extra class license, with station license K6BP. He is active in the innovation of new codecs and protocols for digital voice communications. He serves AMSAT in helping to create a new geostationary satellite in cooperation with FEMA, which will provide 24-hour digital communications including disaster services.

# [Debian 壁纸 集锦](https://gitee.com/yuandj/siger/issues/I5LYU6#note_12321591_link)

![输入图片说明](https://foruda.gitee.com/images/1660260005539759818/屏幕截图.png "屏幕截图.png")

[Actualizar Debian 10 a Debian Bullseye - DEV Community](https://dev.to/luismorenomx/actualizar-debian-10-a-debian-bullseye-673)
dev.to|1000 × 500 jpeg|图像可能受版权保护。

![输入图片说明](https://foruda.gitee.com/images/1660260277064610513/rij6kll.jpeg "rIj6KlL.jpg")

[Debian Wallpapers - Wallpaper Cave](https://wallpapercave.com/debian-wallpaper)
Wallpaper CaveWallpaper Cave|1440 × 900 jpeg|Image may be subject to copyright.

![输入图片说明](https://foruda.gitee.com/images/1660260381233842532/zakhcrj.jpeg "zaKhCRJ.jpg")

[Debian Wallpapers - Wallpaper Cave](https://wallpapercave.com/debian-wallpapers)
Wallpaper CaveWallpaper Cave|1920 × 1200 jpeg|Image may be subject to copyright.

![输入图片说明](https://foruda.gitee.com/images/1660260507515813838/wallpapersden.com_debian-os-linux_1920x1200.jpeg "wallpapersden.com_debian-os-linux_1920x1200.jpg")

[debian, os, linux Wallpaper, HD Hi-Tech 4K Wallpapers, Images, Photos ...](https://wallpapersden.com/debian-os-linux-wallpaper/)
wallpapersden.com|1920 × 1200 jpeg|Image may be subject to copyright.

![输入图片说明](https://foruda.gitee.com/images/1660260715586241338/debian-red-logo-4k-red-brickwall-debian-logo-linux-scaled.jpeg "debian-red-logo-4k-red-brickwall-debian-logo-linux-scaled.jpg")

[Debian 11 Bullseye inicia concurso para arte do desktop do sistema ...](https://www.bitbaru.com/site/debian-11-bullseye-inicia-concurso-para-arte-do-desktop-do-sistema/)
bitbaru.com|2560 × 1600 jpeg|Image may be subject to copyright.

![输入图片说明](https://foruda.gitee.com/images/1660260851375574935/em1l2mj.jpeg "Em1L2mj.jpg")

[Linux Wallpapers - Wallpaper Cave](https://wallpapercave.com/linux-wallpaper)
Wallpaper CaveWallpaper Cave|2880 × 1800 jpeg|Image may be subject to copyright.

![输入图片说明](https://foruda.gitee.com/images/1660260995997220829/157688-linux-debian-minimalism.jpeg "157688-Linux-Debian-minimalism.jpg")

[Linux, Debian, Minimalism HD Wallpapers / Desktop and Mobile Images ...](https://hdwallpaperim.com/linux-debian-minimalism/)
hdwallpaperim.com|3840 × 2160 jpeg|Image may be subject to copyright.

![输入图片说明](https://foruda.gitee.com/images/1660261111326832643/4474536.jpeg "4474536.jpg")

[Debian Desktop Wallpapers - Top Free Debian Desktop Backgrounds ...](https://wallpaperaccess.com/debian-desktop)
wallpaperaccess.com|1680 × 1050 jpeg|Image may be subject to copyright.

![输入图片说明](https://foruda.gitee.com/images/1660261386168279094/debian-metal-logo-blue-metal-background-linux-artwork-debian.jpeg "debian-metal-logo-blue-metal-background-linux-artwork-debian.jpg")

[Scarica sfondi Debian in metallo con logo, blu metallo, sfondo, Linux ...](https://besthqwallpapers.com/it/download/original/92957)
besthqwallpapers.com|2560 × 1600 jpeg|Image may be subject to copyright.

![输入图片说明](https://foruda.gitee.com/images/1660261454586877423/homeworld_wallpaper.png "homeworld_wallpaper.png")

["Homeworld" will be the default theme for Debian 11 - Bits from Debian](https://bits.debian.org/2020/11/homeworld-will-be-the-default-theme-for-debian-11.html)
bits.debian.org|2620 × 1474 png|Image may be subject to copyright.

![输入图片说明](https://foruda.gitee.com/images/1660261740635258626/com0579.jpeg "com0579.jpg")

[Debian. Desktop wallpaper. 1920x1080](https://en.free-wallpapers.su/img135857.htm)
en.free-wallpapers.su|711 × 400 jpeg|Image may be subject to copyright.

![输入图片说明](https://foruda.gitee.com/images/1660261981763637794/linux-1660261938333-1216.jpeg "linux-1660261938333-1216.jpg")

[Linux, Debian Wallpapers HD / Desktop and Mobile Backgrounds](https://wallup.net/linux-debian-10/)
wallup.net|2560 × 1600 jpeg|Image may be subject to copyright.

![输入图片说明](https://foruda.gitee.com/images/1660262039175488068/mockup-1.png "mockup-1.png")

[DebianArt/Themes/non-distracting-mode - Debian Wiki](https://wiki.debian.org/DebianArt/Themes/non-distracting-mode)
DebianDebian|1600 × 1200 png|Image may be subject to copyright.

![输入图片说明](https://foruda.gitee.com/images/1660262154605778425/linux-1660262079555-3650.jpeg "linux-1660262079555-3650.jpg")

[Linux, Debian Wallpapers HD / Desktop and Mobile Backgrounds](https://wallup.net/linux-debian-7/)
wallup.net|1920 × 1200 jpeg|Image may be subject to copyright.