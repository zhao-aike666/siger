- [openEuler 熊伟：如何看待开源社区中的 SIG 组织模式？](https://my.oschina.net/oscpyaqxylk/blog/5560611)
- [没有 SIG 的开源项目不是好社区？](https://my.oschina.net/oscpyaqxylk/blog/5546957)
- [CompuServe](http://www.trs-80.org/compuserve/) # [Special Interest Groups](#special-interest-groups)

<p><img width="706px" src="https://foruda.gitee.com/images/1665677935187022449/ce61d7f5_5631341.jpeg" title="330SIGoe.jpg"></p>

> openEuler 最鼓励的行为就是国际化，最看重的也是国际化，没有之一。我希望 openEuler 更多地能在国际上发出声音，和国际社区形成协同共赢的关系。

这是本期专题的着力点之一，OE 国际化是我最开始加入 OE 就能切身感受到的，封面的背景是2021年6的 OE 开发者日上我的展台，谁能想到 树莓派 SIG 组的初衷竟然是国际化，尽管执行团队最终并未能如愿，在 SIG 工作报告中，能感受到，这份国际化的努力，而我也是因此才结缘 树莓派社区的，就在此刻，代表着国际文化交流的 石榴派 以及 Blesschess 国际版，已经陈列在 树莓派国际基金会的图书馆中，而我日常邮件中，多了一份来自英国的关注。

> 封面故事：我携带者1个月发愿，获得社区支持得到的 RISC-V 版 石榴派，与 OE 社区的两大 SIG 组，sig-Raspberry & sig-RISCV 同台展示，内容是 同属 blesschess 民族棋序列的 Luckystar，以及 基于 OE 衍生的发行版石榴派，硬件两个平台，树莓派 3B+（还是 OE 社区捐赠的），石榴派 FPGA RISCV 开发板。而我带去的小礼物，是我的学生们手工制作的 民族棋DIY棋具。在展台上，我娓娓道来民族棋的故事，成为了当天的全部，而就在1周前，我还无法确认能否到场，结果是圆满的，我实现了感谢 OE 的愿望，这是一场成功的展示，OE 社区助力民族棋文化的传播，尽管国际化当时还正在酝酿，是半年后的 11月份，我和社团同学以及社区的小朋友们，共同精心准备的礼物，漂洋过海，抵达了目的地。现在想来，这就是一份满满的纪念啦。这不是 OE 社区的目标，但它是 SIGer 的。

说道感谢，不能不说 SIG，我也会再任何时候都会介绍 这个名词包括现在 All in 开源的科普事业，都要感谢 OE 的 sig 建设，我是 oe 第 75 个 SIG 组 sig-minzuchess 的 Maintainer。尽管提交的 pr 很少，但它的使命就是 为青少年参与到开源社区建设，提供一个通道。现在是时候可以发力了。因为 SIGer 的自然生长，以及即将展开的 《全民开源素养计划》必然包含这部分内容。经过一年多的准备，社团的同学们已经摩拳擦掌，跃跃欲试了。

> SIGer 专题的 三篇推荐，已经成了惯例，第三篇 TRS-80 就隶属于 计算机史 的范畴，《开源观止》第4期的封面，就与它一脉相承，都是 80 年代的微型计算机的崛起，新的专题很快就会发布。所以，本期专题能同时称为 SIGer 缘起啦。再次感谢 OE 社区的分享，感谢 开源社区，感谢 OSCHINA ...

# [openEuler 熊伟：如何看待开源社区中的 SIG 组织模式？](https://my.oschina.net/oscpyaqxylk/blog/5560611)

原创 OSCHINA编辑部 [开源治理](https://my.oschina.net/oscpyaqxylk?tab=newest&catalogId=7048510) 07/31 22:15 阅读数 270

本期单口开源我们请到 openEuler 技术委员会的委员熊伟来和大家聊一聊 “如何看待开源社区中的 SIG 组织模式？“

单口开源栏目旨在分享开源理念与观点。

> 大家好我是熊伟，来自 openEuler，我是 openEuler 技术委员会的委员。

openEuler 作为一个全场景的 ICT 的操作系统平台，已经发展得非常庞大。对于如此庞大的一个社区，如何进行社区的组织，是一个值得探讨的话题。今天我们就探讨一下这个问题。

目前为止，openEuler 是以 SIG 组为基本的组织单元来进行社区组织的，SIG 也是全球比较通用的一种说法，就是全称是 Special Interest Group。

SIG 组这种组织形式实际上是把社区划分成不同的功能模块，按照功能模块来进行人员的组织。这种划分有两种方法：第一种办法就是我们横向划分。横向划分就是把软件栈从下到上按照不同层次以 SIG 的形式进行组织。还有一种模式是纵向划分。就是将 SIG 组用解决方案这种方式从上到下进行一个贯通式的组织。纵横正交叉，从技术维度层面来讲的话，就是它进行了技术全覆盖，使得不遗漏任何的技术点。同时这种纵横交叉也使得不同 SIG 之间有着很好的联系关系，使得 SIG 和 SIG 之间能形成很好的协同关系。

在 SIG 中我们是有专门的 Release SIG 来协调大家共同开发、共同演进，同时共同发布这个大版本。

这种组织形式大家听下来以后会发现其实是比较适合以企业为主的社区，同时社区的产出是要有商业目标，它的发布版本是要进行商用的应用的，这时候对质量和时间性要求比较强，所以是比较适合社区组织的。当然社区组织以 SIG 为基础的组织形式，可能有它的优势，但是也有它的劣势和弊端，弊端就是对个人爱好者相对来讲不是那么友好。

在未来的 openEuler 发展过程中可能我们会对整个技术社区包括组织形式进行进一步的演化，结合 SIG 的优势同时也能够进一步发展，使得个人爱好者能够更容易的融合到我们社区的发展中。所以未来的 openEuler 社区是一个以 SIG 为主，同时辅助于新的组织形式和基础设施能够让个人爱好者也能更自由的灵活的加入到社区开发者模式。

不同的社区可能有不同的组织形式，今天我们所讲的组织形式是 openEuler 经过过去两到三年我们摸索和演化而来的组织形式。如何进行社区组织其实是一个非常大的话题，也是一个经久不衰的话题，也是每个社区能够长治久安发展的非常核心的问题，我们欢迎后续大家能够加入到 openEuler 社区里和社区一块探讨如何进行社区的组织，如何进行基础设施的演化。我们也欢迎大家对社区如何进行组织这一个核心问题进行交流。

# [没有 SIG 的开源项目不是好社区？](https://my.oschina.net/oscpyaqxylk/blog/5546957)

原创 OSCHINA编辑部 开源治理 06/29 18:12 阅读数 1.3W

在开源社区活跃过的同学估计都加入过或是见过各种名中带 SIG 的小组。但凡上了点规模、社区人数在 200 人以上的开源项目大多都会成立一个个专门的 SIG 小组，从不同角度或技术模块去治理一个庞大的工程。

SIG——Special Interest Group，可以说是天生就带着计算机工程基因的组织模式。最早在 1961 年，计算机协会 ACM 提出可以创建 SIG，给在特定领域的知识、学习或技术方面有着共同的兴趣的成员合作机会，以供兴趣相同的人在特定领域内共同提出解决方案，并且可以交流、会面和组织会议等。直至今日，ACM 对于 SIG 作用的看法也基本和最初相同，“管理计算学科和专业的动态挑战需要协作努力，以加速研究、产生解决方案和传播结果。”

> ACM 的特殊兴趣小组 (SIG) 代表计算的主要领域，解决推动创新的技术社区的利益。SIG 提供大量专注于特定计算子学科的会议、出版物和活动。它们使成员能够分享专业知识、发现和最佳实践。

但是，19 世纪 60 年代，囿于网络条件，能够通过网络找到并加入某个 SIG 讨论的人极为有限。而后 SIG 真正开始普及则有赖于一家分时系统在线服务商 CompuServe 。1979 年 8 月，CompuServe 作为微型计算机 MicroNET 用户的在线服务推出。MicroNET 通过电话调制解调器向消费者开放了通常为企业保留的 CompuServe 网络。并且 CompuServe 提供了许多功能来吸引用户，比如电子邮件、线上游戏、新闻、天气预报等等，其中就还包括 SIG 服务，或者称之为 SIG 论坛。

CompuServe 的流行功能之一是[特殊兴趣组](http://www.trs-80.org/compuserve/)，也称为 SIG 或论坛。一位名为 Richard Taylor 的工程师重写了自己的 TRS-80 BBS 软件 Message-80，以在 MicroNET 上作为多用户 BBS 运行。之后，Richard Taylor 还为其他人和公司创建了 SIG，到 1981 年总共有 8 个，其中有 3 个与 TRS-80 有关。这也恰恰印证了，SIG 源于工程师对特定领域的兴趣，并作为一个汇聚特定人群，解决特定领域的难题的平台被建立。

> _CompuServe 当年的广告_ 

今天，在众多开源项目和组织中，SIG 模式得到延续。当然在不同的地方，对 SIG 的划分标准，和其所承担的责任互有不同，OSCHINA 编辑部拟邀请各大社区分享自身的 SIG 建设经验，呈现当下开源社区中 SIG 的具体组织模式与所带来的结果。

本篇我们邀请到 openEuler 社区技术委员会委员熊伟进行分享。

开源 2 年时间以来，openEuler 社区已经成立近百个 SIG，贡献者 8000 多人。从开源之初，openEuler 社区便按照 SIG 模式组建，每一个 SIG 都会拥有一个或多个项目，这些项目会拥有一个或多个 Repository，SIG 的交付成果会保存在 Repository 内。参与者可以在 SIG 对应的 Repository 内提交 Issue、针对特定问题参与讨论，提交和解决问题，参与评审等。

SIG 组成员包括开发者、运维人员、用户等，SIG 会根据 openEuler 开源社区每半年发布一个新版本的节奏，召开 SIG 组版本规划会议，将 SIG 的开发工作和 openEuler 的版本发布协调起来。每个 SIG 组的日常工作，通常包括每两周一次的例会，技术人员见面（Meetup）、在 openEuler 开发者大会，或者 openEuler 操作系统峰会等场合进行应用成果展示等。通过这样的形式，就串联起来了开发，测试，最后融入新版本的流程。从而保证了 openEuler 整体的发展和各个 SIG 组的发展的协调一致。

<p align="right">讲述：熊伟</p>

<p align="right">采访整理：一君</p>

## 通过 SIG 模式保证 openEuler 的商用基础

openEuler 是一个比较重视商业应用的开源社区，它首先要产出产品，服务 openEuler 的上下游企业。我们最终的目标还是要实现规模商用，openEuler 要真正进入到企业应用中去。这就对 openEuler 的运营，开发，质量管控等提出了非常苛刻的要求。

在国际上，社区组织的形式有不同的形式。以 SIG 这种形式进行社区组织我个人认为是比较适应企业需求的，比较便于各个企业之间进行协同，形成规范、高效的组织形式。通过 SIG 把庞大的系统按照模块切分，再引入相应的企业，可以保证专业的人来做专业的事情。我们始终要保证一个 SIG 中的核心开发是在这个领域最具影响力和专业能力的人。

对于 openEuler 来说，对于大多数 SIG，我们倾向于 SIG 的核心团队是以企业员工为主体，而且相关企业也一定是从事这个领域的专业公司，这样才能保证 SIG 的专业性。因为操作系统真正部署的时候，一般会需要维护 3-5 年，有的行业甚至需要十年以上的维护周期，所以社区需要保证稳定，持续的人力投入，这样才能保证系统的稳定可靠，用户才能放心使用，而这种诉求一般只有企业才能承担。我们不能简单依靠爱好来驱动产品的产出。所以我们现在通过 SIG 的机制保证企业应用的稳定，这就是我们社区整体设计的出发点。

## SIG 切分：按照软件栈及领域划分

openEuler 社区对 SIG 的切分大体有两个维度。

一个维度是横向的，从下往上按软件栈切分，某些软件处在整个基础软件栈中的一层。比如虚拟化是操作系统中的一层，虚拟化之上是各种服务，那么我们就可以将虚拟化软件独立抽象出来，单独成立” 虚拟化 SIG“。

另一种是纵向的，针对领域做垂直划分，把某一领域下的相关软件集成在一起组成 SIG，可能还会做二次开发，形成这个领域开箱可用的一种解决方案。比如云原生、边缘计算等 SIG，这些都是属于垂直领域的 SIG。

社区早期阶段，横向 SIG  比较多一些，慢慢现在新成立的 SIG  就以纵向为主了，以产出解决方案居多。横向软件栈的切分数量是有限的，经过一段时间积累横向软件栈都补齐以后，纵向解决方案就成为社区的主要工作了。这种趋势也是我们乐见其成的，因为 openEuler 社区的真正价值最终还是要体现在一个个垂直领域的应用场景上去。

对于 openEuler 社区本身来说，我们自身的管理，运维也是以 SIG 形式呈现的，可以说 everything in SIG，比如基础设施 SIG，文档 SIG 等，开发流水线 SIG 等等。包括技术委员会本身也归属一个 SIG。这样的组织形式也保证了社区本身的运作是稳定，统一和高效的。

## 开发者参与及 SIG 内部管理

因为 openEuler 所有内容都是免费的，所以如果只是单纯的使用，直接下载使用就好了，并不需要所谓参与到 SIG 中去。

### 所谓参与 SIG，更多是指贡献。

比如往项目里增加、修正代码，或者你觉得哪里不够好，或者有一个 idea 要再做个新的软件等等，这个时候，就需要和 SIG 打交道了。

简单来说，贡献分几个步骤。

首先贡献前需要签署 CLA 协议。这是参与社区贡献的第一步。

签署完 CLA，就可以做很多工作了，比如报一个 issue，也可以修复一个问题，或者是贡献文档、提交 PR 等都算是贡献和参与。或者，你如果发现 openEuler 中少了某个常用软件，或者是有一些自己用起来很顺手的软件想要贡献进社区，也可以提交，并归属到某个 SIG 中。但这时的贡献者往往只有提交的权利，还没有代码审查、合入的权利。如果要拥有往 openEuler 中合入的权力，则要成长为 committer 或者 maintainer。

任何一个 SIG 都有 2 个重要的角色。。一个是 committer，假设 SIG 中有 50 个软件仓，其中有 2 个仓指派给了某个 committer 进行管理，那么这个 committer 可以对这 2 个软件仓有了合入等权力，但对其他 48 个未经授权的软件仓就没有管理权力。committer 权力相对比较小，只需要对相关领域比较熟悉就可以了。SIG 中另一个重要角色是 maintainer，他对整个 SIG 中所有仓都拥有管理权利，可以进行合入，删除等处理。Maintainer 的要求就比较高一些，他要对所属 SIG 的具有全面的了解和知识覆盖。所以一般开发者的成长路径是从提交者逐步成长为 committer，最后成长为 maintainer。

因此，我们通常的建议是，开发者最初可以先提交一些小的 patch，让 committer 或者 maintainer 合入。做了一段时间之后，有能力去做维护某些软件以后，就可以成长为 committer，对 SIG 中的部分组件有维护权。再过一段时间，可能你对整个 SIG 的相关知识都比较熟悉了，大家也认可你的能力，这时就可以被推选到 maintainer 的 角色，对 SIG 全部的软件都有话语权。通过这种形式，我们既能保证现有的产品交付和产品质量，也能不断的培养新生力量。

## 未来 openEuler 的组织形式

目前以 SIG 为核心的社区组织形式运转还是比较良好的，但是并不代表完美无缺。主要有两个问题：

1. SIG 不可能无限制地膨胀，不可能持续不断的一直增加 SIG
2. SIG 这种组织形式对个人爱好者并不友好，并不利于个人开发者加入到 openEuler 社区开发中。

所以现在我们在思考，下一步社区的方向可能就是分布式化，允许个人开发者加入到社区中，不需要成立 SIG、也不需要加入某个 SIG。如果他对某个领域或者某几个软件很有心得，都可以通过一套机制融合到 openEuler 里，可以随着正式版本发行，或者是进入流水线。

我们期望在今年年底，openEuler 基础设施 2.0 能够出来，大家能够看到一个更加完整的体系，能够融合更多人进入到 openEuler。我们后续可能把个人爱好者提供的软件单独分一类，然后通过基础设施，让其能够融合到社区里面去。

## 成立新 SIG 的流程

成立 SIG 需要在技术委员会上申请，技术委员会通过投票来决定是否成立。

成立 SIG 需要说清楚的有几个方面的问题：

1. 每个 SIG 的整体设计目标是什么，相关的交付件是什么，比如建立什么样的仓，每个仓具体都做一些什么样的工作。
2. SIG 的初始 maintainer 是谁也需要确定。要说明为什么这些 maintainer 具有相关的专业能力运作好这个 SIG，我们不会允许一个做大数据的团队申请建立一个虚拟化的 SIG。
3. 除了人和事，还要有很明确的交付节奏，也就是要有明确的开发 roadmap。同时要规划好和社区版本的匹配。

以上三个问题如果能清晰的明确出来，那么在技术委员会上就比较容易通过。

SIG 申请通过之后，技术委员会会给新成立的 SIG 配一个技术委员会委员作为导师，协助新成立的 SIG 早日开展工作。

技术委员会有时候会遇到这样的申请，比如有人想做一个或者两三个小软件，然后把这两三个软件形成一个 SIG， 通常对于这种申请，技术委员会会建议归到现有的 SIG 中。因为 SIG 运作也是有成本的，SIG 不仅需要把软件维护好，同时要做市场宣传、质量把控，还要遵守整个社区的版本发布规划等等工作，因此并不是 SIG 越多越好，切分越碎越好。

openEuler 社区鼓励 SIG 本身要做好对未来的规划，发展方向是什么？是不是能自研一些软件？比如现有 SIG 领域里，是不是能孵化出一些新的软件，这些新的软件能比现在的软件更具技术优势。创新一直都应该是 SIG 的重要工作目标。

SIG 成立只是万里长征第一步，并不是成立以后就万事大吉。技术委员会也会定期对 SIG 的运作进行审视，如果运作不好，SIG 也可能会被撤销或是合并掉。另外要强调一点的是，SIG 是一个动态而非静态的概念。如果 SIG 太大了，也可能会分裂成若干小的 SIG，也可能会出现小 SIG 合并成为一个大的 SIG。这些在我们社区都已经发生了很多次了。

## 技术委员会：统筹 SIG，但不干涉 “内政”

技术委员会是 SIG 的管理机构，但是技术委员会的名字可能会让很多人有误解。可能有的人会认为技术委员会是决定一切技术的地方，委员也应该是是事无巨细，什么技术都懂的” 技术多面手”。但实际上，openEuler 作为一个庞大的操作系统体系平台，技术方向非常多，在每个细节点上，专业深度都非常深，没有什么技术高手能够对每一个技术细节都很了解。所以，openEuler 对于技术治理的思路很简单，就是 “专业的事情交给专业的 SIG 去做”。整个管理体系尽可能的扁平化，SIG 对自己内部事务有完整的治理权。技术委员会不干涉具体的技术决策，比如内核 SIG 自己来规划整个内核应该如何演进。

技术委员会主要有四个方面的关注。

第一，把握整个社区的演进方向，治理结构等。对于 SIG 组的管理就体现了这方面的工作。管理好 SIG 的整体结构就相当于管理好了整个社区的总体架构。

第二，对 SIG 要行使监督，同时要随着 openEuler 在不同阶段的任务对架构进行调整。包括对技术方向的调整等，始终保证社区能够在一个正确的轨道上运转。

第三，对整个社区的技术拥有最终的仲裁权。虽然具体的技术点是由 SIG 做决策，但是因为操作系统比较复杂，不同 SIG 之间肯定会有交集，不同 SIG 之间有配合关系，很多时候也会有冲突，我们通常都鼓励 SIG 间能自行协商解决，但如果 SIG 自身解决不了，最终决策权就到了技术委员会，对技术分歧进行最终决策。

第四，对整个社区未来的发展方向做规划，最开始 openEuler 只是做一个服务器版本的操作系统，但很快发现有些公司开始用 openEuler 做边缘计算了，再往后有些公司开始将 openEuler 做到嵌入式系统中，甚至社区还有用 openEuler 做机器人操作系统平台的。因此 openEuler 本身的定位也是在社区的演化中不断修正。最初狭窄的定义现在已经扩展到 ICT 的各个领域中了。那么如何满足不同行业的差异，以及未来整个技术走向，技术架构怎么归一等等这些问题，也是技术委员会需要去考虑的。

整体来说，技术委员会把握宏观方向，细节要放给专业的 SIG 团队去做。

一般很多人听到这里会担心这种组织形式听起来好像比较松散，可能大多数人的直观感觉应该是：规章制度越多，系统会越稳定。但其实并不是这样，当一个系统大到一定程度，并不是增加更多的规章制度才能管理好，可能恰恰需要做减法，把握好大的原则，然后相信整个系统的自组织能力，权利下放，由 SIG 按照他们的特性来制定适合的运作模式。大方向整齐划一，细节领域百花齐放，我认为这是管理大规模系统的一种较好的方式。

社区在运行过程中肯定会出现做得很优秀的 SIG，也肯定会产生一些运作较差的 SIG。这个时候不要太担心，我们需要一些容错性，消化一些错误成本。首先技术领域不同，公司来源不同，每人的素质能力不同，怎么可能刻一个模子把所有 SIG 都规范起来？这个是不可能的。同时，如果我们真的把 SIG 都搞成千人一面，也往往就扼杀了一些创造性。所以让每个 SIG 找到自己的发展规律，技术委员会做好监督工作，及时修正差的 SIG，那么整体运作上就不会出现大的问题。

这是 openEuler 目前主要的运作理念，当然也不一定是对所有社区都适用，但至少对于 openEuler 社区来说，从过去两年半的运作来看，情况还不错。

## SIG 和 openEuler 发布版本的关系

openEuler 社区每年都会有新版本发布，技术委员会虽然不干涉 SIG 日常的运作，但版本发布周期规定很清楚，每年 3 月和 9 月发版，两年一个 LTS 版本。质量标准是确定的，各个时间节点，比如什么时候软件仓代码冻结也是很清晰的。接下来就是各个 SIG 配合发布计划去做，只要最终结果的质量达标就可以。所以虽然微观上技术委员会好像没有对 SIG 进行管辖，但是宏观上实际每个 SIG 都是需要在同一个步调内统一协作的。

对于运作不好的 SIG，虽然社区也不会有惩罚机制，但是市场会有惩罚机制。举一个例子，当一个新版本发布以后，某个 SIG 所属的硬件没有赶上这个版本发布，那么市场上其他公司的硬件都支持了，只有你的硬件不支持，那么这个硬件所在的公司就会失去部分市场机会。所以各个 SIG 中的公司会按照自己的商业目标和利益来规划自身在社区中的动作，如果跟不上新版，那么这个版本所覆盖的一些客户群可能就丢失了，这是一种很合理的市场调节逻辑。

这也是我比较强调 SIG 一定要跟自己的公司业务相关的原因。这也是我们能够保证 openEuler 能够及时适应市场，保证质量的前提。

## SIG 目前的氛围和关系怎么样？

目前，SIG 的氛围还不错。SIG 申请者大体上有两类，一类是以单个公司为主导，后面可能会加入一些其他公司。那么在以一个公司为主导的 SIG 中，管理相对来说就比较简单。另一种就是发起时便是几家厂商联合，显然是已经几家公司已经沟通好了。其实无论是一家企业发起，还是几家企业发起，我们看到大家在社区里互相关系都是比较好。

另外在技术层面，大家都是实事求是的，所以目前为止，社区中我没看到氛围方面有任何问题。这个也是我非常开心的事情。肯定也会有观念不一致的地方，但原则也很简单，首先技术委员会鼓励 SIG 内部协商讨论。有些 SIG 可能是少数服从多数，有些可能是某个人有一票否决权，具体怎么在 SIG 内达成一致，这些权利完全下放给 SIG 去自行决定。

SIG 和 SIG 之间如果出现矛盾、版本冲突等问题，实在无法内部解决，技术委员会就会做为最终的裁决机构，给出决策。当然这个决策可能过了几个月之后发现不合适，那么也可以再重新决策。我们要保证的是决策的及时性。一个及时的坏决策也远远好过不决策，时间才是最大的成本，错了只要及时纠正就好了，但是不决策往往会带来更大的灾难。

## openEuler 最好的 SIG 是哪个？

目前在 openEuler 社区中的 SIG 有接近 100 个 SIG 了，很多 SIG 都很有特点，如果一定要选一个最好的话，我个人认为，运作最好的是 OpenStack 这个 SIG。

首先它是由若干公司共同建立的，这是 openEuler 社区一直倡导的共建精神。

其次协作很好，不光是内部协作很好，和 openEuler 版本的发布协同也很好，

第三，这个 SIG 也贡献和孵化了一些自研的项目，这是我很看重的，一个 SIG 很重要还是要有 “自己的干货”。

最后，也是我最满意的一点，就是这个 SIG 的国际化做的很好。OpenStack 本身有上游社区，他们把 openEuler 放到了上游社区的 CI 平台。这样 OpenStack 社区就能和 openEuler 形成良好的协作，将 openEuler 和国际社区做了连接。这是我们所有 SIG 都应该最终做到的事情。openEuler 最鼓励的行为就是国际化，我最看重的也是国际化，没有之一。我希望 openEuler 更多地能在国际上发出声音，和国际社区形成协同共赢的关系。

# [CompuServe](http://www.trs-80.org/compuserve/)

> written by Matthew Reed

CompuServe was the most famous of the early online services and the one with closest ties to the TRS-80. It actually started in 1969 as a timesharing system, renting mainframe computer time to businesses over phone lines. However, what most people remember as CompuServe dates to August 1979, launched as an online service for microcomputer users named MicroNET.

MicroNET opened the CompuServe network, normally reserved for businesses, to consumers with a telephone modem. Access was only available outside of business hours, when their mainframes were normally idle. MicroNET provided more or less raw access to the CompuServe mainframes running the TOPS-10 operating system. Users could use the included programs or create and run their own programs on the system.

![CompuServe advertisement](https://foruda.gitee.com/images/1665671986998312615/d455867a_5631341.png "屏幕截图")

> Two page CompuServe advertisement from the August 1981 issue of [80 Microcomputing](http://www.trs-80.org/80-microcomputing/)

## CompuServe Information Service

On July 1, 1980 CompuServe rebranded their consumer service as CompuServe Information Service or CIS. The name MicroNET lived on as the “personal computer” area, but a simpler to use menu system became the main user interface.

At the same time, Radio Shack became the exclusive retailer for the CompuServe starter kits, which included the necessary software and one hour of free time on CompuServe.

From the 1980 press release:

> The era of instant, convenient access to up-to-the-minute electronic information and services is here today, according to a joint announcement by Radio Shack and CompuServe Incorporated.
> 
> Radio Shack, through its nationwide chain of Radio Shack Computer Centers, Radio Shack stores and dealers, will be the exclusive retailer of the software necessary to access the CompuServe Information service through microcomputers such as its TRS-80, or by means of “dumb” information terminals.
> 
> Beginning August 1, microcomputer users with Radio Shack’s new TRS-80 VIDEOTEX software package will be able to access data provided by CompuServe through its national packet switching and computer network. This is presently available in more than 230 markets on an inexpensive local-call basis.
> 
> Initially, the network will offer a variety of services at a low hourly rate, including: major news services, electronic mail and bulletin board service; educational and financial programs; a securities information service and various computer games.

## Cost

Like all of the online services, CompuServe access wasn’t inexpensive. In 1981, the cost for 300 baud access was:

- $5.00 an hour during non-peak hours (6PM to 5AM weekdays and all days weekends and holidays)
- $22.50 an hour during peak hours (5AM to 6PM weekdays)

Not surprisingly, most people used CompuServe during non-peak hours. Those prices only included the CompuServe charges; anyone not fortunate enough to live in a city with a local access number also had to pay long distance or network access charges ($2.00 an hour for Tymnet).

To put these prices in perspective, in 1981 the federal minimum wage in the United States was $3.35 an hour.

## Services

CompuServe offered many features to attract users, many of which mirror services available on the Internet today. Some of those services included:

- electronic mail
- online games
- access to informational databases, such as stock quotes
- the CB simulator
- Comp-U-Store (known earlier as COMP-U-Star), an electronic shop-at-home service
- weather reports
- Associated Press news stories (although this experiment ended in 1982)
- Special Interest Groups
- programming languages, including APL, BASIC, BLIS10, FOCAL, PASCAL, SNOBOL, XF4 (FORTRAN), and MACRO (PDP-10 assembly language), as well as a variety of cross-assemblers.

The menu system could be bypassed by typing in a destination directly using the “Go” command. Popular TRS-80 related destinations included:

- the Radio Shack newsletter (Go TRS)
- LDOS (Go PCS-49)
- MNET80 (Go PCS-54)
- VTOS ST80 (Go PCS-56)
- TRS-80 color (Go PCS-126)
- Scott Adams' games (Go GAM-28)
- TRS-80 programs (Go PCS-45)
- Orchestra-90 music (Go HOM-13)

## CompuServe’s EMAIL

The CompuServe electronic mail service, originally called the MicroNET Electronic Mail System, was one of the few nationwide options for electronic mail. It was a major reason why many people used CompuServe.

CompuServe used eight digit octal numbers to identify each user. For example, the address to submit information and ideas to the [TRS-80 Microcomputer News](http://www.trs-80.org/trs-80-microcomputer-news/) was 70000,535.

## Special Interest Groups

One of the popular features of CompuServe was the Special Interest Groups, also known as SIGs or forums. The original software powering the SIGs was created by Richard Taylor. He rewrote Message-80, his TRS-80 BBS software, to run as a multi-user BBS on MicroNET. His new program was named CBBS.CMD and could be run from his personal directory using the command:  **IRUN CBBS.CMD[70161,101]** . It powered MNET80, which was the original SIG. Although MNET80 was a TRS-80 forum, there was no official connection of any kind to Radio Shack.

Richard Taylor also created SIGs for other people and companies, leading to a total of eight by 1981. It’s interesting to note that of those eight SIGs, three were related to the TRS-80.

- MNET80: “a TRS-80 Users Group that provides information and software for the TRS-80 line of microcomputers. Its members include software authors, many of whom have made their programs available to the membership at large.”
- VTOS: “a group comprised of VTOS and ST80 users. The group has a wide range of software interests, particularly in the software for the TRS-80 microcomputers.”
- LDOS: “comprised of LDOS users, who have interests in the uses and applications of the advanced LDOS operating system for the TRS-80.”

Later on, there were other TRS-80 related SIGS, such as the Color Computer SIG, the Model 100 SIG, and the TRS-80 Professional SIG. As the TRS-80 software market contracted, it was inevitable that those SIGs would start to disappear.

In 1985, CompuServe closed down MNET80, which at the time was its longest running SIG. Bob Snapp, president of Snappware, said:

> The Model I is gone; the Model III is up for sale. They, like MNET80, have been replaced by the next generation. In any event, it is always sad to see the passing of an old friend.

## Related Posts:

[![输入图片说明](https://foruda.gitee.com/images/1665672298197638710/9114c06b_5631341.png "屏幕截图")](http://www.trs-80.org/trs-80-videotex/) [![输入图片说明](https://foruda.gitee.com/images/1665672304576959019/5d5abf0c_5631341.png "屏幕截图")](http://www.trs-80.org/compuserve-menus-in-1982/) [![输入图片说明](https://foruda.gitee.com/images/1665672309856410457/26aa8a07_5631341.png "屏幕截图")](http://www.trs-80.org/fidonet/) [![输入图片说明](https://foruda.gitee.com/images/1665672314684101642/c462ab98_5631341.png "屏幕截图")](http://www.trs-80.org/pt-210-portable-data-terminal/)

- [TRS-80 VIDEOTEX](http://www.trs-80.org/trs-80-videotex/)
- [CompuServe Menus in 1982](http://www.trs-80.org/compuserve-menus-in-1982/)
- [FidoNet](http://www.trs-80.org/fidonet/)
- [PT-210 Portable Data Terminal](http://www.trs-80.org/pt-210-portable-data-terminal/)

Categories: [Telecommunications](http://www.trs-80.org/categories/telecommunications)