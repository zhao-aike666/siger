(以专访为名，隔空向 [RMS](https://gitee.com/yuandj/siger/issues/I3DLD8#note_5109877_link) 致敬)

[![输入图片说明](https://images.gitee.com/uploads/images/2021/0730/201702_95258a40_5631341.jpeg "RMS副本.jpg")](https://images.gitee.com/uploads/images/2021/0730/124009_f42a3b5f_5631341.jpeg)

终于可以给 RMS 老爷子写邮件了，因为 [GNUgames99](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/Issue%2011%20-%20GNUgames%2099.md) 的愿已经发好了。就剩下分享给更多同学一起实现啦。  
(点上面的封面，弹出的是 GNUgames99 的大图。) 因为：

> 我要用 [RISC-V](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%201st%20%E3%80%8A%E6%98%9F%E8%BE%B0%EF%BC%8C%E6%9C%BA%E9%81%87%EF%BC%8C%E4%BD%BF%E5%91%BD%E3%80%8B1.1.md) 驱动的[石榴派](https://gitee.com/shiliupi/shiliupi99/blob/master/MagSi/Issue1%20-%20The%20Personal%20Programmable%20Computer%20-%20Shiliu%20Pi%2099.md)，载着全栈赋能的 [石榴核](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/Shiliu%20PI,%20Shiliu%20Silicon,%20Shiliu%20Si,%20%E7%9F%B3%E6%A6%B4%E6%A0%B8.md)，驱动我们共同的[未来 —— 少年们](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/ISSUE%209%20-%20RV%20%E5%B0%91%E5%B9%B4.md)，去[践行自由软件的理想](https://gitee.com/yuandj/siger/issues/I3DLD8#note_5110108_link) - [education](https://www.gnu.org/education/education.html)。这就是 RMS 老爷子的愿，我们一起实现。这就是我自己寻找到的答案：

  - 《[开源治理的根基是什么？](https://gitee.com/yuandj/siger/issues/I3DLD8)》

    是 "_**[教育](https://gitee.com/yuandj/siger/issues/I3DLD8#note_6052266_link)**_ " , 是 **[赋能](https://gitee.com/shiliupi/shiliupi99/blob/master/MagSi/Issue1%20-%20The%20Personal%20Programmable%20Computer%20-%20Shiliu%20Pi%2099.md)** 。

      > 教育即指明方向，指北就是文化选择的问题，要什么样的生活的问题。  
      > 赋能是赋能未来，赋能青少年去担当，肩负创造未来美好生活的责任与使命，即理想和信念。

---

先看看我研读 RMS 的成果，上头条：

### 自由软件与教育

- 教育机构应使用自由软件
- (9965) GNU 在远方
- GNU 和 Linux
- 理查德*马修*斯托曼  
  (Richard Matthew Stallman, RMS)
- 捐赠 & FSF 会员资格
- GNU Backgammon & GPL3
- RMS TEDx video: 
  "Introduction to Free Software and the Liberation of Cyberspace"

而这只是近 4个月来，学习笔记的一部分，从头开始，梳理下我寻找到答案的线索：先上目录。

- 开源治理的根基是什么？- 4个月前
  - 如何选开源许可证？- 11天前
- 吉牛 GNU - 3个月
- 教育 - 3个月
- GNU 软件 - GNUbg - 12天前
- Free software to use for gameplaying - 2天前
- GNU(games)99 吉牛久久 - 1天前

---

### [开源治理的根基是什么](https://gitee.com/yuandj/siger/issues/I3DLD8)？

- 缘起：收获的一篇专访 《[卫剑钒，开源圈的“世外高手”](https://gitee.com/flame-ai/siger/blob/master/%E4%B8%93%E8%AE%BF/%E3%80%90%E5%8D%AB%E5%89%91%E9%92%92%E3%80%91%E4%B8%80%E4%BB%B6%E5%A5%BD%E4%BA%8B.md)》  - 4个月前
  > 将作为 SIGer 大使收录专访栏目，供同学们学习。  
  > 另外，卫SIR关于开源社区的许可证的其他译作，也将合集到一处，进行研究，  
  > 这是解答，为什么来到开源社区的问题。

  - [如何选开源许可证？](https://my.oschina.net/u/4489239/blog/4988178)- [开源治理专题](https://www.oschina.net/group/osgovernance)。【[笔记](https://gitee.com/yuandj/siger/issues/I3DLD8#note_5899538_link)】  
    已收录：[siger](https://gitee.com/flame-ai/siger/) / [专访](https://gitee.com/flame-ai/siger/tree/master/%E4%B8%93%E8%AE%BF) / 【一君】[如何选开源许可证](https://gitee.com/flame-ai/siger/blob/master/%E4%B8%93%E8%AE%BF/%E3%80%90%E4%B8%80%E5%90%9B%E3%80%91%E5%A6%82%E4%BD%95%E9%80%89%E5%BC%80%E6%BA%90%E8%AE%B8%E5%8F%AF%E8%AF%81%EF%BC%9F.md)？.md - 11天前

    在开发者决定开始一个开源项目时，第一件事应该是选择开源许可证。目前经开源促进协会 OSI（Open Source Initiative）认证的许可证已有近百个。通过选择许可证，开发者可以较为自由地决定自己的开源项目可以在什么条件下开放哪些许可。
    - 开源许可证内容
      - 6个常用许可证的简单区分
      - 许可证主要内容分类
    - 许可证结构概览
    - 多语言问题
    - 开源许可证变更
    - 开源与自由

    文章中最纠结的就是 自由软件 GPL 许可证后，为了兼容各种需求，叠加了众多变体，给新人带来困惑。初读问题的我，也如新人一样纠结，实际是不了解自己的目标和愿景，也就无从选择。如今有了坚定的方向和使命，自然就有了选择 —— GPL3.0. 下面是[我的选择](https://gitee.com/yuandj/siger/issues/I3DLD8#note_5899540_link)：【[笔记](https://gitee.com/yuandj/siger/issues/I3DLD8#note_5899540_link)】

    - SIGer 开源文化期刊：CC-BY-SA-4.0 知识共享，因为《大教堂与集市》
    - LuckyLudii 民族棋中国史：MIT，因为 LudiiGames 通用游戏引擎，国际棋盘游戏史数据库
    - Shiliu Pi 99 石榴派：GPL-3.0，因为 RMS GNUchess & GNUbg

- [鼓励 SIGer 编辑，与 一君 老师互动！](https://gitee.com/yuandj/siger/issues/I3DLD8#note_4855206_link) - 4个月前  
- 耀眼的阳光下发现了艳丽的花朵长了 “腻虫”，豆绿色，爬满花蕊... 这是读到《[用爱发电](https://gitee.com/yuandj/siger/issues/I3DLD8#note_4891377_link)》时的感受 - 3个月前  
这促使我要寻根究底 《[开源治理的根基是什么](https://gitee.com/yuandj/siger/issues/I3DLD8)？》

---

### [吉牛 GNU](https://images.gitee.com/uploads/images/2021/0627/214919_a8ca1e2d_5631341.png)

用这个名字是 [CNRV 演讲前制作海报](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/CNRV%202021.6.%2023.%20-%2024.md#day3-rv4kids-post)的灵感，因为 [LuckyDoku](https://gitee.com/blesschess/luckystar/tree/master/LuckyDoku) 吉祥数独的前身是 社区志愿活动 “吉牛报春” ，此时此刻，我希望 GNU 能够为我们的未来带来希望，为开源社区治理找到根本解决方案，带来好运，所以借用这个名字，也将成为，接下来我在践行自由软件精神的中国推广中的 “鲤鱼”，而且是锦鲤，两条呦。

- [GNU中文翻译团队-摘要](https://gitee.com/yuandj/siger/issues/I3DLD8#note_5109839_link) - 3个月前
  - GNU中文翻译团队-摘要：项目成员列表
    - 现役成员
    - 目前不活跃的会员
  - [最新消息](https://gitee.com/yuandj/siger/issues/I3DLD8#note_5109859_link)

    由wxie发表的支持 RMS 的信，2021年3月24日，星期三UTC-0回复
    > 亲爱的FSF，  
    > 我写这封信是为了支持Richard Stallman博士成为FSF的董事会成员。  
    > 理查德·斯托曼（Richard Stallman）博士一直是自由软件运动的有力领导者。

    - 支持RMS的信函
    - 自由软件基金会获得Charity Navigator的完美评分，并连续第八次获得四星级评级

      > 尊敬的GNU CTT：  
      > 非常感谢您过去几年的辛勤工作。  
      > 祝大家春节快乐，快乐，健康！  
      > 我们很自豪地知道：  
      > 美国马萨诸塞州波士顿-2021年2月9日-自由软件基金会...


- [Richard Stallman](https://gitee.com/yuandj/siger/issues/I3DLD8#note_5109877_link) - 3个月前  
  理查德·马修·斯托曼

  （Richard Matthew Stallman, RMS），于1953年出生，自由软件运动的精神领袖、GNU计划以及自由软件基金会（Free Software Foundation）的创立者、著名黑客。主要成就包括Emacs及后来的GNU Emacs，GNU C 编译器及GNU 除错器。他所写作的GNU通用公共许可证（GPL）是世上最广为采用的自由软件许可证，为copyleft观念开拓出一条崭新的道路。他最大的影响是为自由软件运动竖立了道德、政Zhi以及法律框架。他被许多人誉为当今自由软件的斗士、伟大的理想主义者，但同时也有人批评他过于固执、观点落伍。

  | 中文名 | 理查德·马修·斯托曼 |
  |---|---|
  | 外文名 | Richard Matthew Stallman |
  | 别名 | RMS |
  | 国籍 | 美国 |
  | 出生日期 | 1953年03月16日 |
  | 职业 | 自由职业者 |
  | 毕业院校 | 哈佛大学 |
  | 主要成就 | 创立GNU, FSF, GPL 发起自由软件运动 |
  | 代表作品 | Emacs, GCC, GDB |
  | 性别 | 男 |

- [关于 GNU](https://gitee.com/yuandj/siger/issues/I3DLD8#note_5110107_link) - 3个月前
  https://www.gnu.org/gnu/gnu.html

  ![输入图片说明](https://images.gitee.com/uploads/images/2021/0514/074641_7071c28b_5631341.png "在这里输入图片标题")

  - GNU 操作系统
    - GNU 和 Linux
      - GNU 和 Linux的关系
      - 为什么‘Linux系统’应该叫做 GNU/Linux
      - 从未听说过 GNU 的 GNU 用户
      - GNU/Linux 问答
    - 其他 GNU 相关的资源
    - GNU 在远方

      >  **(9965) GNU**    
      > 主带小行星 (9965) GNU，暂时按照 1992 EF2 设计，在 Minor Planet Circular 41571（小行星通报） 中以 GNU 工程为名。该小行星是 Spacewatch 于 1992 年 3 月 5 日在 Kitt Peak 发现的。

  ![输入图片说明](https://images.gitee.com/uploads/images/2021/0514/074823_f77ed7b2_5631341.png "在这里输入图片标题")   _“自由软件基金会（FSF）是一个非盈利组织。我们的使命是在全球范围内促进计算机用户的自由。我们捍卫所有软件用户的权利。”_ 

---

### [教育](https://gitee.com/yuandj/siger/issues/I3DLD8#note_5110108_link)

https://www.gnu.org/education/education.html  
 _[这是针对英文原版页面的中文翻译。](https://www.gnu.org/education/education.en.html)_ 

> GNU 教育团队欢迎自由软件基金会关于呼吁全球学校停止要求学生使用非自由软件的请愿。请为课堂自由签名支持该请愿。

-  **自由软件与教育** 

  - 自由软件和教育有什么关系？

    > 软件自由在教育中扮演了根基性的角色。任何层次的教育机构都应该使用和教授自由软件，因为这些是唯一能帮助他们契合教育的本质使命的软件：传播人类知识，帮助学生成为他们所在团体的优秀成员。自由软件的源代码和各种方法是人类知识的一部分。与此相反，商业软件中包含的是商业机密和受限制的知识，这违背了教育机构的使命。自由软件支持教育，商业软件妨碍教育。

    > 自由软件不仅仅是一个技术层面的问题；还是道德问题，社会问题和政问题治。它是关于一个软件用户应该拥有的权力的问题。自由和合作是自由软件的精华所在。GNU 系统实现着这些价值和分享原则，分享令人愉快的且有利于推动人类进程。

    > 欲了解更多，请访问 [自由软件定义](https://www.gnu.org/philosophy/free-sw.html) 和 [软件为什么应该是自由的](https://www.gnu.org/philosophy/shouldbefree.html) 页面。

  - 基础

    > GNU 工程是 1983 年由理查德·斯托曼 发起的，旨在开发一个自由的操作系统：GNU 操作系统。时至如今，每个人都可以在计算机上自由的使用这个系统。

    > 在这段6分钟的视频中 理查德·斯托曼 简要解释了自由软件的基本理念以及它们是如何与教育相联系的。

      ![输入图片说明](https://images.gitee.com/uploads/images/2021/0514/075608_ecd194e6_5631341.png "屏幕截图.png")

      我是Richard Stallman。25年前，我创立了自由软件运动。
      自由软件是尊重用户自由和 
      他所在社区的社会团结。非自由程序是专有的
      软件; 也就是说，它们剥夺了用户的自由，使他们
      分裂而无助。

      因禁止共享程序而划分，以及 
      无奈，因为用户没有程序的源代码。
      也就是说，他们无法更改它，他们无法找出程序是什么 
      确实在做，它可以做非常坏的事情。

      自由软件意味着用户拥有4个基本自由：

      自由0是出于任何目的运行程序的自由。

      自由1是研究程序源代码的自由，并且 
      对其进行更改以使其按您的意愿进行。

      自由2是帮助邻居的自由。就是说，
      随时创建并分发该程序的精确副本。

      自由3是为社区做出贡献的自由。那是，
      制作和分发修改后版本的副本的自由 
      该程序。

      有了这四个自由，一个程序就是自由软件，因为社交 
      其使用和分发的系统是遵守 
      每个人的自由和用户社区的自由。

      软件必须是自由的，因为我们都应享有自由，我们都应拥有 
      被允许参加一个自由社区。

      因此，学校应该只教自由软件。有四个
      之所以如此。 

      最浅层的原因是省钱。学校不够
      钱，所以他们不应该浪费他们的钱来支付使用许可 
      专有软件。这是一个明显的原因，但有些专有
      软件公司通常通过捐赠以下内容来消除这一原因： 
      向学校免费提供他们专有的程序或少量 
      价格。
      他们这样做的原因是使学生上瘾。它是
      恶意计划。这是一项计划，要利用学校作为手段强加
      永久依赖学生。
      如果学校教如何使用专有程序，则学生将 
      依赖于该计划，他毕业后，他将不得不支付 
      使用该程序。他可能会为一家没有工作的公司工作
      免费获得该程序的副本。
      因此，专有软件开发人员利用学校强加 
      对整个社会的永久依赖。学校必须拒绝
      参加这样的恶意计划，因为学校有社交活动 
      教育下一代成为一个好公民的使命 
      强大，强大，独立，合作和自由的社会。这个可以
      只有通过教自由软件才能实现。学校必须消除
      删除专有软件并安装免费软件。

      但是有一个更深层的原因：组建好的程序员，因为 
      为了学习良好的编程技能，学生需要阅读大量的 
      代码并编写大量代码。学习为大写好的代码
      程序学生需要在大代码中写一些小的变化 
      程式。所有这些只有使用免费软件才有可能。仅免费
      软件允许进行信息技术教育。

      但是，还有一个更深层的原因：要教好公民意识，因为 
      学校的使命不仅是教授事实和技术技能，
      但最重要的是善意的精神，是帮助他人的习惯。
      因此，每个班级应遵循以下规则：学生，如果您携带 
      上课，你不能自己保留它，你必须分享它 
      与班上其他同学一样。
      学校应该实践所讲的内容，只应介绍 
      教室中的免费软件。

      所有学校都应迁移到免费软件，并独家教授免费软件 
      软件，因为每所学校都应参与领导社会 
      争取自由和社会团结。

      谢谢你。

  - 较深层次

    - 了解更多有关为什么教育机构应使用自由软件的 [理由](https://www.gnu.org/education/edu-why.html)。
    - 一篇由 Richard Stallman 撰写的文章：[为什么在学校中只应使用自由软件](https://www.gnu.org/education/edu-schools.html)。
    - 一篇由 V. Sasi Kumar 博士撰写的文章，[印度的教育系统](https://www.gnu.org/education/edu-system-india.html)。

     _**我们正寻找自由的教育游戏，或者是关于可用于教育的自由游戏的信息。详情请联系**_  <education@gnu.org>

  - [Move freedom forward: Donate today and help us reach our matching goal of USD 11,000 by July 19](https://gitee.com/yuandj/siger/issues/I3DLD8#note_5880181_link)  
    [推动自由前进：今天捐赠并帮助我们在 7 月 19 日之前实现 11,000 美元的匹配目标](https://gitee.com/yuandj/siger/issues/I3DLD8#note_5880269_link)

    [今天就向 FSF 捐款！](https://gitee.com/yuandj/siger/issues/I3DLD8#note_5880292_link)
    - 加入 FSF 会员资格
    - 支持自由软件基金会
    - [捐赠](https://gitee.com/yuandj/siger/issues/I3DLD8#note_5880474_link)：

      > At 2021.6.22, I was using the LOGO of GNU and the story of FSF to encourage chinese youth to join the open source software community and launched our youth education project which is a programmable personal computer with software and hardware and I/O, named Shiliu PI. today, I'll continue to write ISSUES about FSF.

        | Total Amount | Financial Type | Received date | Status |
        |---|---|---|---|
        | $USD 6.22 | Donation | July 18th, 2021 | Completed |

        15,185 $USD - 14913 $USD 又精进了一步！  :pray: [at this time](https://gitee.com/yuandj/siger/issues/I3DLD8#note_5882694_link).

- [为什么教育机构应该使用和教授自由软件](https://gitee.com/yuandj/siger/issues/I3DLD8#note_5110131_link) [eng](https://www.gnu.org/education/edu-why.html)
- [为什么在学校中只应使用自由软件](https://gitee.com/yuandj/siger/issues/I3DLD8#note_5110157_link) [eng](https://gitee.com/yuandj/siger/issues/I3DLD8#note_5110157_link)

- [Richard Stallman's TEDx video: "Introduction to Free Software and the Liberation of Cyberspace"](https://gitee.com/yuandj/siger/issues/I3DLD8#note_5110239_link)  

  https://www.fsf.org/blogs/rms/20140407-geneva-tedx-talk-free-software-free-society/

  ![输入图片说明](https://images.gitee.com/uploads/images/2021/0514/083925_d1c70096_5631341.png "屏幕截图.png")

---

### [GNU 软件](https://gitee.com/yuandj/siger/issues/I3DLD8#note_5880560_link) - 12天前

  https://www.gnu.org/software/software.html

  GNU是一个100%由自由软件构成的操作系统。它由 Richard Stallman（rms）在1983年发起，并由许多珍视软件用户自由的人士共同协作开发。技术上，GNU大致类似Unix。但是和Unix不同，GNU给予其用户自由。

  GNU系统包括所有正式的GNU软件包（列表如下），还包括非GNU的自由软件，其中有著名的TeX和X Window系统。另外，GNU系统并非是一个单一的静态软件集合；用户和发布者可以根据他们的需要选择不同的软件包。这些仍然是GNU系统的一种变形。

  - 如何获取 GNU 软件
  - 开发GNU软件
  - 所有 GNU 软件包 （391）

    - 39. [GNU Chess](https://gitee.com/yuandj/siger/issues/I3DLD8#note_5880586_link) - [GNUchess](https://www.gnu.org/software/chess/)
    - 96. [GCC, the GNU Compiler Collection](https://gitee.com/yuandj/siger/issues/I3DLD8#note_5880685_link)

  - www.gnu.org 网页翻译指南  
    https://www.gnu.org/server/standards/README.translations.html
    - 如何参与
    - 翻译小组

    - [gnun](https://gitee.com/yuandj/siger/issues/I3DLD8#note_5880866_link) - GNUnited Nations
      https://www.gnu.org/software/gnun/

  - [GNU Mirror List](https://gitee.com/yuandj/siger/issues/I3DLD8#note_5880956_link)  
    https://www.gnu.org/prep/ftp.html

    [![输入图片说明](https://images.gitee.com/uploads/images/2021/0718/142122_91e6946f_5631341.png "在这里输入图片标题")](https://mirrors.aliyun.com/gnu/gnubg/gnubg-1_06_002-20180802-setup.exe)

      - [GPL 3.0](https://gitee.com/yuandj/siger/issues/I3DLD8#note_5881012_link) ENG - 13天前
      - [Which version would you like to install?] - 13天前(https://gitee.com/yuandj/siger/issues/I3DLD8#note_5881040_link)
        - Use universal version (works on most older CPUs)
        - Use SSE2 compatible version ( **recommended for your computer** )
      - [Setup will install GNU Backgammon into the following folder.](https://gitee.com/yuandj/siger/issues/I3DLD8#note_5881052_link)  - 13天前
        - At least 104.4MB of free disk space is required.
      - [Setup will create the program's shortcuts in the following Start Menu folder.](https://gitee.com/yuandj/siger/issues/I3DLD8#note_5881075_link) - 13天前
      - [Select Additional Tasks](https://gitee.com/yuandj/siger/issues/I3DLD8#note_5881081_link) - 13天前
        - Create a desktop icon
        - Create a desktop icon( **CLI** )
      - [Read to Install](https://gitee.com/yuandj/siger/issues/I3DLD8#note_5881090_link) - 13天前
      - [Installing...](https://gitee.com/yuandj/siger/issues/I3DLD8#note_5881102_link) - 13天前
      - [Completing...](https://gitee.com/yuandj/siger/issues/I3DLD8#note_5881105_link) - 13天前

    [![输入图片说明](https://images.gitee.com/uploads/images/2021/0718/144134_02d62681_5631341.png "在这里输入图片标题")](https://gitee.com/yuandj/siger/issues/I3DLD8#note_5881118_link)
    - https://gitee.com/yuandj/siger/issues/I3DLD8#note_5881348_link
      - [HELP](https://gitee.com/yuandj/siger/issues/I3DLD8#note_5881356_link)
        - Commands
        - GNU Backgammon
        - GNU Backgammon CLI （gray icon）
        - Manual (all about)
        - Manual (web)
        - [About GNU Backgammon](https://images.gitee.com/uploads/images/2021/0718/165523_bb019ff2_5631341.png)
          - [Credits](https://images.gitee.com/uploads/images/2021/0718/165840_1d16c8f4_5631341.png)
          - [Build Info](https://images.gitee.com/uploads/images/2021/0718/165924_6d6fec80_5631341.png)
          - Copying conditions
          - Warranty
          - [Report Bug](https://savannah.gnu.org/bugs/?func=additem&group=gnubg)
          - Evaluation Engine

    131 [gnubg](https://gitee.com/yuandj/siger/issues/I3DLD8#note_5880719_link)  
    https://www.gnu.org/software/gnubg/

     **GNU Backgammon** 
    Translations of this page
    - GNU 双陆棋简介
    - GNU 双陆棋的特点
    - 下载 GNU 双陆棋
    - 文档和其他资源

    - bing.com  - 13天前
      [how to translate the gnu software .mo files](https://gitee.com/yuandj/siger/issues/I3DLD8#note_5882349_link)
      - 10.3 GNU MO 文件的格式
      - [How to Translate Python Applications with the GNU gettext ...](https://phrase.com/blog/posts/translate-python-gnu-gettext)

    - [gnubg-release-1.06.002-sources.tar.gz](https://gitee.com/yuandj/siger/issues/I3DLD8#note_5882441_link) - 13天前
      - 8. 生成二进制 MO 文件

    - [fork 3 gnubg from github](https://gitee.com/yuandj/siger/issues/I3DLD8#note_5882616_link):  
      就 石榴派的三个棋类目标，CHESS, 双陆，数独，双陆是最符合民族棋特点的：
      - 首先它是一个历史悠久的游戏，追溯到宋辽
      - 火焰棋适配的游戏之一，就是它的变体，《登陆奥林匹斯》
      - 机器博弈属性明显，有相关赛项。
      - CLI 模式，符合RISCV 构建需求
      - WEB 移植，也符合私有云需求
      - 有 X86 WIN 版本，将成为四平台范例。

    - [TOTOL: 391 FREE SOFTWAREs.](https://gitee.com/yuandj/siger/issues/I3DLD8#note_5880577_link) - 13天前

  - [欢迎来到自由软件目录 自由软件](https://gitee.com/yuandj/siger/issues/I3DLD8#note_5882674_link) 13天前  
    https://directory.fsf.org/wiki/Main_Page

  - [硬件](https://gitee.com/yuandj/siger/issues/I3DLD8#note_5882679_link) - 13天前
    https://h-node.org/

  - [BOOK](https://images.gitee.com/uploads/images/2021/0719/191624_c4f812df_5631341.jpeg): [Free Software Free Society, 3rd Edition](https://gitee.com/yuandj/siger/issues/I3DLD8#note_5896380_link) - 12天前
    [Home](https://shop.fsf.org/) » [Books & Docs](https://shop.fsf.org/collection/books-docs) » [Free Software Free Society: Selected Essays of Richard M. Stallman, 3rd Edition](https://shop.fsf.org/books-docs/free-software-free-society-selected-essays-richard-m-stallman-3rd-edition)  **[$25.00](https://images.gitee.com/uploads/images/2021/0719/193119_1e56fd3e_5631341.jpeg)** 

---

### [Free software to use for gameplaying](https://gitee.com/yuandj/siger/issues/I3DLD8#note_6028635_link) - 2天前

https://directory.fsf.org/wiki/Category/Game

- [board-game](https://directory.fsf.org/wiki/Category/Game/board-game)
- [chess-related](https://directory.fsf.org/wiki/Category/Game/chess-related)
- [puzzle](https://directory.fsf.org/wiki/Category/Game/puzzle)

这三类作为引入目标，将先行构建验证后，再提交PR。

其他：mud，word ... 不做考虑

详细的功课 源自：
https://gitee.com/openeuler/RISC-V/issues/I41QXI?from=project-issue#note_6028542_link
https://gitee.com/openeuler/RISC-V/issues/I41QXI?from=project-issue#note_6028612_link

[1st Table](https://gitee.com/yuandj/siger/issues/I3DLD8#note_6036147_link): No.  - 1天前

- [chess-related](https://gitee.com/yuandj/siger/issues/I3DLD8#note_6028718_link) (19)
  https://directory.fsf.org/wiki/Category/Game/chess-related

- [board-game](https://gitee.com/yuandj/siger/issues/I3DLD8#note_6029215_link) (36)
  https://directory.fsf.org/wiki/Category/Game/board-game

- [puzzle](https://directory.fsf.org/wiki/Category/Game/puzzle) (49)
  https://directory.fsf.org/wiki/Category/Game/puzzle 【[issue](https://gitee.com/yuandj/siger/issues/I3DLD8#note_6033056_link)】
  - [1-10](https://gitee.com/yuandj/siger/issues/I3DLD8#note_6035709_link) - 1天前
  - [11-20](https://gitee.com/yuandj/siger/issues/I3DLD8#note_6035679_link)  - 1天前
  - [21-30](https://gitee.com/yuandj/siger/issues/I3DLD8#note_6035422_link) - 1天前
  - [31-40](https://gitee.com/yuandj/siger/issues/I3DLD8#note_6035750_link) - 1天前
  - [41-49](https://gitee.com/yuandj/siger/issues/I3DLD8#note_6035818_link) - 1天前

[对齐同名游戏](https://gitee.com/yuandj/siger/issues/I3DLD8#note_6036858_link)： - 1天前

- Antimicro | 抗微
![输入图片说明](https://images.gitee.com/uploads/images/2021/0729/204915_525ffdf8_5631341.png "屏幕截图.png")
- Arbitools | 仲裁工具
![输入图片说明](https://images.gitee.com/uploads/images/2021/0729/204923_38421f14_5631341.png "屏幕截图.png")
- Caro | 卡罗
![输入图片说明](https://images.gitee.com/uploads/images/2021/0729/204931_e23d48dd_5631341.png "屏幕截图.png")
- Dragon Go Server | 龙围棋服务器
![输入图片说明](https://images.gitee.com/uploads/images/2021/0729/204941_6f4356d1_5631341.png "屏幕截图.png")

2nd Table No.	
- chess-related	(19)	
- board-game	(33)	
- puzzle	(47)

---

### [GNU99 吉牛久久](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/Issue%2011%20-%20GNUgames%2099.md) - 1天前

[3rd Table workspace](https://gitee.com/yuandj/siger/issues/I3DLD8#note_6037923_link): - 1天前
- No.	chess-related	(19)	
- No.	board-game	(33)	
- No.	puzzle	(47)

[![输入图片说明](https://images.gitee.com/uploads/images/2021/0730/124028_93b6891f_5631341.jpeg "在这里输入图片标题")](https://gitee.com/yuandj/siger/issues/I3DLD8#note_6043728_link)

[Issue 11 - GNUgames 99.md.](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/Issue%2011%20-%20GNUgames%2099.md)  - 21小时前

[已经收录](https://gitee.com/yuandj/siger/issues/I3DLD8#note_6044023_link)！  - 20小时前
