- [SpaceX 即将获得 FAA 的星舰首次发射许可证](http://mp.weixin.qq.com/s?__biz=Mzg5NTc1NjY1MQ==&mid=2247496617&idx=1&sn=2fb32805a91670cd0f5692de9efaa5b7)？
- [SpaceX 最新纪录片《重返太空》来了](https://mp.weixin.qq.com/s?t=pages/video_detail_new&scene=1&vid=wxv_2350361541235146753&__biz=Mzg5NTc1NjY1MQ==&mid=2247488947&idx=1&sn=1c90f19d993b40803e3c139fb81a0e1e&vidsn=#wechat_redirect)
- [SpaceX完成星舰助推器31台引擎静态点火测试+多角度](https://mp.weixin.qq.com/s?t=pages/video_detail_new&scene=1&vid=wxv_2791997341720657921&__biz=Mzg5NTc1NjY1MQ==&mid=2247496338&idx=1&sn=1e37023dc7243386c7e02187584bb920&vidsn=#wechat_redirect)
- [星舰超重型助推器：精彩的新一年已向你走来](https://mp.weixin.qq.com/s?t=pages/video_detail_new&scene=1&vid=wxv_2763347645972365314&__biz=Mzg5NTc1NjY1MQ==&mid=2247496311&idx=1&sn=2ebf9732b331ca286605932c13f0ee9c&vidsn=#wechat_redirect)

<P><img width="706px" src="https://foruda.gitee.com/images/1677123068680613831/3e4a559d_5631341.jpeg" title="561SPACEXSTARSHIP.jpg"></p>

> 伴随着31台猛禽发动机的轰鸣，星舰已经获得足够的动力飞向苍穹，火星人类就要来啦。人类的摇篮是地球，是时候离开她飞向征途，这是我看过的最震撼人心的画面，干些 SPACEX 爱好者为我们分享的所有内容。400+ 分享，致敬码更人 :pray: 

回到太空则讲述了龙飞船安全载人往返空间站的故事，充满了不甘人后的傲娇，孩子们的英雄应该是我们自己。与各位老师和同学共勉! :+1: :+1: :+1: 

### [SpaceX 即将获得 FAA 的星舰首次发射许可证](http://mp.weixin.qq.com/s?__biz=Mzg5NTc1NjY1MQ==&mid=2247496617&idx=1&sn=2fb32805a91670cd0f5692de9efaa5b7)？

原创 R Starman SpaceX爱好者 2023-02-22 22:56 发表于江苏

![输入图片说明](https://foruda.gitee.com/images/1677120870784498087/c5801da6_5631341.png "屏幕截图")

- 微信号：SpaceX爱好者 SpaceX-watch
- 功能介绍：SpaceX火箭发射、太空探索技术，人类未来的命运，制造，科学，航天。本公众号以SpaceX(太空探索技术)公司为观察主题。欢迎广大感兴趣的爱好者们共同关注和探讨！

SpaceX 的一位高级主管预计，美国联邦航空管理局 (FAA) 将在“不久的将来”授予其星舰首次轨道级试飞的发射许可证。

![输入图片说明](https://foruda.gitee.com/images/1677120921589378551/c83f57bb_5631341.png "屏幕截图")

在 2023 年太空会议上，SpaceX 国家安全太空解决方案高级总监加里·亨利 (Gary Henry) 还表示，星舰仍有望最早在 3 月发射。六周前，首席执行官埃隆·马斯克 (Elon Musk) 在推特上表示，SpaceX“确实有机会在 2 月下旬发射，但补充说“极有可能在 3 月进行发射尝试。”  Henry 认为 2 月是不可能了，但 3 月可能仍然是一个可行的目标。

![输入图片说明](https://foruda.gitee.com/images/1677120930093394496/818a6cef_5631341.png "屏幕截图")

可以说，SpaceX 在 2023 年初对于星舰的首次轨道发射尝试前各方面取得了重大进展。就在上个月 SpaceX 对合体的 S24 和 B7 星舰系统进行了首次湿式彩排加注燃料，还进行了有助于验证完整发射倒计时程序的测试。

![输入图片说明](https://foruda.gitee.com/images/1677120936802099357/f8dbcda3_5631341.png "屏幕截图")

两周半后，SpaceX 试图对 B7 的所有 33 台 Raptor 2（猛禽） 发动机进行静态点火测试，但只有 31 台发动机按计划点火，全程产生了大约 3600 吨推力，马斯克称 31 台也足够升空使其送入轨道。

![输入图片说明](https://foruda.gitee.com/images/1677120944924850648/c7e1171f_5631341.png "屏幕截图")

从各方面来看，这次测试对 SpaceX 来说是一个巨大的成功。虽然测试只产生约 50% 的推力，但 B7 没有受到明显的损坏，SpaceX 也没有像之前那样拆除或更换任何猛禽发动机。这可能表明所有 33 台都几乎完好，可以留在助推器上进行星舰的首次轨道发射尝试，这本身就是一项重大成就/进展。

![输入图片说明](https://foruda.gitee.com/images/1677120952692252617/2b51ae1a_5631341.png "屏幕截图")

2 月 21 日，Gary Henry 证实，B7 和支撑其破纪录静态点火测试的发射台“状态良好”。与历史上几乎所有其他大型火箭不同的是，星舰的第一个轨道发射台没有喷水系统、火焰沟槽或推力分流器来抑制或重新定向火箭发动机可以产生的惊人能量。尽管如此，发射台正下方的平坦混凝土似乎经受住了约 3600 吨的推力和高温，但只有轻微的剥落和损坏。

![输入图片说明](https://foruda.gitee.com/images/1677120955402662303/478b1b4d_5631341.png "屏幕截图")

不过，最近 SpaceX 已经在安装一个喷水系统了，最终目的是为了发射场更能承受星舰测试和发射时带来的压力。然而，安装该系统并建设足够大的供水系统将需要数月时间，并且很可能会妨碍 3 月的发射尝试，这表明 SpaceX 的首次发射尝试将在没有它的情况下进行。

![输入图片说明](https://foruda.gitee.com/images/1677120961375423184/4dc07a91_5631341.png "屏幕截图")

然而，SpaceX 已经开始在发射台上安装防护层/板，该任务可能需要在发射尝试之前完成，并且前后改造可能需要几周时间。

![输入图片说明](https://foruda.gitee.com/images/1677120983439955558/d94e22bc_5631341.png "屏幕截图")

但有相关人士表示，如果获得 FAA 发射许可证过程中没有出现重大阻碍，星舰可能会在几周内准备好发射。

### [回到太空](https://www.bilibili.com/video/av596087523)

![输入图片说明](https://foruda.gitee.com/images/1677120830302954307/fcb2b793_5631341.png "屏幕截图")

### SpaceX 爱好者

- [SpaceX 即将获得 FAA 的星舰首次发射许可证](http://mp.weixin.qq.com/s?__biz=Mzg5NTc1NjY1MQ==&mid=2247496617&idx=1&sn=2fb32805a91670cd0f5692de9efaa5b7)
- [SpaceX 最新纪录片《重返太空》来了](https://mp.weixin.qq.com/s?t=pages/video_detail_new&scene=1&vid=wxv_2350361541235146753&__biz=Mzg5NTc1NjY1MQ==&mid=2247488947&idx=1&sn=1c90f19d993b40803e3c139fb81a0e1e&vidsn=#wechat_redirect)
- [SpaceX 载人飞船是如何运作的](https://mp.weixin.qq.com/s?t=pages/video_detail_new&scene=1&vid=wxv_2802558292102496258&__biz=Mzg5NTc1NjY1MQ==&mid=2247496503&idx=1&sn=4d99e6cde27817176f4c0e5002d7b25e&vidsn=#wechat_redirect)
- [分享图片](https://mp.weixin.qq.com/s?t=pages/image_detail&scene=1&__biz=Mzg5NTc1NjY1MQ==&mid=2247496438&idx=2&sn=c649f00b2f3b75478114ee3c29c450cf#wechat_redirect)
- [SpaceX完成星舰助推器31台引擎静态点火测试+多角度](https://mp.weixin.qq.com/s?t=pages/video_detail_new&scene=1&vid=wxv_2791997341720657921&__biz=Mzg5NTc1NjY1MQ==&mid=2247496338&idx=1&sn=1e37023dc7243386c7e02187584bb920&vidsn=#wechat_redirect)
- [星舰超重型助推器：精彩的新一年已向你走来](https://mp.weixin.qq.com/s?t=pages/video_detail_new&scene=1&vid=wxv_2763347645972365314&__biz=Mzg5NTc1NjY1MQ==&mid=2247496311&idx=1&sn=2ebf9732b331ca286605932c13f0ee9c&vidsn=#wechat_redirect)
- [分享图片](https://mp.weixin.qq.com/s?t=pages/image_detail&scene=1&__biz=Mzg5NTc1NjY1MQ==&mid=2247496256&idx=2&sn=e5cbf075fa1e59b3203193a66add9b3b#wechat_redirect)
- [分享图片](https://mp.weixin.qq.com/s?t=pages/image_detail&scene=1&__biz=Mzg5NTc1NjY1MQ==&mid=2247496179&idx=2&sn=025a3ce76c3bd868ffc29d45eddd829b#wechat_redirect)
- [SpaceX 宣布推迟今天猎鹰重型为美国太空军发射的机密任务 USSF-67，新的发射目标是北京时间1月16日6时56分（美国东部时间1月15日17时56分），天气预报 >90% 有利，届时将于佛罗里达州肯尼迪航天中心 39A 发射场升空（不出意外）。](https://mp.weixin.qq.com/s?t=pages/image_detail&scene=1&__biz=Mzg5NTc1NjY1MQ==&mid=2247496124&idx=1&sn=784bc21d0f73f9c879663ee1ffaaeb47#wechat_redirect)
- [要不再来欣赏一波 S24+B7 全栈图，均来自 SpaceX 官方拍摄分享。另，今天后台收到留言精选回复功能测试可用通知（即可回复评论其他人的留言/盖楼），你们可以试下看看有回复两字吗。](https://mp.weixin.qq.com/s?t=pages/image_detail&scene=1&__biz=Mzg5NTc1NjY1MQ==&mid=2247496046&idx=1&sn=5a46af7a13e7da850a6403f49c1c7036#wechat_redirect)
- [再来瞧瞧SpaceX星城白锈钢组合体（S24+B7），B9已被运回去（图7）。](https://mp.weixin.qq.com/s?t=pages/image_detail&scene=1&__biz=Mzg5NTc1NjY1MQ==&mid=2247496022&idx=1&sn=82ff42e4d9f8e738c2e42d12698d2f7d#wechat_redirect)
- [SpaceX 再次将大玩具星舰超重型助推器 B7（Starship Booster 7）推出到了发射台，按照马斯克昨天发推称最有可能在今年 3 月进行首次轨道级试飞，希望这是最后一次推出，不然真要旁边待命的 B9 上了。](https://mp.weixin.qq.com/s?t=pages/image_detail&scene=1&__biz=Mzg5NTc1NjY1MQ==&mid=2247495968&idx=1&sn=4de692fe19bddb07b448bd9533927444#wechat_redirect)
- [开年猎鹰重型运载火箭（Falcon Heavy）就要起飞了，今天SpaceX官推分享称已在佛州 39A 号发射场机库中待命。这将是 FH 的第 5 次发射，同样是为美国太空部队执行机密任务 USSF-67，美东时间1月12日下午 5:30。](https://mp.weixin.qq.com/s?t=pages/image_detail&scene=1&__biz=Mzg5NTc1NjY1MQ==&mid=2247495937&idx=1&sn=e66a02ba20ee2725f2b8dcb9a843e52c#wechat_redirect)
- [最近，SpaceX 星舰基地工作人员在给发射台做负载测试，可以从图中看到组装新型升降装置，技术视频可以看前几天发的。](https://mp.weixin.qq.com/s?t=pages/image_detail&scene=1&__biz=Mzg5NTc1NjY1MQ==&mid=2247495923&idx=1&sn=07623f86fa0e2b728680e481d71ccac8#wechat_redirect)
- [SpaceX星舰进行发射前，发射台要解决的核心问题是什么](https://mp.weixin.qq.com/s?t=pages/video_detail_new&scene=1&vid=wxv_2737351299666821123&__biz=Mzg5NTc1NjY1MQ==&mid=2247495815&idx=1&sn=a7db5ae6d3cc9e398b95ecc8ad88c7d8&vidsn=#wechat_redirect)
- [最近（12月28日）一箭十一飞执行了 Starlink Group 5-1（第二代星链）任务的猎鹰9号一级助推器 B1062 已由无人回收驳船 A Shortfall of Gravitas（简称ASOG）拖回卡纳维拉尔港，并准备吊运回厂区。另，也祝大家新快乐，万事如意！](https://mp.weixin.qq.com/s?t=pages/image_detail&scene=1&__biz=Mzg5NTc1NjY1MQ==&mid=2247495786&idx=1&sn=105c70400a5c47422ded26d8f901793f#wechat_redirect)
- [NASA 2022：成功的一年](https://mp.weixin.qq.com/s?t=pages/video_detail_new&scene=1&vid=wxv_2734438288648060928&__biz=Mzg5NTc1NjY1MQ==&mid=2247495786&idx=2&sn=049c0e8fca3bd307f27f66cbadb64d96&vidsn=#wechat_redirect)

![输入图片说明](https://foruda.gitee.com/images/1677121117097343799/94ef5c02_5631341.jpeg "0.jpg")![输入图片说明](https://foruda.gitee.com/images/1677121126332561198/753676a0_5631341.jpeg "0 (1).jpg")![输入图片说明](https://foruda.gitee.com/images/1677121136752977657/bd6d1aaf_5631341.jpeg "0 (2).jpg")![输入图片说明](https://foruda.gitee.com/images/1677121144915850304/43b293d4_5631341.jpeg "0 (3).jpg")![输入图片说明](https://foruda.gitee.com/images/1677121577054967123/273e1be2_5631341.jpeg "0 (22).jpg")![输入图片说明](https://foruda.gitee.com/images/1677121590038295818/1ce70505_5631341.jpeg "0 (23).jpg")![输入图片说明](https://foruda.gitee.com/images/1677121599286410650/061e31e7_5631341.jpeg "0 (24).jpg")![输入图片说明](https://foruda.gitee.com/images/1677121608091833648/23a50f3f_5631341.jpeg "0 (25).jpg")![输入图片说明](https://foruda.gitee.com/images/1677121616547805935/10b9bb0f_5631341.jpeg "0 (26).jpg")![输入图片说明](https://foruda.gitee.com/images/1677121624721392370/0dab241b_5631341.jpeg "0 (27).jpg")![输入图片说明](https://foruda.gitee.com/images/1677121633452687249/2189a0f5_5631341.jpeg "0 (28).jpg")![输入图片说明](https://foruda.gitee.com/images/1677121642225336067/738db164_5631341.jpeg "0 (29).jpg")![输入图片说明](https://foruda.gitee.com/images/1677121650446962164/c22082ea_5631341.jpeg "0 (30).jpg")![输入图片说明](https://foruda.gitee.com/images/1677121658957792998/bafa92f3_5631341.jpeg "0 (31).jpg")![输入图片说明](https://foruda.gitee.com/images/1677121668050695457/a2323405_5631341.jpeg "0 (32).jpg")![输入图片说明](https://foruda.gitee.com/images/1677121678623685275/ff55b14b_5631341.jpeg "0 (33).jpg")![输入图片说明](https://foruda.gitee.com/images/1677121686922643565/3db1b472_5631341.jpeg "0 (34).jpg")![输入图片说明](https://foruda.gitee.com/images/1677121696702066346/8a3b8611_5631341.jpeg "0 (35).jpg")![输入图片说明](https://foruda.gitee.com/images/1677121705708175491/a0e00c9e_5631341.jpeg "0 (36).jpg")![输入图片说明](https://foruda.gitee.com/images/1677121713469611355/86f368f6_5631341.jpeg "0 (37).jpg")![输入图片说明](https://foruda.gitee.com/images/1677121722521772252/a45336ec_5631341.jpeg "0 (38).jpg")![输入图片说明](https://foruda.gitee.com/images/1677121731427110695/3209c039_5631341.jpeg "0 (39).jpg")![输入图片说明](https://foruda.gitee.com/images/1677121739259286819/5e271698_5631341.jpeg "0 (40).jpg")![输入图片说明](https://foruda.gitee.com/images/1677121747436823725/08042f48_5631341.jpeg "0 (41).jpg")![输入图片说明](https://foruda.gitee.com/images/1677121755589135984/3149217c_5631341.jpeg "0 (42).jpg")![输入图片说明](https://foruda.gitee.com/images/1677121765368522903/e9dae396_5631341.jpeg "0 (43).jpg")![输入图片说明](https://foruda.gitee.com/images/1677121773873746513/58579c82_5631341.jpeg "0 (44).jpg")![输入图片说明](https://foruda.gitee.com/images/1677121783618538643/ea93303e_5631341.jpeg "0 (45).jpg")![输入图片说明](https://foruda.gitee.com/images/1677121791820949897/5c2735e0_5631341.jpeg "0 (46).jpg")![输入图片说明](https://foruda.gitee.com/images/1677121799313152301/18a8224e_5631341.jpeg "0 (47).jpg")![输入图片说明](https://foruda.gitee.com/images/1677121807196182747/8e7460de_5631341.jpeg "0 (48).jpg")![输入图片说明](https://foruda.gitee.com/images/1677121814551827476/7920e55f_5631341.jpeg "0 (49).jpg")![输入图片说明](https://foruda.gitee.com/images/1677121822418362048/108a3b4d_5631341.jpeg "0 (50).jpg")![输入图片说明](https://foruda.gitee.com/images/1677121832132116546/fbb78bbe_5631341.jpeg "0 (51).jpg")![输入图片说明](https://foruda.gitee.com/images/1677121841568505965/20233a25_5631341.jpeg "0 (52).jpg")![输入图片说明](https://foruda.gitee.com/images/1677121850779500766/93a19d30_5631341.jpeg "0 (53).jpg")