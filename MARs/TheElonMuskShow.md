通往火星之路，ROAD TO MARS : 

- [【通往火星之路】体验近距离拍摄星舰 Booster 7](https://mp.weixin.qq.com/s?t=pages/video_detail_new&scene=1&vid=wxv_2615566700474023938&__biz=Mzg5NTc1NjY1MQ==&mid=2247493792&idx=1&sn=eaab128af85bb3f25b604ab531796a2d&vidsn=#wechat_redirect)
- [星舰合体“微电影”](https://mp.weixin.qq.com/s?t=pages/video_detail_new&scene=1&vid=wxv_2619975790705000449&__biz=Mzg5NTc1NjY1MQ==&mid=2247493863&idx=1&sn=ceee911a5957f0bd313c10ed557621e3&vidsn=#wechat_redirect)
- [有点朦胧的科幻感](https://mp.weixin.qq.com/s?t=pages/video_detail_new&scene=1&vid=wxv_2628648306982010881&__biz=Mzg5NTc1NjY1MQ==&mid=2247494001&idx=2&sn=650b5dddb070330175f82d8170adeb7a&vidsn=#wechat_redirect)
- [SpaceX：星际基地上 S24、B7 合体](https://mp.weixin.qq.com/s?t=pages/video_detail_new&scene=1&vid=wxv_2631475531548737538&__biz=Mzg5NTc1NjY1MQ==&mid=2247494070&idx=2&sn=7604858b441c26295945c7b576cda47a&vidsn=#wechat_redirect)
- [SpaceX新宣传片：Life at Starbase](https://mp.weixin.qq.com/s?t=pages/video_detail_new&scene=1&vid=wxv_2651803708296806405&__biz=Mzg5NTc1NjY1MQ==&mid=2247494466&idx=1&sn=da8eebaf6bd0556892d9b24246cceb71&vidsn=#wechat_redirect)
- [载人版星舰飞向火星模拟动画](https://mp.weixin.qq.com/s?t=pages/video_detail_new&scene=1&vid=wxv_2676482305921925122&__biz=Mzg5NTc1NjY1MQ==&mid=2247494764&idx=1&sn=a8ebf1a37d26ffd2511daa71fb8f6b6b&vidsn=#wechat_redirect)

新纪录片《埃隆·马斯克秀》（中文字幕）

- [《埃隆·马斯克秀》(一)：亲人，朋友，同事讲述马斯克的故事](https://mp.weixin.qq.com/s?t=pages/video_detail_new&scene=1&vid=wxv_2624277778846466050&__biz=Mzg5NTc1NjY1MQ==&mid=2247493927&idx=1&sn=127cc7ce336eac681d900d26257e5970&vidsn=#wechat_redirect)
- [《埃隆·马斯克秀》(二)：解决问题的外星人](https://mp.weixin.qq.com/s?t=pages/video_detail_new&scene=1&vid=wxv_2631518437617713153&__biz=Mzg5NTc1NjY1MQ==&mid=2247494070&idx=1&sn=910e4e17887e63d8703792284dbd1500&vidsn=#wechat_redirect)
- [《埃隆·马斯克秀》(三)：父亲，争议，火星](https://mp.weixin.qq.com/s?t=pages/video_detail_new&scene=1&vid=wxv_2641611697736400897&__biz=Mzg5NTc1NjY1MQ==&mid=2247494263&idx=1&sn=0b83c3d921ba682d433b9a90d323c72b&vidsn=#wechat_redirect)

更多精彩，尽在 SpaceX 爱好者：

- [S25进行低温测试 壮观排气](https://mp.weixin.qq.com/s?t=pages/video_detail_new&scene=1&vid=wxv_2648594253417562113&__biz=Mzg5NTc1NjY1MQ==&mid=2247494426&idx=2&sn=a2ea783b0c7531bf02a79c95ddeab83b&vidsn=#wechat_redirect)
- [B7 14 台引擎完成静态点火测试](https://mp.weixin.qq.com/s?t=pages/video_detail_new&scene=1&vid=wxv_2666264256300204033&__biz=Mzg5NTc1NjY1MQ==&mid=2247494596&idx=1&sn=be6df4c57cf6b1e0c8a18218f1a65c70&vidsn=#wechat_redirect)
- [星舰 B7 11 台引擎完成静态点火测试](https://mp.weixin.qq.com/s?t=pages/video_detail_new&scene=1&vid=wxv_2688006049123287040&__biz=Mzg5NTc1NjY1MQ==&mid=2247494967&idx=1&sn=280d5a1cc59f36d35ced093104b054e1&vidsn=#wechat_redirect)
- [【微电影】SpaceX星舰助推器静态点火](https://mp.weixin.qq.com/s?t=pages/video_detail_new&scene=1&vid=wxv_2695259421115170822&__biz=Mzg5NTc1NjY1MQ==&mid=2247495109&idx=1&sn=598bf3bef416d8de93cdd5a4cf7f0a8e&vidsn=#wechat_redirect)
- [星舰(S24)完成单引擎静态点火测试 俯视方式观看](https://mp.weixin.qq.com/s?t=pages/video_detail_new&scene=1&vid=wxv_2711414844868280321&__biz=Mzg5NTc1NjY1MQ==&mid=2247495424&idx=2&sn=b744a07b05bfde156e2ac99542f87575&vidsn=#wechat_redirect)
- [星舰新助推器（B9）运到发射场，准备进行低温测试](https://mp.weixin.qq.com/s?t=pages/video_detail_new&scene=1&vid=wxv_2712824495312748546&__biz=Mzg5NTc1NjY1MQ==&mid=2247495455&idx=2&sn=2052842b85bd07a54b32bd7231049733&vidsn=#wechat_redirect)
- [想不出来标题，看完你想到/说什么](https://mp.weixin.qq.com/s?t=pages/video_detail_new&scene=1&vid=wxv_2725610983427424260&__biz=Mzg5NTc1NjY1MQ==&mid=2247495636&idx=2&sn=97eea6b9c4bf62a8be999704662c6b62&vidsn=#wechat_redirect)
- [星舰超重型助推器：精彩的新一年已向你走来](https://mp.weixin.qq.com/s?t=pages/video_detail_new&scene=1&vid=wxv_2763347645972365314&__biz=Mzg5NTc1NjY1MQ==&mid=2247496311&idx=1&sn=2ebf9732b331ca286605932c13f0ee9c&vidsn=#wechat_redirect)
- [SpaceX完成星舰助推器31台引擎静态点火测试 多角度](https://mp.weixin.qq.com/s?t=pages/video_detail_new&scene=1&vid=wxv_2791997341720657921&__biz=Mzg5NTc1NjY1MQ==&mid=2247496338&idx=1&sn=1e37023dc7243386c7e02187584bb920&vidsn=#wechat_redirect)
- [距离星舰轨道发射还有多远？推力和倒计时分析](https://mp.weixin.qq.com/s/_o5W3ejfYCYcJBmuVcpOYA)

星舰发射系统，深度解读：

- [为何星舰迟迟不能发射？](https://mp.weixin.qq.com/s?t=pages/video_detail_new&scene=1&vid=wxv_2709813690220691456&__biz=Mzg5NTc1NjY1MQ==&mid=2247495387&idx=1&sn=f105c4699167b4bb9bb0398069ab7a86&vidsn=#wechat_redirect)
- [SpaceX星舰进行发射前，发射台要解决的核心问题是什么](https://mp.weixin.qq.com/s?t=pages/video_detail_new&scene=1&vid=wxv_2737351299666821123&__biz=Mzg5NTc1NjY1MQ==&mid=2247495815&idx=1&sn=a7db5ae6d3cc9e398b95ecc8ad88c7d8&vidsn=#wechat_redirect)

<p><img width="706px" src="https://foruda.gitee.com/images/1677356780738078656/9ecbfe6c_5631341.jpeg" title="564ROADTOMARS.jpg"></p>

> 我知道为什么我喜欢 SPACEX 爱好者了，就在之前也出过一片纯摄影作品的专题，没有过多辞藻，就是凝视着大火箭，它能满足我们所有的想象，起飞，火星之路。又是连看三级。

# [2023年打算建造多少艘星舰？马斯克：大概5艘+完整堆栈](https://mp.weixin.qq.com/s?__biz=Mzg5NTc1NjY1MQ==&mid=2247496124&idx=2&sn=c57daf2d0d9669bb8bdc86b63f149bcf)

SpaceX爱好者 2023-01-15 21:43 发表于江苏

![输入图片说明](https://foruda.gitee.com/images/1677345955626715893/f39fc449_5631341.png "屏幕截图")

- 微信号：SpaceX爱好者 SpaceX-watch
- 功能介绍：SpaceX火箭发射、太空探索技术，人类未来的命运，制造，科学，航天。本公众号以SpaceX(太空探索技术)公司为观察主题。欢迎广大感兴趣的爱好者们共同关注和探讨！

今天，就 SpaceX 与美国国家科学基金会签署协议，防止 Starlink（星链）干扰天文学报道一文。马斯克评论称：拥有巨大货舱的星舰/星际飞船（Starship）将为天文学提供令人难以置信的太空望远镜。

![输入图片说明](https://foruda.gitee.com/images/1677345985580333732/375132f0_5631341.png "屏幕截图")

目前，SpaceX 为 NASA 执行第 26 次国际空间站商业补给任务 CRS-26 的货运龙飞船已被运回。

![输入图片说明](https://foruda.gitee.com/images/1677346084061152834/fdd0e807_5631341.png "屏幕截图")

另外，硅谷特斯拉车主俱乐部询问马斯克（经常有互动），今年，SpaceX 打算要建造多少艘星舰时。

![输入图片说明](https://foruda.gitee.com/images/1677345996005293681/1c0e05b1_5631341.png "屏幕截图")

他则回复说：大概 5 艘完整堆栈（星舰系统，即飞船+超重型助推器合体）。

# [马斯克：人类极有可能10年内登陆火星，最快5年，NASA局长：SpaceX的成功就是NASA的成功](https://mp.weixin.qq.com/s?__biz=Mzg5NTc1NjY1MQ==&mid=2247496369&idx=2&sn=52f7cf7c8fde51f2dac091451066531c)

原创 R Starman SpaceX爱好者 2023-02-11 21:21 发表于江苏

![输入图片说明](https://foruda.gitee.com/images/1677346195953315069/257a8a85_5631341.png "屏幕截图")

- 微信号：SpaceX爱好者 SpaceX-watch
- 功能介绍：SpaceX火箭发射、太空探索技术，人类未来的命运，制造，科学，航天。本公众号以SpaceX(太空探索技术)公司为观察主题。欢迎广大感兴趣的爱好者们共同关注和探讨！

昨天，在 B7+31 台引擎静态点火成功后，马斯克发推说："总有一天，星际飞船会将带我们去往火星。"随后就有人发问，那么人类何时能登上火星呐，马斯克则回复说：“必须承认我天生乐观（否则 SpaceX 和特斯拉就不会存在）；但我认为，5 年是有可能的，10 年是极有可能的。”

![输入图片说明](https://foruda.gitee.com/images/1677346278987055705/c5799f8e_5631341.png "屏幕截图")

说到 B7，以下我们来简单回顾下它的 4 次静态点火测试对比：1引擎，3引擎，14引擎，31引擎。说实话，就算不能由它进行轨道级试飞，也是来回推出并返厂最频繁、折腾最多、呆的最长久的（快一年了）、也可谓是劳苦功高了。

![输入图片说明](https://foruda.gitee.com/images/1677346392285150488/3e682012_5631341.png "屏幕截图")

另外，在隔了一天后，现任 NASA 局长比尔・纳尔逊 (Bill Nelson)今天发来了贺电。首先，他祝贺了 SpaceX 通过在昨天对星舰超重型助推器静态点火测试向前迈出了一大步，星舰是 NASA 月球到火星架构（计划）中不可或缺的一部分，并帮助我们将宇航员送上月球。还称 SpaceX 的成功就是 NASA 的成功，就是世界的成功。

![输入图片说明](https://foruda.gitee.com/images/1677346317307316855/a6dbca81_5631341.png "屏幕截图")

马斯克：我不说话，就给你默默转个推吧。图片