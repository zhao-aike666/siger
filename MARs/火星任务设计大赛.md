- [2022 Mars Society 国际火星大会海报公布！Searching for Life with Heavy Lift](https://mp.weixin.qq.com/s?__biz=MzI4MDA2OTM1Mg==&mid=2247534920&idx=1&sn=e55f0db273527994df48701f597395ed)
- [大咖云集！附线上课程链接！国际火星学会高中生火星任务设计大赛暑期课程圆满结束！](https://mp.weixin.qq.com/s?__biz=MzI4MDA2OTM1Mg==&mid=2247534940&idx=1&sn=094973978b26625d1e6206237b5eb905)
- [想在火星上种植植物吗?高中生项目发现了《红色星球》的两个窍门](https://mp.weixin.qq.com/s?__biz=MzI4MDA2OTM1Mg==&mid=2247535026&idx=2&sn=482841abee879802468a2cef27600578)

<p><img width="706px" src="https://foruda.gitee.com/images/1661148055765725314/247mars2022poster.jpeg" title="247MARS2022POSTER.jpg"></p>

> 中学生设计火星任务的可能性就在我们面前，可以将火星任务作为 SIGer 火星频道的常设任务，予以推荐，以期在不久的将来，中国团队站在世界的舞台上展示我们的聪明才智和时代担当！:pray:

# [2022 Mars Society 国际火星大会海报公布！Searching for Life with Heavy Lift](https://mp.weixin.qq.com/s?__biz=MzI4MDA2OTM1Mg==&mid=2247534920&idx=1&sn=e55f0db273527994df48701f597395ed)

火星学会 火星学会MarsSociety 2022-08-16 07:48 发表于上海

![输入图片说明](https://foruda.gitee.com/images/1661146644561214886/屏幕截图.png "屏幕截图.png")

- 微信号: 火星学会MarsSociety marssociety
- 功能介绍: 世界上最大和最有影响力的太空倡导组织，致力于促进人类探索和移居火星。主席罗伯特祖柏林《赶往火星》中的原位燃料制造深远影响了NASA和SpaceX。伊隆马斯克，詹姆斯卡梅隆等名人都曾加入。火星学会的指导委员会成员包括登月宇航员巴兹·奥尔德林。

![输入图片说明](https://foruda.gitee.com/images/1661146681665391055/屏幕截图.png "屏幕截图.png")

![输入图片说明](https://foruda.gitee.com/images/1661146695471905377/屏幕截图.png "屏幕截图.png")

火星学会非常高兴地宣布，Antonella Berti Benza赢得了2022年火星学会海报大赛的第一名!

Benza女士是阿根廷Córdoba的居民，她在Nellabe艺术学院从事艺术指导、平面设计和插图工作。参加今年设计比赛的平面艺术家被要求提交一份海报，主题是“用重型航天器寻找火星生命”，作为整体构图的一部分。

 **目前的重型航天飞行方案包括NASA的SLS和SpaceX的Starship，两者都能够将大型有效载荷送入轨道，前往月球和火星。这种能力为对这颗红色星球进行广泛的机器人和人类探索任务打开了大门，包括扩大对火星生命的搜索。** 

Benza女士的获奖海报将在火星学会的网站和各种社交媒体平台上自豪地展示，这是我们为10月20-23日在亚利桑那州立大学举行的第25届国际火星学会年会所做的宣传工作的一部分。比赛组织者也向参加今年比赛的其他有创意的参赛者表示祝贺。2022年加入我们的绝对是一群才华横溢的艺术家和设计师。

关于即将召开的国际火星学会大会的更多信息，包括注册细节和志愿者和赞助机会，请访问我们的网站(www.marssociety.org)。

请注意，提前售票截止到8月31日(周三)。

中国区会员报名参会/Speaker请联系火星喵！

# [大咖云集！附线上课程链接！国际火星学会高中生火星任务设计大赛暑期课程圆满结束！](https://mp.weixin.qq.com/s?__biz=MzI4MDA2OTM1Mg==&mid=2247534940&idx=1&sn=094973978b26625d1e6206237b5eb905)

原创 火星学会 火星学会MarsSociety 2022-08-17 23:29 发表于四川

![输入图片说明](https://foruda.gitee.com/images/1661146992156827005/屏幕截图.png "屏幕截图.png")


今年夏天，国际火星学会(Mars Society）效仿世界上一些最好的大学的 **工程设计课程** ，为来自 **世界各地的40名国际高中生** 开展了一项真正具有 **开创性的教育项目** 。

![输入图片说明](https://foruda.gitee.com/images/1661147051903645099/屏幕截图.png "屏幕截图.png")

详情👉：[官方 | 国际高中生火星任务设计大赛！](http://mp.weixin.qq.com/s?__biz=MzI4MDA2OTM1Mg==&mid=2247530823&idx=1&sn=9ab05ccd54ee56bd0d7197c7ba9e5b22)

Guest lecturers included:

![输入图片说明](https://foruda.gitee.com/images/1661147089116319766/屏幕截图.png "屏幕截图.png")

火星学会通过我们的YouTube频道向公众免费提供了几乎所有这些教育讲座：  
link： https://www.youtube.com/playlist?list=PLn0lnGc1Saikn3jsiegYa_cShVkeW5Gf0

-  **Dr. James Bell** , principal investigator of the Mastcam-Z camera system on NASA’s Perseverance rover, who shared the latest techniques and approaches for exploring Mars with rover-based science instruments.詹姆斯·贝尔博士是美国宇航局 **NASA“毅力”号火星车** Mastcam-Z摄像系统的首席研究员，他分享了用火星车上的科学仪器探索火星的最新技术和方法。

-  **Dr. Steven Benner** , an expert microbiologist who covered the nature of DNA and RNA and how to detect life signatures.史蒂文·本纳博士是一位微生物学家，他研究了 **DNA和RNA的本质以及如何检测生命特征** 。

-  **Dr. Carol Stoker** , a NASA Ames scientist who discussed concepts and approaches for searching for life on Mars, as well as past attempts.卡罗尔·斯托克博士是美国宇航局NASA艾姆斯项目的科学家，她讨论了 **在火星上寻找生命的概念和方法，以及过去的尝试** 。

-  **Dr. Chris McKay** , a NASA Ames researcher who covered his work in searching for life on the Red Planet and in extreme environments on Earth.克里斯·麦凯博士是 **美国宇航局NASA** 艾姆斯研究所的研究员，他分享了自己 **在火星和地球极端环境中寻找生命的工作** 。

-  **Homer Hickam** , the author of “ **Rocket Boys** ” (which was made into the movie “October Sky”), who shared his experiences with rocketry as a high schooler in the 1950s and as a NASA employee during the Space Shuttle era in support of astronaut training.荷马·希卡姆， **《火箭男孩》(曾被拍成电影《十月天空》)的作者** ，他分享了自己在上世纪50年代上高中时与火箭有关的经历， **以及在航天飞机时代作为NASA员工支持宇航员培训的经历** 。

-  **Dr. Lawrence Kuznetz** , a former Apollo mission controller who is also an expert on lunar & Mars surface EVA suits.劳伦斯·库兹涅兹博士，前 **阿波罗任务控制员** ，也是 **月球和火星表面EVA太空服的专家** 。

-  **Dr. Greg Autry** , a space policy expert with Arizona State University who provided a professional roadmap for students interested in pursuing space careers.格雷格·奥特里(Greg Autry)博士是亚利桑那州立大学的太空政策专家，他 **为有兴趣从事太空事业的学生提供了一份职业路线图** 。

-  **Dr. Reut Sorek Abramovich** , founder of the D-MARS analog program based in Israel, who is an expert on field science and astrobiology.罗伊特·索雷克·阿布拉莫维奇博士是 **以色列D-MARS模拟项目的创始人** ，他是现场科学和天体生物学方面的专家。

-  **Dr. Geoffrey Landis** , NASA scientist and science fiction author, who discussed his work on NASA’s Mars Pathfinder and Mars Polar Lander missions.杰弗里·兰迪斯博士， **美国宇航局科学家和科幻小说作家** ，他谈到了他在NASA火星探路者和 **火星极地登陆任务中** 的工作。

-  **Dr. Madhu Thangavelu** , an expert on lunar exploration from the University of Southern California.南加州大学的 **月球探索专家** Madhu Thangavelu博士

-  **Dr. David Poston** ,  formerly with Los Alamos National Laboratory and an expert on the use of nuclear power in space.大卫·波斯顿博士，曾就职于洛斯阿拉莫斯国家实验室，是 **太空核能利用方面的专家** 。

-  **Dr. Nathaniel Putzig** , a scientist from the Planetary Science Institute and expert on mapping water ice on Mars.纳撒尼尔·普齐格博士是 **行星科学** 研究所的科学家，也是 **绘制火星水冰地图的专家** 。

-  **Ashwini Ramesh** , an aerospace engineer and educator, who has researched the approaches to grow and create all types of food on Mars.航空航天工程师兼教育家阿什维尼·拉梅什(Ashwini Ramesh)研究了在 **火星上种植和创造各种食物的方法** 。

-  **Dr. Ken Brandt** , a NASA Solar System Ambassador, who covered past failures and successes of Mars missions as well as landing site selection criteria.肯·勃兰特博士是 **美国宇航局的太阳系大使** ，他讲述了过去火星任务的失败和成功，以及着陆点的选择标准。

-  **Caleb Eastman** , an engineering expert, who focuses on both hardware and software robotics technologies.专注于 **硬件和软件机器人技术的工程专家** Caleb Eastman。

为期六周的虚拟项目于7月5日开始，将于8月中旬结束。 **火星学会希望通过展示一种新的、比目前更有创造性的方法在中学水平上教授科学和工程的价值，来创造教育史** 。该课程的会员学费为象征性的50美元，使所有经济水平的学生都可以参加，而该项目的全球性质突出了 **国际高中生群体的多样化和多元文化** 。在这个项目中， **学生们被鼓励使用任何他们最熟悉的工具和方法进行交流和合作。根据每个学生居住的时区，以及他们自己报告的理想工作时间，他们被分成了五个单独的小组。在世界上一些最杰出的火星专家通过Zoom视频会议就各种各样的主题提供了背景讲座之后** ，

![输入图片说明](https://foruda.gitee.com/images/1661147569966119705/屏幕截图.png "屏幕截图.png")

 **与会者目前正在努力制定他们自己独特的首次人类对火星的探索任务** 。其中包括如何在 **火星上寻找生命，如何设计宇航服和其他关键任务元素，以及火星探索背后的科学和技术** 。

高中生们设计他们的火星表面任务，包括它的 **栖息地、表面车辆、科学仪器、电力系统** 和其他 **设备和补给** 、 **人员规模** 和 **组成、任务地点、科学目标、口粮、持续时间和探索计划** 。他们有30吨的有效载荷，已经运送到火星表面，再加上一个上升飞行器，能够将多达6名宇航员从火星返回地球。

 **学生们还在创建任务设计元素时考虑到四个主要因素** : **科学、工程、人类操作挑战和成本** 。优化其中任何一项都必然会与其他内容发生冲突。例如，更多的科学设备将增加重量，这将减少用于额外探索范围的质量或牺牲支持设备的数量，而做得更好几乎总是会增加成本。因此，除了尽可能地处理好自己的领域之外，学生们还必须努力找出可能的最佳折衷方案，以获得最佳的整体结果。

每个团队的设计将写在一个报告中，每个团队成员负责编写一个部分。此外，八位导师也协助和支持学生的设计工作和演讲准备活动。

在几周内，五个团队将以口头、视觉和书面形式向一个专家评审团展示他们的任务设计元素，评审团将根据他们的技术和科学价值以及他们如何解决这四个主要因素对他们的设计进行评分。

![输入图片说明](https://foruda.gitee.com/images/1661147671923962722/屏幕截图.png "屏幕截图.png")

 **The Mars mission designs**  的设计将 **由火星学会出版，并在亚马逊网站上出售** 。 **参加设计活动的学生将获得证书，证明他们的参与，这对增加他们的大学入学申请很有用** 。此外， **获胜的设计团队将受邀于今年10月在亚利桑那州立大学举办的2022年国际火星学会大会上展示他们的方案** 。

国际火星任务计划将于8月中旬结束，火星学会希望在明年续办甚至扩大这个比赛计划！了解更多：官方 | 国际火星学会宣布火星科考机器人设计大赛！

# [想在火星上种植植物吗?高中生项目发现了《红色星球》的两个窍门](https://mp.weixin.qq.com/s?__biz=MzI4MDA2OTM1Mg==&mid=2247535026&idx=2&sn=482841abee879802468a2cef27600578)

原创 火星胡萝卜 火星学会MarsSociety 2022-08-19 17:29 发表于四川

![](https://foruda.gitee.com/images/1661147822832596742/屏幕截图.png "屏幕截图.png")

-  **微信号** : 火星学会MarsSociety marssociety
-  **功能介绍** : 世界上最大和最有影响力的太空倡导组织，致力于促进人类探索和移居火星。主席罗伯特祖柏林《赶往火星》中的原位燃料制造深远影响了NASA和SpaceX。伊隆马斯克，詹姆斯卡梅隆等名人都曾加入。火星学会的指导委员会成员包括登月宇航员巴兹·奥尔德林。

![输入图片说明](https://foruda.gitee.com/images/1661147870873126641/屏幕截图.png "屏幕截图.png")

三种萝卜在不同水类型的富苜蓿火星模拟土壤中生长。Image credit: Kasiviswanathan et al., 2022, PLOS ONE, CC-BY 4.0 

火星的土壤和水通常对作物来说太苛刻了。

一名高中二年级学生领导的研究发现：苜蓿植物和光合细菌可能有助于使火星的土壤和水适合种植。（(https://creativecommons.org/licenses/by/4.0/）

考虑到从地球发射任何东西到这颗红色星球的高昂成本，在火星上为宇航员提供食物将是任何长期人类任务的主要挑战。长期以来，科学家们一直在寻找在火星上种植农作物的方法，但火星土壤中大多数植物生长所需的有机营养物质很缺乏，而且 **火星上的水含盐量极高** 。

在这项新研究中， **研究人员研究了充分利用火星土壤和水的方法** 。过去火星上的火山活动意味着火星土壤主要是破碎的火山岩，所以科学家们用从五金商店和壁炉商店买来的火山岩进行了实验。领导这项研究的是普贾·卡西维斯瓦纳坦，她在爱荷华州艾姆斯高中读大二时就开始了这个项目。

![输入图片说明](https://foruda.gitee.com/images/1661147918918829181/屏幕截图.png "屏幕截图.png")

![输入图片说明](https://foruda.gitee.com/images/1661147925997927918/屏幕截图.png "屏幕截图.png")

![输入图片说明](https://foruda.gitee.com/images/1661147937545385114/屏幕截图.png "屏幕截图.png")

Kasiviswanathan说:“在成长过程中，我一直在想地外环境中是否存在生命的可能性，这让我对天体生物学产生了强烈的热情。”研究人员发现，通常作为牲畜干草收割的苜蓿，在这片贫瘠的土壤上生长得很好。此外，当科学家们碾碎这些苜蓿植物时，产生的粉末可以作为肥料，帮助萝卜、萝卜和生菜在贫瘠的火星土壤中生长。研究报告的合著者、爱荷华州立大学艾姆斯分校的生物地球化学家伊丽莎白·斯万纳说:“我觉得最令人惊讶的是， **我们能够在模拟的火星风化层上种植苜蓿，而没有进行营养修改** 。”“这是有希望的，因为苜蓿可以用来给风化层施肥，并种植通常不会在这种材料中生长的食用植物。”

在火星上种植作物是这位金球奖得主面临的主要挑战之一，2015年雨果奖获奖电影《火星救援》也激发了这项研究的灵感。Kasiviswanathan现在是爱荷华州立大学艾姆斯分校的一名大三学生，他说:“这部电影让我更加好奇，我们如何能够制定策略，在火星条件下种植植物，以帮助未来的人类火星任务。”

科学家们还发现， **在地球上的海水生物脱盐工厂中经常使用的一种名为聚球菌(Synechococcus sp. PCC 7002)的海洋光合细菌** ，可以像火星那样有效地 **去除海水中的盐分** 。研究人员甚至可以通过在这颗红色星球上发现的火山岩过滤暴露在细菌中的水，从而提高海水淡化的效果。卡西维斯瓦纳坦说:“我希望我们的发现可以支持美国宇航局在不久的将来进行的火星任务的研究。”然而，尽管粮食作物确实在肥沃的土壤中生长，但结果仍然与陆地作物不匹配，因为科学家们种植的萝卜、萝卜和生菜营养价值不高。斯旺纳说:“这些食物当然能为人类提供必要的维生素和矿物质，尽管它们可能提供不了太多的卡路里密度。”“我认为这项研究表明， **利用苜蓿进行生物施肥是可能的** ，未来应该研究其他植物性食物的生长。”

此外，虽然土壤和水模拟了火星，但空气却不太匹配。研究人员确实尝试在类似火星的富含二氧化碳的大气中进行这些实验，但事实证明，在密封的环境中种植植物非常困难。“这对预算有限的我们来说是一个挑战，但肯定也是研究的下一步，”斯旺纳说。科学家们在8月17日的《公共科学图书馆·综合》杂志上详细介绍了他们的发现（https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0272209）