# [兆松科技三剑客再聚，RISC-V中国峰会见证行业创新与发展](https://gitee.com/RV4Kids/iEDA/issues/I7VS3J)

2023 RISC-V 中国峰会来到了 DAY2，峰会依照议程有条不紊的进行，反响热烈。不只是超级网友线下见面会，更是行业交流的顶级盛会。作为本次峰会唯一的青少年开源社区的代表，SIGer 开源科普也带去了自己的展陈作品，YKriscv 使用 PYTHON 开发的 RISC-V 模拟器，在三层技术交流区获得一席，期待获得社区的拥抱。当日峰会日程还未正式开始，SIGer 就迎来了第一位嘉宾，志愿者热情前迎介绍本次展陈的内容，最后驻足在了 Poster 海报前，并对我们的科创少年 YKriscv 的作者 赞赏有加，他就是 兆松科技的创始人 Tunghwa Wang。

本次峰会兆松科技共有1个主会分享，两个 POSTER ，主题如下：

| 时间 | 标题 | 演讲者 | 形式 |
|---|---|---|---|
| 24日 12:10-12:20 | RISC—V敏捷芯片开发·困境和解药 | 伍华林 | 主会 |
| 24日 上午 # 9 | RISC—V SoC性能分析平台一让你的架构探索更轻松 | 雷依钒 | poster |
| # 11 | 高性能RVV自动向量化编译器 | 晏明 | poster |

而他们三位在 OSDT开源开发工具大会2021 线上会议，代表兆松科技，就有分享，主题依次为：

- 伍华林 - [Accelerate the design of heterogenous SoCs](https://www.bilibili.com/video/BV1FR4y1W7SQ/) - 202112118
- 雷依钒 - [深度剖析Gem5 - 202112118 - 第13届开源开发工具大会](https://www.bilibili.com/video/BV1mZ4y1Q7SF/)（[OSDTConf2021](https://space.bilibili.com/296494084/channel/collectiondetail?sid=78954)）
- 晏明 - [RISC-V向量扩展指令架构及LLVM自动向量化支持](https://www.bilibili.com/video/BV1bq4y1B7aV/) - 202112118

兆松科技的三位代表再次齐聚北京，不仅见证了RISC-V领域的创新与发展，同时也代表公司向世界展示了他们在RISC-V生态系统中的独特贡献。他们三位在RISC-V技术社区有相当多的分享，本文就引用了三位代表在OSDT开源开发工具大会2021上的精彩发言，并[转载了LLVM自动向量化支持](兆松科技：RISC-V%20VECTOR自动向量化及在LLVM中的实现.md "RISC-V VECTOR自动向量化及在LLVM中的实现")的相关内容。

与此同时，兆松科技的技术创新也从另一个角度赢得了业界的认可。[他们开发的RISC-V zcc 工具链 可以实现性能的2位数提升](兆松科技%20ZCC%20工具链全面支持%20ANDES%20晶心科技%20RISC-V%20处理器.md "兆松科技 ZCC 工具链全面支持 ANDES 晶心科技 RISC-V 处理器")，为异构SoC的设计和验证提供了重要支持。兆松科技专注于提供高质量的RVV自动向量化能力，通过优化底层函数库并降低优化门槛，为开发者提供了更轻松的开发环境和更高效的应用性能。

在参加RISC-V中国峰会前不久，兆松科技的成功融资也将推动他们在RISC-V生态系统中的进一步发展。这次融资得到了领投方珞珈梧桐以及跟投方普朗克创投基金和楚商领先创投基金的共同支持。这一资金注入将为兆松科技的研发项目和客户支持服务提供强大的财务支持，进一步巩固他们在RISC-V领域的领导地位。

峰会花絮：兆松科技的一位用户就是清华大学的一个图形卡项目，他们在峰会的主题演讲中就对兆松科技表示了感谢。这个图形卡项目正是基于RISC-V架构开发，并且取得了显著的成果。这样的用户反馈更加凸显了兆松科技在RISC-V生态系统中的重要地位和影响力。

本文润笔时，峰会还未结束，一个同期活动《[第二届硬件敏捷开发与验证方法学研讨会](第二届硬件敏捷开发与验证方法学研讨会.md "【达坦科技】2023 中国峰会 同期活动（线上）")》正在进行，兆松科技CTO的伍华林就是首位演讲嘉宾《[RISC-V敏捷开发-软件定义芯片](https://www.bilibili.com/video/BV1Qh4y1K7pH/ "b站：BV1Qh4y1K7pH 达坦科技")》，积极参加各种技术社区的活动，分享兆松科技的创新和实力，是与他们在RISC-V生态中的领导地位相匹配的，相信未来的发展空间更大，发展动力更强。

<p align="right"><a href="../README.md" target="_blank">sigRISCV.com</a> 青少年视角的 RISCV 兴趣社区<br>SIGer 青少年开源科普<br>袁德俊 2023.8.28</p>

<p><img width="31.69%" src="https://foruda.gitee.com/images/1693222452976436001/cd5d77bf_5631341.jpeg" title="0G3A6903-opq561005388.jpg"> <img width="31.69%" src="https://foruda.gitee.com/images/1693222570347083164/0fc25f7a_5631341.jpeg" title="CCTV7421-opq561023649.jpg"> <img width="31.69%" src="https://foruda.gitee.com/images/1693222825144247911/7c785f85_5631341.jpeg" title="0G3A6903-opq561005388UUU.jpg"> <img width="31.69%" src="https://foruda.gitee.com/images/1693222537552568115/1c6e127c_5631341.jpeg" title="CCTV7481-opq561027075.jpg"> <img width="31.69%" src="https://foruda.gitee.com/images/1693222420321772703/f6391b02_5631341.jpeg" title="CCTV7410-opq561023113.jpg"> <img width="31.69%" src="https://foruda.gitee.com/images/1693222553950742646/fb443ac5_5631341.jpeg" title="CCTV7469-opq561026347.jpg"></p>

> 24日 12:10-12:20 RISC—V敏捷芯片开发·困境和解药 伍华林 ([主会场照片直播](https://as.alltuu.com/album/1130314542/1563251646/))