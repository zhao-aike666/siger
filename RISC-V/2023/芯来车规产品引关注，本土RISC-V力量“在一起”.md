# [RV中国峰会：芯来车规产品引关注，本土RISC-V力量“在一起”](https://mp.weixin.qq.com/s?__biz=MzUxNDkyNzYxNA==&mid=2247499018&idx=1&sn=eaaba39f2e61af5d346dde77c389b866)

芯来科技 芯来科技 2023-08-25 17:00 发表于北京

![输入图片说明](https://foruda.gitee.com/images/1693150663752435568/5e65feb2_5631341.png "屏幕截图")

- 微信号: 芯来科技 nucleisys
- 功能介绍: 芯来科技致力于研发基于RISC-V指令架构的国产自主可控处理器内核IP，大幅降低国内处理器内核的使用成本，赋能本土产业生态。芯来的处理器IP已授权众多知名芯片公司进行量产，实测结果达到业界一流指标。

![输入图片说明](https://foruda.gitee.com/images/1693150695875624581/164894a2_5631341.png "屏幕截图")

> 2023年8月23日-8月25日， **第三届RISC-V中国峰会及同期活动圆满落幕** 。本次峰会采用了 **主论坛+主题报告+展览展示+同期活动+Poster形式** ，涵盖100余个主题报告、16场同期活动、18个企业展位、16个社区展位、64个poster，以及31个poster预约交流，报名人数超过了2000人，这也成为RISC—V峰会历史上规模最大、内容最丰富、影响最广泛的一届峰会。

峰会举行期间，在展览会场A2展位， **芯来技术和市场团队** 对产品和方案进行了现场展示与介绍，与业界各方进行了深入交流。 **RISC-V国际基金会首席执行官Calista Redmond** 莅临芯来科技展台参观，与芯来科技团队进行了友好互动交流。

<p><img width="49%" src="https://foruda.gitee.com/images/1693150777676137526/5098b734_5631341.png"> <img width="49%" src="https://foruda.gitee.com/images/1693150773820072578/d8bc07b4_5631341.png"></p>

### RISC-V新技术、新成果、新产品发布仪式

![输入图片说明](https://foruda.gitee.com/images/1693150847884854895/b985a09b_5631341.png "屏幕截图")

在峰会首日“RISC-V新技术、新成果、新产品发布”环节， **芯来发布的全球首个通过ISO26262 ASIL D产品认证的RISC-V CPU IP——NA900再次亮相。** 参会人员共同见证了包括芯来NA900在内的共12项RISC-V新成果的发布。这些新技术、新成果和新产品对进一步推动我国RISC-V生态与产品应用发展具有重大意义，也将为全球芯片产业快速发展注入新的动力。

### RISC-V全面开启关键计算领域的新时代

![输入图片说明](https://foruda.gitee.com/images/1693150860785934376/2bf47571_5631341.png "屏幕截图")

首日上午， **芯来科技CEO彭剑英** 进行了《 **RISC-V全面开启关键计算领域的新时代** 》的主题分享。本次分享向大家介绍了RISC-V近年来的发展情况以及RISC-V在国内集成电路领域的生态进展，结合目前全球RISC-V的技术和生态情况，深挖在 **关键计算领域所面临的机遇与挑战** 。同时借此机会向公众介绍我们刚刚发布的RISC-V CPU IP产品-NA900。最终希望芯来科技能在未来RISC-V的发展道路上，与各中国本土RISC-V力量“ **在一起** ”共同开创国产化应用新时代。

![输入图片说明](https://foruda.gitee.com/images/1693151042903566888/f3baf34f_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1693151084287541252/8279d82b_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1693151113669159567/2b995439_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1693151195598091287/59874ec3_5631341.png "屏幕截图")

在24日峰会会议中，芯来科技的两位会议报告人分别从 **RISC-V V/P软硬一体优化、功能安全标准下RISC-V内核开发** 等方面详述了芯来一年来的技术演进历程与研究成果。

### 芯来科技AE总监 胡进 RISC-V V/P扩展软硬一体优化提升应用性能

![输入图片说明](https://foruda.gitee.com/images/1693151249596440249/f6516fb1_5631341.png "屏幕截图")

 **芯来科技AE总监胡进** 重点介绍了Nuclei VPU的优势特点及配置功能。以芯来900系列为切入点，向大家说明了强大的算力以及灵活的可配置选项。并展示了NUCLEI VPU软硬优化结果和性能提升案例。

![输入图片说明](https://foruda.gitee.com/images/1693151262249127179/e7faee56_5631341.png "屏幕截图")

### 芯来科技车规功能安全经理 范添彬 满足ISO 26262 ASIL-B&D的RISC-V CPU内核开发

![输入图片说明](https://foruda.gitee.com/images/1693151285056554294/a83ce131_5631341.png "屏幕截图")

 **芯来科技车规功能安全经理范添彬** 向大家介绍芯来科技前瞻性布局车用功能安全处理器领域，经过近2年努力，最终NA系列两款不同级别的CPU IP（NA900和NA300）顺利获得了最高汽车功能安全等级ISO 26262 ASIL-D的认证证书，ASIL B的方案也在头部客户实施和落地实现了在车规的方案全覆盖。同时分享了在汽车客户RISC V CPU IP的解决方案。

![输入图片说明](https://foruda.gitee.com/images/1693151306834740733/8f0c36d6_5631341.png "屏幕截图")

---

同时，在会场三层绿宝厅POSTER交流分享环节， **芯来科技软件总监方华启** 从基于RISC-V架构的嵌入式AI，开源调试器和可信执行环境三个方面为大家进行了技术展示。

![输入图片说明](https://foruda.gitee.com/images/1693151339136687309/04fe670e_5631341.png "屏幕截图")

对相关内容感兴趣的朋友可以阅读以下电子版本了解详情，也欢迎各位提供宝贵建议！

![输入图片说明](https://foruda.gitee.com/images/1693151352953419370/54c2ca8a_5631341.png "屏幕截图")

感谢各位前来芯来展位与我们交流探讨，未来芯来科技也将携手产业链上下伙伴，继续努力， **“在一起”合作助力集成电路设计设计产业的蓬勃发展！** 

### 关于芯来科技

芯来科技成立于2018年，国内本土专业RISC-V处理器IP及整体解决方案提供商。公司从零开始，开发出全系列国产自主的RISC-V处理器IP产品：200、300、600、900等，覆盖从低功耗到高性能的各种应用场景。并且和重量级的行业客户在众多应用领域落地量产，遍及5G通信、工业控制、人工智能、汽车电子、物联网、存储、MCU、网络安全等多个领域。

目前已有超过150家正式授权客户。

更多详情访问： **www.nucleisys.com** 

### ▼往期精彩回顾▼

- [芯来RISC-V内核助力速显微发布“天都二号”多功能MPU](http://mp.weixin.qq.com/s?__biz=MzUxNDkyNzYxNA==&mid=2247498501&idx=1&sn=989fc7403fbcc76cb0e8a130b3af3b6e)
- [喜讯 | 芯来科技入选第五批国家专精特新“小巨人”企业](http://mp.weixin.qq.com/s?__biz=MzUxNDkyNzYxNA==&mid=2247498424&idx=1&sn=98edcfd54112d70b66dca7e9893c7265)
- [智芯科基于芯来RISC-V内核的存内计算芯片成功回片](http://mp.weixin.qq.com/s?__biz=MzUxNDkyNzYxNA==&mid=2247498411&idx=1&sn=fd3fb0625db1c2bebefd3f6b235dc686)
- [芯来科技与IAR达成战略合作伙伴关系](http://mp.weixin.qq.com/s?__biz=MzUxNDkyNzYxNA==&mid=2247498227&idx=1&sn=d6f11d3d47349be750686f074e132c45)
- [芯来科技邀您参与2023全国大学生嵌入式芯片与系统设计竞赛](http://mp.weixin.qq.com/s?__biz=MzUxNDkyNzYxNA==&mid=2247498225&idx=1&sn=ef0632927f253c0c8e44acef0bb2f435)
- [芯来科技加入中国移动物联网联盟RISC-V工作组](http://mp.weixin.qq.com/s?__biz=MzUxNDkyNzYxNA==&mid=2247498179&idx=1&sn=d5f8ea18439f4be965cf4c41f85fe714)
- [四城回顾 | 走进RISC-V车规“芯”技术](http://mp.weixin.qq.com/s?__biz=MzUxNDkyNzYxNA==&mid=2247498101&idx=1&sn=2ab19e89f2c3068eff0ba611b5521bf7)
- [纽瑞芯发布高性能UWB定位通信系统芯片，内置芯来内核](http://mp.weixin.qq.com/s?__biz=MzUxNDkyNzYxNA==&mid=2247493902&idx=1&sn=d5e46adedd2f0765e32a3b2d623e42c3)
- [滴水湖RISC-V国产芯阅兵，芯来四客户上榜](http://mp.weixin.qq.com/s?__biz=MzUxNDkyNzYxNA==&mid=2247493784&idx=1&sn=6d831fc2cd14318dfd69faf9fe73169b)
- [RISC-V产业联盟完成换届，芯来CEO彭剑英当选新任秘书长](http://mp.weixin.qq.com/s?__biz=MzUxNDkyNzYxNA==&mid=2247493661&idx=1&sn=075f7b2dca51d0341a1b1dba2d8a7621)
- [芯来RISC-V CPU IP产品获奖，连续三年荣登“中国芯”](http://mp.weixin.qq.com/s?__biz=MzUxNDkyNzYxNA==&mid=2247493647&idx=1&sn=139581b80d5bfd39783f9bde526690fb)
- [芯来助力裕太微推出高性能以太网交换芯片](http://mp.weixin.qq.com/s?__biz=MzUxNDkyNzYxNA==&mid=2247492979&idx=1&sn=6109b9fef0b8c2d0f50cd7f92db58a14)
- [沐创发布基于芯来RISC-V内核高速接口安全芯片](http://mp.weixin.qq.com/s?__biz=MzUxNDkyNzYxNA==&mid=2247492854&idx=1&sn=b123dab4a2dd6e60d6924811663e9586)
- [NucleiStudio 2022.08更新版本发布](http://mp.weixin.qq.com/s?__biz=MzUxNDkyNzYxNA==&mid=2247492731&idx=1&sn=d81223efe3d0158ea9e7ebb29001d680)

![输入图片说明](https://foruda.gitee.com/images/1693151415130703321/5a44ade2_5631341.png "屏幕截图")