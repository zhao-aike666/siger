# [赛昉科技重磅发布全新RISC-V处理器内核及多核子系统IP平台](https://mp.weixin.qq.com/s?__biz=MzU2MDY3NzE1Nw==&mid=2247492499&idx=1&sn=57b1a1ae74ae101e2035af47a01ccb36)

StarFive 2023-08-17 09:30 发表于北京

![输入图片说明](https://foruda.gitee.com/images/1692257725240933445/b25577ac_5631341.png "屏幕截图")

- 微信号：StarFive StarFive2020
- 功能介绍：赛昉科技官方账号。旨在介绍基于RISC-V指令集的商业化CPU核心IP、平台、生态，分享最新资讯，并致力于RISC-V在国内的推广及发展。

![输入图片说明](https://foruda.gitee.com/images/1692257750074070305/81b1a3d9_5631341.png "屏幕截图")

8月17日，中国RISC-V软硬件生态领导者赛昉科技正式发布两款自主研发的高性能RISC-V处理器内核新产品：昉·天枢-90（Dubhe-90）与昉·天枢-80（Dubhe-80）。 **Dubhe-90主打极致性能** ，是Dubhe Max Performance系列旗舰产品； **Dubhe-80主打高能效比** ，是Dubhe Efficiency Performance系列首款产品。

与此同时，基于Dubhe-90、Dubhe-80以及赛昉科技自主研发的片上一致性互联IP——昉·星链-500（StarLink-500），赛昉科技重磅发布首个国产高性能RISC-V多核子系统IP平台，这也是全球首款RISC-V大小核处理器子系统解决方案，填补了RISC-V领域相应市场的空白。

![输入图片说明](https://foruda.gitee.com/images/1692257784993256929/34f44579_5631341.png "屏幕截图")

>  _赛昉科技IP产品矩阵_ 

2022年，赛昉科技向客户交付了当时业界性能最高RISC-V CPU Core IP——昉·天枢（Dubhe）。一年后，赛昉科技再次发布性能更高的CPU Core IP——Dubhe-90， **性能比肩ARM Cortex-A76，SPECint2006 9.4/GHz，继续保持国产可交付性能最高的商业级RISC-V CPU IP产品的领先位置** 。Dubhe-90的客户主要来自于PC、高性能网络通讯、机器学习、数据中心等高端应用领域。

![输入图片说明](https://foruda.gitee.com/images/1692257832784883945/c739b31e_5631341.png "屏幕截图")

>  _Dubhe-90 CPU Core IP_ 

 **相较于Dubhe-90，Dubhe-80专为高能效场景而生，在性能差仅为20%的前提下，能效比提升50%** ，满足移动、桌面、工控、人工智能、汽车等场景的应用需求。随着Dubhe-80的正式推出，赛昉科技首次展现了高性能与高能效双轨迭代的RISC-V CPU Core IP产品路线图。

作为一款基于RISC-V指令集架构的64位商用处理器内核，Dubhe-80采用9+级流水线、三发射、乱序执行的设计， **SPECint2006 8.0/GHz，性能超过ARM Cortex-A75** 。Dubhe-80支持至今最完整的RISC-V指令集，包括RV64GC、位操作扩展B（Bitmanip 1.0）、向量扩展V（Vector 1.0）及虚拟化扩展H（Hypervisor 1.0）等。

![输入图片说明](https://foruda.gitee.com/images/1692257878466066290/e977cd97_5631341.png "屏幕截图")

>  _Dubhe-80 CPU Core IP_ 

经过预集成及验证，基于Dubhe-80，赛昉科技可为客户提供内存一致性的Cluster内单核、双核或四核的配置选择，极大简化SoC开发工作。在配套软件方面，赛昉科技能为客户提供裸机SDK、Linux SDK、基于Eclipse的IDE等。

“从高性能到高能效，赛昉科技在RISC-V领域持续探索，不断扩充自研RISC-V CPU IP矩阵。” **赛昉科技董事长兼CEO徐滔表示** ：“Dubhe-90与Dubhe-80的推出将进一步扩展RISC-V处理器的应用边界，协同促进RISC-V在端、边、云场景的应用落地。”

在SoC设计中，高性能内核与高能效内核的搭配使用，能使芯片在工作中达到最佳的能耗比。基于Dubhe-90、Dubhe-80与StarLink-500，赛昉科技发布首个国产高性能RISC-V多核子系统IP平台解决方案，支持8个核心的一致性协同工作， **可被广泛应用于PC、笔记本电脑、移动设备、瘦客户机、NAS、工控机及各类行业终端的主控芯片设计。值得一提的是，赛昉科技高性能RISC-V处理器——昉·惊鸿-8100（JH-8100）便采用了该设计方案** 。

![输入图片说明](https://foruda.gitee.com/images/1692257934949330390/d14d13e1_5631341.png "屏幕截图")

>  _高性能RISC-V多核子系统IP平台_ 

 **作为全球领先的RISC-V计算平台提供商，赛昉科技能为客户提供成熟的高性能、高带宽、低延迟的RISC-V芯片系统解决方案** ，方案包括高性能内核、高能效内核、高速的一致性NoC、RISC-V Trace/Debug调试接口、RISC-V中断控制器(PLIC，CLINT)、功耗管理、安全管理、虚拟化、IO一致性（IO Coherency）和内存子系统等， **助力客户实现在嵌入式、客户端、服务器及高性能计算等场景的芯片落地** 。

本次发布填补了全球范围内多项RISC-V技术、产品及解决方案空白，彰显了赛昉科技在构建高性能多核RISC-V处理器的技术领先地位。 **8月23日下午，赛昉科技将在北京RISC-V中国峰会现场举办“RISC-V芯片应用交流会”分论坛** ，探讨高性能RISC-V芯片破局之道，欢迎大家莅临交流。

![输入图片说明](https://foruda.gitee.com/images/1692258019137719177/29cf1382_5631341.png "屏幕截图")

>  _扫码报名参会_ 

未来，赛昉科技将依托自研CPU Core IP、Interconnect Fabric IP等核心产品和技术，不断推出满足不同应用场景的高性能RISC-V芯片系统解决方案，实现RISC-V在高性能应用场景的全方位覆盖, 为客户创造更多价值。

![输入图片说明](https://foruda.gitee.com/images/1692258054498923649/cde2c7ef_5631341.png "屏幕截图")

-End-

# [破局RISC-V落地难点 | RISC-V中国峰会赛昉同期活动报名开启](https://mp.weixin.qq.com/s?__biz=MzkzNTM5NDI0Mg==&mid=2247484654&idx=1&sn=043825e05a209c99264d34dc91f98522)

北京开源芯片研究院 2023-08-09 17:00 发表于北京

![输入图片说明](https://foruda.gitee.com/images/1692268887340970527/61ed8368_5631341.png "屏幕截图")

- 微信号：北京开源芯片研究院 gh_272a7ab004ba
- 功能介绍：开源芯片 普惠世界

### RISC-V Summit China 2023 2023 RISC-V 中国峰会

8月23日下午，第三届RISC-V中国峰会现场，赛昉科技将主办“RISC-V芯片应用交流会”，携手诸多客户与生态伙伴，分享赛昉科技高性能RISC-V芯片的 **软件生态、应用产品、解决方案** 等全面进展，破局RISC-V落地难点！

除精彩主题分享外，现场还将展示基于赛昉科技RISC-V芯片的应用产品和软硬件方案：工业防火墙、SoM板方案、平板电脑、笔记本电脑、边缘智能终端、Debian、openKylin等。

![输入图片说明](https://foruda.gitee.com/images/1692268963906142960/3826de2c_5631341.jpeg "640 (7).jpg")

### 温馨提示

您可扫描上图二维码或点此链接：   
https://starfive.mikecrm.com/2uhE0cF  
诚邀大家踊跃报名，到场参会！

赛昉科技将审核您的信息，您会很快收到短信反馈。期待与您见面！

### 关于北京开源芯片研究院

近年来，RISC-V快速发展，已经成为当前国际科技竞争的焦点。为提升我国集成电路设计水平，建设与国际开源社区对接的技术平台，北京市和中科院高度重视RISC-V发展，组织国内一批行业龙头企业和顶尖科研单位于2021年12月6日发起成立北京开源芯片研究院。研究院以开源开放凝聚产业发展共识，以协同创新激发应用牵引潜力，着力推进RISC-V创新链和产业链的加速融合，加速科技创新成果产业化落地，加快打造全球领先的RISC-V产业生态。

![输入图片说明](https://foruda.gitee.com/images/1690985561522088170/89b1f55e_5631341.gif "在这里输入图片标题")

### 报名抽奖

![输入图片说明](https://foruda.gitee.com/images/1692272388783824595/2a263a26_5631341.jpeg "Mvzk9bjLaNkTJWV9UmUQDeKD7nMwk7M6.jpg")

![输入图片说明](https://foruda.gitee.com/images/1692271651264912986/b30f8d51_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1692271725931221745/8da4736d_5631341.png "屏幕截图")