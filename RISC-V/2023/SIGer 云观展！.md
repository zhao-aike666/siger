尊敬的参会单位，

我带着自豪和激情向您推荐我们为本次大会奉献的志愿服务——SIGer 云观展。袁德俊老师已经为志愿者进行了全面[培训](https://www.bilibili.com/video/BV1wh4y1D7Cw/ "SIGer 云观展，2023年RISC-V峰会POSTER志愿者培训")，其中详细介绍了云观展的特点和优势，并展示了其展示的核心内容。（详情尽在 SIGer RISC-V 频道： https://gitee.com/flame-ai/siger/tree/master/RISC-V/ ）（手机可阅览 https://gitee.com/flame-ai/siger/tree/master/RISC-V/README.md ）

我们的云观展旨在鼓励志愿者参与和贡献，SIGer 志愿者不仅仅是迎宾和答疑的角色，还会介绍 RISC-V 峰会的内容和RISCV社区的开源文化。志愿者们将在三层 SIGer 展区进行值守和导引工作，确保来访嘉宾对展示内容有全面了解。

我们还安排了志愿者班次，能在峰会期间进行现场探访，为了不能到场的同学。而且峰会之前，所有志愿者都参与了 RISC-V 相关的学习，和峰会相关信息的搜集和整理，主要是与青少年关注的话题和内容相关。峰会现场，志愿者可能会向您提问，以一种非正式的采访和学习的方式。全部内容都将记录到 SIGer RISC-V 频道的 2023 专题目录下。 （ https://gitee.com/flame-ai/siger/tree/master/RISC-V/2023 ） 现已收录了部分参会信息。

我们期待您能分享更多参展信息给我们（群内分享即可）能提前剧透更好，以满足青少年对科技前沿的学习热情。这也是为青少年的教育和发展贡献一份力量！希望您能积极参与 SIGer 云观展，共同创造一个富有成果和启发的峰会体验。

再次感谢您对 SIGer 云观展的关注和支持！任何疑问或需求，请随时与我们联系。

祝大会圆满成功！

2023/8/18
SIGer 青少年开源科普
https://gitee.com/flame-ai/siger/

（转载评述的错漏，请第一时间告知，诚意感谢）

---

SIGer 开源科普作为 RISCV 中国峰会的见证人，获得本次峰会垂爱，入驻技术社区交流展区，分享最新的 SIGer 少年芯 2.0 青少年芯片教育全栈方案，同时也带去了 RISCV 的青少年优秀的科创案例。而SIGer 的科普使命，宣传本届省会更是责无旁贷。随着本届峰会的各方面信息的陆续披露，峰会的轮廓越发清晰，本片云观展就是为同学们整理的观展指南。

SIGer 的云观展的角度有两个，一个是教育主题的汇集，另一个是 SIGer 发现同一个单位会在多个场景下展示自己，SIGer 特地针对了解的单位，汇集了其峰会期间的全部活动，与其关注具体的内容，不如直接关注内容产出者，这就是 SIGer 云观展的独特之处。

首先的范例，就是最先上线的 芯来科技的内容，他们是最早动手发布峰会相关信息的公司，SIGer 也第一时间整理了 专题页推介：
- 10天倒计时，厂商代表发言，有芯来CEO的发言
- 主会板块，推荐演讲，收录了3个芯来的主题演讲
- POSTER 交流活动，芯来也以社区的角度提供了选题给社区开发者

再就是，中科昊芯的DSP，因为其选用的是芯来的IP核，受芯来的推荐，因而收录
- 转载了近期的一场开芯会的活动：
- 主会板块的推荐演讲，
- 不只在二层主会C4展区设展，三层技术交流区也会设有展台
- POSTER 交流活动，也同样提供了选题与社区开发者。
- [科技芯青年爱生活，2023 RISC-V 中国峰会见.md](
科技芯青年爱生活，2023%20RISC-V%20中国峰会见.md) 是本次云观展灵感的起点，SIGer 的考古能力还是不错的，最先锁定的是昊芯掌门人吴军宁老师的视频号，“科技芯青年” 的 IP非常醒目，给人朝气蓬勃的感觉，公号里时不时的各种节日祝贺和小伙伴脸上洋溢的笑容，是我用“爱生活”来点赞昊芯的主要理由，SIGer 海报是重要的标识，而 SIGer 一级频道中 能和 RISCV 匹敌的就是 “生活” 频道啦。

随着峰会的临近，各个参展单位开始密集地推送各自的信息，赛昉就在昨天更新了自己的新品发布，同时预报了峰会的同期活动，云观展怎能错过
- 以赛昉新品发布为基础新闻，更新到2023专题，
- 补充上主办单位预报的 同期活动的内容
- 第一时间和赛昉的老师取得联系，确认了峰会同期活动报名的通道有效。
- 2022年，赛昉新品发布时，SIGer 就制作了一期专题，对其和 UBUNTU 合作发布 IOT 版本进行了报道，其对极致性能的追求，让人印象深刻！
- 特地增加了，同期活动报名链接和海报

| date | # | name | title |
|---|---|---|---|
| 24日 | 1 | Xue Allen | [RISC—V首例一站式用户体验中心一赛防科技生态产品RVspace2.0](https://foruda.gitee.com/images/1692323391843813148/8265c032_5631341.jpeg) |
| 上午 | 2 | 谢家晖 | [Object Detection Solution on VisionFive2](https://foruda.gitee.com/images/1692323409445878954/d59ff5d7_5631341.jpeg) |

<img width="29%" src="https://foruda.gitee.com/images/1692323391843813148/8265c032_5631341.jpeg" title="saifang1.jpg"> <img width="29%" src="https://foruda.gitee.com/images/1692323409445878954/d59ff5d7_5631341.jpeg" title="VisionFive 2 0biect Detection-via GpU Accelerations.jpg">

昨天云观展的更新，特地讲志愿者关注的香山作为了重点，无论是主会还是同期，都是大篇幅的
- 最早的同期活动更新，就收录了香山，IEDA，一生一芯系列
- 收录了SIGer 本次峰会 POSTER 志愿者的 云观展测试范例，香山，并作为峰会推荐链接到了 SIGer 推介
- 在演讲报告推荐，选择了敏捷开发在香山的作用，王凯帆同学，一直都是同学们的偶像，第一届时的邂逅更是佳话，当然不能作为云观展的依据，他从第一颗本科生果核到如今的博士研究生，成长历程，绝对是同学们的榜样！

---

23日 DAY1: 

云观展筹备的成果就是 23日-25日 的技术交流区的展陈啦！ 奋战到凌晨4点，真正做到了自己的事情自己要操心，看着最后一张海报正确到位，才放心地离开，录制了一段儿视频给自己，也是给即将到来的志愿者和同学们。 《[RISCV峰会 SIGER来啦](https://www.bilibili.com/video/BV1ZN411B7sZ/ "B站：BV1ZN411B7sZ")》 封面的 sigRISCV 来啦！为了是不剧透，即将在峰会结束前的发布！

- [SIGer 云观展，2023年RISC-V峰会POSTER志愿者培训](https://www.bilibili.com/video/BV1wh4y1D7Cw) (充分的准备工作，才是峰会圆满完成任务的保障！)

- 凌晨准备好展陈的物料，奔赴现场，遇到了两位见证人！

  [<img width="19%" src="https://foruda.gitee.com/images/1693080192758689824/e98ff02e_5631341.jpeg" title="红帽老友！贡献了一位小志愿者！">](https://gitee.com/RV4Kids/iEDA/issues/I7W2QK) [<img width="19%" src="https://foruda.gitee.com/images/1693080205735827259/9e6795c6_5631341.jpeg" title="两位俄罗斯程序员">](https://gitee.com/RV4Kids/iEDA/issues/I7W2RX)

  - 基于RISC-V的Linux开源软件生态—发行版及启动固件现状与展望 傅炜 RVI大使 红帽软件（北京）有限公司首席软件工程师
  - Compiler optimization challenges for high performance RISC·V cores - Sergey Yakushkin & Konstantin Vladimirov

- sigRISCV-1 群是 Day1 的见证人 （106）

  <p><img height="169px" src="https://foruda.gitee.com/images/1693080956253537594/04eff329_5631341.jpeg" title="2D908B62@A913395C.B8B7E76400000000.jpg"> <img height="169px" src="https://foruda.gitee.com/images/1693080966007895441/be8110cc_5631341.jpeg" title="34CB302B@64CD2F1B.B8B7E76400000000.jpg"> <img height="169px" src="https://foruda.gitee.com/images/1693081023185096650/12cb3512_5631341.jpeg" title="86F93668@62824010.B8B7E76400000000.jpg"> <img height="169px" src="https://foruda.gitee.com/images/1693081042676184890/cd1654d9_5631341.jpeg" title="562D252F@63712B38.B8B7E76400000000.jpg"> <img height="169px" src="https://foruda.gitee.com/images/1693081109840456824/3f90608b_5631341.jpeg" title="2534C3DD@4D2A4F52.B7B7E76400000000.jpg"> <img height="169px" src="https://foruda.gitee.com/images/1693081123071586729/231f669c_5631341.jpeg" title="6257C45C@B98F595B.B8B7E76400000000.jpg"> <img height="169px" src="https://foruda.gitee.com/images/1693081182334335030/26dcbcfe_5631341.jpeg" title="34433634@BF607259.B8B7E76400000000.jpg"> <img height="169px" src="https://foruda.gitee.com/images/1693081196020021471/94a60ed4_5631341.jpeg" title="35066135@671A664C.B8B7E76400000000.jpg"> <img height="169px" src="https://foruda.gitee.com/images/1693081260093899177/6c6c30aa_5631341.jpeg" title="93011046@E4E55A3E.B8B7E76400000000.jpg"> <img height="169px" src="https://foruda.gitee.com/images/1693081302612971353/8ad093ab_5631341.jpeg" title="CD3320F0@B4F89F10.B8B7E76400000000.jpg"></p>

---

24日 DAY2:

有了第一天的经验，第2天可以精神抖擞的提高任务目标啦：

- sigRISCV-2 群是 Day2 的见证人 （111）

  <p><img height="169px" src="https://foruda.gitee.com/images/1693082257959389432/c56a8df8_5631341.jpeg" title="1.jpg"> <img height="169px" src="https://foruda.gitee.com/images/1693082271321450767/481c9241_5631341.jpeg" title="2.jpg"> <img height="169px" src="https://foruda.gitee.com/images/1693082283567307140/8dd93900_5631341.jpeg" title="3.jpg"> <img height="169px" src="https://foruda.gitee.com/images/1693082294334270088/04f228cf_5631341.jpeg" title="4.jpg"> <img height="169px" src="https://foruda.gitee.com/images/1693082308922577096/3b817627_5631341.jpeg" title="5.jpg"> <img height="169px" src="https://foruda.gitee.com/images/1693082320141678667/b2f02e97_5631341.jpeg" title="6.jpg"> <img height="169px" src="https://foruda.gitee.com/images/1693082331890813637/ca66c63a_5631341.jpeg" title="7.jpg"> <img height="169px" src="https://foruda.gitee.com/images/1693082342552170442/d53811b1_5631341.jpeg" title="8.jpg"> <img height="169px" src="https://foruda.gitee.com/images/1693082355238598673/5a3192c4_5631341.jpeg" title="9.jpg"> <img height="169px" src="https://foruda.gitee.com/images/1693082367852511581/44fd8b54_5631341.jpeg" title="10.jpg"> <img height="169px" src="https://foruda.gitee.com/images/1693082379729176039/80c86948_5631341.jpeg" title="11.jpg"> <img height="169px" src="https://foruda.gitee.com/images/1693082390954168560/0e908cea_5631341.jpeg" title="12.jpg"> <img height="169px" src="https://foruda.gitee.com/images/1693082404359282362/0fa9ba25_5631341.jpeg" title="13.jpg"> <img height="169px" src="https://foruda.gitee.com/images/1693082417175557283/bdc1bf61_5631341.jpeg" title="14.jpg"> <img height="169px" src="https://foruda.gitee.com/images/1693082429480726081/514709e0_5631341.jpeg" title="15.jpg"> <img height="169px" src="https://foruda.gitee.com/images/1693082442046081527/827094c0_5631341.jpeg" title="16.jpg"> <img height="169px" src="https://foruda.gitee.com/images/1693082453497043611/39af9fc8_5631341.jpeg" title="17.jpg"> <img height="169px" src="https://foruda.gitee.com/images/1693082464853949429/5d424401_5631341.jpeg" title="18.jpg"> <img height="169px" src="https://foruda.gitee.com/images/1693082475870072882/65c57cd5_5631341.jpeg" title="19.jpg"> <img height="169px" src="https://foruda.gitee.com/images/1693082495373246144/2c872497_5631341.jpeg" title="20.jpg"> <img height="169px" src="https://foruda.gitee.com/images/1693082509790669834/d5e49714_5631341.jpeg" title="21.jpg"> <img height="169px" src="https://foruda.gitee.com/images/1693082525336791467/72c75c01_5631341.jpeg" title="22.jpg"> <img height="169px" src="https://foruda.gitee.com/images/1693082538339268721/b81a8aee_5631341.jpeg" title="23.jpg"> <img height="169px" src="https://foruda.gitee.com/images/1693082549250099232/ee6e4b68_5631341.jpeg" title="24.jpg"> <img height="169px" src="https://foruda.gitee.com/images/1693082563681703608/dd9ed7a0_5631341.jpeg" title="25.jpg"> <img height="169px" src="https://foruda.gitee.com/images/1693082575988709917/bdc94a1e_5631341.jpeg" title="26.jpg"> <img height="169px" src="https://foruda.gitee.com/images/1693082590015315889/9189a8e5_5631341.jpeg" title="27.jpg"> <img height="169px" src="https://foruda.gitee.com/images/1693082601918247706/20d190ae_5631341.jpeg" title="28.jpg"> <img height="169px" src="https://foruda.gitee.com/images/1693082614893925177/05e1ad9e_5631341.jpeg" title="29.jpg"> <img height="169px" src="https://foruda.gitee.com/images/1693082628156834959/ee921050_5631341.jpeg" title="30.jpg"></p>

同为三层技术交流区的小伙伴睿思芯科的海报必须刊登：

<p><img width="21.9%" src="RiVAI：聚焦RISC-V处理器核心技术产品自主研发.jpg"> <img width="29%" src="基于RISC-V架构的高端CPU-RiVAI%20P600.jpg"></p>

| 日期 | 序号 | 姓名 | 单位 | 内容 |
|---|---|---|---|---|
| 24日上午 | 10 | 刘鹏 | [睿思芯科技术有限公司](RiVAI：聚焦RISC-V处理器核心技术产品自主研发.jpg) | [基于RISC-V架构的高端CPU-RiVAI P600](基于RISC-V架构的高端CPU-RiVAI%20P600.jpg) 

---

25日 DAY3: 

最后一天是重要的 Poster 演讲日，任务量减半：

- sigRISCV-3 群是 Day3 的见证人 （52）

  <p><img width="19%" src="https://foruda.gitee.com/images/1693085041229556128/fe0c366a_5631341.jpeg" title="1.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1693085058048496572/3786e843_5631341.jpeg" title="2.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1693196749067802786/91771f7e_5631341.jpeg" title="5.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1693085103166855506/19f72af0_5631341.jpeg" title="6.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1693085126543343489/00167e69_5631341.jpeg" title="8.jpg"></p>

- sigRISCV.com 正式上线于 2023.8.23 07:17:06 峰会开幕的当天，这是一个意外惊喜。经过三天的推荐，一个青少年视角的 RISCV 兴趣社区已经浮现，发布会就是在 25日上午 9:30 - 9:50 “SIGer 少年芯 2.0” 的发布现场，这个事先预演的活动，可以说 sigRISCV 是在整个峰会的嘉宾的关注下诞生的。而最后一日的几张代表性的合影，则是来自多个方面的祝福和见证了。

  <p><img width="19%" src="https://foruda.gitee.com/images/1693107005184171689/729ffe63_5631341.jpeg" title="IMG_1135 - 副本.JPG"> <img width="19%" src="https://foruda.gitee.com/images/1693107025827751244/3cf4f00d_5631341.jpeg" title="IMG_1153 - 副本.JPG"> <img width="19%" src="https://foruda.gitee.com/images/1693107042297823432/1bd8ef02_5631341.jpeg" title="IMG_1157 - 副本.JPG"> <img width="19%" src="https://foruda.gitee.com/images/1693107059218705981/1fb56786_5631341.jpeg" title="IMG_9513 - 副本.JPG"> <img width="19%" src="https://foruda.gitee.com/images/1693107074158435107/26c2bda0_5631341.jpeg" title="IMG_E9505 - 副本.JPG"></p>

  - sigRISCV.com 发布仪式是在 SIGer GROVE 与 veriMAKE YADAN 合体的 “满脑子电线的奥特鸭” 的发布现场。还有两位见证人，YKriscv 交大附中的许元开同学 是作为 SIGer 科创少年代表，带着 YKriscv 从上海来北京为 SIGer 站台的。另一位 重庆大学的周喜川校长，他是 RVI AI/ML sig 的主席，我们对教育环境建设的理念，获得到了他的认可。
  - 本次峰会，SIGer主视觉是民族棋中国史的 —— 非遗小游戏 “九子贤棋”，它是社区连接的利器，来自厦门开芯会的贠利君老师，成为了家长志愿者代表，在厦门传播民族棋。
  - 志愿者是峰会最忙碌的啦，本场发布会的志愿者，在会后来到 SIGer 站台前，专门学习了九子贤棋，并合影留念
  - PLCT 的吴伟老师，在峰会结束前，来到 SIGer 站台前，与志愿者们合影，感谢他们的辛勤付出
  - LINUX 开发板一直都是 RISCV 社区的指标，赛昉回访 SIGer 站台，许下 all the best! 的愿景，唯一连接整个社区，才能实现，这是我的解读！

<p align="right"><a href="../README.md" target="_blank">sigRISCV.com</a> 青少年视角的 RISCV 兴趣社区<br>SIGer 青少年开源科普<br>袁德俊 2023.8.27</p>