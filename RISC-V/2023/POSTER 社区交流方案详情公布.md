# [2023 RISC-V 中国峰会 POSTER & 社区交流方案详情公布](https://mp.weixin.qq.com/s?__biz=MzUyMzA2NzkzOA==&mid=2247484919&idx=1&sn=2ac7d44a436010892785384f5d8ede5d)

原创 RVSC组委会 CNRV 2023-08-17 16:58 发表于浙江

![输入图片说明](https://foruda.gitee.com/images/1692270656695858994/fb6fc179_5631341.png "屏幕截图")

- 微信号：CNRV risc-v
- 功能介绍：RISC-V中国社区

![输入图片说明](https://foruda.gitee.com/images/1692270330896417962/8761f332_5631341.jpeg "webwxgetmsgimg (2).jpg")

### POSTER 交流分享

| 日期 | 序号 | 姓名 | 单位 | 内容 |
|---|---|---|---|---|
| 23日 | 1 | 老笨 | 奔跑吧Linux社区 | 《RISC—V体系结构编程与实践》一旧的回顾，新的启程
| 下午 | 2 | 崔恩放 | 中国电信研究院 | RISC—V云网融合边缘计算关键技术梳理和TeleBox解决方案架构设计解析 |
| | 3 | Wang Xiao | Intel | Here!An Overview of Various Methods to<br>Apply RVV(RISC-V Vector) into SW Optimization 
| | 4 | Rigo Alvise | Virtual Open Systems | RISC—V平台上一种混合关键系统解决方案 |
| | 5 | 杨叶倩 | 山东大学 | 基于RISC—VCPU的服务器上实现SBI固件和UEFI固件的最新技术进展
| | 6 | 孟成真 | 山东大学 | 基于RISC—VCPU的服务器上成功应用高性能TPU进行AI模型推理加速的工作
| | 7 | 刘亚南 | 苏州库瀚信息科技有限公司 | 基于RISC—V架构在存储、网络以及AI数据搬运等场景的实践
| | 8 | 刘雨冬 | 中科南京软件技术研究院 | Yocto、openEuler Embedded、RISC-V Linux等多系统适配场景 
| | 9 | 张 飞 | 中科南京软件技术研究院 | 基于MUSL LIBC 基础库和 RISC—V RVV 1.0扩展指令集的协同研究点
| | 10 | Zheng Lv | 进迭时空 | Topics! Privileged Modifications around RVH,<br>Advanced Interrupt Architecture and I/O Memory Management Unit. 
| | 11 | 陈骅 | 北京中科昊芯科技有限公司 | RISC—V多核场景中的调试创新点、调试使用方法和任务分配
| 24日 | 1 | Xue Allen | 赛昉科技 | RISC—V首例一站式用户体验中心一赛防科技生态产品RVspace2.0
| 上午 | 2 | 谢家晖 | 赛昉科技 | Object Detection Solution on VisionFive2 
| | 3 | 费晓龙 | 进迭时空（杭州）科技有限公司 | 进迭时空X100的功能特性及架构创新
| | 4 | 邓金斌 | 深圳国微芯科技有限公司 | RISC-V CPU在EsseFormal形式验证过程中的遇到的问题以及实战经验分享
| | 5 | Han Duo | 北京中科昊芯科技有限公司 | μC／OS-ii操作系统在HX2000系列RISC-VDSP上的应用 
| | 6 | 杨国华 | 苏州库瀚信息科技有限公司 | 针对存储及I／O场景，数款RISC—V芯片如何在微架构层面实现创新
| | 7 | 梁宇宁 | 深度数智 | 软硬融合的RISC-V生态产品-RISC-V笔记本 ROMA 
| | 8 | 叶从容 | 中国科学院计算技术研究所 | ByteFloat机制：基于“一生一芯”的卷积神经网络部署方案
| | 9 | 雷依钒 | 兆松科技（武汉）有限公司 | RISC—V SoC性能分析平台一让你的架构探索更轻松
| | 10 | 刘鹏 | 睿思芯科技术有限公司 | 基于RISC-V架构的高端CPU-RiVAI P600 
| | 11 | 晏明 | 兆松科技（武汉）有限公司 | 高性能RVV自动向量化编译器
| 24日 | 1 | 刘诣元 | 浙江大学 | MorFuzz:Fuzzing Processor via Runtime Instruction<br>Morphing enhanced Synchronizable Co-simulation 
| 下午 | 2 | 于波 | PLCT | Debian RISC-V的移植进展 |
| | 3 | 方华启 | 芯来科技 | 基于RISC—V架构的嵌入式AI，开源调试器和可信执行环境的技术展示
| | 4 | 冯二虎 | 上海交通大学 | SIOPMP：一种高效与可扩展的I／O隔离保护方案
| 25日 | 1 | 袁德俊 | SIGer 青少年开源文化期刊 | SIGER 少年芯 2.0 青少年开源芯片教育方案
| 上午 | 2 | 朱燕翔 | 南京仁面集成电路技术有限公司 | 基于RISC—V开源软核YADAN，实现PWM模块的方便使用
| | 3 | 邰 阳 | 中国科学院软件研究所 | PolyOS Mobile：基于QEMU RISC-V架构的OpenHarmony标准系统 
| | 4 | 许元开 | 交通大学附属中学 | 基于Python语言的RISC—V指令集模拟器

### 技术社区交流名单：

社区交流地点：酒店三层公共区域

#### 社区/企业交流：

| 社区/企业名称 | 展示内容 |
|---|---|
| 澎峰科技PerfXLab | 衡山系列RISC—V高性能服务器（HS—1、HS—2） |
| 泰晓科技 | 泰晓 Linux 实验盘 |
| SIGer 开源科普 | 芯片教育全栈方案 + 青少年 RISCV 科创案例 |
| openKylin | openKylin（开放麒麟）开源桌面操作系统 |
| DatenLord | open-rdma |
| 深度数智 | 最新RISC—V硬件产品和软件解决方案 |
| IAR | IAR Embedded Workbench for RISC-V | 
| CCF 开源发展委员会 | GitLink开源创新服务平台 |
| 睿思芯科 | 针对数据中心应用的RISC—V CPU——RiVAI P600 |
| 平头哥 | 让高性能RISC—V软硬全栈技术·触手可及 |
| 北京中科昊芯科技 | 基于昊芯RISC—V DSP的伺服系统解决方案&无传感器电机控制解决方案 |
| 江苏润开鸿数字科技有限公司 | 基于RISC—V架构与OpenHarmony操作系统的终端应用 |
| 人民邮电出版社异步社区 | RISC—V相关的优秀书籍推荐 |
| 奔跑吧Linux社区 | 《RISC—V体系结构编程与实践》书籍推荐 | 

#### 个人交流：

| 社区/企业名称 | 展示内容 |
|---|---|
| 刘阳 | box64 for RISC-V on LicheePi 4A |
| 于波 | Debian RISC—V 的移植进展 |


尊敬的各位RISC-V伙伴:

我们非常高兴的宣布，2023 RISC-V中国峰会将于8月23日至25日在北京香格里拉饭店盛大开幕！在这次令人期待的峰会上，我们将聚集来自世界各地的RISC-V爱好者、专业人士和创新者，共同分享他们在开源处理器领域的研究成果和实践经验。无论你是初学者还是行业专家，这都是一个展示你的创意、学习他人见解的绝佳机会。

本次峰会组委会特别安排了一系列精彩的交流活动。

- 在酒店三层的绿宝厅，我们将举行POSTER交流分享，提供展示创意和研究成果的机会。您可以借此平台与其他与会者交流，探讨技术、分享见解，共同推动RISC-V技术的创新发展。

![输入图片说明](https://foruda.gitee.com/images/1692270699524698694/8051e489_5631341.png "屏幕截图")

- 同时，三层酒店的公共区域设立了社区交流空间，在这里，您可以畅所欲言，结识志同道合的朋友，共同探讨开源技术的前沿趋势与挑战。无论是轻松的闲聊还是深入的技术讨论，这个开放的交流环境将为您带来丰富的交流体验。

![输入图片说明](https://foruda.gitee.com/images/1692270716605164166/2819f9f5_5631341.png "屏幕截图")

让我们在2023 RISC-V中国峰会中相聚，共同开启一个充满创新、合作和学习的时刻！如果您有任何疑问或需要进一步信息，请邮件联系组委会RV2023@bosc.ac.cn。

2023RISC-V中国峰会组委会

2023年08月17日

> 2023年08月17日，8月社区共相聚，17嗨一起玩一起学！官宣啦。结识志同道合的朋友，共同探讨开源技术的前沿趋势与挑战。无论是轻松的闲聊还是深入的技术讨论，太期待了！  这就是社区应有的样子！三天中午，必须是社区高潮中的高潮！

> 2023 RISCV 中国峰会 倒计时第6天。【SIGER 云观展】正式发布：
https://gitee.com/flame-ai/siger/tree/master/RISC-V 同学们开始会议准备了。首先测试，地图位置，我们在 3层社区交流区，3天展区，25日上午主题演讲在 POSTER 绿宝厅。请同学们重点查看首页地图。需要在电脑上看，手机需要切换到桌面版。或者  https://gitee.com/flame-ai/siger/blob/master/RISC-V/README.md

> 这是 SIGer 的专题。这几天会陆续有新内容。主要信息都有了。尤其是导航图。地铁站出来，到酒店，直接三层，就能见到我啦。