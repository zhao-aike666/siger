# [基于RISC-V开源软核YADAN，实现PWM模块的方便使用](https://www.bilibili.com/video/BV15j411m7hr/)

| 日期 | 序号 | 姓名 | 单位 | 内容 |
|---|---|---|---|---|
| 25上 | 2 | 朱燕翔 | [南京仁面集成电路技术有限公司](https://verimake.com/ "veriMAKE") | [基于RISC-V开源软核YADAN，实现PWM模块的方便使用](https://www.bilibili.com/video/BV17N4y1R7jH/ "B站：BV17N4y1R7jH 2023 RISC-V 中国峰会 POSTER 现场，SIGer 致谢 veriMake YADAN 助力青少年社区") - [芮懿博](https://www.bilibili.com/video/BV15j411m7hr/ "PWM方案介绍，芮懿博")

<p><img width="31.69%" src="https://foruda.gitee.com/images/1693380117964701036/0b5848ad_5631341.jpeg" title="鸭蛋添加pwm外设(1)-1.jpg"> <img width="31.69%" src="https://foruda.gitee.com/images/1693380128795831080/b51bd148_5631341.jpeg" title="鸭蛋添加pwm外设(1)-2.jpg"> <img width="31.69%" src="https://foruda.gitee.com/images/1693380137935744709/43bb56c8_5631341.jpeg" title="鸭蛋添加pwm外设(1)-3.jpg"> <img width="31.69%" src="https://foruda.gitee.com/images/1693380150588891216/3076aa45_5631341.jpeg" title="鸭蛋添加pwm外设(1)-4.jpg"> <img width="31.69%" src="https://foruda.gitee.com/images/1693380162831437804/262c52f9_5631341.jpeg" title="鸭蛋添加pwm外设(1)-5.jpg"> <img width="31.69%" src="https://foruda.gitee.com/images/1693380173507381714/5402e203_5631341.jpeg" title="鸭蛋添加pwm外设(1)-6.jpg"></p>

### [YADAN Core](https://foruda.gitee.com/images/1693380117964701036/0b5848ad_5631341.jpeg)

![输入图片说明](https://foruda.gitee.com/images/1693380411849169664/9e7465c3_5631341.png "屏幕截图")

- 32-bit RISC-V cores with I/M ISA
- 5级流水线
- 3736 多行Verilog代码
- 7k LUTs资源

### [由Yadan core构成的SoC](https://foruda.gitee.com/images/1693380128795831080/b51bd148_5631341.jpeg)

![输入图片说明](https://foruda.gitee.com/images/1693380470511200358/47df8865_5631341.png "屏幕截图")

- AHB-APB
- Timers
- UART
- GPIO
- SPI

### [添加PWM外设](https://foruda.gitee.com/images/1693380137935744709/43bb56c8_5631341.jpeg)

![输入图片说明](https://foruda.gitee.com/images/1693380518257125772/40c8beea_5631341.png "屏幕截图")

| 外设名称 | 地址 |
|---|---|
| UART | 0x4A100000 |
| GPIO | 0x4A101000 |
| SPI | 0x4A102000 |
| TIMER | 0x4A103000 |
| PWM | 0x4A104000 |

### [在SoC中添加PWM外设](https://foruda.gitee.com/images/1693380150588891216/3076aa45_5631341.jpeg)


```
APB_PWM u_apb_pwm(
    .PCLK ( clk ),
    .PRESETn ( rst ),
    .PADDR ( S_PADDR ),
    .PSEL ( S4_HSEL & S4_PSEL ),
    .PENABLE ( S_PENABLE ),
    .PWRITE ( S_PWRITE ),
    .PWDATA ( S_PWDATA ),
    .PREADY ( S4_PREADY ),
    .PRDATA ( S4_PRDATA ),
    .PSLAVEERR ( S4_PSLVERR ),
    .PWM ( pwmgpio )
);
```

### [PWM外设具体实现](https://foruda.gitee.com/images/1693380162831437804/262c52f9_5631341.jpeg)

实现一个简单的PWM功能，输出的PWM周期、占空比能够通过寄存器来设置，并且能够有使能端控制PWM的开关。

1. 设计寄存器

   | 寄存器名称 | 地址 | 说明 |
   |---|---|---|
   | 周期控制寄存器 | 0x4A104000 | 32位寄存器控制周期的分频系数 |
   | 占空比控制寄存器 | 0x4A104004 | 32位寄存器控制占空比的系数 |
   | 输出管脚控制寄存器 | 0x4A104008 | 32位寄存器低八位控制PWM输出管脚数量 |
   | 使能控制寄存器 | 0x4A10400C | 32位寄存器最低位控制PWM输出使能 |

2. APB总线接口设计——以写寄存器为例

   ![输入图片说明](https://foruda.gitee.com/images/1693380732704502832/34d26051_5631341.png "屏幕截图")

### [PWM外设C代码](https://foruda.gitee.com/images/1693380173507381714/5402e203_5631341.jpeg)

[![输入图片说明](https://foruda.gitee.com/images/1693380850966035732/905005a8_5631341.png "屏幕截图")](https://foruda.gitee.com/images/1693380803326297562/7bb2b57c_5631341.png)

