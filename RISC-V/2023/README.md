![输入图片说明](https://foruda.gitee.com/images/1692077603811974966/373160fb_5631341.jpeg "峰会主会2层平面图")

[![](https://foruda.gitee.com/images/1690985195116771835/b67337a6_5631341.png)](https://riscv-summit-china.com/)

> 关于RISCV技术峰会的消息通知，它宣布在北京举行的RISCV技术峰会将提供实时的中英文翻译服务。届时，参会者可以选择线上或线下形式参加，活动将于8月23日至25日举行，主要内容涉及RISCV技术的技术和商业创新。你可以在网站 https://riscv-summit-china.com/ 查看日程并进行注册。

<p><img width="33.5%" src="https://foruda.gitee.com/images/1691781517888443777/258a782b_5631341.jpeg"> <img width="33%" src="https://foruda.gitee.com/images/1691781528041468638/026917c6_5631341.jpeg"> <img width="32.5%" src="https://foruda.gitee.com/images/1691781539015734451/ad7c7a2e_5631341.jpeg"> <img width="33.25%" src="https://foruda.gitee.com/images/1691781335156689284/d4dfe92e_5631341.jpeg"> <img width="33.1%" src="https://foruda.gitee.com/images/1691781345795801991/08b72ed8_5631341.jpeg"> <img width="32.5%" src="https://foruda.gitee.com/images/1691781357670708281/8f41cceb_5631341.jpeg"></p>

> We will have real-time Chinese-English translations at #RISCVSummitChina! Join us virtually or in-person in Beijing from August 23-25 to learn about technical and business #RISCV innovations. View the agenda and register here: https://riscv-summit-china.com/

![输入图片说明](https://foruda.gitee.com/images/1692270330896417962/8761f332_5631341.jpeg "webwxgetmsgimg (2).jpg")

<img width="48.75%" src="https://foruda.gitee.com/images/1692270699524698694/8051e489_5631341.png"> <img width="49%" src="https://foruda.gitee.com/images/1692270716605164166/2819f9f5_5631341.png">

| 日期 | 序号 | 姓名 | 单位 | 内容 |
|---|---|---|---|---|
| 23日 | 1 | 老笨 | 奔跑吧Linux社区 | 《RISC—V体系结构编程与实践》一旧的回顾，新的启程
| 下午 | 2 | 崔恩放 | 中国电信研究院 | RISC—V云网融合边缘计算关键技术梳理和TeleBox解决方案架构设计解析 |
| | 3 | Wang Xiao | Intel | Here!An Overview of Various Methods to<br>Apply RVV(RISC-V Vector) into SW Optimization 
| | 4 | Rigo Alvise | Virtual Open Systems | RISC—V平台上一种混合关键系统解决方案 |
| | 5 | 杨叶倩 | 山东大学 | 基于RISC—VCPU的服务器上实现SBI固件和UEFI固件的最新技术进展
| | 6 | 孟成真 | 山东大学 | 基于RISC—VCPU的服务器上成功应用高性能TPU进行AI模型推理加速的工作
| | 7 | 刘亚南 | 苏州库瀚信息科技有限公司 | 基于RISC—V架构在存储、网络以及AI数据搬运等场景的实践
| | 8 | 刘雨冬 | 中科南京软件技术研究院 | Yocto、openEuler Embedded、RISC-V Linux等多系统适配场景 
| | 9 | 张 飞 | 中科南京软件技术研究院 | 基于MUSL LIBC 基础库和 RISC—V RVV 1.0扩展指令集的协同研究点
| | 10 | Zheng Lv | 进迭时空 | Topics! Privileged Modifications around RVH,<br>Advanced Interrupt Architecture and I/O Memory Management Unit. 
| | 11 | 陈骅 | 北京中科昊芯科技有限公司 | RISC—V多核场景中的调试创新点、调试使用方法和任务分配
| 24日 | 1 | Xue Allen | 赛昉科技 | [RISC—V首例一站式用户体验中心一赛防科技生态产品RVspace2.0](https://foruda.gitee.com/images/1692323391843813148/8265c032_5631341.jpeg)
| 上午 | 2 | 谢家晖 | 赛昉科技 | [Object Detection Solution on VisionFive2](https://foruda.gitee.com/images/1692323409445878954/d59ff5d7_5631341.jpeg) 
| | 3 | 费晓龙 | 进迭时空（杭州）科技有限公司 | 进迭时空X100的功能特性及架构创新
| | 4 | 邓金斌 | 深圳国微芯科技有限公司 | RISC-V CPU在EsseFormal形式验证过程中的遇到的问题以及实战经验分享
| | 5 | Han Duo | 北京中科昊芯科技有限公司 | [μC／OS-ii操作系统在HX2000系列RISC-VDSP上的应用](Haawking%20IDE：助力risc-v%20dsp多核实时调试V2.jpg) 
| | 6 | 杨国华 | 苏州库瀚信息科技有限公司 | 针对存储及I／O场景，数款RISC—V芯片如何在微架构层面实现创新
| | 7 | 梁宇宁 | 深度数智 | 软硬融合的RISC-V生态产品-RISC-V笔记本 ROMA 
| | 8 | [叶从容](https://www.bilibili.com/video/BV1Ld4y1V7DA/ "一生一芯B站：BV1Ld4y1V7DA") | 中国科学院计算技术研究所 | [ByteFloat机制：基于“一生一芯”的卷积神经网络部署方案](../../sig/ysyx/ByteFloat机制：基于“一生一芯”的卷积神经网络部署方案.md)
| | 9 | 雷依钒 | [兆松科技](https://gitee.com/RV4Kids/iEDA/issues/I7VS3J)（武汉）有限公司 | RISC—V SoC性能分析平台一让你的架构探索更轻松
| | 10 | 刘鹏 | [睿思芯科技术有限公司](RiVAI：聚焦RISC-V处理器核心技术产品自主研发.jpg) | [基于RISC-V架构的高端CPU-RiVAI P600](基于RISC-V架构的高端CPU-RiVAI%20P600.jpg) 
| | 11 | 晏明 | [兆松科技](https://gitee.com/RV4Kids/iEDA/issues/I7VS3J)（武汉）有限公司 | 高性能RVV自动向量化编译器
| 24日 | 1 | 刘诣元 | 浙江大学 | MorFuzz:Fuzzing Processor via Runtime Instruction<br>Morphing enhanced Synchronizable Co-simulation 
| 下午 | 2 | 于波 | PLCT | Debian RISC-V的移植进展 |
| | 3 | [方华启](芯来车规产品引关注，本土RISC-V力量“在一起”.md) | [芯来科技](../2023%20RISC-V中国峰会，芯来不见不散.md/#2023-risc-v中国峰会-芯来科技演讲及报告) | [基于RISC—V架构的嵌入式AI，开源调试器和可信执行环境的技术展示](https://foruda.gitee.com/images/1693151352953419370/54c2ca8a_5631341.png)
| | 4 | 冯二虎 | 上海交通大学 | SIOPMP：一种高效与可扩展的I／O隔离保护方案
| 25日 | 1 | 袁德俊 | SIGer 青少年开源文化期刊 | [SIGER 少年芯 2.0 青少年开源芯片教育方案](https://foruda.gitee.com/images/1692285854361219787/5d9b0658_5631341.jpeg)
| 上午 | 2 | 朱燕翔 | [南京仁面集成电路技术有限公司](基于RISC-V开源软核YADAN，实现PWM模块的方便使用.md "PDF veriMAKE") | [基于RISC—V开源软核YADAN，实现PWM模块的方便使用](https://foruda.gitee.com/images/1692285816675457755/dcfc752b_5631341.jpeg) - [芮懿博](https://www.bilibili.com/video/BV15j411m7hr/ "B站：BV15j411m7hr")
| | 3 | 邰 阳 | 中国科学院软件研究所 | [PolyOS Mobile：基于QEMU RISC-V架构的OpenHarmony标准系统](https://www.bilibili.com/video/BV1Ph4y1S77L/ "B站：BV1Ph4y1S77L") 
| | 4 | 许元开 | 交通大学附属中学 | [基于Python语言的RISC—V指令集模拟器](https://foruda.gitee.com/images/1692285902083453205/3a0c0c5c_5631341.jpeg)

 **技术社区交流名单：** 

社区交流地点：酒店三层公共区域

 **社区/企业交流：** 

| 社区/企业名称 | 展示内容 |
|---|---|
| 澎峰科技PerfXLab | 衡山系列RISC—V高性能服务器（HS—1、HS—2） |
| 泰晓科技 | 泰晓 Linux 实验盘 |
| SIGer 开源科普 | [芯片教育全栈方案](https://foruda.gitee.com/images/1692285839167897457/fae7a237_5631341.jpeg) + [青少年 RISCV 科创案例](https://foruda.gitee.com/images/1692285887505230778/c26c4d9e_5631341.jpeg) |
| openKylin | openKylin（开放麒麟）开源桌面操作系统 |
| DatenLord | open-rdma |
| 深度数智 | 最新RISC—V硬件产品和软件解决方案 |
| IAR | IAR Embedded Workbench for RISC-V | 
| CCF 开源发展委员会 | GitLink开源创新服务平台 https://www.gitlink.org.cn |
| 睿思芯科 | 针对数据中心应用的RISC—V CPU——RiVAI P600 |
| 平头哥 | 让高性能RISC—V软硬全栈技术·触手可及 |
| 北京中科昊芯科技 | 基于昊芯RISC—V DSP的伺服系统解决方案&无传感器电机控制解决方案 |
| 江苏润开鸿数字科技有限公司 | 基于RISC—V架构与OpenHarmony操作系统的终端应用 |
| 人民邮电出版社异步社区 | RISC—V相关的优秀书籍推荐 |
| 奔跑吧Linux社区 | 《RISC—V体系结构编程与实践》书籍推荐 | 

 **个人交流：** 

| 社区/企业名称 | 展示内容 |
|---|---|
| 刘阳 | box64 for RISC-V on LicheePi 4A |
| 于波 | Debian RISC—V 的移植进展 |
