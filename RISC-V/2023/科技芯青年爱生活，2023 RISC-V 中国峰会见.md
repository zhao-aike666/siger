# [邀请函|昊芯邀您参加2023 RISC-V 中国峰会](https://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247488709&idx=1&sn=a915303bed93a4c6b4fcbf6b0fa9ca57)

RISC-V DSP 专家 中科昊芯 2023-08-10 17:54 发表于北京

![输入图片说明](https://foruda.gitee.com/images/1691821739960446198/1fc071f3_5631341.png "屏幕截图")

- 微信号：中科昊芯 HAAWKING2019 
- 功能介绍：北京中科昊芯科技有限公司

 **2023 RISC-V中国峰会（RISC-V Summit China 2023）** 将于 **8月23日至25日** 在 **北京香格里拉酒店** 举行。本届峰会将以“ **RISC-V生态共建** ”为主题，结合当下全球新形势，把握全球新时机，呈现RISC-V全球新观点、新趋势。

![输入图片说明](https://foruda.gitee.com/images/1691821799843699869/0f068196_5631341.png "屏幕截图")

本届峰会采用“主会议+技术研讨会+展览展示+同期活动”的方式，预计将举办超过20场主题活动，拟邀请RISC-V国际基金会、业界专家、企业嘉宾及社区代表等出席，预计将吸引超过百家企业及研究机构、开源技术社区，近百家媒体参与报道。

峰会期间， **昊芯** 将在 **主会场C3展位** 恭候您的莅临，向您重点展示Haawking-HX2000系列DSP最新成员—— **HXS320F280025C** 和 **HXS320F280049C** 以及“ **HaawkFOC** ” **无感FOC电机控制解决方案** ，并在 **8月24日（周四）下午** 的峰会上以《 **HaawkFOC⸺提供专业的无传感器 FOC 电机控制解决方案** 》为题进行演讲，欢迎到场聆听。 

- 峰会时间：2023/08/23-25

- 峰会地点：北京·香格里拉酒店（紫竹院路29号）

- 展位号：主会场C3展位

- 演讲时间：8月24日/周四 18:20 - 18:30

- 演讲题目：《HaawkFOC⸺提供专业的无传感器 FOC 电机控制解决方案》

- 演讲人：康爱红 电机控制工程师 

![输入图片说明](https://foruda.gitee.com/images/1691821916774653847/23d712d9_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1691821922614689600/919e2b70_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1691821928173543522/542110a3_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1691821935898870475/d1d299d7_5631341.png "屏幕截图")

点击了解RISC-V中国峰会完整议程

 **报名通道** 

![输入图片说明](https://foruda.gitee.com/images/1691821946893544443/8fb6da35_5631341.png "屏幕截图")

### 关于昊芯

“智由芯生 创享未来”，昊芯是数字信号处理器专业供应商。作为中国科学院科技成果转化企业，瞄准国际前沿芯片设计技术，依托多年积累的雄厚技术实力及对产业链的理解，以开放积极的心态，基于开源指令集架构RISC-V，打造多个系列数字信号处理器产品，并构建完善的处理器产品生态系统。产品具有广阔的市场前景，可广泛应用于工业控制及电机驱动、数字电源、光伏、储能、新能源汽车、消费电子、白色家电等领域。

### 往届RISCV峰会

- [邀请函丨昊芯邀您参加2023 RISC-V 中国峰会](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247488709&idx=1&sn=a915303bed93a4c6b4fcbf6b0fa9ca57)
- [芯直播丨2022 RISC-V中国峰会同期直播活动，昊芯与您相约！](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247487676&idx=1&sn=1b86a347d94615af0b56ddfd28eeb551)
- [昊芯在2022 RISC-V中国峰会等你来！](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247487608&idx=1&sn=38112ca34f96a835c9fa8d56f21f0272)
- [RISC-V首届中国峰会 丨 中科昊芯赋能产业发展丨 演讲、展示、直播！干货满满！](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247484077&idx=1&sn=2d724dc59e23ba414fe5b1a8ec9d340b)
- [峰会报告一：率先破局，中科昊芯全球首款基于RISC-V的DSP芯片及其应用](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247484077&idx=2&sn=edad6241867ab1f6e330372c822c62d1)
- [峰会报告二：HaawkingIDE—RISC-V DSP集成开发环境](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247484077&idx=3&sn=a2889fed0229fac4af2191cc6c31e809)
- [中科昊芯与您相约首届RISC-V中国峰会，上海见！](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247484028&idx=1&sn=c5ad57655a116982ddcbcdcbd92f138f)

### 科技芯青年，爱生活

昊芯（HAAWKING）是一家专注于芯片设计的企业，总部位于中国。他们在RISC-V架构领域取得了重要的突破，并受到了广泛的认可和赞誉。昊芯的使命是推动国产芯片并加速打破国外芯片在相关领域的垄断。

昊芯在行业内举办了多场顶级峰会，并与来自全球的行业领袖和专家们紧密合作，共同推动RISC-V技术和国产芯片的发展。他们的峰会和活动吸引了众多的参与者，展示了他们的技术实力和行业影响力。

昊芯的产品主要包括基于RISC-V架构的DSP芯片，广泛应用于新能源汽车、储能、光伏等领域。他们自主研发的芯片在性能和功耗方面具有显著的优势，为中国本土产业提供了创新的解决方案，并减少了对国外芯片的依赖。

除了芯片设计，昊芯还积极参与行业交流和合作。他们与各大高校、研究机构和企业建立了广泛的合作关系，推动着行业的发展和创新。他们还举办了多场技术培训和教育活动，分享自身的专业知识和经验，助力更多的人了解和应用RISC-V技术。

昊芯致力于成为中国芯片设计领域的领导者，并通过创新和优质的产品助力中国实现芯片自给自足的目标。他们的发展历程充满了成功和荣誉，受到了行业和市场的广泛认可。

总之，昊芯作为一家芯片设计企业，在RISC-V技术领域取得了重要的突破，致力于推动国产芯片的发展，为中国本土产业提供创新解决方案，并与行业合作伙伴共同推动行业发展和创新。他们的努力和成就为中国芯片产业的崛起做出了重要贡献。

| # | title | date |
|---|---|---|
| 1. | [邀请函丨昊芯邀您参加2023 RISC-V 中国峰会](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247488709&idx=1&sn=a915303bed93a4c6b4fcbf6b0fa9ca57) | 2023-8-10 |
| 2. | [邀请函丨昊芯与您相约ICDIA滴水湖论坛RISC-V国产芯片展](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247488690&idx=1&sn=3a0afd3dc58d70201fa0b3af247a480f) | 2023-7-7 |
| 3. | [邀请函丨昊芯将携“新芯片 新算法”亮相上海慕尼黑电子展](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247488680&idx=1&sn=f4cb83e25c3baa17d4bef645ebd3bf82) | 2023-7-6 |
| 4. | [端午安康](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247488663&idx=1&sn=a853e4f284b6bb894fb6b5818585a5ac) | 2023-6-22 |
| 5. | [儿童节快乐](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247488650&idx=1&sn=6eea5062616901aeb9d8b4d98ac3474f) | 2023-6-1 |
| 6. | [芯活动丨欢迎测评！昊芯参加电子发烧友第二届RISC-V开发板测评大赛](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247488640&idx=1&sn=d7b719f4b76b77644bdfc525522a856d) | 2023-5-26 |
| 7. | [昊芯闻丨共建共享！RISC-V DSP论坛上线！](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247488629&idx=1&sn=f7dd39a6ac531b70d390ebe2e5bfd837) | 2023-5-9 |
| 8. | [五四青年节快乐！](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247488602&idx=1&sn=f5ca1d851f00bab1c3eb1407d341fdaf) | 2023-5-4 |
| 9. | [五一劳动节快乐！](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247488594&idx=1&sn=77327e2d2a3d099b85cd79249c3a0237) | 2023-5-1 |
| 10. | [昊芯闻丨中科院自动化所党委书记牟克雄一行莅临昊芯调研指导](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247488564&idx=1&sn=35b38b67766c6b3097d40d03d18f7774) | 2023-4-13 |
| 11. | [清明](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247488542&idx=1&sn=7260fae8206d504a692ff9af50f7ee56) | 2023-4-5 |
| 12. | [芯生活丨携手前行！中科昊芯2022年会暨团建活动圆满举办](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247488527&idx=1&sn=f49edfbe80dfa790905eeb26a86b33e2) | 2023-3-28 |
| 13. | [女神节快乐](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247488480&idx=1&sn=a5dc9f0f3acc5e0f9973ffba6b8ae769) | 2023-3-8 |
| 14. | [昊芯闻丨喜获一金！昊芯荣获2022 RISC-V生态创新创业大赛一等奖](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247488444&idx=1&sn=f28423b2455413045b7b5213e6f6dce7) | 2023-2-27 |
| 15. | [芯教程丨昊芯HX2000系列芯片助力电机安全在线调试保护](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247488391&idx=1&sn=b6f04e2babfb2f5756639d6572c25f2b) | 2023-2-11 |
| 16. | [元宵节快乐](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247488354&idx=1&sn=d093f6efd109d83d1d4cc64922c4b5b9) | 2023-2-5 |
| 17. | [立春快乐](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247488353&idx=1&sn=54dc4a5919647b1649be96e74c07dfed) | 2023-2-4 |
| 18. | [开工大吉](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247488325&idx=1&sn=e97d25837e5965bd869b9508efc17740) | 2023-1-28 |
| 19. | [兔年大吉](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247488312&idx=1&sn=12e43c623cc1d71735f469fb724a1c04) | 2023-1-22 |
| 20. | [大寒快乐](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247488311&idx=1&sn=ee2ac099c3964b37102d3f197932465a) | 2023-1-20 |
| 21. | [小年快乐](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247488286&idx=1&sn=426c758cdd6fa184c200bee69ba4bcbd) | 2023-1-14 |
| 22. | [昊芯闻丨双喜临门！昊芯同时荣获德勤“中国明日之星”及“海淀明日之星”称号](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247488285&idx=1&sn=d537d9f4fcbeb8433548782239ae0950) | 2023-1-13 |
| 23. | [小寒快乐~](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247488239&idx=1&sn=56672b5ab5025acfd96d7c8f7e0b15cb) | 2023-1-5 |
| 24. | [元旦快乐](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247488220&idx=1&sn=8a6342827d9a9a98b2ed78973b388524) | 2023-1-1 |
| 25. | [芯生活丨祝2022第四季度昊芯小伙伴生日快乐！](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247488212&idx=1&sn=b80aa53eae9695859f431bafffa1e8c9) | 2022-12-30 |
| 26. | [喜讯！中科昊芯获近亿元人民币A 轮融资，自研RISC-V DSP主控芯片加速打破国外垄断](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247488186&idx=1&sn=3a353e3957568f978f2017d6d0b7324d) | 2022-12-23 |
| 27. | [冬至快乐~](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247488174&idx=1&sn=e6f84c3d99a15b6ef83ad3ac01af7f97) | 2022-12-22 |
| 28. | [芯教程丨平头哥助力昊芯HX2000系列芯片LIN本地网络互连](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247488161&idx=1&sn=598739ed2a24b91f043aefddeb6ad318) | 2022-12-14 |
| 29. | [大雪快乐](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247488135&idx=1&sn=27c5e2710fb3dd54bb9915d90926d976) | 2022-12-7 |
| 30. | [昊芯闻丨迈上“新”台阶！中科昊芯荣获北京市“专精特新”企业称号](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247488120&idx=1&sn=5d48852f1287fd7b59bba0d5520ea2f5) | 2022-11-30 |
| 31. | [小雪快乐](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247488091&idx=1&sn=a744dd2dbdafd220d1a0c2ba6ee79936) | 2022-11-22 |
| 32. | [昊芯闻丨喜讯！昊芯荣获硬核中国芯2022年度最具潜力IC设计奖！](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247488083&idx=1&sn=373c76474a6dc9b9ff7ce3225fe3b90f) | 2022-11-18 |
| 33. | [2022慕尼黑华南电子展现场报道，昊芯在2D10展位等你来！](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247488058&idx=1&sn=d94e9399ef01153d9e3297fa801f3813) | 2022-11-15 |
| 34. | [立冬快乐](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247488029&idx=1&sn=1a90e9e486900f4768d0a960318fdea8) | 2022-11-7 |
| 35. | [芯教程丨平头哥助力昊芯HX2000系列芯片专题SCI串口通讯奇偶校验(二)SCI增强FIFO](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247488015&idx=1&sn=6f4a6f085ae9bd4c57e5913a0dd3ab9e) | 2022-11-2 |
| 36. | [昊芯携RISC-V DSP 在2022慕尼黑华南展等你来，我们深圳见！](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247488006&idx=1&sn=b7ada433369cf7ed7028cc1a95da3550) | 2022-10-26 |
| 37. | [霜降快乐](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247487992&idx=1&sn=c0cbb2b4b7fc5c7a3dac91d0c6ee1fd6) | 2022-10-23 |
| 38. | [芯教程丨平头哥助力昊芯HX2000系列芯片专题SCI串口通讯奇偶校验(一)标准SCI](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247487926&idx=1&sn=688bc89d6fc3c84a324f7d9ba5f678d0) | 2022-10-13 |
| 39. | [寒露快乐](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247487904&idx=1&sn=3af1c6f981064a5d6d780e4e21b2baa8) | 2022-10-8 |
| 40. | [国庆快乐！](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247487896&idx=1&sn=5ec6487f5f0b5f3da8e169ca5a4711e8) | 2022-10-1 |
| 41. | [芯生活丨祝2022第三季度昊芯小伙伴生日快乐！](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247487895&idx=1&sn=0f92d34becff0e9bcb894af522d6694c) | 2022-9-29 |
| 42. | [昊芯闻丨喜讯！昊芯荣获HICOOL 2022全球创业大赛三等奖！](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247487827&idx=1&sn=2a8e411778abc4aaf19cb1f875142db4) | 2022-9-27 |
| 43. | [秋分快乐](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247487819&idx=1&sn=217c455d40e751e9f70ed451c49ab9d7) | 2022-9-23 |
| 44. | [芯教程丨平头哥助力昊芯HX2000系列芯片CAN网络通信](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247487808&idx=1&sn=3dcd781cf852ce262da5237bc387cedd) | 2022-9-16 |
| 45. | [中秋快乐](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247487789&idx=1&sn=da2e3076521890fcc251b830583c3697) | 2022-9-10 |
| 46. | [白露快乐](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247487775&idx=1&sn=652b13bdcd0933fddb743268f7e5e9dd) | 2022-9-7 |
| 47. | [喜讯！热烈庆祝昊芯上海办公室正式成立！](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247487725&idx=1&sn=25212457afad5fddade1cee61569c747) | 2022-8-29 |
| 48. | [芯直播丨2022 RISC-V中国峰会同期直播活动，昊芯与您相约！](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247487676&idx=1&sn=1b86a347d94615af0b56ddfd28eeb551) | 2022-8-24 |
| 49. | [处暑快乐](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247487646&idx=1&sn=957b2934e76bac595838152446bd9bc3) | 2022-8-23 |
| 50. | [芯直播丨昊芯携手立创商城线上直播~不见不散！](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247487645&idx=1&sn=59c970845f4b80e7250341417819d9bb) | 2022-8-22 |
| 51. | [昊芯在2022 RISC-V中国峰会等你来！](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247487608&idx=1&sn=38112ca34f96a835c9fa8d56f21f0272) | 2022-8-15 |
| 52. | [芯教程丨平头哥助力昊芯HX2000系列芯片EPWM_PC电源稳压输出](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247487567&idx=1&sn=21c8dcc82a0639d259ca9308eaedb75f) | 2022-8-12 |
| 53. | [立秋快乐](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247487543&idx=1&sn=b263d87f9f41e0bbea93183d81175db4) | 2022-8-7 |
| 54. | [芯生活丨凝心聚力，共创辉煌!--昊芯古北水镇团建](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247487542&idx=1&sn=50bdf188395c49135d4121ac1750398f) | 2022-8-3 |
| 55. | [昊芯闻丨昊芯基于RISC-V打造DSP芯片的初衷、要点和方向](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247487427&idx=1&sn=b95a072ef8459534ac32a81fd52318fa) | 2022-7-28 |
| 56. | [芯教程丨平头哥助力昊芯HX2000系列芯片EPWM_DC空窗滤波功能应用](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247487418&idx=1&sn=0819491eea7f51239ff58834dec7d10a) | 2022-7-27 |
| 57. | [大暑快乐](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247487413&idx=1&sn=9121a3269b1a16c554f4c8b125838bac) | 2022-7-23 |
| 58. | [喜讯！热烈庆祝昊芯深圳办公室正式成立！](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247487367&idx=1&sn=0e0a8f9c75cbc9ab399878f1af360deb) | 2022-7-15 |
| 59. | [「中科昊芯」获近亿元人民币A轮融资，基于RISC-V架构自研DSP芯片，<br>瞄准新能源汽车、储能及光伏等领域加速国产替代](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247487266&idx=1&sn=faa626a3c05c71e354d59067d0f427a7) | 2022-7-11 |
| 60. | [官宣！昊芯完成近亿元A轮融资，引入产业资本加速RISC-V DSP落地发展](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247487196&idx=1&sn=8a7126ad444e862d09017146ab2f229a) | 2022-7-8 |
| 61. | [小暑快乐](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247487138&idx=1&sn=20ae874b3ed793cc4578cbea7efdd823) | 2022-7-7 |
| 62. | [芯生活丨祝2022第二季度昊芯小伙伴生日快乐！](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247487119&idx=1&sn=2a9956c9824742e1898c3b395adcf587) | 2022-7-5 |
| 63. | [昊芯致建党101周年华诞](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247487022&idx=1&sn=09d36924b80a488a3843969b9a77a11b) | 2022-7-1 |
| 64. | [芯教程丨平头哥助力昊芯HX2000系列芯片QEP电机测速](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247486963&idx=1&sn=5ee1d765462f1d78c40dbdee61d60d73) | 2022-6-28 |
| 65. | [夏至快乐](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247486931&idx=1&sn=61d150292a784fe9792f7d226185ce04) | 2022-6-21 |
| 66. | [芯教程丨平头哥助力昊芯HX2000系列芯片电机过流跳闸保护专题(三)比较器超阈值检测](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247486913&idx=1&sn=dd358ee346afc6f5f56742ceea1b2977) | 2022-6-16 |
| 67. | [芯直播丨昊芯携手立创商城线上直播~只等你来！](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247486911&idx=1&sn=6feb842f3104dca80704dc7ad0cd4875) | 2022-6-14 |
| 68. | [芯教程丨平头哥助力昊芯HX2000系列芯片电机过流跳闸保护专题(二)EPWM_DC数字比较](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247486840&idx=1&sn=d6cb924216dc98f5cbb740de5107b0d4) | 2022-6-9 |
| 69. | [芒种快乐](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247486781&idx=1&sn=4245ef6b70f0fc0cdbceec23bb24bc72) | 2022-6-6 |
| 70. | [端午安康](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247486780&idx=1&sn=25eec8bfea5bb9d8bbc1366afad19429) | 2022-6-3 |
| 71. | [芯教程丨平头哥助力昊芯HX2000系列芯片电机过流跳闸保护专题(一)EPWM_TZ错误联防](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247486767&idx=1&sn=b865949598d34e455b5a5b2f2760c150) | 2022-5-31 |
| 72. | [芯教程丨平头哥助力中科昊芯HX2000系列芯片之ECAP_APWM电机备用调速](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247486730&idx=1&sn=7beafdf99a3855acbfa5506d80b159d6) | 2022-5-23 |
| 73. | [小满快乐](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247486728&idx=1&sn=e1d669c339d4cd1c4d8bf26d6fcb63d5) | 2022-5-21 |
| 74. | [芯教程丨平头哥助力中科昊芯HX2000系列芯片专题SCI串口通信（三）FIFO中断通信](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247486695&idx=1&sn=48535d377170d012bd56bc6e68e118fe) | 2022-5-16 |
| 75. | [芯应用丨HXS320F28027PTT在步进电机驱动器的应用](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247486694&idx=1&sn=41df3992ce6610606e8108dba2b7f362) | 2022-5-10 |
| 76. | [立夏快乐](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247486693&idx=1&sn=ad13b6740d3b3cdc2dca99fb3e216982) | 2022-5-5 |
| 77. | [青年节快乐](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247486692&idx=1&sn=dc59155716dccb4fcbe385e7b732d570) | 2022-5-4 |
| 78. | [劳动节快乐](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247486691&idx=1&sn=389be3776c9324f05bf6a957d6d100fb) | 2022-5-1 |
| 79. | [芯教程丨平头哥助力中科昊芯HX2000系列芯片专题SCI串口通信（二）AutoBaud自动波特率](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247486653&idx=1&sn=d7264a82c770073c209b8a0a0090a960) | 2022-4-26 |
| 80. | [芯教程丨平头哥助力中科昊芯HX2000系列芯片之SCI串口通信专题（一）FIFO通信](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247486627&idx=1&sn=0c72166b7c44a82a3e93c3346c677f35) | 2022-4-22 |
| 81. | [RISC-V首届中国峰会 丨 中科昊芯赋能产业发展丨 演讲、展示、直播！干货满满！](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247484077&idx=1&sn=2d724dc59e23ba414fe5b1a8ec9d340b) | 2021-6-24 |
| 82. | [峰会报告一：率先破局，中科昊芯全球首款基于RISC-V的DSP芯片及其应用](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247484077&idx=2&sn=edad6241867ab1f6e330372c822c62d1) | 2021-6-24 |
| 83. | [峰会报告二：HaawkingIDE—RISC-V DSP集成开发环境](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247484077&idx=3&sn=a2889fed0229fac4af2191cc6c31e809) | 2021-6-24 |
| 84. | [中科昊芯与您相约首届RISC-V中国峰会，上海见！](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247484028&idx=1&sn=c5ad57655a116982ddcbcdcbd92f138f) | 2021-6-18 |
| 85. | [全球首款RISC-V DSP即将量产 丨 国产芯片四大件](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247483886&idx=1&sn=f3be568b7a4b8ca9692878ef30aff941) | 2021-4-2 |
| 86. | [中自投资专访李任伟：智造“中国芯”，构建RISC-V微处理器新生态](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247483679&idx=1&sn=b1be11dd71061f3c7096f22e0fdaff56) | 2019-3-27 |
| 87. | [昊芯出品 丨 计算机视觉模组和处理器深度调研报告](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247483677&idx=1&sn=0ea5e8ffe517dc751fabccd81a2b4ff2) | 2019-3-23 |
| 88. | [你好，我是中科昊芯！](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247483666&idx=1&sn=87f2e35d9afec56df1193fbf99f44943) | 2019-3-21 |
| 89. | [耐威科技参股子公司，专攻导航与DSP芯片](http://mp.weixin.qq.com/s?__biz=Mzg5NDExMTY1NQ==&mid=2247483656&idx=1&sn=f49c3670634406fb8a6d6a189adc430a) | 2019-2-26 |



![输入图片说明](https://foruda.gitee.com/images/1691860521631124268/ac085b01_5631341.jpeg "科技芯青年副本.jpg")
