# [直播预告 |开源芯片系列讲座第10期：全球第一台RISC-V笔记本ROMA和HPC原生开发](https://mp.weixin.qq.com/s?__biz=MzkwMzM5MTEyMQ==&mid=2247484101&idx=1&sn=9006a8c8b6582a0f8a2d2300b45f1d10)

厦门市开源芯片产业促进会 2023-06-02 19:00 发表于福建

![输入图片说明](https://foruda.gitee.com/images/1692079354290833367/36706856_5631341.png "屏幕截图")

- 微信号: 厦门市开源芯片产业促进会 gh_4afe58a421f8
- 功能介绍: 厦门市开源芯片产业促进会由中科（厦门）数据智能研究院牵头发起设立，宗旨是为集成电路相关的行业优质企业在开源芯片的产品开发、生产、应用及项目投融资、资源对接与整合等方面提供信息、人才、技术、资金、政策等相关支持与服务。

![输入图片说明](https://foruda.gitee.com/images/1692079382534022213/c6e31aeb_5631341.png "屏幕截图")

[![输入图片说明](https://foruda.gitee.com/images/1692079436912364628/ade2b2af_5631341.png "屏幕截图")](https://mp.weixin.qq.com/s?__biz=MzkwMzM5MTEyMQ==&mid=2247484101&idx=1&sn=9006a8c8b6582a0f8a2d2300b45f1d10)

### 鹭岛论坛 开源芯片系列讲座第10期  「 全球第一台 RISC-V 笔记本 ROMA 和 HPC 原生开发」

 **6月28日20：00 图片 精彩开播** 

 **期待与您云相聚，共襄学术盛宴！** 

![输入图片说明](https://foruda.gitee.com/images/1692079488788919073/bbca8241_5631341.png "屏幕截图")

### 直播信息

 **报告简介** 

本次分享梁宇宁将带来世界上第一台 RISC-V SoC 驱动的笔记本电脑 ROMA 的开发历程，以及鉴释科技、深度数智和合作伙伴们的 RISC-V 生态规划和进展。

- 全球第一台RISC-V笔记本
- 基于 28nm RISC-V SoC 的 SoM 人工智能机器人
- ROMA 的核心 RISC-V SoC 驱动的 SoM 模块及其基础开发板

同时，还将分析原生软件开发方法的重要性，发布在笔记本电脑 ROMA 上运行的鉴释科技 HPC 原生编译器，并演示在基于 SoM 的开发板或产品上开发 seL4 应用程序。

 **报告嘉宾** 

- 梁宇宁
- 鉴释科技CEO
- 深度数智顾问

 **特邀主持** 

- 笨叔
- 奔跑吧Linux社区 主理人

 **讲座时间** 

2023年6月28日（周三）20:00-21:00

 **讲座形式** 

- 视频号、B站、电子发烧友、
- 蔻享学术、智能制造、抖音等
- 多平台同步直播图片

点击  预约  精彩不容错过！👇

[![输入图片说明](https://foruda.gitee.com/images/1692079436912364628/ade2b2af_5631341.png "屏幕截图")](https://mp.weixin.qq.com/s?__biz=MzkwMzM5MTEyMQ==&mid=2247484101&idx=1&sn=9006a8c8b6582a0f8a2d2300b45f1d10)

![输入图片说明](https://foruda.gitee.com/images/1692079601980924929/4a2abaa0_5631341.png "屏幕截图")

直播海报

 **诚邀各界嘉宾** 

 **线上参会、扫码关注图片** 

「 开芯会」交流群

![输入图片说明](https://foruda.gitee.com/images/1692079621299971130/88515ab4_5631341.png "屏幕截图")

欢迎所有对RISC-V感兴趣的朋友扫码加入，芯友们可以互动交流，深入探讨~

「 开芯会」视频号

![输入图片说明](https://foruda.gitee.com/images/1692079630711580263/12f611b3_5631341.png "屏幕截图")

关注「 开芯会」视频号

扫码预约观看直播

### 报告嘉宾

![输入图片说明](https://foruda.gitee.com/images/1692079647528656622/6b8409c7_5631341.png "屏幕截图")

 **梁宇宁** 

梁宇宁，鉴释科技创始人兼CEO，同时担任深度数智资深顾问。深度数智专注开发基于 RISC-V SoM 的创新科技产品，从第一款RISC-V 开发笔记本电脑 ROMA，到 AR 眼镜、AI 机器人和 AV 汽车等。梁宇宁的职业生涯从英国到瑞士、再到韩国，最后来到中国。他在嵌入式、平台 API和系统软件方面拥有强大的实践背景。

![输入图片说明](https://foruda.gitee.com/images/1692079673140545033/dac3f90d_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1692079688774536989/d65ef059_5631341.png "屏幕截图")

END

![输入图片说明](https://foruda.gitee.com/images/1692079706777572052/c2c7b65f_5631341.png "屏幕截图")

### [鹭岛论坛](https://mp.weixin.qq.com/mp/appmsgalbum?__biz=MzkwMzM5MTEyMQ==&action=getalbum&album_id=2536545883006402561)

| # | title 开源芯片系列讲座 | date | img |
|---|---|---|---|
| 13. | 直播预告 - 第11期：[ENCORE:一个面向处理器的敏捷验证框架](http://mp.weixin.qq.com/s?__biz=MzkwMzM5MTEyMQ==&amp;mid=2247484121&amp;idx=1&amp;sn=b241c5c6cef4ac6e920a88d5c4aacf24) | 07/11 | <img height="69px" src="https://foruda.gitee.com/images/1692080327906639684/e7e70320_5631341.png"> |
| 12. | 明日直播 - 第10期：[全球第一台RISC-V笔记本ROMA和HPC原生开发](http://mp.weixin.qq.com/s?__biz=MzkwMzM5MTEyMQ==&amp;mid=2247484109&amp;idx=1&amp;sn=5b5aae89f89457fc2230d3410a705ad3) | 06/27 | <img height="69px" src="https://foruda.gitee.com/images/1692080406953937629/e0830e5a_5631341.png"> |
| 11. | 直播预告 - 第10期：[全球第一台RISC-V笔记本ROMA和HPC原生开发](http://mp.weixin.qq.com/s?__biz=MzkwMzM5MTEyMQ==&amp;mid=2247484101&amp;idx=1&amp;sn=9006a8c8b6582a0f8a2d2300b45f1d10) | 06/02 | <img height="69px" src="https://foruda.gitee.com/images/1692080406953937629/e0830e5a_5631341.png"> |
| 10. | 明日开播 - 第09期：[RISC-V软硬件协同设计全流程软件栈](http://mp.weixin.qq.com/s?__biz=MzkwMzM5MTEyMQ==&amp;mid=2247484076&amp;idx=1&amp;sn=251997504ccd868964246a7547f7fdc0) | 05/09 | <img height="69px" src="https://foruda.gitee.com/images/1692080452047002920/d695159c_5631341.png"> |
| 9. | 第09期：[RISC-V软硬件协同设计全流程软件栈](http://mp.weixin.qq.com/s?__biz=MzkwMzM5MTEyMQ==&amp;mid=2247484060&amp;idx=1&amp;sn=dda555322a1d1d5c769099ca977f2972) | 04/23 | <img height="69px" src="https://foruda.gitee.com/images/1692080452047002920/d695159c_5631341.png"> |
| 8. | 第08期：[RISC-V计算软件栈与高性能计算进展](http://mp.weixin.qq.com/s?__biz=MzkwMzM5MTEyMQ==&amp;mid=2247484020&amp;idx=1&amp;sn=f474d614991ffcc23a358446a58ea7a1) | 03/31 | <img height="69px" src="https://foruda.gitee.com/images/1692080520168090486/50acfe8d_5631341.png"> |
| 7. | 第07期：[基于RISC-V的Linux发行版及软件生态](http://mp.weixin.qq.com/s?__biz=MzkwMzM5MTEyMQ==&amp;mid=2247483948&amp;idx=1&amp;sn=ec5b8e7a8a77ce8ccc49277d7696f48b) | 2022/12/07 | <img height="69px" src="https://foruda.gitee.com/images/1692080551866156274/415fd9fc_5631341.png"> |
| 6. | 第06期：[这是最坏的时代，也是最好的年代](http://mp.weixin.qq.com/s?__biz=MzkwMzM5MTEyMQ==&amp;mid=2247483929&amp;idx=1&amp;sn=193fa4143d3c0dc60bec6e30e3314931) | 2022/10/26 | <img height="69px" src="https://foruda.gitee.com/images/1692080584890132535/7c76e4e8_5631341.png"> |
| 5. | 第05期：[Chiplet技术的发展回顾及其互联协议分析](http://mp.weixin.qq.com/s?__biz=MzkwMzM5MTEyMQ==&amp;mid=2247483795&amp;idx=1&amp;sn=9beff75ff7875aef39b51c77ffacb25a) | 2022/09/21 | <img height="69px" src="https://foruda.gitee.com/images/1692080624476441076/8d268188_5631341.png"> |
| 4. | 第04期：[RISC-V在中国的发展与实践](http://mp.weixin.qq.com/s?__biz=MzkwMzM5MTEyMQ==&amp;mid=2247483726&amp;idx=1&amp;sn=b1854f21743dea339edd2deaae28274e) | 2022/08/29 | <img height="69px" src="https://foruda.gitee.com/images/1692080654483549370/456e6120_5631341.png"> |
| 3. | 第03期: [RISC-V国内外发展现状](http://mp.weixin.qq.com/s?__biz=MzkwMzM5MTEyMQ==&amp;mid=2247483672&amp;idx=5&amp;sn=657e1bf45dfd52ff3a7a59f924610857) | 2022/08/18 | <img height="69px" src="https://foruda.gitee.com/images/1692080681209878079/f31aa169_5631341.png"> |
| 2. | 第02期：[EDA之测试综合](http://mp.weixin.qq.com/s?__biz=MzkwMzM5MTEyMQ==&amp;mid=2247483672&amp;idx=4&amp;sn=538a5af0d81dffa605dc33b39ee9b3af) | 2022/08/18 | <img height="69px" src="https://foruda.gitee.com/images/1692080715021520831/bb11134f_5631341.png"> |
| 1. | 第01期：[中国开放指令生态联盟 & 香山RISC-V处理器介绍](http://mp.weixin.qq.com/s?__biz=MzkwMzM5MTEyMQ==&amp;mid=2247483672&amp;idx=3&amp;sn=66e6325027ff2c4aa0052096877a68f5) | 2022/08/18 | <img height="69px" src="https://foruda.gitee.com/images/1692080755306941433/66f22382_5631341.png"> |