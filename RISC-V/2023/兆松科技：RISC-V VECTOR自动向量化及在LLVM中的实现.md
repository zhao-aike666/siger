https://www.terapines.com/post/695/

# [RISC-V VECTOR自动向量化及在LLVM中的实现](https://www.terapines.com/post/695/)
一条评论 / 编译器 / 作者： Aries

本来想取一个爆炸的标题《可能是全球范围内最好的RISC-V Vector自动向量化器的实现》，但是本着科学严谨的态度，以及对未知事物的敬畏，还是取了这个没有吸引力的标题，这样即使被打脸了，也还有挽回的余地。

RISC-V Vector与目前市面上大部分SIMD的向量指令集是不同的，它有点类似于ARM的SVE和SVE2可变长向量指令，但又不完全一样。比如在ARM中SVE/SVE2变长向量指令集可以在程序启动的时候配置向量寄存器大小，运行时动态设置元素宽度，而RISC-V Vector扩展更加灵活，可以使用vsetvl指令，在运行时动态调整寄存器大小，组数，元素数据宽度，以及掩码操作等等。这样的架构对于指令集来说好处是解决了对未来的兼容性，并且可以让同一份可执行文件，在不同架构的向量处理器中充分利用各种不同宽度的向量处理器，实现一份代码，兼容不同硬件架构，从而大大降低了软件的维护成本。

不过，由于目前主流的编译器框架llvm,gcc只支持SIMD类型的自动向量化，EPI(European Processor Initiative 欧洲超算中心的一个项目)虽然基于llvm做了一些初步的RISC-V可变长自动向量化的工作，但是实现的非常粗糙，并且产生的RVV代码质量比较差。据悉，目前上游RISC-V相关的维护团队已经放弃了在gcc当中实现RVV自动向量化的想法，llvm上游也迟迟未动工。

所以兆松科技基于llvm框架，实现了一套高质量的RVV自动向量化器。据当下可获取的信息，可以说我们实现的自动向量化器，是目前业内最完整，质量最好的自动向量化器。目前使用zcc编译我们自研的DSP函数库，大部分循环均可以产生高质量的RVV指令，对于一些更复杂的循环，我们也正在研发更好的自动向量化机制，目标是降低高性能函数库优化的门槛，坚决避免手撸RVV intrinsic甚至手撸汇编，提高底层函数库的可移植性以及降低函数库的维护成本。

文后提供了兆松团队成员晏明参加OSDT2021的视频和PPT，其中包括zcc编译器当中所实现的RVV自动向量化器，以及对RVV架构的简介，横向对比了EPI以及llvm上游的实现质量（其实上游只对RVV简单的做了一些intrinsics实现而已）。

### OSDTConf2021视频：

https://www.bilibili.com/video/BV1bq4y1B7aV/

### OSDTConf2021演示文稿：

![输入图片说明](https://foruda.gitee.com/images/1692901857815022074/5d3f4b14_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1692901865254930392/dcdfe6ee_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1692901872333493247/01687187_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1692901878234306514/5e243c4f_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1692901884172732779/f0996842_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1692901889115837638/7231b3d8_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1692901896462358164/c99c10f7_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1692901901481106298/99da1706_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1692901908450243380/e8d094d6_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1692901912817930270/c9f669ec_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1692901919939074448/65556642_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1692901924154700011/02079371_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1692901930306242485/743aeffa_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1692901934690285728/9bad3c41_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1692901939715969748/f9aea677_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1692901944810842948/65d5fc3d_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1692901951223636962/a5203940_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1692901956746661727/fdeae9ab_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1692901962954605950/5f7604a5_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1692901969794725613/7da1b8f3_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1692901974325124771/df81ffb0_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1692901979550795612/d84e769e_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1692901985554639003/029fc2dd_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1692901993069775659/6eda82a9_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1692901996801100773/a4e61447_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1692902002520747723/73327982_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1692902009076250246/0daa34d2_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1692902014480097676/189a5030_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1692902019772593770/bbc6b4ff_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1692902028249939441/abfdbbd7_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1692902033552037947/4a512e4a_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1692902038797450308/788bfa5a_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1692902045410371369/79405065_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1692902051369679514/218b5111_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1692902056317481320/a9e7b079_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1692902062621936670/96416291_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1692902069197784636/eeb01bdf_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1692902073707450851/7ec9f5d7_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1692902078324102276/25b758f0_5631341.png "屏幕截图")
