https://openhw.org/index.html

- [2022 AMD-Xilinx 暑期学校 Summer School](https://openhw.org/summer_school.html)
- [OpenHW 10year Ceremony Event Overview](https://www.bilibili.com/video/BV1pZ4y1C73T/)
- [OpenHW 2021 优秀竞赛作品分享](https://space.bilibili.com/519580335)

<p><img width="706px" src="https://foruda.gitee.com/images/1675617647487738023/40af8301_5631341.jpeg" title="523OpenHW副本.jpg"></p>

> 一张封面的活动合影吸引了我，酒店大堂背景的影壁太像芯片啦，影壁周围还布满了引脚。这是本期专题立项的最直接印象，而 OpenHW 的故事当然和 SIGer 创刊之初的一个采访有关，嘉宾正式 OpenHW 的创造者。在本月学习FPGA的过程中，查看到了一篇文章引用了当时的 blog 。现在首页清爽干练的信息脉络，推介的优秀项目使用的是 FPGA 初始之时的 ZYNQ 红板，尽管现在已经被 PYNQ 替代，经典不可替代。所以本期专题是一篇故事帖，之后相关的专题会按照本期素材线索，单独征求转载。

![输入图片说明](https://foruda.gitee.com/images/1675428050403369423/70a2ddf7_5631341.png "屏幕截图")

# 主页 https://openhw.org/index.html 

| [资源](https://openhw.org/resource.html) | [赛事](https://openhw.org/contest.html) | [集锦](https://openhw.org/gallery.html)

![输入图片说明](https://foruda.gitee.com/images/1675428208800340326/1ff40cde_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1675428254524708257/91658a5b_5631341.jpeg "Hackathon.jpg")

![输入图片说明](https://foruda.gitee.com/images/1675428280245129073/8b7addf5_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1675428389651697658/9ddcf854_5631341.jpeg "2015.jpg")

![输入图片说明](https://foruda.gitee.com/images/1675428420291797467/7fec8b72_5631341.png "屏幕截图")

### Featured Projects

- 更多项目（集锦） https://openhw.org/gallery.html

  - PYNQ_OpenHW https://space.bilibili.com/519580335

    <p><a href="https://www.bilibili.com/video/BV1tL411K77a"><img width="39.69%" src="https://foruda.gitee.com/images/1675571462406151518/173f3894_5631341.jpeg" title="https://openhw.org/images/projects/2021/OpenHW/2.jpg"></a> <a href="https://www.bilibili.com/video/BV1RF411E7WR"><img width="39.69%" src="https://foruda.gitee.com/images/1675571470535184348/3202590c_5631341.jpeg" title="https://openhw.org/images/projects/2021/OpenHW/3.jpg"></a> <a href="https://www.bilibili.com/video/BV1L44y1n7Cu"><img width="39.69%" src="https://foruda.gitee.com/images/1675571621365959737/e2da4975_5631341.jpeg" title="https://openhw.org/images/projects/2021/OpenHW/4.jpg"></a> <a href="https://www.bilibili.com/video/BV1aS4y1r73C"><img width="39.69%" src="https://foruda.gitee.com/images/1675571630290115750/981e9a18_5631341.jpeg" title="https://openhw.org/images/projects/2021/OpenHW/5.jpg"></a> <a href="https://www.bilibili.com/video/BV1Ni4y127nY"><img width="39.69%" src="https://foruda.gitee.com/images/1675571638750886275/a91b0756_5631341.jpeg" title="https://openhw.org/images/projects/2021/OpenHW/6.jpg"></a> <a href="https://www.bilibili.com/video/BV14L411K7i3"><img width="39.69%" src="https://foruda.gitee.com/images/1675571648037935234/e447a252_5631341.jpeg" title="https://openhw.org/images/projects/2021/OpenHW/7.jpg"></a> <a href="https://www.bilibili.com/video/BV1Di4y117nn"><img width="39.69%" src="https://foruda.gitee.com/images/1675571656666091452/122f5902_5631341.jpeg" title="https://openhw.org/images/projects/2021/OpenHW/10.jpg"></a> <a href="https://www.bilibili.com/video/BV12R4y1L7cB"><img width="39.69%" src="https://foruda.gitee.com/images/1675571664678009268/a24855ff_5631341.jpeg" title="https://openhw.org/images/projects/2021/OpenHW/11.jpg"></a></p>

  - posters

    <p><a href="https://openhw.org/images/poster_22/xkd.pdf"><img width="39.69%" src="https://foruda.gitee.com/images/1675572239504942546/842868a6_5631341.jpeg" title="https://openhw.org/images/poster_22/xkd.jpg"></a> <a href="https://openhw.org/images/poster_22/xju.pdf"><img width="39.69%" src="https://foruda.gitee.com/images/1675573361823969649/d6ddc7ad_5631341.jpeg" title="https://openhw.org/images/poster_22/xju.jpg"></a> <a href="https://openhw.org/images/poster_22/tdse.pdf"><img width="39.69%" src="https://foruda.gitee.com/images/1675572317481611652/567f137d_5631341.jpeg" title="https://openhw.org/images/poster_22/tdse.jpg"></a> <a href="https://openhw.org/images/poster_22/riscv.pdf"><img width="39.69%" src="https://foruda.gitee.com/images/1675572332997404375/6f0ca03d_5631341.jpeg" title="https://openhw.org/images/poster_22/riscv.jpg"></a> <a href="https://openhw.org/images/poster_22/nxd.pdf"><img width="39.69%" src="https://foruda.gitee.com/images/1675573373284806633/69aa85d4_5631341.jpeg" title="https://openhw.org/images/poster_22/nxd.jpg"></a> <a href="https://openhw.org/images/poster_22/mimo.pdf"><img width="39.69%" src="https://foruda.gitee.com/images/1675573384015893085/6fabb009_5631341.jpeg" title="https://openhw.org/images/poster_22/mimo.jpg"></a> <a href="https://openhw.org/images/poster_22/lips.pdf"><img width="39.69%" src="https://foruda.gitee.com/images/1675573399328919414/398c2342_5631341.jpeg" title="https://openhw.org/images/poster_22/lips.jpg"></a> <a href="https://openhw.org/images/poster_22/drug.pdf"><img width="39.69%" src="https://foruda.gitee.com/images/1675573415016556890/5e993e50_5631341.jpeg" title="https://openhw.org/images/poster_22/drug.jpg"></a> <a href="https://openhw.org/images/poster_22/cqu.pdf"><img width="39.69%" src="https://foruda.gitee.com/images/1675573426630044872/a5cd5512_5631341.jpeg" title="https://openhw.org/images/poster_22/cqu.jpg"></a> <a href="https://openhw.org/images/poster_22/ccc.pdf"><img width="39.69%" src="https://foruda.gitee.com/images/1675573437486073677/30d8bf30_5631341.jpeg" title="https://openhw.org/images/poster_22/ccc.jpg"></a></p>

  - githubs

    <p><a href="https://github.com/kongxiangcong/Pulsar-FPGA"><img width="39.69%" src="https://foruda.gitee.com/images/1675571971782311098/40ea7e88_5631341.png" title="https://openhw.org/images/projects/2020/OpenHW/pulsar - 副本.png"></a> <a href="https://github.com/jiangwx/SkrSkr"><img width="39.69%" src="https://foruda.gitee.com/images/1675571982677787762/ca108860_5631341.png" title="https://openhw.org/images/projects/2020/OpenHW/shanghaitech - 副本.png"></a> <a href="https://github.com/dhm2013724/yolov2_xilinx_fpga"><img width="39.69%" src="https://foruda.gitee.com/images/1675571992054512693/271a7d5b_5631341.png" title="https://openhw.org/images/projects/2020/OpenHW/4 - 副本.png"></a> <a href="https://github.com/clancylea/SNN-simulator-NEST_14.0-xilinx_FPGA_cluster"><img width="39.69%" src="https://foruda.gitee.com/images/1675572002846897075/a6ddb306_5631341.png" title="https://openhw.org/images/projects/2020/OpenHW/jiangnan - 副本.png"></a></p>

  - http://openasic.org/

    <a href="http://openasic.org/"><img width="79.69%" src="https://foruda.gitee.com/images/1675572013522715219/e9203752_5631341.png" title="https://openhw.org/images/projects/2020/OpenHW/fudan - 副本.png"></a>



### Programs

- [暑期学校 Summer School](https://openhw.org/summer_school.html)

  > 经过多年的实践和总结形成了有一定影响力并广受同学和老师好评的有特色的暑期学校项目。

    - 活动介绍

      2022 AMD-Xilinx暑期学校（简称“暑期学校”）由信息技术新工科产学研联盟主办，可定制计算人才培养工作委员会联合赛灵思（AMD-Xilinx）、东南大学、南京集成电路人才培训基地联合承办。暑期学校面向全国高校大学生开放，采用1-2两周的基于项目的训练营模式，强调理论学习与动手实践紧密结合，在企业工程师的支持下完成跨学科的最新FPGA技术和项目实训。FPGA暑期学校从2018年开始，截止2021年已培养上千名学员，涵盖了深度学习，物联网、计算机视觉等前沿技术的全栈工程实践。暑期学校的100小时实战理念深受广大学员认可。从2013年与东南大学合作第一届校内暑期学校，到2018年与东南大学以及南京江北新区ICisC联合开展第一届全国暑期学校以来，每一年我们都根据上一届的反馈进行改进，经过多年的实践和总结形成了有一定影响力并广受同学和老师好评的有特色的暑期学校项目。

      2022年第五届FPGA暑期学校将坚持100小时实战理念，带领学员体验从Python到Linux再到RTL的全栈设计方法。尤其适合将要利用Zynq开展毕设，科研或参加竞赛的同学；以及期望在异构平台深入实践CV/DSP/AI等算法软硬协同设计的同学。

      ![输入图片说明](https://foruda.gitee.com/images/1675575104109512056/abb27ec3_5631341.png "屏幕截图")

      2022暑期学校时间表 （[点击跳转至报名链接](https://www.wjx.cn/vj/r3Gs4cn.aspx)，6月20日截止）

    - 活动特色
传统保持：

      - 坚持公益的产学合作授课
      - 基于项目100小时实战训练
      - 坚持在系统设计中学习和实践加深理解

      持续改进：

      - 2018年产学政三方合作首次全国招生
      - 2019年列入信息技术新工科联盟暑期项目
      - 2020年千人在线实践，基础班、进阶班同步进行
      - 2021年引入全球PYNQ HACK，进阶式线上线下交替混合
      - 2022年开通线上实验平台，疫情阻碍不了学习与发展的步伐

    - 暑期学校体验概览

      暑期学校一般由三阶段构成，基础知识储备阶段、训练营阶段、项目后续开发及优化阶段。学员可根据自己的技术水平与需求选择参与，主办方根据完成情况给予相应认证。下面介绍各阶段的活动形式与内容概况.

      - 基础知识储备期

        - 时间：7月
        - 目标：完成专业基础知识的储备
        - 活动内容：

        ![输入图片说明](https://foruda.gitee.com/images/1675575086526544831/881a66e1_5631341.png "屏幕截图")

      - 训练营阶段

        - 时间：8月上旬
        - 目标：能完成一个完整的软硬件协同系统设计项目
        - 活动内容：以2人小队的形式参加为期1周的全栈实战培训并完成前沿领域项目

        ![输入图片说明](https://foruda.gitee.com/images/1675575079145695840/11309215_5631341.png "屏幕截图")

      - 项目后续开发与优化

        - 时间：训练营结束后
        - 目标：结合初步完成的系统，持续优化项目成果
        - 活动内容：与企业技术人员交流完成相应优化的任务，时间相对灵活

        ![输入图片说明](https://foruda.gitee.com/images/1675575069631366948/f4cfa27e_5631341.png "屏幕截图")

    - 往届暑期学校回顾

      - [#FPGA学习 Xilinx Summer School-2020官方视频](https://www.bilibili.com/video/BV1Ab4y1f7b3/)

        经过三个月的发展，IP技术已经不是传统的我们的做数字模型的概念，在这些新的技术推广的过程中，我们发现，学校的这个教学跟产业界的针对人的需求有一定的这个差距。零年前我们就开始跟东南大学合作，还有在上大学开展一个暑期的这个最佳的这个尝试。那经过这几年的这个适应期，我发现这个这样的一个模式是非常有效的，所以，今年我们就是开始把这样的模式全部推广，

        - 陆佳华 Xilinx （暑期学校发起人）
        - 彭竞宇 企业导师（松果机器人CEO）
        - 郭丰收 企业导师（镭神科技研发总监）
        - 王运富 企业导师（华为云资深工程师） 对学生，这个未来自己的职业规划
        - 张帆 企业导师（深鉴科技应用工程师 DEEPHi） 后面我们也非常希望
        - 汤勇明 暑期学校发起人（东南大学电子学院副院长） 我们也是希望学生通过这次
        - 冯志强 企业导师（依元素技术支持经理）本次暑期学校所取得的良好效果

        而心率高，而且确实是抱着学习的态度，学到这种知识的态度来的，我们企业有爱心面的一些需求，然而这个平时，他想做一些项目，他又找不到一个合适的一切机会，这个平台，刚好给我们提供了这种机会。对学生们这个未来的自己的职业的规划或者是研研究发现的时候一定非常好，指引是后面我们也非常希望能够参与到这样的教学，教学我们来，我们也是希望学生通过这一个学校，那么把更多的数学研团队，学研的知识大家能掌握到，本次的数据学校都取得了良好效果，我们在高校新东方建设校区。也会很好的工作，提到了华为，

        - （松果）而我们也可以从学生这里学到一些 而我们，
        - 发掘一些优秀的在校大学生
        - 每个老师都很负责任，很用心
        - 感觉对这个行业的新的趋势
        - （学生）了解更多的有关赛灵思软件
        - （学生） 互相和同龄人了解，还可以了解FPGA，两点很好

        而我们，也会从学院这里学到一些，就是新的东西，可以做一些产品的，发给一些比较优秀的在校大学生或者毕业生，然后能让他们快速的跟社会业界一起去进那把他们学到的知识，让他们的跟他们谈。了解更多的有关304软件操作的一些流程的规范，相对来说接下来了解一下，然后这个可以有一个可以去实现这两点的。

        FOR ADAPTIVE AGILE INTELLIGENCE  
        打造更尖端的人才和世界

        - icisc 南京集成电路产业服务中心
        - 东南大学
        - XILINX
        - HUAWEI

        中国·南京

        @[绿豆汤带你玩转FPGA](https://space.bilibili.com/606612114) https://space.bilibili.com/606612114

        <p><a href="https://mp.weixin.qq.com/s/o2DN4L0KNaPmBUwsYliT8g"><img width="39.69%" src="https://foruda.gitee.com/images/1675575802811068100/b148b422_5631341.png" alt="images/summer/2021.png"></a> <a href="https://mp.weixin.qq.com/mp/appmsgalbum?__biz=MzU0Mzk1MzMyNQ==&amp;action=getalbum&amp;album_id=1491702185896837124&amp;scene=173&amp;from_msgid=2247484666&amp;from_itemidx=1&amp;count=3&amp;nolastread=1#wechat_redirect"><img width="39.69%" src="https://foruda.gitee.com/images/1675575862783893640/22bac467_5631341.png" alt="images/summer/2020.png"></a> <a href="https://mp.weixin.qq.com/s/MZ-I-qT36V7xa3llK9jsvg"><img width="39.69%" src="https://foruda.gitee.com/images/1675575979176653220/98ecbd84_5631341.png" alt="images/summer/2019.png"></a> <a href="https://www.bilibili.com/video/BV1zK4y1d7Cg?spm_id_from=333.999.0.0"><img width="39.69%" src="https://foruda.gitee.com/images/1675601894347395874/72dc5c25_5631341.png" alt="images/summer/2018.png"></a></p>                            

        - #2020年暑假学校（东南大学）

          | # | title | date | img |
          |---|---|---|---|
          | 1. | [盛夏的果实：100小时后暑期学校成果检阅](http://mp.weixin.qq.com/s?__biz=MzU0Mzk1MzMyNQ==&amp;mid=2247484666&amp;idx=1&amp;sn=bb39247a97afe83cf52a2e1e05d2e8c5) | 2020-8-2 | <img width="69px" src="https://foruda.gitee.com/images/1675604878936291462/84379328_5631341.png"> |
          | 2. | [暑期学校优秀项目分享：手写体数学公式计算器](http://mp.weixin.qq.com/s?__biz=MzU0Mzk1MzMyNQ==&amp;mid=2247484698&amp;idx=1&amp;sn=1c77d85af6fc7177e46dd313209a6896) | 2020-8-7 | <img width="69px" src="https://foruda.gitee.com/images/1675604908876977199/6c66142f_5631341.png"> |
          | 3. | [100小时从零开始：用FPGA造一个游戏机](http://mp.weixin.qq.com/s?__biz=MzU0Mzk1MzMyNQ==&amp;mid=2247484714&amp;idx=1&amp;sn=3380fdee1dae6a620ff539265776d9f7) | 2020-8-12 | <img width="69px" src="https://foruda.gitee.com/images/1675604926383348512/ea6afe0f_5631341.png"> |
          | 4. | [100小时从零开始：AI口罩佩戴检测系统](http://mp.weixin.qq.com/s?__biz=MzU0Mzk1MzMyNQ==&amp;mid=2247484731&amp;idx=1&amp;sn=67bfa88e0abbff9f10b2aa61a0fd4a2c) | 2020-8-14 | <img width="69px" src="https://foruda.gitee.com/images/1675604946901225874/f27c0c53_5631341.png"> |
          | 5. | [100小时从零开始：造一台仪器连接物理和数字世界](http://mp.weixin.qq.com/s?__biz=MzU0Mzk1MzMyNQ==&amp;mid=2247484749&amp;idx=1&amp;sn=308b0b62cbc84199f5fefb11ab9b220b) | 2020-8-19 | <img width="69px" src="https://foruda.gitee.com/images/1675604967474015699/5d60d57e_5631341.png"> |
          | 6. | [100小时，从零开始用HLS搭建SSD目标检测系统](http://mp.weixin.qq.com/s?__biz=MzU0Mzk1MzMyNQ==&amp;mid=2247484768&amp;idx=1&amp;sn=5754175f71005b2e3ac0da2aa556b88f) | 2020-8-21 | <img width="69px" src="https://foruda.gitee.com/images/1675604985247704027/79811d4b_5631341.png"> |
          | 7. | [100小时从零开始：用摄像头认识世界](http://mp.weixin.qq.com/s?__biz=MzU0Mzk1MzMyNQ==&amp;mid=2247484787&amp;idx=1&amp;sn=885fa1b8fe50d028534ee21a8998c9cc) | 2020-8-26 | <img width="69px" src="https://foruda.gitee.com/images/1675605000984614601/c6e8c342_5631341.png"> |
          | 8. | [100小时从零开始：双目视觉重建系统](http://mp.weixin.qq.com/s?__biz=MzU0Mzk1MzMyNQ==&amp;mid=2247484809&amp;idx=1&amp;sn=54925e23c4beb1ba91db5312c9f476a0) | 2020-8-28 | <img width="69px" src="https://foruda.gitee.com/images/1675605018515930429/0b94827c_5631341.png"> |
          | 9. | [100小时从零开始：找一把可以打开密码学大门的密钥](http://mp.weixin.qq.com/s?__biz=MzU0Mzk1MzMyNQ==&amp;mid=2247484821&amp;idx=1&amp;sn=dfddba17f06c65f7c174a6bb03589bc2) | 2020-9-2 | <img width="69px" src="https://foruda.gitee.com/images/1675605058794606690/9610db77_5631341.png"> |
          | 10. | [100小时从零开始：失焦图像去模糊系统](http://mp.weixin.qq.com/s?__biz=MzU0Mzk1MzMyNQ==&amp;mid=2247484839&amp;idx=1&amp;sn=b30b97d14b2820a76dfd38dcd95d9a11) | 2020-9-4 | <img width="69px" src="https://foruda.gitee.com/images/1675605076980239831/e82c7881_5631341.png"> |
          | 11. | [PYNQ社区2020回顾](http://mp.weixin.qq.com/s?__biz=MzU0Mzk1MzMyNQ==&amp;mid=2247485191&amp;idx=1&amp;sn=dd9ce9606dd25d6c500799d0fa9c14f6) | 2021-1-1 | <img width="69px" src="https://foruda.gitee.com/images/1675605106141557947/81e83250_5631341.png"> |

- [冬令营 Winter Camp](https://openhw.org/#)

  > 小而精的学术导向交流论坛，与来自学界和工业界的顶尖大牛进行零距离交流。

- [赛事 Competition](https://openhw.org/contest.html)

  > OpenHW与多项竞赛合作，给予优秀项目以支持和帮助，共建OpenHW社区。

  [![输入图片说明](https://foruda.gitee.com/images/1675573918597114395/2ef4278f_5631341.png "屏幕截图")](http://www.fpgachina.cn/)

  - 全国大学生FPGA创新设计竞赛 http://www.fpgachina.cn/

    > 全国大学生FPGA创新设计竞赛是由中国电子学会等主办，旨在推进高校与企业的人才培养合作共建，为社会培养具有创新思维、团队合作精神、解决复杂工程问题能力的优秀人才的赛事。

  [![输入图片说明](https://foruda.gitee.com/images/1675573894540244913/2d7d3162_5631341.png "屏幕截图")](https://openhw.org/cpipc.html)

  - 中国研究生电子设计竞赛 https://openhw.org/cpipc.html

    > 中国研究生电子设计竞赛是由教育部学位与研究生教育发展中心指导，中国学位与研究生教育学会、 中国电子学会等联合主办的研究生学科竞赛， 是“中国研究生创新实践系列大赛”主题赛事之一。

      - 赛事介绍

        中国研究生电子设计竞赛（简称“研电赛”）是由教育部学位管理与研究生教育司指导，中国科协青少年科技中心、中国电子学会联合主办。研电赛是面向全国在读研究生的一项团体性电子设计创新创意实践活动，到2021年这个25周年的比赛已经发展到全国高校及科研院5000余支队伍参与的主流赛事。AMD赛灵思（Xilinx）公司作为FPGA、可编程SoC及ACAP的发明者，提供业界最具活力的处理器技术，实现灵活应变的智能计算，也是研电赛的重要企业合作者。

        2022年将举办第十七届研电赛，本次比赛中Xilinx公司设置了最高奖励达1万元的“Xilinx企业专项奖”，同时Xilinx还将选取若干优秀队伍推荐到Xilinx全球大赛，并对有潜力的队伍进行项目孵化支持。研电赛期间，Xilinx将会开展赛事宣讲以提供更多信息，敬请关注。

        ![输入图片说明](https://foruda.gitee.com/images/1675574219110292664/93cdb866_5631341.png "屏幕截图")

      - 第十七届研电赛时间表（[点击跳转至比赛官网](https://cpipc.acge.org.cn/cw/hp/6)） https://cpipc.acge.org.cn/cw/hp/6

      - 研电赛Xilinx专项奖

        > Xilinx将为2022年第十七届研究生电子竞赛参赛队伍设立 Xilinx特别奖项，所有与Xilinx技术相关的作品皆可申请报名。具体报名时间与方式可参考本年度研电赛官网通知

        - 奖项设置：

          1. Xilinx特别奖1名，奖金1万元人民币；
          2. Xilinx优秀奖3名，最新开发套件一套；
          3. 同时Xilinx将选取若干优秀队伍推荐到Xilinx全球大赛，并对有潜力的队伍进行项目孵化支持。

        - 命题描述：

          1. 提供作品介绍文档（中文与英文各一篇）,至少包含以下内容：

             1. 作品简介
             2. 500字左右的作品介绍，请描述作品来源、功能、架构、创新点/难点、外设清单等
             3. 系统框图清楚展示系统的结构、外设连接、资源分配等信息
             4. 作品展示照片（1-5张）
             5. Github源代码链接（可选项，鼓励开源）

          2. 提供3分钟左右的作品视频文件(或链接)，内容以作品演示为主，避免念PPT。

        - 评审标准：

          | 创新性/实用性 | 难度/工作量 | 作品完成度 |
          |---|---|---|
          | 40% | 30% | 30% |

        - 技术支持：

          Xilinx建议和鼓励所有有意向的队伍提前与我们取得联系，获得平台借用和技术支持。联系邮箱为 xup_china@xilinx.com 可通过“PYNQ开源社区” 微信公众号获取相关的开源设计加速设计部署，同时欢迎加入竞赛交流qq群：702548434交流!

  [![输入图片说明](https://foruda.gitee.com/images/1675573848826057693/c672e2e1_5631341.png "屏幕截图")](https://xupsh.github.io/ccc2021/)

  - 定制计算算法实现挑战赛 https://xupsh.github.io/ccc2021/

    > 2021定制计算算法实现挑战赛由CCF体系结构专委主办，北京大学CECA和Xilinx学术合作部联合承办，是首次发起的基于HLS的算法到硬件实现编程挑战赛，不仅考察编程能力还考察算法硬件实现能力。

      ![输入图片说明](https://foruda.gitee.com/images/1675574589170874309/0f2b721c_5631341.jpeg "cover_new2.jpg")

- [创客马拉松 Hackthon](https://openhw.org/#)

  > 基于项目导向学习PBL（Project Based Learning）的模块化Hackathon活动。

### 关于OpenHW

- OpenHW是什么？ <img align="right" src="https://foruda.gitee.com/images/1675428632920177557/5b53b73e_5631341.png">

  > OpenHW是一个开放包容的学习平台，汇聚了各种赛事信息以及学习资料。本网站将不断更新有用、有趣的信息及知识，欢迎各位老师同学关注并参与我们的活动。

- 如何联系我们？

  > xup_china@xilinx.com

- PYNQ_OpenHW https://space.bilibili.com/519580335

  - [OpenHW 10year Ceremony Event Overview](https://www.bilibili.com/video/BV1pZ4y1C73T/) | 309 | 0 | 2022-3-1

    <p><img width="96.396%" src="https://foruda.gitee.com/images/1675434163586648860/98afd82d_5631341.jpeg" title="001.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675434182512224564/43c20a17_5631341.jpeg" title="002.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675434192675662673/b8e60246_5631341.jpeg" title="003.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675434201447673831/e2cfd88e_5631341.jpeg" title="004.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675434211149513206/67a6965e_5631341.jpeg" title="005.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675434221206937424/972bfa7c_5631341.jpeg" title="006.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675434230787251162/2da895a5_5631341.jpeg" title="007.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675434239850493255/bda4c770_5631341.jpeg" title="008.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675434250070543706/2a03a57d_5631341.jpeg" title="009.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675434258736528818/835503ff_5631341.jpeg" title="010.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675434267906922054/bc608493_5631341.jpeg" title="011.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675434277173926860/777b1e7d_5631341.jpeg" title="012.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675434286309708706/18526219_5631341.jpeg" title="013.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675434294887635697/af98fe4a_5631341.jpeg" title="014.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675434303963329635/47f0dc9d_5631341.jpeg" title="015.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675434312675321464/fc0d6646_5631341.jpeg" title="016.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675434321489467324/90c78095_5631341.jpeg" title="017.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675434329982182673/a39498c1_5631341.jpeg" title="018.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675434341108438270/5b73a036_5631341.jpeg" title="019.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675434349470607902/1192c5c1_5631341.jpeg" title="020.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675434358856337521/26791504_5631341.jpeg" title="021.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675434367156094218/6035ba36_5631341.jpeg" title="022.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675434375405227355/15cdd7f6_5631341.jpeg" title="023.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675434482027702220/22bfab1f_5631341.jpeg" title="024.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675434490963988873/b8b600f2_5631341.jpeg" title="025.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675434499197105810/304cf6b6_5631341.jpeg" title="026.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675434508495857163/23392024_5631341.jpeg" title="027.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675434516798020582/3ceeb851_5631341.jpeg" title="028.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675434528972254690/10ece6a3_5631341.jpeg" title="029.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675434539214739246/7d51ca2f_5631341.jpeg" title="030.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675565940985650303/f8bb8386_5631341.jpeg" title="031.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675565951235903702/323a7bde_5631341.jpeg" title="032.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675565961735886787/6bb66dfd_5631341.jpeg" title="033.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675565971939523755/e64164f4_5631341.jpeg" title="034.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675565984889988443/efc72308_5631341.jpeg" title="035.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566020482671627/9425bba9_5631341.jpeg" title="036.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566031452343513/3183184e_5631341.jpeg" title="037.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566041945143258/398d0a9e_5631341.jpeg" title="038.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566051177765239/c93f0ab1_5631341.jpeg" title="039.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566061271347252/73d46de7_5631341.jpeg" title="040.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566072200921705/c3cc9593_5631341.jpeg" title="041.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566081486025165/f15d3acc_5631341.jpeg" title="042.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566090485334834/d238c444_5631341.jpeg" title="043.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566100190483872/8624eb7f_5631341.jpeg" title="044.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566109472790907/732d7f0e_5631341.jpeg" title="045.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566118694381071/dc7eee12_5631341.jpeg" title="046.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566128835163651/1d2e0509_5631341.jpeg" title="047.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566138575891807/bb79a357_5631341.jpeg" title="048.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566147498715663/bb798005_5631341.jpeg" title="049.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566157554335514/e6b63fa5_5631341.jpeg" title="050.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566166970410238/d8ba611b_5631341.jpeg" title="051.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566178134592795/ab3a70b9_5631341.jpeg" title="052.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566188299180710/daad468a_5631341.jpeg" title="053.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566197786142445/7b844062_5631341.jpeg" title="054.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566208231846385/5e037df9_5631341.jpeg" title="055.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566218941329632/b09177d3_5631341.jpeg" title="056.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566234924504198/3bd9f1cc_5631341.jpeg" title="057.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566244443935792/577182a8_5631341.jpeg" title="058.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566259049774223/fad0d6f5_5631341.jpeg" title="059.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566268800293558/41ecaf51_5631341.jpeg" title="060.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566280661129654/1cd299e3_5631341.jpeg" title="061.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566289886876348/afd38352_5631341.jpeg" title="062.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566298203489083/a0663e19_5631341.jpeg" title="063.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566307296571990/6ecf4799_5631341.jpeg" title="064.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566318309335790/cc7435c6_5631341.jpeg" title="065.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566328537812249/8bd73dcd_5631341.jpeg" title="066.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566337781606416/ea7fadb3_5631341.jpeg" title="067.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566347445793005/0f3369b6_5631341.jpeg" title="068.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566357253027749/c9b15ae6_5631341.jpeg" title="069.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566367002559847/d6952455_5631341.jpeg" title="070.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566378427293682/569cd3f4_5631341.jpeg" title="071.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566388844962446/ed44435a_5631341.jpeg" title="072.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566398828908849/0c8353bb_5631341.jpeg" title="073.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566410440265264/6d7c7afd_5631341.jpeg" title="074.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566425766254258/5fb64dde_5631341.jpeg" title="075.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566437396411382/1bd41d51_5631341.jpeg" title="076.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566447610813095/469d690f_5631341.jpeg" title="077.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566457851713858/d82555ef_5631341.jpeg" title="078.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566467252796596/c55930d3_5631341.jpeg" title="079.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566485422152479/db926105_5631341.jpeg" title="080.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566496681080503/9d986fd5_5631341.jpeg" title="081.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566507485880141/270fc9da_5631341.jpeg" title="082.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566519210763930/722796bf_5631341.jpeg" title="083.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566529464583643/adee6580_5631341.jpeg" title="084.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566539004402530/780191af_5631341.jpeg" title="085.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566549448992070/d84967d0_5631341.jpeg" title="086.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566558782566630/9e51ebb8_5631341.jpeg" title="087.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566568330682671/7c7ac9a8_5631341.jpeg" title="088.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566578388795488/10bec443_5631341.jpeg" title="089.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566589033560903/78e483eb_5631341.jpeg" title="090.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566598775629800/28d68c61_5631341.jpeg" title="091.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566610611249324/34ab4628_5631341.jpeg" title="092.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566621373870830/244048f5_5631341.jpeg" title="093.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566631381432738/f4a9515e_5631341.jpeg" title="094.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566641301375900/3a6b84ef_5631341.jpeg" title="095.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566651769302533/d189b5f3_5631341.jpeg" title="096.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566662816916453/9f0117d7_5631341.jpeg" title="097.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566675910326427/bca6325d_5631341.jpeg" title="098.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566685629738989/ae3cfddd_5631341.jpeg" title="099.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566697273958028/d21ba8e5_5631341.jpeg" title="100.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566708519580873/c1126bf2_5631341.jpeg" title="101.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566719836550302/a189405a_5631341.jpeg" title="102.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566731073378517/c8cce6f6_5631341.jpeg" title="103.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566742435209572/2515b1ef_5631341.jpeg" title="104.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566754940823509/573580ff_5631341.jpeg" title="105.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566766961794386/ce5817dc_5631341.jpeg" title="106.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566776746261555/7e6abe3b_5631341.jpeg" title="107.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566787072618110/b0df6b59_5631341.jpeg" title="108.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566798150062657/ae8d164c_5631341.jpeg" title="109.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566810222702139/11d05071_5631341.jpeg" title="110.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566821018907477/22c0fac6_5631341.jpeg" title="111.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566831604371889/fecd4659_5631341.jpeg" title="112.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566841762734045/d83571f6_5631341.jpeg" title="113.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566851428193311/2231e997_5631341.jpeg" title="114.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566860563210858/d488c077_5631341.jpeg" title="115.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566875712835331/76a3b3f6_5631341.jpeg" title="116.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1675566886311310063/4d4e35c7_5631341.jpeg" title="117.jpg"> </p>

  - [OpenHW2021优秀竞赛作品分享之ThundeRiNG](https://www.bilibili.com/video/BV1Ab4y1t7ZK/) | 242 | 0 | 2022-2-18

    ![输入图片说明](https://foruda.gitee.com/images/1675431420479637837/16a52f03_5631341.jpeg "118.jpg")
    
  - [OpenHW2021优秀竞赛作品分享之基于RISC- V的积木式可拼接图形化编程系统](https://www.bilibili.com/video/BV1tL411K77a/) | 2060 | 1 | 2022-2-18

    ![输入图片说明](https://foruda.gitee.com/images/1675431392612084848/638cc745_5631341.jpeg "119.jpg")

  - [OpenHW2021优秀竞赛作品分享之工业模型生产体系遇见FPGA](https://www.bilibili.com/video/BV1RF411E7WR/) | 315 | 0 | 2022-2-18 

    ![输入图片说明](https://foruda.gitee.com/images/1675431316047543530/7bc7afac_5631341.png "屏幕截图")

  - [OpenHW2021优秀竞赛作品分享之基于神经网络的心肺音分离系统](https://www.bilibili.com/video/BV1L44y1n7Cu/) | 593 | 1 | 2022-2-18

    ![输入图片说明](https://foruda.gitee.com/images/1675431543211398197/50b66f55_5631341.jpeg "121.jpg")

  - [OpenHW2021优秀竞赛作品分享之基于FPGA的一种抗量子数字签名加密](https://www.bilibili.com/video/BV1aS4y1r73C/) | 2344 | 0 | 2022-2-18

    ![输入图片说明](https://foruda.gitee.com/images/1675431264361400681/c3606601_5631341.jpeg "122.jpg")

  - [OpenHW2021优秀竞赛作品分享之基于ZYNQ FPGA的高精度双目光学定位导航系统](https://www.bilibili.com/video/BV1Ni4y127nY/) | 5079 | 2 | 2022-2-18

    ![输入图片说明](https://foruda.gitee.com/images/1675431224973860467/bef7e9ca_5631341.jpeg "123.jpg")

  - [OpenHW2021优秀竞赛作品分享之基于数据流的目标检测网络加速器](https://www.bilibili.com/video/BV14L411K7i3/) | 410 | 2 | 2022-2-18
    ![输入图片说明](https://foruda.gitee.com/images/1675431161788318986/26250166_5631341.png "屏幕截图")

  - [OpenHW2021优秀竞赛作品分享之CCC初赛队伍分享](https://www.bilibili.com/video/BV1wu411X7EH/) | 496 | 0 | 2022-2-18

    ![输入图片说明](https://foruda.gitee.com/images/1675430972271567575/80f59679_5631341.jpeg "125.jpg")

  - [OpenHW2021优秀竞赛作品分享之CCC决赛队伍分享](https://www.bilibili.com/video/BV1db4y147FC/) | 549 | 0 | 2022-2-18

    ![输入图片说明](https://foruda.gitee.com/images/1675431084596791230/6b538f36_5631341.jpeg "126.jpg")

  - [基于边缘计算的低功耗全时空生态监控系统](https://www.bilibili.com/video/BV1Di4y117nn/) | 312 | 0 | 2022-2-18

    ![输入图片说明](https://foruda.gitee.com/images/1675430772003588657/c6deff63_5631341.png "屏幕截图")

---

【[笔记](https://gitee.com/shiliupi/A-Z80/issues/I6B7IJ#note_15915342_link)】

- [FPGA开发之RAM IP的使用](https://gitee.com/shiliupi/A-Z80/issues/I6B7IJ#note_15915342_link)
- [https://openhw.org/index.html](https://gitee.com/shiliupi/A-Z80/issues/I6B7IJ#note_16041004_link)
- [封面素材](https://gitee.com/shiliupi/A-Z80/issues/I6B7IJ#note_16054136_link)

# [FPGA开发之RAM IP的使用](https://blog.csdn.net/c602273091/article/details/39694145)

Snail_Walker | 2014-09-30 17:56:09 发布

- 分类专栏： Digital Chip Design 文章标签： fpga RAM IP核 verilog

CORE Generator里有很多的IP核，适合用于各方面的设计。一般来说，它包括了：基本模块，通信与网络模块，数字信号处理模块，数字功能设计模块，存储器模块，微处理器，控制器与外设，标准与协议设计模块，语音处理模块，标准总线模块，视频与图像处理模块等。

在Xilinx的IP核里有xilinx core generator 里面的memory interface generator 和block ram，使用这两个可以使用FPGA内部和外部的RAM。memory interface generator 是 ddr2/ddr3/qdr2 这些外部存储器的接口，block ram 是 fpga 芯片内部片上的存储器。接下来介绍一下block ram。

block ram有三种：单口RAm、简化双口RAM和真双口RAM。

单口：

![输入图片说明](https://foruda.gitee.com/images/1674919168990451927/2f887d67_5631341.png "屏幕截图")

简化双口，A写入，B读出：

![输入图片说明](https://foruda.gitee.com/images/1674919179017526482/14b01fe0_5631341.png "屏幕截图")

真双口，A和B都可以读写：

![输入图片说明](https://foruda.gitee.com/images/1674919187877991390/60c650fd_5631341.png "屏幕截图")          
                
使用IP核，确定数据位宽和深度：（超出地址范围将返回无效数据，在对超出地址范围的数据进行操作的时候，不能够set或者reset）。这里我选择的是16位的位宽，128的深度。

![输入图片说明](https://foruda.gitee.com/images/1674919200632939449/43334642_5631341.png "屏幕截图")

设置操作模式：（写优先，读优先，不改变）

![输入图片说明](https://foruda.gitee.com/images/1674919211221550069/6cccc9d5_5631341.png "屏幕截图")

这里的写优先的意思就是你写入的数据，会出现在输出端口，不管你给的地址是什么。这种好处就是保证了你读出的数据是最新的。

![输入图片说明](https://foruda.gitee.com/images/1674919221636412181/73f9a173_5631341.png "屏幕截图")

读优先指的就是：不管你写入的数据，是先把你要读的数据读出。

![输入图片说明](https://foruda.gitee.com/images/1674919233364279431/5ae82636_5631341.png "屏幕截图")

不改变模式就是正常的模式，该读的时候读，改写的时候写：（一般没有特殊要求就是选这个）

![输入图片说明](https://foruda.gitee.com/images/1674919243165704922/b7687ed9_5631341.png "屏幕截图")

接着写coe文件，打开txt，输入：

```
MEMORY_INITIALIZATION_RADIX = 10;
MEMORY_INITIALIZATION_VECTOR = 512,515,518,522,525,528,531,535,538,54,......12,23；
```

保存之后为coe格式。

这个如果很少就自己输入，如果比较大，比如一幅图片，那就使用matlab吧！

举个栗子，你要生成ROM：


```
          % 生成 ROM 的 .coe文件

                  clc

                  clear all

                  close all

                  x = linspace(0, pi/2 ,1024);     % 在区间[0,2pi]之间等间隔地取1024个点

                  y_cos = cos(x);

                  y_sin = sin(x);


                  y_cos = y_cos * 2^16;   

                  y_sin = y_sin * 2^16;


                  fid = fopen('D:/cos_coe.txt','wt');   fprintf(fid, ' .0f \n ' , y_cos);

                  fclose(fid);


                  fid = fopen('D:/sin_coe.txt','wt');

                  fprintf(fid, ' .0f \n ' , y_sin);     

                  fclose(fid);
```

比特写功能（byte_write）：

当使用8bit一字节的时候没有优先级，而存储在宽度限定为8bit的倍数。当使用9bit一字节的时候，每一个字节都包含一个优先级位，存储限定为9bit的倍数。9bit的一般不用于NO_CHANGE模式。对于双口的RAM，只能是读优先或者写优先。

我们要设置为primitive原语的模式，还可以选择增加复位等功能，但是复位不能异步，只能同步实现。

最后生成了IP核之后，点击你的IP核的下方，然后可以看到你的instance，你就知道怎么用这个IP核了。另外对于IP核的使用，其实你看datasheet那就更好了。

![输入图片说明](https://foruda.gitee.com/images/1674919348349938744/5271c7a5_5631341.png "屏幕截图")    

![输入图片说明](https://foruda.gitee.com/images/1674919358350481366/35a1cf01_5631341.png "屏幕截图")  

### 参考资料：

初始化文件写法：

- http://www.openhw.org/guozhiyang/blog/14-03/302478_4c05f.html
- http://www.openhw.org/guozhiyang/blog/14-03/302479_5e3a4.html#articletop
- http://bbs.eetop.cn/thread-291962-1-1.html
- http://blog.sina.com.cn/s/blog_6f3d37ff01012fea.html

RAM的用法指南：

- http://wenku.baidu.com/view/b98307ddce2f0066f5332283.html

IP核的使用：

- http://wenku.baidu.com/view/6256fa4e767f5acfa1c7cd44.html?re=view

Block ram设计：

- http://wenku.baidu.com/link?url=O5AfZGnBEbW68cMpLbxI1vQ0MgsAiZD2dyIw9NFDRnjlcRgU_ZdDvjidH-ZasUz7Yw8I4RdA2TtvOz7FQK87nhgb0Pz3brhjfcIb100Hc4O

一些问题：

- http://xilinx.eetrend.com/forum/627
- http://xilinx.eetop.cn/viewthread-304021

综合时使用BR：

- http://www.eefocus.com/utoo/blog/10-03/185645_ed1e3.html
