# CRVS2020
https://www.yuque.com/riscv/articles/crvs2020

## 讨论

林宁亚: 明天b站那个下午2.20的那个和3.40以后得关注一下，3.40以后的是plct的，2.20的那个当时说实现了arm原位替换成riscv，跟咱bmc需求有点像，当时想找他们后来没联系上，厦门半导体，三个座机没打通

### Session1. RISC-V生态
http://live.bilibili.com/22389259(上午9:00 - 11:45)

- DMR: 一款RISC-V架构的乱序超标量通用处理器核
- NutShell: 本科生设计的可运行Linux的RISC-V芯片
- 面向AIoT的RISC-V原生操作系统研究
- RISC-V Vector指令集及定制指令再Gem5仿真平台下的实践
- 基于RISC-V向量化指令集优化OpenBLAS和OpenCV初步进展
- 基于Python的"RISC-V+通用AI"SOC框架PyYard

### Session2. RISC-V 处理器及IP设计，RISC-V指令扩展
http://live.bilibili.com/22389259(下午13:30 - 17:30)
### Session3. RISC-V验证平台和SoC平台，RISC-V基础软件和工具
https://live.bilibili.com/22391464(下午13:30 - 17:55)
### Session4. 中国开放指令（RISC-V）联盟河南分中心技术报告会
https://live.bilibili.com/22391470(下午13:30 - 18:00)

## 中国开放指令生态(RISC-V)联盟 2020年度年中技术研讨会
https://mp.weixin.qq.com/s/sxMicUmrJpo_uvqKk3R4iw

中国开放指令生态RISCV联盟 2020-07-16

![输入图片说明](https://images.gitee.com/uploads/images/2021/0324/112236_4f75da33_5631341.png "屏幕截图.png")

### 会议介绍

中国开放指令生态（RISC-V）联盟将于7月18日，召开 “CRVA联盟技术研讨会（CRVA Technical Seminar，CRVS 2020）”。会议将紧密围绕基于RISC-V的芯片生态的关键技术问题，分享RISC-V处理器设计、开发工具、IP核与SoC以及系统软件等领域的前沿研发成果，推动开源芯片设计理念的繁荣发展。


 **会议日期** ：2020年7月18日

 **会议形式** ：线上网络会议

 **会议网站** ：http://crva.ict.ac.cn/crvs2020/



 **直播地址** ：
CRVS 2020 Session RISC-V生态 ：  
http://live.bilibili.com/22389259 (上午9:00 - 11:45) 

CRVS 2020 Session RISC-V 处理器及IP设计，RISC-V指令扩展 ：  
http://live.bilibili.com/22389259 (下午13:30 - 17:30)

CRVS 2020 Session RISC-V验证平台和SoC平台，RISC-V基础软件和工具 ：   
https://live.bilibili.com/22391464 (下午13:30 - 17:55)

中国开放指令（RISC-V）联盟河南分中心技术报告会 ：  
https://live.bilibili.com/22391470 (下午13:30 - 18:00)

![0](https://images.gitee.com/uploads/images/2021/0324/112407_278a5d27_5631341.png "屏幕截图.png")
[![1](https://images.gitee.com/uploads/images/2021/0324/112416_29523697_5631341.png "屏幕截图.png")](https://live.bilibili.com/22389259)
[![2](https://images.gitee.com/uploads/images/2021/0324/112428_7f4adf07_5631341.png "屏幕截图.png")](http://live.bilibili.com/22389259)
[![3](https://images.gitee.com/uploads/images/2021/0324/112439_f4396007_5631341.png "屏幕截图.png")](https://live.bilibili.com/22391464)
[![4](https://images.gitee.com/uploads/images/2021/0324/112509_02225502_5631341.png "屏幕截图.png")](https://live.bilibili.com/22391470)

### 程序委员会

 **主席：** 

- 包云岗	    中科院计算所

 **委员会（按姓名拼音排序）：** 

- 陈   岚    中科院微电子所
- 陈   渝    清华大学
- 丁   然    珠海全志
- 胡振波	    芯来科技
- 孟建熠	    阿里平头哥
- 宋   威    中科院信工所
- 孙轶群	    优矽科技
- 谭章熹	    帕特森RISC-V国际开源实验室（RIOS）
- 唐   丹    中科院计算所
- 武延军	    中科院软件所
- 易江芳	    北京大学
- 张先轶	    澎峰科技

![输入图片说明](https://images.gitee.com/uploads/images/2021/0324/134622_6bd25b21_5631341.png "屏幕截图.png")

## CRVA联盟技术研讨会（CRVA Technical Seminar， CRVS 2020）

中国开放指令生态（RISC-V）联盟将于7月18日，召开 “CRVA联盟技术研讨会（CRVA Technical Seminar， CRVS 2020）”。会议将紧密围绕基于RISC-V的芯片生态的关键技术问题，分享RISC-V处理器设计、开发工具、IP核与SoC以及系统软件等领域的前沿研发成果，共同推动开源芯片设计理念的繁荣发展。

 **会议日期** ：2020年7月18日
 **会议形式** ：线上网络会议

### 一．议题：

会议聚焦技术经验分享，面向在RISC-V生态上取得阶段性技术突破的领域专家，公开征集报告：

- RISC-V处理器核设计，覆盖乱序高性能核、低功耗等多种设计
- 基于FPGA 和IC 的RISC-V芯片设计
- 编译器，工具链（调试器、跟踪器、加载器等）和RISC-V系统软件支持
- 开源EDA设计流程（如RTL仿真器，综合，DFT，布局布线等）
- 领域专用体系结构设计（如AI、IoT、自动驾驶等）
- 基于RISC-V的安全体系结构设计
- RISC-V的系统软件及应用软件接口规范等
- RISC-V在教学领域的实践

### 二、程序委员会：

### 三、会议议程：

#### 1.1 Session：RISC-V生态  Chaired by 宋威(中科院信工所)

- DMR：一款RISC-V架构的乱序超标量通用处理器核   
  孙彩霞（国防科技大学计算机学院） [[video](https://www.bilibili.com/video/BV1KZ4y1g72t)] [[slides](http://crva.ict.ac.cn/crvs2020/index/slides/1-1.pdf)]

- NutShell-本科生设计的可运行Linux的RISC-V芯片  
  王华强（中国科学院大学） [[video](http://https://www.bilibili.com/video/BV1c54y1B7ay)] [[slides](http://crva.ict.ac.cn/crvs2020/index/slides/1-2.pdf)]

- 面向AIoT的RISC-V原生操作系统研究  
  于佳耕（中科院软件所） [[video](https://www.bilibili.com/video/BV1kh411o7e9)][[slides](http://crva.ict.ac.cn/crvs2020/index/slides/1-3.pdf)]

- RISC-V Vector指令集及定制指令在Gem5仿真平台下的实践  
  欧阳鑫（希姆计算） [[video](https://www.bilibili.com/video/BV1rp4y1i7TN)][[slides](http://crva.ict.ac.cn/crvs2020/index/slides/1-4.pdf)]

- 基于RISC-V向量指令集优化OpenBLAS和OpenCV的初步进展  
  张先轶（澎峰科技） [[video](https://www.bilibili.com/video/BV1bA411v7mT)][[slides](http://crva.ict.ac.cn/crvs2020/index/slides/1-5.pdf)]

- 基于Python的“RISC-V+通用AI”SoC框架PyYard  
  陈若晖（华南理工大学） [[video](https://www.bilibili.com/video/BV1MT4y1j7t1)][[slides](http://crva.ict.ac.cn/crvs2020/index/slides/1-6.pdf)]

#### 1.2.1 Session: RISC-V 处理器及IP设计  Chaired by 张先轶(澎峰科技)

- “双模”可配置RISC-V处理器IP设计和实现  
李海忠（芯来科技） [[video](https://www.bilibili.com/video/BV1T54y1B7bW)][[slides](http://crva.ict.ac.cn/crvs2020/index/slides/2-1.pdf)]

- Use RISC-V to build programmable DSA AI Processor  
詹荣开（希姆计算） [[video](https://www.bilibili.com/video/BV1rp4y1i74H)]

- 基于RISC-V指令集的Egret处理器设计  
王旭（厦门半导体投资集团有限公司） [[video](https://www.bilibili.com/video/BV1wt4y1X7nn)][[slides](http://crva.ict.ac.cn/crvs2020/index/slides/2-3.pdf)]

- 分布式执行单元的思想与应用  
刘权胜（上海赛昉科技有限公司（StarFiveTech）） [[video](https://www.bilibili.com/video/BV1zK4y1e72s)][[slides](http://crva.ict.ac.cn/crvs2020/index/slides/2-4.pdf)]

- 基于RISC-V的智能语音交互系统  
曾宪仁（深圳优矽科技） [[video](https://www.bilibili.com/video/BV1Gt4y1X7WW)][[slides](http://crva.ict.ac.cn/crvs2020/index/slides/2-5.pdf)]

#### 1.2.2 Session: RISC-V指令扩展   Chaired by 吕方(中科院计算所)

- RISC-V 向量扩展在Clang/LLVM中的支持  
王鹏（中国科学院软件所，智能软件中心） [[video](https://www.bilibili.com/video/BV1oV411B72i)][[slides](http://crva.ict.ac.cn/crvs2020/index/slides/2-6.pdf)]

- 基于RVV-0.8 SPEC的VPU的设计与集成  
梁展豪（深圳优矽科技） [[video](https://www.bilibili.com/video/BV1ND4y1D7gb)][[slides](http://crva.ict.ac.cn/crvs2020/index/slides/2-7.pdf)]

- 基于LLVM实现RISC-V用户自定义指令支持---以玄铁C910为例  
陈影（中国科学院软件所，智能软件中心） [[video](https://www.bilibili.com/video/BV1H54y1S7xi)][[slides](http://crva.ict.ac.cn/crvs2020/index/slides/2-8.pdf)]

- RISC-V Vector Performance Analysis  
叶锡聪（深圳优矽科技） [[video](https://www.bilibili.com/video/BV1b54y1S7r9)][[slides](http://crva.ict.ac.cn/crvs2020/index/slides/2-9.pdf)]

#### 1.3.1 Session: RISC-V验证平台和SoC平台  Chaired by 常轶松(中科院计算所)

- 支持一致性缓存的Spike仿真器  
宋威（中国科学院信息工程研究所，信息安全国家重点实验室） [[video](https://www.bilibili.com/video/BV1d5411h79R)][[slides](http://crva.ict.ac.cn/crvs2020/index/slides/3-1.pdf)]

- RISC-V QEMU Support for Nuclei SoC Emulation  
高志远（首尔大学） [[video](https://www.bilibili.com/video/BV1pK411J7CY)][[slides](http://crva.ict.ac.cn/crvs2020/index/slides/3-2.pdf)]

- 开源处理器敏捷软硬件协同验证基础平台实现  
陈欲晓（中国科学院计算技术研究所） [[video](https://www.bilibili.com/video/BV16i4y137hd)][[slides](http://crva.ict.ac.cn/crvs2020/index/slides/3-3.pdf)]

- 基于开源测试激励的RV64验证  
倪晓强（国防科技大学计算机学院） [[video](https://www.bilibili.com/video/BV1ez4y1D7xM)][[slides](http://crva.ict.ac.cn/crvs2020/index/slides/3-4.pdf)]

- 基于RISC-V的SoC FPGA芯片互联架构方案  
周文（京微齐力科技有限公司） [[video](https://www.bilibili.com/video/BV1d54y1U7hJ)][[slides](http://crva.ict.ac.cn/crvs2020/index/slides/3-5.pdf)]

#### 1.3.2 Session: RISC-V基础软件和工具 Chaired by 吴伟(中科院软件所)

- 面向64位RISC-V的基础数学库自动化移植  
曹浩 (数学工程与先进计算国家重点实验室) [[video](https://www.bilibili.com/video/BV1va4y1a75H)][[slides](http://crva.ict.ac.cn/crvs2020/index/slides/3-6.pdf)]

- 基于图形化EDA设计的RISC-V教学实践  
赖晓铮 (华南理工大学) [[video](https://www.bilibili.com/video/BV1Z5411h7Fe)][[slides](http://crva.ict.ac.cn/crvs2020/index/slides/3-7.pdf)]

- The standardized boot flow for RISC-V platforms   
Jagan Teki (Amarula Solutions,India) [[video](https://www.bilibili.com/video/BV1JK411J7oL)][[slides](http://crva.ict.ac.cn/crvs2020/index/slides/3-8.pdf)]

- OpenEuler4RISC-V: 构建OpenEuler面向RISC-V的操作系统  
周鹏（中科院软件所；OpenEuler sig RISC-V） [[video](https://www.bilibili.com/video/BV1fV411B7e6)][[slides](http://crva.ict.ac.cn/crvs2020/index/slides/3-9.pdf)]

- 开源开放的RISC-V嵌入式软件平台探索  
方华启（芯来科技Nuclei） [[video](https://www.bilibili.com/video/BV1rK411J7JQ)][[slides](http://crva.ict.ac.cn/crvs2020/index/slides/3-10.pdf)]

#### 1.4.1 Session 4A: 中国开放指令（RISC-V）联盟河南分中心技术报告会  
Chaired by 李栋(中科院计算所大数据研究院)

- 报告1 包云岗
- 报告2 张涛
- 面向RISC-V的系统软件及生态 武延军
- 智联网（AIoT）数据产业报告 孙冶平

#### 1.4.1 Session 4B: 智联网（AIoT）数据感知实验室第一届学术委员会会议暨河南智慧岛智联网（AIoT）产业联盟筹备会  
Chaired by 王元卓(中科院计算所大数据研究院)

- 开场致辞 主持人
- 领导致辞 魏宁娣
- 嘉宾合影
- 智联网(AIoT)数据感知实验室发展报告 李栋
- 智联网(AIoT)数据感知实验室建设研讨结论 主持人
- 讨论成立河南智慧岛智联网（AIoT）产业联盟相关的宗旨、组织形式、章程等 主持人
- 筹建河南智慧岛智联网（AIoT）产业联盟相关结论 筹备委员会
 

### 指导单位：
- 中央网信办信息化发展局
- 工信部信息化和软件服务业司
- 中科院科技促进发展局

### 主办单位：
- 中国开放指令生态（RISC-V）联盟
- 河南省郑州市郑东新区管理委员会

### 协办单位：
- 中国开放指令生态（RISC-V）联盟河南分中心
- 河南省物联网行业协会
- 河南省大数据与人工智能专家委员会
- 中科院计算技术研究所大数据研究院

中国开放指令生态（RISC-V）联盟秘书处  
2020年6月27日