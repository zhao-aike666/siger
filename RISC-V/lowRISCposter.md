<p><img width="48.57%" src="https://foruda.gitee.com/images/1690411608062452846/fcf9491b_5631341.jpeg"> <img width="49.1%" src="https://foruda.gitee.com/images/1690416434547319602/0d642f7f_5631341.jpeg"></p>

# [How we used differential testing to rapidly find and fix missed optimisation opportunities in LLVM's RISC-V backend](https://lowrisc.org/blog/2020/10/how-we-used-differential-testing-to-rapidly-find-and-fix-missed-optimisation-opportunities-in-llvms-risc-v-backend/)

by Luís Marques, October 8, 2020

At this [October 2020 LLVM Developers’ Meeting](http://llvm.org/devmtg/2020-09/) I presented a [poster](https://www.lowrisc.org/blog/2020/10/how-we-used-differential-testing-to-rapidly-find-and-fix-missed-optimisation-opportunities-in-llvms-risc-v-backend/llvm-dev-meeting-oct-2020-poster.pdf) about how, with a surprisingly simple tool, we were able to rapidly identify, isolate, and fix a range of missed optimisation opportunities in LLVM’s RISC-V backend.

The [tool](https://github.com/lowRISC/longfruit) works by generating random C programs, compiling each program with both Clang and GCC (targeting RISC-V) and comparing the assembly generated by both compilers. If it estimates that Clang/LLVM generated worse code than GCC then it saves that case for further analysis. We found that, even with a simple implementation, this tool was surprisingly effective.

The tool works in multiple stages:

- The random code generator (a simple recursive descent generator) directly emits the code, without building an AST. This is easy to customise, so we can use our knowledge of the RISC-V ISA and of the backend to ensure it generates C code that is more likely to identify problematic cases.
- To perform quality estimation and comparison of the assembly output, the tool assigns costs to the individual instructions, and adds up the costs for the entire assembly sequence. An individual instruction cost is determined by its operation kind (load/store/branch/arithmetic/…). The tool detects cases where Clang could improve by checking if its assembly output has a higher total cost than that of GCC.
- Detected cases are minimized by running [C-Reduce](https://embed.cs.utah.edu/creduce/) in combination with the tool. By minimizing the source program while ensuring that the quality difference is preserved we produce concise test cases that isolate code quality issues.

This straight-forward approach has proven to be powerful enough to detect issues across a variety of categories. Examples include:

- Poor constant materialisation sequences;
- Unnecessary sign extensions;
- Using branches instead of RISC-V comparison instructions;
- Not using the offset immediates in some load and store instructions;
- Unnecessary floating-point conversions;
- Dead instructions.

We have already addressed a range of the issues we found. As we take care of all of the low-hanging fruit, we can also keep improving the tool to detect more subtle cases.

If you missed the poster session, you can still watch a 5-minute video summary, shown below.

- https://www.youtube.com/embed/chi74XqWU1c

# 我们如何利用差异测试快速发现并修复LLVM的RISC-V后端中错过的优化机会

by Luís Marques, October 8, 2020

在 [2020 年 9 月](http://llvm.org/devmtg/2020-09/) 的 LLVM 开发人员会议上，我展示了一张[海报](https://www.lowrisc.org/blog/2020/10/how-we-used-differential-testing-to-rapidly-find-and-fix-missed-optimisation-opportunities-in-llvms-risc-v-backend/llvm-dev-meeting-oct-2020-poster.pdf)，讲述了我们如何通过一个非常简单的工具快速识别、隔离和修复 LLVM RISC-V 后端中一系列错失的优化机会。

[该工具](https://github.com/lowRISC/longfruit)的工作原理是生成随机 C 程序，使用 Clang 和 GCC（针对 RISC-V）编译每个程序，并比较两个编译器生成的程序集。如果它估计Clang/LLVM生成的代码比GCC更差，那么它会保存这种情况以供进一步分析。我们发现，即使通过简单的实现，这个工具也非常有效。

该工具分多个阶段工作：

- 随机码生成器（一个简单的递归下降生成器）直接发出代码，而无需构建 AST。这很容易定制，因此我们可以利用我们对 RISC-V ISA 和后端的了解来确保它生成的 C 代码更有可能识别有问题的情况。
- 为了对装配输出进行质量估算和比较，该工具将成本分配给各个指令，并将整个装配序列的成本相加。单个指令成本由其操作类型（加载/存储/分支/算术/...）决定。该工具通过检查其装配输出的总成本是否高于 GCC 来检测 Clang 可以改进的情况。
- 通过将[C-Reduce](https://embed.cs.utah.edu/creduce/)与该工具结合使用，可以最大程度地减少检测到的病例。通过最小化源程序，同时确保保留质量差异，我们生成简洁的测试用例，隔离代码质量问题。

事实证明，这种直截了当的方法足够强大，可以检测各种类别的问题。示例包括：

- 恒定物化顺序差;
- 不必要的符号扩展;
- 使用分支而不是RISC-V比较指令;
- 在某些加载和存储指令中不使用偏移量;
- 不必要的浮点转换;
- 死指令。

我们已经解决了我们发现的一系列问题。当我们处理所有唾手可得的果实时，我们还可以不断改进工具以检测更微妙的情况。

如果您错过了海报会议，您仍然可以观看 5 分钟的视频摘要，如下所示。

- https://www.bilibili.com/video/BV1Rm4y1L7EY/

![输入图片说明](https://foruda.gitee.com/images/1690411608062452846/fcf9491b_5631341.jpeg "llvm-dev-meeting-oct-2020-poster.jpg")

# Quickly Finding RISC-V Code Quality Issues with Differential Analysis

Luís Marques <luismarques@lowrisc.org> LLVM Dev Meeting, Oct 2020

### Problem

 **The Good News** 

- RISC-V target accepted into LLVM
  - Successfully compiled thousands of Linux packages
- Overall high performance generated code
  - Benefiting from LLVM’s target-independent optimizations
  - Benchmark speed results similar to RISC-V GCC

 **The Bad News** 

- We still had cases of poor RISC-V code generation for
various code patterns (e.g. simple expressions)
- These issues had gone unnoticed when we looked at the
generated code for large programs and benchmarks
- How could we quickly find them?

![输入图片说明](https://foruda.gitee.com/images/1690412243430727463/27b82192_5631341.png "屏幕截图")

### Approach

- Project LongFruit: differential analysis of Clang vs GCC
- Python tool
  - The simplest possible implementation that could work
  - Custom random C code generator
    - Recursive descent direct code generator
    - Optimized for our needs (focuses on problematic areas)
  - RISC-V assembly parser and instruction cost estimator
    - Very simple cost model, based on instruction class (ALU, FPU, load/store, branch, etc.)
  - Plumbing to: run the random C code generator; compile the source with both Clang and GCC; analyze the resulting assembly; compare the estimated costs; filter out uninteresting cases; run a code reducer on the source code; save each reduced case to a file

![输入图片说明](https://foruda.gitee.com/images/1690412365399460298/59f5ce1f_5631341.png "屏幕截图")

### Results

- Very simple tool, but highly effective
- Finds candidate issues in a few seconds, reduces them in a few minutes
- Immediately found many cases of low-hanging fruit
  - Manual triage reduced the initial batch to around a dozen independent issues.
- Code quality issues spanned a variety of categories.
- Resulted in multiple patches to address those issues
  - We still have a backlog of issues to address

### Related Work

- Finding Missed Optimizations in LLVM (and other compilers). G. Barany, 2018 European LLVM Developers Meeting.

### Example

 **Inefficient use of offsets in loads and stores** 

 **C source** 

```
float f() {
    return 1.0;
}
```

 **Test Scenario** 

```
RV64GC ILP64D
```

 **GCC 10 Output** 

```
lui a5,%hi(.LC0)
flw fa0,%lo(.LC0)(a5)
ret
```

 **Clang 10 Output** 

```
lui a0, %hi(.LCPI0_0)
addi a0, a0, %lo(.LCPI0_0)
flw fa0, 0(a0)
ret
```

 **Fix**  https://reviews.llvm.org/D79690

 **Clang 11 Output** 

```
lui a0, %hi(.LCPI0_0)
flw fa0, %lo(.LCPI0_0)(a0)
ret
```

### Many more... <img align="right" src="https://foruda.gitee.com/images/1690413632076977285/b7f77a9f_5631341.png">

- Poor constant materializations
- Unnecessary sign extensions
- Use of branches instead of comparison instructions
- Extraneous floating-point conversions
- Dead instructions

[More examples](https://gist.github.com/luismarques/b3cad6e0178a6b1058eca98876b49ef2): 

https://github.com/lowRISC/longfruit

# 通过差分分析快速发现RISC-V代码质量问题

路易斯·马克斯 <luismarques@lowrisc.org> LLVM 开发人员会议，2020 年 <> 月

### 问题

 **好消息** 

- RISC-V目标被LLVM接受
  - 成功编译了数千个 Linux 软件包
- 整体高性能生成的代码
  - 受益于 LLVM 与目标无关的优化
  - 基准速度结果与 RISC-V GCC 相似

 **坏消息** 

- 我们仍然有针对各种代码模式（例如简单表达式）的RISC-V代码生成不佳的情况
- 当我们查看为大型程序和基准测试生成的代码时，这些问题被忽视了。
- 我们如何快速找到它们？

### 方法

- 长果计划：Clang与GCC的差分分析
- 蟒蛇工具
  - 可以工作的最简单的实现
  - 自定义随机 C 代码生成器
    - 递归下降直接代码生成器
    - 针对我们的需求进行优化（专注于有问题的领域）
  - RISC-V汇编解析器和指令成本估算器
    - 非常简单的成本模型，基于指令类（ALU、FPU、负载/存储、分支等）
  - 管道到：运行随机C代码生成器;使用 Clang 和 GCC 编译源代码;分析生成的组件;比较估计成本;过滤掉无趣的案例;在源代码上运行降码器;将每个简化的案例保存到文件中

### 结果

- 非常简单的工具，但非常有效
- 在几秒钟内找到候选问题，在几分钟内减少它们
- 立即发现许多唾手可得的果实
  - 手动分类将初始批次减少到大约十几个独立问题。
- 代码质量问题跨越了多个类别。
- 导致多个修补程序来解决这些问题
  - 我们仍有积压的问题需要解决

### 相关工作

- 在 LLVM（和其他编译器）中查找错过的优化。G. Barany，2018年欧洲LLVM开发者大会。

### 例

 **负载和存储中偏移量的使用效率低下** 

- C 源
- 测试场景
- GCC 10 输出
- 叮当 10 输出
- 修复 https://reviews.llvm.org/D79690
- 叮当 11 输出

### 更多...

- 常数材料化差
- 不必要的标志扩展
- 使用分支而不是比较说明
- 无关的浮点转换
- 死指令

更多示例： https://github.com/lowRISC/longfruit

---

# 【笔记】[How we used differential testing to rapidly find and fix missed optimisation opportunities in LLVM's RISC-V backend](https://gitee.com/RV4Kids/PLCT-Weekly/issues/I7OB3G)

## 来自你的消息:
还是你来帮我介绍下吧：

1. Luís Marques 的背景介绍，他是 LLVM 的主要贡献者吗？或者是维护者？
2.  LLVM Dev Meeting 2020 做一个整体介绍，并介绍下它的成果，比如 LLVM 社区发展的重要成果，主要演讲等等。

## 来自ChatAI的消息:
1. Luís Marques 是 LLVM 社区的活跃贡献者之一，尽管他的具体角色没有明确说明。他在 LLVM 社区做出了多个重要的贡献，并在 RISC-V 相关的编译优化领域展现了专业知识和技术能力。然而，LLVM 项目有着庞大的贡献者群体，其中有许多人为项目做出了重要的贡献。

2. LLVM Dev Meeting 是 LLVM 社区每年举行的一次会议，旨在为开发者和用户提供一个交流和展示最新研究成果的平台。以下是对 LLVM Dev Meeting 2020 的整体介绍和一些主要成果的简要概述：

- 时间和地点：LLVM Dev Meeting 2020 是于 2020 年 10 月举行的一次线上会议，由 LLVM 社区组织和承办。

- 演讲和主题：会议涵盖了广泛的主题，包括编译器优化、语言设计、工具链开发等。参会者可以分享他们的研究成果、实践经验和新技术的应用。

- 主题演讲：LLVM Dev Meeting 2020 有一些重要的主题演讲，涉及 LLVM 社区的一些关键领域和创新。其中一场演讲是 Luís Marques 提到的 "Quickly Finding RISC-V Code Quality Issues with Differential Analysis"，演讲介绍了利用差异分析快速发现 RISC-V 代码质量问题的方法。

- 开源项目展示：会议还提供了一个开源项目展示的机会，让开发者们可以展示和分享他们的项目，促进合作与交流。

- 社区发展的重要成果：LLVM Dev Meeting 是一个聚集了全球 LLVM 社区成员的重要活动，让人们可以互相学习和分享。通过演讲、展示和讨论，会议对 LLVM 社区的发展和创新做出了积极贡献，推动了编译器、语言和工具链等方面的进步与改进。

这些是对 LLVM Dev Meeting 2020 的整体介绍和主要成果的简要概述。具体的演讲和成果可以通过相关资料和会议记录来进一步了解。


- riscv-europe.org
https://riscv-europe.org/media/proceedings/posters/...
[PDF]Relocatable RISC-V Rust Applications for Embedded …
WebJun 2, 2023 · Hugo McNally <hugom@lowrisc.org>Luís Marques <luismarques@lowrisc.org> Problem The RISC-V ecosystem currently lacks an …

- llvm.org
https://lists.llvm.org/pipermail/llvm-dev/2021-July/151962.html
[llvm-dev] Building compiler-rt for RISC-V?
WebJul 28, 2021 · On Tue, Jul 27, 2021 at 11:51 AM Thomas Goodfellow via llvm-dev <llvm-dev at lists.llvm.org> wrote: > however although that yields the RISCV compiler target libraries …

  > 根据你提供的信息，Luís Marques 在 llvm-dev 邮件列表上回复了关于在 RISC-V 平台上构建 compiler-rt 的问题。

  > 他提供了一段 cmake 命令的示例，用于在 RISC-V 平台上构建 compiler-rt。该示例包含了一些必要的编译选项和参数，用于指定目标架构、ABI、编译器路径等。同时，他还提到在集成 LLVM 构建过程中，可以将 Compiler-RT 添加到 runtimes 列表而不是作为子项目，并可以使用 cmake 缓存文件来配置运行时库。

  > 请注意，这只是他在邮件中提供的部分回复，可能没有完整的上下文。如果你想了解更多细节，建议你参考完整的邮件列表讨论或向 llvm-dev 邮件列表提问以获取更准确和详细的信息。

  > 需要注意的是，这份邮件的时间戳是 2021 年 7 月 28 日，因此这段回复代表当时 Luís Marques 的观点和建议，目前的情况可能有所不同。建议在实际操作时参考最新的官方文档和资源，以确保获得准确和最新的信息。

- LinkedIn
https://www.linkedin.com/company/lowrisc
lowRISC CIC | LinkedIn
WebJun 9, 2023 · Luís Marques Richard Nicoll Robert Mullins Vladimir Rožić Hardware Engineer at LowRISC CIC See all employees Updates lowRISC CIC 2,435 followers 6d

  > 根据你提供的信息，在 LinkedIn 上可以找到关于 Luís Marques 的个人资料。他在 LowRISC CIC 担任硬件工程师的职位。LowRISC CIC 是一个与 RISC-V 相关的组织，专注于开源硬件的开发和创新。

  > 这个信息表明 Luís Marques 与 LowRISC CIC 有直接的工作关系，并在该组织中担任硬件工程师一职。由此可推测，他与该组织合作开发了与 RISC-V 相关的项目或参与了相关的研究工作。



![输入图片说明](https://foruda.gitee.com/images/1690416434547319602/0d642f7f_5631341.jpeg "2023-06-08-Hugo-MCNALLY-poster.jpg")

# [Relocatable RISC-V Rust Applications for Embedded Systems](https://riscv-europe.org/media/proceedings/posters/2023-06-08-Hugo-MCNALLY-poster.pdf)

- Hugo McNally <hugom@lowrisc.org>
- Luís Marques <luismarques@lowrisc.org>

### Problem

The RISC-V ecosystem currently lacks an Application Binary Interface (ABI) that adequately supports common requirements for relocatable applications in embedded systems.

 **Requirements:** 
- No Memory Management Unit (MMU)
- Execute In Place (XIP)
- RAM and ROM segments can be relocated independently

The existing RISC-V System V ABI does not allow satisfying all of these at the same time, as it assumes the use of a single relocation offset (base address) for the entire application.

### Examples

The following assembly examples are implementations of this C function, where x is a global variable.


```
int get_x() {
    return x;
}
```

### RISC-V Absolute Addressing

If the address of x is independent of application position, e.g. it's a memory-mapped device, one can continue to use absolute addressing.


```
lui a0, %hi(x)
lw a0, %lo(x)(a0)
ret
```

### RISC-V PC-Relative Addressing

If x is within 2 GiB of the PC, PC-relative addressing can be used. This is because the offet from the PC is independent ofthe application position.


```
1: auipc a0, %pcrel_hi(x)
   lw a0, %pcrel_lo(1b)(a0)
   ret
```

### RISC-V System V ABI

The System V ABI enables creating relocatable applications. For RISC-V, this is typically done by using PC-relative addressing when x is within 2 GiB of the PC and by using a Global Offet Table for when x is not local (preemptible).

The GOT is filled with the absolute address of each global (preemptible) symbol by the application loader. It's position is known relative to the PC.


```
1: auipc a0, %got_pcrel_hi(x)
   lw a0, %pcrel_lo(1b)(a0)
   lw a0, 0(a0)
   ret
```

![输入图片说明](https://foruda.gitee.com/images/1690416980443642905/8348b90a_5631341.png "屏幕截图")

Application Binary
- Text Segment
- GOT | Data Segment

Offsets maintained when relocated  
The SystemVABI assumes a single base address.

Memory
- Text Segment <- PC
- GOT | Data Segment

### Why can't the System V ABI be used?

Although using the GOT solves the problem of having independently relocatable text and data segments, the ABI relies on the GOT itself being at a known offset relative to the PC, which means the text and data segments cannot be independently relocated. FDPIC Style ABIs solve this.

### RISC-V GP-Relative Addressing

GP-relative addressing can be used when x is within 2 GiB ofthe globalpointer (GP).


```
lui a0, %gprel_hi(x)
add a0, gp, a0
lw a0, %gprel_lo(x)(a0)
ret
```

### Possible RISC-VFDPIC ABI 

FDPIC allows the text and data segments to be independently relocatable, by reserving a register, the global pointer (GP), to point to a known position in the data segment. This means the GOT can be found relative to the GP and not the PC.

With this ABI, PC-relative addressing or GP-relative addressing can be used when x is a local symbol. It must be within 2 GiB of the PC or GP.

When x is external (preemptible), the address can be computed using the GOT.


```
lui a0, %gprel_got_hi(x)
add a0, gp, a0
lw a0, %gprel_got_lo(x)(a0)
lw a0, 0(a0)
ret
```

![输入图片说明](https://foruda.gitee.com/images/1690417191342727180/03fd6295_5631341.png "屏幕截图")

- Possible FDPIC ABI 
- Proposed ePIC ABI

Independently Relocatable Segments

### The Proposed ePIC ABI

The proposed ePIC ABI uses a combination of PC- and GP-relative addressing, similar to FDPIC local addressing. Since we do not require shared library support, we can avoid paying for the cost of the GOT, used for external (preemptible) data addressing.

Sometimes only the linker knows whether a symbol will reside in the text segment or data segment. For these situations ePIC has instruction-rewriting relocations, allowing the linker to select between PC-Relative or GP-relative addressing depending on the symbol's segment.


```
1: lui a0, %epic_hi(x)
add a0, gp, a0, %epic_base_add(x)
lw a0, %epic_lo(1b)(a0)
ret
```


```
1:
auipc a0, %pcrel_hi(x)
nop // can be removed
lw a0, %pcrel_lo(1b)(a0)
ret
```


```
lui a0, %gprel_hi(x)
add a0, gp, a0
lw a0, %gprel_lo(x)(a0)
ret
```

### Why ePIC is being proposed over potential alternatives such as an FDPIC-style ABI

 **Memory Usage** : FDPIC uses a GOT which has an entry for each global symbol. This costs one word per entry (` RV32).

 **Start-up Cost** : FDPIC requires the GOT to be filled by the loader at load time, increasing the startup time.

 **Simplicity** : The full generality and cost of FDPIC is not needed, as support for shared libraries is not required.

 **Access Cost** : PC-relative and GP-relative accesses are cheaper than GOT based accesses. Compare the instruction sequences' lengths, noting the additional load instruction.

### The Security Tock OS

The Rust Language enforces more restrictions than are common in systems level languages, but these restrictions provide guarantees of correctness from the compiler. These correctness guarantees greatly reduce security risks.

Tock OS is an Operating System built to capitalise on these features. It only opts out of these restrictions (using the unsafe dialect) where it is necessary in the core of the kernel. Most functionality is managed in Capsules (Drivers) which have the restrictions enforced. Processes are dynamically loaded by the kernel. They are isolated using the MPU and scheduled preemptively to have stronger system liveness guarantees.

### Tock OS Architecture

![输入图片说明](https://foruda.gitee.com/images/1690417422790798663/e2603ddd_5631341.png "屏幕截图")

### The Motivation behind ePIC

Processes being dynamically loadable allows them to be built, signed and updated separately from the kernel.

Tock OS allows multiple applications to be loaded at the same time. Because there is no MMU, to make sure applications don't collide with one another, they have to be relocatable.

ePIC enables effcient relocatable applications for Tock OS that can be written in Rust to take full advantage of its correctness guarantees.

![输入图片说明](https://foruda.gitee.com/images/1690417515363748906/0e1272d3_5631341.png "屏幕截图")

[The ePIC Spec](https://github.com/luismarques/epic-spec/blob/main/epic.adoc)

# 用于嵌入式系统的可重定位RISC-V Rust应用

- 雨果·麦克纳利 hugom@lowrisc.org
- 路易斯·马克斯·luismarques@lowrisc.org

### 问题

RISC-V生态系统目前缺乏一个应用程序二进制接口（ABI），该接口充分支持嵌入式系统中可重定位应用程序的常见要求。

 **要求：** 

- 无内存管理单元 （MMU）
- 就地执行 （XIP）
- RAM 和 ROM 段可以独立重新定位

现有的RISC-V系统V ABI不允许同时满足所有这些要求，因为它假设对整个应用使用单个重定位偏移（基址）。

### 例子

以下程序集示例是此 C 函数的实现，其中 x 是全局变量。


```
int get_x() {
    return x;
}
```

### RISC-V 绝对寻址

如果 x 的地址与应用程序位置无关，例如它是一个内存映射设备，则可以继续使用绝对寻址。


```
lui a0, %hi(x)
lw a0, %lo(x)(a0)
ret
```

### RISC-V PC 相对寻址

如果 x 在 PC 的 2 GiB 范围内，则可以使用 PC 相对寻址。这是因为PC上的支架与应用位置无关。


```
1: auipc a0, %pcrel_hi(x)
   lw a0, %pcrel_lo(1b)(a0)
   ret
```

### RISC-V System V ABI

系统 V ABI 支持创建可重定位的应用程序。对于RISC-V，这通常是通过在x在PC的2 GiB以内时使用PC相对寻址来完成的，当x不是本地（抢占）时使用全局办公室表来完成的。

GOT由应用程序加载程序填充每个全局（抢占式）符号的绝对地址。它的位置相对于PC是已知的。


```
1: auipc a0, %got_pcrel_hi(x)
   lw a0, %pcrel_lo(1b)(a0)
   lw a0, 0(a0)
   ret
```

![输入图片说明](https://foruda.gitee.com/images/1690416980443642905/8348b90a_5631341.png "在这里输入图片标题")

应用程序二进制文件

- 文本段
- 戈特 |数据细分受众群

重新定位  
时保留的偏移 系统VABI假定单个基址。

记忆

- 文本段 < - PC
- 戈特 |数据细分受众群

### 为什么不能使用系统 V ABI？
尽管使用 GOT 解决了具有可独立重定位的文本和数据段的问题，但 ABI 依赖于 GOT 本身相对于 PC 处于已知偏移量，这意味着文本和数据段无法独立重新定位。FDPIC风格的ABI解决了这个问题。

### RISC-V GP-相对寻址
当 x 在全局指针 （GP） 的 2 GiB 以内时，可以使用 GP 相对寻址。


```
lui a0, %gprel_hi(x)
add a0, gp, a0
lw a0, %gprel_lo(x)(a0)
ret
```

### 可能的 RISC-VFDPIC ABI
FDPIC允许文本和数据段独立地重新定位，方法是保留寄存器，即全局指针（GP），以指向数据段中的已知位置。这意味着可以相对于GP而不是PC找到GOT。

使用此 ABI，当 x 是局部符号时，可以使用 PC 相对寻址或 GP 相对寻址。它必须在电脑或 GP 的 2 GiB 范围内。

当 x 是外部（可抢占）时，可以使用 GOT 计算地址。


```
lui a0, %gprel_got_hi(x)
add a0, gp, a0
lw a0, %gprel_got_lo(x)(a0)
lw a0, 0(a0)
ret
```

输入图片说明![输入图片说明](https://foruda.gitee.com/images/1690417191342727180/03fd6295_5631341.png "在这里输入图片标题")

- 可能的 FDPIC ABI
- 拟议的ePIC ABI

可独立重定位的段

### 拟议的ePIC ABI

所提出的ePIC ABI使用PC和GP相对寻址的组合，类似于FDPIC本地寻址。由于我们不需要共享库支持，因此我们可以避免支付用于外部（抢占式）数据寻址的 GOT 成本。

有时，只有链接器知道符号是驻留在文本段还是数据段中。对于这些情况，ePIC 具有指令重写重定位功能，允许链接器根据符号的段在 PC 相对寻址或 GP 相对寻址之间进行选择。


```
1: lui a0, %epic_hi(x)
add a0, gp, a0, %epic_base_add(x)
lw a0, %epic_lo(1b)(a0)
ret
```


```
1:
auipc a0, %pcrel_hi(x)
nop // can be removed
lw a0, %pcrel_lo(1b)(a0)
ret
```


```
lui a0, %gprel_hi(x)
add a0, gp, a0
lw a0, %gprel_lo(x)(a0)
ret
```

### 为什么提出ePIC而不是潜在的替代方案，例如FDPIC式ABI

 **内存使用情况** ：FDPIC使用GOT为每个全局符号提供一个条目。每个条目花费一个单词（' RV32）。

 **启动成本** ：FDPIC要求GOT在加载时由加载器填充，从而增加启动时间。

 **简单性** ：不需要FDPIC的全部通用性和成本，因为不需要对共享库的支持。

 **访问成本** ：相对于PC和GP的访问比基于GOT的访问便宜。比较指令序列的长度，注意附加的加载指令。

### 安全托克操作系统

Rust 语言强制执行比系统级语言中常见的限制更多的限制，但这些限制提供了编译器正确性的保证。这些正确性保证大大降低了安全风险。

Tock OS是一个操作系统，旨在利用这些功能。它仅在内核核心需要的情况下选择退出这些限制（使用不安全的方言）。大多数功能在强制实施限制的胶囊（驱动程序）中进行管理。进程由内核动态加载。它们使用 MPU 隔离并抢先调度，以获得更强的系统活动性保证。

### 托克操作系统架构

![输入图片说明](https://foruda.gitee.com/images/1690417422790798663/e2603ddd_5631341.png "在这里输入图片标题")

### ePIC背后的动机

可动态加载的进程允许它们与内核分开构建、签名和更新。

Tock OS允许同时加载多个应用程序。由于没有 MMU，为了确保应用程序不会相互冲突，它们必须是可重定位的。

ePIC 为 Tock OS 提供了有效的可重定位应用程序，这些应用程序可以用 Rust 编写，以充分利用其正确性保证。

![输入图片说明](https://foruda.gitee.com/images/1690417515363748906/0e1272d3_5631341.png "在这里输入图片标题")

[电子专利信息规范](https://github.com/luismarques/epic-spec/blob/main/epic.adoc)