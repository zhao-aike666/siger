- [用Verilator仿真Ibex的hello world](https://zhuanlan.zhihu.com/p/108624018)
- [简评几款开源RISC-V处理器](https://baijiahao.baidu.com/s?id=1664724825695807851) - 胡振波

<p><img width="706px" src="https://foruda.gitee.com/images/1668080830603246248/fb358aa4_5631341.jpeg" title="415lowRISC.jpg"></p>

> 歪果仁喜欢用独特的动物当做吉祥物，lowRISC ibex 也不例外，我特地找来一张与 FPGA 合影的吉祥物附在封面上，设计感的海报，大大的 LOGO 的原设计，被我添加上的 元素围剿啦。不管怎么说，lowRISC 都是 RISCV 的前传啦，有着根正苗红的形象，一顿考古过后，各个 RISCV 派系的衍生，渐渐清晰了，本期就是一份考古帖，没有技术功课，各位同学可以依据自己的喜好，研究下各家的长处。（附上考古笔记）

【[笔记](https://gitee.com/RV4Kids/yadan-board/issues/I5X5XL#note_14246015_link)】

https://github.com/lowRISC/ibex  
https://gitee.com/RV4Kids/ibex/issues/I6064X

- [用Verilator仿真Ibex的hello world](https://zhuanlan.zhihu.com/p/108624018) 发布于 2020-02-23 15:19

  - [lowRISC的Ibex项目Hello World的仿真](https://blog.csdn.net/qq_33766313/article/details/112780529) 2021-01-18 15:39:53
  - [lowRISC的Ibex项目Hello World的仿真](https://www.liangzl.com/get-article-detail-227975.html) 2021-01-20 12:19:46

- Open to the core https://lowrisc.org/

  > lowRISC is a not-for-profit company with a full stack engineering team based in Cambridge, UK. We use collaborative engineering to develop and maintain open source silicon designs and tools.
- Gitee 极速下载 / lowRISC https://gitee.com/mirrors/lowRISC

  > lowRISC 目的是开发一个完全开放的硬件平台，从处理器到开发版

- [RISC-V硬盘处理器？希捷发布！令机械硬盘性能暴涨3倍！](https://www.ednchina.com/news/6101.html)
  - [简评几款开源RISC-V处理器](https://baijiahao.baidu.com/s?id=1664724825695807851) - 胡振波
  - [芯来赋能 | 兆易创新GD32 RISC-V MCU荣获全球电子成就奖之“年度微控制器产品”大奖](https://www.nucleisys.com/newsdetail.php?id=164)

    > 2019年11月7日，深圳，在全球CEO峰会及全球分销与供应链峰会上，兆易创新GD32VF103系列RISC-V内核通用MCU产品荣获全球电子成就奖之“年度微控制器/接口产品”大奖。GD32VF103系列RISC-V MCU采用了全新的基于开源指令集架构RISC-V的Bumblebee处理器内核，该内核是由中国领先的RISC-V处理器内核IP和解决方案厂商芯来科技(Nuclei System Technology)针对兆易创新通用MCU产品定制。芯来科技提供了内核授权以及工具链和技术支持。

![输入图片说明](https://foruda.gitee.com/images/1668081312637281686/7f619e58_5631341.png "屏幕截图")

# [用Verilator仿真Ibex的hello world](https://zhuanlan.zhihu.com/p/108624018)

- [Yao Zhao ：Digital design engineer](https://www.zhihu.com/people/yao-zhao-10)

<p data-first-child="" data-pid="S0lQ4qD2">Ibex是个优秀的小巧的开源RISC-V core，基于Zero Riscy，现由LowRISC维护。其是一个RV32IMCZicsrZifencei的RISC-V core（其中M是可选的），支持M、U两个privilege mode，硬件描述语言使用VerilogHDL。官方的仓库地址是<code>https://github.com/lowRISC/ibex</code>。User manual在<code>https://ibex-core.readthedocs.io/en/latest/</code>。顺便说一下Google维护的OpenTitan项目使用的CPU便是Ibex。 Ibex支持用FuseSoc来执行类似Makefile的功能，可以执行诸如仿真、lint之类的任务，仿真可以使用Verilator、VCS等。 Verilator是个开源的仿真工具，说它是仿真工具其实不太准确，它还可以扮演一些lint等其它的功能。它是通过把RTL编译成C++的文件，然后配合C++的testbench再编译成可执行文件进行仿真。可以想象，Verilator仅能支持Verilog、SystemVerilog的子集，这个子集里绝大部分是需要可综合的，其他的是一些类似<code>$display</code>之类的系统调用。</p><h2>1. 把仿真跑起来</h2><h3>1.1 Verilator</h3><p data-pid="dygiQmax">若想用Verilator跑一个Ibex的hello world仿真，那Verilator是必不可少的。Ibex需要版本比较新的verilator，所以不建议使用编译好的安装包，我们自己把verilator仓库 clone下来编译、安装。 Verilator的仓库地址是<code>https://git.veripool.org/git/verilator</code>，在安装verilator之前可能需要安装一些依赖库，下面是官方的安装步骤：</p>

```
# Prerequisites:
#sudo apt-get install git make autoconf g++ flex bison
#sudo apt-get install libfl2  # Ubuntu only (ignore if gives error)
#sudo apt-get install libfl-dev  # Ubuntu only (ignore if gives error)

git clone https://git.veripool.org/git/verilator   # Only first time
## Note the URL above is not a page you can see with a browser, it's for git only

# Every time you need to build:
unsetenv VERILATOR_ROOT  # For csh; ignore error if on bash
unset VERILATOR_ROOT  # For bash
cd verilator
git pull        # Make sure git repository is up-to-date
git tag         # See what versions exist
#git checkout master      # Use development branch (e.g. recent bug fixes)
#git checkout stable      # Use most recent stable release
#git checkout v{version}  # Switch to specified release version

autoconf        # Create ./configure script
./configure
make
sudo make install
# Now see "man verilator" or online verilator.pdf's for the example tutorials
```

<h3>1.2 RISC-V tool chain</h3><p data-pid="qWWykgEe">在Ibex里有ibex_simple_system下的hello world C代码，我们需要把它编译成Ibex可以执行的二进制代码（这个代码最终load到Ibex的RAM里执行）。我们需要确保使用比较新的版本，所以最好是clone下来自己编译，下载、编译的时间比较久。RISC-V toolchain的仓库在<code>https://github.com/riscv/riscv-gnu-toolchain</code>。</p>

```
git clone https://github.com/riscv/riscv-gnu-toolchain
./configure --prefix=/opt/riscv --with-arch=rv32imc --with-abi=ilp32
make
```

<p data-pid="w7t51R_5">安装好之后要把<code>/opt/riscv/bin</code>加到<code>$PATH</code>，使用<code>which riscv32-unknown-elf-gcc</code>测试一下是否安装成功。</p><h3>1.3 FuseSoc</h3><p data-pid="ZJvf6XJj">其实FuseSoc不是必须的，FuseSoc起到的是类似Makefile的功能，我们多自己敲一些命令可以起到同样的效果，但Ibex已配置成FuseSoc可用的状态，可以帮我们省很多力气。所以我们这里使用FuseSoc。FuseSoc的仓库在<code>https://github.com/olofk/fusesoc</code>。如果Linux系统里可以安装现成的安装包可是可以的。下面是官方的安装步骤：</p>

```
git clone https://github.com/olofk/fusesoc
cd fusesoc
sudo pip install -e .
```

<p data-pid="YoYRSUWr">所以前提还要有pip工具。好吧，需要准备的工作还不少。 FuseSoc的默认安装路径是<code>~/.local/bin</code>，安装完要把这个路径加到<code>$PATH</code>里，加好后使用<code>which fusesoc</code>测试一下是否成功。</p><h3>1.4 hello world跑起来</h3><p data-pid="g2KKap8v">在Ibex的根目录下可以按照下面的步骤跑起来了：</p>

```
## Building Simulation
fusesoc --cores-root=. run --target=sim --setup --build lowrisc:ibex:ibex_simple_system --RV32M=1 --RV32E=0

## Building hello world
make -C examples/sw/simple_system/hello_test

## Running the Simulator
./build/lowrisc_ibex_ibex_simple_system_0/sim-verilator/Vibex_simple_system [-t] --raminit=<sw_vmem_file>
```

<p data-pid="dvnMwbhT"><code>&lt;sw_vmem_file&gt;</code>是RISC-V的可执行文件路径，对于编译好的hello world，使用<code>./examples/sw/simple_system/hello_test/hello_test.vmem</code>。 编译出来的可执行文件是<code>Vibex_simple_system</code>，<code>--raminit=&lt;sw_vmem_file&gt;</code>用于把编译好的hello world RISC-V可执行代码load到RAM里去。<code>-t</code>是用来生成GTKWave可看的waveform，可以不用加。 大功告成，最后Ibex的输出写在<code>ibex_simple_system.log</code>里，可以打开看看是否符合期望。由于Ibex带了仿真的trace功能，指令trace信息还会存在<code>trace_core_00000000.log</code>。</p><p data-pid="3CqsuxkP">不使用FuseSoc也是可以的，在Building Simulation这一步骤我们需要自己准备file list，用verilator编译，其他步骤都是一样的。</p><h2>2. 深入理解背后的原理</h2><p data-pid="z4SKnnp9">作为一名合格的工程师，我们肯定不会满足于按照给定的步骤跑一下，我们还需要知道这背后到底发生了什么。 首先我们知道Verilator是把一个仿真，包括DUT和testbench编译成Linux下的一个可执行文件，并且使用的是C++。那么这里必定有一个main函数，它藏身于<code>examples/simple_system</code>之下，我们来看看<code>main</code>函数里都有什么。</p>

```
int main(int argc, char **argv) {
  ibex_simple_system top;
  VerilatorSimCtrl &simctrl = VerilatorSimCtrl::GetInstance();
  simctrl.SetTop(&top, &top.IO_CLK, &top.IO_RST_N,
                 VerilatorSimCtrlFlags::ResetPolarityNegative);

  simctrl.RegisterMemoryArea("ram", "TOP.ibex_simple_system.u_ram");

  std::cout << "Simulation of Ibex" << std::endl
            << "==================" << std::endl
            << std::endl;

  if (simctrl.Exec(argc, argv)) {
    return 1;
  }
  // ......
}
```

<p data-pid="iC8g_u-Y">里面提到的<code>ibex_simple_system</code>类毫无疑问就是我们的DUT了，是我们design RTL编译成的top class。里面的<code>IO_CLK</code>和<code>IO_RST_N</code>就是design的clock和reset输入，编译成C++后变成了里面的CData类的对象，main函数中用指针将他们连到testbench的激励上去。 我们有必要了解一下这个simple system都有哪些外设，顶层是如何连接的。</p><h3>2.1 ibex_simple_system SoC</h3><p data-pid="JvJqNodG">作为最简单的SoC系统来讲，只需要把CPU core的指令总线和数据总线接到一个RAM即可。但是Ibex simple system想做的不仅仅如此，它还挂了一个<code>simulator_ctrl</code>外设，这个外设可以提供写数据到log file的功能，也可以往特定地址写1来停止仿真。顶层连接如下图所示。 </p>

![输入图片说明](https://foruda.gitee.com/images/1668081753277717249/96153a7f_5631341.png "屏幕截图")

<p data-pid="1xov0gYU"> 其中bus充当了decoder和arbiter的功能，Ibex系统使用的是自定义的总线，也很简单易懂。<code>ibex_core_tracing</code>把I bus和D bus上的活动trace下来供debug使用，当然没有它来跑hello world也是可以的。 由于verilator只能编译可综合的设计，所以对于RAM我们必须提供一个可综合的model，RAM的可综合model非常简单，用二维数组就可以实现。但是我们必须解决如何把一个初始的RAM镜像load进去，如果不考虑灵活性的话，直接指定一个镜像文件也是可行的。但如果要让testbench指定的话，就必须提供一个接口，可供C++调用，在verilator编译之后这个DPI-C的函数就变成了ibex_simple_system class下的一个函数。</p>

```
`ifdef VERILATOR
    // Task for loading 'mem' with SystemVerilog system task $readmemh()
    export "DPI-C" task simutil_verilator_memload;
    // Function for setting a specific 32 bit element in |mem|
    // Returns 1 (true) for success, 0 (false) for errors.
    export "DPI-C" function simutil_verilator_set_mem;

    task simutil_verilator_memload;
      input string file;
      $readmemh(file, mem);
    endtask

    function int simutil_verilator_set_mem(input int index,
                                           input logic[31:0] val);
      if (index >= Depth) begin
        return 0;
      end

      mem[index] = val;
      return 1;
    endfunction
  `endif
```

<p data-pid="d9RGm-BZ">对于<code>simulator_ctrl</code>，有两个寄存器，往0x0写一个byte会记录到log中去，相当于往终端上打印一个字符，往0x4写1会调用<code>$finish</code>终止仿真。</p><h3>2.2 VerilatorSimCtrl</h3><p data-pid="-Q8AM8pH">在<code>main</code>函数中被实例化的另一个类就是<code>VerilatorSimCtrl</code>了，可以想象，这个类的对象应该是static的。这个类提供了很多供verilator使用的testbench功能，比如把memory镜像load到memory去，比如连接DUT的clock及reset、激励clock及reset。它藏身于<code>dv/verilator/simutil_verilator/cpp/verilator_sim_ctrl.cc</code>，里面便声明要使用RAM model中欲导出的两个DPI-C外部函数。 main函数中首先调用<code>SetTop</code>函数把clock和reset接到<code>VerilatorSimCtrl</code>内部。如下面代码所示。</p>

```
void VerilatorSimCtrl::SetTop(VerilatedToplevel *top, CData *sig_clk,
                              CData *sig_rst,
                              VerilatorSimCtrlFlags flags) {
  top_ = top;
  sig_clk_ = sig_clk;
  sig_rst_ = sig_rst;
  flags_ = flags;
}
```

<p data-pid="TV0drpIv">之后使用<code>RegisterMemoryArea</code>来注册ibex_simple_system里面的memory，供对其初始化使用。里面的具体细节就不赘述了，其基本原理是把某个memory的对象的hierarchy，如<code>TOP.ibex_simple_system.u_ram</code>绑定到一个名称上，如<code>ram</code>，之后便可以调用RAM model里的函数把数据写入到对应的二维数组里去了。 上面是准备工作，真正对DUT进行激励是调用<code>VerilatorSimCtrl</code>里的<code>Exec</code>函数，并把<code>main</code>收到的参数原封不动地传给它。<code>Exec</code>中再调用<code>RunSimulation</code>，<code>RunSimulation</code>再调用<code>Run</code>，历经辗转我们终于来到施加激励的地方了。其核心部分如下：</p>

```
while (1) {
    if (time_ >= initial_reset_delay_cycles_ * 2) {
      SetReset();
    }
    if (time_ >= reset_duration_cycles_ * 2 + initial_reset_delay_cycles_ * 2) {
      UnsetReset();
    }

    *sig_clk_ = !*sig_clk_;

    if (*sig_clk_ && (callback_ != nullptr)) {
      callback_(time_);
    }

    top_->eval();
    time_++;

    Trace();

    if (request_stop_) {
      std::cout << "Received stop request, shutting down simulation."
                << std::endl;
      break;
    }
    if (Verilated::gotFinish()) {
      std::cout << "Received $finish() from Verilog, shutting down simulation."
                << std::endl;
      break;
    }
    if (term_after_cycles_ && time_ > term_after_cycles_) {
      std::cout << "Simulation timeout of " << term_after_cycles_
                << " cycles reached, shutting down simulation." << std::endl;
      break;
    }
  }
```

<p data-pid="Clhe3H6C">简单说起来就是对clock赋值1之后evaluate，再赋值0然后再evaulate，如此循环一直到仿真结束，如论这个结束是主动的还是被动的。</p><h3>2.3 Hello world</h3><p data-pid="WUk96YlR">Ibex上跑的hello world C code肯定不能像Linux上的hello world上面简单调用stdlib里的printf就可以的。ibex_simple_system上并没有terminal，也没有操作系统提供系统调用，我们除了RAM之外的外设就是simulator_ctrl了，往这个外设的0x0写一个byte，这个字符会被记录到特定的log file里。于是我们提供一个基本的<code>putchar</code>函数：</p>

```
#define DEV_WRITE(addr, val) (*((volatile uint32_t *)(addr)) = val)

int putchar(int c) {
  DEV_WRITE(SIM_CTRL_BASE + SIM_CTRL_OUT, (unsigned char)c);

  return c;
}
```

<p data-pid="PI8ZGBO-">进而可以写出输出一个字符串的函数<code>puts</code>，也可以写出输出给定的32位16进制数值的函数<code>puthex</code>。这些辅助函数放在<code>examples/sw/simple_system/common/simple_system_common.c</code>中。于是此hello world就调用这些辅助函数输出一些字符和32位数值：</p>

```
#include "simple_system_common.h"

int main(int argc, char **argv) {

  puts("Hello simple system\n");
  puthex(0xDEADBEEF);
  putchar('\n');
  puthex(0xBAADF00D);
  putchar('\n');

  return 0;
}
```

<p data-pid="YaCN9hLE">在<code>examples/sw/simple_system/common/crt0.S</code>中维护着vector table，其中包括上电后的程序入口<code>main_entry</code>，也便是默认的0x80地址。</p>

```
#......
main_entry:
  /* jump to main program entry point (argc = argv = 0) */
  addi x10, x0, 0
  addi x11, x0, 0
  jal x1, main

  /* Halt simulation */
  li x5, SIM_CTRL_BASE + SIM_CTRL_CTRL
  li x6, 1
  sw x6, 0(x5)
#......
```

<p data-pid="seHnpTcg">我们可以看到里面有调到<code>main</code>函数的动作，<code>jal</code>中的<code>main</code>便是hello_test中<code>main</code>函数的地址，link之后会把它的地址填进去。从<code>main</code>中返回之后便往simulator_ctrl的0x4地址写1去停止仿真。</p><h3>2.4 结语</h3><p data-pid="kyY-pZJO">Ibex的verilator仿真框架便是如此，SoC可以更复杂些，挂上各种其它的外设，但因为要用verilator，需要单时钟域，可综合的外设model。我们也可以用这个框架来跑RISC-V的compliance tests，不过需要再加一个bus master可以主动去取memory特定地址上的数据。总而言之，万变不离其宗，原理掌握了，功能是否能实现、该怎样去实现便不是一件难事了。</p>

- 今年的毕设就是学习优化改进ibex 谢谢大佬的文章[握手] 2021-10-12

  - 我去 老哥加个联系方式呗[赞同] 2021-12-05
  - 老哥我也是 2021-12-03

- toolchain那里make 一直卡在一开始gcc的clone怎么办呀 09-18

  - 用的国外的源吧？换个国内的，具体方法请自行搜索。 09-18

- 请问verilator和modelsim有啥区别嘛，他们是否都可以用 verilog对RTOS的例进行仿真呢 04-09

  - 区别很大，verilator限制条件很多。仿真操作系统级别用modelsim肯定不行了，verilator还有希望。 04-09

- 请问verilator是只在时钟信号的上升沿更新信号吗？ 2021-12-12

  - 是的，或者理解为verilator没有时延信息 2021-12-13
  - 噢噢，那调试异步中断的时候比较麻烦了 2021-12-13

- 很棒。感谢~~ 2020-12-26

# [简评几款开源RISC-V处理器播报文章](https://baijiahao.baidu.com/s?id=1664724825695807851)

RISCV技术爱好者 2020-04-23 09:28

> RISC-V 能够实现“自主可控”与“普世通用”的国产处理器，中国应该拥抱此技术。胡振波

![输入图片说明](https://foruda.gitee.com/images/1668082201238944114/5bd18db0_5631341.png "屏幕截图")

RISC-V指令集是怎么回事就不多说了。开门见山，说说几款我接触过的开源处理器，按我接触的时间顺序来排序。

### 1. Rocket，BOOM

很多RISC-V开发者，无论硬件还是软件，首次接触的CPU core就是Rocket。Rocket Chip Generator可以生成包括Rocket core的一整套SoC，各种参数统统可配置。Rocket Chip是用Chisel开发的，初学者（CPU设计开发的数字前端初学者，尤其是只懂Verilog的初学者）要去看Rocket的代码还是会有些吃力的，对初学者不太友好。不过正因为其面世较早，又有Berkeley的纯正血统，粉丝众多，很多paper都是基于Rocket Chip做的，资料也很好找，但似乎没有详细的官方文档。

![输入图片说明](https://foruda.gitee.com/images/1668082221261053559/26a91252_5631341.png "屏幕截图")

Rocket chip带MMU，支持操作系统，所以在上面跑Linux是没有问题的。Rocket chip使用Tilelink总线，支持缓存一致性的一款总线。支持Verilator+OpenOCD+GDB仿真。

![输入图片说明](https://foruda.gitee.com/images/1668082229987770180/8bfd38a6_5631341.png "屏幕截图")

Rocket是64位CPU core（也是今天介绍的几款处理器中唯一一款64位的），采用经典五级流水，顺序执行，单发射，还支持各种分支预测。BOOM（Berkeley Out-of-Order Machine）基于Rocket，乱序执行，BOOM有还算比较详细的文档。这两个用来学习还是很不错的。而且Rocket还是比较成熟的，基于Rocket core已经有很多ASIC产品了。只是Chisel是道坎，CPU设计还没开始学就跳进Chisel的坑里去了！

Rocket Chip github： https://github.com/chipsalliance/rocket-chip

BOOM github:  https://github.com/riscv-boom/riscv-boom

一句话总结：Berkeley纯正血统，但欲学本core，必先入Chisel之坑。

### 2. Hammingbird E203

蜂鸟处理器是在国内RISC-V社区大名鼎鼎的芯来科技开发的RISC-V MCU系列。E203是其开源的一款单privilege mode，两级流水（不严格说法）的MCU，主打小面积、低功耗。使用Verilog开发。麻雀虽小，五脏俱全，也包括debug module，代码严谨优美，用来学习设计没得说。官方文档不算多，但是市面上可以买到胡振波大牛写的两本书，也算是学习资料丰富了。

![输入图片说明](https://foruda.gitee.com/images/1668082256793329765/be1f73c7_5631341.png "屏幕截图")

开源的E203在github上其实是一个SoC平台。E203使用自定义的类AXI接口，支持debug spec 0.11。唯一的缺憾是没有官方的verilator环境，如果使用verilator的话得自己搭个环境了。像我这种在家用不了商业仿真软件（也不愿意装D版）的硅农，只能用意念仿真了，目前还是看看代码，等有时间的话Port一个verilator仿真环境过去。开源蜂鸟E203支持Icarus Verilog开源仿真工具，这个仿真工具不知道还有没有人维护，就我以前使用而言，几乎只能仿纯verilog，对于很多不可综合的写法也不支持，使用非常受限，就更别提把GDB接入到仿真了。

![输入图片说明](https://foruda.gitee.com/images/1668082263879321602/241390c7_5631341.png "屏幕截图")

这里值得是说一下胡振波大牛写的《手把手教你设计CPU——RISC-V处理器篇》，还是很不错的设计入门之作，里面有很多干货，是本让初学者受益良多、让熟手硅农深有共鸣的好书。另一本貌似是嵌入式开发，目前还没有拜读。

![输入图片说明](https://foruda.gitee.com/images/1668082273378155522/63ccbaa6_5631341.png "屏幕截图")

github:  https://github.com/SI-RISCV/e200_opensource

一句话点评：初学者学习RISC-V处理器设计首选。

### 3. Ibex

提到开源RISC-V就不能不提Riscy系列了，尤其是zero-riscy，使用很广泛。Ibex是脱胎于zero-riscy的core，支持RV32IMC及一些Z系列指令，由LowRISC维护。Ibex小巧精悍，文档详实，学习资料丰富，支持verilator， 可以使用verilator+openOCD+GDB 仿真时debug。对于我这样的重度Verilator依赖者来说非常友好。

![输入图片说明](https://foruda.gitee.com/images/1668082293794898313/0c7e4a5d_5631341.png "屏幕截图")

Ibex支持machine mode和user mode两种privilege mode，可以实现比单machine mode更加丰富的功能。Ibex采用system verilog开发，对于传统的IC工程师是个好消息。Ibex现在也支持了指令cache了，提高了performance，但装了cache会让core变得臃肿很多，对于学习cache controller的设计是个好事情。

Ibex使用类TLUL的自定义接口，官方的SoC是PULP。Google的OpenTitan项目也是基于Ibex。相关的设计学习资料算是相当多了。

github:  https://github.com/lowRISC/ibex

文档： https://ibex-core.readthedocs.io/en/latest/introduction.html

一句话点评：啥都别说了，这么好的东西还不赶紧盘它？

### 4. VexRiscv

VexRiscv可配置，可支持RV32IMCA，可配置为经典5级流水。VexRiscv使用SpinalHDL开发，类似Chisel，也是一款基于Scala的硬件建构语言。可配置MMU，所以理论上可以支持操作系统。

```
classVexRiscv(val config : VexRiscvConfig) extends Component with Pipeline{ type T = VexRiscvimport config._
//Define stages def newStage(): Stage = { val s = new Stage; stages += s; s }val decode = newStage()val execute = newStage()val memory = ifGen(config.withMemoryStage) (newStage())val writeBack = ifGen(config.withWriteBackStage) (newStage())//......
```

VexRiscv有个官方的SoC：Briey，使用AXI接口。和Rocket Chip一样，支持Verilator+OpenOCD+GDB仿真。和Rocketchip一样都是使用scala解释器sbt工具，超占内存，快把我的老爷机拖垮了。

![输入图片说明](https://foruda.gitee.com/images/1668082364532079882/6295864f_5631341.png "屏幕截图")

也许是大牛都不爱写文档，VexRiscv文档很少很粗略。

github： https://github.com/SpinalHDL/VexRiscv

一句话点评：佳作，SpinalHDL代表作，需入SpinalHDL的坑。

### 5. SweRV EH1

SweRV EH1是WD开发的其中一款RISC-V core，支持RV32IMC，双发射，单线程，9级流水，性能应该说是相当不错，28nm可以跑到1GHz。而且还有份详细的文档，不愧是大厂出品。

![输入图片说明](https://foruda.gitee.com/images/1668082372545364923/e76c059e_5631341.png "屏幕截图")

SweRV是使用Verilog/System Verilog开发，使用AXI接口，对熟悉AMBA且不想去学Chisel及Scala的同学来说是相当友好了。而且支持verilator，必须点赞。值得一提的是SweRV带指令cache，且实现了丰富的cache maintenance自定义指令，非常值得学习。

![输入图片说明](https://foruda.gitee.com/images/1668082378473104130/5c8afe0c_5631341.png "屏幕截图")

github:  https://github.com/chipsalliance/Cores-SweRV

一句话点评：大厂出品，进阶学习佳作。

### 6. 无剑

听说平头哥的RISC-V处理器开源我还是很兴奋的，想仔细研究一番，但入手后有些失望。代码是刻意处理过的，注释都被去掉了，代码段的排版也非常不利于阅读，甚至有些代码是接近网表的加扰后的代码，顿时失去了兴趣。平头哥目的应该是推广这套平台，里面包括软硬件系统及SDK，从开源的程度来看，重点应该是软件。当然我是从研究学习硬件部分出发来说的，如果是使用无剑平台，包括仿真、FPGA、软件开发，应该是没有问题的。

github:  https://github.com/T-head-Semi/wujian100_open

一句话点评：学习资料缺乏，代码可读性较差，不是一个理想的学习对象。

---

[lowRISC at Week of Open Source Hardware · lowRISC: Collaborative open ...](https://www.lowrisc.org/blog/2019/06/lowrisc-at-week-of-open-source-hardware/)
lowrisc.org|1280 × 854 jpeg|Image may be subject to copyright.

![输入图片说明](https://foruda.gitee.com/images/1668078948355745179/8c466e6c_5631341.jpeg "lowRISC_enamel_badge_640w_hu45977cb0bfc770fd7ffa0509577547f2_53634_1280x0_resize_q75_box.jpg")

Six more weeks of Ibex development - what's new? · lowRISC ...
lowrisc.org|400 × 533 jpeg|Image may be subject to copyright.

![输入图片说明](https://foruda.gitee.com/images/1668078973145675255/080b0129_5631341.png "屏幕截图")

[Ibex on FPGA - Get stuff executed · lowRISC: Collaborative open silicon ...](https://lowrisc.org/blog/2019/10/ibex-on-fpga-get-stuff-executed/)
lowrisc.org|800 × 535 jpeg|Image may be subject to copyright.

![输入图片说明](https://foruda.gitee.com/images/1668079109751236179/c9665662_5631341.png "屏幕截图")

[Ibex on FPGA - Get stuff executed · lowRISC: Collaborative open silicon ...](https://lowrisc.org/blog/2019/10/ibex-on-fpga-get-stuff-executed/)
lowrisc.org|800 × 684 png|Image may be subject to copyright.

![输入图片说明](https://foruda.gitee.com/images/1668079179543962118/ba63fbb7_5631341.png "屏幕截图")

[Antmicro · Enabling open source Ibex synthesis and simulation in ...](https://antmicro.com/blog/2020/12/ibex-support-in-verilator-yosys-via-uhdm-surelog/)
antmicro.com|1200 × 628 |Image may be subject to copyright.

![输入图片说明](https://foruda.gitee.com/images/1668079218794135576/0d394261_5631341.png "屏幕截图")

[lowRISC • Sublime Digital • Graphic & Web Design](https://sublime-digital.com/portfolio/lowrisc/)
sublime-digital.com|1200 × 1800 jpeg|Image may be subject to copyright.

![输入图片说明](https://foruda.gitee.com/images/1668079629489480882/8752fa0e_5631341.png "屏幕截图")