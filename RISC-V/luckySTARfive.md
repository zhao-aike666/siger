- [Canonical携手赛昉科技 Ubuntu成功运行VisionFive RISC-V开发板](https://mp.weixin.qq.com/s/fJqIE2R3rIQiRWQV9K5_mA)

<p><img width="706px" src="https://foruda.gitee.com/images/1660808612456264987/241starfive副本.jpeg" title="241STARFIVE副本.jpg"></p>

当看到 Ubuntu IOT 适配成功的消息，这场芯势力的新品发布会足以令人翘首以盼了。与其他开发者不同的是，我手中的石榴派，此时此刻和 昉·星光2 开发板有了交集。第一时间联系到赛昉的老师，补上2021的课。

> **StarFive** : Star 是星星 Five 是 RISC-V 的 V 的读音，我们希望未来 RISC-V 可以像繁星一样闪耀。另外也寓意着 starfive 是 RISC-V 产业界冉冉升起的明星。

本次 SIGer 出品的 RVSC 2022 青少年科普专题的 主题是 对话·未来·教育，饱含着对未来的愿景，期待 RISC-V 的能成为一份来自未来的礼物，2021 RVSC 的集锦收录在 B站 RISCV国际基金会 中，“年轻人的第一个ISA” 就是这个意思。

石榴派是 2021 RVSC 我带去的礼物之一，另一个礼物 RV4kids 已经成长为 RV少年 科普自媒体 专注报道青少年视角的RISC-V产业的动态和学习资源。两者有机结合，石榴派是 RISCV 的青少年习作，也是实践成果的展现，RV少年 —— SIGer 学习小组 是学习笔记的呈现和分享，相辅相成。

之所以用补课，是2021 RVSC 同为参展“单位”，间隙也造访赛昉的展台，是当时对 RISCV 固有的印象，跑不了石榴派的 DESKTOP 应用，整个 RISCV 产业还没有太多高性能开发板能够满足所有“需求”，整体在爬升阶段。今天看到了 这则激动人心的消息，我振奋啦。石榴派除了南向发展，推出了 FPGA 开发板，跑 RISCV 软核，用 ISA 写CPU，同时北向应用对接科普第一线，一直都是借鉴 树莓派社区的经验，作为 LINUX 发行版，也是石榴派一直在做的事情，汇集了一众时刻开源科普的应用，对接主流的发行版都做过适配。包括 DEBIAN, RASPBAIN, UBUNTU。其中 UBUNTU IOT 在 RASPBIAN 4B+ 的适配成功，大大简化了 石榴派应用构建的效率。也是 2022年 一年我的标准工作环境，此时此刻编写的 SIGER 文章，就是 UBUNTU IOT。桌面，引用了 Canonical 去年适配树莓派时使用的 “用心做好DESKTOP” 的标语，尽管它是突出 UBUNTU IOT 的 LINUX 内核。但这不能掩盖，开发板本身的优秀，才能支撑起 UBUNTU IOT的顺利运行。

- 我想从现在起，我可以用上国人自研的 RISC-V 开发板，验证应用，搭建学习环境，撰写 RISCV 科普专栏啦。

我作为科普志愿者，除了专长的人工智能科普，在社区教授小朋友用AI技术复原的非遗民族棋小游戏，去年参加 RVSC 2021的视频，在同学们中也早就传遍啦。只是手里的 RISCV FPGA 开发板，只能小小地运行几个示范应用，现在非遗民族棋项目已经和国际社区合作，将民族棋中国史项目，升级为国际民族棋史，世界各地的游戏样本已经 1314 个，使用通用的人工智能引擎予以示教，UBUNTU IOT 是最顺畅的，不只通用的简单UI的版本，复杂精美的UI界面的示教程序同样可以畅快运行。如果再给小朋友们演示世界民族棋小游戏的时候，一块RISCV开发板在手，就全都有啦。非常值得期待。

- 熟悉我的朋友都见过，我头像的照片，一个小姑娘，举着她画的彩色的五角星，它也是一个非遗小游戏，叫 LuckyStar 是首个被国际古棋盘游戏史项目收录的中国游戏，我带同学们来开源社区，接受社区的 “拥抱” 也有它的功劳。我想带上一个完整地可以自由运行 LUCKYSTAR 的 RISCV 开发板，给她展示石榴派最新的科普成果，1314 个世界民族棋。她一定会开心的。她是我的学生，社区小志愿者，绘制这幅精美的幸运星，已经征服了全世界，其中一幅就陈列在 树莓派基金会的图书馆里。

我们都相信愿的力量，RVSC 2022再次地欣欣向荣，就说明，整个 RISCV 中国社区的活力，如果再更加聚集在一起，合力攻坚，我们共同的未来，一定是美景无限的。这份抒情，不只给 STARFIVE 点赞，而是给 整个 RVSC 2022的全体嘉宾的。本期专访，相对偷懒，没有深度访问 赛昉，相信，在接下来石榴派的适配工作中一定会有更多交流，再行分享。附上 2021 RVSC 的回顾，作为弥补，充实本期专题，作为 第一个正式的 RVSC 2022 对话未来教育的专题，奉献给各位老师。

# 2022 RVSC 收视指南 STARFIVE 专题

| | 2022 Speech | speaker | 2021 topic |
|---|---|---|---|
| 8/24 A3 14:50 | 全球首款量产的高性能RISC-V多媒体处理器 | 赵晶 | [RISC-V IP在芯片中的应用](https://www.bilibili.com/video/BV1vw411d78G)
| 8/24 A3 15:10 | 全球首款高性能RISC-V开源SBC—VisionFive 2全球首发 | 许理 | 
| 8/24 B3 14:30 | CUDA on RISC-V GPGPU | 夏品正 | 
| 8/25 B5 11:50 | Open-source CPU profiling tools for open-source ISA cores RISC-V | Ley Foon Tan and Dizhao Wei | 
| 8/26 A9 10:20 | Automatic translation of ARM Neon Intrinsics to RISC-V vector instructions | Arka Maity | 
| 8/26 A9 10:50 | High performance GEMM on RISC-V | 丘庆和吴志刚 | 


| 2021 topic | speaker |
|---|---|
| [RISC-V崛起的必然](https://www.bilibili.com/video/BV12M4y1M73U) | 徐滔 Thomas Xu | 
| [SiFive Intelligence: Applying RISC-V vector processing to AI and machine learning](https://www.bilibili.com/video/BV1r44y127wb) | 胡进 | 
| [天枢：高性能乱序RISC-V处理器架构设计](https://www.bilibili.com/video/BV1444y127SJ) | 余红斌 | 
| [基于RISC-V架构的新一代智能存储：开放，高效的存算一体化芯片](https://www.bilibili.com/video/BV1mP4y147iV) | 吴子宁博士 英韧科技董事长<br>周杰 赛昉科技资深销售总监 | 
| [基于RISC-V的高性能数据处理组件——DPDK](https://www.bilibili.com/video/BV1NX4y1c7zU) | 吴冶 | 

| [RISCV 国际基金会](https://space.bilibili.com/1121469705) 的动态 （B站） | 赛昉科技发布日期 |
|---|---|
| [第一届RISC-V中国峰会·赛昉科技·专场论坛](https://www.bilibili.com/video/BV1eB4y1N7xT) | 2021-07-13 |
| [RISC-V产业发展与未来趋势；昉·星光VisionFive RISC-V单板计算机操作实例详解](https://www.bilibili.com/video/BV1VY411F7te) | 2022-06-08 |

# [Canonical携手赛昉科技 Ubuntu成功运行VisionFive RISC-V开发板](https://mp.weixin.qq.com/s/fJqIE2R3rIQiRWQV9K5_mA)

赛昉科技 StarFive 2022-08-18 10:02 发表于北京

![输入图片说明](https://foruda.gitee.com/images/1660799532022315405/屏幕截图.png "屏幕截图.png")

-  **微信号** ：StarFive StarFive2020
-  **功能介绍** ：赛昉科技官方账号。旨在介绍基于RISC-V指令集的商业化CPU核心IP、平台、生态，分享最新资讯，并致力于RISC-V在国内的推广及发展。

8月18日，作为Ubuntu官方版本的发行方及商业运营公司，Canonical宣布已成功在昉·星光单板计算机（VisionFive）上运行Ubuntu操作系统。未来，Canonical将与赛昉科技继续深入合作，为业界提供开源软硬件解决方案。

>  _以下为Canonical官方新闻译文_ 

2021年，我们首次推出可在RISC-V处理器和硬件上运行的Ubuntu。但我们没有止步于此。现在我们很自豪地宣布推出新一版适配赛昉科技昉·星光RISC-V开发板的Ubuntu。

### RISC-V，开源硬件的新范式

在过去十年中，开源和开放标准重塑了我们的世界，并带来了深远的影响。RISC-V联盟扩展了开源的范围，开发了标准的开源处理器架构。向开放社区发布RISC-V标志着硬件社区首次在此级别接受开源标准和协作。

通过开放标准协作及全行业内的快速应用，免费且开放的指令集架构ISA将开启处理器创新的新时代。RISC-V ISA在架构上让免费且可扩展的软件和硬件自由度达到了新高度。该架构可广泛应用于从低端微控制器到高端服务器级处理器。

### 赛昉科技，RISC-V技术和生态系统的领导者和推动者

赛昉科技（StarFive）于2018年成立，是一家具有自主知识产权的本土高科技企业，提供全球领先的基于RISC-V的CPU IP、SoC、开发板等系列产品，是中国RISC-V软硬件生态的领导者。

VisionFive作为当前市场上性价比最高的基于Linux系统的RISC-V单板计算机，搭载了JH7100视觉处理芯片。该芯片采用双核64位高性能RISC-V CPU、带2 MB的二级缓存，工作频率为1.0 GHz，为高性能计算场景提供了强劲的性能保证。

相较于其他RISC-V开发板，VisionFive定位更加明确：性能优先、价格适中、易于上手。它配置了8 GB的LPDDR4内存，40-pin GPIO header及端到端的软硬件基础架构，方便开发者灵活配置及部署外设，搭建特定的专属应用运行环境。

![输入图片说明](https://foruda.gitee.com/images/1660799703450789049/屏幕截图.png "屏幕截图.png")

### 由Canonical支持的开源计划

虽然RISC-V支持稳定的参考架构和硬件，但在新的开发板上运行稳定的软件仍然具有挑战性。稳定且可靠的底层操作系统（OS）是必不可少的软件基石。Linux是跨内核、驱动程序和发行版的操作系统，广受开发者欢迎，因此在Linux上进行开发的需求对于RISC-V来说更具吸引力

Canonical相信开源是加速创新的最佳方式。它激励我们在Ubuntu保护伞下广泛地赋能开源社区。然而，开源软件也有其自身的挑战。Canonical旨在将Ubuntu定位为不仅是创新者和开发者的参考操作系统，而且是可以让他们不必担心底层框架的稳定性，专注于核心应用程序，从而使他们能够更快地将产品推向市场的工具。

RISC-V潜力无限，并逐渐从多个市场中脱颖为极具竞争力的ISA。考虑于此，我们自然而然地便将 Ubuntu移植到RISC-V，为使用RISC-V的弄潮儿提供参考操作系统。

### 赛昉科技和Canonical将Ubuntu适配到VisionFive开发板

“VisionFive旨在满足业界对RISC-V开发板的热切需求和高期望，”赛昉科技创始人兼CEO徐滔表示。“感谢Canonical，我们很高兴Ubuntu能在VisionFive上成功运行。操作系统是RISC-V高性能应用程序的关键。赛昉科技致力于推动RISC-V 生态系统的发展，将与Canonical一起为开源世界做出进一步的贡献。”

在VisionFive开发板上运行的Ubuntu是Canonical和赛昉科技工程团队合作的结果。作为两家公司长期合作的一部分，Canonical的团队已经将Ubuntu 移植到由赛昉科技工程团队支持的VisionFive开发板。

由Canonical支持的Ubuntu提供了商业级的Linux发行版，创新者和开发人员也可以免费使用该发行版。“Ubuntu是最受欢迎的开源操作系统，我们很高兴能够扩大对RISC-V开源社区的支持。”Canonical硅联盟副总裁Cindy Goldberg表示。

“Canonical和赛昉科技正在合作为VisionFive开发板创建企业级Ubuntu镜像，将开源软件和开源硬件结合在一起，供开发人员在边缘构建广泛的计算机视觉应用程序。我迫不及待地想看看开发者使用VisionFive和Ubuntu能构建出什么。”

### 适配VisionFive开发板的Ubuntu已向社区开放

赛昉科技的VisionFive现在可以使用Ubuntu镜像，我们将开发更多新功能以及移植最新的Ubuntu。

赛昉科技VisionFive开发板由Ubuntu 22.04.1支持。适配VisionFive的镜像文件为： https://cdimage.ubuntu.com/releases/22.04.1/release/ubuntu-22.04.1-preinstalled-server-riscv64+visionfive.img.xz 。

在此处下载赛昉科技VisionFive开发板的最新Ubuntu镜像 https://ubuntu.com/download/risc-v 。

赛昉科技将在RISC-V中国峰会期间展示在VisionFive开发板上运行Ubuntu。想要一睹为快，请在活动期间参加赛昉科技的演示，并访问此处 https://forum.rvspace.org/t/about-visionfive-2-english-forum-categories/626 了解更多信息。

若需获取镜像的安装指南以及参与讨论，请参照此文档 https://discourse.ubuntu.com/t/ubuntu-on-the-visionfive-and-the-nezha-boards/29858 并到文档下的讨论区发言，以便每个人都可以从您的经验中受益。

> -End-

### 关于Canonical

Canonical是Ubuntu的发行商，Ubuntu是用于容器、云和超大规模计算的领先操作系统。Ubuntu是应用于大多数公共云工作负载以及新兴类别的智能网关、自动驾驶汽车和高级机器人的操作系统。Canonical为Ubuntu的商业用户提供企业安全、支持和服务。

### 关于赛昉科技

赛昉科技（StarFive）于2018 年成立，是一家具有自主知识产权的本土高科技企业，提供全球领先的基于RISC-V的CPU IP、SoC、开发板等系列产品，是中国RISC-V软硬件生态的领导者。

---

### 全球首款集成3D GPU的量产高性能RISC-V单板计算机即将发售，报名预约发布会：

![输入图片说明](https://foruda.gitee.com/images/1660799879525975841/屏幕截图.png "屏幕截图.png")

### 延伸阅读

- [见证RISC-V芯突破  赛昉科技新产品发布会报名开启](http://mp.weixin.qq.com/s?__biz=MzU2MDY3NzE1Nw==&mid=2247490784&idx=1&sn=4b8694ff094df8881aa5ee2f5b4557e9)
- [Embedded World 2022 | 昉·星光单板计算机携手Ubuntu闪耀国际嵌入式展](http://mp.weixin.qq.com/s?__biz=MzU2MDY3NzE1Nw==&mid=2247490306&idx=1&sn=edf0ca59b177d9d235127f4158bb58b1)

[阅读原文](https://mp.weixin.qq.com/s/fJqIE2R3rIQiRWQV9K5_mA)
