- [基于 RISC-V 设计一门开发操作系统的入门课程（中科院软件所）汪辰](https://www.bilibili.com/video/BV13f4y1j7bz)
- [26集] [循序渐进，学习开发一个RISC-V上的操作系统 - 汪辰 - 2021春](https://www.bilibili.com/video/BV1Q5411w7z5) 共15小时

<p><img width="706px" src="https://foruda.gitee.com/images/1660117036115934026/223rvos副本.jpeg"></p>

> 今天又回顾了一遍汪辰老师的 RVOS 的 POSTER 演讲，略显紧张的他可能是内容太多无法压缩在1分钟之内，相比海报上他精挑细选的那段寄语，满满地使命感。我是见证人，就如他是石榴派的见证人一样。RVOS 课程推出1个月后，我才报名，是PLCT实验室结缘的起点。至今我的课程留言是所有新同学的打卡地，会时常收到，都会让我回想起我们的友谊。一年前的演讲，落款到 1万4 的播放，到一年后的 7万5的播放，上千弹幕。应该没有任何一篇 RISCV 视频能出其右啦。RVSC 2021 最热门的香山，霸榜冠军位置，一年后的今天也只有4.3万。看着26期，将近15个小时的视频长度，就能知道 汪辰老师的耕耘。我这个课代表，今天又摘录了其他同学的经典语录，俨然就是课堂认真听讲的同学们的情景再现。课堂上的他，才真正的如鱼得水。不是那个紧张的演讲者，也不是峰会上忙碌的会务志愿者，他是我们的汪辰老师。

这个是本科还是研究生课程？
- 本科，非计算机亦可
- 本科。但不需要到本科就可以学会

> 这两个回答，精辟地描述了我搬小马扎，主动当课代表的场景，我带的学生都有中学生呢。而这回答的两个人一个是汪辰老师，另一个就是校长PLCT的吴伟老师了，PLCT的同学都叫他吴老板。后面，我会给同学们剧透他。所以说，这与其说是 RVOS 的课程推荐，不如说是 RVOS 的课堂风貌和校园氛围的描写，更像是讲故事。而只有打入内部，才能真正地分享到 PLCT 的学习环境和氛围，我讲用两个专题，来为同学们介绍 “神秘的” PLCT。先上 RVOS 

  - 课代表的 “小报告” （B站课程下，同学们的精彩回帖）
  - POSTER Speech
  - All of POSTER list
  - MagSi：RVOS 成功适配 fpga 软核模拟器 （课程生态）

### 课代表的 “小报告”

老师讲的真好。我现在在UCB上61C Great Ideas in Computer Architecture，RV32的部分教授讲得很模糊，读了教材看了几遍lecture都没能弄明白函数调用过程的完整步骤以及怎么理解第五章part7里“函数调用过程中有关寄存器的编程约定”那张表的内容。老师讲得很细、概念解释也清楚明了，还有详细的实例讲解，实在是太良心了。谢谢老师的分享❤️ repo已star，已经推荐给同学了～

wwww!这门课讲的真好啊！我们学校的课大多都太垃圾了！  
可能是里面很多东西都提前学过了！但是内容的广度、深度、以及教学方法都太好了！  
我上了个假大学，前半个小学期，课都不咋听，讲的太烂了，考试都是复习一晚上，直接把PPT背下来，直接考，还大多考原题！  
真的学校里教学太垃圾了！

老师讲的是真好。目前看到P4，老师讲的这些知识已经和我原来的学的相链接了。原来撸过51汇编、arm汇编，学过rtos。现在想自己也搞一个rtos，希望我能坚持学完[喜极而泣]

最近又复习了一遍汪老师在B站讲的操作系统的课程，讲的真的太好了，学到了很多新知识，也解决了我之前很多的疑惑。感谢汪老师提供如此优质的课程。【Happy】

尾调用的解释感觉有问题，尾调用应该是指函数最后一步是直接return另一个函数的返回结果。

- 我回去听一下再回复您哈，自己都不记得说了些啥了[笑哭]
- 您说的是第五章 part7 最后的那个例子中对 “jal square”这条语句解释说是 “尾调用”么？这个解释的确是错误的，非常感谢指正。正确的解释应该是 leaf call。
- 回复 @unicornxdotw :包括在27分钟时，tail offset的解释好像也是讲错了。
- 回复 @davidlamb :不好意思才看到，我稍后看看，谢谢指正
- 回复 @davidlamb :已经在 v0.9.1 中解决， issue 已关闭。课程勘误表中增加一项说明，请检查。

这个是本科还是研究生课程
- unicornxdotw 本科，非计算机亦可
- lazyparser UP 本科。但不需要到本科就可以学会

请教下UP, 进程切换那里switch_to的实现用t6寄存器来当临时存储 mscratch寄存器的值, 即便t6/x31是最后一个save的寄存器 上下文保存的时候的t6已经被我们改过了, 我怀疑如果用户态程序刚好使用了t6 不就数据损坏了吗
- 你说的有道理，我发现这是代码中的一个 bug。t6 的值忘记保存了。
- 我提了一个 issue：https://gitee.com/unicornx/riscv-operating-system-mooc/issues/I42AUE  
让我们用这个 issue 来跟踪这个问题的解决。如果您有 gitee 账号的话可以加入这个 issue 的讨论以获取及时的进展。 您只需要用 gitee 账号登录后在上面随便写个 comment 就好。  
非常感谢您的细心指出。这个问题比较严重，我会尽快出一个修复的版本。
- 请问您加入我们的 QQ 学习群了么，在那里讨论会比较方便， QQ群号请参考仓库的 README
- 回复 @unicornxdotw :好的，我待会加下群，我感觉t6应该可以放到堆栈中去
- 回复 @木子牙膏 :和其他寄存器一样保存到上下文里去就好了，一开始交换在 mscratch 里备份着呢，在进一步使用 mscratch 之前用这个备份的值恢复到 context 里去
- 已经在 v0.9.1 中解决， issue 已关闭。
- 回复 @unicornxdotw :老哥辛苦！

### POSTER Speech

22日下午，POSTER Speech

基于 RISC-V 设计一门开发操作系统的入门课程（中科院软件所）汪辰
- https://www.bilibili.com/video/BV13f4y1j7bz

  <p><img width="555px" src="https://foruda.gitee.com/images/1660015268992868934/屏幕截图.png"></p>

  大家好，大家下午好，我，也是来自于这个做作业软件所PRCG实验室的，我的名字叫汪晨，今天呢给大家介绍一下，就是我们这边，基于risk five设计的一个开发操作系统的一个课程，那么我们的课程内容，是基于risk five群集介绍，如何循序渐进的，从零开始设计和开发一个小型的操作系统。本课程的目的，是希望对技术软件的推广以及对risk five的宣传起到积极的作用。授课对象，是爱好变成小白了，基本只要具备基本的资源和数据结构基础即可学习上手。我们采用的授课方式，有很多种，就是一个是采用线下教学，同时我们也录制了慕课视频，并且在线公开，同时我们提供了配套的演示代码和练习，也提供了在线的答疑QQ群和微信群，那目前的课程的效果的话，今年4月上线以来，点击的播放量这个数据又稍微有点迟了，那么这个最新的话已经超过了1.3万次，获赞的话应该是有450，加授课人数有四百一千四百。

  - https://www.bilibili.com/video/BV1Q5411w7z5

    ![输入图片说明](https://foruda.gitee.com/images/1660015225546644533/屏幕截图.png "屏幕截图.png")

    <p><img width="555px" src="https://foruda.gitee.com/images/1660015188641469062/屏幕截图.png"></p>

  - POSTER List:

    <p><img width="169px" src="https://foruda.gitee.com/images/1660204845399709826/img_20210622_132532.jpeg" title="RVOS 汪辰"> <img width="169px" src="https://foruda.gitee.com/images/1660204859421354573/img_20210622_131059.jpeg" title="石榴派 袁德俊"></p>

    我们都是 POSTER。这是 峰会首日 下午的 1分钟演讲环节。我是第一个，加上预热用了两分钟。前面的录音文字，我就是见证人，照片是我在台下拍摄的，所有的 POSTER 都坐在主席台左侧，所以拍摄的都是右脸。

  - 6月26日 PLCT RISC-V Day @Shanghai （中科院软件所 PLCT 实验室）

    <p><img width="555px" src="https://foruda.gitee.com/images/1660204334651153762/img_20210626_114425.jpeg"></p>

    <p><img width="555px" src="https://foruda.gitee.com/images/1660204358212405140/img_20210626_134919.jpeg"></p>

    > 这是 RVSC 2021 最后一天上午，忙碌了整个峰会后，PLCT 实验室的 DAY，一直感念峰会志愿者的热情，源头就是PLCT实验室的这群年轻人。和峰会正式的场合不同，自家的日子，吴伟老师尽显风趣幽默，俨然邻家大男孩，峰会期间志愿者们忙坏了，自家的开放日他亲自做起了会务直播，忙前跑后的。整个峰会下来，和志愿者们不分彼此的我，也自在地像在家里。POSTER 演讲不过瘾，石榴派的漂流计划就是邀请的汪老师给我站台，启动的。

### All of POSTER list

<p><img width="11.99%" src="https://foruda.gitee.com/images/1660187986707966034/img_20210623_120429.jpeg"> <img width="11.99%" src="https://foruda.gitee.com/images/1660188006573442739/img_20210623_120417.jpeg"> <img width="11.99%" src="https://foruda.gitee.com/images/1660188028057160116/img_20210623_120343.jpeg"> <img width="21.239%" src="https://foruda.gitee.com/images/1660188047558484478/img_20210623_120321.jpeg"> <img width="11.99%" src="https://foruda.gitee.com/images/1660188064072802496/img_20210623_120308.jpeg"> <img width="11.99%" src="https://foruda.gitee.com/images/1660188081520553706/img_20210623_120257.jpeg"> <img width="11.99%" src="https://foruda.gitee.com/images/1660188098423716535/img_20210623_120251.jpeg"></p>

22日下午，POSTER List

POSTER - 石榴派 - 袁德俊 - 第一届 RISC-V 中国峰会
- https://www.bilibili.com/video/BV1qV411H7j9

  <p><img width="555px" src="https://foruda.gitee.com/images/1660082436126470279/屏幕截图.png"></p>

基于 RISCV 实现声纹识别的探索与实践（平安科技）王健宗
- https://www.bilibili.com/video/BV1B44y127SM

  <p><img width="555px" src="https://foruda.gitee.com/images/1660014500493387727/屏幕截图.png"></p>

  大家好，我是天安科技王建新，深人识别是通过深刻的识别身份、情绪、性别、场景等等的AI技术，主要依存在基于精品的人服务器上，而基于安全和便利的原因多元的体现，生命世界的需求，越来越多。本次我们在一块简单的第二的开发版实现了深人性别识别的。全能产证实了威斯泰的芯片在语音，智能方面有目共睹，后续我们愿意语音派的商家合作，打造更多低廉的语音智能产品的功能，谢谢。


### MagSi：RVOS 成功适配 fpga 软核模拟器

[RVOS 成功适配 NutShell 软核的 NEMU 模拟器](https://gitee.com/RV4Kids/PLCT-Weekly/issues/I43DNU?from=project-issue#note_7831913_link)

> 肝了整个国庆假期，小小完成了 [PLCT](https://gitee.com/RV4Kids/PLCT-Weekly/issues/I47QMY) 的连接 《[RVOS成功适配NutShell软核的NEMU模拟器](https://gitee.com/RV4Kids/PLCT-Weekly/issues/I43DNU)》把玩 Logo 是标配内容。海报封面的两部分成果清晰可见，阳光版石榴派是 OS 发布的主题，而石榴核的根基自然就是石榴派 RV 版的适配了。也正因为有了这两个底气，才真正开启了，石榴派国际漂流，直到安全抵达 GB, 才截稿一个多月的通讯的撰写。（本段引言，补自 2021-12-13）

发表在，石榴派核心开发者期刊 MagSi/[Issue2 - Opreating System Shiliu Pi 99 was Released.md](https://gitee.com/shiliupi/shiliupi99/blob/master/MagSi/Issue2%20-%20Opreating%20System%20Shiliu%20Pi%2099%20was%20Released.md)

在此感谢 PLCT.


