# [日程公布！2023RISC-V中国峰会，超强阵容抢先看！](https://mp.weixin.qq.com/s?__biz=MzUyMzA2NzkzOA==&mid=2247484827&idx=1&sn=b5ef63f23e3dbaa6ce74b49589d32cca)

原创 RVSC2023组委会 CNRV 2023-08-04 19:42 发表于浙江

2023 RISC-V中国峰会（RISC-V Summit China 2023）将于8月23日至25日在北京香格里拉饭店举行。本届峰会将以“RISC-V生态共建”为主题，结合当下全球新形势，把握全球新时机，呈现RISC-V全球新观点、新趋势。本届峰会采用“主会议+技术研讨会+展览展示+同期活动”的方式，预计将举办超过20场主题活动，拟邀请RISC-V国际基金会、业界专家、企业嘉宾及社区代表等出席，预计将吸引超过百家企业及研究机构、开源技术社区，近百家媒体参与报道。

众多精彩会议内容

不容错过

[一起来看详细议程！](https://riscv-summit-china.com/cn/agenda.html)

↓↓↓

![](https://foruda.gitee.com/images/1690985195116771835/b67337a6_5631341.png)

![输入图片说明](https://foruda.gitee.com/images/1691781517888443777/258a782b_5631341.jpeg "image003 (1).jpg")

![输入图片说明](https://foruda.gitee.com/images/1691781528041468638/026917c6_5631341.jpeg "image004 (1).jpg")

![输入图片说明](https://foruda.gitee.com/images/1691781539015734451/ad7c7a2e_5631341.jpeg "image005 (1).jpg")

目前会议报名通道已开启，参会观众、参展商、媒体等均可通过扫描下方二维码参与报名。

![输入图片说明](https://foruda.gitee.com/images/1691217432167100373/f4302c92_5631341.png "640.png")

温馨提示：

1. 请注意选择相应选票类型进行会议注册（参会嘉宾、参展商、媒体等）。

2. 注册审核通过后，注册邮箱将会收到确认邮件，并附参会签到二维码，会议当天凭二维码进行签到入场。

注意：报名通道将于8月14日17:00 截止

### [The agenda is announced!](https://riscv-summit-china.com/en/agenda.html)

The 2023 RISC-V Summit China 2023 (RISC-V Summit China 2023) will be held at the Shangri-La Hotel in Beijing from August 23rd to 25th. The theme of this summit will be "RISC-V Ecological Co-construction", combined with the current global new situation, grasping new global opportunities, and presenting new global perspectives and trends of RISC-V. This summit adopts the method of "main conference + technical seminar + exhibition + concurrent activities". It is expected that more than 20 themed activities will be held. It is planned to invite RISC-V International Foundation, industry experts, corporate guests and community representatives to attend. It is expected to attract more than 100 companies and research institutions, open source technology communities, and nearly 100 media to participate in the report.

Many exciting conference content not to be missed

Let's take a look at the detailed agenda!

↓↓↓

![输入图片说明](https://foruda.gitee.com/images/1691781335156689284/d4dfe92e_5631341.jpeg "image003.jpg")

![输入图片说明](https://foruda.gitee.com/images/1691781345795801991/08b72ed8_5631341.jpeg "image004.jpg")

![输入图片说明](https://foruda.gitee.com/images/1691781357670708281/8f41cceb_5631341.jpeg "image005.jpg")

Currently, the registration channel for the conference has been opened. Attendees, exhibitors, media, etc. can participate in the registration by scanning the QR code below.

![输入图片说明](https://foruda.gitee.com/images/1691781306976553924/d5284aff_5631341.png "屏幕截图")

 **Reminder:** 

1. Please pay attention to select the corresponding ballot type for conference registration (participants, exhibitors, media, etc.).

2. After the registration review is passed, the registered mailbox will receive a confirmation email with a QR code for signing in. On the day of the meeting, sign in and enter the venue with the QR code.

Note: The registration channel will end at 17:00 on August 14

> RISC-V Summit China is considered one of the highest specification, largest scale, and most influential conferences in the RISC-V field in China. The guiding units of this summit include the Institute of Computing Technology of the Chinese Academy of Sciences, the Beijing Municipal Bureau of Economy and Information Technology, the Beijing Municipal Science & Technology Commission, the Administrative Commission of Zhongguancun Science Park, and t Haidian District People’s Government of Beijing Municipality. Beijing Institute of Open Source Chip is the host of the summit, while the co-organizers include the China RISC-V Alliance (CRVA), the RISC-V China Community (CNRV), and the China RISC-V Industry Consortium (CRVIC).

> We will have real-time Chinese-English translations at #RISCVSummitChina! Join us virtually or in-person in Beijing from August 23-25 to learn about technical and business #RISCV innovations. View the agenda and register here: https://riscv-summit-china.com/

  - 关于RISCV技术峰会的消息通知，它宣布在北京举行的RISCV技术峰会将提供实时的中英文翻译服务。届时，参会者可以选择线上或线下形式参加，活动将于8月23日至25日举行，主要内容涉及RISCV技术的技术和商业创新。你可以在网站 https://riscv-summit-china.com/ 查看日程并进行注册。
