![输入图片说明](https://images.gitee.com/uploads/images/2021/0324/015052_dde18a34_5631341.png "屏幕截图.png")

# RISC-V与处理器研究
https://www.yuque.com/riscv

## 功能区：

- 首页   (home)
- 知识库  (book) https://www.yuque.com/r/riscv/books
- 讨论区 （issues） https://www.yuque.com/riscv/topics
- 成员   (members) https://www.yuque.com/riscv/members

## 头条推荐

![输入图片说明](https://images.gitee.com/uploads/images/2021/0324/012918_c9cee99f_5631341.png "屏幕截图.png")

### RISC-V Global Forum 2020
https://www.yuque.com/riscv/rvnews/20200906

 **RV与芯片评论.20200906.第六期** 

本周开了一个好大的会RISC-V Global Forum 2020，然而上周的此处为语雀文档，点击链接查看：https://www.yuque.com/riscv/rvnews/qs2yst还没整理完呢。
这周看到不少关于LPC2020的讨论，比如平头哥又露了一把脸，比如Arnd Bermann对2030年cpu的大胆预言，说起来还有外国网友认为未来的开源CPU只能靠中国了。但是这个情况下，RISC-V倒是面临了IC验证的问题，这周各方大佬都有在讨论，为此我专门列了个专题。另外本周比较热点的就是Imagination的网课了。但总的来说，RISC-V是富有朝气的。

### 重点聚焦

本周3号， RISC-V全球论坛开了十多个小时，有趣的东西不少，但是没来得及整理。小标题整理在这里：
此处为语雀文档，点击链接查看：https://www.yuque.com/riscv/rvnews/dvlmhb
"[Some RISC-V high performance implementations presented at RISC-V Global Forum by European Processor Initiative, NSITEXE, OpenBLAS, and SemiDynamics](http://primeurmagazine.com/weekly/AE-PR-10-20-38.html) (From primeurmagazine.com 2020.09.14)"

- CRVS2020 
  https://www.yuque.com/riscv/articles/crvs2020
- RV与芯片评论.20200801 
  https://www.yuque.com/riscv/rvnews/20200801

## 左侧主展示区

### RISC-V动态
    https://www.yuque.com/riscv/rvnews

[![输入图片说明](https://images.gitee.com/uploads/images/2021/0324/013754_30865c14_5631341.png "屏幕截图.png")](https://www.yuque.com/riscv/rvnews)
RISC-V和芯片动态简报

### 文献和资料库
    https://www.yuque.com/riscv/articles

![输入图片说明](https://images.gitee.com/uploads/images/2021/0324/014141_ac3c821d_5631341.png "屏幕截图.png")

- RISC-V 仿真验证
- CRVS 2020与CCF DAC会议报告: RISC-V部分
- CRVS2020
  - Session 评论
  - Session1. RISC-V生态
  - Session2a. RISC-V处理器及IP设计
  - Session2b. RISC-V指令扩展
  - Session3a. RISC-V验证平台和SoC平台
  - Session3b. RISC-V基础软件和工具
  - Session4A. 中国RV河南分中心报告会
- 走进RISC-V开源指令集及其软件生态

### 技术
    （空）

## 右侧侧边条

### 快捷入口

- RISC-V International https://riscv.org/
- 中国半导体行业协会 http://www.csia.net.cn/

### 最近发布的文档
 
- RV与芯片评论.20210320：2021年第12周(总第34期)
  https://www.yuque.com/riscv/rvnews/20210320

### 最近更新的知识库

- RISC-V和芯片动态简报 https://www.yuque.com/riscv/rvnews
  更新于：03-20 17:06

- 文献库 https://www.yuque.com/riscv/articles
  更新于：2020-11-10 15:06

### 最新讨论

- 拉袁老师入伙吧！yuandj 创建于 今天 01:23 0