- FPGA的架构和工具解析 | 大话 “万能的FPGA？”
- 往期回顾 | 歪睿老哥的2021年终总结 
- "乾坤大挪移"，FPGA工程师七层技术修炼之道

<p><img width="706px" src="https://foruda.gitee.com/images/1675343521338054712/3a1ac377_5631341.jpeg" title="521万能的FPGA副本.jpg"></p>

> 这篇 FPGA 结构讲解的最清楚的文章，可以缓解 IC-newer 一知半解的状况。特地收录了老哥的 FPGA工程师七层技术修炼，让同学们的成长之路不再迷茫，找准定位加油努力吧。“把握一个方向，勤于修炼，总能不断精进。” 这是前辈的良言，请记心间 :pray: 

首发于：[歪睿老哥和芯片那些事](https://www.zhihu.com/column/c_1341359196768645120)

![输入图片说明](https://foruda.gitee.com/images/1674968257992975142/f5fd64ca_5631341.png "屏幕截图")

# [FPGA的架构和工具解析](https://zhuanlan.zhihu.com/p/427787229)

公众号：歪睿老哥

> 一个芯片设计老哥；

### 0：芯片和FPGA的本质

说起FPGA之前，先提个问题。

芯片的本质是什么？

老哥认为芯片的本质是电路！

简单来说，数字芯片，不论多复杂，其底层就是 与，或，非的组合。

这个是某宝上可以买到的世界上简单的芯片之一， 74LS系列，很便宜，两毛钱；

![输入图片说明](https://foruda.gitee.com/images/1674992131009302144/a5f65429_5631341.png "屏幕截图")

其功能就是二输入与非门。这是一个最简单的芯片，其电路和版图如下

![输入图片说明](https://foruda.gitee.com/images/1674992136766708297/8795475a_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1674992145741000544/e6ddf497_5631341.png "屏幕截图")

上图中一共4个二输入与非门。

而CPU或者GPU等大芯片，有几千万门甚至上亿门的电路。

但是如果我们深入到大芯片的底层，就会发现这些大芯片也是一个与非门，或非门，等逻辑门组成的。

这就是电路，CPU和GPU也是一种电路的组织形式。

无论多复杂的芯片，都是芯片设计工程师通过硬件描述语言（HDL）来描述电路。

看起来像是和软件工程师一样，都在敲代码，在编程，实际上是在搭电路。

EDA工具把语言转换成电路，最终得出这个版图（GDS）。然后这个版本提交给厂家生产。流片厂家把GDS变成硅。封装厂家完成硅 （DIE）到 CHIP的封装。

这个过程和设计PCB电路基本上是非常相似的。

都是电路转换成PCB 到厂家制版。

我们所说的卡脖子，目前看，主要是制造环节，也就是从版图到硅的阶段。其他也有，但是这个问题这个有时间找专题另说。

今天主要说一下 ，设计一款芯片研发最大的问题是啥？

有两点是大家公认的。

 **第一，研发迭代周期长：** 

大芯片花费了一年或者2年研制，这个很常见。或者因为功能缺陷，或者市场变化，最终没有卖出去，又要重新迭代。这个就是非常恐怖，小的芯片公司，因为芯片失败，钱花光，直接一波流带走也是很常见的。

 **第二，芯片投入高：** 

芯片研发包括流片成本，IP成本，人力成本等。28nm的MASK在1000万，12nm接近2500万，除此之外还有人力，IP等成本。研发一颗28nm的芯片总投入成本投入大几千万还是有的，7nm和5nm更是几亿的投入。这么高的成本（NRE），将来要分摊到每一颗芯片上去的。

如果一个项目或者需求，只有几千颗或者几万颗的量，是否值得来去研发芯片就是一个很大的问题。

流片不合适，那么没有一种替代的方案？迭代周期短，代价小的实现这个电路的需求。

FPGA，他来了！

### 1：FPGA：实现电路的电路

FPGA

Field Programmable Gate Array。

现场可编程门阵列。

FPGA和专用芯片不同。

可以编程。

文章开头说过了， 芯片的本质就是电路。

那么FPGA的本质是什么？

FPGA本质也是一种芯片。

但是可以也可以实现数字电路功能，如CPU，GPU，NPU等这些电路，都可以放在FPGA内部实现，效率另说。。

那FPGA的本质就是可以通过编程实现电路的电路。

这个是怎么做到的？

或者说，什么样的电路可以实现与或非这些基本操作

我们以 F=A&B&C&D 这个电路举例：
一个16x1的RAM，这个RAM的每一bit都可以编程为0或1。

这个RAM有4位地址， DCBA。通过这4bit选择RAM的输出。

通过配置RAM中不同的值，实现输出F 和输入A，B，C，D的关系

![输入图片说明](https://foruda.gitee.com/images/1674992182422590061/ae43e16b_5631341.png "屏幕截图")

上图中，我们把16bitRAM 配置为 0000000000000001 ，这个电路 则等效 F=A&B&C&D；

只有A=B=C=D=1时， F=1，其他情况 F=0；

完美实现了 F=A&B&C&D；

重要的事情再说一遍；

16bitRAM 配置为 0000000000000001 ， 则等效F=A&B&C&D；

那么现在“0000000000000001” 这串数就是FPGA的编程。

这就是FPGA最基本的原理；

举一反三，如果实现电路 F=A|B|C|D 。

这个电路如何编程：

![输入图片说明](https://foruda.gitee.com/images/1674992198382086082/740740e6_5631341.png "屏幕截图")

16bit RAM 配置为 01111111111111111， 则等效F=A|B|C|D。

大家可以试一下：通过配置16bitRAM 的值，可以实现A，B，C，D四个输入的任何逻辑操作。

FPGA就是利用了这个转化，具备了描述任何电路的能力。

上图这个结构在FPGA中有一个专有名词，叫做LUT ，lookup table（ 查找表）。

LUT就构成了所有FPGA的最基本的单元。

LUT只能实现数字组合逻辑，所以又添加了一个寄存器flipflop （ff），可以实现数据的锁存；

如下图所示：LUT+寄存器构成了现代FPGA基本结构。

FPGA的基本结构，就是依靠如此简单的电路实现了无比复杂的逻辑。

这个包括LUT和FF的基本结构，这两个合并成为一个基本的逻辑单元（LOGIC BLOCK）。

![输入图片说明](https://foruda.gitee.com/images/1674992210431463284/32b07a39_5631341.png "屏幕截图")

这种能够实现ABCD四个输入计算的LUT的叫做4输入LUT，此外还有5输入，6输入等等变种。

万变不离其宗。

这种结构，从FPGA诞生以来，就没有怎么变过。

这个电路，也可以看作是一个最小的FPGA。

现在能实现一个功能的芯片，少则几万门，多则几千万门，上亿门。

单纯靠这个电路实现，这就是开玩笑了。

那么就需要无数的LUT和FF来实现，。

FPGA就实现了一个无数 Logic Block（Logic Block内部就是LUT+FF）的阵列，中间用布线资源连接起来。

把互联和逻辑单元结合起来就是一个FPGA芯片，图如下所示。

![输入图片说明](https://foruda.gitee.com/images/1674992226599692199/c129ec58_5631341.png "屏幕截图")

一个典型的FPGA开发流程如下。从HDL（verilog的电路描述语言）到配置文件 bit流

相比一下，专用芯片的开发流程从HDL（verilog的电路描述语言）到硅。这个时间就长多了。

![输入图片说明](https://foruda.gitee.com/images/1674992233865306296/596d9254_5631341.png "屏幕截图")

这些bit流包括啥？

开头说过了，16bitRAM （LUT）配置为 0000000000000001 ，

则等效F=A&B&C&D；

FPGA最终生成的Bit-stream流，包括 LUT 的配置文件，以及布线资源的配置文件。

到这里，FPGA的设计及编程就完成了。

简单明了！

### 2：EDA工具：从知到行的距离

看起来设计一个FPGA芯片也不复杂。

电路不复杂，市场上高性能的FPGA，可选择的余地不多。

从知到行，这里面有一个巨大的鸿沟，

按照本文开头的原理，假如一个厂商设计完毕了一款FPGA芯片。

等到给客户使用的时候，就碰上了一个大麻烦。

EDA工具。

如果给客户提供FPGA芯片，则需要配套提供给客户一个EDA工具。

没有EDA工具，难不成让客户手动生成FPGA的bitstream的文件。

芯片都做出来了，EDA工具，还难吗？

难，真的难！

这是一个巨大的坑。

下图是一个 开源的FPGA的设计流程（OpenFPGA），我们可以看看，即使一个开源项目，其涉及到的EDA工具最少都要有哪些？

![输入图片说明](https://foruda.gitee.com/images/1674992331613695513/99eec9cc_5631341.png "屏幕截图")

这些需要给客户的EDA工具包括：

1.  **综合工具** 

2.  **布局布线工具** 

3.  **bit生成工具** 

4.  **时序分析工具** 

5.  **仿真工具** 

6.  **嵌入式逻辑分析仪器等调试工具** 

7.  **功耗分析工具** 

这些还算是最少的集合，但已经比一个CPU芯片提供给用户GCC编译工具要难多了。

老哥装过某司的EDA工具，这些大小都是十几个G，比windows安装盘还要大。

如果CPU的GCC工具难度是1，FPGA的EDA工具的难度就是10到100。

借用《让子弹飞》里的一段话：

项目成功了，芯片功劳怎么才占7成。

七成是EDA的，芯片也就三成。

就这三成，还要看EDA的脸色。

辛辛苦苦半天，做出来芯片，还要看EDA工具的脸色。

看看EDA都有哪些“脸色”？

![输入图片说明](https://foruda.gitee.com/images/1674992378134522745/94bab4c7_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1674992657598287702/2f6e2f89_5631341.gif "v2-d832a93b230530671d925d64d516cb2c_b.gif")

![输入图片说明](https://foruda.gitee.com/images/1675100284105181051/7b72877c_5631341.gif "kogfq-75p7t.compressed.gif")

![输入图片说明](https://foruda.gitee.com/images/1675100623013262574/97818341_5631341.gif "2igsw-0mcbh.compressed.gif")

以上图的布局布线工具为例，这个“脸色”看起来就比较复杂。

如果做不好，整个FPGA的利用率极低，还布不通，你说重要不重要。

除了传统的EDA工具，实现HDL，例如verilog这种编程之外，还包括HLS这种高层次综合的描述语言。

![输入图片说明](https://foruda.gitee.com/images/1675100714661709042/41956617_5631341.png "屏幕截图")

HLS的引入使软件工程师也能参与到FPGA设计中来，把电路开发屏蔽掉，直接开发软件，这个就更是EDA工具的能力了。

但是，这个本质上，讲高级语言（C，C++）转换成硬件描述语言（HDL），在通过综合工具转换成电路。

好处是，更贴近软件工程师的习惯，缺点就是加了一层转换，带来了效率的损失。

### 3：FPGA的架构：融合超越

文章开始说了，FPGA就是Logic Block，内部主要是（LUT+FF），以及布线资源。

除了这些。

FPGA还有很多硬核IP，也叫做宏单元。

例如PLL，SERDES，RAM等常规IP的。

随着现在芯片的演进。

FPGA内部也集成了很多新的东西。

这些新东西中，比较有特色的就是CPU，在FPGA内部集成了硬核CPU系统，甚至可以运行OS（操作系统）。

这样CPU+FPGA。

CPU软件编程，FPGA 硬件电路编程，

这个就是双剑合璧，更胜一筹。

同样serdes也是高端FPGA的必须模块，没有serdes，FPGA就是孤家寡人，没有办法实现和其他芯片电路的高速互联。

现在与外部器件连接，支持比如PCIe，SATA，10G/100G ethernet这些高速协议。

数字信号处理，FPGA应用于数字信号处理等计算密集型任务，如雷达等等

所以，FPGA内部也集成了了很多DSP单元，实现乘法等操作。

为了在人工智能时代分一杯羹，有的FPGA内部集成了用于AI处理的神经网络加速的硬核。

总结一下，除了LUT，FPGA内部集成硬核IP包括：

1. **RAM：用于实现存储资源；** 

2. **PLL：提供高速的时钟信号和资源；** 

3. **DSP：乘法操作，滤波器，数字信号处理模块；** 

4. **SERDES：实现PCIE，SATA，FC，100G ethernet等高速接口** 

5. **CPU系统：提供软件编程开发能力；** 

6. **NPU硬核：提供AI处理加速；** 

而根据市场需要，还可能添加更多的硬核IP 与传统的FPGA进行融合。

融合更多功能IP。

这就是FPGA架构未来的趋势。

### 4：FPGA优势：解决问题才是王道

FPGA和CPU有什么区别和优势？

我们讨论一种架构的优劣，重点是解决了什么问题？

而不是CPU与FPGA，孰优？

只有放在固定的应用场景下，才能说哪种架构更适合解决这些问题。

FPGA编程是电路，本质上还是可以看作，逻辑门（与或非）等等效电路。

CPU是指令操作，运行的是软件。

FPGA是时间并行运行的，而CPU是时间串行的，单CPU核总是需要一条一条来执行指令来实现功能（也有指令级并行，但原理不变）。

FPGA的并行度较高，相比CPU的计算方式，数据吞吐量大，时延控制比较好。

但是CPU的频率很高，可以运行操作系统，作为通用计算单元非常灵活，并不是FPGA更能做的。

FPGA更适合做ASSP，配合CPU做专门的运算单元使用，适合专一的大数据量的计算。

FPGA既然是”万能的芯片“，似乎那就可以替代实现所有芯片功能。

是不是可以不研发专用芯片，只用FPGA。

相比专用集成电路：FPGA有三个劣势

 **1：面积大，成本高：** 

和专用集成电路（芯片）相比，FPGA由于采用LUT来表征基本逻辑门单元，所以其面积粗略来比是专用电路的10倍，所以其成本简单类比也会高很多。

 **2：面积大，功耗高：** 

同样功耗也没有优势，做低功耗设备，例如手持的供电式设备，基本不太可行。

 **3：运行频率低，计算效率低。** 

FPGA内部LUT之间，由于其互联较长，导致FPGA的频率相比同工艺下的ASIC也是慢很多。专用电路门与门之间的延迟要小很多。

但是，FPGA优势也很明显。

FPGA最大的优势，就是灵活性高。

用FPGA不用重新流片，节约了NRE成本。

在一些雷达，5G，网络，存储，高性能计算等数据密集型计算领域都有广泛的应用。

特别是需求不明确，量不大，不值得做芯片，或者，需求老在变化，FPGA都是不错的选择。

intel收购后altera后， FPGA的重心变成了数据中心市场，特别是利用FPGA在数据中心加速方面，投入颇大，最新的intel的IPU其中是FPGA技术，用于数据中心基础设施的卸载，也是类似DPU的一种实现。（见上一篇：大话DPU—从网络到数据）。

目前很多家DPU都是用Intel和Xilinx的FPGA实现的。

这个就是DPU需求不固定，数据中心对于DPU的需求一直在变化的一种体现。

FPGA全球市场2018年大约60亿美金，Xilinx 和Altera 是这个FPGA市场上最重要的提供者，其他都是比较小的公司。（2015年英特尔宣布以167亿美元收购FPGA厂商Altera），给其他家留的份额就不多了。

![输入图片说明](https://foruda.gitee.com/images/1675100988612686455/db86f139_5631341.png "屏幕截图")

老哥早些年，这两家FPGA都使用过，各有千秋。

国内也有很多FPGA的公司，有的出货也比较客观。与国外巨头相比，处于解决了有无的问题阶段，能够满足部分国产化需求。其容量，性能，特别是EDA工具方面，有明显的差距，还需要继续市场的磨练。

这里举两个国产FPGA的非典型案例，非常有意思。

2014年，毛熊并入克里米亚，M国出售制裁，M国对毛熊的封锁禁运也是很厉害，毛熊芯片告急，特别是高端的FPGA芯片，某司FPGA芯片抓住这个机会，解决了国际友人的燃眉之急，成就了某司的FPGA出口，也获得不错的利润。

另外，某手机大厂手机屏幕是有特殊转码格式的，所以如果返修，只能使用原厂屏，其他屏幕格式对不上，原厂屏价格很高。某司就定做了一批超小型FPGA，实现了手机屏的解码，中国的屏幕提供厂很多（LCD+LED等等），直接就可以替代原厂屏，在手机返修屏市场也是卖的风声水起。

在这些细分领域，找到了定位，实现了很大的突破，不论是利润还是数量。

如果有数量几十万片，几百万片，甚至更多，做专用芯片更合适。

如果没有那么多数量，需求又不固定，用FPGA更合适。

芯片设计中，其中有个环节叫做FPGA原型验证，FPGA原型验证就是把芯片代码放在FPGA做原型实现，加快芯片设计迭代的速率。

本文开始说：FPGA特性，就是能够描述芯片电路。

所以数字芯片流片之前，用FPGA装入芯片逻辑来等效测试，也是非常重要的一个环节。

从这个角度看，FPGA和芯片（专用集成电路）从来就不是对立的，

FPGA，万能芯片，从功能上看是万能的，理论上可以实现所有功能。

但是从，PPA上看，性能，功耗，面积（成本），这三个维度来衡量，又是非常受限的。

FPGA也在进化，拓展更多的领域，满足那些变化的市场需求。

也有部分市场需求更为固化，被专用芯片所取代。

万能的芯片，变化的应用。

找到市场定位，解决用户的问题，才能获得一席之地。

FPGA是如此，芯片也是如此。

发布于 2021-10-31 16:28

- 系统架构
- ARM 架构
- 现场可编辑逻辑门阵列（FPGA）

---

8 条评论

- 夜云 2021-10-31

  > EDA要慢慢来，步子迈大了，咔，容易扯着蛋。国内EDA还有挺长的路要走，就教育部分都是空的。

- 陈磊 2021-10-31

  > 谢谢大佬！有一个问题再说说就好了，就是可编程非易失性的物理原理是啥，这个最让人惊奇，直观上总感觉需要在最底层熔断烧结一些孔洞沟槽啥的才能制作电路，但是显然事实不是这样，这个最神奇

  - Jeff 2021-11-01

    > 非易失性存储的核心单元是一种叫做浮栅管单元的东西。它是一种点那三端器件，放置在二氧化硅绝缘层中。施加电压之后，电子可以穿越绝缘层，形成了电势差。而断电之后，电子不能穿越绝缘层，所以电势差就可以得到保持。相当于，那1bit的信息就被存储下来了，掉电也不影响了啊。

  - 陈磊 re: 歪睿老哥 2022-07-18

    > 老哥，谢谢，今天了解到一个新的fpga，actel这个牌子的可以直接用flash配电路，不用flash配ram，ram配电路了

  - 歪睿老哥 作者 2021-10-31

    > FPGA有一个配置的flash芯片，上电时，flash芯片内容写到内部。FPGA内部有一个配置电路。[握手]

  - 陈磊 re: Jeff 2021-11-01

    > 神奇，绝缘也能读取？ 读取后也不流失电荷，微观比较颠覆很多电学常识。发明这些的人真厉害[赞]

  - Jeff re: 陈磊 2021-11-01

    > 所以叫半导体嘛[飙泪笑]

- MinghuaShen 2021-11-08

  > 作者使用V. Betz的图片和文字翻译要加引用啊。

---

文章被以下专栏收录

- [歪睿老哥和芯片那些事](https://www.zhihu.com/column/c_1341359196768645120)

  > CPU，FPGA，AI，DPU等芯片行业那些事。

![输入图片说明](https://foruda.gitee.com/images/1675335940154782379/a608b248_5631341.png "屏幕截图")

> 这篇非常符合老哥气质。不知道，众多武林豪侠，老哥最喜欢哪个人物，为什么？我想等老哥有时间，SIGER读者再学习研究一段，可以为老哥做一次专访，再多列些问题，以餐读者。

# ["乾坤大挪移"，FPGA工程师七层技术修炼之道](https://mp.weixin.qq.com/s?__biz=Mzg3NjU2NDIzMQ==&mid=2247484086&idx=1&sn=a335b5ceeebc5d5e6bc565253e59cc87)

![输入图片说明](https://foruda.gitee.com/images/1675336871357876254/30098173_5631341.png "屏幕截图")

原创 歪睿老哥 歪睿老哥 2021-09-10 08:05

![输入图片说明](https://foruda.gitee.com/images/1675336353279564216/162f382d_5631341.png "屏幕截图")

- 微信号：歪睿老哥 verilog-2001
- 功能介绍：一个芯片设计行业老哥；忙时研发，闲时写作；聚焦芯片行业的那些事，唯武侠与芯片不可辜负。

如何成长为一个合格的FPGA工程师？

武林中，乾坤大挪移有七层的修炼境界，FPGA工程师技术修炼之路也是如此。

### 第一层：熟悉代码，能做仿真。

作为FPGA初级工程师，首先应该要会写veriog代码，会写testbench。会用modelsim或者VCS仿真工具。能够写模块级别的代码，能够仿真，算是精通第一层。

虽然第一层看似简单，但是修炼不好就会影响进阶之路，主要是verilog设计基本功，电路模块设计思想，而不是简单凑时序。代码编程质量以及设计的高内聚，低耦合的模块划分原则。这一层是后续修炼的基础。

### 第二层：熟悉架构，能写约束

对于FPGA内部资源如数家珍，能够有效利用FPGA内部资源，例如各种类型的RAM，LUT，BUG，serdes等等。通过熟悉这些知识，工程师可以进行FPGA选型。

根据需要约束设计：

1. IO的约束：这个相对简单；
2. 时序的约束：这个需要对外部芯片接口时序非常熟悉；
3. 位置的约束：这个需要在布局布线不通时，或者时序比较紧张时，通过位置约束来满足设计需求；

精通此层的人，可以称得上算是会FPGA了，略窥门径，有所小成。

### 第三层：熟悉接口，能担项目

此层的工程师熟悉掌握XILINX等FPGA的IP。例如PCIE的IP，XDMA， SATA，rapid  IO，Ethernet等等。

可以把XILILNX厂商的用例跑通，可以在用例的基础上，通过添加及修改实现用户需求及功能。

客户有需求，工程师可以很快接手，进行功能分解，完成代码开发，交付项目。

精通此层的人，可以成为FPGA项目负责人。

### 第四层：定位问题，解决问题

可以通过各种调试解决问题，熟悉使用各种硬件，软件，FPGA内部逻辑分析仪等调试手段。

解决包括且不限于硬件问题，软件问题，FPGA问题。一句话：解决别人解决不了的问题。

在公司里面，大家有FPGA解决不了的问题就找他解决。专业解决各种疑难杂症。

精通此层的人，通常在公司被称为大牛。

### 第五层：略懂算法，庖丁解牛；

此层境界，可以将某些算法协议，创造性实现在FPGA上。

这些算法协议经常的例子包括：

AI的inference。这种能力可以将AI的python程序变成verilog在FPGA上高效实现。

TCP的offload。这种能力可以将系统kernel级别的C程序，别人不知道如何下手，可以在FPGA上干净利索的实现。

核心指标是在FPGA上能够完成别人搞不定的算法，或者别人达不到的性能。（注意：这个复杂度是AI的性能指标比被人强或者TCP卸载并发数据流比别人多来衡量的，并不是一般的算法实现）

精通此层的人，经常被称为架构师或者技术专家。

### 第六层：体系结构，软硬划分。

修炼此层境界的人才精通体系结构，实现软硬件划分。熟悉例如kernel，虚拟化，容器，SRIOV，virtIO等。了解如何这些需求和FPGA的硬件能力配合，

精通此层的人，可以实现例如虚拟化OVS卸载，P4协议实现，裸金属云管理，云端AI加速等等。

此层大神可以通过FPGA实现类似DPU的卸载，目前的确也有很多公司做FPGA来实现做DPU。

的确FPGA挺合适来做DPU的，尤其是在DPU功能比较不确定的情况下。ASPLOS 2020阿里案例的裸金属云bm-hypervisor管理网卡，就是FPGA实现（见 [云端芯片之战-小乌云还是大风暴](http://mp.weixin.qq.com/s?__biz=Mzg3NjU2NDIzMQ==&mid=2247483844&idx=1&sn=275daa43908651f1b99e17c7d35cb151)）。

精通此层的人，可以去技术创业，大公司做研究员，小公司可以做技术总监。

### 7：第七层：“日盈昃，月满亏蚀”。

乾坤大挪移的第七层，张无忌也没有练成过，

“原来当年创制乾坤大挪移心法的那位高人，内力虽强，却也未到相当于九阳神功的地步，只能练到第六层而止。他所写的第七层心法，自己已无法修炼，只不过是凭着聪明智慧，纵其想象，力求变化而已。张无忌所练不通的那一十九句，正是那位高人单凭空想而想错了的，似是而非，已然误入歧途。” 

电影《苏乞儿》中，周星驰扮演的主人公苏乞儿最后一掌“亢龙有悔”解决掉大boss，悟到了第十八掌是前十七掌的组合。苏乞儿通过这些基本招式的组合成额外的一招，提升了掌法的威力，

![输入图片说明](https://foruda.gitee.com/images/1675336688023248898/841c2154_5631341.png "屏幕截图")

通过上述案例，类比一下。

那么，FPGA的第七层，那就是能够融汇贯通前面6层的能力，实现的从顶层到底层全栈能力。

精通此层人，通常被称之为“大神”。

不过大神啥都做，就会比较累

“ **日盈昃，月满亏蚀** ”。

把握一个方向，勤于修炼，总能不断精进。

但行好事，莫问前程，心之所向，无问西东。

>  **欢迎关注：歪睿老哥，如果你觉得本文还不错，欢迎点赞，在看，分享。** 

![输入图片说明](https://foruda.gitee.com/images/1675336326647169289/6f047b27_5631341.png "屏幕截图")

### 往期阅读：

- [苹果也来凑热闹，能否补齐RISC-V的短板](http://mp.weixin.qq.com/s?__biz=Mzg3NjU2NDIzMQ==&mid=2247484075&idx=1&sn=a9b85c139d8b4aaf5e8e3d98e54724cd)？
- [这个刷爆全网视频的背后，是“元宇宙”的未来](http://mp.weixin.qq.com/s?__biz=Mzg3NjU2NDIzMQ==&mid=2247484052&idx=1&sn=9129a47470069c4c97a45cfdf25bdfe9)
- [资本宠儿慕容复，芯片创业为什么会失败？](http://mp.weixin.qq.com/s?__biz=Mzg3NjU2NDIzMQ==&mid=2247484037&idx=1&sn=f60e0eb3ccb07f8fd16a6fca3c931f89)
- [大话手机处理器-世界上最复杂的芯片](http://mp.weixin.qq.com/s?__biz=Mzg3NjU2NDIzMQ==&mid=2247484023&idx=1&sn=de1a3b5a840662bc332c205c833fb348)
- [日本芯片产业大败局](http://mp.weixin.qq.com/s?__biz=Mzg3NjU2NDIzMQ==&mid=2247484006&idx=1&sn=79ca6b63b411b8e1fb8230df9c158139)
- [从技工到英特尔CEO，Pat Gelsinger的逆袭](http://mp.weixin.qq.com/s?__biz=Mzg3NjU2NDIzMQ==&mid=2247483995&idx=1&sn=3ea8f2e1a7d80127bf5af5f5bd0fe900)
- [“硅仙人”Jim Keller的芯片研发封神之道](http://mp.weixin.qq.com/s?__biz=Mzg3NjU2NDIzMQ==&mid=2247483956&idx=1&sn=b513f2dc026d51b99da0094e9a22d100)
- [英特尔收购，RISC-V赢了](http://mp.weixin.qq.com/s?__biz=Mzg3NjU2NDIzMQ==&mid=2247483943&idx=1&sn=a738eb216bba56520f7e331e585daf12)
- [香山：开源高性能RISC-V处理器](http://mp.weixin.qq.com/s?__biz=Mzg3NjU2NDIzMQ==&mid=2247483987&idx=1&sn=3ccbce02e505a16c3f0315ee83433af7)
- [RISC-V理事会，80%的最高级别会员被中国企业申请了](http://mp.weixin.qq.com/s?__biz=Mzg3NjU2NDIzMQ==&mid=2247483928&idx=1&sn=06905eceaa9f86306f07cf711c57f72e)？
- [处理器指令集，不是一本武功秘籍](http://mp.weixin.qq.com/s?__biz=Mzg3NjU2NDIzMQ==&mid=2247483888&idx=1&sn=2f346ed6022523db095b9ea6415fe2ff)

————— [2023-02-02](https://gitee.com/shiliupi/A-Z80/issues/I6B7IJ#note_16007873_link) —————

歪睿老哥 14:59

- [往期回顾](http://mp.weixin.qq.com/s?__biz=Mzg3NjU2NDIzMQ==&mid=2247484230&idx=1&sn=7ae19224d6ec00a208125a6b959c4717)

  (更新了往期回顾里未收录的11篇最近的分享，重点推荐：《[ **2021年终总结** ](http://mp.weixin.qq.com/s?__biz=Mzg3NjU2NDIzMQ==&mid=2247484362&idx=1&sn=09465c849ddfcea19952eb268c985466)》)

  - [给大家拜年了！](http://mp.weixin.qq.com/s?__biz=Mzg3NjU2NDIzMQ==&mid=2247484407&idx=1&sn=082ba76e34c1d0d0e551acd9811db0bc)
  - [为芯片奠定数学基础的那些大神们都是谁？](http://mp.weixin.qq.com/s?__biz=Mzg3NjU2NDIzMQ==&mid=2247484402&idx=1&sn=38524ae77667933b089ce39ae21333e5)
  - [当帝国时代爱上魔兽世界，我的青春早已不再。](http://mp.weixin.qq.com/s?__biz=Mzg3NjU2NDIzMQ==&mid=2247484385&idx=1&sn=6e6e2dafaf70bb47a62486b64f666fcd)
  - [边缘AI芯片是个什么玩意？](http://mp.weixin.qq.com/s?__biz=Mzg3NjU2NDIzMQ==&mid=2247484377&idx=1&sn=1872b84fda0763b3a63e569fa4bddacc)
  - [ **歪睿老哥的2021年终总结** ](http://mp.weixin.qq.com/s?__biz=Mzg3NjU2NDIzMQ==&mid=2247484362&idx=1&sn=09465c849ddfcea19952eb268c985466)
  - [为什么互联网大厂喜欢扎堆造“芯”](http://mp.weixin.qq.com/s?__biz=Mzg3NjU2NDIzMQ==&mid=2247484358&idx=1&sn=705a3138f5a5733f14523d71e272d14c)
  - [FPGA芯片同时识别22张人脸，总共需要几步？](http://mp.weixin.qq.com/s?__biz=Mzg3NjU2NDIzMQ==&mid=2247484339&idx=1&sn=5009249a38bfb6bcaca6d4beeb8d59b6) （#关于FPGA 1/3）
  - [大佬口中的“颠覆式”芯片技术，我差点就信了！](http://mp.weixin.qq.com/s?__biz=Mzg3NjU2NDIzMQ==&mid=2247484309&idx=1&sn=cbbc81c259d9f9c4f76b97d1fd9a9dc8)
  - [芯片工艺的5nm和7nm是怎么来的？揭开芯片工艺和摩尔定律背后的“秘密”](http://mp.weixin.qq.com/s?__biz=Mzg3NjU2NDIzMQ==&mid=2247484293&idx=1&sn=160f1e61c570a3224aba21ab522f0347)
  - [EDG夺冠了！EDA也夺冠了，这个冠军还解决了一项芯片难题！](http://mp.weixin.qq.com/s?__biz=Mzg3NjU2NDIzMQ==&mid=2247484269&idx=1&sn=055b78b53cf6e39bc10c415a103417e6)
  - [芯片工程师为什么变的这么贵？](http://mp.weixin.qq.com/s?__biz=Mzg3NjU2NDIzMQ==&mid=2247484239&idx=1&sn=e52a07dffbdf97d7454180ace907ef6d)

  申墨-歪睿老哥 歪睿老哥 2021-10-31 23:16

  ![输入图片说明](https://foruda.gitee.com/images/1675339773698577255/3a8c6b19_5631341.png "屏幕截图")

  1. [人类高质量芯片工程师的那些“黑话”](http://mp.weixin.qq.com/s?__biz=Mzg3NjU2NDIzMQ==&amp;mid=2247484224&amp;idx=1&amp;sn=1a499ade8c683c2475bcd098cf1002cd)
  1. [从人训练AI，到AI训练AI，路还有多远？](http://mp.weixin.qq.com/s?__biz=Mzg3NjU2NDIzMQ==&amp;mid=2247484185&amp;idx=1&amp;sn=c50457734e67e115169ff9ca88c6c30e)
  1. [大话FPGA-“万能的芯片？”](http://mp.weixin.qq.com/s?__biz=Mzg3NjU2NDIzMQ==&amp;mid=2247484177&amp;idx=1&amp;sn=be5c7ffe31329d942e5da159025f9194)

     ![输入图片说明](https://foruda.gitee.com/images/1675340008664214650/e4105746_5631341.png "屏幕截图")

  1. [大话DPU—从网络到数据](http://mp.weixin.qq.com/s?__biz=Mzg3NjU2NDIzMQ==&amp;mid=2247484134&amp;idx=1&amp;sn=05891309cbb1385ca0e77c204228f867)
  1. [芯片代工产业简史-创造自己，也创造了客户](http://mp.weixin.qq.com/s?__biz=Mzg3NjU2NDIzMQ==&amp;mid=2247484112&amp;idx=1&amp;sn=dadd78c8d23d88dfeee698daf549f282)
  1. [“无法破解的芯片”到底是个什么原理？](http://mp.weixin.qq.com/s?__biz=Mzg3NjU2NDIzMQ==&amp;mid=2247484101&amp;idx=1&amp;sn=7ad5973ec46f4e648dde6d49523e505d)
  1. [乾坤大挪移，FPGA工程师七层技术修炼之道](http://mp.weixin.qq.com/s?__biz=Mzg3NjU2NDIzMQ==&amp;mid=2247484086&amp;idx=1&amp;sn=a335b5ceeebc5d5e6bc565253e59cc87)
  1. [苹果也来凑热闹，能否补齐RISC-V的短板？](http://mp.weixin.qq.com/s?__biz=Mzg3NjU2NDIzMQ==&amp;mid=2247484075&amp;idx=1&amp;sn=a9b85c139d8b4aaf5e8e3d98e54724cd)
  1. [这个刷爆全网视频的背后，是“元宇宙”的未来](http://mp.weixin.qq.com/s?__biz=Mzg3NjU2NDIzMQ==&amp;mid=2247484052&amp;idx=1&amp;sn=9129a47470069c4c97a45cfdf25bdfe9)
  1. [资本宠儿慕容复，芯片创业为什么会失败？](http://mp.weixin.qq.com/s?__biz=Mzg3NjU2NDIzMQ==&amp;mid=2247484037&amp;idx=1&amp;sn=f60e0eb3ccb07f8fd16a6fca3c931f89)
  1. [大话手机处理器-世界上最复杂的芯片](http://mp.weixin.qq.com/s?__biz=Mzg3NjU2NDIzMQ==&amp;mid=2247484023&amp;idx=1&amp;sn=de1a3b5a840662bc332c205c833fb348)
  1. [日本芯片产业大败局](http://mp.weixin.qq.com/s?__biz=Mzg3NjU2NDIzMQ==&amp;mid=2247484006&amp;idx=1&amp;sn=79ca6b63b411b8e1fb8230df9c158139)
  1. [从技工到英特尔CEO，Pat Gelsinger的逆袭](http://mp.weixin.qq.com/s?__biz=Mzg3NjU2NDIzMQ==&amp;mid=2247483995&amp;idx=1&amp;sn=3ea8f2e1a7d80127bf5af5f5bd0fe900)
  1. [香山：开源高性能RISC-V处理器](http://mp.weixin.qq.com/s?__biz=Mzg3NjU2NDIzMQ==&amp;mid=2247483987&amp;idx=1&amp;sn=3ccbce02e505a16c3f0315ee83433af7)
  1. [“硅仙人”Jim Keller的芯片研发封神之道](http://mp.weixin.qq.com/s?__biz=Mzg3NjU2NDIzMQ==&amp;mid=2247483956&amp;idx=1&amp;sn=b513f2dc026d51b99da0094e9a22d100)
  1. [英特尔收购，RISC-V赢了](http://mp.weixin.qq.com/s?__biz=Mzg3NjU2NDIzMQ==&amp;mid=2247483943&amp;idx=1&amp;sn=a738eb216bba56520f7e331e585daf12)
  1. [RISC-V理事会，80%的最高级别会员被中国企业申请了？](http://mp.weixin.qq.com/s?__biz=Mzg3NjU2NDIzMQ==&amp;mid=2247483928&amp;idx=1&amp;sn=06905eceaa9f86306f07cf711c57f72e)
  1. [美国“核高基”来了，格局却小了](http://mp.weixin.qq.com/s?__biz=Mzg3NjU2NDIzMQ==&amp;mid=2247483901&amp;idx=1&amp;sn=985ba8f2af3be07ec38aff7469ea133b)
  1. [处理器指令集，不是一本武功秘籍](http://mp.weixin.qq.com/s?__biz=Mzg3NjU2NDIzMQ==&amp;mid=2247483888&amp;idx=1&amp;sn=2f346ed6022523db095b9ea6415fe2ff)
  1. [EDA工具，芯片打工人爱恨交织的宿命](http://mp.weixin.qq.com/s?__biz=Mzg3NjU2NDIzMQ==&amp;mid=2247483877&amp;idx=1&amp;sn=0ebf217bf94e7ef0143f59d3fb6e977a)
  1. [矿机芯片的今天，AI芯片的明天？](http://mp.weixin.qq.com/s?__biz=Mzg3NjU2NDIzMQ==&amp;mid=2247483855&amp;idx=1&amp;sn=1b7e8aa02e9975b002fbe447c1b0aa72)
  1. [云端芯片之战-小乌云还是大风暴](http://mp.weixin.qq.com/s?__biz=Mzg3NjU2NDIzMQ==&amp;mid=2247483844&amp;idx=1&amp;sn=275daa43908651f1b99e17c7d35cb151)
  1. [芯片过热？一场芯片供应链的饱和式救援。](http://mp.weixin.qq.com/s?__biz=Mzg3NjU2NDIzMQ==&amp;mid=2247483829&amp;idx=1&amp;sn=74ce1d7282b72bf9e90fec0bb5b9dffe)
  1. [“为了这点醋，包了一顿饺子”-AI芯片的落地之道](http://mp.weixin.qq.com/s?__biz=Mzg3NjU2NDIzMQ==&amp;mid=2247483822&amp;idx=1&amp;sn=cd1139a1163c3a26b7c8ca82d7a2cdcf)
  1. [降低芯片流片失败风险的"七种武器"](http://mp.weixin.qq.com/s?__biz=Mzg3NjU2NDIzMQ==&amp;mid=2247483809&amp;idx=1&amp;sn=e2682a79a88f6a963b0378d29b0cc21f)
  1. [芯片设计公司修炼的“四层境界”，中国公司属于哪一层？](http://mp.weixin.qq.com/s?__biz=Mzg3NjU2NDIzMQ==&amp;mid=2247483797&amp;idx=1&amp;sn=abc33237bd1b85863b9d21603d7d8b5b)
  1. [中国芯片产业什么时候能够超过美国？](http://mp.weixin.qq.com/s?__biz=Mzg3NjU2NDIzMQ==&amp;mid=2247483787&amp;idx=1&amp;sn=dda01977eb0f084be37b3059b9a6bf1b)
  1. [百度成立芯片公司的底气从何而来？](http://mp.weixin.qq.com/s?__biz=Mzg3NjU2NDIzMQ==&amp;mid=2247483753&amp;idx=1&amp;sn=ce5299e7357e35c1dfa5ff47f558e50a)
  1. [为“宅男”定制的超大芯片是什么样的？](http://mp.weixin.qq.com/s?__biz=Mzg3NjU2NDIzMQ==&amp;mid=2247483732&amp;idx=1&amp;sn=2b70eff14fc760ac4544d4fff431c043)
  1. [CPU的《长安十二时辰》是如何运行的？](http://mp.weixin.qq.com/s?__biz=Mzg3NjU2NDIzMQ==&amp;mid=2247483714&amp;idx=1&amp;sn=7d0eef12829c454017430353e6ccdeb0)
  1. [RV64X：开源GPU来了，这次靠谱吗？](http://mp.weixin.qq.com/s?__biz=Mzg3NjU2NDIzMQ==&amp;mid=2247483690&amp;idx=1&amp;sn=24e7f12954771ed7ff03bd9c2822c704)
  1. [人工智能的芯片江湖（3）-独孤求败](http://mp.weixin.qq.com/s?__biz=Mzg3NjU2NDIzMQ==&amp;mid=2247483678&amp;idx=1&amp;sn=0fc16e31f812a99a3a8290ddf6904480)
  1. [人工智能的芯片江湖（2）-华山论剑](http://mp.weixin.qq.com/s?__biz=Mzg3NjU2NDIzMQ==&amp;mid=2247483671&amp;idx=1&amp;sn=759c6ff834fd634eb7dde94bf2dcaed6)
  1. [人工智能的芯片江湖（1）-NVIDIA的野望](http://mp.weixin.qq.com/s?__biz=Mzg3NjU2NDIzMQ==&amp;mid=2247483665&amp;idx=1&amp;sn=0e6e9953e2c1526c817c133371615672)
  1. [老将归来-intel 新CEO的挑战](http://mp.weixin.qq.com/s?__biz=Mzg3NjU2NDIzMQ==&amp;mid=2247483653&amp;idx=1&amp;sn=a1ea01ab242a3804ad217f46dbc566a3)

歪睿老哥 15:00 <img align="right" width="99px" src="https://foruda.gitee.com/images/1675337028510297400/a1c4dda9_5631341.png">

- [歪睿老哥](http://mp.weixin.qq.com/s?__biz=Mzg3NjU2NDIzMQ==&mid=100000209&idx=1&sn=dfbdcaf3826425b89d5c8110147c1c2c)

  添加歪睿老哥微信号一起聊一下IC行业那些事；扫描二维码 或者 添加微信号 ic-verilog

袁德俊 15:02

- [FPGA的架构和工具解析](https://mp.weixin.qq.com/s/M7skXrxNw6xORHW2gKDcpg) :pray: 

  同名知乎文章：大话FPGA 万能芯片[合十][666]