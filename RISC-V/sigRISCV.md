SIGer 开源科普选送的海报：

- 满脑子电线的奥特鸭（SIGer 少年芯2.0 芯片教育全栈解决方案）[sig-OTTO](https://gitee.com/siger01/ai-opensource/issues/I7ARN7)
- 青少年科创案例 [YKriscv](https://gitee.com/siger01/YKriscv)

![输入图片说明](https://foruda.gitee.com/images/1693249240495964261/ac8553a2_5631341.jpeg "POSTERsigRISCV-2.jpg")

> veriMAKE YADAN + SIGer GROVE = 满脑子电线的奥特鸭（sig-OTTO），首秀脑壳内部的电线连接。

![输入图片说明](https://foruda.gitee.com/images/1693249254383003010/1eb2913e_5631341.jpeg "POSTERsigRISCV-3.jpg")

> 拥抱开源社区的 SIGer RISCV 回报给 RISC-V 中国峰会的礼物就是 sigRISCV.com 青少年视角的RISCV兴趣社区！

---

2023年8月25日 DAY3:  转载自：[SIGer 云观展！](2023/SIGer%20云观展！.md)

最后一天是重要的 Poster 演讲日，任务量减半：

- sigRISCV-3 群是 Day3 的见证人 （52）

  <p><img width="19%" src="https://foruda.gitee.com/images/1693085041229556128/fe0c366a_5631341.jpeg" title="1.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1693085058048496572/3786e843_5631341.jpeg" title="2.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1693196749067802786/91771f7e_5631341.jpeg" title="5.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1693085103166855506/19f72af0_5631341.jpeg" title="6.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1693085126543343489/00167e69_5631341.jpeg" title="8.jpg"></p>

- sigRISCV.com 正式上线于 2023.8.23 07:17:06 峰会开幕的当天，这是一个意外惊喜。经过三天的推荐，一个青少年视角的 RISCV 兴趣社区已经浮现，发布会就是在 25日上午 9:30 - 9:50 “SIGer 少年芯 2.0” 的发布现场，这个事先预演的活动，可以说 sigRISCV 是在整个峰会的嘉宾的关注下诞生的。而最后一日的几张代表性的合影，则是来自多个方面的祝福和见证了。

  <p><img width="19%" src="https://foruda.gitee.com/images/1693107005184171689/729ffe63_5631341.jpeg" title="IMG_1135 - 副本.JPG"> <img width="19%" src="https://foruda.gitee.com/images/1693107025827751244/3cf4f00d_5631341.jpeg" title="IMG_1153 - 副本.JPG"> <img width="19%" src="https://foruda.gitee.com/images/1693107042297823432/1bd8ef02_5631341.jpeg" title="IMG_1157 - 副本.JPG"> <img width="19%" src="https://foruda.gitee.com/images/1693107059218705981/1fb56786_5631341.jpeg" title="IMG_9513 - 副本.JPG"> <img width="19%" src="https://foruda.gitee.com/images/1693107074158435107/26c2bda0_5631341.jpeg" title="IMG_E9505 - 副本.JPG"></p>

  - sigRISCV.com 发布仪式是在 SIGer GROVE 与 veriMAKE YADAN 合体的 “满脑子电线的奥特鸭” 的发布现场。还有两位见证人，YKriscv 交大附中的许元开同学 是作为 SIGer 科创少年代表，带着 YKriscv 从上海来北京为 SIGer 站台的。另一位 重庆大学的周喜川校长，他是 RVI AI/ML sig 的主席，我们对教育环境建设的理念，获得到了他的认可。
  - 本次峰会，SIGer主视觉是民族棋中国史的 —— 非遗小游戏 “九子贤棋”，它是社区连接的利器，来自厦门开芯会的贠利君老师，成为了家长志愿者代表，在厦门传播民族棋。
  - 志愿者是峰会最忙碌的啦，本场发布会的志愿者，在会后来到 SIGer 站台前，专门学习了九子贤棋，并合影留念
  - PLCT 的吴伟老师，在峰会结束前，来到 SIGer 站台前，与志愿者们合影，感谢他们的辛勤付出
  - LINUX 开发板一直都是 RISCV 社区的指标，赛昉回访 SIGer 站台，许下 all the best! 的愿景，唯一连接整个社区，才能实现，这是我的解读！

<p align="right"><a href="README.md" target="_blank">sigRISCV.com</a> 青少年视角的 RISCV 兴趣社区<br>SIGer 青少年开源科普<br>袁德俊 2023.8.27</p>