- [BUILD YADAN SOC with TangDynasty 5.0.5 & 4.6.6](https://gitee.com/RV4Kids/yadansoc/issues/I5ZP4T)
- [Electronics Weekly.com](https://www.electronicsweekly.com/digital-edition/) ： [China FPGA strategy takes shape](https://www.electronicsweekly.com/news/business/china-fpga-strategy-takes-shape-2017-10/)

<p><img width="706px" src="https://foruda.gitee.com/images/1668074101508342398/337792f7_5631341.jpeg" title="413YADANSOC.jpg"></p>

> 引用了 2017年 的外媒的一篇关于 TD anlogic 的报道，突出中国芯的自主可控有多重要，背景是 TD 软件的功能说明，5.0的版本号非常醒目，以FPGA显微图托底，是外媒标题：中国策略，封面主体是 YADAN SOC 的设计图（[采自 RVSC2021 的演讲视频](https://www.bilibili.com/video/BV19q4y1W7Jh)）

# [BUILD YADAN SOC with TangDynasty 5.0.5 & 4.6.6](https://gitee.com/RV4Kids/yadansoc/issues/I5ZP4T)

- [BUILD SOC #I5ZP4T 袁德俊  17 1天前](https://gitee.com/RV4Kids/yadansoc/issues/I5ZP4T)

  双击Yadan.al打开工程，（前提是安装好TangDynasty工具，本工程使用的TangDynasty版本号为win版本5.0.3 29524）[对不起您的权限不足，无法安装，请联系 anlogic.com 安路科技。](https://foruda.gitee.com/images/1667753511846006683/f2eb1da0_5631341.png)

  - [尝试 低版本重建 工程环境。](https://gitee.com/RV4Kids/yadansoc/issues/I5ZP4T#note_14190566_link) `HDL-8007 ERROR: yadan_riscv is a black box` [文件路径图](https://foruda.gitee.com/images/1667781514552694608/8425239d_5631341.png)，[源码图](https://foruda.gitee.com/images/1667781549514745176/989c5d78_5631341.png)，

  - [TangDanasty5.0 Yandan.al](https://gitee.com/RV4Kids/yadansoc/issues/I5ZP4T#note_14226056_link) 2022-11-8 12:37:32

  - [TD4.6.6 & TD5.0.5](https://gitee.com/RV4Kids/yadansoc/issues/I5ZP4T#note_14239319_link) ★★★

    - [找到一个 VERIMAKE 的一个合集](https://gitee.com/RV4Kids/yadansoc/issues/I5ZP4T#note_14240445_link) ：[安路FPGA竞赛用开发板SparkRoad-V教程合集](https://space.bilibili.com/356383684/channel/collectiondetail?sid=345844)

    - [比对两个版本的差距](https://gitee.com/RV4Kids/yadansoc/issues/I5ZP4T#note_14241283_link)，[发现：apb_gpio.sv，apb_spi_master.sv，apb_timer.sv 无法加入 4.6.6](https://gitee.com/RV4Kids/yadansoc/issues/I5ZP4T#note_14241410_link) 

    - [SystemVerilog源代码文件](https://gitee.com/RV4Kids/yadansoc/issues/I5ZP4T#note_14241802_link) https://filefans.com/ext/filetypes/developer/sv

      > 根据本网站数据统计，SV 文件扩展名相关的格式总共有 0 种。当前这种 SV 格式是一种 开发者文件 ，是由 IEEE 研发，作为一种 SystemVerilog源代码文件使用，此文件流行程度是 80 分（0-100满分）。Windows系统下，推荐使用Sigasi工作室 Mentor Graphics ModelSim, 任何文本编辑器软件程序打开、查看、运行此类文件。在苹果Mac系统下，推荐使用Sigasi工作室, 任何文本编辑器软件程序打开运行。如果是在手机上运行，请下载安卓或者IOS应用程序，通过APP打开 SV 文件。请参阅下面详细信息，了解更多有关 SV 文件扩展名信息、系统、软件、程序等内容。以及更多关于 开发者文件 的信息。 此外，我们还提供简单的故障排除信息，以帮助您打开 SV 文件。

    - [用 sv2v 把 SystemVerilog 转成 Verilog](https://gitee.com/RV4Kids/yadansoc/issues/I5ZP4T#note_14241998_link)

    - https://github.com/zachjs/sv2v/releases/download/v0.0.9/sv2v-Windows.zip [可用迅雷种子下载。](https://gitee.com/RV4Kids/yadansoc/issues/I5ZP4T#note_14242660_link)

      - SV2V: [apb_gpio.sv](https://gitee.com/RV4Kids/yadansoc/issues/I5ZP4T#note_14242806_link)，[apb_spi_master.sv](https://gitee.com/RV4Kids/yadansoc/issues/I5ZP4T#note_14243058_link)，[apb_timer.sv](https://gitee.com/RV4Kids/yadansoc/issues/I5ZP4T#note_14242910_link)

    - [BIT-1004 : Generate bits file RiscvSOC.bit.BIT-1004 : Generate bits file RiscvSOC.bit.](https://gitee.com/RV4Kids/yadansoc/issues/I5ZP4T#note_14243324_link) | [download to board](https://foruda.gitee.com/images/1667919611007661312/8891dab8_5631341.png).

  - [Arduino 验证 bLINK。](https://gitee.com/RV4Kids/yadansoc/issues/I5ZP4T#note_14245353_link)

  - [PR .v to](https://gitee.com/RV4Kids/yadansoc/issues/I5ZP4T#note_14245416_link) https://gitee.com/RV4Kids/yadansoc/

- [4、使用Arduino IDE开发 #I600W6 袁德俊  9 5小时前](https://gitee.com/RV4Kids/yadansoc/issues/I600W6)

  详细的方法参见：https://gitee.com/verimake/yadan_arduino

  - [3.4. 安装 Arduino IDE 并配置对 YADAN Board 的支持](https://gitee.com/RV4Kids/yadansoc/issues/I600W6#note_14226378_link)

    - [7.2.2. 第二种方法：离线安装](https://gitee.com/RV4Kids/yadansoc/issues/I600W6#note_14226742_link) 离线安装开发板开发包.rar

      `https://pan.baidu.com/s/1uzZ-Ic4XPclwRXgk5f-Z_g  提取码：3005`

  - [测试 ARDUINO. BLINK.](https://gitee.com/RV4Kids/yadansoc/issues/I600W6#note_14226857_link) [OK](https://gitee.com/RV4Kids/yadansoc/issues/I600W6#note_14226962_link)

    [![输入图片说明](https://foruda.gitee.com/images/1667886679254313901/b6dedbc9_5631341.png "在这里输入图片标题")](https://gitee.com/RV4Kids/yadansoc/issues/I600W6#note_14226882_link)

【[笔记](https://gitee.com/RV4Kids/yadan-board/issues/I5X5XL#note_14238491_link)】

[TangDynasty(TD) - TD软件 - 上海安路信息科技有限公司](http://m.anlogic.com/m/prod_view.aspx?TypeId=11&Id=171&Fid=t3:11:3&typefid=11)
m.anlogic.com|1200 × 1746 jpeg|Image may be subject to copyright.

![输入图片说明](https://foruda.gitee.com/images/1668047325898347148/1cf8602f_5631341.jpeg "CgAGfFxKsAuAVWx2AAUen5GVUWI070.jpg")

[China FPGA strategy takes shape](https://www.electronicsweekly.com/news/business/china-fpga-strategy-takes-shape-2017-10/)
Electronics WeeklyElectronics Weekly|2278 × 2299 jpeg|Image may be subject to copyright.

![输入图片说明](https://foruda.gitee.com/images/1668047423792556684/4d5ae8ee_5631341.jpeg "IMG_0878.jpg")

# [China FPGA strategy takes shape](https://www.electronicsweekly.com/news/business/china-fpga-strategy-takes-shape-2017-10/)

With President Trump denying China access to Lattice’s technology, the nascent China FPGA industry is looking to develop home-grown programmable logic.

Digitimes reports that Shanghai Anlogic Information Technology vp Chen Li-guang says that China will have to develop the technology by themselves while concentrating on expanding their market share by focussing on entry-level products.

Chen reckons that international FPGA vendors are shifting their focus away from entry-level and mid-range products which gives the China companies the opportunity to establish themselves in the market.


As well as Shanghai Anlogic, there are other Chinese FPGA companies pursuing this strategy – Shenzhen Pango Microsystems, Gowin Semiconductor, Xian Intelligence Silicon Tech and AGM Microelectronics.



Anlogic shipped 500,000 FPGA ICs last year, says Chen, and plans to ship 1.5 million this year and six million next year. It has started its 28nm development.

Pango is on 40nm with its Titan series FPGAs and Gowin’s chips are on 55nm.

![输入图片说明](https://foruda.gitee.com/images/1668047793466268885/38120a3d_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1668047975096215334/42aa3fbe_5631341.png "屏幕截图")

https://www.electronicsweekly.com/digital-edition/

<h1 class="page-title">Digital Editions</h1>
<h2>2022</h2>

<li class="digital-edition post-listing post">
<a href="https://static.electronicsweekly.com/wp-content/uploads/2022/10/26155907/EWE_021122_001.jpg"><img width="227" height="300" src="https://foruda.gitee.com/images/1668072895250060519/aacb3fd3_5631341.png"><h3 class="post-title">2 November</h3>
</a>

</li><!-- .post-listing -->
<li class="digital-edition post-listing post">
<a href="https://static.electronicsweekly.com/wp-content/uploads/2022/10/14114250/EWE_191022_001.jpg"><img width="227" height="300" src="https://foruda.gitee.com/images/1668073216426611550/a42874d0_5631341.png"><h3 class="post-title">19 October</h3>
</a>

</li><!-- .post-listing -->
<li class="digital-edition post-listing post">
<a href="https://static.electronicsweekly.com/wp-content/uploads/2022/09/30150216/EWE_051022_001.jpg"><img width="227" height="300" src="https://foruda.gitee.com/images/1668073259313490908/4e645d8e_5631341.png"><h3 class="post-title">5 October</h3>
</a>

</li><!-- .post-listing -->
<li class="digital-edition post-listing post">
<a href="https://static.electronicsweekly.com/wp-content/uploads/2022/09/15165644/EWE_210922_001.jpg"><img width="227" height="300" src="https://foruda.gitee.com/images/1668073329945153768/6309846e_5631341.png"><h3 class="post-title">21 September</h3>
</a>

</li><!-- .post-listing -->
<li class="digital-edition post-listing post">
<a href="https://static.electronicsweekly.com/wp-content/uploads/2022/09/01150707/EWE_070922_001.jpg"><img width="227" height="300" src="https://foruda.gitee.com/images/1668073375382043781/64ab5828_5631341.png"><h3 class="post-title">7 September</h3>
</a>

</li><!-- .post-listing -->
<li class="digital-edition post-listing post">
<a href="https://static.electronicsweekly.com/wp-content/uploads/2022/08/08160921/EWE_100822_001.jpg"><img width="227" height="300" src="https://foruda.gitee.com/images/1668073422782719388/de6d8017_5631341.png"><h3 class="post-title">10 August</h3>
</a>

</li><!-- .post-listing -->
<li class="digital-edition post-listing post">
<a href="https://static.electronicsweekly.com/wp-content/uploads/2022/07/15160026/EWE_200722_001.jpg"><img width="227" height="300" src="https://foruda.gitee.com/images/1668073470000588962/7b9ec28b_5631341.png"><h3 class="post-title">20 July</h3>
</a>

</li><!-- .post-listing -->
<li class="digital-edition post-listing post">
<a href="https://static.electronicsweekly.com/wp-content/uploads/2022/06/30115455/EWE_060722_001.jpg"><img width="227" height="300" src="https://foruda.gitee.com/images/1668073511968822957/1a388fe2_5631341.png"><h3 class="post-title">6 July</h3>
</a>

</li><!-- .post-listing -->
<li class="digital-edition post-listing post">
<a href="https://static.electronicsweekly.com/wp-content/uploads/2022/06/16112439/EWE_220622_001.jpg"><img width="227" height="300" src="https://foruda.gitee.com/images/1668073559079666664/ad68042f_5631341.png"><h3 class="post-title">22 June</h3>
</a>

</li><!-- .post-listing -->
<li class="digital-edition post-listing post">
<a href="https://static.electronicsweekly.com/wp-content/uploads/2022/06/06114200/EWE_080622_001.jpg"><img width="227" height="300" src="https://foruda.gitee.com/images/1668073602064005877/ee6b0758_5631341.png"><h3 class="post-title">8 June</h3>
</a>

</li><!-- .post-listing -->
<li class="digital-edition post-listing post">
<a href="https://static.electronicsweekly.com/wp-content/uploads/2022/05/19115132/EWE_250522_001.jpg"><img width="227" height="300" src="https://foruda.gitee.com/images/1668073652776450303/a3101f6f_5631341.png"><h3 class="post-title">25 May</h3>
</a>

</li><!-- .post-listing -->
<li class="digital-edition post-listing post">
<a href="https://static.electronicsweekly.com/wp-content/uploads/2022/05/05165818/EWE_110522_001.jpg"><img width="227" height="300" src="https://foruda.gitee.com/images/1668073688971775020/2509972b_5631341.png"><h3 class="post-title">11 May</h3>
</a>

</li><!-- .post-listing -->
<li class="digital-edition post-listing post">
<a href="https://static.electronicsweekly.com/wp-content/uploads/2022/04/25122857/EWE_270422_001.jpg"><img width="227" height="300" src="https://foruda.gitee.com/images/1668073728757114414/1d5bc35a_5631341.png"><h3 class="post-title">27 April</h3>
</a>

</li><!-- .post-listing -->
<li class="digital-edition post-listing post">
<a href="https://static.electronicsweekly.com/wp-content/uploads/2022/04/05094241/EWE_060422_001.jpg"><img width="227" height="300" src="https://foruda.gitee.com/images/1668073768305718719/897bcd25_5631341.png"><h3 class="post-title">6 April</h3>
</a>

</li><!-- .post-listing -->
<li class="digital-edition post-listing post">
<a href="https://static.electronicsweekly.com/wp-content/uploads/2022/03/22093337/EWE_230322_001.jpg"><img width="227" height="300" src="https://foruda.gitee.com/images/1668073810242169121/51910c55_5631341.png"><h3 class="post-title">23 March</h3>
</a>

</li><!-- .post-listing -->
<li class="digital-edition post-listing post">
<a href="https://static.electronicsweekly.com/wp-content/uploads/2022/03/04102019/EWE_090322_001.jpg"><img width="227" height="300" src="https://foruda.gitee.com/images/1668073850921725280/fe247dee_5631341.png"><h3 class="post-title">9 March</h3>
</a>

</li><!-- .post-listing -->
<li class="digital-edition post-listing post">
<a href="https://static.electronicsweekly.com/wp-content/uploads/2022/02/17100503/EWE_230222_001.jpg"><img width="227" height="300" src="https://foruda.gitee.com/images/1668073898452296984/5ad2fc87_5631341.png"><h3 class="post-title">23 February</h3>
</a>

</li><!-- .post-listing -->
<li class="digital-edition post-listing post">
<a href="https://static.electronicsweekly.com/wp-content/uploads/2022/02/08114636/EWE_090222_001.jpg"><img width="227" height="300" src="https://foruda.gitee.com/images/1668073945840800183/21fd0aa5_5631341.png"><h3 class="post-title">9 February</h3>
</a>

</li><!-- .post-listing -->
<li class="digital-edition post-listing post">
<a href="https://static.electronicsweekly.com/wp-content/uploads/2022/01/20151654/EWE_260122_001.jpg"><img width="227" height="300" src="https://foruda.gitee.com/images/1668073988298422838/6aa0ef26_5631341.png"><h3 class="post-title">26 January</h3>
</a>

</li><!-- .post-listing -->
