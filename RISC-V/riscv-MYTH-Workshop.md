![](https://images.gitee.com/uploads/images/2021/0322/092042_b8961178_5631341.png "屏幕截图.png")

# [基于RISC-V的MYTH](https://gitee.com/yuandj/siger/issues/I3B6OG#note_4562292_link)（三十小时内为您提供微处理器）
https://riscv.org/community/learn/risc-v-learn-online/

## 右侧边栏

### 格式
基于云的虚拟培训研讨会
### 持续时间 
-5天（+2天支持） 
### 成本
 -  $ 199（现价 $ 70 ）
### 日期：
2021年3月24-28日
天 0 小时 17 分钟 8 秒 35
报名截止日期：2021年3月22日IST
### 退款政策：
- 申请退款的最后日期是IST 2021年3月22日晚上11:59。
- 缺勤的学生研讨会期间， 不进行退款资格。
### 登记：
印度参加者：[现在报名](https://razorpay.com/payment-button/pl_GUkMPyOzDy374o/view/?utm_source=payment_button&utm_medium=button&utm_campaign=payment_button)
（由Razorpay担保）
国际参加者：[现在报名](https://razorpay.com/payment-button/pl_GUkJHTZ0RAcV3r/view/?utm_source=payment_button&utm_medium=button&utm_campaign=payment_button)
（由Razorpay担保）

有关信息，请发送电子邮件至：vsd@vlsisystemdesign.com

## 概述
关于“基于RISC-V的神话”的初学者为期5天的研讨会（在VSD-IAT平台上24小时x 5天）

当我们说“入门级”时，在研讨会结束时您将了解
- RISC-V规范
- RISC-V软件
- 如何使用TL-Verilog实施RISC-V基本规范
- 模拟您自己的RISC-V内核
-  **简而言之，您将自己编写RTL并构建RISC-V内核** 

### 研讨会日内容：
- 第一天：RISC-V ISA和GNU编译器工具链简介

  1. RISC-V基本关键字简介
  2. RISC-V软件工具链的实验
  3. 整数表示
  4. 有符号和无符号算术运算

- 第2天：ABI简介和基本验证流程

  1. 应用程序二进制接口（ABI）
  2. 使用ABI函数调用进行实验室工作
  3. 使用iverilog的基本验证流程

- 第3天：使用TL-Verilog和Makerchip的数字逻辑

  1. 使用Makerchip的TL-Verilog中的组合逻辑
  2. 顺序和流水线逻辑
  3. 有效性
  4. 等级制度

- 第4天：基本RISC-V CPU微体系结构

  1. 用于简单RISC-V CPU的微体系结构和测试平台
  2. 提取，解码和执行逻辑
  3. RISC-V控制逻辑

- 第5天：完整的流水线RISC-V CPU微体系结构/存储

  1. 流水线化CPU
  2. 加载和存储指令和内存
  3. 完成RISC-V CPU
  4. 总结和未来的机会

许多人一直在要求VSD举办有关如何进行RTL编码的研讨会–好的，您就可以了。

### 退款政策：
  1. 申请退款的最后日期是IST 2021年3月22日晚上11:59。
  2. 缺勤的学生研讨会期间， 不进行退款资格。

## 开源工具
   1. RISC-V ISA仿真器
   2. RISC-V GNU工具链
   3. 长钉
   4. TL-Verilog
   5. Markerchip IDE
### 退款政策：
  1. 申请退款的最后日期是IST 2021年3月22日晚上11:59。
  2. 缺勤的学生研讨会期间， 不进行退款资格。

## 投放方式
   1. 具有专业教练指导的虚拟教练平台
   2. 基于云的专用虚拟机执行设计实验室
   3. 智能评估技术（IAT）和项目分配
   4. 5天24小时实验室访问和按需提供讲师帮助
   5. 使用RISC-V ISA模拟器模拟基本C程序，使用Spike进行调试-全部在VSD-IAT平台上
   6. 使用TL-Verilog设计自己的RISC-V内核，并使用验证器进行验证-全部在Makerchip IDE上进行
### 退款政策：

  1. 申请退款的最后日期是IST 2021年3月22日晚上11:59。
  2. 缺勤的学生研讨会期间， 不进行退款资格。

## 讲师简介
### 讲师简介：
 **VLSI系统设计（VSD）Corp. Pvt的联合创始人Kunal Ghosh** 。Kunal Ltd.是在线开源EDA  （qflow和openroad）/开源硬件（特别是RISC-V）设计和学习领域的先驱。目前，Kunal在开源EDA /硬件及其周围拥有大约32个高质量的VLSI在线课程，正在141个国家/地区的28700多名学生使用。除了培训外，Kunal还与IIT Madras和IIT Guwahati合作开展了开源活动和设计项目。目前，Kunal及其团队正在开发高质量的开源模拟/数字IP，这将是开源硬件设计领域的第一人。在加入VSD之前，Kunal在SoC设计领域曾与Qualcomm和Cadence合作。Kunal在孟买IIT的VLSI和纳米电子学领域取得了硕士学位，专门研究了100nm以下电子束光刻技术。

 **红木EDA的 创始人史蒂夫·胡佛（Steve Hoover）** 通过多种技术（包括支持RISC-V的WARP-V CPU内核生成器）培育了开放源码的硅生态系统。他的主要重点是由交易级Verilog（TL-Verilog）支持的设计方法论和工具，该工具可在makerchip.com上所有人获取。他还是第一个用于云FPGA的CLaaS开源框架的首席开发人员。Steve拥有Rensselaer Polytechnic Institute的电气工程学学士学位，并以优异的成绩获得了伊利诺伊大学的计算机科学硕士学位。他为高性能服务器CPU设计了许多组件。和DEC，Compaq和Intel的网络体系结构。

### TA资料：
Shivani Shah目前是班加罗尔国际信息技术学院（IIITB）的研究学生。在第二次迭代中，她参加了VSD和Redwood EDA的RISC-V MYTH研讨会。从第三个讲习班开始，她是该讲习班的助教。

## 常问问题
### 我可以按时区参加我的时间表吗？

是的，在整个研讨会期间，您还将在各个时区得到导师的全天候24x7全天候支持。 我们使用Slack提供实时聊天支持，并根据需要进行日常同步和一对一通话。

### 经验丰富的系统设计师可以加入以刷新概念吗？

我们欢迎来自职业生涯各个阶段的有兴趣的参与者。即使您过去曾经学习过逻辑设计和CPU微体系结构，本课程也提供了现代的观点。TL-Verilog是一个新兴标准，对行业和学术界都非常有用。参与革新您的设计/教学/学习过程！

### 我是数字逻辑的新手。我可以完成课程吗？

本课程在现代设计方法的背景下教授数字逻辑的基础知识。因此，新手将和经验丰富的设计师一样学到一些东西。我们收到了从12岁到行业资深人士的学习者的积极反馈，尽管我们建议该课程适合大学生或以上的技术路线人士。

### 讲习班结束后可以访问内容吗？

研讨会中使用的主要工具是Makerchip.com在线IDE，该工具是公开的，并且始终对开放源代码设计的开发开放。研讨会结束后，您还将获得终身访问幻灯片和实验室文件的权限。

### 我需要安装任何软件或工具来进行实验吗？

不会。实验室将在VSD-IAT云平台和Makerchip.com在线IDE上完成。 您将在浏览器中访问Linux终端，该终端已安装了所有必需的工具。研讨会结束后，我们将提供脚本和模板以在您的个人系统上安装或使用这些工具。

### TL-Verilog与Verilog有何不同？

TL-Verilog（事务处理级Verilog）是一种新兴的标准，支持“定时抽象”数字设计，否则，这门课程将是不可能的。除了强大的建模结构外，它还消除了Verilog的遗留复杂性，例如规则和连线，生成块，阻塞与非阻塞等。它提供了易于理解的干净语义，无论您是否已经知道Verilog。通过生成可综合的（系统）Verilog，它与现有的商业和开源EDA工具集成在一起。在 https://www.redwoodeda.com/tl-verilog 上了解更多信息

### 退款政策：
  1. 申请退款的最后日期是IST 2021年3月22日晚上11:59。
  2. 缺勤的学生研讨会期间， 不进行退款资格。

## Git回购

### GitHub是VLSI行业的新简历
GitHub确实是VLSI行业的新RESUME。的确，如果您正在招聘人员，并希望为公司中的某个职位选拔新的候选人，请询问GitHub项目链接。应聘者在履历表上撰写的项目以及GitHub上可用的项目将立即为您提供一个关于他/她的毅力，奉献精神，诚意，生产力和他/她可以投入项目的辛勤工作的想法。

请看一下我们以前的工作坊Github Repos

 **RISC V MYTH研讨会Github回购** 

|姓名|Github链接|
|---|---------|
| 阿比南德（Abhinand Amarnath） |  https://github.com/abhierao/RISCV_CORE_4_Stage |
| 阿米特·罗伊（Amit Roy） | https://github.com/AMITROY71/risc-v-myth-workshop-august-AMITROY71 |
| 阿努拉格·沙玛（Anurag Sharma） | https://github.com/designerguy13-photonics/risc-v-myth-workshop-august-designerguy13-photonics |
| 阿舒托什·萨胡（Ashutosh Sahoo） | https://github.com/RISCV-MYTH-WORKSHOP/RISC-V-CPU-Core-using-TL-Verilog |
| 哈里纳拉扬·拉杰戈帕尔（Harinarayan Rajgopal） | https://github.com/RISCV-MYTH-WORKSHOP/risc-v-myth-workshop-august-harinarayan18 |
| Manjunathveeraiah Kalmath | https://github.com/dipta30/VSD-PD-workshop |
| 鲁思维·库拉姆（Ruthvik Ku​​ram） | https://github.com/rkuram/Beginner-Physical-design |
| 西瓦尼·沙（Shivani Shah） | https://github.com/shivanishah269/risc-v-core |
| 维博尔·辛格（Vibhor Singh） | https://github.com/vibhor68/risc-v-myth-workshop-august-vibhor68 |
| 库比兰·卡拉卡兰（Kubiran Karakaran） | https://github.com/kuby1412/RISC-V-MYTH-Workshop |
| 穆库·贾瓦德卡（Mukul Javadekar） | https://github.com/RISCV-MYTH-WORKSHOP/risc-v-myth-workshop-august-mukuljava |
| 约瑟夫·阿隆森 | https://github.com/aronsonj52/riscv_myth_workshop |
| Akil M. | https://github.com/akilm/MYTH-RV32I-core-akilm |
| 哈里斯瓦尔·雷迪（Harishwar Reddy） | https://github.com/RISCV-MYTH-WORKSHOP/RISC-V-Core |
| 拉兹万·伊内斯库（Razvan Ionescu） | https://github.com/RISCV-MYTH-WORKSHOP/riscv_myth_workshop_dec20-razvanionescu-77 |
| 卡尔提克（Karthik K） | https://github.com/RISCV-MYTH-WORKSHOP/riscv_myth_workshop_dec20-karthikvnit |
| 肖恩·塔韦尔 | https://github.com/ShonTaware/RISC-V_Core_4_Stage |