- [FPGA 手写数字识别CNN硬件加速](https://space.bilibili.com/397002941/channel/collectiondetail?sid=366852)
  - 专访 @雪天鱼：健身，看书。小说，动漫，学习自己感兴趣的知识的好少年！:+1

- [Imagination：大学项目](https://www.bilibili.com/video/BV17q4y1W79G)
  - (1) 移动图形概论》v2.2
  - (2) RVfpga：深入理解计算机体系结构
  - (3) 快速指南：RISC-V 许可和软硬件
  - (4) Fun with Beagle – Exploring the GPU
  - (5) 边缘人工智能 – 原理与实践

<p><img width="706px" src="https://foruda.gitee.com/images/1660116715577888459/227rvfpga副本.jpeg"></p>

各位同学本期专题是一期延展学习的笔记，为大家介绍是如何找到宝藏男孩 @雪天鱼 的，文末附上对他的专访。现在我还不能确定这样的学习方法有没有什么现有的理论支持，或者直观的名字，兴趣驱动或者什么...。总之本期 RVSC 2021 的考古收回丰厚。上笔记。

# Imagination：大学项目

在重温 RVSC 2021 的视频时，RVfpga 的演讲嘉宾和我同台，我先讲完的，台下从容为她录了全程。第一时间发了朋友圈视频号，会后还访问了专场培训，获得了尚未发布的中文资料。演讲视频的注释 “资料获取” 和培训现场特地的叮嘱尚未发布，促成了本期 RVfpga 的考古。一年之后的培训效果如何呢？

<p><img width="555px" src="https://foruda.gitee.com/images/1660007435294631459/屏幕截图.png"></p>

### POSTER - [RVfpga 第一个基于RISC-V的计算机体系的课程](https://www.bilibili.com/video/BV17q4y1W79G) - 许可 - 第一届 RISC-V 中国峰会

https://www.bilibili.com/video/BV17q4y1W79G
- Slides资料获取：7月20日之后通过 CNRV 的 GitHub 账号 微信公众号(ID: risc-v) 后台输入【slides】获取。

大家好，我是imagination公司市场部的许可，那我今天，给大家介绍的是一个RVFPG的课程，那这个课程，是全球首个基于respell的完整计算机体系结构的这个官方课程，它是由imagination公司大学计划部门开发，主导开发，联合全球20多家企业和大学共同合作的一个鼓励的成果。那这个课程，我们也有幸请到了戴维的泰森教授，做教学顾问，这个课程的目的，是想请通过这个课程，能够帮助。老师和学生能够更深入的了解和运用risk five这个直接架构，能够了解商用的risk five的内核和系统。那这个课程里，也会介绍关于risk five生态系统中目前比较活跃的这个工具量和这个操作系统等很多知识。

那information，现在和瑞斯还有基金会，也有很深入的合作，那这个课程现在也已经被列入这个瑞斯还有基金会的一个标准的课程，那现在，就是很高兴的是这个课程的中文版，现在是上个月，我们公司已经正式的release了，那感兴趣的师生，和开发者朋友可以到imagination的官网，我们的大学计划的页面，能够可以免费的去下载，那我们在这个峰会期间，明天也会有一个线下的workshop。那也请这个感兴趣的朋友可以到这个明天下午两点钟，在隔壁的一楼多功能厅，可以去现场聆听，也可以感受上帝的操作，那我们在隔壁的这个展台上，也是有展位，也欢迎各位去展台去留言，谢谢。

<p><img width="19%" src="https://foruda.gitee.com/images/1660000568093314470/1496578748.jpeg"><img width="19%" src="https://foruda.gitee.com/images/1660000583567974699/1313914033.jpeg"><img width="19%" src="https://foruda.gitee.com/images/1660000595841010304/1612586631.jpeg"><img width="19%" src="https://foruda.gitee.com/images/1660000606924757457/408893336.jpeg"><img width="19%" src="https://foruda.gitee.com/images/1660000618881654649/853367114.jpeg"></p>

申请下载：  
https://university.imgtec.com/rvfpga

课程支持：  
RVfpga 论坛：  
https://university.imgtec.com/forums/rvfpga

下载地址：  
https://university.imgtec.com/teaching-download/#RVGuide

申请下载：  
https://university.imgtec.com/teaching-download/#FWB

教学视频：  
https://university.imgtec.com/fun-with-beagle-video/

https://university.imgtec.com/teaching-download/#RVGuide  
下载 Imagination 大学项目手册

### 官网已经发布

Home » Downloads » Imagination University Programme Brochure CN 大学项目手册中文版本  
https://university.imgtec.com/resources/download/cn-brochure/

Imagination University Programme Brochure CN 大学项目手册中文版本 Version 1.8  
https://cdn2.imgtec.com/iup-downloads/IUP_Brochure_v1.8_Simplified_Chinese.pdf

### 更多官网资料（简体中文）

Teaching Materials:
- Chinese  RVfpga (简/繁)

  - [研讨会材料包](https://university.imgtec.com/resources/download/rvfpga-%e7%a0%94%e8%ae%a8%e4%bc%9a%e6%9d%90%e6%96%99%e5%8c%85/)

  - [观看研讨会视频](https://www.bilibili.com/video/BV1JB4y1N7QJ?vd_source=6c41d3cc62a95ef9191f586d4ceab39a) B站

    <p><img width="555px" src="https://foruda.gitee.com/images/1660009395107647809/屏幕截图.png"></p>

    - P1 [Imagination大学项目简介](https://www.bilibili.com/video/BV1JB4y1N7QJ?p=1) 15:51
    - P2 [RVfpga 课程内容概览与学习成果展示](https://www.bilibili.com/video/BV1JB4y1N7QJ?p=2) 48:32
    - P3 [实验演示与动手操作1](https://www.bilibili.com/video/BV1JB4y1N7QJ?p=3) 34:17
    - P4 [实验演示与动手操作2](https://www.bilibili.com/video/BV1JB4y1N7QJ?p=4) 24:22

  - [教材下载(简体中文)](https://university.imgtec.com/resources/download/rvfpga-understanding-computer-architecture-%e7%90%86%e8%a7%a3%e8%ae%a1%e7%ae%97%e6%9c%ba%e4%bd%93%e7%b3%bb%e7%bb%93%e6%9e%84-%e7%ae%80%e4%bd%93%e4%b8%ad%e6%96%87%e7%89%88-simplified-chinese/)

- 移动图形概论(简/繁)

  - [Introduction To Mobile Graphics V2.2 – Simplified CN](https://university.imgtec.com/resources/download/introduction-to-mobile-graphics-2020-edition-cn/) 《移动图形概论2020版》

  - [学堂: Introduction to Mobile Graphics](https://www.xuetangx.com/course/PKU08091004417/10331244)

    ![输入图片说明](https://foruda.gitee.com/images/1660009005099830786/屏幕截图.png "屏幕截图.png")

RVfpga SoC Design:
- RVfpgaSoC（简/繁） 
  RVfpga – [Introduction to RVfpgaSoC – simplified Chinese](https://university.imgtec.com/resources/download/introduction-to-mobile-graphics-2020-edition-workshop-package/) – V1.0

Project: Guide to RISC-V:
- [Guide to RISC-V](https://university.imgtec.com/resources/download/guide-to-risc-v-cn-ver/) (简/繁)

Project: Fun with Beagle: Exploring the GPU:
- 《[与 BeagleBone GPU 愉快玩耍](https://university.imgtec.com/resources/download/fun-with-beagle-exploring-the-gpu-%e3%80%8a%e4%b8%8ebeaglebone-gpu-%e6%84%89%e5%bf%ab%e7%8e%a9%e8%80%8d%e3%80%8b%e7%ae%80%e4%bd%93%e4%b8%ad%e6%96%87%e7%89%88/)》(简/繁)

# 社区学习笔记 

RVfpga是一个关于计算机体系结构的新课程。它以RISC-V工业级别核心为基础，包含三个学期的课程材料，内容涵盖基础和高级计算机体系结构与SoC设计。这是一个由20多家公司和大学通力合作的成果，由David Patterson教授指导，以最高标准的质量帮助师生拥抱RISC-V这一技术。

### P2 [RVfpga 课程内容概览与学习成果展示](https://www.bilibili.com/video/BV1JB4y1N7QJ?p=2) 48:32

- 席宇浩
  - 浙江大学大四，研究生
  - 处理器的设计以及特定领域加速器的设计

- Nexys A7-100T FPGA
  - ￥2000+

- SweRV EH1 内核 （工业验证过的，图形加速内核）
  - Western Digital 的开源内核
  - 32位（RV32ICM）超标量内核，具有双发射9级流水线
  - 独立的指令和数据存储器（ICCM和DCCM）,与内核紧密耦合
  - 4路组组相连缓存，具有奇偶校验或ECC保护
  - 可编程中继控制器
  - 符合RISC-V调试规范的内核调试单元
  - 系统总线：AXI4 或 AHB-Lite 

- RVfpga实验
  - 0：RVfpga 快速入门指南
  - 1：创建 Vivado 项目
  - 2：C 语言编程
  - 3：RISC-V 汇编语言
  - 4：函数调用
  - 5：图像处理：C 语言和汇编语言
  - 6：i/o 简介
  - 7：7 段显示屏
  - 8：定时器
  - 9：中继驱动 i/o
  - 10：串行总线

- The Complete Course in Understanding Computer ArchitectureBuilding a RISC-V CPU Core (LFD111x)

### RVfpga：深入理解RISCV体系结构（1）Vivado项目创建
https://www.bilibili.com/video/BV1MW4y1k75c

雪天鱼

498 关注 1756 粉丝 1927 获赞

复旦微电子学院研究生在读，手把手教你做CNN硬件加速器，

Imagination官方推出的《Rvfpga:深入理解计算机结构》是全球首个基于RISC-V的计算机体系结构完整课程。  
同过动手实践部署实现的方法来了学习FPGA、RISC-V 处理器和RISC-V 生态系统，所有资料都是开源的。

- 课程链接：RISC-V教学网络研讨会 https://sourl.cn/NkdBJQ  
- Imagination 大学项目（IUP）官方链接： http://university.imgtec.com

单独开了个 RVfpga 学习交流群 (qq)：541434600

之后所有下载好的资料都会发这个群聊，欢迎大家前来交流讨论~

# 基于PYNQ-Z2的手写数字识别卷积加速器设计

[TA的合集和视频列表基于「PYNQ-Z2」实现手写数字识别卷积神经网络硬件加速器设计](https://space.bilibili.com/397002941/channel/collectiondetail?sid=366852)

- 【基于PYNQ-Z2的手写数字识别卷积加速器设计】

  | # | title | hot | date | len | 
  |---|---|---|---|---| 
  | 1. | [滑窗模块设计与仿真](https://www.bilibili.com/video/BV1gi4y1U7ys) | 5734 | 4-13 | 13:34 |
  | 2. | [整体设计思路](https://www.bilibili.com/video/BV1DY4y1a7qY) | 2225 | 4-18 | 11:58 |
  | 3. | [卷积模块讲解](https://www.bilibili.com/video/BV1gA4y1D7ty) | 1430 | 4-26 | 15:46 |
  | 4. | [卷积模块设计代码讲解](https://www.bilibili.com/video/BV1L3411K7dr) | 1200 | 4-27 | 06:19 |
  | 5. | [池化模块设计思路](https://www.bilibili.com/video/BV1SY4y1t7WG) | 715 | 5-1 | 10:34 |
  | 6. | [池化模块设计代码与仿真讲解](https://www.bilibili.com/video/BV1yF411T7n3) | 1356 | 5-1 | 09:19 |
  | 7. | [全连接模块设计与代码讲解](https://www.bilibili.com/video/BV13Y4y1C7XP) | 1189 | 5-5 | 06:30 |
  | 8. | [硬件加速器顶层模块设计](https://www.bilibili.com/video/BV1M3411g7yS) | 2273 | 6-8 | 06:30 |

- 项目开源地址：https://gitee.com/jccao/cnn_accelerator
  - 已经 403：
    
- 最新开源地址：https://github.com/JiachengCao/cnn_accelerator
  - FORK 了一个新的。https://gitee.com/RV4Kids/cnn_accelerator

![输入图片说明](https://foruda.gitee.com/images/1660014166068640428/屏幕截图.png "屏幕截图.png")

> https://jccao-tc1.oss-cn-shanghai.aliyuncs.com/xsj/20220520154137.png

### 专访 @雪天鱼：健身，看书。小说，动漫，学习自己感兴趣的知识的好少年！

1. RFfpga 是你们复旦上的课程了吗？能有多少同学一起？相当羡慕。

   > RVFPGA是 Imagination Tech 公司的 University Programme，不是学校课程。是我偶然接触后感觉这个课程比较系统全面，就想借此学习一下RSICV，为以后的科研打打基础

2. 设备保有量是怎样的？保证人手一块吗？可以带回宿舍吗？学习末要上交还是留下啦。

   > 非学校课程，设备自行准备

3. 看RVfpga 的板子没有 PYNQ-Z2 ，和你的CNN课程不是一样的板子。你选PYNQ-Z2是自己采购的吗？

   > PYNQ-Z2是因为打竞赛经常使用，所以选用的pynq-z2，后续能否满足使用暂时未知

4. 我申请入群了。1群满，500人的，2群90多，也就有600多粉丝啦，都是同班同学吗？还是网友慕名而来？其中真正开始学习的 RVFPGA  CNN 的比例有多少？估计下人数。我非常期待能有更多同学一起学。我也有动力

   > 比例，无法统计，目前2个大群，一个2群，一起1k1百左右，暂时无扩建群的想法

5. FPGA 的工作环境 VIVADO 用的是哪个版本？这个小白问题确实困扰了很久。RVFPGA 的开发环境和PYNQ-Z2 CNN 的环境是一样的吗？

   > 开发环境应该没有太大要求，我自己使用的vivado2019.2，2021.2两种版本

6. 我看官网RVFPGA 的B站课程，是去年峰会期间的录像，其中有一个细节，BITSTREAM 的驱动是不同的，VIVADO 的驱动是自己的格式，但是还有 PLATFORMIO 的驱动可是用，这环境配置着实小白困惑了。你的项目环境是用的哪个IO驱动。

   > BITSTEAM用VIVADO编译生成和下载即可

7. RVFPGA 是  Imagination 的大学项目，官方课程里有 GPU 课程的推荐，你们学校是同时有一起学习吗？riscv 的板卡中很少集成GPU的，rvfpga 中的实验课，并没有太多 RISCV 的内容，使用的是成熟的软核。它更多是 FPGA 实验课。对RISCV 的学习，你们有相关的课程吗？是计算机体系架构吗？会要写RISC-V 流水线吗？

   > 课程是自己无意了解到且由于很感兴趣所以发到了B站，和学校课程无关。目前学校课程有计算机体系结构方面的，自己选课

8. CNN 的手写应用，是我们编辑部的一个实验项目，科普用的，因为它很典型，同时我们也有FPGA实验板卡就是PYNQ-Z2，所以特别有好感。我们选型的 cnn 模型只有4层，因为我们还是初学，不是很理解这中间的差别，你的CNN是5层，这是怎么设定的？有什么依据吗？

   > LetNet-5就是5层网络，所以实现5层，依据可参考LetNet-5论文

9. 我们构想过一个应用，手写识别CNN+RISCV MCU 设计一个手写数字答题写字板。SIGER本期专题是一个系列，标题为，在全栈学习的道路上。就这么一个假象应用，以你现在的储备，你觉得能把握拿下吗？如果能，有多大把握，多久？用PYNQ-Z2实现DEMO。如果还欠缺些，就全栈学习，你的理解是怎么样的？还需要哪些补充和学习的方向。当然全栈的名字我们是学习稚晖君得来的。他现在都溢出了。

   > 有把握，Xilinx目前暑期学校就有相关的实验，识别MNIST手写数据集，已经开源了

   > 全栈太广泛，FPGA分为应用和设计两大方面，目前市场上的FPGA学习一般都是指的应用，也就是接口协议，算法设计这些，而FPGA设计，则是设计FPGA芯片，目前国内研究的高校不多，研究的企业也不多，我目前实习的就是一家研究FPGA芯片设计的高新企业。

10. SIGER 理论上不能算媒体，就是我们同学给自己一个打气的平台，吧学习成果分享下。鼓励自己也鼓励其他同学，自己那么白都敢分享，不怕。SIGER 有一个惯例，就是介绍自己从《我的兴趣爱好》开篇。你如果愿意，能分享一下自己的兴趣爱好吗？平时除了肝代码，都做些什么？

    > 兴趣爱好：健身，看书。小说，动漫，学习自己感兴趣的知识

很高兴你能接受我们的专访。回答的也特别好，非常符合SIGER的风格，这就叫有缘千里。如果能分享下学校开源的FPGA项目，就太完美了。那家FPGA我有所听闻，现在有了内线，可以专门做一期行业深度报道。学习无止境。万事开头难，有你的帮助，这次峰会专题一定顺利。再次感谢！


---

图片来源网络：

[PYNQ-Z2 Python FPGA Board Adds Raspberry Pi Header, 24-Bit Audio Codec|Watch now](https://www.cnx-software.com/2019/03/25/pynq-z2-python-fpga-board-raspberry-pi-header-audio-codec/)  
CNX SoftwareCNX Software|3075 × 2051 jpeg|Image may be subject to copyright.

<p><img width="369px" src="https://foruda.gitee.com/images/1660111584383120394/pynq-z2-large.jpeg" title="PYNQ-Z2-Large.jpg"></p>

[雕刻垫板_切割板 a1白芯自愈 双面雕刻垫板 手工裁切护 - 阿里巴巴](https://detail.1688.com/offer/528459320092.html)  
detail.1688.comdetail.1688.com|1920 × 1920 jpeg|图像可能受版权保护。

<p><img width="369px" src="https://foruda.gitee.com/images/1660112334230422230/3621934633_2056674086.jpeg" title="3621934633_2056674086.jpg"></p>

[Nexys A7 Artix-7 FPGA Trainer Board - Digilent](https://digilent.com/shop/nexys-a7-fpga-trainer-board-recommended-for-ece-curriculum/?ctk=72a996d0-b1c2-4910-bc3b-49ec7b3c1e7d)  
digilent.com|1280 × 873 jpeg|Image may be subject to copyright.

<p><img width="369px" src="https://foruda.gitee.com/images/1660114621717019729/nexysa7-use-600__73766.jpeg" title="NexysA7-use-600__73766.jpg"></p>