- 专访：[北京化工大学 电子科学与技术 柯子翼、雷圆圆、沈继明、黄传勇](#北京化工大学电子科学与技术柯子翼雷圆圆沈继明黄传勇)

<p><img width="706px" src="https://foruda.gitee.com/images/1669091842930716024/362f3f3a_5631341.jpeg" title="461pX4.jpg"></p>

- 开源EDA在PCB设计的应用我觉得工业推广和自学的难度阻力都是非常小的，非常容易上手。所以针对板级的EDA设计其实很多技术爱好者、学者和工程师都会把这个当作自己的爱好，在闲暇之余开发一些有趣的产品。

- 开源EDA在“IC设计”中的使用，这个近几年在很多IC公司都会使用他们来进行芯片设计并进行流片，但是抛开科研的目的外花费几年时间专注研究来熟练掌握这个工具这个时间精力门槛还是很高的。

RISCV 进2年的学习，所有的焦点都聚焦到了  **EDA**  上，本期专访是我遇到的最详尽准备的同学，而且还是带队参加，这份期待是值得的，尽管跨年。整个访谈的每一个问题都得到了近乎完满的回答，能够为即将踏入大学生活的学弟学妹们一个展示，RISCV 芯片设计的一个分支开源 EDA 的应用，摘录了相关观点，还是相当中肯的。以下访谈由：北京化工大学  **电子科学与技术** ，[柯子翼（大四）、雷圆圆、沈继明、黄传勇](#北京化工大学电子科学与技术柯子翼雷圆圆沈继明黄传勇)，四位同学集体接受采访。

> 封面创作，仅来自翼同学分享的一个线索，不代表采访全文。文末【笔记】多与之相关，可以作为同学们的学习线索。

————— 2022-11-18 —————

袁德俊 16:22

我是群聊“开源EDA技术讨论”的袁德俊，SIGER编辑部请求采访？开源工具链。

袁德俊 16:25

[So we are 100% Cadence based. We use Cadence PCB tools to design the electronics products, and we use the Cadence chip design tools to design our silicon. We use Xcelium to do verification, we do Synthesis through Genus, we do place and route with Innovus, STA signoff with Tempus, and Quantus for extraction.](https://play.vidyard.com/9H13oEJXzBzgeqUjGXjauE?disable_popouts=1&v=4.3.10&type=inline)

袁德俊 16:25

> 翼：商业的EDA他的这个物理库和逻辑库涉及到的设计库和参考库，都是经过大量的厂家经过大量流片后总结反馈验证。很多时候开源的EDA他就是一个理论上有这样一个东西或者没有大量的反馈支撑。这样子设计的时候流片的成功率会偏低

像买的一些好点的EDA，他会爆出很多warning。这些警告不能说他是理论上错误的，但是改之后你就会发现原来还真的对性能有提升[捂脸]

袁德俊 16:25

感谢回答[合十]

翼 16:26

别客气[破涕为笑]

袁德俊 16:26

SIGer 青少年开源科普 https://gitee.com/flame-ai/siger 我们的刊例！

翼 16:27

这个刊物挺好的[呲牙]

袁德俊 16:29

https://play.vidyard.com/9H13oEJXzBzgeqUjGXjauE?disable_popouts=1&v=4.3.10&type=inline

袁德俊 16:29

这个是我的采访线索。树莓派的芯片设计案例。

翼 16:29

我们中国这块确实起步晚一些

翼 16:30

但是现在越来越重视了

# [北京化工大学电子科学与技术：柯子翼、雷圆圆、沈继明、黄传勇](https://app.yinxiang.com/fx/4326514c-0c2d-4ff8-b636-7e4990033d97)

1. Cadence 是一个大厂，有一系列工具链，以下以树莓派为例，其使用的各个环节和工具。你对这些工具和环节熟悉吗？在平时的工作学习中用到的eda工具都有那些？下面的这些环节都有涉及吗？对等的工具经验是怎样的？是什么项目中的经验？（以下译文不够准确，请纠正）

   - So we are 100% Cadence based. 所以我们是100%基于Cadence。
   - We use Cadence PCB tools to design the electronics products, 我们使用Cadence PCB工具来设计电子产品，
   - and we use the Cadence chip design tools to design our silicon. 我们使用Cadence芯片设计工具来设计我们的硅。
   - We use Xcelium to do verification, 我们使用Xcelium进行验证，
   - we do Synthesis through Genus, 通过Genus进行Synthesis，
   - we do place and route with Innovus, 使用Innovus进行place和route，
   - STA signoff with Tempus, 使用Tempus进行STA sign，
   - and Quantus for extraction.使用quantum进行提取。

2. 我只是之前简单设计过一次CPU，龙芯中科和MIPS，是什么项目经验？能介绍下吗？

   我们学校开设过“大规模集成电路设计基础”的课程，在这个课程中我们使用radhat集成的EDA开发和设计。我们的课程实验任务是数字IC设计，其中涉及到VCS、DC和ICC三部分。VCS是EDA工具厂商synopsys旗下的工具，用于在linux系统下进行仿真查看波形，类似于windows下的modelsim以及questasim等工具，以及quartus、vivado仿真的操作。VCS是编译型verilog仿真器，VCS先将verilog/systemverilog文件转化为C文件，在linux下编译链生成可执行文件，在Linux下查看仿真结果。同时还会学习DC实践，这个过程是一个编写脚本、获得时序报告等综合的过程：

   - Synthesis（综合） = Translation（转换） + Gate Mapping（映射） + Logic Optimization（逻辑优化）
   - Translation：把verilog或VHDL通过一个通用的技术库（GETCH）转化为一个通用的网表
   - Gate Mapping：将通过GETCH得到网表替换为指定工艺厂商的网表
   - Logic Optimization：DC工具优化上一步得到的网表，以满足时许约束、DRC约束
   - IC Compiler简称为ICC，是常用的后端设计EDA工具，是Synopsys的新一代后端设计工具。ICC具有强大的设计布图规划能力不同阶段的设计收敛能力以及从RTL代码到GDSII版图输出的无缝流程，并支持TCL命令方式和GUI图形化界面方式两种人机交互方式。我们使用这个工具完成最后的VFP虚拟展开布局、PNS电源网络综合、布局布线、时钟树综合等最后的开发流程。

3. 就其设计流程，使用到的开源EDA工具有那些，都是那几个环节和步骤以及工具？

   一个完整的EDA工程设计流程通常涉及系统建模、逻辑综合、故障测试、功能仿真、时序分析、形式验证等内容。而对于设计工程师而言，系统建模中的器件模型由生产厂商给出，工程师只需要完成系统设计、逻辑综合、布局布线、仿真验证和下载测试几个步骤。在设计输入时，源程序的编辑和编译要变成工程师熟悉的VHDL格式，常见的输入方式有三种，原理图输入、文本输入和状态机输入。EDA工具主要有以下几种：HFSS,CST,IE3d,XFDTD,AWR和ADS等。其中主要基于时域仿真方法的工具有CST和XFDTD，其可以通过一次时域仿真可以获得多频点的响应，适用于求解宽带问题，但相对单次仿真而言较为耗时；其他软件主要基于频域仿真方法，单次仿真获得单频点的响应，适用于求解窄带问题，当需要获取宽带响应时，往往需重新设置剖分网格。

   HFSS,CST和XFDTD这类基于体元胞算法的EDA软件在使用时均需要设置边界条件，擅长对三维场问题进行直接求解，适用于金属波导、介质谐振器和接头等立体实体；而基于矩量法的IE3d和ADS Momentum在使用时无需考虑边界条件，建模简单，更适合对2.5维的层状结构进行仿真，在多层电路设计中具有更高的效率。此外，利用CST和XFDTD之类的时域求解器，还能够直接获得求解区域电磁场的完整时域响应，在时域波或脉冲信号的可视化上具有独到的优势，因此常用于设计超宽带器件，如UWB天线和低失真结构等。

   **总的来说，设计流程主要有以下17个步骤：** 

   1. **硬件设计定义说明（Hardware Design Specification)** 硬件设计定义说明描述芯片总体架构、规格参数、模块划分、使用的总线，以及各模块的详细定义等。

   2. **模块设计及IP复用（Module Design&IP Reuse)**  模块设计及IP复用是根据硬件设计所划分出的功能模块，确定需要重新设计的部分及可复用的IP核。IP核可自主研发或者购买其他公司的IP.目前，设计的输入是采用硬件描述语言(HDL),如Verilog或VHDL,所以，数字模块的设计通常称为RTL代码编写。

   3. **顶层模块集成（Top Lovel Intogration)**  顶层模块集成将各个不同的功能模块，包括新设计的与复用的整合在一起，形成一个完整的设计，通常采用硬件描述语言对电路进行描述，其中需要考虑系统时钟／复位、VO环等问题。

   4. **前仿真（Pre-layout Simulation)**  前仿真也叫RTL级仿真：功能仿真。通过HDL仿真器验证电路逻到功能是否有效，即HDL描述是否符合设计所定义的功能期望，在前仿真时，通常与具体的电路版图实现无关，没有时序信息。

   5. **逻辑综合（Logic Synthesis)**  逻辑综合是指使用 EDA 工具把由硬件描述语言设计的电路自动转换成特定工艺下的网表（Netlist),即从RTL级的HDL描述通过编译产生符合约束条件的门级网表，网表是一种描述逻辑单元和它们之间互连的数据文件。约束条件包括时序、面积和功耗的约束。其中，时序是最复杂和最关键的约束，决定了整个芯片的性能，在综合过程中，EDA工具会根据约束条件对电路进行优化。

   6. **版图布局规划（Floorplan)**  版图布局规划完成的任务是确定设计中各个模块在版图上的位置，主要包括：

      - IO规划，确定I/O的位置，定义电源和接地口的位置
      - 模块放置，定义各种物理的组、区域或模块，对这些大的宏单元进行放置：
      - 供电设计，设计整个版图的供电网络，基于电压降（IR Drop)和电迁移进行拓扑优化。

      版图布局规划的挑战是在保证布线能够走通且性能允许的前提下，如何最大限度地减少芯片面积，是物理设计过程中需要设计者付出最大努力的地方之一。

   7. **功耗分析（Power Analysis)**  在设计中的许多步骤都需要对芯片功耗进行分析，从而决定是否需要对设计进行改进。在版图布局规划后，需要对电源网络进行功耗分析（PNA,Power Network Analysis),确定电源引脚的位置和电源线宽度。在完成布局布线后，需要对整个版图的布局进行动态功耗分析和静态功耗分析。除了对版图进行功耗分析以外，还应通过仿真工具快速计算动态功耗，找出主要的功耗模块或单元。这也是功耗分析的重要一步。

   8. **单元布局和优化（Placement&Optimization)**  单元布局和优化主要定义每个标准单元（Cell)的摆放位置并根据摆放的位置进行优化。现在，EDA工具广泛支持物理综合，即将布局和优化与逻辑综合统一起来，引入真实的连线信息，减少了时序收敛所需要的迭代次数。

   9. **静态时序分析（STA,Static Timing Analysis)**  静态时序分析技术是一种穷尽分析方法，它通过对提取的申路中所有路径上的延迟信息的分析，计算出信号在时序路径上的延迟，找出违费时序的束的机误。如建立时间(Setup Time)和保持时间（Hold Time) 是否满足要求。静态时序分析的方法不依赖于激励，而且可以穷尽所有路径，运行速度快，占用内存少。它完全克服了动态时序验证的缺陷，是SoC 设计中重要的一个环节。在后端设计的很多步骤完成后都要进行静态时序分析，如在逻辑综合完成之后、在布局优化之后、在布线完成后等。

   10. **形式验证（Formal Verification)**  这里所指的形式验证是逻辑功能上的等效性检查。这种方法与动态仿真最大的不同点在于它不需要输入测试向量，而根据违路的结构判断两个设计在逻和功能上是否相等。在整个设计流程中会多次引入形式验证用于比较RTL代码之间、门级网表与RTL代码之间，以及门级网表之间在修改之前与修改之后功能的一致性。形式验证与静态时序分析一起，构成设计的静态验证。

   11. **可测性电路插入（DFT,Design for Test)**  可测性设计是SoC设计中的重要一步。通常，对于逻辑电路采用扫描链的可测试结构，对于芯片的输入／输出端口采肚边界扫描的可测试结构、基本思想是通过插入扫地质，增加电路内部艾点的可控性和叫观测性，以达到提高测试效率的目的。一般在逻辑综合或物理综合后进行有描电路的插入和优化。

   12. **时钟树综合（Clock Tree Synthesis)**  SoC设计方法强调同步电路的设计，即所有的寄存器或一组寄存器是由同一个时钟的同一个也沿驱动的，构造芯片内部全局或局部平衡的时钟链的过程称为时极综合。分布在芯片内部寄存器与时钟的驱动电路构成了一种树状结构，这种结构称为时钟树，时钟树综合是在布线设计之前进行的。

   13. **布线设计（Routing)**  这一阶段完成所有节点的连接。布线工具通常将布线分为两个阶段：全局布线与详细布线，在布局之后，电路设计通过全局布线决定布局的质量及提供大致的延时信息。如果单元布局的不好，全局布线将会花上远比单元布局多得多的时间。不好的布局同样会影响设计的整体时序。因此，为了减少综合到布局的迭代次数及提高布局的质量，通常在全局布线之后要提取一次时序信息，尽管此时的时序信息没有详细布线之后得到的准确。得到的时序信息将被反标（BackAnhotation)到设计网表上，用于做静态时序分析，只有当时序得到满足时才进行到下一阶段。详细布线是布局工具做的最后一步，在详细布线完成之后，可以得到精确的时序信息。

   14. **寄生参数提取（Parasitic Extraction)**  寄生参数提取是提取版图上内部互连所产生的寄生电阻和电容值。这些信息通常会转换成标准延迟的格式被反标回设计，用于做静态时序分析和后仿真。

   15. **后仿真（Post-layout Simulation)**  后仿真也叫门级仿真、时序仿真、带反标的仿真，需要利用在布局布线后获得的精确延迟参数和网表进行仿真，验证网表的功能和时序是否正确。后仿真一般使用标准延时（SDF,StandardDelay Format)文件来输入延时信息。

   16. **ECO修改（ECO,Engineering Change Order)**  ECO 修改是工程修改命令的意思。这一步实际上是正常设计流程的一个例外。当在设计的最后阶段发现个别路径有时序问题或逻辑错误时，有必要对设计进行小范围的修改和重新布线。ECO 修改只对版图的一小部分进行修改而不影响到芯片其余部分的布局布线，这样就保留了其他部分的时序信息没有改变。在大规模的IC设计中，ECO修改是一种有效、省时的方法，通常会被采用。

   17. **物理验证（Physical Verification)**  物理验证是对版图的设计规则检查（DRC,Design Rule Check)及逻辑图网表和版图网表比较（LVS,Layout Vs. Schematic).DRC用以保证制造良率，LVS用以确认电路版图网表结构是否与其原始电路原理图（网表）一致。LVS可以在器件级及功能块级进行网表比较，也可以对器件参数，如MOS电路沟道宽／长、电容／电阻值等进行比较。

   在完成以上步骤之后，设计就可以签收、交付到芯片制造厂流片了。在实际的IC设计中，设计工程师将依赖EDA工具完成上述各步骤。不同的EDA厂商通常会结合自己的EDA工具特点提供设计流程，但这些设计流程大体上是一致的。

   就开发工具而言，我们课程教学全程使用的是Iinux的开发环境，我们使用VCS编译型仿真器，DC使用的SMIC180工艺库，后端设计EDA工具用的是ICC（IC Compiler，是Synopsys的新一代后端设计工具），使用的是仍然是配套DC的SMIC180工艺的lib。

4. 树莓派有点对标intel 的飞扬系列，MIPS对标 酷睿和志强，就芯片的应用场景，DESKTOP, SERVER, 嵌入式，移动手机？通用处理器，FPGA，ARM，就学习计算机体系架构，RISCV是现在学界的首推？你怎么看？自己的学习曲线是什么样的？

   传统上，通用处理器(General Purpose Processor, GPP)采用冯.诺依曼存储器结构，程序与数据共用一个存储器空间，通用一组总线（一个地址总线和一个数据总线）链接到处理器核。通常，做一次乘法会发生4次存储器访问，用掉至少四个指令周期。通用芯片关键是“通用”二字，这意味着其必须具备处理各式各样千奇百怪的指令要求，并且经常同时存在多个外部设备的请求，它必须拥有随时中止目前的运算转而进行其他运算，完成后再从中断点继续当前运算的能力。GPP的操作标准化和通用性已经取得成效，支持操作系统，所以原来开发的嵌入式系统方便人机交互以及和标准接口设备通信，不再需要硬件开发。

   现场可编程逻辑阵列(Field Programmable Gate Array, FPGA)性能不断在提高，以其高速度、高灵活、高集成性广泛应用于各种领域。由于FPGA内部结构的特点，它可以很容易地实现分布式的算法结构，这一点对于实现无线通信中的高速数字信号处理十分有利。因为在无线通信系统中，许多功能模块通常都需要大量的滤波运算，而这些滤波函数往往需要大量的乘和累加操作。而通过FPGA来实现分布式的算术结构，就可以有效地实现这些乘和累加操作。目前，无线通信一方面正向话音和数据综合的方向发展，另一方面迫切需要将移动技术综合到手持PDA产品中去。因此，随着无线移动通信系统的发展，以及对更为完善的便携式系统的期望，构架系统模块的处理器就必须更加地强有力。

   通用处理器能够计算任何可计算的。拥有指令集扩展的通用处理器与通用处理器类似，不过其针对一些特定的应用能更好的执行。专用处理器（或者协处理器）在计算一些特定任务时很快，但是无法计算其之外的其它应用。FPGA可以用于构建以上所有硬件，但是与一个ASIC方法比较，其保证灵活性的同时牺牲了速度。CPU与FPGA的根本区别在于软件与硬件的差异。CPU为冯诺依曼结构，串行地执行一系列指令；而FPGA可以实现并行操作，就像在一个芯片中嵌入多个CPU，其性能会是单个CPU的十倍、百倍。一般来说，CPU可以实现的功能，都可以用硬件设计的方法由FPGA来实现。当然，极其复杂的算法用硬件实现会比较困难，资源消耗也很大，如果没有高性能要求，那用硬件实现就有点得不偿失了。对于一个复杂系统而言，进行合理的软、硬件划分，由CPU（或DSP）和硬件电路（如FPGA）合作完成系统功能是非常必要的，也是高效的。而ARM系架构因其兼顾功耗、代码密度、性价比，在一众处理器中应用范围最广，非常适用于移动通信领域。移动手机不如电脑对性能的要求高，更注重于便携程度。电脑所用的处理器为以X86为代表的复杂指令集架构。这就是现在手机处理器和电脑处理器尽管频率已不尽相同，但手机性能远不及电脑，电脑便携程度远不及手机。目前超85%的无线通讯设备采用的是ARM，ARM在此领域一家独大。与之类似，ARM在嵌入式技术中使用也同样最为广泛。多数数码相机中也应用了ARM嵌入式微处理器，专用性强，具有较好的实用性和可扩展性。

   现在主流的处理器架构主要包括X86架构、Power PC架构、ARM架构、MIPS架构、RISC-V架构五种主流架构。X86是一种可变指令长度，主要是复杂指令集计算，这个架构是Intel的商业架构，占据了大部分的DESKTOP, SERVER市场份额。而Power PC架构属于RISC架构，其是由1991年苹果公司、国际商用机器公司（IBM）和摩托罗拉联盟创建，在嵌入式和高性能处理器中仍然很受欢迎。它在很大程度上基于IBM早起的POWER指令集架构，并保持了高度的兼容性。而ARM架构师精简指令集架构（RISC），这种处理器成本低、功耗低而且产生的热量更低，因此被广泛使用在便携式设备或者嵌入式设备中。MIPS架构也称为无互锁流水线级微处理器（RISC）架构，由MIPS计算机系统公司开发，这个架构也是国内龙芯主推的架构。而RISC-V架构是一种基于已建立的RISC原则的开放标准指令集架构。与大多数其他ISA设计不同，RISC- V是在开源许可下提供的，使用时不需要付费，所以相对于前面四者（商业化或者收费）来说它是性价比最高的一种选择。

   从2018年开始，RISC-V架构在我国迅速发展。

   - 2018 年 11 月，我国政府成立了中国开放指令生态联盟（China RISC-V Alliance，CRAV），旨在基于 RISC-V 指令集建立起中国的开源芯片生态，国内很多研究机构、企业、院校和个人设计者都加入到RISC-V 指令集的研究中。2017 年，吕倩茹设计了一种专用于网络报文转发场景的 RISC-V 的重定制压缩指令集和专用指令扩展。
   - 2018 年，侯鹏飞等人设计了一套专用密码指令集扩展，他在 6 种基本整数指令格式的基础上，又设计了源目的类型、特殊寄存器类型和双循环类型这三种特殊格式的指令。这套专用密码指令集扩展的特殊之处在于它能够有效地改善和加快密码算法的处理速度。
   - 2019年7月，阿里旗下半导体公司 T-head 发布了一款基于 RISC-V 指令集架构处理器 Xuantie-910 时。其不仅实现了 RV64GCV，而且同时设计实现了内存访问增强和基本算术运算增强两类自定义指令扩展。

   除此之外，国内还有很多RISC-V指令集扩展研究项目，比如晶心科技的Andes Custom Extension、芯来科技的自定义指令扩展以及华米科技的用于AI的处理器的指令扩展等等。

   - 2019 年 7 月 25 日，平头哥半导体发布了成立以来的首个产品——玄铁910，它带有高性能向量计算单元，同时还实现了 50 多条自定义扩展指令，最多可支持 16 核，在计算、存储和多核等方面性能较为出色。芯来科技不仅推出了开源 RISC-V 处理器 E203，还出版了相应的 RISC-V 处理器设计书籍在中国宣传推广RISC-V。蜂鸟 E200系列是芯来科技发布的RISC-V 架构的系列处理器，面Io T 领域，其中，E203 Core是开源的，同时也是国内首个开源 RISC-V处理器，E205 Core支持RV32IMAC 架构，硬件实现了单周期乘法和多周期除法。
   - 2020 年，高扬设计出了自定义RISC-V向量指令集扩展，指令长度位32 位，选用的用户自定义操作码空间custom-0即为0001011，具有积指令、池化指令和非线性指令三种指令。

   除此之外，国内还有许多相关研究成果：
   - 华米科技在其推出的智能手环中采用了自家研制的基于RISC-V 内核的处理器黄山 1 号；
   - 嘉楠科技基于 Rocket 内核发布了面向 AIo T 场景的 RISC-V 处理器 K210，服务于人工智能边缘计算。
   - 元峰公司致力于 RISC-V开发环境及操作系统的研究。

   不仅是各个研究团队或者企业，国内各大高校也积极对RISC-V 指令集的研究投入精力，
   - 由清华大学、北京大学、中科院计算所等高校与企业发起成立了中国开放指令生态（RISC-）联盟。

   RISC-V架构的确是国内学术界和工业界中硬件架构的好选择。但是由于校内课程的设置，我们学校的学习初期是学习X86架构为主，后来由于X86架构并不对学术开源，后来学习国内的龙芯MIPS架构。我们的学习路线主要是：龙芯整体架构-基本的数值运算与存储器基础- LoongIDE- MIP S32指令集架构-中央处理器架构-协处理器架构-汇编语言-中断与异常- LInux操作系统的移植。

5. 现在手上有什么芯片相关的项目？和学习内容吗？是北京化工大学电子科学与技术的项目？还是社区的？

   我们最近在进行一个科研项目，我们通过使用FPGA设计计算加速器和NIR近红外光谱仪通过URAT相连接，主要是讲NIR扫描的光谱信息使用神经网络进行处理。我们将常用的卷积层、激活函数、池化层、全连接层等通过verilog写好基础的算子，然后再使用这些算子构建称为加速网络。可以协助加速处理光谱信息。

6. 是什么机缘，加入到开源EDA群的？听过那些讲座？受益大，可以推荐下。

   我是在朋友的介绍下加入开源EDA群的，主要是在群里分享几次的讲座。我觉得有一次分享“基于RISC- V的Linux发行版及软件生态”那次讲座不错，很偏向于实战。还有一次由海思EDA首席专家分享的“数字芯片后端EDA工具的挑战与机遇”我觉得非常好，那一次全方面地介绍EDA的设计流程，对我的启发非常大。


7. 对于全栈学习，小白新人，适合使用开源EDA吗？开源EDA只是一个学界的研究目标，并不实用？谈谈你对开源EDA的看法？至少目前使用开源EDA的人并不多。至少我了解的一些 FPGA 竞赛，都是商业EDA的天下，和厂家的努力。

   开源的EDA往往是少数学者、工程师构建的体系，但是国内学习EDA的教学资源是非常有限的，特别是缺少完善的社区环境和自学的答疑解惑的平台。我在第一次学习EDA的过程中遇到的问题是非常多的，如果没有工程师的知道和学长的帮助，我感觉通过个人的努力或者百度平台来解决我所遇到的问题是十分困难的。从全球来看，集成电路是全球半导体产业的最大分市场，而开源EDA是集成电路行业最精尖起支撑作用的部分，EDA工具的覆盖面和具备功能已经十分全面，涉及领域广，所以全球乃至中国内其市场规模一直稳步上升。目前全球EDA市场处于新思科技、铿腾电子、西门子EDA三家厂商垄断的格局，行业高度集中。2020年国际EDA前五企业全球市场占有率超过85%。其中铿腾电子占比最大，达32%、其次分别为新思科技、西门子EDA、ANSYS、是德科技，占比分别为29.1%、16.6%、4.8%、3.3%。开源的EDA其实也会有工程上的应用，国内比较成功的有立创EDA，实质上它并不是一个“IC”设计的平台，而是PCB电路板设计平台。对于开源EDA其实也有细分，常用的EDA技术主要针对电子电路设计、PCB设计和IC设计，也可以细分为系统及、电路级和物理实现级。对于开源EDA在PCB设计的应用我觉得工业推广和自学的难度阻力都是非常小的，非常容易上手。所以针对板级的EDA设计其实很多技术爱好者、学者和工程师都会把这个当作自己的爱好，在闲暇之余开发一些有趣的产品。但是即使这样子，也至少需要有电路、数电和模电的基础，这些理论知识往往是大学课程中用一到两年的时间去展开的，这也带来一定的门槛。关于开源EDA在“IC设计”中的使用，这个近几年在很多IC公司都会使用他们来进行芯片设计并进行流片（如海思等），但是个人抛开科研的目的外花费几年时间专注研究来熟练掌握这个工具这个时间精力门槛还是很高的。

8. 无人机竞赛的经历，也请分享一二，开源飞控的使用经验特别宝贵，如果有开源仓就更棒啦！

   我们没有单独参加过专项的无人机竞赛，但是无人机曾经作为比赛过程中的一个硬件载体来使用。我们有一个互联网+的农业项目有一部分是无人机的组成的。其中是利用开源的Pixhawk自动驾驶仪开发的F450，我们那个时候为了方便视觉开发，我们使用的是Dronekit库而不是常用的ArduPilot库。那个时候可以直接使用边缘设备通过UART将自动驾驶仪的硬件信息读入到树莓派或者jetson nano中，然后主要是通过单目摄像头和单点测距雷达来定位到特定的农田处进行植保。如果通过Dronekit其实写代码非常简单，而且仿真也很容易，因为可以直接在终端安装好这个库任何输入飞行的参数比如说偏航角、油门的参数就可以飞行（如果使用的是笛卡尔坐标系）。这个过程如果使用开源的pixhawk的话反而工程量最大的是软件而非硬件，因为硬件主要是一些I/O接口的连接。然后第二个是我们做了很多远程和小范围的通讯，比如说远程的话我们更倾向于给端侧设备接入4G网络远程登录进行控制，如果是集群的话还会涉及到WSN或者ad hoc自组网的构建。小范围的常用的是UWB方案，既能通讯又能定位，适合用于农田作业。同时，我们也尝试在无人机控制和通讯方面使用FPGA。我们更加倾向于把FGPA当作是边缘的加速计算和数字处理模块来开发。主要的方向是基于Pixhawk与FPGA相结合的结构，使用FPGA完成传感器数据的读取、仲裁、融合与滤波等功能，研究传感器数据仲裁算法在FPGA上的实现。他能够很好地帮助我们提高实时图像处理的速度,使得在惯性传感器易受环境干扰的情况下能够支持图像导航的方法来进行导航。其核心的一个思想还是FPGA的并行加速算子来为yolo、unet等检测、分割类视觉算法提供加速。

9. 其他科创竞赛，都有参加过那些？FPGA竞赛有参加吗？或者推荐一下你们学校其他同学的竞赛经历。

   科创竞赛我们主要参加的是互联网+、挑战杯、蓝桥杯、电赛等。FPAG一般每年是电赛规模比较大，但是FPGA竞赛我个人是没有参加的，那个赛道我很多同学参加。这个方向常常会见到高速信号处理方向的题目，当然有时候也会有一些开放性的题目，如近年比较火的一些加速计算的方向的题目、物联网方向的一些题目。这个赛道一般是为在FPGA有一年以上的学习基础的同学设置的，事实上我们也认为没有一年以上FPGA学习经验是不能算入门的，这还是在熟练掌握数电模电和verilog的基础上，能够自主地完成一些电路设计而不是纯吹的ctrl-C+V 的编程方式。我的团队中有一位同学非常擅长设计神经网络的算子，因为FPGA的本质上来说它的硬件机制是并行计算的，所以它在硬件加速上非常有优势。他参加的竞赛也是这个赛道，需要用硬件电路去实现一些算法或者计算过程。这就需要额外对选手再增加一些数理基础的要求了。

10. 能分享一些中学的学习经历吗？选择专业和兴趣的关系。未来准备出国吗？考研？专业选择，或者未来规划是怎样的？

    我在中学的话仍然是以丰富自己学历为主线任务，主要是通过记忆、刷题等一些方法不断提高自己对于一些基础学科的熟悉程度。那个时候还不涉及复杂的技术问题和科学理论，主要是对基础知识的记忆和简单数学理论的建立和推到。从性价比来说，初高中在没有获得保送或者出国时，个人学历带来的提升比任何的技术积累都要好一些。当然也会有很多优秀的同学在初高中积累的技术已经从量变达到了质变的程度。但是把科学技术当作一种兴趣爱好绝对是一个不错的选择。一般来说抛开升学的需求不谈，物理学、数学两门学科是将来所有技术和科学的基础，对于对某个技术领域（而不是政治、金融等）特别有兴趣的同学来说，这两门学科也许是需要重点下苦工钻研的科目。未来是打算出国的，但是应该是博士阶段才出国了，我的本科和硕士专业选择都是电子科学与技术。就专业选择来说，我的选择更加偏向于社会需求+个人兴趣爱好，我个人更喜欢那种项目开发的过程，所以我的方向会偏工业化一些。所以电科专业也会有细分，我是偏设计类+应用类为主。

11. 《我的兴趣爱好》是SIGER 保留问题，可以分享除了学习工作之外的兴趣爱好吗？

    学习工作之余，我最喜欢就是寻找视野开阔的山区放无人机，我有一台早年购入的大疆无人机，当然有时候会拿开源的F450无人机去航拍风景。这个过程非常有意思，因为每一个地方的风景都不一样，特别是无人机可以飞到300m以上时可以俯瞰整个周围的风景非常有意思。或者周围十公里以内的地方飞一下都是很让人放松的一件事。

---

【笔记】[pixhawk（飞控）+ jetson nano](https://gitee.com/RV4Kids/ibex/issues/I61XN6)

- [So we are 100% Cadence based. We use Cadence PCB tools to design the electronics products, and we use the Cadence chip design tools to design our silicon. We use Xcelium to do verification, we do Synthesis through Genus, we do place and route with Innovus, STA signoff with Tempus, and Quantus for extraction.](https://play.vidyard.com/9H13oEJXzBzgeqUjGXjauE?disable_popouts=1&v=4.3.10&type=inline)

- 我们的无人机上面用的都是pixhawk（飞控）➕jetson nano或者树莓派的处理器

  ![](https://foruda.gitee.com/images/1668763063640032777/c4c399d1_5631341.png)

- FPGA的作为核心，第一个目标是构建一个适应航天飞控的并行高速MCU。这还只是一个初步的想法。和项目开发不同，SIGER 以科普期刊的角度，试图从0开始呈现。如何使用 FPGA 构建一颗 高速并行相应 飞控需求的 MCU 为命题。你做过一颗CPU设计，进展到那个步骤啦？如果做为起点，是否适合？距离并行高速MCU差距还有多少？

  - FPGA我是做图像加速的，设计加速算子，搭建神经网络服务的[捂脸]
  - 非常期待咱们的方案，因为这样子一来智能一体化设备
  - 真的非常期待FPGA飞控[坏笑]，因为最近我就在为这个事情头疼

- [Jeston 与 PX4（一）](https://blog.csdn.net/qq_58054907/article/details/122376734) （[摘要](https://gitee.com/RV4Kids/ibex/issues/I61XN6#note_14482102_link)）

  - [Jeston与PX4（二）](https://blog.csdn.net/qq_58054907/article/details/122446455)
  - [Jeston与Px4（三）](https://blog.csdn.net/qq_58054907/article/details/122489542)

- [一天精通无人机：初级篇系列第 11 讲：Pixhawk简介](https://zhuanlan.zhihu.com/p/41921881) （[摘要](https://gitee.com/RV4Kids/ibex/issues/I61XN6#note_14488227_link)）

  <p><img height="169px" src="https://foruda.gitee.com/images/1668783665629749789/d2fe7a2d_5631341.png"> <img height="169px" src="https://foruda.gitee.com/images/1668783672529104016/3b021ad2_5631341.png"></p>

  | # | title | date | img |
  |---|---|---|---|
  | 6. |  [自制小四轴-6-总体配置](http://mp.weixin.qq.com/s?__biz=MzUzMTMzMTY0OA==&amp;mid=2247485455&amp;idx=1&amp;sn=2c3de662a5d425b492b4f02ab946d2aa) | 11/01 | <img width="64px" src="https://foruda.gitee.com/images/1668784587190421549/80b6c5df_5631341.png"> |
  | 5. | [自制小四轴-5-遥控器电路](http://mp.weixin.qq.com/s?__biz=MzUzMTMzMTY0OA==&amp;mid=2247485434&amp;idx=1&amp;sn=8c7b7aaf09e93693de380da076437c22) | 10/15 | <img width="64px" src="https://foruda.gitee.com/images/1668784557229455736/65668b6b_5631341.png"> |
  | 4. | [自制小四轴-4-单片机电路](http://mp.weixin.qq.com/s?__biz=MzUzMTMzMTY0OA==&amp;mid=2247485405&amp;idx=1&amp;sn=27e998ccf1b9d9a6556b15874f839f69) | 09/24 | <img width="64px" src="https://foruda.gitee.com/images/1668784527899894649/aedb3eb0_5631341.png"> |
  | 3. | [自制小四轴-3-电源电路](http://mp.weixin.qq.com/s?__biz=MzUzMTMzMTY0OA==&amp;mid=2247485388&amp;idx=1&amp;sn=0233e9612c93b797404ba78160dd2e53) | 08/28 | <img width="64px" src="https://foruda.gitee.com/images/1668784503368357204/adea6967_5631341.png"> |
  | 2. | [自制小四轴-2-电机驱动电路](http://mp.weixin.qq.com/s?__biz=MzUzMTMzMTY0OA==&amp;mid=2247485364&amp;idx=1&amp;sn=2cc52ac8fbf6f17480ff603efc2e2170) | 08/24 | <img width="64px" src="https://foruda.gitee.com/images/1668784477925465959/6f0b4b71_5631341.png"> |
  | 1. | [自制小四轴-1-我的无人机情结](http://mp.weixin.qq.com/s?__biz=MzUzMTMzMTY0OA==&amp;mid=2247485336&amp;idx=1&amp;sn=8667a918d8a7bf24a878c3c05db9555b) | 08/22 | <img width="64px" src="https://foruda.gitee.com/images/1668784456966367060/27756f30_5631341.png"> |

- [如何用开源飞控PIXHAWK进行二次开发？](https://www.zhihu.com/question/38874663) （[摘要](https://gitee.com/RV4Kids/ibex/issues/I61XN6#note_14488332_link)）

  <p><img height="169px" src="https://foruda.gitee.com/images/1668784961669672603/1af29508_5631341.png"> <img height="169px" src="https://foruda.gitee.com/images/1668784975984974962/b8a3f968_5631341.png"> <img height="169px" src="https://foruda.gitee.com/images/1668784982736320070/bd48cc40_5631341.png"> <img height="169px" src="https://foruda.gitee.com/images/1668784993428982968/06cb68b6_5631341.png"> <img height="169px" src="https://foruda.gitee.com/images/1668785000888319321/ec6bcd71_5631341.png"> <img height="169px" src="https://foruda.gitee.com/images/1668785109936126416/9c004ef5_5631341.png"></p>

  - https://docs.px4.io/v1.11/zh/config/firmware.html
  - https://docs.px4.io/v1.11/zh/

  [PX4 自动驾驶用户指南](https://gitee.com/RV4Kids/ibex/issues/I61XN6#note_14488340_link)

  > PX4 是一款专业级飞控。 它由来自业界和学术界的世界级开发商开发，并得到活跃的全球社区的支持，为从竞速和物流无人机到地面车辆和潜水艇的各种载具提供动力。

  - [Pixhawk原生PX4固件中的坑 - fly2mato](https://www.cnblogs.com/fly2mato/p/6575556.html) - 博客园

- [GPS，北斗飞控定位模块的选择 - APM飞控/Pixhawk飞控](www.playuav.com/article/293) ...
  
  > 网页2016-12-12 · GPS，北斗飞控定位模块的选择. 硬件市场. 飞控定位是无人机的关键，选择无 …

    <p><img height="169px" src="https://foruda.gitee.com/images/1668785332879304520/e608e0dd_5631341.png"></p>

- [飞控开发 - APM飞控/Pixhawk飞控 - 开源飞控讨论](www.playuav.com/explore/category-7)

  > 2017-1-5 · 杭州中科微，是国内北斗模块主要厂家之一，北斗办权威比测第一名！ 可提供性 …

    - 飞控硬件Pixhawk官网 http://pixhawk.org/
    - 飞控软件PX4 ® & ArduPilot ®官网 https://px4.io/
    - http://ardupilot.org/
    - 视频资源官网 https://www.youtube.com/user/PX4Autopilot/videos

    编辑于 2019-07-12 16:22

- [RC Bellergy的无人机  我的DIY无人机笔记](https://gitee.com/RV4Kids/ibex/issues/I61XN6#note_14488452_link)

  https://bellergy.com/

  <p><img height="169px" src=""> <img height="169px" src="https://foruda.gitee.com/images/1668786386983986162/2309c171_5631341.png"> <img height="169px" src="https://foruda.gitee.com/images/1668786468890862616/4d4e5460_5631341.png"> <img height="169px" src="https://foruda.gitee.com/images/1668786544330609873/8187eb77_5631341.png"> <img height="169px" src="https://foruda.gitee.com/images/1668786623558281396/b6338e9c_5631341.jpeg"></p>

- 全国北斗杯大赛作品视频  https://www.bilibili.com/video/BV15W411s74s

- [第二届MATLAB杯全球（中国赛区）大学生无人机竞赛](https://www.aero.sjtu.edu.cn/Data/View/4277)  2021-07-20  （[转载](https://gitee.com/RV4Kids/ibex/issues/I61XN6#note_14488586_link)）

- [关于EDE](http://www.int-ede.com/) 全国青少年无人机大赛 （[摘要](https://gitee.com/RV4Kids/ibex/issues/I61XN6#note_14488589_link)）

- [全国大学生电子设计大赛控制类无人机这块难度有多大？](https://www.zhihu.com/question/315831416) （[摘要](https://gitee.com/RV4Kids/ibex/issues/I61XN6#note_14488591_link)）

- [PX4用户指南-基本组装-pixhawk连线 - 创客智造](https://www.ncnynl.com/archives/201810/2628.html)
ncnynl.com|1377 × 1837 jpeg|图像可能受版权保护。 

  ![输入图片说明](https://foruda.gitee.com/images/1669090836625018099/24eb68d9_5631341.jpeg "pixhawk_infographic2.jpg")

  - （[封面](https://gitee.com/RV4Kids/ibex/issues/I61XN6#note_14552021_link)）

  - [提问1](https://gitee.com/RV4Kids/ibex/issues/I61XN6#note_14558801_link)、[提问 2-11](https://gitee.com/RV4Kids/ibex/issues/I61XN6#note_14559881_link)

    - [北京化工大学电子科学与技术：柯子翼、雷圆圆、沈继明、黄传勇](https://gitee.com/RV4Kids/ibex/issues/I61XN6#note_15632048_link)

  - [序言](https://gitee.com/RV4Kids/ibex/issues/I61XN6#note_15633361_link)、[笔记](https://gitee.com/RV4Kids/ibex/issues/I61XN6#note_15633707_link)

- [Tensilica & Cadence](https://gitee.com/RV4Kids/ibex/issues/I61SLK)

  ![Cadence Design Syste…](https://foruda.gitee.com/images/1668734477157514192/573a5c59_5631341.png "cadence design systems") ![输入图片说明](https://foruda.gitee.com/images/1668734484578528901/b25c993f_5631341.png "屏幕截图")![输入图片说明](https://foruda.gitee.com/images/1668734489320158208/21d15ee5_5631341.png "屏幕截图")![输入图片说明](https://foruda.gitee.com/images/1668734494689615897/0aa09f95_5631341.png "屏幕截图")![输入图片说明](https://foruda.gitee.com/images/1668734499158263474/c2868370_5631341.png "屏幕截图")![输入图片说明](https://foruda.gitee.com/images/1668734506954537569/756a052a_5631341.png "屏幕截图")![输入图片说明](https://foruda.gitee.com/images/1668734511486616330/07365ad7_5631341.png "屏幕截图")![输入图片说明](https://foruda.gitee.com/images/1668734515423911512/22d980f9_5631341.png "屏幕截图")![输入图片说明](https://foruda.gitee.com/images/1668734519956518482/ccfcb1ba_5631341.png "屏幕截图")![输入图片说明](https://foruda.gitee.com/images/1668734527099980139/1aef9d95_5631341.png "屏幕截图")![输入图片说明](https://foruda.gitee.com/images/1668734537678260890/f0e57b7f_5631341.png "屏幕截图")![输入图片说明](https://foruda.gitee.com/images/1668734543102785788/6d6df95a_5631341.png "屏幕截图")![输入图片说明](https://foruda.gitee.com/images/1668734548733447830/405a90a8_5631341.png "屏幕截图")![输入图片说明](https://foruda.gitee.com/images/1668734555429595359/587a97a4_5631341.png "屏幕截图")![输入图片说明](https://foruda.gitee.com/images/1668734561819187903/5561a7a0_5631341.png "屏幕截图")![输入图片说明](https://foruda.gitee.com/images/1668734566920982087/ac2fee9a_5631341.png "屏幕截图")![输入图片说明](https://foruda.gitee.com/images/1668734570534566890/5baafd27_5631341.png "屏幕截图")

  - Cadence Design Systems
  - Cosmic Circuits
  - Synopsys
  - CEVA, Inc.
  - Arm
  - HiSilicon
  - EEMBC
  - NXP Semiconductors
  - Sigrity
  - ARC International
  - Imagination Technologies
  - STMicroelectronics
  - Microchip Technology
  - Xilinx
  - Open-Silicon
  - JEDEC
  - MediaTek