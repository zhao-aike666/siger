DAY3 6/24 RVSC 2021

- 16:00-16:20 [基于RISC-V 架构的系统类课程教学现状与思考（南京大学）袁春风](https://www.bilibili.com/video/BV1Rg411j79t)
- 17:20-17:30 [RISC-V 教学体系构建，拓展开放架构产学研合作（芯来科技）胡灿](https://www.bilibili.com/video/BV11b4y167Ag)
- 17:40-18:30 [圆桌论坛主题：RISC-V 教育及软件生态](https://www.bilibili.com/video/BV1hq4y1H7HK)

<p><img width="706px" src="https://foruda.gitee.com/images/1660116821628023571/226教育生态2021副本.jpeg"></p>

封面置顶了两本南京大学袁春风老师演讲中推荐的两本教材，她横向比对了国内外的教学现状，展示了RISCV的开放赋予教育的机会。一门课打通整个计算机体系，最符合全栈学习的议题，因此予以推荐。RVSC 2021 回顾有三个专题都有芯来的身影，本专题收录了两个同场分享的嘉宾也介绍了与芯来的合作，其对产学研的观点具有代表性。当天结尾的圆桌论坛对教育和软件生态的讨论，主持人总结的使命：帮助中国CPU实现 “自主，可控，繁荣，创新” 展示了整个行业对青年未来的期待，回顾这一年的 RISCV 生态建设的方略，从这里找到了端倪。下面是我梳理的复习笔记，顺序记录，同学们可以选择性地重温。

2021 RVSC 回顾提纲：

16:00-16:20 基于RISC-V 架构的系统类课程教学现状与思考（南京大学）袁春风
- https://www.bilibili.com/video/BV1Rg411j79t

16:20-16:40 RV-SoC Design Methodology: 沟内首个基于商业级 RISC-V 处理器的 MOOC 课程（西南交通大学）
- https://www.bilibili.com/video/BV16b4y167ek

  - BOPPPS: 1. 热身暖场 2. 学习目标 3. 前测 4. 参与式学习 5. 后测 6. 总结
  - 可以修改和复制的 MOOC: SOC课程知识点化，可以很好融如老师自己的课程体系中

16:50-17:00 RISC-V 在 IC 在线教育培训平台中的应用（赖琳辉）摩尔精英
- https://www.bilibili.com/video/BV1mo4y1D76r

17:10-17:20 全面拥抱 RISC-V 生态、探索开放架构产学研合作（兆易创新）潘攀
- https://www.bilibili.com/video/BV1Lv411E7xY

17:20-17:30 RISC-V 教学体系构建，拓展开放架构产学研合作（芯来科技）胡灿
- https://www.bilibili.com/video/BV11b4y167Ag

24日下午，17:40 - 18:30 圆桌论坛

圆桌论坛主题：  
RISC-V 教育及软件生态
- https://www.bilibili.com/video/BV1hq4y1H7HK

主持人：  
汪志伟 芯原微电子 副总裁、系统平台解决方案事业部 总经理

拟邀请圆桌嘉宾：  
- 胡灿：芯来科技 生态发展总监  
- 潘攀：兆易创新 合肥总经理  
- 张先轶：澎峰科技 CEO& 创始人  
- 凌明：东南大学国家专用集成电路系统工程技术研究中心 副教授  
- 邸志雄：西南交通大学 电子工程系 副系主任

# 大学课程教学现状

16:00-16:20 基于RISC-V 架构的系统类课程教学现状与思考（南京大学）袁春风
- https://www.bilibili.com/video/BV1Rg411j79t

  <p><img width="23.69%" src="https://foruda.gitee.com/images/1660092576041721539/屏幕截图.png"> <img width="23.69%" src="https://foruda.gitee.com/images/1660092442651200125/屏幕截图.png"> <img width="23.69%" src="https://foruda.gitee.com/images/1660092536304955969/屏幕截图.png"> <img width="23.69%" src="https://foruda.gitee.com/images/1660092520977116114/屏幕截图.png"></p>

### 国内外大学采用 RISC-V 架构进行教学的情况

- 美国大学  
  UC Berkeley CS 61 C  
  CMU ECE 18-447、MIT EECS 6.004 等
- 中国大学  
  清华、南大、国科大、国防科大、武大、华中科大  
  东南大学、天大、华南理工、江南大学 等

- 南京大学基于 RISC-V 架构课程实验教学情况

  - 数字逻辑与计算机组成（大一下）
  - 数字逻辑与计算机组成实验（大二上）
  - 计算机系统基础（大二上）
  - 操作系统（大二下）
  - 计算机系统设计综合实验（大三下）

- 开放的 RISC-V 课程资源

### 《计算机组成与设计》课件资源
https://2d.hep.com.cn/mobile/book/show/F3VW3EDXU

- 计算机系统概述
  - 计算机基本结构和基本工作原理，计算机系统层次结构，性能评价
- 数据的类型及机器级表示（《计算机系统基础》教材对应内容）
  - 二进制数表示，整数表示，浮点数表示，非数值数据的编码
- 数据的运算及运算部件
  - 各种加法器和ALU，定点数运算及其部件，浮点数运算及其运算部件
- 指令系统及程序的机器级表示
  - 指令系统概述，ISA设计，指令系统实例：RISC-V（基础整数指令集、过程调用、程序转换链接与加载、可选扩展指令集、异常和中断机制）
- 中央处理器设计
  - CPU 概述，单周期CPU，多周期CPU，带异常处理的CPU，流水线CPU，冒险处理，分支预测，高级流水线技术（超流水，超标量，动态流水线，乱序发射等）
- 存储器层次结构（《计算机系统基础》教材对应内容）
  - 存储器概述，主存储器基本结构，高速缓存，虚拟存储器
- 系统互连与输入/输出（《计算机系统基础》教材对应内容）
  - 外设及其与主机的互连，I/O接口和I/O端口，I/O方式（查询，中断和DMA）
- 并行处理系统
  - UMA多处理器，CC-NUMA多处理器，片级多处理器和多线程技术、共享存储器的同步控制（用RISC-V相关指令实现）、集群、网格、向量处理机、SIMD技术、GPU以及并行处理编程模型概述

### 《数字逻辑与计算机组成》教材 
http://www.hzcourse.com/web/teachRes/detail/5131/222

- 信息的二进制编码
  - 计算机系统概述，二进制数表示，整数表示，浮点数表示．非数值数据的编码
- 数字逻辑基础 
  - 逻辑门和数字抽象，布尔代数，逻辑关系描述，逻辑函数的化简和变换
- 组合逻辑电路
  - 组合逻辑概述，典型组合逻辑部件设计，组合逻辑电路时序分析
- 时序逻辑电路
  - 时序逻辑概述，锁存器和触发器
  - 同步时序逻辑设计，典型时序逻辑部件设计

- FPGA设计和硬件描述语言
  - 可编程逻辑器件和FPGA设计，Verilog语言简介、建模方式和代码实例
- 运算方法和运算部件
  - 各种加法器和ALU，定点数运算及其部件，浮点数运算及其运算部件
- 指令系统
  - 指令系统概述，指令系统设计，指令系统实例：RISC-V (RV321) 
- 中央处理器
  - CPU概述，单周期CPU，多周期CPU，流水线CPU，冒险处理，高级流水线
- 存储器层次结构
  - 存储器概述、主存储器基本结构、高速缓存、虚拟存储器
- 系统互连与输入／输出
  - 外设及其与主机的互连、I/O接口和I/O端口、I/O方式（查询、中断和DMA) 

# 企业的产学研合作

### 摩尔精英

16:50-17:00 RISC-V 在 IC 在线教育培训平台中的应用（赖琳辉）摩尔精英

- https://www.bilibili.com/video/BV1mo4y1D76r

  ASIC 教育云 转 RISCV，通过替换RISCV核，构建ASIC实验环境。

  <p><img width="23.69%" src="https://foruda.gitee.com/images/1660095087027826513/屏幕截图.png"> <img width="23.69%" src="https://foruda.gitee.com/images/1660095315444739927/屏幕截图.png"> <img width="23.69%" src="https://foruda.gitee.com/images/1660095259128950558/屏幕截图.png"> <img width="23.69%" src="https://foruda.gitee.com/images/1660095292209405253/屏幕截图.png"> <img width="23.69%" src="https://foruda.gitee.com/images/1660095335179057547/屏幕截图.png"> <img width="23.69%" src="https://foruda.gitee.com/images/1660095350558663011/屏幕截图.png"> <img width="23.69%" src="https://foruda.gitee.com/images/1660095377825932355/屏幕截图.png"> <img width="23.69%" src="https://foruda.gitee.com/images/1660095406650745564/屏幕截图.png"></p>

### 兆易创新

17:10-17:20 全面拥抱 RISC-V 生态、探索开放架构产学研合作（兆易创新）潘攀

- https://www.bilibili.com/video/BV1Lv411E7xY

  <p><img width="23.69%" src="https://foruda.gitee.com/images/1660095489389901148/屏幕截图.png"> <img width="23.69%" src="https://foruda.gitee.com/images/1660095582402639087/屏幕截图.png"> <img width="23.69%" src="https://foruda.gitee.com/images/1660095601681257939/屏幕截图.png"> <img width="23.69%" src="https://foruda.gitee.com/images/1660095629167368560/屏幕截图.png"> <img width="23.69%" src="https://foruda.gitee.com/images/1660095651360402499/屏幕截图.png"> <img width="23.69%" src="https://foruda.gitee.com/images/1660095676279554096/屏幕截图.png"> <img width="23.69%" src="https://foruda.gitee.com/images/1660095712765902571/屏幕截图.png"> <img width="23.69%" src="https://foruda.gitee.com/images/1660095754042802867/屏幕截图.png"> <img width="23.69%" src="https://foruda.gitee.com/images/1660095772705784006/屏幕截图.png"> <img width="23.69%" src="https://foruda.gitee.com/images/1660095805264405400/屏幕截图.png"> <img width="23.69%" src="https://foruda.gitee.com/images/1660095824651855789/屏幕截图.png"> <img width="23.69%" src="https://foruda.gitee.com/images/1660095862084101888/屏幕截图.png"> <img width="23.69%" src="https://foruda.gitee.com/images/1660095877399484973/屏幕截图.png"> <img width="23.69%" src="https://foruda.gitee.com/images/1660095893311726932/屏幕截图.png"> <img width="23.69%" src="https://foruda.gitee.com/images/1660095915677477098/屏幕截图.png"> <img width="23.69%" src="https://foruda.gitee.com/images/1660095939323273852/屏幕截图.png"> <img width="23.69%" src="https://foruda.gitee.com/images/1660095959816006788/屏幕截图.png"> <img width="23.69%" src="https://foruda.gitee.com/images/1660095982936347026/屏幕截图.png"> <img width="23.69%" src="https://foruda.gitee.com/images/1660096015640360064/屏幕截图.png"> <img width="23.69%" src="https://foruda.gitee.com/images/1660096030171331095/屏幕截图.png"> <img width="23.69%" src="https://foruda.gitee.com/images/1660096087019790414/屏幕截图.png"> <img width="23.69%" src="https://foruda.gitee.com/images/1660096151385331838/屏幕截图.png"> <img width="23.69%" src="https://foruda.gitee.com/images/1660096181200675849/屏幕截图.png"> <img width="23.69%" src="https://foruda.gitee.com/images/1660096234040604880/屏幕截图.png"></p>

### 芯来科技

17:20-17:30 RISC-V 教学体系构建，拓展开放架构产学研合作（芯来科技）胡灿

- https://www.bilibili.com/video/BV11b4y167Ag

  <p><img width="23.69%" src="https://foruda.gitee.com/images/1660096888816411658/屏幕截图.png"> <img width="23.69%" src="https://foruda.gitee.com/images/1660096906827475080/屏幕截图.png"> <img width="23.69%" src="https://foruda.gitee.com/images/1660096926600020318/屏幕截图.png"> <img width="23.69%" src="https://foruda.gitee.com/images/1660096949397760036/屏幕截图.png"> <img width="23.69%" src="https://foruda.gitee.com/images/1660096960262059682/屏幕截图.png"> <img width="23.69%" src="https://foruda.gitee.com/images/1660096985797594425/屏幕截图.png"> <img width="23.69%" src="https://foruda.gitee.com/images/1660096996969406521/屏幕截图.png"> <img width="23.69%" src="https://foruda.gitee.com/images/1660097012424412685/屏幕截图.png"> <img width="23.69%" src="https://foruda.gitee.com/images/1660097025256685533/屏幕截图.png"> <img width="23.69%" src="https://foruda.gitee.com/images/1660097051454177981/屏幕截图.png"> <img width="23.69%" src="https://foruda.gitee.com/images/1660097063673535044/屏幕截图.png"> <img width="23.69%" src="https://foruda.gitee.com/images/1660097088420031198/屏幕截图.png"></p>

  ![](https://foruda.gitee.com/images/1660097039008802004/屏幕截图.png)

  - 中国研究生电子设计竞赛
  - 全国大学生集成电路创新创业大赛
  - 中国研究生创芯大赛
  - 2021嵌入式芯片与系统设计大赛

# 教育及软件生态

24日下午，17:40 - 18:30 圆桌论坛

圆桌论坛主题：  
RISC-V 教育及软件生态

https://www.bilibili.com/video/BV1hq4y1H7HK

主持人：  
汪志伟 芯原微电子 副总裁、系统平台解决方案事业部 总经理

拟邀请圆桌嘉宾：  
胡灿：芯来科技 生态发展总监  
潘攀：兆易创新 合肥总经理    
张先轶：澎峰科技 CEO& 创始人  
凌明：东南大学国家专用集成电路系统工程技术研究中心 副教授  
邸志雄：西南交通大学 电子工程系 副系主任

![输入图片说明](https://foruda.gitee.com/images/1660097435557865121/屏幕截图.png "屏幕截图.png")

### 问题一：

企业在RISC-V教育中应该扮演什么样的角色和提供什么样的内容，才能有效促进RISC-V人才培养？

- 做好产品，产业热了，同学们就有学习热情啦。支持老师完善课程。
- 科研的，产业的，是有区分的。有联盟更宏观的把握。
- 同学们多参加开源软件社区的贡献，就是最好的贡献了。
- 社区更重要，精准的了解需求。

### 问题二：

企业如何与高效达成有效的合作，以实现真正的需求？

- 企业的什么阶段的产品，去和高校合作？今天的产品，明天的产品，后天的产品。应该是明天的，后天的未来的拿来给高校。来自学生，高于学生。3-5年的前瞻。
- 竞赛适合工科学生，补充学习的内容，研究生适合芯片设计
- 需求侧的推动，联合实验室（商业计划书是起点），大学计划的合伙人公司（虚拟的，需要高频有效沟通）

### 问题三：

高效在本科与研究生教学中如何构建基于RISC-V处理器的课程体系？

- 知识体系的重构，才有可能减少一些旧的，而增加上RV。课程都有压缩的趋势。
- 课程体系，不因该基于RISC-V，初衷不对。不因热而设，是因为RV是OPEN，能看到EVERYTHING. 指令集技术，微架构技术，通用技术，开源性是特点。设计精巧，与其他的差别是促进同学们思考的（袁春风老师同观点）

### 问题四：

基于RISCV的CPU该和什么样的操作系统实现共赢？  
鸿蒙、阿里OS、RT-thread、FreeRTOS等操作系统该如何选择？  
谷歌的Fuchsia OS在RISC-V时代的影响力预期如何？

"RISC-V产业及生态发展” 圆桌讨论现场嘉宾投票结果  
（6月22日，RISC-V 2021中国峰会）

1. 鸿蒙 56.27%
2. 谷歌 Fuchsia OS 14.24%
3. RT-thread 11.86%
4. FreeRTOS 9.49%
5. 其他 4.75%
6. 阿里OS 3.39%

- 不同OS不同应用场景，RISC-V应根据场景捆绑什么OS共赢。（胡）


### 一句话总结：

- 软件生态，距离硬件生态差很多，希望产业界和学术界共同努力
- 学生是中国生态的希望，激发学生的热情很重要
- 商业力量，致力于天下没有难做的硬件（MCU）
- 众人拾柴火焰高，中国RISCV生态指日可待
- RISCV 起源于开源，社区做好了，就都好了，学生也从社区中汲取营养。基础软件生态，教育生态，和社区生态相结合（胡）
- 开放的项目，学校和企业共建
- 百花齐放的局面下，各自发挥优势，贡献自己的力量，在未来大一统的时代找到自己的定位。
- 利用RISCV ，开源，开放，可精简的特点，加强企业高校的合作，促进RISC-V 生态的发展。帮助中国CPU实现 自主，可控，繁荣，创新。

---

本期专题的线索，[来自2021的峰会手册](https://gitee.com/RV4Kids/PLCT-Weekly/issues/I5LBSH)：

<p><img width="369px" src="https://foruda.gitee.com/images/1660001635708226115/2053484940.jpeg"><br>
<img width="369px" src="https://foruda.gitee.com/images/1660001647107745626/1619218366.jpeg"><br>
<img width="369px" src="https://foruda.gitee.com/images/1660001659582762778/1059449190.jpeg"><br>
<img width="369px" src="https://foruda.gitee.com/images/1660001670503279264/499297770.jpeg"></p>