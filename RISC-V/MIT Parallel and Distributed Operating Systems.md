Title:

- MIT PDOS Frans Kaashoek Robert Morris 
- 6.S081 Operating System Engineering
- 6.824 Distributed System
- ‘Morris Worm’ turns 30

Contents:

- 5.4 RISC-V 寄存器 
- 肖宏辉​ 知乎首页
- MIT6.S081 操作系统工程中文翻译
  - github 1.1k stars
  - SUMMARY.md Table of contents
  - 课程摘要：1.-27.
- Hong Hui Xiao GITHUB homepage
- 6.824 Distributed System
- ‘Morris Worm’ turns 30
- [其他学习笔记](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SW63#note_8766735_link)

<p><img width="706px" src="https://images.gitee.com/uploads/images/2022/0219/023540_ea9a692f_5631341.png" title="MIT Parallel and Distributed Operating Systems"></p>

> 刚刚发布的 [RISC-V Instruction Set LuckyDoku Extendsion (RISC-V Luckies) v0.1.69](https://gitee.com/flame-ai/siger/blob/master/sig/ysyx/RISC-V%20Luckies%20v0.1.69.md) 是 [石榴派 寒假集训](https://gitee.com/flame-ai/siger/blob/master/sig/ysyx/) 的重要成果，而这个春节后诞生的成果，其学习过程的花絮同样精彩，因为，它挖掘出了一个宝藏，MIT 的热门网课，是从头数理自己计算机体系知识的宝贵资源，同时，挖掘出了它们的译者。刚看 一生一芯 的三期视频时，就曾扫过 MIT 分布式系统课，除了弹幕各种膜拜，其实并没有那么强烈的学习动力，直到寒假课程的契机到来，确定学习方案，竟然是 MIT 操作系统工程的一篇 RISC-V 资料。机缘就是这么奇妙 :pray:

> 开篇提纲就是学习线索，一路展开，直到挖掘出 MIT 大神 [Robert Morris](#morris-worm-turns-30)，世界首个 计算机 Worm 就是他的杰作，自行脑补，这可是30多年前的过往啊。真真地知道，我们缺少的太多太多，无论发展多快，都无法弥补我们的不足，这就是不争的事实。唯有学习，学习，再学习啦。快快随我畅游在这知识的海洋里吧，满足感，获得感，充实在心 :pray:

# [5.4 RISC-V寄存器](https://zhuanlan.zhihu.com/p/295439950) 【[笔记](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SW63#note_8590201_link)】

肖宏辉(网络/OpenStack/SDN/NFV搬运工)

我们之前看过了汇编语言和RISC-V的介绍。接下来我们看一下之后lab相关的内容。这部分的内容其实就是本节课的准备材料中的内容。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0209/150714_03377e64_5631341.png "屏幕截图.png")

你们现在对于这个表达应该都很熟悉了，这个表里面是RISC-V寄存器。寄存器是CPU或者处理器上，预先定义的可以用来存储数据的位置。寄存器之所以重要是因为汇编代码并不是在内存上执行，而是在寄存器上执行，也就是说，当我们在做add，sub时，我们是对寄存器进行操作。所以你们通常看到的汇编代码中的模式是，我们通过load将数据存放在寄存器中，这里的数据源可以是来自内存，也可以来自另一个寄存器。之后我们在寄存器上执行一些操作。如果我们对操作的结果关心的话，我们会将操作的结果store在某个地方。这里的目的地可能是内存中的某个地址，也可能是另一个寄存器。这就是通常使用寄存器的方法。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0209/150720_ab6106a7_5631341.png "屏幕截图.png")

寄存器是用来进行任何运算和数据读取的最快的方式，这就是为什么使用它们很重要，也是为什么我们更喜欢使用寄存器而不是内存。当我们调用函数时，你可以看到这里有a0 - a7寄存器。通常我们在谈到寄存器的时候，我们会用它们的ABI名字。不仅是因为这样描述更清晰和标准，同时也因为在写汇编代码的时候使用的也是ABI名字。第一列中的寄存器名字并不是超级重要，它唯一重要的场景是在RISC-V的Compressed Instruction中。基本上来说，RISC-V中通常的指令是64bit，但是在Compressed Instruction中指令是16bit。在Compressed Instruction中我们使用更少的寄存器，也就是x8 - x15寄存器。我猜你们可能会有疑问，为什么s1寄存器和其他的s寄存器是分开的，因为s1在Compressed Instruction是有效的，而s2-11却不是。除了Compressed Instruction，寄存器都是通过它们的ABI名字来引用。

a0到a7寄存器是用来作为函数的参数。如果一个函数有超过8个参数，我们就需要用内存了。从这里也可以看出，当可以使用寄存器的时候，我们不会使用内存，我们只在不得不使用内存的场景才使用它。

表单中的第4列，Saver列，当我们在讨论寄存器的时候也非常重要。它有两个可能的值Caller，Callee。我经常混淆这两个值，因为它们只差一个字母。我发现最简单的记住它们的方法是：

- Caller Saved寄存器在函数调用的时候不会保存
- Callee Saved寄存器在函数调用的时候会保存

![输入图片说明](https://images.gitee.com/uploads/images/2022/0209/150746_4103a207_5631341.png "屏幕截图.png")

这里的意思是，一个Caller Saved寄存器可能被其他函数重写。假设我们在函数a中调用函数b，任何被函数a使用的并且是Caller Saved寄存器，调用函数b可能重写这些寄存器。我认为一个比较好的例子就是Return address寄存器（注，保存的是函数返回的地址），你可以看到ra寄存器是Caller Saved，这一点很重要，它导致了当函数a调用函数b的时侯，b会重写Return address。所以基本上来说，任何一个Caller Saved寄存器，作为调用方的函数要小心可能的数据可能的变化；任何一个Callee Saved寄存器，作为被调用方的函数要小心寄存器的值不会相应的变化。我经常会弄混这两者的区别，然后会到这张表来回顾它们。

如果你们还记得的话，所有的寄存器都是64bit，各种各样的数据类型都会被改造的可以放进这64bit中。比如说我们有一个32bit的整数，取决于整数是不是有符号的，会通过在前面补32个0或者1来使得这个整数变成64bit并存在这些寄存器中。

- 学生提问：返回值可以放在a1寄存器吗？
  > TA：这是个好问题。我认为理论上是可以的，如果一个函数的返回值是long long型，也就是128bit，我们可以把它放到一对寄存器中。这也同样适用于函数的参数。所以，如果返回值超过了一个寄存器的长度，也就是64bit，我们可以将返回值保存在a0和a1。但是如果你只将返回值放在a1寄存器，我认为会出错。

- 学生提问：为什么寄存器不是连续的？比如为什么s1与其他的s寄存器是分开的？
  > TA：我之前提到过，但是也只是我的猜想，我并不十分确定。因为s1寄存器在RISC-V的Compressed Instruction是可用的，所以它才被分开。

- 学生提问：除了Stack Pointer和Frame Pointer，我不认为我们需要更多的Callee Saved寄存器。
  > TA：s0 - s11都是Callee寄存器，我认为它们是提供给编译器而不是程序员使用。在一些特定的场景下，你会想要确保一些数据在函数调用之后仍然能够保存，这个时候编译器可以选择使用s寄存器。

发布于 2020-11-15 21:34

# 肖宏辉​ (网络/OpenStack/SDN/NFV搬运工) 【[笔记](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SW63#note_8590578_link)】
  
- 知乎首页摘要:

  - 居住地 : 现居北京
  - 所在行业 : 计算机软件
  - 职业经历 : 字节跳动（ByteDance）| VMware（威睿）| 华为 | IBM
  - 教育经历 : 中国科学院大学
  - 个人简介 : 瞎折腾

  什么是知乎众裁官，如何成为众裁官？

- [赞同了回答2021-02-10 08:41](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SW63#note_8591028_link)   《 一个自律的人有多可怕？ 》 虎山行不行
  
  > 看见这题我就溜达进来了。 我知道自律现在是个褒义词，高度自律的话堪比封神。 于是乎我进来就想说一句“别太自律”喔各位老铁。会要命的。 我自己要是没有所谓“高度自律”的切身体会，我这么唠嗑会被各位乱棍打死是没跑的。 所以我先简单说说自己经历，再说说关于自律的看法。 自律的经历我就说健身，写文，两个方面。 先说健身。 我从初中开始泡拳馆…
  
- [赞同了文章 2021-01-21 13:43](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SW63#note_8591394_link) Lockless Programming 兰新宇 talk is cheap
  
  _说明：本文主要是汇总了以下的几篇文章，并经过了一定的加工和整理。_ 

  - [Lockless Programming Considerations for Xbox 360 and Microsoft Windows](https://docs.microsoft.com/zh-cn/windows/win32/dxtecharts/lockless-programming?redirectedfrom=MSDN) 
  - [Memory Barriers Are Like Source Control Operations](https://preshing.com/20120710/memory-barriers-are-like-source-control-operations/) 
  - [acquire-and-release-semantics](https://preshing.com/20120913/acquire-and-release-semantics/)

  **正文** 202 赞同 9 条评论

  > 包括spinlock, mutex在内的「锁」的机制会带来一些问题，比如需要持有两个锁，但加锁的顺序不对，就可能形成"A-B"类死锁，或者由于设计错误、持锁线程被抢占等原因，导致一个thread持锁时间太长。

  > 似乎「无锁编程」的概念可以解决上述的这些问题，但"lockless programming"本身是复杂的，需要深刻地理解它，才能从技术的角度获得预想的收益。来看下在实际的应用中，无锁编程都面临哪些挑战，以及如何应对。
  
- [赞同了文章 2020-10-03 16:22](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SW63#note_8591622_link) 二十八画生征友：一起来通关6.S081/6.828吧~ 胡津铭
  
  > 6.828 Operating System Engineering是MIT久负盛名的一门操作系统相关的课程。这门课程有多牛呢？举个例子，以陈海波老师为首的上海交通大学IPADS实验室可以说是国内最强的操作系统相关实验室之一，而陈海波老师等人在他们读博的时候（早在2005年左右）就完成了6.828课程的学习，甚至还以6.828的JOS系统为基础开发了Corey操作系统，并发表了一篇ODSI2008论文Corey: An operating system for many cores，该论文也是陈老师至今引用最高的论文。而后，IPADS老师也基于6.828的资源，在上海交通大学软件学院开设了多年操作系统课程。

  > 好的，那么我就先写到这里，感兴趣的同学请独立完成前两个Lab之后加群交流~ **群号为603579009，加群的时候请附上自己完成前两个Lab的github repo地址~~** 谢谢大家啦
  
- [赞同了回答 2019-06-23 22:25](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SW63#note_8592049_link) ( 2674 赞同 257 评论)
  
  [中美网络通讯的海底光缆大约有多少根？每根有多粗？](https://www.zhihu.com/question/314793505/answer/621832267) 
  
  > 这个之前有人利用wikipedia的数据和谷歌地图，做出了一幅互动式的世界海底光缆分布图。 目前中国大陆和美国连通的海底光缆主要有： 1. 跨太平洋快线（Trans-Pacific Express，简称TPE） 跨太平洋快线又称横太平洋快速海缆，为连接中国大陆、台湾、韩国、日本和美国的一条海底通信电缆。由中国电信、中国网通、…
  
- 赞同了回答 2018-02-23 07:02 (13.3万 赞同 5367 条评论) 人是怎么废掉的？ (曾加) 数学话题下的优秀答主

  >  **沉溺于「轻易获得高成就感」的事情** ：有意无意地寻求用很小付出获得很大「回报」的偏方，哪怕回报是虚拟的。这种行为会提升自己的「兴奋阈值」，让人越来越不容易拥有成就感，从而越来越依赖虚拟的成就感，陷入恶性循环。症状轻的： **沉溺游戏** （在虚拟世界中加快时间流逝的速度，使得「成功」来得更快）、 **种马小说** （意淫人能轻易获得想要的东西）；…
  
- [回答了问题2017-05-08 15:56](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SW63#note_8592616_link)   

  [私有云爆发，该不该意外？](https://www.zhihu.com/question/58755220/answer/166472677) （肖宏辉）
  
  > 公有云不能完全取代私有云， 1，如果私有云成本更低或者相当，企业更愿意使用私有云。大部分企业没有这个实力让成本更低，因此大家才觉得公有云有发展。不过公司的CEO CTO 有时不甘心，还是想尝试一下。 2，对安全或其他有要求的企业部门，更倾向于自建或者委托别人搭建私有云。比如银行和政府部门不可能把核心业务放到公有云上。就算是…

# [MIT6.S081 操作系统工程中文翻译](https://www.zhihu.com/column/c_1294282919087964160) 【[笔记](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SW63#note_8593414_link)】

 **置顶内容** 

### MIT6.S081（2020）翻译简介

肖宏辉(网络/OpenStack/SDN/NFV搬运工)

因为学习MIT6.824，偶然知道了MIT6.S081这门课程。MIT6.S081这门课程的标题是Operating System Engineering，主要讲的就是操作系统。授课教授是Robert Morris和Frans Kaashoek，两位都是非常出名的程序员。

课程是基于一个类似于Unix但是简单的多的教学操作系统XV6来讲解，虽然不是原汁原味的Linux，但是对于理解Linux的工作方式和结构是足够了。与MIT6.824一样的是，这门课程是全英文，甚至英文字幕都没有。对于国内的同学来说，如果英文没有足够好，很难较好的理解这门课程。因此我计划将这门课程翻译成中文文字版。我将在语句通顺的前提下，尽量还原课程的内容，希望可以帮助大家学习到这门课程。如果你的英语不是那么好，建议阅读完文字再去看视频相关的课程。

目前MIT的这门课还没有上完，按计划是在12月初完成，具体的内容可以参考【1】。每一节课都在80分钟左右，大概会有6-9个知识点，我会按照独立的知识点将每节课拆分成6-9个小节。

----------------------------------------------------------------------------------

2021-04-24 更新：

今天终于把问答课以外的20节课程都翻译完了，总共大概有35万个字，花费时间大概在200个小时左右。

这门课程相比6.824来说更像是一个整体。6.824更多的是在理解和设计分布式系统时的一些技术和技巧，而6.S081介绍了Unix风格操作系统的各个方面（虽然这两个课没什么关系(￣.￣)，但是因为是连着翻译的难免会有对比）。

实际中的操作系统会更加的复杂，但是通过这门课程的学习基本上可以对操作系统有一个全面的认识。经过翻译的过程，我自己也把之前的一些知识盲区补全了。这门课程虽然只是一个MIT的本科课程，但是我推荐给所有从事IT相关工作的同学，掌握了操作系统对于面试，debug，写代码都是有好处的。

最后，希望我的翻译可以帮助到你。

【1】https://pdos.csail.mit.edu/6.828/2020/schedule.html

### 如果

- 你发现了翻译的错误，或者想把剩下几门课程的翻译补上，可以向关联的[github](https://link.zhihu.com/?target=https%3A//github.com/huihongxiao/MIT6.S081)提交PR
- 你觉得我做的还不错，可以关注我的[知乎](https://www.zhihu.com/people/xiao-hong-hui-15)，并给我一个点赞。
- 你正在找一份工作，并且想与和我一样的工程师共事，请联系
  我：honghui_xiao@yeah.net
- 还想学习其他MIT课程，我还做了一些其他的翻译：
  - [MIT6.824 - 分布式系统](https://link.zhihu.com/?target=https%3A//mit-public-courses-cn-translatio.gitbook.io/mit6-824/)
  - [MIT6.829 - 计算机网络](https://link.zhihu.com/?target=https%3A//mit-public-courses-cn-translatio.gitbook.io/mit6.829/) （work in progress）

### _声明_ 

 _此次翻译纯属个人爱好，如果涉及到任何版权行为，请联系我，我将删除内容。文中所有内容，与本人现在，之前或者将来的雇佣公司无关，本人保留自省的权利，也就是说你看到的内容也不一定代表本人最新的认知和观点。_ 

编辑于 2022-01-25 11:13

# [github 1.1k stars](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SW63#note_8593868_link)

### About
No description, website, or topics provided.  
Resources
- [Readme](https://gitee.com/shiliupi/MIT6.S081/blob/master/README.md) 【[笔记](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SW63#note_8593414_link)】
- Stars 1.1k stars
- Watchers 20 watching
- Forks 182 forks 
- commits  390  ( [06f26f0](https://github.com/huihongxiao/MIT6.S081/commit/06f26f00b91b1ca2109adcf4425592aa5c2bbf47) 5 days ago )

### [SUMMARY.md](https://gitee.com/shiliupi/MIT6.S081/blob/master/SUMMARY.md)

# Table of contents 

* [简介](https://gitee.com/shiliupi/MIT6.S081/blob/master/README.md)
* [Lec01 Introduction and Examples \(Robert\)](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec01-introduction-and-examples/README.md)
  * [1.1 课程内容简介](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec01-introduction-and-examples/1.1-ke-cheng-jian-jie.md)
  * [1.2 操作系统结构](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec01-introduction-and-examples/1.2-cao-zuo-xi-tong-jie-gou.md)
  * [1.3 Why Hard and Interesting](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec01-introduction-and-examples/1.3-why-hard-and-interesting.md)
  * [1.4 课程结构和资源](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec01-introduction-and-examples/1.4-ke-cheng-zi-yuan.md)
  * [1.5 read, write, exit系统调用](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec01-introduction-and-examples/1.5-some-systemcalls.md)
  * [1.6 open系统调用](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec01-introduction-and-examples/1.6-open-xi-tong-tiao-yong.md)
  * [1.7 Shell](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec01-introduction-and-examples/1.7-shell.md)
  * [1.8 fork系统调用](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec01-introduction-and-examples/1.8-fork.md)
  * [1.9 exec, wait系统调用](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec01-introduction-and-examples/1.9-exec-and-wait-systemcall.md)
  * [1.10 I/O Redirect](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec01-introduction-and-examples/1.10-io-redirect.md)
* [Lec03 OS Organization and System Calls \(Frans\)](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec03-os-organization-and-system-calls/README.md)
  * [3.1 上一节课回顾](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec03-os-organization-and-system-calls/3.1-shang-jie-ke-hui-gu.md)
  * [3.2 操作系统隔离性（isolation）](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec03-os-organization-and-system-calls/3.2-cao-zuo-xi-tong-ge-li-xing-isolation.md)
  * [3.3 操作系统防御性（Defensive）](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec03-os-organization-and-system-calls/3.3-cao-zuo-xi-tong-fang-yu-xing-defensive.md)
  * [3.4 硬件对于强隔离的支持](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec03-os-organization-and-system-calls/3.4-ying-jian-dui-yu-qiang-ge-li-de-zhi-chi.md)
  * [3.5 User/Kernel mode切换](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec03-os-organization-and-system-calls/3.5-user-kernel-mode-switch.md)
  * [3.6 宏内核 vs 微内核 （Monolithic Kernel vs Micro Kernel）](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec03-os-organization-and-system-calls/3.6-monolithic-kernel-vs-micro-kernel.md)
  * [3.7 编译运行kernel](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec03-os-organization-and-system-calls/3.7-bian-yi-yun-xing-kernel.md)
  * [3.8 QEMU](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec03-os-organization-and-system-calls/3.8-qemu.md)
  * [3.9 XV6 启动过程](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec03-os-organization-and-system-calls/3.9-xv6-qi-dong-guo-cheng.md)
* [Lec04 Page tables \(Frans\)](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec04-page-tables-frans/README.md)
  * [4.1 课程内容简介](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec04-page-tables-frans/4.1-ke-cheng-nei-rong-jian-jie.md)
  * [4.2 地址空间（Address Spaces）](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec04-page-tables-frans/4.2-di-zhi-kong-jian-address-spaces.md)
  * [4.3 页表（Page Table）](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec04-page-tables-frans/4.3-ye-biao-page-table.md)
  * [4.4 页表缓存（Translation Lookaside Buffer）](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec04-page-tables-frans/4.4-ye-biao-huan-cun-translation-lookaside-buffer.md)
  * [4.5 Kernel Page Table](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec04-page-tables-frans/4.5-kernel-page-table.md)
  * [4.6 kvminit 函数](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec04-page-tables-frans/4.6-kvminit-han-shu.md)
  * [4.7 kvminithart 函数](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec04-page-tables-frans/4.7-kvminithart.md)
  * [4.8 walk 函数](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec04-page-tables-frans/4.8-walk-han-shu.md)
* [Lec05 Calling conventions and stack frames RISC-V \(TA\)](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec05-calling-conventions-and-stack-frames-risc-v/README.md)
  * [5.1 C程序到汇编程序的转换](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec05-calling-conventions-and-stack-frames-risc-v/5.1-introduction-to-lecture05.md)
  * [5.2 RISC-V vs x86](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec05-calling-conventions-and-stack-frames-risc-v/5.2-risc-v-vs-x86.md)
  * [5.3 gdb和汇编代码执行](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec05-calling-conventions-and-stack-frames-risc-v/5.3-gdb-he-hui-bian-dai-ma-zhi-hang.md)
  * [5.4 RISC-V寄存器](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec05-calling-conventions-and-stack-frames-risc-v/5.4-risc-v-ji-cun-qi.md) 【[笔记](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SW63#note_8590201_link)】
  * [5.5 Stack](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec05-calling-conventions-and-stack-frames-risc-v/5.5-stack.md)
  * [5.6 Struct](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec05-calling-conventions-and-stack-frames-risc-v/5.6-struct.md)
* [Lec06 Isolation & system call entry/exit \(Robert\)](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec06-isolation-and-system-call-entry-exit-robert/README.md)
  * [6.1 Trap机制](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec06-isolation-and-system-call-entry-exit-robert/6.1-trap.md)
  * [6.2 Trap代码执行流程](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec06-isolation-and-system-call-entry-exit-robert/6.2-trap-dai-ma-zhi-xing-liu-cheng.md)
  * [6.3 ECALL指令之前的状态](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec06-isolation-and-system-call-entry-exit-robert/6.3-before-ecall.md)
  * [6.4 ECALL指令之后的状态](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec06-isolation-and-system-call-entry-exit-robert/6.4-ecall-zhi-ling-zhi-hou-de-zhuang-tai.md)
  * [6.5 uservec函数](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec06-isolation-and-system-call-entry-exit-robert/6.5-uservec.md)
  * [6.6 usertrap函数](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec06-isolation-and-system-call-entry-exit-robert/6.6-usertrap.md)
  * [6.7 usertrapret函数](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec06-isolation-and-system-call-entry-exit-robert/6.7-usertrapret.md)
  * [6.8 userret函数](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec06-isolation-and-system-call-entry-exit-robert/6.8-userret.md)
* [Lec08 Page faults \(Frans\)](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec08-page-faults-frans/README.md)
  * [8.1 Page Fault Basics](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec08-page-faults-frans/8.1-page-fault-basics.md)
  * [8.2 Lazy page allocation](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec08-page-faults-frans/8.2-lazy-page-allocation.md)
  * [8.3 Zero Fill On Demand](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec08-page-faults-frans/8.3-zero-fill-on-demand.md)
  * [8.4 Copy On Write Fork](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec08-page-faults-frans/8.4-copy-on-write-fork.md)
  * [8.5 Demand Paging](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec08-page-faults-frans/8.5-demand-paging.md)
  * [8.6 Memory Mapped Files](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec08-page-faults-frans/8.6-memory-mapped-files.md)
* [Lec09 Interrupts \(Frans\)](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec09-interrupts/README.md)
  * [9.1 真实操作系统内存使用情况](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec09-interrupts/9.1-memory-in-real-os.md)
  * [9.2 Interrupt硬件部分](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec09-interrupts/9.2-interrupt-handware.md)
  * [9.3 设备驱动概述](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec09-interrupts/9.3-device-driver.md)
  * [9.4 在XV6中设置中断](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec09-interrupts/9.4-xv6-set-interrupt.md)
  * [9.5 UART驱动的top部分](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec09-interrupts/9.5-uart-driver-top.md)
  * [9.6 UART驱动的bottom部分](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec09-interrupts/9.6-uart-driver-bottom.md)
  * [9.7 Interrupt相关的并发](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec09-interrupts/9.7-interrupt-related-concurrency.md)
  * [9.8 UART读取键盘输入](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec09-interrupts/9.8-uart-read-keyboard.md)
  * [9.9 Interrupt的演进](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec09-interrupts/9.9-interrupt-envolving.md)
* [Lec10 Multiprocessors and locking \(Frans\)](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec10-multiprocessors-and-locking/README.md)
  * [10.1 为什么要使用锁？](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec10-multiprocessors-and-locking/10.1-why-lock.md)
  * [10.2 锁如何避免race condition？](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec10-multiprocessors-and-locking/10.2-avoid-race-condition.md)
  * [10.3 什么时候使用锁？](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec10-multiprocessors-and-locking/10.3-when-use-lock.md)
  * [10.4 锁的特性和死锁](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec10-multiprocessors-and-locking/1.5-locks-properties-and-deadlock.md)
  * [10.5 锁与性能](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec10-multiprocessors-and-locking/10.5-suo-yu-xing-neng.md)
  * [10.6 XV6中UART模块对于锁的使用](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec10-multiprocessors-and-locking/10.6-case-study-uart.md)
  * [10.7 自旋锁（Spin lock）的实现（一）](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec10-multiprocessors-and-locking/10.7-spin-lock-1.md)
  * [10.8 自旋锁（Spin lock）的实现（二）](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec10-multiprocessors-and-locking/10.8-spin-lock-2.md)
* [Lec11 Thread switching \(Robert\)](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec11-thread-switching-robert/README.md)
  * [11.1 线程（Thread）概述](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec11-thread-switching-robert/11.1-thread.md)
  * [11.2 XV6线程调度](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec11-thread-switching-robert/11.2-xian-cheng-diao-du.md)
  * [11.3 XV6线程切换（一）](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec11-thread-switching-robert/11.3-xv6-thread-switching-1.md)
  * [11.4 XV6线程切换（二）](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec11-thread-switching-robert/11.4-xv6-thread-switching-2.md)
  * [11.5 XV6进程切换示例程序](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec11-thread-switching-robert/11.5-xv6-thread-switching-code.md)
  * [11.6 XV6线程切换 --- yield/sched函数](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec11-thread-switching-robert/11.6-yield-and-sched.md)
  * [11.7 XV6线程切换 --- switch函数](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec11-thread-switching-robert/11.7-xv6-switch-function.md)
  * [11.8 XV6线程切换 --- scheduler函数](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec11-thread-switching-robert/11.8-xv6-scheduler-function.md)
  * [11.9 XV6线程第一次调用switch函数](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec11-thread-switching-robert/11.9-xv6-call-switch-function-first-time.md)
* [Lec13 Sleep & Wake up \(Robert\)](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec13-sleep-and-wakeup-robert/README.md)
  * [13.1 线程切换过程中锁的限制](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec13-sleep-and-wakeup-robert/13.1-lock-limitation-during-thread-switching.md)
  * [13.2 Sleep&Wakeup 接口](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec13-sleep-and-wakeup-robert/13.2-sleep-wakeup.md)
  * [13.3 Lost wakeup](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec13-sleep-and-wakeup-robert/13.3-lost-wakeup.md)
  * [13.4 如何避免Lost wakeup](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec13-sleep-and-wakeup-robert/13.4-avoid-lock-wakeup.md)
  * [13.5 Pipe中的sleep和wakeup](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec13-sleep-and-wakeup-robert/13.5-sleep-and-wakeup-in-pipe.md)
  * [13.6 exit系统调用](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec13-sleep-and-wakeup-robert/13.6-exit-systemcall.md)
  * [13.7 wait系统调用](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec13-sleep-and-wakeup-robert/13.7-wait-systemcall.md)
  * [13.8 kill系统调用](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec13-sleep-and-wakeup-robert/13.8-kill-systemcall.md)
* [Lec14 File systems \(Frans\)](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec14-file-systems-frans/README.md)
  * [14.1 Why Interesting](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec14-file-systems-frans/14.1-why-interesting.md)
  * [14.2 File system实现概述](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec14-file-systems-frans/14.2-file-system-api.md)
  * [14.3 How file system uses disk](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec14-file-systems-frans/14.3-how-file-system-uses-disk.md)
  * [14.4 inode](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec14-file-systems-frans/14.4-inode.md)
  * [14.5 File system工作示例](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec14-file-systems-frans/14.5-file-system-example.md)
  * [14.6 XV6创建inode代码展示](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec14-file-systems-frans/14.6-xv6-code-inode.md)
  * [14.7 Sleep Lock](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec14-file-systems-frans/14.7-sleep-lock.md)
* [Lec15 Crash recovery \(Frans\)](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec15-crash-recovery-frans/README.md)
  * [15.1 File system crash概述](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec15-crash-recovery-frans/15.1-file-system-crash-gaishu.md)
  * [15.2 File system crash示例](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec15-crash-recovery-frans/15.2-file-system-crash-shi-li.md)
  * [15.3 File system logging](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec15-crash-recovery-frans/15.3-file-system-logging.md)
  * [15.4 log\_write函数](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec15-crash-recovery-frans/15.4-log-write-han-shu.md)
  * [15.5 end\_op函数](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec15-crash-recovery-frans/15.5-endop-han-shu.md)
  * [15.6 File system recovering](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec15-crash-recovery-frans/15.6-file-system-recovering.md)
  * [15.7 Log写磁盘流程](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec15-crash-recovery-frans/15.7-log-xie-ci-pan-liu-cheng.md)
  * [15.8 File system challenges](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec15-crash-recovery-frans/15.8-file-system-challenges.md)
* [Lec16  File system performance and fast crash recovery \(Robert\)](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec16-file-system-performance-and-fast-crash-recovery-robert/README.md)
  * [16.1 Why logging](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec16-file-system-performance-and-fast-crash-recovery-robert/16.1-why-logging.md)
  * [16.2 XV6 File system logging回顾](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec16-file-system-performance-and-fast-crash-recovery-robert/16.2-xv6-file-system-logging-hui-gu.md)
  * [16.3 ext3 file system log format](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec16-file-system-performance-and-fast-crash-recovery-robert/16.3-ext3-file-system-log-format.md)
  * [16.4 ext3如何提升性能](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec16-file-system-performance-and-fast-crash-recovery-robert/16.4-ext3-ru-he-ti-sheng-xing-neng.md)
  * [16.5 ext3文件系统调用格式](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec16-file-system-performance-and-fast-crash-recovery-robert/16.5-ext3-wen-jian-xi-tong-diao-yong-ge-shi.md)
  * [16.6 ext3 transaction commit步骤](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec16-file-system-performance-and-fast-crash-recovery-robert/16.6-ext3-transaction-commit-bu-zhou.md)
  * [16.7 ext3 file system恢复过程](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec16-file-system-performance-and-fast-crash-recovery-robert/16.7-file-system-hui-fu-guo-cheng.md)
  * [16.8 为什么新transaction需要等前一个transaction中系统调用执行完成](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec16-file-system-performance-and-fast-crash-recovery-robert/16.8-transaction-concurrency.md)
  * [16.9 总结](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec16-file-system-performance-and-fast-crash-recovery-robert/16.9-zong-jie.md)
* [Lec17 Virtual memory for applications \(Frans\)](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec17-virtual-memory-for-applications-frans/README.md)
  * [17.1 应用程序使用虚拟内存所需要的特性](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec17-virtual-memory-for-applications-frans/17.1-ying-yong-cheng-xu-shi-yong-xu-ni-nei-cun-suo-xu-yao-de-te-xing.md)
  * [17.2 支持应用程序使用虚拟内存的系统调用](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec17-virtual-memory-for-applications-frans/17.2-zhi-chi-ying-yong-cheng-xu-shi-yong-xu-ni-nei-cun-de-xi-tong-tiao-yong.md)
  * [17.3 虚拟内存系统如何支持用户应用程序](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec17-virtual-memory-for-applications-frans/17.3-xu-ni-nei-cun-xi-tong-zhi-chi-yong-hu-cheng-xu.md)
  * [17.4 构建大的缓存表](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec17-virtual-memory-for-applications-frans/17.4-build-large-cache.md)
  * [17.5 Baker's Real-Time Copying Garbage Collector](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec17-virtual-memory-for-applications-frans/17.5-bakers-garbage-collector.md)
  * [17.6 使用虚拟内存特性的GC](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec17-virtual-memory-for-applications-frans/17.6-shi-yong-xu-ni-nei-cun-te-xing-de-gc.md)
  * [17.7 使用虚拟内存特性的GC代码展示](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec17-virtual-memory-for-applications-frans/17.7-shi-yong-xu-ni-nei-cun-te-xing-de-gc-dai-ma-zhan-shi.md)
* [Lec18 OS organization \(Robert\)](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec18-os-organization-robert/README.md)
  * [18.1 Monolithic kernel](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec18-os-organization-robert/18.1-monolithic-kernel.md)
  * [18.2 Micro kernel](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec18-os-organization-robert/18.2-micro-kernel.md)
  * [18.3 Why micro kernel?](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec18-os-organization-robert/18.3-why-micro-kernel.md)
  * [18.4 L4 micro kernel](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec18-os-organization-robert/18.4-l4-micro-kernel.md)
  * [18.5 Improving IPC by Kernel Design](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec18-os-organization-robert/18.5-improving-ipc-by-kernel-design.md)
  * [18.6 Run Linux on top of L4 micro kernel](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec18-os-organization-robert/18.6-run-linux-on-top-of-l4-micro-kernel.md)
  * [18.7 L4 Linux性能分析](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec18-os-organization-robert/18.7-l4-linux-performance.md)
* [Lec19 Virtual Machines \(Robert\)](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec19-virtual-machines-robert/README.md)
  * [19.1 Why Virtual Machine?](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec19-virtual-machines-robert/19.1-why-virtual-machine.md)
  * [19.2 Trap-and-Emulate --- Trap](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec19-virtual-machines-robert/19.2-trap-and-emulate-trap.md)
  * [19.3 Trap-and-Emulate --- Emulate](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec19-virtual-machines-robert/19.3-trap-and-emulate-emulate.md)
  * [19.4 Trap-and-Emulate --- Page Table](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec19-virtual-machines-robert/19.4-trap-and-emulate-page-table.md)
  * [19.5 Trap-and-Emulate --- Devices](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec19-virtual-machines-robert/19.5-trap-and-emulate-devices.md)
  * [19.6 硬件对虚拟机的支持](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec19-virtual-machines-robert/19.6-ying-jian-dui-xu-ni-ji-de-zhi-chi.md)
  * [19.7 Dune: Safe User-level Access to Privileged CPU Features](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec19-virtual-machines-robert/19.7-dune.md)
* [Lec20 Kernels and HLL \(Frans\)](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec20-kernels-and-hll-frans/README.md)
  * [20.1 C语言实现操作系统的优劣势](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec20-kernels-and-hll-frans/20.1-c-yu-yan-shi-xian-cao-zuo-xi-tong-de-you-lie-shi.md)
  * [20.2 高级编程语言实现操作系统的优劣势](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec20-kernels-and-hll-frans/20.2-gao-ji-bian-cheng-yu-yan-shi-xian-cao-zuo-xi-tong-de-you-lie-shi.md)
  * [20.3 高级编程语言选择 --- Golang](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec20-kernels-and-hll-frans/20.3-choose-golang.md)
  * [20.4 Biscuit](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec20-kernels-and-hll-frans/20.4-biscuit.md)
  * [20.5 Heap exhaustion](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec20-kernels-and-hll-frans/20.5-heap-exhaustion.md)
  * [20.6 Heap exhaustion solution](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec20-kernels-and-hll-frans/20.6-heap-exhaustion-solution.md)
  * [20.7 Evaluation: HLL benefits](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec20-kernels-and-hll-frans/20.7-evaluation-hll-benefits.md)
  * [20.8 Evaluation: HLL performance cost\(1\)](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec20-kernels-and-hll-frans/20.8-evaluation-hll-performance-cost-1.md)
  * [20.9 Evaluation: HLL performance cost\(2\)](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec20-kernels-and-hll-frans/20.9-evaluation-hll-performance-cost-2.md)
  * [20.10 Should one use HLL for a new kernel?](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec20-kernels-and-hll-frans/20.10-should-one-use-hll-for-a-new-kernel.md)
* [Lec21 Networking \(Robert\)](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec21-networking-robert/README.md)
  * [21.1计算机网络概述](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec21-networking-robert/21.1-ji-suan-ji-wang-luo-gai-shu.md)
  * [21.2 二层网络 --- Ethernet](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec21-networking-robert/21.2-er-ceng-wang-luo-ethernet.md)
  * [21.3 二/三层地址转换 --- ARP](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec21-networking-robert/21.3-er-san-ceng-di-zhi-zhuan-huan-arp.md)
  * [21.4 三层网络 --- Internet](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec21-networking-robert/21.4-san-ceng-wang-luo-internet.md)
  * [21.5 四层网络 --- UDP](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec21-networking-robert/21.5-si-ceng-wang-luo-udp.md)
  * [21.6 网络协议栈（Network Stack）](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec21-networking-robert/21.6-wang-lu-xie-yi-zhan-network-stack.md)
  * [21.7 Ring Buffer](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec21-networking-robert/21.7-ring-buffer.md)
  * [21.8 Receive Livelock](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec21-networking-robert/21.8-receive-livelock.md)
  * [21.9 如何解决Livelock](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec21-networking-robert/21.9-ru-he-jie-jue-livelock.md)
* [Lec22 Meltdown \(Robert\)](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec22-meltdown-robert/README.md)
  * [22.1 Meltdown发生的背景](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec22-meltdown-robert/22.1-meltdown-fa-sheng-de-bei-jing.md)
  * [22.2 Speculative execution\(1\)](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec22-meltdown-robert/22.2-speculative-execution-1.md)
  * [22.3 Speculative execution\(2\)](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec22-meltdown-robert/22.3-speculative-execution-2.md)
  * [22.4 CPU caches](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec22-meltdown-robert/22.4-cpu-caches.md)
  * [22.5 Flush and Reload](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec22-meltdown-robert/22.5-flush-and-reload.md)
  * [22.6 Meltdown Attack](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec22-meltdown-robert/22.6-meltdown-attack.md)
  * [22.7 Meltdown Fix](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec22-meltdown-robert/22.7-meltdown-fix.md)
* [Lec23 RCU \(Robert\)](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec23-rcu-robert/README.md)
  * [23.1 使用锁带来的问题](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec23-rcu-robert/23.1-shi-yong-suo-dai-lai-de-wen-ti.md)
  * [23.2 读写锁 \(Read-Write Lock\)](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec23-rcu-robert/23.2-du-xie-suo-readwrite-lock.md)
  * [23.3 RCU实现\(1\) - 基本实现](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec23-rcu-robert/23.3-rcu-shi-xian-1-ji-ben-shi-xian.md)
  * [23.4 RCU实现\(2\) - Memory barrier](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec23-rcu-robert/23.4-rcu-shi-xian-2-memory-barrier.md)
  * [23.5 RCU实现\(3\) - 读写规则](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec23-rcu-robert/23.5-rcu-shi-xian-3-du-xie-xian-zhi.md)
  * [23.6 RCU用例代码](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec23-rcu-robert/23.6-rcu-yong-li-dai-ma.md)
  * [23.7 RCU总结](https://gitee.com/shiliupi/MIT6.S081/blob/master/lec23-rcu-robert/23.7-rcu-zong-jie.md)

### 课程摘要：

- [1.](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SW63#note_8599345_link) 
- [3-9.](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SW63#note_8599352_link) 
- [10-19.](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SW63#note_8599367_link) 
- [20-27.](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SW63#note_8599380_link) 

| Title | Intro |
|---|---|
| 1.1 课程内容简介 | 大家好，欢迎来到6.S081 --- 操作系统这门课程。我的名字叫Robert，我会与Frans一起讲这门课。David，Nicholas会作为这门课程的助教。在我们在线授课的过程中，请尽情提问。你可以直接语音打断我，或者通过聊天窗口提问，我们工作人员会关注聊天窗口。顺便说一下，我们也会通过视频录制这门课程，之后我们会… |
| 1.2 操作系统结构 | 过去几十年，人们将一些分层的设计思想加入到操作系统中，并运行的很好。我将会为你列出操作系统经典的组织结构，这个组织结构同时也是这门课程的主要内容，这里的组织结构对于操作系统来说还是挺常见的。 这里实际上就是操作系统内部组成，当我想到这里的组织结构时，我首先会想到用一个矩形表示一个计算机，这个计算机有一些硬件资源，我会将它… |
| 1.3 Why Hard and Interesting | 我还想说一下为什么我认为学习操作系统是挑战和乐趣并存的？以及为什么我们需要专门针对操作系统设置一门课程？ 学习操作系统比较难的一个原因是，内核的编程环境比较困难。当你在编写、修改，扩展内核，或者写一个新的操作系统内核时，你实际上在提供一个基础设施让别人来运行他们的程序。当程序员在写普通的应用程序时，应用程序下面都是操作系… |
| 1.4 课程结构和资源 | 接下来，我将花几分钟来介绍6.s081这门课程的结构，之后我们再来介绍具体的技术内容。 课程有一个网站，可以通过google查询6.s081找到。网站上会有课程的计划，lab的内容，课程结构的信息，例如最后是怎么打分的。 另一个资源是Piazza，它主要用来做两件事情，第一件是人们可以在上面咨询有关lab的问题，助教们会尝… |
| 1.5 read, write, exit系统调用 | 接下来，我将讨论对于应用程序来说，系统调用长成什么样。因为系统调用是操作系统提供的服务的接口，所以系统调用长什么样，应用程序期望从系统调用得到什么返回，系统调用是怎么工作的，这些还是挺重要的。你会在第一个lab中使用我们在这里介绍的系统调用，并且在后续的lab中，扩展并提升这些系统调用的内部实现。 我接下来会展示一些简单… |
| 1.6 open系统调用 | 前面，copy代码假设文件描述符已经设置好了。但是一般情况下，我们需要能创建文件描述符，最直接的创建文件描述符的方法是open系统调用。下面是一个叫做open的源代码，它使用了open系统调用。 学生提问：字节流是什么意思？ Robert教授：我。。。我的意思是，如果一个文件包含了一些字节，假设包含了数百万个字节，你触发了… |
| 1.7 Shell | 我说了很多XV6的长的很像Unix的Shell，相对图形化用户接口来说，这里的Shell通常也是人们说的命令行接口。如果你还没有用过Shell，Shell是一种对于Unix系统管理来说非常有用的接口，它提供了很多工具来管理文件，编写程序，编写脚本。你之前看过我演示一些Shell的功能，通常来说，当你输入内容时，你是在告… |
| 1.8 fork系统调用 | 下一个我想查看的例子叫做fork。fork会创建一个新的进程，下面是使用fork的一个简单用例。 在第12行，我们调用了fork。fork会拷贝当前进程的内存，并创建一个新的进程，这里的内存包含了进程的指令和数据。之后，我们就有了两个拥有完全一样内存的进程。fork系统调用在两个进程中都会返回，在原始的进程中，fork系… |
| 1.9 exec, wait系统调用 | 在接下来我展示的一个例子中，会使用echo，echo是一个非常简单的命令，它接收任何你传递给它的输入，并将输入写到输出。 我为你们准备了一个文件名为exec的代码， 代码会执行exec系统调用，这个系统调用会从指定的文件中读取并加载指令，并替代当前调用进程的指令。从某种程度上来说，这样相当于丢弃了调用进程的内存，并开始执行… |
| 1.10 I/O Redirect | 最后一个例子，我想展示一下将所有这些工具结合在一起，来实现I/O重定向。 我们之前讲过，Shell提供了方便的I/O重定向工具。如果我运行下面的指令， Shell会将echo的输出送到文件out。之后我们可以运行cat指令，并将out文件作为输入， 我们可以看到保存在out文件中的内容就是echo指令的输出。 Shell之所以… |

# [Hong Hui Xiao/肖宏辉 huihongxiao](https://github.com/huihongxiao) GITHUB homepage

[<img width="199px" src="https://images.gitee.com/uploads/images/2022/0209/170121_f5e48e40_5631341.png">](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SW63#note_8594679_link)

- Popular repositories:

  - thor 雷神项目，翻译 mit 6.824 2020
  
    视频资源

    - https://www.bilibili.com/video/BV1R7411t71W?p=1

      **2020 MIT 6.824 分布式系统** 
      13.5万播放 · 总弹幕数18812020-02-09 10:18:08

      > MIT著名课程：6.824: Distributed Systems 分布式系统
课程主页：https://pdos.csail.mit.edu/6.824/index.html
课程安排：https://pdos.csail.mit.edu/6.824/schedule.html（有资料
中文字幕组召集中！先会有英文字幕~（CC字幕请自选，手机端在右上三点按钮选择中）
感谢MIT公开这么好的课程资源，我也会持续更新~

        - 字幕制作者（英语（美国））： [Bigbang1984](https://space.bilibili.com/193659599)
          - [第三期“一生一芯”系列视频](https://www.bilibili.com/video/BV1PU4y1V7X3)
        - 字幕制作者（中文（中国））： Cyru1s

# [6.824 Distributed System](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SW63#note_8595820_link)

课程链接：https://pdos.csail.mit.edu/6.824/

> 系统方向非常好的一门课程，每堂课都讲一个新的分布式系统模型，没有教材，每堂课都是直接讲论文。老师是MIT PDOS的神牛Robert Morris (不错，这人就是当年因为发明蠕虫而蹲监然后回MIT当教授的神人)和Frans Kaashoek。这些分布式系统都是实际用在各个大公司里的系统，比如说Spark, GFS，PNUTS。当年我修这门课的时候感觉课程压力非常大，有期中期末考试，有lab作业，有reading work, 还有course project，但是整个课程设计得非常好。lab要用Golang实现，硬生生地学了门新的语言。

### Frans Kaashoek团队

> 来自如雷贯耳的MIT CSAIL，主要研究方向包括操作系统，网络，编译器等，PDOS组的负责人之一，绝对的大佬，后面你会看到被他支配的恐惧感。该实验室大佬众多，超新星Nickolai Zeldovich、Adam Belay、Mit6.284的Robert Morris以及已故的图灵奖获得者、计算机密码发明者Fernando J. Corbató都是个组的，这里暂以他为代表。可以看下团队近期的文章，毕竟一个会议也就20来篇。另外，很多文章都出自其“徒子徒孙”的团队。

- 引自：[系统领域TOP团队](https://zhuanlan.zhihu.com/p/371379581)

### [MIT经典课程“分布式系统”视频版上线！网友：终于来了非偷拍清晰版本](https://zhuanlan.zhihu.com/p/107980328)

<p><img width="369px" src="https://images.gitee.com/uploads/images/2022/0209/174237_52e12e20_5631341.png"></p>

> 来自MIT的经典课程, MIT的分布式系统课程，已经有20年历史，2000年就已经开设课程。当时讲授这门课的老师是Frans Kaashoek（ACM Fellow）和Robert Morris教授。现在，这门课程的讲授者，就只有Robert Morris教授一人了——长达20年教龄。

  - 课程视频官方地址：https://www.youtube.com/channel/UC_7WrbZTCODu1o_kfUMq88g​
  - 课程地址：6.824 Schedule: Spring 2020​ https://pdos.csail.mit.edu/6.824/schedule.html
  - BILIBILI: https://www.bilibili.com/video/av87684880/
  
  
- [Robert Morris](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SW63#note_8596245_link)
  https://www.csail.mit.edu/person/robert-morris

  ![输入图片说明](https://images.gitee.com/uploads/images/2022/0209/174824_692a4b90_5631341.png "在这里输入图片标题")
  
- [Frans Kaashoek](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SW63#note_8596316_link)  
  https://people.csail.mit.edu/kaashoek
  
  ![输入图片说明](https://images.gitee.com/uploads/images/2022/0209/175207_bd639995_5631341.png "在这里输入图片标题")

- [Project Noria: a new data-flow system for web applications](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SW63#note_8596402_link)  
  https://www.csail.mit.edu/research/noria-new-data-flow-system-web-applications
  
- [Research Group： Parallel and Distributed Operating Systems](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SW63#note_8596521_link)  
  https://www.csail.mit.edu/research/parallel-and-distributed-operating-systems
  
  ![输入图片说明](https://images.gitee.com/uploads/images/2022/0209/175820_0c13d243_5631341.jpeg "在这里输入图片标题")
  
- [BING `Robert Morris Frans Kaashoek Map of the internet`](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SW63#note_8596668_link)
  
# ‘Morris Worm’ turns 30
  
![输入图片说明](https://images.gitee.com/uploads/images/2022/0209/180520_23a0e98d_5631341.jpeg "morris-worm-dinesh-abeywickrama-inoshi-ratnasekera-1024x812-1024x812.jpg")

 _The ‘Morris Worm’ turns 30, Dinesh Abeywickrama and Inoshi Ratnasekera explore the History of Computer Worms._ 

## Morris Worm

On 02nd of November 1988, world’s first major malware attack on the internet called ‘Morris Worm’ was unleashed. Even though this was not the first worm, it was considered the first major attack, which was a wakeup call to internet security engineers to consider the risk of software bugs and start research and development of network security (Ivey, 2018; Marsan, 2008).

### Robert Tappan Morris

At 22 years, while attending Cornell graduate university Robert Tappan Morris developed the worm that spread via internet.

Robert Tappan Morris (creator of the ‘Morris Worm’) was born in 1965 and his father was a computer scientist at Bell Labs, who helped design Multics and Unix; and later became the chief scientist at the National Computer Security Center, a division of the National Security Agency (NSA).

<p><img width="199px" src="https://images.gitee.com/uploads/images/2022/0209/214917_f0698f88_5631341.png"></p>

Robert Tappan Morris, creator of ‘Morris worm’ in 2008 

### What / why was it?

Nowadays worms are very common on the internet, 30 years back Morris Worm had affected 6000 UNIX based systems. Before the security experts released patches 24hours later, the worm had affected and crashed 10 percent of internet usage. Rest of the networks were slow. Reports say Morris Worm is not only a worm attack, but also the first distributed DDoS attack (Vaughan-Nichols, 2018).

Morris worm can be considered as self-replicating software, which exploited common weakness of famous soft wares such as ‘Sendmail’ mail sending program, ‘Finger’ tool software help to find logged on users in system. Morris Worm, had exploited three weakness of internet to enter target networks and UNIX systems.

1. Vulnerability in the debug mode of UNIX’s sendmail program
2. A buffer overrun hole in the finger daemon protocol
3. rexec/rsh network logins set up without passwords 

Not only that but ‘Moris Worm’ was the first worm which used a list of popular passwords as a dictionary attack and used simple ‘Xoring’ encryption method to hide password and other strings (Ivey, 2018). According to the security experts, ‘Moris worm’ started from MIT computers and hid, back tracking by unlinking after affecting other networks on the internet. Even though the worm did not include malicious payload, worm caused serious damages to the infected systems as the infected systems tried to spread the worm. Because of this, infected networks slowed down and some systems, which depended on Sun OS, variant of Linux and Solaris, crashed due to the heavy load. At the same time, Morris had included sets of codes to spread fast and finally he had realized the worm was no longer in his control (Vaughan-Nichols, 2018). Out of control the worm started wave after wave of computer and system attacks.

Morris worm spread fast in 1988 when internet was small and people thought it was a very friendly place. Many other organizations, including the U.S. Department of Defense, had to unplug, internet cables to prevent worm infection (Marsan, 2008). 

![输入图片说明](https://images.gitee.com/uploads/images/2022/0209/215005_39f6aec6_5631341.png "屏幕截图.png")

Morris Worm Source Code

### CERT

Because of the damages occurred by ‘Morris worm’ attack, an independent response team named CERT was created at Carnegie Mellon University,  which was funded by the US Department of Defense’s Defense Advanced Research Projects Agency (DARPA). CERT security expert teams had to identify and address potential security threats, risks while consulting vendors, and independent security response teams around the world.

## What happen to creator of ‘Morris’?

In 1991, Morris was sentenced to a three-year probation with 400 hours of community service and a $10,000 fine. Further, Morris was the first person who was tried and convicted of violating the 1986 Computer Fraud and Abuse Act.

After completing his sentence Morris founded two companies and one of his companies were sold to a search giant Yahoo Inc. Morris continued his computer network architectures at Massachusetts Institute of Technology as a professor (Ivey, 2018).

In 1988, when the worm was launched, there was not much commercial traffic or web sites in the internet. Since then, worm damages were limited to government agencies, universities and organizations that used internet networks to exchange e-mails and files. 

### Modern attackers

Today, crashing the internet is not profitable for attackers.  Therefore modern cyber attackers are trickier when attacking systems, they gather information from affected computers, display ads etc.

‘Morris Worm’ made cyber security a legitimate topic among cyber security communities. In early days of computer security, only a few people worked on cyber security and all of them were cryptographers. After ‘Morris Worm’, security experts started to pay much more attention to cyber security as a field of study. 

<p><img width="99px" src="https://images.gitee.com/uploads/images/2022/0209/215057_294b334b_5631341.png"></p>

 **Dinesh Abeywickrama Ph.D.(reading), MBCS, MBA, BCS** 

<p><img width="99px" src="https://images.gitee.com/uploads/images/2022/0209/215112_0d29fee0_5631341.png"></p>

 **Inoshi Ratnasekera MBA, BS.c (Hons) ** 

## References

- Ivey, J.M., 2018. The Morris Worm Turns 30.   
[Online] Available at: https://www.globalknowledge.com/blog/2018/11/01/the-morris-worm-turns-25/   
[Accessed 01 November 2018].

- Marsan, C.D., 2008. Morris worm turns 20: Look what it’s done.   
[Online] Available at: https://www.networkworld.com/article/2268919/lan-wan/morris-worm-turns-20–look-what-it-sdone.html  
[Accessed 23 November 2018].

- Vaughan-Nichols, J., 2018. The day computer security turned real: The Morris Worm turns 30.   
[Online] Available at: https://www.zdnet.com/article/the-day-computer-security-turned-real-the-morris-wormturns-30/   
[Accessed 23 November 2018]. 

# [其他学习笔记](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SW63#note_8766735_link)

- [RISCV-Accelerator](https://gitee.com/RV4Kids/RISCV-Accelerator)
  - [It's report error when make verilog](https://gitee.com/RV4Kids/RISCV-Accelerator/issues/I4SWFR) 
- [RISC-V-CPU](https://gitee.com/RV4Kids/RISC-V-CPU)
  - [给RISC-V设计自定义指令，有什么需要注意的事情吗?](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SWFK) 
  - [好好使用 TOOL CHAIN。](https://gitee.com/RV4Kids/RISC-V-CPU/issues/I4SW63) 
- [riscv_cpu](https://gitee.com/RV4Kids/riscv_cpu)
  - [重温了 objdump 的参数 inst.bin](https://gitee.com/RV4Kids/riscv_cpu/issues/I4SVWW) 
  - [如果你把 ubuntu 上 mnt 的 镜像失误删除，只能悲剧拉！](https://gitee.com/RV4Kids/riscv_cpu/issues/I4SVHA) 
  - [make riscv_cpu](https://gitee.com/RV4Kids/riscv_cpu/issues/I4SV84) 
  - [RISCV五级流水CPU设计](https://gitee.com/RV4Kids/riscv_cpu/issues/I4SQOP) 
  - [用Verilog搭出RISC-V架构单周期CPU](https://gitee.com/RV4Kids/riscv_cpu/issues/I4SQON) 

