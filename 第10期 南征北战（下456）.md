《南征北战》[目录](https://gitee.com/yuandj/siger/blob/master/%E7%AC%AC10%E6%9C%9F%20%E5%8D%97%E5%BE%81%E5%8C%97%E6%88%98%EF%BC%88%E4%B8%8A12%EF%BC%89.md#%E5%8D%97%E5%BE%81%E5%8C%97%E6%88%98)：
- 一、封面故事：[阳光海涛](https://gitee.com/yuandj/siger/blob/master/%E7%AC%AC10%E6%9C%9F%20%E5%8D%97%E5%BE%81%E5%8C%97%E6%88%98%EF%BC%88%E4%B8%8A12%EF%BC%89.md#%E4%B8%80%E5%B0%81%E9%9D%A2%E6%95%85%E4%BA%8B%E9%98%B3%E5%85%89%E6%B5%B7%E6%B6%9B)
  - [能在社区传播的路上有个我](https://gitee.com/yuandj/siger/blob/master/%E7%AC%AC10%E6%9C%9F%20%E5%8D%97%E5%BE%81%E5%8C%97%E6%88%98%EF%BC%88%E4%B8%8A12%EF%BC%89.md#-%E8%83%BD%E5%9C%A8%E7%A4%BE%E5%8C%BA%E4%BC%A0%E6%92%AD%E7%9A%84%E8%B7%AF%E4%B8%8A%E6%9C%89%E4%B8%AA%E6%88%91)
  - [从零开始在Windows下启动openEuler（WSL）](https://gitee.com/yuandj/siger/blob/master/%E7%AC%AC10%E6%9C%9F%20%E5%8D%97%E5%BE%81%E5%8C%97%E6%88%98%EF%BC%88%E4%B8%8A12%EF%BC%89.md#-%E4%BB%8E%E9%9B%B6%E5%BC%80%E5%A7%8B%E5%9C%A8windows%E4%B8%8B%E5%90%AF%E5%8A%A8openeulerwsl)
- 二、[从 0 开始构建 RISC-V 私有云](https://gitee.com/yuandj/siger/blob/master/%E7%AC%AC10%E6%9C%9F%20%E5%8D%97%E5%BE%81%E5%8C%97%E6%88%98%EF%BC%88%E4%B8%8A12%EF%BC%89.md#%E4%BA%8C%E4%BB%8E-0-%E5%BC%80%E5%A7%8B%E6%9E%84%E5%BB%BA-risc-v-%E7%A7%81%E6%9C%89%E4%BA%91)
  - 见证社区成长：[优矽科技、芯来科技 加入 openEuler](https://gitee.com/yuandj/siger/blob/master/%E7%AC%AC10%E6%9C%9F%20%E5%8D%97%E5%BE%81%E5%8C%97%E6%88%98%EF%BC%88%E4%B8%8A12%EF%BC%89.md#%E8%A7%81%E8%AF%81%E7%A4%BE%E5%8C%BA%E6%88%90%E9%95%BF%E4%BC%98%E7%9F%BD%E7%A7%91%E6%8A%80%E8%8A%AF%E6%9D%A5%E7%A7%91%E6%8A%80-%E5%8A%A0%E5%85%A5-openeuler)
  - [SUMMER 2021 & NutShell](https://gitee.com/yuandj/siger/blob/master/%E7%AC%AC10%E6%9C%9F%20%E5%8D%97%E5%BE%81%E5%8C%97%E6%88%98%EF%BC%88%E4%B8%8A12%EF%BC%89.md#-summer-2021--nutshell)
- 三、[一生一芯 NutShell 国科大](https://gitee.com/yuandj/siger/blob/master/%E7%AC%AC10%E6%9C%9F%20%E5%8D%97%E5%BE%81%E5%8C%97%E6%88%98%EF%BC%88%E4%B8%AD3%EF%BC%89.md)
- 四、[欧拉指北 从 0 到 10](https://gitee.com/yuandj/siger/blob/master/%E7%AC%AC10%E6%9C%9F%20%E5%8D%97%E5%BE%81%E5%8C%97%E6%88%98%EF%BC%88%E4%B8%8B456%EF%BC%89.md#%E5%9B%9B%E6%AC%A7%E6%8B%89%E6%8C%87%E5%8C%97-%E4%BB%8E-0-%E5%88%B0-10)
- 五、[SIG组生态树 73-81 SIG 治理地图 生机盎然](https://gitee.com/yuandj/siger/blob/master/%E7%AC%AC10%E6%9C%9F%20%E5%8D%97%E5%BE%81%E5%8C%97%E6%88%98%EF%BC%88%E4%B8%8B456%EF%BC%89.md#%E4%BA%94sig%E7%BB%84%E7%94%9F%E6%80%81%E6%A0%91-73---81-sig-%E6%B2%BB%E7%90%86%E5%9C%B0%E5%9B%BE-%E7%94%9F%E6%9C%BA%E7%9B%8E%E7%84%B6)
  - [社区成立73个特别兴趣小组（SIG）](https://gitee.com/yuandj/siger/blob/master/%E7%AC%AC10%E6%9C%9F%20%E5%8D%97%E5%BE%81%E5%8C%97%E6%88%98%EF%BC%88%E4%B8%8B456%EF%BC%89.md#%E7%A4%BE%E5%8C%BA%E6%88%90%E7%AB%8B73%E4%B8%AA%E7%89%B9%E5%88%AB%E5%85%B4%E8%B6%A3%E5%B0%8F%E7%BB%84sig)
  - [17 sigs not in table:](https://gitee.com/yuandj/siger/blob/master/%E7%AC%AC10%E6%9C%9F%20%E5%8D%97%E5%BE%81%E5%8C%97%E6%88%98%EF%BC%88%E4%B8%8B456%EF%BC%89.md#17-sigs-not-in-table)
  - 大作业：[北向 sig-minzuchess 南向 sig-riscv](https://gitee.com/yuandj/siger/blob/master/%E7%AC%AC10%E6%9C%9F%20%E5%8D%97%E5%BE%81%E5%8C%97%E6%88%98%EF%BC%88%E4%B8%8B456%EF%BC%89.md#%E5%A4%A7%E4%BD%9C%E4%B8%9A%E5%8C%97%E5%90%91-sig-minzuchess-%E5%8D%97%E5%90%91-sig-riscv)
- 六、[用 RISC-V 赋予的全栈能力，打破条条框框，轻装前进！](https://gitee.com/yuandj/siger/blob/master/%E7%AC%AC10%E6%9C%9F%20%E5%8D%97%E5%BE%81%E5%8C%97%E6%88%98%EF%BC%88%E4%B8%8B456%EF%BC%89.md#%E5%85%AD%E7%94%A8-risc-v-%E8%B5%8B%E4%BA%88%E7%9A%84%E5%85%A8%E6%A0%88%E8%83%BD%E5%8A%9B%E6%89%93%E7%A0%B4%E6%9D%A1%E6%9D%A1%E6%A1%86%E6%A1%86%E8%BD%BB%E8%A3%85%E5%89%8D%E8%BF%9B)

---

## 四、[欧拉指北](https://images.gitee.com/uploads/images/2021/0508/102704_3fdc052f_5631341.jpeg) 从 0 到 10

[![输入图片说明](https://images.gitee.com/uploads/images/2021/0322/170937_a9e73951_5631341.png "屏幕截图.png")](https://gitee.com/flame-ai/siger/blob/master/%E7%AC%AC0%E6%9C%9F%20Hello,%20openEuler!%20%E6%8B%A5%E6%8A%B1%E6%9C%AA%E6%9D%A5.md) ![输入图片说明](https://images.gitee.com/uploads/images/2021/0509/005802_2ae2282a_5631341.png "屏幕截图.png")

Hello 欧来指北: [hello,openEuler!](https://gitee.com/flame-ai/hello-openEuler/issues/) 是 SIGer 编委会的创刊号，0 号期刊，它当时融入了渴望拥抱，以未来为题的意愿，本期回归 openEuler 的命名起点，数学家欧拉，也是以作业形式，回赠社区的行动。复古创刊时的笔体，也是清算本仓 issues 的行动啦。

早先转载过一篇 “[独家参访华为总部](https://gitee.com/yuandj/siger/issues/I37X3N)” 是征求过作者同意的。融入本期也是一个好归宿，尽管当时收录了它有些八卦之嫌。[拉片](https://gitee.com/flame-ai/hello-openEuler/issues/I3I5TZ) 的技能，同学们已经完美体验。这是沿袭了 [北电的风采](https://gitee.com/flame-ai/hello-openEuler/issues/I3I5TZ)。[轻量级 PR 的进阶技能](https://gitee.com/flame-ai/hello-openEuler/issues/I3CEQG)，是从 sig-RISC-V 的拥抱开始的，当然要收入本期。3个月前的[ “拥抱” 暗号](https://gitee.com/flame-ai/hello-openEuler/issues/I34VDZ)，本期封存，作为历史见证，留下一笔。关于 Wiki 图床问题的 “入门” 现在看来已经不是事儿了，这属于 Gitee 的顽疾，早已不是新人的我，已经开始用各种办法，挽留其他老师驻足 Gitee，尽管无果，就和 “敏感词” 这个顽疾一样，将一直伴随左右。

- 【华为】 #[I37X3N](https://gitee.com/yuandj/siger/issues/I37X3N) 独家参访华为总部：关于华为，你不知道的10件事儿
- 【技能】 #[I3I5TZ](https://gitee.com/flame-ai/hello-openEuler/issues/I3I5TZ) 【北电考研局】长腿学长在线拉片——手把手教你拉片子。 27天前
- 【进阶】 #[I3CEQG](https://gitee.com/flame-ai/hello-openEuler/issues/I3CEQG) 推荐使用【轻量级PR】增强社区参与感的好方法！
- 【拥抱】 #[I34VDZ](https://gitee.com/flame-ai/hello-openEuler/issues/I34VDZ) 本仓接受【拥抱】欢迎 SIGer 来访 :family: 3个月前
- 【入门】 #[I34VDX](https://gitee.com/flame-ai/hello-openEuler/issues/I34VDX) wiki - homepage - duoyibu-AI 403 error.

## 五、[SIG组生态树](https://gitee.com/flame-ai/hello-openEuler/issues/I2DRB3) 73 - 81 SIG 治理[地图](https://gitee.com/flame-ai/hello-openEuler/issues/I2DRB3#note_4787521_link) 生机盎然

这是 SIGer 创刊号 第 0 期 遗留的唯一一个开放的 ISSUE ，它是种子，才生发出了 “阳光海涛” 和 sig-RISC-V ，以及《南征北战》的宏大场景。下面是学习笔记 和 作业成果。

- [openEuler: One Innovation Platform for All](https://gitee.com/flame-ai/hello-openEuler/issues/I2DRB3#note_4702572_link) 提纲标题（[16页](https://gitee.com/flame-ai/hello-openEuler/issues/I2DRB3#note_4702827_link)）
- [Dev] [openEuler 21.03创新版本发布评审](https://gitee.com/flame-ai/hello-openEuler/issues/I2DRB3#note_4731158_link) ： 加强【南向生态】多硬件平台兼容能力
- 《南征北战》故事要点：场景（[7个](https://gitee.com/flame-ai/hello-openEuler/issues/I2DRB3#note_4740353_link)）
- P4 以开放的形态进行治理. SIG 治理[地图](https://gitee.com/flame-ai/hello-openEuler/issues/I2DRB3#note_4787521_link)
- 阳光海涛：[工作日志](https://gitee.com/flame-ai/hello-openEuler/issues/I2DRB3#note_5014425_link)
- 《能在社区传播的路上有个我》的[邀约](https://gitee.com/flame-ai/hello-openEuler/issues/I2DRB3#note_5015041_link)

### 社区成立73个特别兴趣小组（SIG）

 **社区生态进展** 

- L: 代码仓管理/技术创新 
- R: 社区治理/社区开发
- 软件包：6914 Fork：14109

 **R: 社区治理/社区开发** 

1. 社区生态进展 (R)

| | |
|---|---|
| Compatibility | Marketing |

2. 安全委员会/技术委员会/社区秘书处 (R)

| | | |
|---|---|---|
| Security-committee | TC | Community |

3. 版本发行相关 (R)

| | | |
|---|---|---|
| release | QA | doc |

4. 软件包管理相关 (R)

| | | |
|---|---|---|
| Packaging | recycle | EasyLife |

5. 构建系统/工具/依赖 (R)

| | | |
|---|---|---|
| buildsystem | OS-Builder | bootstrap |

6. 社区基础设施 (R)

| | | |
|---|---|---|
| Infrastructure | CICD | Gatekeeper |

 **L: 代码仓管理/技术创新** 

1. 云原生基础设施 (L)

| | | | | | | |
|---|---|---|---|---|---|---|
| Kubernetes | openstack | OKD | oVirt | Virt | iSulad | Container |

2. 中间件 (L)

| | | | | | | | |
|---|---|---|---|---|---|---|---|
| android-middleware | cms | wine | ROS | Ha | DB | ai-bigdata | ceph |

3. 桌面/图形系统 (L)

| | | | | | | | | |
|---|---|---|---|---|---|---|---|---|
| KIRAN-DESKTOP | desktop-apps | Desktop | KDE | DDE | xfce | UKUI | mate-desktop | GNOME |

4. 工具链/语言/运行时 (L)

| | | | | | |
|---|---|---|---|---|---|
| Application | System-tool | python-modules | perl-modules | Runtime | libboundscheck |
| Compiler | Programming-language | ruby | golang | Java | nodejs |

5. 基础功能/特性/工具 (L)

| | | | |
|---|---|---|---|
| high-performance-network | confidential-computing | A-Tune | Base-service |
| Computing | Storage | Networking | security-facility |

6. 架构/处理器/内核/驱动 (L)

| | | | | | |
|---|---|---|---|---|---|
| RaspberryPi | RISC-V | kernel | kae | REDF | DyscheOS |

 **软件包：6914 Fork：14109** 

| | | | | | | |
|---|---|---|---|---|---|---|
| 扩展包 | 标准包 | 基础包 | 内核 | 基础包 | 标准包  | 扩展包 |

从上游社区 -> 集成、依赖关系建立 -> 最终用户场景 -- 形成闭环 已经实现部分开源软件的有序管理

![输入图片说明](https://images.gitee.com/uploads/images/2021/0409/005116_45ff3523_5631341.png "屏幕截图.png")

- 以开放的形态进行治理
  （SIG 组是社区资源的代名词，其生态树是体现社区生命力的象征。）
- 软件供应链的管理：从最终业务场景到组件原生社区的闭环

- sig/sigs.yaml 中新增的

### 17 sigs not in table:

- dev-utils (8.)
- Others (17.)
- Private (20.)
- sig-aarch32 (29.)
- sig-bio (32.)
- sig-CloudNative (36.)
- sig-Compatibility-Infra (39.)
- sig-compliance (40.)
- sig-embedded (46.)
- sig-industrial-control (51.)
- sig-KubeSphere (55.)
-  **sig-minzuchess (58.)** 
- sig-ONL (61.)
- sig-OpenResty (62.)
- sig-ops (64.)
- sig-Ostree-Assembly (66.)
- sig-Rust (77.)
- sig-WayCa (80.)

### 大作业：北向 sig-minzuchess 南向 sig-riscv

完成 SIG 治理地图后 sig-minzuchess 的位置清晰可见，于 sig-risc-v 的结合成为社区活力的典范。

sig- **[minzuchess](https://gitee.com/blesschess)**  
-  **[LuckyStar](https://gitee.com/blesschess/LuckyLudii)** 
- [Shiliu  **Pi**  99](https://gitee.com/shiliupi/shiliupi99)

> 剧情：晚上，桃村的村民们正忙着给子弟兵们准备粮食和物资。在某村民家的院子里，村民们都在忙着碾麦子，筛面粉。机枪手刘永贵坐在妻子旁边抱着已经熟睡的儿子，妻子在一旁一边筛着面粉，一边对他唠叨着。  
> 永贵嫂：你走你的，我可不能拦你，我一个人跟村长上山打游击，不用你操心，孩子你可要带走。  
> 刘永贵：可你那光说气话，你说哪有野战军带着孩子打仗的？  
> 永贵嫂（偷偷地笑了笑）：你们跑东跑西倒利索，家就扔下不管了？  
> 剧情：这时，小姑子二嫚端着簸箕走了过来，准备舀一些面粉。  
> 二 嫚：嫂子！别难为大哥了，JIANG介石来了，大不了再上山去打两年游击。

[![输入图片说明](https://images.gitee.com/uploads/images/2021/0408/225701_5b9e1d6f_5631341.png "在这里输入图片标题")](https://gitee.com/flame-ai/hello-openEuler/issues/I2DRB3#note_4740353_link)

- （[上集](http://blog.sina.com.cn/s/blog_e781b75e01030767.html)）: 由于部队待命准备打狙击，机枪手和妻子小聚，但也只能是在乡亲们抓紧时间补充给养做好后勤。对话中，这份军民一心默契的情景就是必胜的先决条件。敌人来了就上山打游击，这是何等丰富的斗争经验啊，正呼应了前文老百姓放下好日子干革命的决心和信心。尽管有小牢骚，但依然是思想行动听指挥的。

> 剧情：在大沙河的拦水坝上，敌人工兵正在准备...  
> 剧情：敌人准备炸坝的行动，被隐蔽在附近山上的张玉敏及民兵所发现。  
> 张玉敏：敌人要炸坝放水。同志们！咱们一定要想法保住水坝，让咱们主力渡过沙河消灭敌人，保卫咱们老百姓的庄稼地。  
> 剧情：张玉敏说罢，便带领着民兵们朝山下走去。

[![输入图片说明](https://images.gitee.com/uploads/images/2021/0408/232840_4ac23726_5631341.png "在这里输入图片标题")](https://gitee.com/flame-ai/hello-openEuler/issues/I2DRB3#note_4740353_link)

- （[下集](http://blog.sina.com.cn/s/blog_e781b75e01030781.html)） : 炸水坝是敌人的备选手段，是阻止我军行军速度和增援的。被民兵游击队识破，成了落空。这是军民一心，心领神会的战术体会。和前面的高营长的思想工作形成了呼应，这也是我军的优良传统，军民一体的游击斗争经验的体现。我军主力赶上，吃掉整个敌军，并最终取得了全面胜利。

人民的战争必须由全体人民团结起来才能取得最终的胜利，社区生态则需要每一个细胞的活力才能生机盎然。

## 六、[用 RISC-V 赋予的全栈能力，打破条条框框，轻装前进！](https://gitee.com/yuandj/siger/blob/master/%E4%B8%93%E8%AE%BF/%E3%80%90%E8%A3%B4%E5%96%9C%E9%BE%99%E3%80%91%E7%94%A8%20RISC-V%20%E8%B5%8B%E4%BA%88%E7%9A%84%E5%85%A8%E6%A0%88%E8%83%BD%E5%8A%9B%EF%BC%8C%E6%89%93%E7%A0%B4%E6%9D%A1%E6%9D%A1%E6%A1%86%E6%A1%86%EF%BC%8C%E8%BD%BB%E8%A3%85%E5%89%8D%E8%BF%9B.md)

> 一位好的老师是值得同学们追随的，今天的专访 “裴喜龙” 老师不只满足了同学们的求知欲，更为我们描绘了一幅清晰可触的科技蓝图。如标题所展示的，这是一个见证历史的机会。接下来我将与同学们一起聆听裴老师真知灼见。此时此刻，我依然难掩[三个月前](https://gitee.com/yuandj/siger/issues/I37ZOL)收到裴老师的来访时的激动，当时阅读他的微博就已经成为了他的粉丝，今天能近距离聆听他的演讲，激动心情溢于言表！（@[袁德俊](https://gitee.com/yuandj)）

#[I37ZOL](https://gitee.com/yuandj/siger/issues/I37ZOL) 【欧来】新康众开源 openEuler on RISC-V
- 采访提纲：https://gitee.com/yuandj/siger/issues/I37ZOL#note_5031825_link

  1，开源社区是什么时候进入的？您提到的华为的曹璟博士业余时间维护COMO，以及后面提到的CAR有什么关联？

  2，对资源进行抽象，无论是SOA、SaaS把程序抽象为服务，还是共享动态链接库，还是微软COM技术，还是Java ClassPath技术等等，这些操作系统与程序设计技术。都是过去式，在今天的借鉴意义？和RISCV操作系统的关联又是怎样的？

  3，感谢您的科普，“余承东在中国信息化百人会2020年峰会上称，”这是您的引用。让我对南向北向的概念更加清晰，也是初识 openEuler 的一个困惑，这次斗胆撰写《南征北战》专题，既是我的作业，也是为同学们更全面的介绍整个社区的架构。就 openEuler 社区和您的操作系统工作，南向的内容多些？还是北向的多些？在群里，您这样介绍，“从北向（应用层）出发，经操作系统，往下看 RISC-V”，是一个怎样的工作形式？是从教学方面入手吗？

  4，《​[过程记录：编译 RISC-V 工具链](https://mp.weixin.qq.com/s/B1ZJen6K8af6eevIy46Y-A)》BalanceTWK 来一颗糖
《[硅谷教父John Hennessy：我们正站在计算机架构第五时代的门槛上](https://cloud.tencent.com/developer/article/1744249)》作者 | 蒋宝尚
我转载了您的推荐，作为本期RISCV专题的科普文章。RV4KIDS 是 SIGer 期刊策划的一个 RISCV 主题的系列科普活动，不是希望同学们也能加入造芯计划（期待他们成长后可以），而是从科普的角度，让他们能够知道并建立一个完整的知识地图，借用 RISCV 为载体，能够从按键的那一刻起，了解计算机系统的所有细节，知道通过自己的努力和学习，实现了然于胸。这个目标可能有点儿大，但对于求知若渴的年轻人，一点儿都不远，RISCV给了他们或者说给了我们希望。您是否可以对青少年，暂且设定为“高中生”推荐一些内容吗？

  5，《[九章不是计算机，](https://weibo.com/ttarticle/p/show?id=2309404598051649224908)》是非常热的量子计算的范畴，惭愧地讲看着有些吃力。就计算机科学从科普的角度，无论是外延，还是深入，也希望能听到您的意见和建议。应该或者如何？推荐那些内容给青少年。这也是 SIGer 编辑部，最希望获得的支持了。

  6，节前，邀请您希望对《硬件技术基础》做一些课程建设的意见交换，如果方便可以约个时间，我希望我们的同学可以旁听！不知是否可能？《计算机系统结构实验指导》

> 剧情：在敌指挥部的会议室里，敌军某上将正在召开军事作战会议，各将领在吞云吐雾中纷纷的议论着。这时，某上将用笔敲了敲面前的茶杯，各将领都安静了下来。这时，敌军南线总指挥张军长立正站了起来，说着自己的想法。  
> ...  
> 李军长：对，你说得很对！对历史教训我们谁也不会忘记。但是，知己知彼，方能百战百胜，这是军事上最普通的常识。  
> 张军长（一脸凶相地站了起来）：是的！在决定D国命运的时候，有人不顾大局，有意保存自己实力，这种常识在军事上也许是没有的。  
> 剧情：张军长说罢一拍桌子便坐了下来，参会将领们对张军长的发言都面面相觑，并保持着沉默，李军长则摇着头无奈地苦笑着。这时，一名少将站了起来。  
> 某少将：我来说两句，我来说两句。D国的利益是高于一切的，所以我们必须同心协力的与 **GONG** 军作战。


[![输入图片说明](https://images.gitee.com/uploads/images/2021/0408/231156_4171f7bf_5631341.png "在这里输入图片标题")](https://gitee.com/flame-ai/hello-openEuler/issues/I2DRB3#note_4740353_link)

- （[中集](http://blog.sina.com.cn/s/blog_e781b75e0103076j.html)） ： 这是典型的各自为战勾心斗角的场景，为之后我军大踏步穿插分割包围，各个歼灭的战略战术奠定了基础。敌人永远是高估自己的实力，遇到问题首尾不能相顾，为最终的败局埋下了伏笔。

> 剧情：在南线敌指挥所，参谋长正在用无线话筒与凤凰山的北线李部联系，张军长焦急地站在一旁。  
> 参谋长：喂！102，102。喂！你们怎么样？你们怎么样啦？  
> （102）：我们已经很困难了，我们已经很困难了，请你们顾全大局，赶紧向凤凰山靠拢，赶紧向凤凰山靠拢。  
> 剧情：张军长从参谋长手中拿过话筒。  
> 张军长：请你们坚持最后五分钟，请你们坚持最后五分钟！

[![输入图片说明](https://images.gitee.com/uploads/images/2021/0408/232824_ed2d7434_5631341.png "在这里输入图片标题")](https://gitee.com/flame-ai/hello-openEuler/issues/I2DRB3#note_4740353_link)

- （[下集](http://blog.sina.com.cn/s/blog_e781b75e01030781.html)） : 凤凰山决胜局就是摩天岭阻击战，最抱怨的一营，被留下来保存体力打狙击，超过汽车轮子提前占领摩天岭狙击阵地，一个营狙击一个军的敌人。敌人居然让被围的敌军向他们靠拢。直到听到被围之敌的军部传来缴枪不杀，才知道战机已失。

[![输入图片说明](https://images.gitee.com/uploads/images/2021/0401/162650_64374ee3_5631341.png "在这里输入图片标题")](https://gitee.com/openeuler/RISC-V/issues/I3CEEF#note_4726513_link)

胜利是影片的高潮，也是历史的分水岭，值得见证的时刻。据说影片的总顾问都是当年实战的将军，只有经历过风雨，才能高度统一地完成战略反攻，并取得最后的胜利，而轻装上阵多么重要，解放思想更是重中之重啦。

2021-5-9 09:38:33  
@[袁德俊](https://gitee.com/yuandj)  
[SIGer](https://gitee.com/flame-ai/siger) 编委会
