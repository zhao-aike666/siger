- [比亚迪全球总部——六角大楼，科技与创新的象征](http://mp.weixin.qq.com/s?__biz=Mzg2NjA5OTE5Nw==&mid=2247510580&idx=1&sn=f94b42ab65f8b87e1e75dc979f8ad3dc)
- [少年强则国强•走进BYD体验汽车魅力 — 为孩子种下振兴民族自主品牌的种子](http://mp.weixin.qq.com/s?__biz=MjM5NDMxNDI5NA==&mid=2651633340&idx=3&sn=23e28ed3ea656a8852e17fc72424800d)

<p><img width="706px" src="https://images.gitee.com/uploads/images/2022/0614/021620_e404c6d2_5631341.jpeg" title="byd副本.jpg"></p>

- [王传福，一个传奇要诞生！](https://view.inews.qq.com/wxn/20220609V05M4800)
- [巴菲特曾坐在比亚迪车里哭过](http://mp.weixin.qq.com/s?__biz=MzAwODQ3NzIwMQ==&mid=402063972&idx=3&sn=a80dbc174e3039f82deb6ed9a81ec524)
- [“2025年达到万亿营收”，比亚迪正悄悄征服全世界！](http://mp.weixin.qq.com/s?__biz=MjM5ODk4NTUzNg==&mid=2664572480&idx=1&sn=75cf3e362fc11b2ff20b838681c649e5)
- [比亚迪2021校招外语/金融/计算机/工程/交通运输等专业](http://mp.weixin.qq.com/s?__biz=MzAxMDE2NjUxMw==&mid=2652226148&idx=6&sn=751f9f0c2093199f78c69056c5005633)
- [一线 | 近交远攻，比亚迪缘何退出储能竞标绞肉场？](http://mp.weixin.qq.com/s?__biz=MzU3NzU4OTc3MA==&mid=2247483902&idx=1&sn=34eba13a4ecc7f19869bfb24a9d02060)

> 这是一个大大的广告，用两篇全文转载，表达对BYD的致敬，民族品牌钢钢的。这颗种子在我心里是种下啦。盖高楼的想法从火焰棋塔的 “装置艺术” 开始就已经在我的心里啦。当时看到电厂的大烟囱我就想象着有朝一日能变成一座座电视塔一样的大棋子。当看到六角大厦的时候，我知道了，这才是丰碑一样的存在。我不认为这和五角大楼有什么关系。重温了BYD混动方案押宝正确的视频后，我对王传福先生的眼界更加敬佩，刚刚的“氢春号·新能源”频道，才是澎湃动力的所在。人们普遍还是需要看到了才相信，而我希望 SIGer 的同学们成为，因为相信，所以见到的人。这个植入，应该说是 FLAMEOI 在蹭热度。希望有朝一日，能展示1面千幅海报的海报墙给王传福先生，当面感谢他，对 FLAMEOI 和我本人的激励。

（文末是FLAMEOI品牌演进的笔记，还不是终稿，它还在自由生长，因为我心中相信，一片崭新的开源世界，一定更加绚烂，一定和现在大家看到的不同。）

# [比亚迪全球总部——六角大楼，科技与创新的象征](https://mp.weixin.qq.com/s?__biz=Mzg2NjA5OTE5Nw==&mid=2247510580&idx=1&sn=f94b42ab65f8b87e1e75dc979f8ad3dc)

原创 GA编辑部 GA环球建筑 2021-09-26 21:55

![输入图片说明](https://images.gitee.com/uploads/images/2022/0614/113333_51b3a10d_5631341.png "屏幕截图.png")



依托于政策面的支持、成熟的汽车电子、新能源供给链条和工程师红利，中国的汽车企业在这一轮产业变革中走到了世界前列，创立于1995年的比亚迪是其中的佼佼者，至今市值最高触及9000亿元，超越了上汽、广汽、通用、宝马、奔驰……



作为新能源汽车的标杆企业，其位于深圳坪山的全球总部——六角大楼，同样具有标志性。



![输入图片说明](https://images.gitee.com/uploads/images/2022/0614/113344_c58ab249_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0614/113353_eac2ca3b_5631341.png "屏幕截图.png")



与大多企业热衷于兴建超高层总部不同的是，“六角大楼”是一种沿地面蔓延的大型建筑，而非向天空蔓延。建筑形似美国国防部的五角大楼，共有六个外立面，分为六层，从空中俯瞰，建筑呈正六边形，造型体现秩序、协调、对称的设计语言。



![输入图片说明](https://images.gitee.com/uploads/images/2022/0614/113653_23897c76_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0614/113412_abebf7e2_5631341.png "屏幕截图.png")



“六角大楼”一词不仅仅代表这座建筑本身，它也是比亚迪科技与创新的象征。作为比亚迪的研发中心和管理中枢，大楼周边环绕着实验室、厂房、汽车测试中心。



![输入图片说明](https://images.gitee.com/uploads/images/2022/0614/113422_47ee05c2_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0614/113432_e9f2af4f_5631341.png "屏幕截图.png")



六角大楼内建有全球最大的“专利墙”，墙上布满了比亚迪数万项专利中甄选出来一千多份专利证书。六角大楼外，最新研发的云轨与之融为一体。“技术为王，创新为本”的发展理念在六角大楼上被加以强调。



### 镜头中的“六角大楼”



![输入图片说明](https://images.gitee.com/uploads/images/2022/0614/113449_503f1c18_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0614/113454_087c5559_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0614/113459_9cd0d5e9_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0614/113511_b56e9d70_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0614/113517_c6a8585d_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0614/113522_aa3a2736_5631341.png "屏幕截图.png")

[43 x 11 + 17 x 31 = 1000](https://gitee.com/yuandj/siger/issues/I4UFQY#note_12354436_link)

![输入图片说明](https://foruda.gitee.com/images/1660438751674351873/屏幕截图.png "屏幕截图.png")

# [少年强则国强•走进BYD体验汽车魅力 — 为孩子种下振兴民族自主品牌的种子](https://mp.weixin.qq.com/s?__biz=MjM5NDMxNDI5NA==&mid=2651633340&idx=3&sn=23e28ed3ea656a8852e17fc72424800d)

原创 深实幼SZSY 刘凌幼教名师工作室 2018-12-03 12:27

![输入图片说明](https://foruda.gitee.com/images/1666290759046273692/e475ec75_5631341.png "屏幕截图")

- 微信号：刘凌幼教名师工作室 liulingworks
- 功能介绍：本工作室是深圳实验幼儿园对外宣传平台！汇聚了国内幼儿教育顶尖名教团队，本着沟通、共享和互相学习的理念，打造幼教共同交流平台，为家长提供科学、实用的育儿知识，携手更多幼教工作者，为孩子种下一生幸福的种子！

2018年11月29日上午阳光灿烂，深圳实验幼儿园百花部中班组全体小朋友在年级长李玲老师及各班老师的组织下，在教研室老师们的支持下，包含15位助教、摄影师黄老师及医护黄医生等一百多人的队伍，开开心心的出发了，本次活动目的地——BYD，让小朋友们深度体验国产汽车的魅力！

![输入图片说明](https://foruda.gitee.com/images/1666290792996745125/09a46a6a_5631341.png "屏幕截图")

<p><img width="49%" src="https://foruda.gitee.com/images/1666290799202842343/7b7f5dfb_5631341.png"> <img width="49%" src="https://foruda.gitee.com/images/1666290804094125038/6a3fcf6d_5631341.png"></p>

比亚迪成立于1995年2月，是一家横跨电子、汽车、新能源和轨道交通四大产业，并在香港和深圳两地上市的高新技术企业。

比亚迪的英文名为BYD，是BuildYourDreams的首字母缩写，意思是成就梦想。公司的使命是：用技术创新满足人们对美好生活的向往。

BYD集团员工超过24万人，新能源车全球运营足迹遍布50多个国家和地区，300多个城市。在全球拥有30多个工业园，实现全球了六大洲的战略布局。总部位于深圳坪山也是我们本次的参观地点，占地219万平方米，总部大楼名为六角大楼。

比亚迪是目前世界上极少数能同时掌握电池、电机、电控以及整车技术的车企，在2017年实现全球新能源车年销量三连冠，国内四连冠。截至今年10月份，比亚迪的新能源总产销超过了45万辆。大家在深圳出行见到的新能源公交车、大巴车、出租车，都是由比亚迪生产制造。

![输入图片说明](https://foruda.gitee.com/images/1666290861595651398/3d7cf410_5631341.png "屏幕截图")

今天我们行程之一是体验比亚迪新能源车以及其搭载的DiLink系统（小迪），和小迪对话、K歌、互动，体验智能汽车如何改变生活。在互动环节，小朋友们开心的体验了遥控驾驶功能，内心满满的成就感！心中的小问号不断的产生，耐心的技术工程师哥哥们，一一为孩子们进行操作、解答，让孩子们感受到，原来，科技的运用能够给人们的生活带来如此的快乐！

![输入图片说明](https://foruda.gitee.com/images/1666290875412835135/f99ccc5a_5631341.png "屏幕截图")

<p><img width="49%" src="https://foruda.gitee.com/images/1666290883457196987/0bf96a50_5631341.png"> <img width="49%" src="https://foruda.gitee.com/images/1666290886223361034/4f1919ee_5631341.png"></p>

![输入图片说明](https://foruda.gitee.com/images/1666290925205522008/abdca9a2_5631341.png "屏幕截图")

行程二我们来到比亚迪总装工厂，参观一条可满足传统燃油汽车、插电式混动汽车、纯电动汽车的先进生产线，来到产线孩子们惊喜的小眼睛四处环望，全程用心感受着庞大的机器设备的工作，见证了一辆辆汽车是如何的诞生，用“哇”的感叹表示着不可思议的见证！

![输入图片说明](https://foruda.gitee.com/images/1666290934632943455/939cb367_5631341.png "屏幕截图")

<p><img width="49%" src="https://foruda.gitee.com/images/1666290940687803422/d98b5ba3_5631341.png"> <img width="49%" src="https://foruda.gitee.com/images/1666290946483325239/618586f0_5631341.png"></p>

行程三我们来体验比亚迪云轨——作为绿色交通整体解决方案的引领者，比亚迪成功推出了具有完全自主知识产权的跨坐式单轨产品—云轨。目前已经获得了国内外十余城市的订单。孩子们即好奇又兴奋的搭乘了云轨，体验了空中绿色出行，在空中游览比亚迪工业园区。真正感受到了科技为人类的生活带了便利，科技的伟大在于创造，只有想不到，没有做不到！

![输入图片说明](https://foruda.gitee.com/images/1666290989773943360/f92f0956_5631341.png "屏幕截图")

<p><img width="49%" src="https://foruda.gitee.com/images/1666290993003817857/87e01b99_5631341.png"> <img width="49%" src="https://foruda.gitee.com/images/1666290998209154290/125d33d7_5631341.png"></p>

在云轨展示区的儿童互动游戏区域，为了迎接孩子们的到来，工作人员组成的DIY团队用心准备与互动，带领孩子们DIY了自己独一无二的画作胸章，吃了香香甜甜的爆米花、与小鸭子人偶姐姐进行了亲密的互动，让孩子们的社会实践增添了趣味性，体验了创作的快乐！

![输入图片说明](https://foruda.gitee.com/images/1666291036266915025/eef29ca0_5631341.png "屏幕截图")

<p><img width="49%" src="https://foruda.gitee.com/images/1666291045652152070/c2474244_5631341.png"> <img width="49%" src="https://foruda.gitee.com/images/1666291051246938667/cd7e58c2_5631341.png"></p>

全部行程结束后，我们来到了比亚迪的总部餐厅用午餐，感谢颜和平妈妈精心为小朋友和大朋友准备了符合孩子们口味的营养午餐，小朋友们吃的香喷喷哒！还体验了特别的工作餐，孩子们的文明用餐礼仪、独立自主的生活习惯得到了充分的展示，赢得了叔叔阿姨们的称赞，在进餐的途中，主动与进餐的叔叔阿姨打招呼，自信大方的中班孩子们也是棒棒哒！

![输入图片说明](https://foruda.gitee.com/images/1666291087410096871/3cfa60db_5631341.png "屏幕截图")

<p><img width="49%" src="https://foruda.gitee.com/images/1666291094922496195/87e0cdb5_5631341.png"> <img width="49%" src="https://foruda.gitee.com/images/1666291100623269101/2a279058_5631341.png"></p>

午餐结束后，BYD体验活动结束了，非常感谢中三班颜和平妈妈为我们策划了这么精彩开心的社会体验活动，在暖暖的阳光中我们乘车返回幼儿园，在车上孩子们进入了甜蜜的梦想，梦里云轨开到了哪里呢？

少年强则国强，走进BYD体验国产汽车魅力，为孩子种下了振兴民族自主品牌的种子。这是一次非常有意义的社会实践活动！


撰稿人：中三班徐语嫣妈妈

摄影：小涛老师

![输入图片说明](https://foruda.gitee.com/images/1666291148861076113/9dbc31bb_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1666291158658333187/3b370f19_5631341.png "屏幕截图")

---

# [FLAME? to FlameOI](https://gitee.com/yuandj/siger/issues/I4UFQY)

### 该问题是怎么引起的？

AI(Artificial Intelligence  
OI(Open Intelligence)  
Not only Open Source  
FLAME ?  
flame what?  
flame any.  
Flame AI  
Flame OI  
Flame Studio  
Flame OI  
Flame Oi

### Open Intelligence | Application & Database Development …
https://www.openintelligence.com

Open Intelligence is an information technology and business solutions firm specializing in application and database development for the NIH scientific research community. The leadership and developers at Open Intelligence offer a combined experience of over 30 years of contracting service to the Federal Government.

The English name is OpenIntelligence, or OpenI for short. The platform aims to promote open source, open and collaborative innovation in the field of artificial intelligence, build OpenI technology chain, innovation chain and ecological chain, promote the healthy and rapid development of artificial intelligence industry ...

- OpenI - A New Generation of Open Source Platform for ...  
  en.openi.org.cn/

### Flame OI studio 火焰棋开放智能实验室

Aigames.tech  
hongyisoft.cc

at flame-ai.gitee.io/aigames  
at flame-ai.gitee.io/hongyisoft

以下个性域名已经被使用：

- aigames.gitee.io
- hongyisoft.gitee.io
- hongyi.gitee.io

http://thcgc.gitee.io/  
http://thcgc.gitee.io/hongyisoft  
forked from flame-ai.gitee.io/aigames

### .OI [各个国家顶级域名](https://gitee.com/yuandj/siger/issues/I4UFQY#note_9266202_link)

- [如何实现 .oi 域名？](https://gitee.com/yuandj/siger/issues/I4UFQY#note_9266522_link)

  - 通过浏览器
  - 自假设 DNS 服务器
  - ...

- [DNSAgent配置实现自定义DNS及域名解析](https://blog.csdn.net/hbh112233abc/article/details/105425806)（[笔记](https://gitee.com/yuandj/siger/issues/I4UFQY#note_9266675_link)）

### [SIGN OF FLAMEOI?](https://gitee.com/yuandj/siger/issues/I4UFQY#note_10811681_link)

六边形图腾已经在 oi 的方案中体现了，出现在校园版海报中！

- 自绘海报，曾经OpenEuler2022开发者日的舞台搭建为背景，未得
- DIY标识，可以用废旧的计算机主板切割
- 化学棋子。

DIY标识，可以用废旧的计算机主板切割
- 废主板装饰

  ![输入图片说明](https://images.gitee.com/uploads/images/2022/0608/232609_00e357fa_5631341.png "屏幕截图.png")  
  ![输入图片说明](https://images.gitee.com/uploads/images/2022/0608/232632_9833303e_5631341.png "屏幕截图.png")

### 六边形图腾

<p><img height="99px" = src="https://images.gitee.com/uploads/images/2022/0608/233839_7da92b4d_5631341.png"> <img height="99px" = src="https://images.gitee.com/uploads/images/2022/0608/234025_5d985d93_5631341.png"> <img height="99px" = src="https://images.gitee.com/uploads/images/2022/0608/234038_f8fd103d_5631341.png"> <img height="99px" = src="https://images.gitee.com/uploads/images/2022/0608/234055_7f1c1ee0_5631341.png"> <img height="99px" = src="https://images.gitee.com/uploads/images/2022/0608/234118_e6647804_5631341.png"> <img height="99px" = src="https://images.gitee.com/uploads/images/2022/0608/234152_f0f2ba6b_5631341.png"> <img height="99px" = src="https://images.gitee.com/uploads/images/2022/0608/234246_c256df91_5631341.png"> <img height="99px" = src="https://images.gitee.com/uploads/images/2022/0608/234427_09e6e057_5631341.png"> <img height="99px" = src="https://images.gitee.com/uploads/images/2022/0608/234545_6d18c92c_5631341.png"> <img height="99px" = src="https://images.gitee.com/uploads/images/2022/0608/234553_2d714617_5631341.png"> <img height="99px" = src="https://images.gitee.com/uploads/images/2022/0608/234608_28837d4b_5631341.png"> <img height="99px" = src="https://images.gitee.com/uploads/images/2022/0608/234628_cf39dee0_5631341.png"> <img height="99px" = src="https://images.gitee.com/uploads/images/2022/0608/234642_84f6626a_5631341.png"> <img height="99px" = src="https://images.gitee.com/uploads/images/2022/0608/234823_491e9f53_5631341.png"> <img height="99px" = src="https://images.gitee.com/uploads/images/2022/0608/234851_1744ec92_5631341.png"> <img height="99px" = src="https://images.gitee.com/uploads/images/2022/0608/234711_e3d4d7f5_5631341.png"> <img height="99px" = src="https://images.gitee.com/uploads/images/2022/0608/234719_45f581a6_5631341.png"> <img height="99px" = src="https://images.gitee.com/uploads/images/2022/0608/234727_206f192b_5631341.png"> <img height="99px" = src="https://images.gitee.com/uploads/images/2022/0608/234745_6ac1c153_5631341.png"> <img height="99px" = src="https://images.gitee.com/uploads/images/2022/0608/234809_61c635b2_5631341.png"> <img height="99px" = src="https://images.gitee.com/uploads/images/2022/0610/010446_ed9d376a_5631341.png"></p>

### [瑞塑 REPLAS](https://gitee.com/yuandj/siger/issues/I5E28Y#note_13819030_link)

- [研究这种微生物的降解原理，进一步破解常温下的高分子聚合物的自然降解机理](https://gitee.com/yuandj/siger/issues/I5E28Y#note_13819040_link)

  - [吃塑料的微生物…塑料垃圾那么多，赶紧喂啊](https://gitee.com/yuandj/siger/issues/I5E28Y#note_13819043_link)

  - [【青岛日报】可以“吃”塑料的海洋微生物在青岛发现](https://gitee.com/yuandj/siger/issues/I4UFQY#note_10811888_link)

### 火焰棋心高度

FLAMEAI -> FLAMEOI

- TOOLS 导入内容，辅助聚合
- 频道为分类期刊
- SIGER 主编原创学习笔记
- GPT-3 封面辅助生成原图，
- 摄影，插画频道提供生图
- 项目制 PBL 实践，产出原创笔记
- 大使导师制传承衣钵

石榴派

- 国内版，增加 
  - MD 自动索引功能，
  - 自主机器搜索背景
- 国际版，产出外交
  - 民族棋中国史
  - SHILIUPI OS