在团队程序开发过程中，常常用到版本控制工具，常见的有两种：git和svn。其中git凭借着其种种优势成为主流，git操作与Github使用也成为了程序工作者必备技能。

在开源圈子，常常听到Pull Request这个词，如果你还不了解Pull Request的含义与功能，请看一下这篇文章，我认为写得通俗易懂：

https://zhuanlan.zhihu.com/p/347918608

这篇文章的作者在文章最后说：

> 如果让我来给 Github 取名字，我可能会取：push request 推请求, merge request 合并请求

在你知晓Pull Request的功能后，可能也会有类似的想法。所以到底为什么是Pull而不是Push呢？我们以fork工作流为基础来解释。


可以看到，在fork工作流中，小明想贡献代码，必须先对smileArchitect的JavaMap仓库进行fork操作，创建属于小明的JavaMap仓库，然后修改本地仓库，进行代码更改。

代码更改完毕以后，小明能直接push到smileArchitect/JavaMap吗？——当然不能！为什么？因为小明不是smileArchitect/JavaMap的owner，也就意味着没有该仓库的push权限。

那小明还想为该仓库贡献代码，怎么办？答案是请求smileArchitect/JavaMap 的所有者smileArchitect去拉取小明/JavaMap，也就是open一个pull request。

所以，核心的逻辑是：只有仓库的owner才能进行push操作，非仓库owner只能请求仓库owner拉取他们的仓库。

当然了，命名不是最重要的，能理解PR是在做一件什么事情才是核心要务。

以上仅个人理解，水平有限，如果哪里不对请批评指正。