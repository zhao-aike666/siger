- ChatGPT 2个月月活用户破亿！
- [MIND | 什么是最好的学习方式？Learn in Public！](https://mixshare.zhubai.love/posts/2235071106099585024)
- [有人说ChatGPT有物理学博士水平？我们的测试结果令人……](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&mid=2651262034&idx=1&sn=7e9adc8397db602d38f229dd048223d8)

<p><img width="706px" src="https://foruda.gitee.com/images/1678301379080012258/4d6804fa_5631341.jpeg" title="582ChatGPT副本.jpg"></p>

> 当所有人开始刷 ChatGPT 的时候，段子也开始不胫而走了，我选择了几篇有代表性的文章，表达我对它的基本观点。首先它的革命性在于它的量到质的变化，海量的数据，巨大的算力，以及 Sample TO USE 的 UI，都支持了这个革命性的产品征服所有人的眼球，各种蹭流量就可以理解了。现在新段子越来越少了，在这时发声不会要引人注意。在所有人的期盼中，它被玩坏了吗？答案肯定是否定的，她的智能表现是人类个体的想象，它还在哪里不会因为我们的使用而停止它的 “进化”，甚至这个词来形容都未必准确，因为我们并不能完全了解他，驾驭它呢？我不发表意见，或许科幻作品中的担忧和描写，现在看来愈发真实，是现实的。

另外两篇分别是 教育创新者的拥抱，以及物理所的代表人类考官的检测。先说后者，它不是用来测试的，测不测意义不大，文章的蹭热点嫌疑很大，我特地附上了历史文章，没有一篇不是标题醒目的，我差点就用 “标题党” 来形容了，但看行文的严谨，确实有学术的做派和文风，我们经常描写 ChatGPT 一本正经的 “胡说八道”，哈哈有没有类比呢？这肯定是玩笑了。至少从物理所，挖掘出了4本经典力学课程，是可以给同学们阅读的。再说 教育创新，或许只有 AI 才能突破人类的极限，去迭代出人类期待的高度吧。我选这篇文章的核心，还是在呼应我的观点，为 “开源” 站台。和作者的观点一致的是，要想改变老师们的固有观念太难了，AI 或许可以成为一个外力。

> 整体看下来，它是一个先进的工具，我们应该拥抱它，后面几期将会以实践的角度来反映它对我们学习工作的促进作用，至于科学伦理和技术危机这些议题，自有“砖家”评点。我也会专门撰写一篇，更详细的文章，介绍如何使用它来促进学习社区的发展。预报到此告一段落，上文章。

# ChatGPT 2个月月活用户破亿！

<p><img width="30%" src="https://foruda.gitee.com/images/1678294017915421219/253e929f_5631341.png"> <img width="30%" src="https://foruda.gitee.com/images/1678294034128351377/9d0e6d27_5631341.png"> <img width="30%" src="https://foruda.gitee.com/images/1678294040298012563/b5593aad_5631341.png"> <img width="30%" src="https://foruda.gitee.com/images/1678294045691560847/777ecf53_5631341.png"> <img width="30%" src="https://foruda.gitee.com/images/1678294051304775887/4e210cb3_5631341.png"> <img width="30%" src="https://foruda.gitee.com/images/1678294057466783984/55e96b4f_5631341.png"> <img width="30%" src="https://foruda.gitee.com/images/1678294061882418110/a5160143_5631341.png"> <img width="30%" src="https://foruda.gitee.com/images/1678294066853930611/c6918da8_5631341.png"> <img width="30%" src="https://foruda.gitee.com/images/1678294071656882155/2216c1e5_5631341.png"></p>

刚刚火爆全球的聊天机器人chat gpt又创造了一个奇迹，他在短短两个月的时间越过用户已经超过1亿了，而且他刚刚拿了微软100亿美金的投资。这世界的背后逻辑是什么样的？我是三手机器人潘博士在硅谷给大家做第一手的解读，你可以先点赞、收藏和关注起来。chat gpt背后的公司是open AI open AI成立于八年前，它其实是一个非盈利组织，它建立的初衷实际上是希望人工智能不要被这几个大公司所掌控。他几个创始人和投资人都非常有名，其中一个就是伊朗马斯马斯特在Stanford只待了几天就辍学了，所以很难说是我们Stanford校友，但open AI的CEO，这个天才少年奥特曼确实是Stanford，不过他也是中间辍学了，他后来也成为硅谷最有名的初创公司富华英外，Comed later的CEO，他是犹太人。

那这次拿微软的100亿美金，实际上是奥特曼的一盘大棋。首先open AI还是非常花钱的，它有将近400个资深的人工智能的专家，光是公司一年就要花掉2亿美金。另外，训练界大语言模型非常划算力，所以在公司成立的这八年里，他们已经是花掉了40亿美金。你可能想不到，他们最早的应用实际上是做一个机器人来复原魔方，八年里边他们试了各种各样的应用，但都没有成功，这要是换在国内，VC早都撤资了。我们这次实际上是做了一个天才的对赌，他答应以后赚钱，首先是让微软还有VC回本，之后是让他们拿到十倍的回报，但拿到十倍的回报之后，公司就完全变回是open AI的了。所以这个如果不成功，那也是拿到了100亿美金的投资，但如果是成功了的话，那公司的股份就全部收回了。

很天才，那这个超级牛的聊天机器人是怎么实现的？它实际上是通过三步来实现，第一步，就是用一个可监督的模型来训练对话，这里，它用了互联网上各种各样的问答型的网站里边的内容进行训练，就是类似中文里边知乎这样的网站里边的内容。第二步，是用加强训练来优化它的这个模型，就是由真人提问，这个聊天机器人会给出三个答案，然后由真人来选出最好的答案，这个就跟围棋里边的阿尔法克。他的这个训练方法是一样的，那第三步，是在在这个上面再做一些微调，整个的这个步骤听起来并不复杂，但是他的训练需要大量的数据，同时耗费的算力也相当惊人，那这个，就是我们前面所说的，是由微软的云服务来提供的，同时也需要大量的人工来帮助他做加强学习，趁GPC的出现确实可能有划时代的意义，微软就准备把它整合到自己的搜索引擎里。

别人来对抗谷歌的搜索，因为我们80%的搜索都是在问一个问题，这些问题，Chat gpt都可以给我们直接的答案。由于chat gpt的火爆，谷歌跟百度也会在最近推出他们相应的服务，但关键是搜索原有的竞争排名的商业模式就不成立了。另外，Chat g pt还会颠覆很多其他的行业，因为它可以帮你写论文，回答你的问题，而且还可以帮你编程，甚至还可以帮你找程序中的bug。有了chat g pt。不只是中学生的家教，应该不需要了，而且一般的程序员也不需要了，他的出现一定会带来新一波的信息革命，我预测也会有大量的围绕柴的G。

# [MIND | 什么是最好的学习方式？Learn in Public！](https://mixshare.zhubai.love/posts/2235071106099585024)

理想觅学 · 02-08
> 本文分享一些对于学习的思考和观点，以及相应的论据说明。

## 什么是"最好"？

首先我们定义“最好”指的是对个人学习潜能发掘到超越自己预期并过渡到匹配的状态，并非是与大规模样本甚至所有人的横向比较。所以因人而异，无放之四海的标准。

基于以上前提，我们聊一聊为什么"learn in Public"可以算是"最好"的学习方式。

设想一个场景：

> 最近开学了，同学们要提交寒假作业给老师检查。在这样的状态下，想必同学们的心态就有滋味百般了：早就完成的，自信满满，昂首挺胸，等待老师检阅；还在赶工期的，畏畏缩缩，闪烁其词，开始编造各种理由："作业丢了/被狗狗吃了/忘在外婆家了......"，懊悔早知如此不如平时每天都完成一点，那就不会拖延到最后压力山大了！

为什么会这样呢？其实"拖延症"是人的本性，心理学上早有研究，如果仅仅依靠个人意志力来坚持，并不容易长久，那如何避免滑入窘境？对应的有效做法是：

 **公开你的学习过程和进度，让更多人看见或者监督。更进一步，将自己所学的知识分享出去，教会别人弄懂，你的收获将会超出预期，成就感和自信心也会油然而生。** 

这其实可以用一句话来精简概括，即是：

 **_Learn in public with open mind，do as Feynman do._** 

## What's "Learn in public"？

"Learn in Public"指的是公开你的学习，是个人发展和教育学习领域的一个不断被广泛采用的趋势，个人通过博客、社交媒体、播客等各种平台，向他人分享他们的学习历程和成果。是一种记录自己成长的方法，也能获得其他人的反馈和支持，并帮助到对类似主题感兴趣的人。

通过公开分享你的目标和进展，你更有可能坚持你的学习计划，避免分心。这是因为你受到了你的关注者的问责，他们期望看到不断的进展。如果你是一个动机不足或很难找到时间实现学习目标的人，这尤其有用。

同时，当你分享你的作品时，你暴露在批评和建议下，真实的反馈可以帮助你更好地提高。还可以从正在进行类似主题学习的人那里获得鼓励和支持，让你感到不那么孤独，更有动力继续学习。

## Who's Feynman? "费曼学习法"是什么？

Feynman指的是诺贝尔物理学奖获得者理查德·费曼。他是20世纪伟大的物理学家，他主要从事粒子物理学和量子力学研究。他发明了费曼图，这是一种表示粒子间相互作用的图解方法。他也对原子核物理学、相对论和引力波等方面做出了重要贡献，是科学界的重量级人物。

费曼学习法（Feynman Technique）是以费曼的名字命名的一种学习方法，虽然Feynman本人并不知道，而是后人假托其名称，但费曼本人的成长历程和学习过程，如果你看过他的采访，尤其是童年的成长过程，你会发现他确实一直都是这样践行的。

"费曼学习法"网络上公认的过程是这样的：

![输入图片说明](https://foruda.gitee.com/images/1678296085800824023/6521a8ef_5631341.png "屏幕截图")


- **Write：选择一个正在学习的知识概念** 

  - 拿出一张白纸，在上方写下你想要学习的主题。想一下，如果你要把它教给一个孩子，你会讲哪些，并写下来。这里你的教授对象不是你自己那些聪明的成年朋友，而是一个8岁的孩子，他的辞汇量和注意力刚好能够理解基本概念和关系。
  - 许多人会倾向于使用复杂的辞汇和行话来掩盖他们不明白的东西。问题是我们只在糊弄自己，因为我们不知道自己也不明白。另外，使用行话会隐藏周围人对我们的误解。
  - 当你自始至终都用孩子可以理解的简单的语言写出一个想法（提示：只用最常见的单词），那么你便迫使自己在更深层次上理解了该概念，并简化了观点之间的关系和联系。如果你努力，就会清楚地知道自己在哪里还有不明白的地方。这种紧张状态很好——预示着学习的机会到来了。

- **Explain 解释** 

  - 在上一步中，你不可避免地会卡壳，梳理一下试着向别人来解释你的理解。
  - 别人的反馈相当宝贵，因为对方会告诉你有没有听得懂。这意味着你自己有没有掌握清晰。懂得自己能力的界限也是一种能力，你刚刚就确定了一个！
  - 这是学习开始的地方。现在你知道自己在哪里卡住了，那么就回到原始材料，重新学习，直到你可以用基本的术语解释这一概念。
  - 认定自己知识的界限，会限制你可能犯的错误，并且在应用该知识时，可以增加成功的几率。

- **Analyse 分析** 

  - 现在你手上有一套自己手写笔记，检查一下确保自己没有从原材料中借用任何行话。将这些笔记用简单的语言组织成一个流畅的故事。
  - 将这个故事大声读出来，如果这些解释不够简单，或者听起来比较混乱，很好，这意味着你想要理解该领域，还需要做一些工作。

- **Repeat 重复以上过程** 

  - 重复以上过程，对不流畅的环节查漏补缺，这样你会不断加深学习的效果。

以上环节中，尤其是Explain（解释）环节，你会发现它其实是"Learn in Public"（公开你的学习）的延伸，因为你需要将你所学的知识公开分享给他人，这也就意味着你的自尊心促使你提前就准备地更加充分。因为通过分享学习过程，个人能够更深入地理解和掌握所学内容，并通过解释他们的想法和思路，将其与他人分享。同时，通过与他人的互动，能够得到关于所学内容的更多见解和反馈，使学习的过程更加有效。

这样的反馈机制也会促进你对事物概念了解地更加透彻，外在表现为你很好地掌握了知识。

其实很多像费曼这样在各行各业有成就、卓越优秀的人也都在无意间遵循了这样的规律，虽然他可能可能并不清楚这居然被人总结成一套方法体系。

因此，"Learn in Public"是费曼学习法的实践，并帮助人们更好地理解那些待学习应用的新知识和需要巩固复习的旧知识。

## 为什么要"OpenSource"开源开放？

"Learn in Public"与开源思想"OpenSource"有着密切的关系。开源思想强调通过共享代码和知识，使社区能够共同改进和发展技术。而 "Learn in Public"也是这一理念在学习领域的体现，通过将学习过程公开，不仅仅是在自身得到提高，同时也为他人提供了帮助和启发。此外， "Learn in Public"还能够通过与他人的互动和沟通，促进社区的共同学习和成长。因此， "Learn in Public"是开源思想的具体实践，它帮助人们充分发挥开源思想的价值，并通过共同学习和探索，实现技术的进步。

近期大火的ChatGPT是有openAI这个公司开发的，它使用的语言模型是GPT-3.5，而openAI在GPT-3第三代之前都是开源的，所以也正是这样的机制和胸怀，为ChatGPT后续的大规模传播奠定了一定的基础，目前市面上还有很多“套壳”的问答类机器人也几乎都是基于openAI过去的成果来二次开发的。不管未来openAI是否闭源，它前期的开源工作绝对是它日后爆发的重要条件之一。

我们暂时把视线从宏观技术未来"独角兽"身上转移到我们每个个体。我们可以回想自己上学时候的情景，每个班上都有学习超级拔尖的同学，他们往往是最乐于帮助别人的，愿意分享他的学习方法或者笔记，因为越是毫无保留地传授给别人，最大的受益者其实是自己。

那肯定有人会说，你毫无保留地分享自己的”独门绝技“，不担心被别人给超越吗？如果你是这样的读者，我可能要挑战你的认知了。

其实我们每一个人在知识的摄取方面很难有所谓的”秘笈“。我们根本不可能占有知识，我们是知识的传播载体，离开了传播的链条，即便你学富五车才高八斗却没有给真实的社会和其他人施加影响，最后可能的结局就是”烂在肚子里“，这又有什么意义呢？

碳基躯壳最终都会消失腐朽，如何能够在力所能及的范围内传播让更多人得以弥合认知上的鸿沟冰受益，你以为自己损失掉了什么吗？

No！你的收获超过你的预期，因为当你分享的时候，你和其他学习者就构成了一个协作的网络关系，而你的收获则是边际递增的，对号入座一下数学经典的”握手问题“。

知识会不断迭代，就像喂给ChatGPT的数据一样，短短80天，它早已今非昔比。类似地效果，处于网络中的协作关系越来越紧密，形成了一个更加"健壮而可持续的"的学习成长支持系统。这系统本身才是最大的价值和学习的奥义啊，就像ChatGPT的人机对话响应机制一样，智能、快速，且超预期。

## 一句话总结我的观点

> 最好的学习方式就是专注聚焦、持续输出并教会他人，知识越开源、心态越开放，越深度参与，收获也越大，不管是授方还是受方，大家都处于一张不断迭代的网络上。所以，你首先需要解构的是观念认知，然后再强化底层逻辑。勤思考、多创造、多分享，拥抱每一个主动纳入到网状成长支持系统的学习者和价值链接者，这才是意义的价值和价值的意义。

 **Learn in Public with Open Mind, do as Feynman do.** 

本文部分灵感受启发于《[Learn in Public](https://www.swyx.io/learn-in-public)》，[ChatGPT](https://mixshare.zhubai.love/posts/2233492008050302976)对本文内容亦有贡献。

![](https://foruda.gitee.com/images/1678296272694447358/02b47982_5631341.png "屏幕截图")

## 延展阅读：

 **[觅学札记根目录（建议收藏）](https://mixshare.zhubai.love/posts/2233624167427964928)** 

[ChatGPT专页 | 觅学札记 ](https://mixshare.zhubai.love/posts/2233492008050302976)

[TΞCH/AGI/AIGC专页 | 觅学札记 ](https://mixshare.zhubai.love/posts/2233596835791212544)

[思维认知方法工具 | 觅学札记](https://mixshare.zhubai.love/posts/2233594131740205056)

[教育思考与实践 | 觅学札记](https://mixshare.zhubai.love/posts/2233595253427109888)

---

本文来自

> **觅学札记** 

> 聚焦学习成长与教育探索，分享学习方法与实践经验，寻觅成长进化的最优路径。在见贤思齐反求诸己的路上持续发光，点亮他人，看见自己。 关注我，让我们一起换个视角看万千世界和斗转星移，在浩瀚无垠的灿烂星河里留下点滴存在的足迹。

> —— 由 理想觅学 创作

# [有人说ChatGPT有物理学博士水平？我们的测试结果令人……](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&mid=2651262034&idx=1&sn=7e9adc8397db602d38f229dd048223d8)

原创 just_iu 中科院物理所 2023-02-12 10:57 发表于北京

![输入图片说明](https://foruda.gitee.com/images/1678296597842217826/517ff5dc_5631341.png "屏幕截图")

- 微信号：中科院物理所 cas-iop
- 功能介绍：物理所科研动态和综合新闻；物理学前沿和科学传播。

![输入图片说明](https://foruda.gitee.com/images/1678296655753754100/d6f2d831_5631341.gif "640.gif")

大家最近肯定被ChatGPT刷屏了，好像它 **神通广大** ， **无所不能** ...

![输入图片说明](https://foruda.gitee.com/images/1678296696901534116/d60a4bda_5631341.png "屏幕截图")

但，无所谓， **物理学** 会出手。今天就让我们来测测它 **物理知识** 的素养吧！

## 1 ChatGPT的介绍

ChatGPT (全称:Chat Generative Pre-trained Transformer ) 是由 **OpenAI** 开发的一个人工智能聊天机器人程序，于2022年11月推出。该程序使用基于GPT-3.5架构的 **大型语言模型并通过强化学习进行训练** 。

ChatGPT目前仍以文字方式互动，而除了 **可以通过人类自然对话方式进行交互** ，还可以用于相对复杂的语言工作，包括 **自动文本生成、自动问答、自动摘要** 等在内的多种任务。如：在自动文本生成方面，ChatGPT可以根据输入的文本自动生成类似的文本（剧本、歌曲、企划等），在自动问答方面，ChatGPT可以根据输入的问题自动生成答案。还具有 **编写和调试计算机程序** 的能力。

![输入图片说明](https://foruda.gitee.com/images/1678296807451536710/59b78316_5631341.png "屏幕截图")

> ChatGPT图

哎，让它自己做个 **自我介绍** 。

![输入图片说明](https://foruda.gitee.com/images/1678296826521879062/0e216ee1_5631341.png "屏幕截图")

那么，简单的介绍做完了，那么就正式进入到我们的物理考试时间吧。

## 2 物理知识考核

咱们从简单到复杂，慢慢的测试它~

### 初中物理知识：

![输入图片说明](https://foruda.gitee.com/images/1678296849030540232/f91da648_5631341.png "屏幕截图")

刚开始就错了，看来ChatGPT也不行啊（图片，正确答案应该是： **大小，方向和作用点** 。让我们继续。

![输入图片说明](https://foruda.gitee.com/images/1678296868214950508/78ea2dfe_5631341.png "屏幕截图")

这个回答差强人意，不过这两个问题说明，它的 **初中知识储备** 还可以。那么知识的 **使用能力** 呢?

![输入图片说明](https://foruda.gitee.com/images/1678296889193370552/2267b88e_5631341.png "屏幕截图")

一个选择问题和一个计算问题也算是回答上来了。看来初中的知识难不倒它。

### 高中物理知识：

升级难度，那么现在就进入高中知识阶段。

![输入图片说明](https://foruda.gitee.com/images/1678296916261831114/28ec3950_5631341.png "屏幕截图")

还是先试一试它的基础知识储备，可以这没问题。但到了高中， **公式推导能力** 就应该提高了,看看它行不行。

![输入图片说明](https://foruda.gitee.com/images/1678296930534116754/3e4ea75a_5631341.png "屏幕截图")

再看看它的回答。

![输入图片说明](https://foruda.gitee.com/images/1678296947194240821/a75731ec_5631341.png "屏幕截图")

只能说还是不尽人意（忽略小编因为马虎而造成的错别字），它最后没有回答正确。

正确答案应该是：

![输入图片说明](https://foruda.gitee.com/images/1678296959544627307/4f26f24a_5631341.png "屏幕截图")

再来看看它的电磁学知识。

![输入图片说明](https://foruda.gitee.com/images/1678296974649919342/85c35649_5631341.png "屏幕截图")

继续问问它。

![输入图片说明](https://foruda.gitee.com/images/1678296989372876890/ecfa8a3d_5631341.png "屏幕截图")

再追问一下。

![输入图片说明](https://foruda.gitee.com/images/1678296997061044999/44803c21_5631341.png "屏幕截图")

还行，看来它还是有点逻辑思维。

那么就继续上强度吧！

### 大学物理知识：

对于物理专业的课程大家可能不是很了解，但肯定都有所耳闻。那小编就给大家稍微总结介绍一下一名 **物理专业的大学生要学习什么物理专业课程** ~

 **力学，光学，电磁学，热学** ：这四门课程也叫做“普通物理学”，俗称普物，这四门课程是承接高中初中所学的知识进行一定的扩展，相当于入门知识。

四大力学（可不是四种力学）： **理论力学，电动力学，量子力学以及统计力学** 。这四门课程对于物理学专业的本科生可谓是重中之重，借用我老师的一句“玩笑话”：如果一个搞物理的人不能搞清楚四大力学，那么就不要说自己是搞物理的了。

![输入图片说明](https://foruda.gitee.com/images/1678297075210402779/204f941c_5631341.png "屏幕截图")

> 四大力学教学书

其实这里还有一个很特殊的课程： **原子物理** 。它就像是承接普物与四大力学之间的桥梁，所以在大学课程中一直保留着。

再后面就会 **根据你的研究方向和兴趣来针对学习** ，比如：凝聚态专业就会选择固体物理和半导体物理等...

好的废话说完了，进入主题。

![输入图片说明](https://foruda.gitee.com/images/1678297117032498501/ed7178bc_5631341.png "屏幕截图")

哈哈哈，失败，它应该是不知道拉格朗日方程的物理含义。你也忘了？来看看这个：[掌控所有运动规律的原理：最小作用量原理](https://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&mid=2651077777&idx=1&sn=05eafcea7c45922f44c8dabb1206fd4b&scene=21#wechat_redirect)。（看完你就比Chat GPT强了 ），继续。

![输入图片说明](https://foruda.gitee.com/images/1678297150209466678/9715d7aa_5631341.png "屏幕截图")

咱们转换一下。

![输入图片说明](https://foruda.gitee.com/images/1678297155848994128/458a1bd8_5631341.png "屏幕截图")

嗯，写的代码是正确的，算它知道。

![输入图片说明](https://foruda.gitee.com/images/1678297170166581613/cda7aebd_5631341.png "屏幕截图")

emmm，格式不太对，但是它描述的还是比较准确的。正确答案应该是

![输入图片说明](https://foruda.gitee.com/images/1678297174947247234/3e23ee0a_5631341.png "屏幕截图")

最后问他一个问题。

![输入图片说明](https://foruda.gitee.com/images/1678297184398139927/e1c2e1ac_5631341.png "屏幕截图")

它写的是 **真空有源下的麦克斯韦方程组** ，没有问题。洛伦兹规范是电动力学中的经典问题，大家可以参考曹则贤老师的演讲：[曹则贤 | 电磁学/电动力学：现象、技术与思想（下）](https://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&mid=2651241491&idx=2&sn=2298c0b4532e965c51ca283ff89711b4&scene=21#wechat_redirect)，小编就不在这里赘述了。

好吧，考试到此结束，小编对它的评价是： **有点眼高手低，人云亦云** 。它因为庞大的数据集，物理知识储备是非常多的，远超一般人所能容纳的知识量，但是它的逻辑能力和辨别能力表现却没有那么完美。

## 3 “学习”能力考核

前面可以看到，虽然它的逻辑很差，但是知识水平不错，那么它的学习能力呢？我们来调戏测试一下。

![输入图片说明](https://foruda.gitee.com/images/1678297271803819645/5ecdaf10_5631341.png "屏幕截图")

好像它真的明白了，我们再换个人名问一下。

![输入图片说明](https://foruda.gitee.com/images/1678297279450108251/e5449430_5631341.png "屏幕截图")

好像它明白了，但明白的不多，咱们换个数学题。

![输入图片说明](https://foruda.gitee.com/images/1678297289248377255/de46e80a_5631341.png "屏幕截图")

我们稍微把题目换一下。

![输入图片说明](https://foruda.gitee.com/images/1678297298162738708/7e395e15_5631341.png "屏幕截图")

小编崩溃它刚开始还是算错了~ 但是它还是很明白计算过程。总的来说，它有学习能力，但是目前来看不多。

注：小编的测试过程是连续的，在同一个窗口进行的。

## 4 小花絮

![输入图片说明](https://foruda.gitee.com/images/1678297313302514646/6a4ce300_5631341.png "屏幕截图")

给它点个赞。

![输入图片说明](https://foruda.gitee.com/images/1678297334613130306/b674cee1_5631341.png "屏幕截图")

小编不说别的，你们应该懂了吧。快来！[《科学公开课》缤纷世界主题课](https://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&mid=2651253557&idx=2&sn=ba299bd496646d7dee432a0b614a699f&scene=21#wechat_redirect)。

##  总结

不得不说，Chat GPT给我们带来了很大的惊喜： **它确实能通过人类自然对话方式进行交互，并可以用于自动文本，代码生成等相对复杂的语言工作** 。但是它还是有很大的局限性， **对于数学计算，脑筋急转弯等需要极强逻辑的领域，会频频犯错** 。并且它有时会一本正经的给出它所谓的正确答案，这对非专业领域的人非常难于辨别（你觉着它的薛定谔方程写的正确吗？），总之， **目前的它是一个不错的工具，但是绝对不要过度依赖它** ！

![输入图片说明](https://foruda.gitee.com/images/1678297395137758557/2938079d_5631341.png "屏幕截图")

### 参考资料：

1. https://zh.wikipedia.org/zh-hans/ChatGPT
2. https://openai.com/blog/chatgpt/

 **编辑：just_iu** 

### 近期热门文章Top10

↓ 点击标题即可查看 ↓

1. 物理博士生看《流浪地球2》是什么体验？
2. 这次「症状重的人」和「症状轻的人」，谁的免疫系统更强？
3. 建造《流浪地球2》的太空电梯，总共分几步？
4. 如果你知道兔兔吃这些，还会觉得ta可爱吗
5. 三体人为什么那么害怕地球人的加速器？
6. 没有电池，机械表如何准确走时？（上）
7. 爱因斯坦直呼难解的方程，被他在战壕里解出来了
8. 兔兔很可爱，但加点糖更……（深夜勿看）
9. 怎么戴口罩，才能既安全又不勒耳朵？
10. 曹则贤 | 电磁学/电动力学：现象、技术与思想（上） | 中国科学院2023跨年科学演讲

[点此查看以往全部热门文章](https://shimo.im/docs/ne3VVl6Y2zh8FB3b/read)

![输入图片说明](https://foruda.gitee.com/images/1678297476266305843/89b137aa_5631341.png "屏幕截图")

## [线上科学日](https://mp.weixin.qq.com/mp/appmsgalbum?__biz=MzAwNTA5NTYxOA==&action=getalbum&album_id=1570417592350064641)

| # | title | date |
|---|---|---|
| 189 | [有人说ChatGPT有物理学博士水平？我们的测试结果令人……](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2651262034&amp;idx=1&amp;sn=7e9adc8397db602d38f229dd048223d8) | 2023-2-12 |
| 188 | [如何用物理的方法制作一个千层蛋糕？](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2651254028&amp;idx=1&amp;sn=5bcdb11b591ee882b94bf2096c19b29f) | 2023-1-30 |
| 187 | [建造《流浪地球2》的太空电梯，总共分几步？](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2651252408&amp;idx=1&amp;sn=829230c4748879c94129b55385f02738) | 2023-1-24 |
| 186 | [物理博士生看《流浪地球2》是什么体验？](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2651252377&amp;idx=1&amp;sn=1441a2cb009b50128b291d069810d465) | 2023-1-23 |
| 185 | [除了泥头车和二向箔，去二次元的路还有……](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2651249392&amp;idx=1&amp;sn=ab112b3fa6d1969247747b9f597f97f1) | 2023-1-15 |
| 184 | [世界杯决赛在即，来点足球发疯文学？](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2651224739&amp;idx=1&amp;sn=19c941037065e117d2af9081171f7c45) | 2022-12-18 |
| 183 | [三体动画竟以“古筝计划”开头，真的有如此强悍的纳米丝么？](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2651222179&amp;idx=1&amp;sn=74f0e0f6df7718bb87d4a04cc06e9da2) | 2022-12-11 |
| 182 | [隐藏在身边的怪盗，不知不觉中"偷走"了我们的“私密信息”？](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2651218822&amp;idx=1&amp;sn=eb462d33606fbf58b8cb8b7f64af1f03) | 2022-12-5 |
| 181 | [三体开播前，帮三体人找找稳定解](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2651218153&amp;idx=1&amp;sn=56452e41e88fbd025b675a5751a443c4) | 2022-12-3 |
| 180 | [卡塔尔这么热，花式降温法了解一下？](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2651214937&amp;idx=1&amp;sn=9fa3ccee321ee23ef4e8e55271b2a5ce) | 2022-11-27 |
| 179 | [动物们为了干饭能有多努力？它们甚至去学了量子力学！](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2651208715&amp;idx=1&amp;sn=b35dc8eeb38618edc5168e85713cf767) | 2022-11-20 |
| 178 | [这难道不是蜜蜂吗？还真不是……](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2651206566&amp;idx=1&amp;sn=32a6113614c80e39136908c7f43ac0d4) | 2022-11-13 |
| 177 | [冰凉的手和烫伤的嘴，抱着保温杯的我想入非非……](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2651202649&amp;idx=1&amp;sn=acda4a612ee45eb0821685435dd2a7e4) | 2022-11-6 |
| 176 | [万圣节还在cos传统鬼怪？来看看物理版的“妖魔鬼怪”！](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2651198324&amp;idx=1&amp;sn=b7225013b7e6404e4da70d7f14741b4d) | 2022-10-30 |
| 175 | [学物理可以让我们永远年轻吗？](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2651194423&amp;idx=1&amp;sn=a7d03ae9a4e28fd1e494e6edeadec61e) | 2022-10-23 |
| 174 | [哔！与刷卡机贴贴的付款卡经历了什么？](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2651190363&amp;idx=1&amp;sn=e1d320a43152c699d91065cbea1fdcb1) | 2022-10-17 |
| 173 | [如何让太阳从西边升起？](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2651186791&amp;idx=1&amp;sn=76bd74cc7766f55e218f8971023fe7f2) | 2022-10-10 |
| 172 | [神奇的秘密道具，让你走路玩手机再也不怕撞！](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2651186516&amp;idx=1&amp;sn=1e83c125547ba3b044ea23eac0a11c57) | 2022-10-9 |
| 171 | [历经1935960小时，我们破解了自行车平衡的奥秘](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2651181016&amp;idx=1&amp;sn=888bd9107ab9a91f3d7dcc3dbde165ba) | 2022-10-2 |
| 170 | [奥特曼可以靠打怪兽拿诺奖吗？](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2651179385&amp;idx=1&amp;sn=0b213a2910b35d6e8d1d52695dca9a0e) | 2022-9-25 |
| 169 | [胖子=表面张力大？瘦子=表面张力小？](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2651176647&amp;idx=1&amp;sn=0d8cc3e3343d70e68f89a457aeee5665) | 2022-9-17 |
| 168 | [今天，中秋节和教师节历史上首次相逢的背后，竟然……](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2651175393&amp;idx=1&amp;sn=a7682a8220d7d0e14cce81a297041731) | 2022-9-10 |
| 167 | [移不动联不通的地方也能通信？北斗短报文，你是我的神](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2651174010&amp;idx=1&amp;sn=37583342a7e18ed2f7bd95c7bf583c69) | 2022-9-6 |
| 166 | [《 褪 色 方 法 大 赏 》](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2651172225&amp;idx=1&amp;sn=b4939ec926dcddf06501c1c858e72037) | 2022-9-4 |
| 165 | [还在吹空调？进来学物理人的消暑方法！](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2651160158&amp;idx=1&amp;sn=aea73971afa917a2776bc0916652fe73) | 2022-8-7 |
| 164 | [《最伟大的作品》，但是中二所](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2651148389&amp;idx=1&amp;sn=c7ed89c4c6746a80586c27ec1b0f8432) | 2022-7-10 |
| 163 | [我成功帮助了便利贴“拒绝内卷”](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2651147002&amp;idx=1&amp;sn=e4d52b8c5d4737c31eb6702841aced63) | 2022-7-3 |
| 162 | [夏日夜宵的美味秘籍竟然是...](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2651134221&amp;idx=1&amp;sn=bffa8e2ced2f564dbc2860d2b22798a3) | 2022-6-12 |
| 161 | [流水的高考题，铁打的小滑块](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2651132547&amp;idx=1&amp;sn=a06c3b35855b8ffde8d01a78b236c022) | 2022-6-5 |
| 160 | [《关于物理所小编也很想拥有一只可达鸭这事》](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2651130370&amp;idx=1&amp;sn=8bff9b19a36c00ef41a9923129e98873) | 2022-5-29 |
| 159 | [快看！中二所独家520绝密“科学”送礼指南！](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2651129000&amp;idx=1&amp;sn=372ef2bd6c17abca6471219c0e758a9c) | 2022-5-20 |
| 158 | [未来战场的主角，背后的原理初中就学过？](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2651124812&amp;idx=1&amp;sn=b5c0d560af7f0f46be30393f8133b88d) | 2022-5-9 |
| 157 | [五一去哪玩？这次你不妨大胆些……](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2651120701&amp;idx=1&amp;sn=8d47e12a634823b82a1260de9b72f007) | 2022-5-1 |
| 156 | [令人匪夷所思的航天人生存技能，你能做到第几条？](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2651115142&amp;idx=1&amp;sn=efabdcfa40cb107132623b6d291dd9ee) | 2022-4-17 |
| 155 | [为什么我的微信运动步数总是不那么精准？](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2651113492&amp;idx=1&amp;sn=88de880dd36ab390a470c0488897bf80) | 2022-4-10 |
| 154 | [绝密资源：中二所上岸秘籍（24h删）](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2651111574&amp;idx=1&amp;sn=9e25a0563a817318abdd010e45ad73f9) | 2022-4-1 |
| 153 | [第二个黑匣子已经找到！黑匣子到底是什么？](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2651110709&amp;idx=1&amp;sn=42b7405fdefd4cc92622e7967d23404e) | 2022-3-27 |
| 152 | [“咔嚓”咬一口宝塔菜，背后分形的秘密竟然是...](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2651109871&amp;idx=1&amp;sn=995a98c22f781f99b3e9e72ad3dcd280) | 2022-3-20 |
| 151 | [一π多吃，看看你是哪种吃π方式？](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2651108343&amp;idx=1&amp;sn=f2028deac6e8aebf562edf593e0d6372) | 2022-3-14 |
| 150 | [牛顿在世，丘比特附身，《自然脱单的数学原理》震撼发布！](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2651095563&amp;idx=1&amp;sn=30772f3accdaaef00eff99fe84b67f7f) | 2022-2-14 |
| 149 | [看完《开端》，我发现了主角循环的秘密](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2651088474&amp;idx=1&amp;sn=5739ff63df66324b54a15487ebd06406) | 2022-1-30 |
| 148 | [快过年了，不准备点窗花都不能说自己学过物理了？！](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2651082521&amp;idx=1&amp;sn=c437b4883eec82e5466f375716ac3746) | 2022-1-16 |
| 147 | [走路的时候如何不碰到别人是一门深奥的学科](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2651067571&amp;idx=1&amp;sn=27143859ba895e575fd898fcec95ff53) | 2021-12-12 |
| 146 | [明明是“超声”清洗，咋还在“滋滋”作响？](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2651063647&amp;idx=1&amp;sn=17a3e3ba966e6b3f6be8290479fcf21b) | 2021-12-5 |
| 145 | [冬天正面穿，夏天反面穿，真有冬暖夏凉的衣服？](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2651058283&amp;idx=1&amp;sn=c6e5129855a7fb852636924b3b646ddf) | 2021-11-28 |
| 144 | [端着咖啡走路是一项鲜为人知的物理学壮举](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2651055909&amp;idx=1&amp;sn=b15327d7b7d1da7de5c6fd615f4974d3) | 2021-11-21 |
| 143 | [睁开眼外面变白了，然而我却陷入了思考：眼前的白真的是白吗？](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2651051568&amp;idx=1&amp;sn=97cb688ec6a35b9a4e7d3598b7595f59) | 2021-11-7 |
| 142 | [老司机都懂的x件事，一般人我不告诉他](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2651049385&amp;idx=1&amp;sn=5d27fe8ef91f85a04bc58f62bff4b467) | 2021-10-31 |
| 141 | [洗头冲水时冲下好些头发，我是要秃了吗？？？](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2651046132&amp;idx=1&amp;sn=de3b771eb5a2ab06d18af0188aaf3351) | 2021-10-24 |
| 140 | [电网系统里用不完的电都去哪儿了？很可能跑到这里了……](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2651040607&amp;idx=1&amp;sn=30bc444efa860a30d04cf8bcc784fde1) | 2021-10-10 |
| 139 | [藏在你身体里的“幻影”，竟然能用它看清……](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2651037678&amp;idx=1&amp;sn=b3156811e5018b8199df89c077a013bd) | 2021-9-29 |
| 138 | [想要提前知晓诺奖结果，首先你得会画画](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2651039077&amp;idx=1&amp;sn=3a3504ab9d075c3f56ffb6c41e4f079f) | 2021-10-3 |
| 137 | [历史上"消失"的日期：我消失了，但仿佛又没消失……](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2651037033&amp;idx=1&amp;sn=a4e5071531b1fa32d427323bc26509b0) | 2021-9-22 |
| 136 | [徒手画圆算什么？我家老师还会……](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2651033888&amp;idx=1&amp;sn=e6bf85e4fbaeaa2ce47aaa5fd214405f) | 2021-9-10 |
| 135 | [其实童年的你离造出火箭只有亿点点距离](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2651031172&amp;idx=1&amp;sn=2fcc53b07d42fbe148d8be393176c632) | 2021-9-5 |
| 134 | [在？“天上飞”不如“轻功水上漂”？](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2651029939&amp;idx=1&amp;sn=c987cb2edeb6e481ee6605df535f06fc) | 2021-8-29 |
| 133 | [你可能想不到，紫外线其实也有温柔的一面……](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2651028101&amp;idx=1&amp;sn=9d03c8364351ade6c2e102439240b691) | 2021-8-22 |
| 132 | [七夕没人陪？我修炼了一手抓娃娃绝技](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2651025882&amp;idx=1&amp;sn=48c411f378a7503c47304d349eb0c6db) | 2021-8-15 |
| 131 | [要想水花压得好，你得……](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2651020690&amp;idx=1&amp;sn=cdae400c18a235e443852fdc85372a64) | 2021-8-1 |
| 130 | [芜湖~如何科学地起(shang)飞(tian)](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2651014299&amp;idx=1&amp;sn=4acd1148ac1ff54e739f0667e01f0a6f) | 2021-7-4 |
| 129 | [我们发电不用浆，全靠浪](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2651010960&amp;idx=1&amp;sn=716acfe4bcdae9bb9779ebb310333652) | 2021-6-20 |
| 128 | [眼会了手废了？有了它，脑子会了就行！](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2651006905&amp;idx=1&amp;sn=19e8bc9f635f9634f645fb76ab40652f) | 2021-6-6 |
| 127 | [莪們哋姃蒤是星辰大(zhong)海(cai)](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2651005742&amp;idx=1&amp;sn=050cd0ed364bafe3cf4c483377513119) | 2021-5-30 |
| 126 | [了 不 起 的 航 天 器](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650992676&amp;idx=1&amp;sn=4175fce3a869db085dd86733eabeca86) | 2021-4-24 |
| 125 | [核废料该如何处理，奥特曼发话了…](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650989661&amp;idx=1&amp;sn=3fd728257ab7d6f81a8228311c260c13) | 2021-4-18 |
| 124 | [为了搞清鸡肉的味道，科学家给鸡按摩，还做了全身体检](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650989661&amp;idx=2&amp;sn=39c0f9a6e9cb4ecb64184ad73382dc5a) | 2021-4-18 |
| 123 | [盲盒 究竟套牢了我们的哪根神经？](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650989661&amp;idx=3&amp;sn=60353e907973d363fd53c717756f8bdd) | 2021-4-18 |
| 122 | [这里有一份病毒排行榜，谁是第一？](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650989661&amp;idx=4&amp;sn=f26fe6122967ea01cde68bd81e881970) | 2021-4-18 |
| 121 | [今天没有推送，千万不要点开！](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650858725&amp;idx=1&amp;sn=ffbb8ff9bbfc93fe498039284760f1c3) | 2018-4-1 |
| 120 | [还记得愚人节的互动问答吗？今天要公布获奖名单啦！当然还有更重要的原理解析哦~](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650858866&amp;idx=1&amp;sn=d820585670b4821d1019dcc7a0b1b88d) | 2018-4-8 |
| 119 | [文科生看了会沉默，理科生看了会流泪！物理所『网红井盖』官方大揭秘！](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650859771&amp;idx=1&amp;sn=ea1bd61d62f353f993301a043bb81d8a) | 2018-5-19 |
| 118 | [9张图了解电池新星——钠离子电池](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650860463&amp;idx=1&amp;sn=4f19b8618136ae5fafa094702c597de8) | 2018-6-17 |
| 117 | [世界杯伪球迷实用装 X 指南](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650860696&amp;idx=1&amp;sn=916f337c22d6ddb09898d06f54005eeb) | 2018-7-1 |
| 116 | [眼前的黑不是黑，你说的白是什么白](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650860769&amp;idx=1&amp;sn=746ad2bff56aa7d09f2061d78961a73e) | 2018-7-8 |
| 115 | [吃瓜时节，我们用公式计算告诉你，如何科学地挑选西瓜！](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650861159&amp;idx=1&amp;sn=0aef2a1bef96649c5d08ac39d9948e12) | 2018-7-29 |
| 114 | [天气太热了，我妈妈在挑战能否用阳光煎鸡蛋](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650861341&amp;idx=1&amp;sn=b27034ba363ee466f9617d3e2943f572) | 2018-8-12 |
| 113 | [想不想知道物理学家们看书的方法？用过的都说好](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650861424&amp;idx=1&amp;sn=8759df021279dc377abd7e1daee35561) | 2018-8-19 |
| 112 | [这是 99.9% 的人没听说过的神奇悖论（上）](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650861528&amp;idx=1&amp;sn=dfbc3e859e029db0560e59b833908997) | 2018-8-26 |
| 111 | [这是 99.9% 的人没听说过的神奇悖论（下）](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650861629&amp;idx=1&amp;sn=5fe34ef6fa235afb706962c332dd3a47) | 2018-9-2 |
| 110 | [这是不用公式就能看懂的数学和物理，看过了的都说好（上）](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650861904&amp;idx=1&amp;sn=e8147ca060f6998ced8827a5df6dee93) | 2018-9-16 |
| 109 | [?不用公式就能看懂的数学和物理，这里有你没见过的操作（下）](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650862003&amp;idx=1&amp;sn=bfe6320477a81126d4a546076a2c9c01) | 2018-9-23 |
| 108 | [良心推荐丨 一份认真实用的国庆长假排队指南](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650862192&amp;idx=1&amp;sn=908ebcf3eb1dffb8a9da4909529cb7ad) | 2018-9-30 |
| 107 | [听说这个假期，你从人潮中幸存下来了……](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650862396&amp;idx=1&amp;sn=4dd39ca71f0b95c4b95a7dae433efe52) | 2018-10-7 |
| 106 | [二狗子，你怎么飘了……](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650862807&amp;idx=1&amp;sn=01d4e6840cca41f0481d9e522318c6af) | 2018-10-21 |
| 105 | [听说你转发了无数条锦鲤，却一次都没中……](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650863031&amp;idx=1&amp;sn=22d718ba4aaf07df27528108b9b465e4) | 2018-10-28 |
| 104 | [学物理的，随时随地都能把天聊死](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650863413&amp;idx=1&amp;sn=cb5e90dc119ec0dbf941ef799575d9f6) | 2018-11-11 |
| 103 | [这场意外里，安卓手机毫发无伤，苹果设备居然近乎全灭](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650863606&amp;idx=1&amp;sn=7f664c43e93f80bacd8574ab43f87983) | 2018-11-18 |
| 102 | [你现在还不知道的地铁上广告的原理，居然和两百年前的发明有关系](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650863807&amp;idx=1&amp;sn=b38f4e233699eb0b4b6bff587c07d38a) | 2018-11-25 |
| 101 | [假如有人告诉你这个世界的本质是弹簧，你愿意相信吗？](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650863930&amp;idx=1&amp;sn=121942a1204d6d1425769b095ff718d6) | 2018-12-2 |
| 100 | [从小到大只会做个纸飞机？关于折纸的「高端」技巧通通告诉你](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650864139&amp;idx=1&amp;sn=736dd823d8a1ab6575e2c437a7013804) | 2018-12-9 |
| 99 | [今天，我想跟你聊聊时间捡……简史](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650864319&amp;idx=1&amp;sn=2b3667de3e53f5f46587f0bba23affc6) | 2018-12-16 |
| 98 | [昨晚，我在秋名山输给了一个沙发……](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650864505&amp;idx=1&amp;sn=cb10b9ddb6849a135c1a9d2785c467d4) | 2018-12-23 |
| 97 | [这个冬天咋这么冷，为啥不能让我们好好跨个年](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650864727&amp;idx=1&amp;sn=a9e6fa122885c52008961d4656044a13) | 2018-12-31 |
| 96 | [这个冬天，盘什么最好玩？](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650865012&amp;idx=1&amp;sn=3d99f684b99b5464a26c608d8c24f226) | 2019-1-13 |
| 95 | [这些东西，看过的人都转疯了！](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650865172&amp;idx=1&amp;sn=8dc96a64d0e27ace1278b654359868d4) | 2019-1-20 |
| 94 | [你知道爱因斯坦人生中发表的第一篇论文是什么吗？](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650864847&amp;idx=1&amp;sn=de3138f3a6edc9ffdabb245616e54496) | 2019-1-6 |
| 93 | [做一条物理的「舔狗」](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650865390&amp;idx=1&amp;sn=1d6cbdd05487967c2ba95f5b6e010b5d) | 2019-1-27 |
| 92 | [怎么避免上厕所没有纸？看完这篇文章你就懂了](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650865904&amp;idx=1&amp;sn=f386fd9e03ea324105e0b5240ca66ed4) | 2019-2-17 |
| 91 | [给你一双「火眼金睛」，你想不想要？](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650865595&amp;idx=1&amp;sn=04bdd958353bc23039becdf565dde096) | 2019-2-3 |
| 90 | [这些小液滴，被科学家玩出了「花」](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650866282&amp;idx=1&amp;sn=70199666fda3e75551831a064087d474) | 2019-3-3 |
| 89 | [怎么用最正确的姿势说「我酸了」](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650866457&amp;idx=1&amp;sn=a606343bc79f51f95a7dd60bfb50c458) | 2019-3-10 |
| 88 | [所以，WiFi和4G到底哪个更耗电？](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650866617&amp;idx=1&amp;sn=52ada73d6b3d8d86e02fe495c9f72c9f) | 2019-3-17 |
| 87 | [传纸条被发现，一看竟写着...](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650866780&amp;idx=1&amp;sn=57d38ddb5676b472fe080c3fd6f0b328) | 2019-3-24 |
| 86 | [国产100 kWh的超大“充电宝”了解下？](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650866972&amp;idx=1&amp;sn=efc98235044a4beb5953cbed5c155f78) | 2019-3-31 |
| 85 | [都说现在算法很智能，为什么音乐软件还老给我推荐神曲](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650867135&amp;idx=1&amp;sn=d41d7664da782901a3cdd009c0d91f4d) | 2019-4-7 |
| 84 | [玩扫雷还有什么技巧？科学家的玩游戏方法你绝对想不到](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650867307&amp;idx=1&amp;sn=6ea25d356d13334e4701ee7c87ae5a4a) | 2019-4-14 |
| 83 | [和光有关的，五个你不可不知的秘密](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650867528&amp;idx=1&amp;sn=93b4af3abb5dd6606e9030a9bdbe9937) | 2019-4-21 |
| 82 | [我明明有那么多故事，为什么却只能当一个背景板...](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650868597&amp;idx=1&amp;sn=87b87c16a90a891c17318ab34de8fe21) | 2019-5-26 |
| 81 | [我怀疑你在开车，而且我有充足的证据](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650868755&amp;idx=1&amp;sn=dc15381c94c5381b520934e56840b45c) | 2019-6-2 |
| 80 | [听说高考数学全国卷三考了朵云……](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650868946&amp;idx=1&amp;sn=406a5f4a954f47ce8055d5c0fdb7cdae) | 2019-6-9 |
| 79 | [冬天穿衣服为了保暖，夏天穿衣服为了什么](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650869140&amp;idx=1&amp;sn=273f48125f7846132c3e5a033bf21a7f) | 2019-6-16 |
| 78 | [为了吃顿瓜，我们差点打起来了](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650869302&amp;idx=1&amp;sn=8458c9ebb644fff060b1521e4b2b5142) | 2019-6-23 |
| 77 | [那一夜，我向“蛐蛐儿”问起了温度](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650869495&amp;idx=1&amp;sn=175d3797583b218e0288b976a3771bed) | 2019-6-30 |
| 76 | [上完大学的我，终于成为了一名“技工”](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650869732&amp;idx=1&amp;sn=19d81559d28799445d982f22e1f7d545) | 2019-7-7 |
| 75 | [5G 来了，你离脱单还有多远？](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650869911&amp;idx=1&amp;sn=9d9abdf5ea16fad83ba58dc479a3af1a) | 2019-7-14 |
| 74 | [托马斯小火车的鸣笛声中，竟然隐藏着宇宙的秘密？](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650870170&amp;idx=1&amp;sn=10bbc123855f892d385be7db1124467f) | 2019-7-21 |
| 73 | [这是一篇理工男写的口红科普文](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650870345&amp;idx=1&amp;sn=17a843fff959d7e4aeae02042ed4d213) | 2019-7-28 |
| 72 | [有些人表面上是在“打水漂”，背地里却……](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650870568&amp;idx=1&amp;sn=f19437f965cffc326556077e6b14be81) | 2019-8-4 |
| 71 | [我在海滩玩沙子，而他们却把沙子玩出了花](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650870853&amp;idx=1&amp;sn=13afc78be0230a06d1cbf2fb4fcf991a) | 2019-8-18 |
| 70 | [最近有网友留言说想了解静电，今天ta来了](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650871049&amp;idx=1&amp;sn=5df9f97ba7eac2189aad021e1f44c88a) | 2019-8-25 |
| 69 | [世上无「南」事，只要……](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650871209&amp;idx=1&amp;sn=725dde8cc4999dd663af84a5ea64a0ba) | 2019-9-1 |
| 68 | [你见过一种能够回到“过去”的流体没？](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650871682&amp;idx=1&amp;sn=583a633fad3d18badd46a7e3d936f7ee) | 2019-9-15 |
| 67 | [投影仪怎么合成「不存在」的颜色](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650872206&amp;idx=1&amp;sn=16d7169723dff7742243db9917b560d9) | 2019-9-29 |
| 66 | [秃了就是秃了，别想着会变强](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650872546&amp;idx=1&amp;sn=ffe5a5037d9d15dfa85e5f352fc55f7a) | 2019-10-6 |
| 65 | [麻麻说：你该穿秋裤了~](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650872854&amp;idx=1&amp;sn=64e82255f8199d152b78194597178b20) | 2019-10-13 |
| 64 | [关于锂离子电池，你所不知道的一些事儿](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650873019&amp;idx=1&amp;sn=3db338fbe12bbf92f639b3632dfd75ca) | 2019-10-20 |
| 63 | [加减法你会，但是你知道色彩里竟然也隐藏着加减法吗？](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650873203&amp;idx=1&amp;sn=b73f1b2211954148bb51ca926b618d9c) | 2019-10-27 |
| 62 | [夸父可以追上“光”吗？](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650873921&amp;idx=1&amp;sn=de6513438173127712622d8f1bfc3412) | 2019-11-17 |
| 61 | [你能在大冬天吃火锅，离不开啤酒厂的努力](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650874106&amp;idx=1&amp;sn=2cead0a6ea60c9c4174fed2851ca8a2b) | 2019-11-24 |
| 60 | [驻波，竟然还可以这么好看！！！](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650874349&amp;idx=1&amp;sn=0343dff76a134e2e7f9b032262c16dda) | 2019-12-1 |
| 59 | [子弹打不碎的玻璃，我能掰碎？](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650874872&amp;idx=1&amp;sn=8bff09146048423305411f91bc5331bc) | 2019-12-15 |
| 58 | [手机电池在冬天为什么那么不耐用](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650875050&amp;idx=1&amp;sn=f4efd6985e9a63d3ef14f9d054434fcc) | 2019-12-22 |
| 57 | [你喜欢“白板黑字”还是“黑板白字”？](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650875435&amp;idx=1&amp;sn=f50a6ed7817eb2b06f4321da956fac3c) | 2019-12-29 |
| 56 | [因纽特人住在雪屋里面不冷吗？](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650875680&amp;idx=1&amp;sn=eed7c85a739095eb66c3b01502ef21d5) | 2020-1-5 |
| 55 | [如何在寒冷的冬季，不摘手套玩手机](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650875883&amp;idx=1&amp;sn=125dbc557b7ffcfbbbe399c4d341e881) | 2020-1-12 |
| 54 | [上升气流有多好看](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650876132&amp;idx=1&amp;sn=21e0364a4be56a4a92d6b060a0091973) | 2020-1-19 |
| 53 | [论如何科学地倒牛奶~](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650876437&amp;idx=1&amp;sn=7901ef07e3cb2f245f8d7585d9eeec3c) | 2020-2-2 |
| 52 | [酒精和84消毒液到底能不能一块用？](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650876596&amp;idx=1&amp;sn=1f1743769d0f4ca7f8f0dfa383d1cbbb) | 2020-2-9 |
| 51 | [震惊！昨天你们立起来的扫把，甚至真的惊动了 NASA](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650876671&amp;idx=1&amp;sn=397cfc6bc897ee97aba7ef8282d8b465) | 2020-2-12 |
| 50 | [运动的“点”与“线”，竟然能产生错觉？？？](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650876813&amp;idx=1&amp;sn=3f0c18424791d879c24f303dc7bfc120) | 2020-2-16 |
| 49 | [童年迷思丨 为什么用洗洁精配成的泡泡水吹不出很多泡泡？](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650877197&amp;idx=1&amp;sn=b5ea9b758acbe9357e6be3d96538b20d) | 2020-2-23 |
| 48 | [没想到螺旋竟然还能这样用？！](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650877471&amp;idx=1&amp;sn=8737ddcf0c02714bf847ad8746e48b2b) | 2020-3-1 |
| 47 | [灯：听说现在都流行赛博朋克，我也想试试…](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650877751&amp;idx=1&amp;sn=90c8174bc4f0eb0d80d04cce884a6156) | 2020-3-9 |
| 46 | [“破镜”真的没办法“重圆”了吗？](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650878036&amp;idx=1&amp;sn=45e1fefe02a7a3c5339b918e96d2494c) | 2020-3-15 |
| 45 | [鸡蛋为什么打不发？因为你发泡原理没学好丨 文末有奖](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650878246&amp;idx=1&amp;sn=59f0b1098c6bfabe3937ffa382ba6a18) | 2020-3-22 |
| 44 | [电饭煲蛋糕硬核科学指南：从入门到放弃](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650878485&amp;idx=1&amp;sn=1a087f7a4c43f38a1f895593d0c9ada1) | 2020-3-29 |
| 43 | [如何科学地从天而降？](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650879077&amp;idx=1&amp;sn=1aaee545905c32b2a7e00b7217fc1e7a) | 2020-4-13 |
| 42 | [这就是去往幼儿园的“公交车”吗？](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650879281&amp;idx=1&amp;sn=5c9724a71fa6e5b4e23f85035ef5214c) | 2020-4-19 |
| 41 | [在？来块小编秘制晶莹剔透小冰块儿？](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650884113&amp;idx=1&amp;sn=cf0e9098d6e509ff316b3e1431fbf75d) | 2020-5-31 |
| 40 | [听完周杰伦的《Mojito》，我不禁想用分子料理做几颗](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650885394&amp;idx=1&amp;sn=1027547a75b0b615b213f82266b01e70) | 2020-6-15 |
| 39 | [如何科学「拍」人](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650885876&amp;idx=1&amp;sn=6e1fc5b5b39560345644810240245886) | 2020-6-22 |
| 38 | [不需要充电就能用几十年的电池竟源自200年前的一个发现？](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650886466&amp;idx=1&amp;sn=a0a25f7b95451fc32edfba4030038f37) | 2020-6-28 |
| 37 | [警告警告，夏天下车时要带上宝宝和宠物，否则……](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650887049&amp;idx=1&amp;sn=169b16dc8d22592ba45233ed8aa284cd) | 2020-7-5 |
| 36 | [这个技术发展到现在就为了让你能愉快地多打几局游戏](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650887538&amp;idx=1&amp;sn=98d25ce8f3763d6b7bfbfa39bd3a96df) | 2020-7-12 |
| 35 | [带火星探测器上天的火箭，为什么有着葫芦形状的推进器？](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650888183&amp;idx=1&amp;sn=60fb8db999c8b5b79f8dd9183ea6d690) | 2020-7-19 |
| 34 | [嫌我们理科生不够浪漫？你有一份音乐数学入门指南待查收](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650888674&amp;idx=1&amp;sn=6ddf384171060b7bec9d206c440eead7) | 2020-7-26 |
| 33 | [对人类网课时期迷惑行为的研究](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650889318&amp;idx=1&amp;sn=329a394d3b44677ec9748ed0a5ebd0ab) | 2020-8-2 |
| 32 | [今天的文章是真的水……](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650889737&amp;idx=1&amp;sn=df12a3917becda9c12fb894b499d27a7) | 2020-8-9 |
| 31 | [还在考驾照的你知道汽车是怎么动起来的吗？](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650890459&amp;idx=1&amp;sn=794783d410321da1b30fc716d7d6ff3a) | 2020-8-16 |
| 30 | [有眼无珠！还不拿好这份科二通过指南？](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650890933&amp;idx=1&amp;sn=e4e9ae1e4711d99f92c099816db94405) | 2020-8-23 |
| 29 | [为了让你过好夏天，你知道科学家们有多努力吗？](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650891500&amp;idx=1&amp;sn=e1f94a89069c5dea9e822aca572170d8) | 2020-8-30 |
| 28 | [这都可以？科学家用碳原子把“积木”搭得出神入化](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650892137&amp;idx=1&amp;sn=52146e8344014c510373392b41c9cc3a) | 2020-9-6 |
| 27 | [想要看懂《信条》，演反间时懂先得你](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650892360&amp;idx=1&amp;sn=e846f030edc6c14539b4c2dd3568bdd2) | 2020-9-9 |
| 26 | [警告！除了钢化玻璃，这些行为也可能引起爆炸](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650894152&amp;idx=1&amp;sn=9d6015a98018ff1e4f265e275f43e487) | 2020-9-27 |
| 25 | [为了做出更好吃的藕粉，他们用上了流体力学和弹性力学](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650894836&amp;idx=1&amp;sn=2b832b7a8bbd140653dc18c6e9632799) | 2020-10-4 |
| 24 | [玻璃，竟然还可以这么智能！！！](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650895740&amp;idx=1&amp;sn=02e42f6bf012fbb19de4a80d43ebcd41) | 2020-10-11 |
| 23 | [量子力学都不懂？还在说你知道量子科技？](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650896972&amp;idx=1&amp;sn=935d65e06e1c7f42b1e0c81f0d04ae3e) | 2020-10-18 |
| 22 | [数字人民币来了，它到底是什么？](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650909902&amp;idx=1&amp;sn=98116d04276e223cc3a00a2dc921ffbb) | 2020-11-1 |
| 21 | [科学解读，到底啥才是真正的「塑料蓝」](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650902229&amp;idx=1&amp;sn=a47d626e8de50b6e24b64f72331778f8) | 2020-10-26 |
| 20 | [想学唱歌？你先把这个玻璃杯震碎](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650913395&amp;idx=1&amp;sn=042a0ac3e74f4bfa965aa86bd3efbadb) | 2020-11-8 |
| 19 | [三分钟，成为特立独行的“凡学人”](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650917487&amp;idx=1&amp;sn=9628c2342bf794d29dae186a83a07b61) | 2020-11-15 |
| 18 | [怎么科学解读闪电鞭？年轻人我劝你耗子尾汁，好好反思](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650921836&amp;idx=1&amp;sn=a811d1a92881520c3837ab9e9e24a89e) | 2020-11-22 |
| 17 | [今天我们不搞科研，来搞颜色](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650926010&amp;idx=1&amp;sn=d7a8b015a2bd592ad043ba65f1dc4b6c) | 2020-11-29 |
| 16 | [如果做不好科研，我就只能回去继承家产了](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650930239&amp;idx=1&amp;sn=da882ac120ddc35c506ee8ef636e963a) | 2020-12-6 |
| 15 | [没有这项技术，赛博朋克2077就算残废？](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650934656&amp;idx=1&amp;sn=067f0d8091ca46dee87577f339a0d011) | 2020-12-13 |
| 14 | [?赛博圣诞老人飞过天空时会震爆电子驯鹿的传感器吗？](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650938158&amp;idx=1&amp;sn=b5c6f6169ec2a01680b952cfd05ca8e9) | 2020-12-20 |
| 13 | [如何科学地与天桥上的贴膜小哥对线？](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650946301&amp;idx=1&amp;sn=13923795d99a61660c5b57fcf39b2266) | 2021-1-3 |
| 12 | [《关于小明转生成为麦克斯韦妖这件事》](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650947942&amp;idx=1&amp;sn=54ff75cd69692c96b5d5c53ac3686c9d) | 2021-1-10 |
| 11 | [今天的文章是冰冰的](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650950723&amp;idx=1&amp;sn=f59b2b333bbcd2db0d9d774b33da2192) | 2021-1-17 |
| 10 | [走近科学：女子睡醒为何如此疲惫](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650954043&amp;idx=1&amp;sn=f0e70c6117a9a5fcc0772bf850613aaf) | 2021-1-24 |
| 9 | [量子世界里有两只猫，一只是薛定谔的猫，另一只是......](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650956566&amp;idx=1&amp;sn=6b03a1e61c2b62c274857a8b4bf81b95) | 2021-1-31 |
| 8 | [我能想到最快乐的事，就是把所有异性都处成朋友](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650965143&amp;idx=1&amp;sn=78b1a598fe352f6dccd23a6cf739adf7) | 2021-2-14 |
| 7 | [就为这顿饺子，我才学的物理](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650968870&amp;idx=1&amp;sn=c80a75d38f6ee79ce48ce26fb29d9071) | 2021-2-21 |
| 6 | [元宵节一起来炸汤（chu）圆（fang）](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650970626&amp;idx=1&amp;sn=9b5ca05b27b86b23e8aa0e394be8b295) | 2021-2-26 |
| 5 | [我们的目标是：星(zao)辰(dai)大(sen)海(qiu)！](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650973973&amp;idx=1&amp;sn=7ba9df79276fe9d01376d66720742355) | 2021-3-7 |
| 4 | [有了这项技术，我们可以科学地闪现！](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650975852&amp;idx=1&amp;sn=bb857458966eaca1b8b3deaaf13581af) | 2021-3-14 |
| 3 | [当我终于摇到了车牌号……](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650982480&amp;idx=1&amp;sn=e1a9eb48d3fda0f9feb7c1a2bc799292) | 2021-3-28 |
| 2 | [道理我都懂，但是这种列车为什么会自己摆动?](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650985132&amp;idx=1&amp;sn=848351af8d9d75af7359e58585efc7dc) | 2021-4-4 |
| 1 | [我已经做好掘雾了：拿不下游泳金牌，还拿不下爱情？](http://mp.weixin.qq.com/s?__biz=MzAwNTA5NTYxOA==&amp;mid=2650988042&amp;idx=1&amp;sn=e00d3602f8710c1860c2451658b992b6) | 2021-4-11 |
