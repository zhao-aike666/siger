- [新必应（New Bing）国内申请与使用教程](https://juejin.cn/post/7199557716998078522)
- [新版bing申请加入后补名单详细教程！](https://zhuanlan.zhihu.com/p/605865709)
- [新必应简介](https://www.bing.com/new/)
- 负责人的人工智能
- [Bing 对话体验和图像创建程序条款](https://www.bing.com/new/termsofuse)
- 新的必应：我们对负责任的人工智能的方法 [The new Bing: Our approach to Responsible AI.pdf](https://blogs.microsoft.com/wp-content/uploads/prod/sites/5/2023/02/The-new-Bing-Our-approach-to-Responsible-AI.pdf)
- [对 Windows 11 进行重大更新，使日常工作更轻松，包括将新的 AI 驱动的必应带到任务栏](https://blogs.windows.com/windowsexperience/2023/02/28/introducing-a-big-update-to-windows-11-making-the-everyday-easier-including-bringing-the-new-ai-powered-bing-to-the-taskbar/)

![输入图片说明](https://foruda.gitee.com/images/1678415747427515627/4df82178_5631341.jpeg "583newBING副本.jpg")

> 日新月异的AI已经开始猝不及防地提示人类自身，再不学习将被AI淘汰啦，这份危机感从AI碾压人类智能的尊严围棋开始正在加速发展，我们距离声势浩大的AI学习 2017年，才不过5年。ML机器学习已经在各个领域遍地开花，AlphaFOLD2 接管了生物学家 90% 的工作，还是保守的估计，就在刚刚，ML开始辅助物理学家预测高温超导啦。OpenAI 之父则爆出 AI智能体的摩尔定律的言论。人类真的能应对未来的到来吗？对于我们大多数人来说，看似遥远的未来，已经无限靠近啦，科幻变现实。

请原谅我的这段科幻式的开场白，科幻频道后遗症。然鹅，这个距离 OpenAI ChatGPT 最近的微软新必应，或许可以给予我们最直接的回答。这期就是通过浏览 New bing 的公开信息，发现的一些蛛丝马迹，来呼应我的这份 “担忧”。我们应该审慎地对待 AI 技术的应用，至少不能让它 “裸奔”，驾驭和使用它，应该还是科学家在实验室中，但对 “变革” 的呼之欲出，万众期待的情形下，人类真的可以抵御这个 “诱惑” 吗？我们真的准备好了吗？至少现在还没有完全准备好... 我和所有用户一样 In the waiting list ... 这是肉眼可见的 “保护” 措施之一，当然还有更大范围的防护措施... 重点推荐 海报顶的标题小字。它是我下一期专题的引子。

> 就在刚刚，我的申请已经通过，可以开始体验了，又将多产出更多期专题了。（截图见文末）也就是前后脚邮件提醒也到了。 :+1:

# [新必应（New Bing）国内申请与使用教程](https://juejin.cn/post/7199557716998078522)

林炳权 lv-4 2023年02月13日 18:21 ·  阅读 348937

![输入图片说明](https://foruda.gitee.com/images/1678413930713231595/a64fd6fd_5631341.png "屏幕截图")

## 新必应（New Bing）国内申请与使用教程

### 新必应申请

下载安装 Edge dev 版本，这个版本可以直接申请(无外网访问限制)使用

Edge dev 下载链接： https://link.juejin.cn/?target=https%3A%2F%2Fwww.microsoftedgeinsider.com%2Fzh-cn%2Fdownload%2Fdev

![输入图片说明](https://foruda.gitee.com/images/1678413976080740259/68619361_5631341.png "屏幕截图")

安装后浏览器集成新必应入口截图（右边）

![输入图片说明](https://foruda.gitee.com/images/1678413995786135877/c632618d_5631341.png "屏幕截图")

完装完成后，点击右边上角必应图标，会弹出申请按钮，登录账号加入等待列表

![输入图片说明](https://foruda.gitee.com/images/1678414007811932341/ba88830a_5631341.png "屏幕截图")

登录微软账号，申请加入，然后会有一封邮件通知你已加入等待列表

![输入图片说明](https://foruda.gitee.com/images/1678414017780693999/168343ab_5631341.png "屏幕截图")

等待微软通过后，你会收到欢迎邮件，你就可以使用新必应

![输入图片说明](https://foruda.gitee.com/images/1678414036288818253/1da21aab_5631341.png "屏幕截图")

## 新必应使用教程

需要先下载安装 HeaderEditor 插件

HeaderEditor 下载链接： https://link.juejin.cn/?target=https%3A%2F%2Fmicrosoftedge.microsoft.com%2Faddons%2Fdetail%2Fheader-editor%2Fafopnekiinpekooejpchnkgfffaeceko

HeaderEditor配置截图：

![输入图片说明](https://foruda.gitee.com/images/1678414192004648943/d876fe2a_5631341.png "屏幕截图")

请按截图来配置，配置参数有


```
// 匹配规则
^http(s?)://(.*).bing\.com/(.*)

// 头名称
x-forwarded-for

// 头内容
8.8.8.8
```

设置完就可以使用了

### 新必应使用截图

直接访问 bing 链接 bing.com

![输入图片说明](https://foruda.gitee.com/images/1678414117580319511/ef0d22c8_5631341.png "屏幕截图")

点击左上角 `聊天` 按钮后，进入 `新必应`

![输入图片说明](https://foruda.gitee.com/images/1678414138260229452/f6b60f8b_5631341.png "屏幕截图")

新必应简单试用

![输入图片说明](https://foruda.gitee.com/images/1678414149352451189/4ad7fc12_5631341.png "屏幕截图")

### 常见问题

如果访问不了，你可以清除一下浏览器必应的 Cookie，地址栏重新输入必应链接：bing.com

![输入图片说明](https://foruda.gitee.com/images/1678414175186369916/ae24de37_5631341.png "屏幕截图")

# [新版bing申请加入后补名单详细教程！](https://zhuanlan.zhihu.com/p/605865709)

1. 打开你的edge浏览器扩展商店，搜索  **ModHeader**  ，然后添加 ModHeader – Modify HTTP headers 这个扩展。你也可以点击下面的链接直接打开这个插件。

   [ModHeader – Modify HTTP headers – Microsoft Edge Addons](https://microsoftedge.microsoft.com/addons/detail/modheader-modify-http-h/opgbiafapkbbnbnjcdomjaghbckfkglc)

   ![输入图片说明](https://foruda.gitee.com/images/1678388749982331074/aedc896e_5631341.png "屏幕截图")

2. 插件安装后，点击右上角的插件图标，然后在窗口中分别输入：`X-forwarded-for` 和 `4.2.2.2` ，输入后点击任意空白处关闭该窗口。

   ![输入图片说明](https://foruda.gitee.com/images/1678388781621103189/1a68be58_5631341.png "屏幕截图")

3. 打开扩展插件，设置 Request headers 的 X-Forwarded-For 参数，建议填微软及 Google 的 DNS，分别如下：

   - 微软 DNS：4.2.2.2
   - 谷歌 DNS：8.8.8.8

   ![输入图片说明](https://foruda.gitee.com/images/1678388811310300431/255468be_5631341.png "屏幕截图")

4. 打开新必应访问网址，并报名。（若无法打开新必应，或加入出错时，建议先清理浏览器的缓存）

   网址： https://www.bing.com/new

![输入图片说明](https://foruda.gitee.com/images/1678388850066079689/1bfd6d59_5631341.png "屏幕截图")

最后静待审批通过，如有问题，可留言交流。

![输入图片说明](https://foruda.gitee.com/images/1678388865352666266/9eb5c836_5631341.png "屏幕截图")

发布于 2023-02-13 12:53・IP 属地上海

# 新必应简介

 **提出真正的问题。获得完整的答案。聊天并创建。** 

你已加入候补名单 

[更快地访问新必应](https://www.bing.com/ck/a?!&&p=930710cde14dc14cJmltdHM9MTY3ODMyMDAwMCZpZ3VpZD0yMWI3NzFmMi1kMWUzLTZiZTAtMWFiMS02MzNjZDAzMTZhY2YmaW5zaWQ9NTEwNQ&ptn=3&hsh=3&fclid=21b771f2-d1e3-6be0-1ab1-633cd0316acf&u=a1L25ldy9mYXN0YWNjZXNzP2Zvcm09TVkwMjkxJk9DSUQ9TVkwMjkx&ntb=1)

## 什么都可以问

提出问题，问题描述可长可短。提问越精确，答案就合适。

- 给我写一份三道菜的菜单。

  > 我需要举办6人晚宴。你能帮我推荐三道不包括坚果或海鲜的菜吗？

- 帮我计划一个很特别的纪念旅行。

  > 我正在规划我的9月份的周年纪念日旅行。能帮忙推荐一些距伦敦希思罗机场航程不超过3小时的去处吗？

- 有什么我可以跟我的孩子一起尝试的艺术创意么？

   > 请推荐一些为1周岁至3周岁孩子作手工的点子和详细说明。制作材料仅限于纸盒、塑料瓶、纸和细绳。

- 可以帮我健身么？

  > 请为我制定一个锻炼手臂和腹肌的计划，无需仰卧起坐和健身器材。每次不应超过30分钟。

- 写一首押韵的诗词。

  > 我8岁的儿子杰克喜欢狗和关于海洋的科学。请为他写一首押韵的诗。

- 帮助我计划钓鱼旅行。

  > 我刚去蒙大拿州的比格霍恩钓过鱼，计划今年春季去佛罗里达群岛钓鱼。请问我需要做的准备工作会有什么不同？

- 可以帮我写个故事么？

  > 为孩子们写一个关于住在月球上的狗的故事 

- 我的编码需要一些帮助。

  > 编写一段输出斐波那契数列的Python代码

- 我需要一辆大型快车。

  > 请推荐几款评价不错、六秒百公里加速、六座及以上的四驱车？

- 帮我做一份智趣问答。

  > 写一个关于流行音乐琐事的测验，我可以和我的朋友一起玩，有 5 个问题。

- 帮我找一个宠物。

  > 我如何挑选最适合我的狗品种？ 是领养还是购买更好？

- 帮我找一个音乐节。

  > 我喜欢电子音乐，想去参加我的第一个音乐节。你对我有什么提示或建议吗？

## 答案仅仅是开始

新必应提供可靠的最新结果，并提供问题的完整答案。当然，它还会引用来源。

- 以你喜欢的方式提问。执行复合搜索。跟进。在聊天中进行完善。你会得到理解，获得令人惊叹的答案。
- 获得答案，而不是面对诸多选项不知所措。必应在网上查找 搜索结果，并汇总具体问题和需求的回复。
- 得到启发。无论是电子邮件还是餐食计划，你只要提供想法和提示，必应都会为你编写一份草稿供你在此基础上进行编辑。

## 常见问题解答

有问题? 从这里开始

### 什么是新必应?

新必应就像你在网上搜索时身边有一位研究助理、个人规划员和创意合作伙伴。使用这组由 AI 提供支持的功能，你可以:

- 询问你的实际问题。当你提出复杂问题时，必应会提供详细的回复。
- 获得实际答案。必应在网上查找搜索结果，为你提供汇总答案。
- 发挥创意。当你需要灵感时，必应可以帮助你编写诗歌、故事甚至分享关于项目的想法。

在聊天体验中，你还可以聊天并提出后续问题，例如，“能否用更简单的术语进行说明”，或者“为我提供更多选项”，以便在搜索中获取不同甚至更详细的答案。


### 如何最恰当地使用新必应?

- 像与人交谈一样提出问题。这意味着包含详细信息、要求提供说明或更多信息以及告诉必应它怎样会对你更有帮助。下面是一个示例:“我计划在九月与朋友一起旅行。距离伦敦希思罗飞行时间在 3 小时以内的有哪些海滩?”然后继续提出类似“我们到了那里该怎么做?”之类的问题。

- 直接询问有关如何与必应交互的提示。尝试“你能做什么?”“你能帮我处理 X 吗?”“有哪些限制?”等。如果必应无法提供回复，它会让你知道。

- 必应努力让答案保持有趣和真实，但鉴于这是早期预览版，它仍可能根据汇总的网络内容显示意外或不准确的结果，因此请运用自己的最佳判断。我们一直在学习，欢迎提供反馈以帮助必应改进。请使用每个必应页面右下角的反馈按钮分享你的想法。


### 它与常规搜索引擎有何不同?

新必应基于现有的必应体验，为你提供新类型的搜索。

- 除了生成相关链接列表之外，必应还整合可靠的网络来源，为你提供一个汇总的答案。
- 按你说话、写作和思考的方式进行搜索。必应接受复合搜索并分享详细回复。
- 在聊天体验中，你可以自然地聊天，并在初始搜索之上提出后续问题，以获取个性化回复。
- 必应可用作创意工具。它可以帮助你编写诗歌、故事甚至分享关于项目的想法。


### 新必应如何生成回复?

必应在网上搜索相关内容，然后汇总找到的内容以生成有用的回复。它还引用内容的来源，因此你能够看到指向它引用的网络内容的链接。


### Microsoft 如何为新必应实现负责任的 AI?

在 Microsoft，我们非常重视对负责任 AI 的承诺。我们根据 [AI 原则](https://www.bing.com/ck/a?!&&p=63a377419834fbc2JmltdHM9MTY3ODMyMDAwMCZpZ3VpZD0yMWI3NzFmMi1kMWUzLTZiZTAtMWFiMS02MzNjZDAzMTZhY2YmaW5zaWQ9NTA4MA&ptn=3&hsh=3&fclid=21b771f2-d1e3-6be0-1ab1-633cd0316acf&u=a1aHR0cHM6Ly93d3cubWljcm9zb2Z0LmNvbS9haS9vdXItYXBwcm9hY2g_YWN0aXZldGFiPXBpdm90MSUzYXByaW1hcnlyNQ&ntb=1)开发必应。我们正在与我们的合作伙伴 OpenAI 合作，以提供鼓励负责任使用的体验。例如，我们在基础模型工作上已经并将继续与 OpenAI 合作，我们设计必应用户体验时以人为本，并且我们开发了一个安全系统，旨在缓解故障并避免滥用内容筛选、操作监视和滥用检测以及其他安全措施。候补名单过程也是我们负责任的 AI 方法的一部分。我们将从具有必应早期访问权限的人员处获取用户反馈，以改进该工具，然后使其广泛可用。

为 AI 赋予责任需要经历一段路程，我们将不断改进我们的系统。我们致力于提高 AI 的可靠性和可信度，您的反馈将帮助我们实现此目标。要详细了解如何负责任地使用必应，请参阅我们的[使用条款和内容策略](https://www.bing.com/ck/a?!&&p=f1e9feb6aebaf2e3JmltdHM9MTY3ODMyMDAwMCZpZ3VpZD0yMWI3NzFmMi1kMWUzLTZiZTAtMWFiMS02MzNjZDAzMTZhY2YmaW5zaWQ9NTA4NA&ptn=3&hsh=3&fclid=21b771f2-d1e3-6be0-1ab1-633cd0316acf&u=a1aHR0cHM6Ly9ibG9ncy5taWNyb3NvZnQuY29tL3dwLWNvbnRlbnQvdXBsb2Fkcy9wcm9kL3NpdGVzLzUvMjAyMy8wMi9UaGUtbmV3LUJpbmctT3VyLWFwcHJvYWNoLXRvLVJlc3BvbnNpYmxlLUFJLnBkZg&ntb=1)。

To learn more about how to use Bing responsibly, please see our [Terms of Use and Code of Conduct](https://www.bing.com/ck/a?!&&p=b02d2cd91e98b1e5JmltdHM9MTY3ODMyMDAwMCZpZ3VpZD0yMWI3NzFmMi1kMWUzLTZiZTAtMWFiMS02MzNjZDAzMTZhY2YmaW5zaWQ9NTA4MQ&ptn=3&hsh=3&fclid=21b771f2-d1e3-6be0-1ab1-633cd0316acf&u=a1aHR0cHM6Ly93d3cuYmluZy5jb20vbmV3L3Rlcm1zb2Z1c2U&ntb=1).


### 如果我看到意外内容或冒犯性内容，该怎么办?

尽管必应努力避免在搜索结果中分享意外的冒犯性内容，并已采取措施防止其聊天功能涉及可能有害的主题，但你仍可能会看到意外结果。我们一直在努力改进预防有害内容的技术。

如果你在系统中遇到有害或不适当的内容，请提供反馈或向必应报告问题，方法是打开回复右上角的菜单，然后单击标志图标。你还可以使用每个必应页面右下角的反馈按钮。我们将继续处理用户反馈，为大家提供安全的搜索体验。


### 必应的 AI 生成回复是否始终都是事实?

必应努力基于可靠的来源提供回复，但 AI 可能会出错，并且互联网上的第三方内容可能并不总是准确或可靠。必应有时会错误地引用它找到的信息，你可能会看到听起来有说服力但不完整、不准确或不恰当的回复。在根据必应的回复做出决策或采取行动之前，请运用自己的判断并仔细检查事实。

要分享网站反馈或报告问题，请打开回复右上角的菜单，然后单击标志图标。你还可以使用每个必应页面右下角的反馈按钮。


### 如何访问新必应?

你可以通过选择“加入候补名单”来请求访问权限。当你被选中后，你将收到一封电子邮件，告知你可以在 Bing.com 访问新必应，然后你可以在常用搜索框中开始键入内容。新必应也可在聊天体验中找到，位于搜索结果的顶部。


### How does Bing work in Edge Mobile?

You can now chat with Bing on your phone in the Microsoft Edge app to get answers from the web, create written content, gather trip planning ideas, and more - all generated by AI. Once you clear the waitlist, you will be able to access the new Bing from your home page on the Microsoft Edge app. Each conversation will be limited to 5 interactions, to keep the interactions grounded in search.

Bing in the Edge app also supports audio interactions – just tap the microphone icon and you can ask Bing your question and hear a voice response.

To share feedback or report a concern in the Edge app, tap and hold on the response and then select either thumbs up or down option for feedback. We welcome your concerns and will continue to work with user feedback to the improve the Bing in Edge app experience for all users.

现在，你可以在微软的Edge应用程序中与手机上的必应聊天，从网络中获取答案，创建书面内容，收集旅行计划想法等，所有这些都是由人工智能生成的。一旦你清除了等待列表，你就可以从微软Edge应用程序的主页访问新的必应。每次对话将被限制为5次互动，以便在搜索中保持互动。

Edge应用中的必应也支持音频交互——只要点击麦克风图标，你就可以向必应提问并听到语音回应。

要在Edge应用程序中分享反馈或报告一个问题，请轻按并按住回复，然后选择点赞或向下选择以获得反馈。我们欢迎您的关注，并将继续根据用户反馈来改善所有用户的Bing in Edge应用程序体验。

### How does Bing work in Skype?

You can now chat with Bing in Skype to get answers from the web, create written content, vacation planning ideas, and more, all generated by AI. Chat with Bing in a one-to-one conversation, or add it to a group with your friends, family, or colleagues.

When Bing is part of a group, Bing will only access the chat context when it is mentioned explicitly using @Bing. Bing will refresh its memory and start over with every new mention. When summarizing a conversation, Bing may get your pronouns wrong. If this happens, you can tell Bing your correct pronouns using @Bing in the message.

Bing does not support audio or video interactions in Skype, and it will not access or participate in any audio or video conversation.

Bing aims to base all its responses on reliable sources, but AI can make mistakes, and third-party content on the internet may not always be accurate or reliable. Use your own judgement and double check the facts before making decisions or taking action based on Bing’s responses in Skype.

Remember this is a preview, and we are still learning. To share feedback or report a concern, right click (desktop) or tap and hold (mobile) on the response and select Report a concern. We’ll take a look.

你现在可以在Skype上与Bing聊天，从网络上获取答案，创建书面内容，度假计划想法等等，所有这些都是由人工智能生成的。在一对一的对话中与Bing聊天，或者将它添加到你的朋友、家人或同事的小组中。

当Bing是群组中的一员时，它只会访问使用@Bing明确提及的聊天上下文。必应将刷新它的记忆，并重新开始每一个新的提及。在总结对话时，必应可能会弄错你的代词。如果发生这种情况，你可以在消息中使用@Bing告诉Bing你正确的代词。

必应不支持Skype中的音频或视频交互，也不会访问或参与任何音频或视频对话。

必应的目标是根据可靠的来源提供所有回复，但人工智能可能会出错，互联网上的第三方内容可能并不总是准确或可靠。根据“必应”在Skype上的回应做出决定或采取行动之前，使用你自己的判断和复查事实。

记住，这只是一个预览，我们还在学习中。共享反馈或报告一个关注，右键单击(桌面)或轻按并保持(移动端)的响应，选择报告一个关注。我们来看看。

# 负责人的人工智能

 **在微软将原则付诸实践** 

> 我们致力于确保以负责任的方式开发人工智能系统，并保证人们的信任。

  播放视频

## 微软负责任的 AI 原则

- 公平：人工智能系统应该公平对待所有人

- 可靠性和安全性：人工智能系统应可靠、安全地运行

- 隐私与安全：人工智能系统应该是安全的，尊重隐私

- 包容：人工智能系统应该赋予每个人权力并吸引人们

- 透明度：人工智能系统应该是可以理解的

- 问 责：人们应该对人工智能系统负责

## 负责任的发展

开发是构建负责任的 AI 系统的第二阶段——从数据收集和处理，到确保性能和代表性的公平性。

### 人类 AI 交互指南

《人与人工智能交互指南》将 20 年的研究综合为 18 条推荐指南，用于在用户交互和解决方案生命周期中设计 AI 系统。

探索准则 

 **[人与人工智能交互指南](https://www.microsoft.com/en-us/haxtoolkit/ai-guidelines/)** 

应用人类与 AI 交互的最佳实践 | 探索 HAX 设计库

 **人与人工智能交互的准则是什么？** 

该指南是一套 18 个普遍适用的最佳实践，用于设计与基于 AI 的产品和功能的人机交互。《指南》分为四组，大致基于它们与人工智能系统交互最相关的时间：初始交互时、交互期间、人工智能系统不可避免地出错时以及随着时间的推移。

 **如何使用指南？** 

当您对基于 AI 的新产品或功能有想法或正在修订现有产品或功能时，请在整个开发过程中使用这些指南。由于实施这些准则通常会影响 AI 系统的各个方面，包括要收集哪些数据以及如何训练 AI 模型，因此越早考虑它们越好。

在规划和需求会议期间，将多个学科摆在桌面上，以思考哪些准则对产品的用户体验最重要，以及使用哪些实施策略。HAX 工作簿可以帮助您的团队在早期规划中与指南保持一致和指导，我们的 HAX 设计库和设计模式可以为您提供如何实施它们的想法。

请记住，指南不是清单，并非所有指南都适用于每种情况。某些方案可能需要额外的专业指导。

了解每个指南，并在 HAX 设计库中查看可用的设计模式和示例。

[![输入图片说明](https://foruda.gitee.com/images/1678391963071495502/916eb37b_5631341.png "屏幕截图")](https://foruda.gitee.com/images/1678392345927152145/d5f98f4e_5631341.jpeg)

 **为什么选择《指南》？** 

我们制定了指南，因为人工智能正在从根本上改变人们与计算系统的交互方式，从业者正在寻求指导，他说：“这是我在设计领域工作过的最模糊的领域......没有任何真正的规则，我们也没有很多工具。为了为创建直观的 AI 用户体验奠定基础，我们综合并验证了 20 多年的研究和设计思维，以创建指南。

 **了解有关指南的更多信息** 

- 该指南在2019年CHI论文中介绍，该论文获得了最佳论文荣誉奖。阅读论文摘要，或在“关于”页面上详细了解我们用于创建和验证论文的过程。
- 若要详细了解团队在其创意设计过程中使用指南的其他方式，请参阅我们的 Medium 帖子创意过程中的 AI 指南：我们如何在 Microsoft 将人类 AI 指南付诸实践。
- 为了了解指南如何影响整个人工智能系统，我们提供了机器学习和软件工程的注意事项。

 **可打印资源** 

- 附有指引的可打印卡片：英文（PDF）和[中文（PDF）](https://www.microsoft.com/en-us/haxtoolkit/uploads/prod/2021/05/Human-AI_-guidelines_CARDS_Chinese_redcover.pdf)
- 带有指南的可打印海报：[PDF](https://www.microsoft.com/en-us/haxtoolkit/uploads/prod/2021/05/AI-Guidelines-poster_nogradient_final.pdf) 和 [jpg](https://www.microsoft.com/en-us/haxtoolkit/uploads/prod/2021/05/AI-Guidelines-poster_nogradient100.jpg)

 **快速参考** 

 **最初** 

- [准则 1](https://www.microsoft.com/en-us/haxtoolkit/guideline/make-clear-what-the-system-can-do/)：明确系统可以做什么。  
帮助用户了解 AI 系统能够做什么。
- [准则 2](https://www.microsoft.com/en-us/haxtoolkit/guideline/make-clear-how-well-the-system-can-do-what-it-can-do/)：明确系统能做什么，  
帮助用户了解人工智能系统犯错误的频率。

 **交互期间** 

- [准则 3](https://www.microsoft.com/en-us/haxtoolkit/guideline/time-services-based-on-context/)：基于上下文的时间服务。
根据用户当前任务和环境执行操作或中断的时间。
- [准则 4](https://www.microsoft.com/en-us/haxtoolkit/guideline/show-contextually-relevant-information/)：显示上下文相关的信息。
显示与用户当前任务和环境相关的信息。
- [准则5](https://www.microsoft.com/en-us/haxtoolkit/guideline/match-relevant-social-norms/)：符合相关的社会规范。
确保根据用户的社会和文化背景，以用户期望的方式提供体验。
- [准则6](https://www.microsoft.com/en-us/haxtoolkit/guideline/mitigate-social-biases/)：减轻社会偏见。
确保人工智能系统的语言和行为不会强化不良和不公平的刻板印象和偏见。

 **出错时** 

- [准则7](https://www.microsoft.com/en-us/haxtoolkit/guideline/support-efficient-invocation/)：支持高效调用。
在需要时轻松调用或请求 AI 系统的服务。
- [准则8](https://www.microsoft.com/en-us/haxtoolkit/guideline/support-efficient-dismissal/)：支持高效解雇。
轻松消除或忽略不需要的 AI 系统服务。
- [准则9](https://www.microsoft.com/en-us/haxtoolkit/guideline/support-efficient-correction/)：支持高效校正。
在 AI 系统出错时，可以轻松编辑、优化或恢复。
- [准则10](https://www.microsoft.com/en-us/haxtoolkit/guideline/scope-services-when-in-doubt/)：有疑问时确定服务范围。
当不确定用户的目标时，参与消除歧义或优雅地降低 AI 系统的服务。
- [准则11](https://www.microsoft.com/en-us/haxtoolkit/guideline/make-clear-why-the-system-did-what-it-did/)：明确系统为什么这样做。
使用户能够访问有关 AI 系统为何如此运行的解释。

 **随着时间的推移** 

- [准则12](https://www.microsoft.com/en-us/haxtoolkit/guideline/remember-recent-interactions/)：记住最近的互动。
保持短期记忆，并允许用户有效地参考该记忆。
- [准则13](https://www.microsoft.com/en-us/haxtoolkit/guideline/learn-from-user-behavior/)：从用户行为中学习。
通过从用户随时间推移的操作中学习来个性化用户体验。
- [准则14](https://www.microsoft.com/en-us/haxtoolkit/guideline/update-and-adapt-cautiously/)：谨慎更新和适应。
在更新和调整 AI 系统的行为时限制破坏性更改。
- [准则15](https://www.microsoft.com/en-us/haxtoolkit/guideline/encourage-granular-feedback/)：鼓励精细反馈。
使用户能够在与 AI 系统的常规交互期间提供反馈，指示其首选项。
- [准则16](https://www.microsoft.com/en-us/haxtoolkit/guideline/convey-the-consequences-of-user-actions/)：传达用户操作的后果。
立即更新或传达用户操作将如何影响 AI 系统的未来行为。
- [准则17](https://www.microsoft.com/en-us/haxtoolkit/guideline/provide-global-controls/)：提供全局控件。
允许用户全局自定义 AI 系统监视的内容及其行为方式。
- [准则18](https://www.microsoft.com/en-us/haxtoolkit/guideline/notify-users-about-changes/)：通知用户有关更改的信息。
在 AI 系统添加或更新其功能时通知用户。

### 对话式 AI 指南

了解如何使用负责任的对话 AI 指南设计以人为本的机器人，并建立对服务的信任。

获取机器人指南 

 **[负责任的机器人：对话式 AI 开发人员的 10 条准则](https://www.microsoft.com/en-us/research/publication/responsible-bots/)** 

2018年<>月

越来越多的人在日常生活中使用机器人，无论是快速解决客户服务问题，还是帮助人们管理日历、查看天气或订购披萨。机器人，或者更一般地说，对话式人工智能，有能力帮助人们实现更多目标，我们才刚刚开始看到它们增强我们能力的潜力。

为了让人们和社会充分发挥机器人的潜力，它们的设计方式需要赢得他人的信任。这些准则旨在帮助你设计一个机器人，以建立对机器人所代表的公司和服务的信任。这些准则不作为法律建议，应单独确保您的机器人符合该领域法律的快速发展。此外，在设计机器人时，应考虑在开发任何以数据为中心的 AI 系统时承担的广泛责任，包括道德、隐私、安全、安全、包容、透明度和问责制。例如，请参阅微软在 2018 年 <> 月出版的《[未来计算](https://news.microsoft.com/futurecomputed/)》一书中发表的负责任开发人工智能的六项原则。

- 计算的未来：人工智能和制造业（[现已提供免费数字下载](https://news.microsoft.com/wp-content/uploads/prod/sites/393/2019/06/Microsoft_TheFutureComputed_AI_MFG_Final_Online.pdf)）

详细了解我们的 [AI 方法](https://www.microsoft.com/en-us/ai/our-approach-to-ai)

[微软推出开发负责任的对话式 AI](https://blogs.microsoft.com/?p=52557177)
的指南 微软官方博客 |十一月 14， 2018

# [Bing 对话体验和图像创建程序条款](https://www.bing.com/new/termsofuse)

上次更新时间: 2023 年 2 月 1 日

这些补充使用条款（以下简称“条款”）适用于 Bing 上的对话体验，以及 Bing 上任何其他 AI 驱动的生成式体验（以下简称“对话体验”）和 Bing 图像创建程序（以下简称“图像创建程序”，与对话体验一起统称为“在线服务”）。这些在线服务旨在取悦、激励和增强人类的创造力。 如果您使用任何在线服务，即表明您同意以下条款。

1. **资格和使用要求** 。 要使用在线服务，您必须拥有有效的 Microsoft 帐户，并且必须是已登录的 Microsoft Rewards 用户。首次推出对话体验后（届时该体验将仅限于注册体验候补名单并被我们选中使用对话体验的用户），我们可能不要求您使用 Microsoft 帐户登录或登录 Microsoft Rewards。您对在线服务的使用受这些条款（以下简称本“协议”）以及 Microsoft 服务协议（通过提及而纳入）的约束。您同意在线服务构成 Microsoft 服务协议中定义的服务。如果本协议与 Microsoft 服务协议之间存在任何冲突，则冲突条款以本协议为准。

   您对在线服务的使用也受 Microsoft 隐私声明（描述了我们如何收集、使用和披露与您使用在线服务相关的信息）的约束。

2. **对话体验候补名单** 。对话体验和其他相关功能最初不会向所有 Bing 用户提供。这些功能将仅向请求添加到候补名单的用户提供，在用户注册此类候补名单后，Microsoft 有权自行决定将用户添加到在线服务，无论 Microsoft 做出决定的方式如何。

3. **使用在线服务** 。Bing 图像创建程序和对话体验都允许您提交文本输入（以下简称“提示”）。对于 Bing 图像创建程序，该服务可能允许您根据提示生成图像（以下简称“图像创作”）。对于对话体验，该服务可能允许您提交提示，并与在线计算机驱动的聊天机器人进行对话，在某些情况下生成文本内容（以下简称“文本创作”，与图像创作一起统称为“创作”）。您使用在线服务必须遵守在线服务行为准则（“行为准则”）。使用在线服务，即表明您确认您已阅读、理解并同意遵守使用条款和行为准则。

   您使用在线服务和生成的创作必须仅 (i) 以合法方式并遵守所有适用法律；(ii) 根据本协议、行为准则、Microsoft 服务协议或其他针对在线服务的 Microsoft 文档；以及 (iii) 以不侵犯或试图侵犯、盗挪用或以其他方式侵犯我们或任何其他个人或实体的任何权利的方式（为明确起见，“方式”包括但不限于导致或试图导致在线服务产生内容的方法、目的和/或手段）。

   由于在线服务的性质，不同用户的创作可能不同，在线服务可能会为 Microsoft 或其他用户生成相同或类似的输出。 其他用户也可以提出类似的问题并得到相同、相似或不同的回答。

   这些在线服务（包括机器与人的对话互动）可能包括广告。

4. **违规行为** 。严重或多次违反行为准则可能会导致您被暂停使用涉及违规行为的任何在线服务，或可能被暂停使用所有在线服务和其他服务（定义见 Microsoft 服务协议）。您可以通过相关服务用户界面提交上诉，对暂停使用适用服务提出上诉。我们保留永久停止您使用在线服务的权利。

5. **暂停和取消** 。除第 4 款外，我们保留随时以任何理由暂停或停止向部分或所有客户提供或支持全部或部分在线服务的权利。根据法律要求，我们可能会基于任何理由（包括但不限于：您违反本协议（包括未能遵守行为准则）；我们怀疑您参与了欺诈或非法活动；或在您的 Microsoft 帐户已被我们或您自己暂停或关闭），在不进行通知的情况下，随时限制、暂停或终止您对任何在线服务的使用。

6. **兑换 Bing Rewards – Bing 图像创建程序** 。您可以将 Microsoft Rewards 兑换为 Bing 图像创建程序增强版，以加快图像创作的生成。

7. **创作的使用** 。在遵守本协议、Microsoft 服务协议和我们的内容政策的前提下，您可以将创作用于在线服务之外的任何合法的个人非商业目的。

8. **内容所有权** 。Microsoft 不主张您向在线服务提供、发布、输入、提交或从在线服务接收的标题、提示、创作或任何其他内容（包括反馈和建议）的所有权。但是，如果您使用在线服务、发布、上传、输入、提供或提交内容，即表明您授予 Microsoft、其关联公司、第三方合作伙伴使用与其业务（包括但不限于所有的 Microsoft 服务）运作相关的标题、提示、创作和相关内容的权利，包括但不限于以下许可权利：复制、分发、传送、公开展示、公开执行、翻制、编辑、翻译和重新排印标题、提示、创作和相关内容；以及向在线服务的任何供应商分许可此类权利。

   根据此处的规定，任何人在使用您的内容时，均无需支付任何报酬。Microsoft 没有义务发布或使用您可能提供的任何内容，并且 Microsoft 可以自行决定随时移除任何内容。

   您保证并声明，您拥有或以其他方式控制内容的所有权利，如这些使用条款中所述，包括但不限于提供、发布、上载、输入或提交内容的所有必要权利。

9. **无担保；无声明或保证；您的赔偿** 。我们计划继续开发和改进在线服务，但我们不保证或承诺在线服务的运行方式或它们能按预期运行。在线服务旨在用于娱乐目的；在线服务并非无错误，可能无法按预期工作，并且可能生成错误信息。您不应依赖在线服务，也不应使用在线服务获取任何形式的建议。您对在线服务的使用风险由您自己承担。

   （本款仅为了明确起见，但不以任何方式限制 Microsoft 服务协议第 12 节）Microsoft 不作出任何形式的以下保证或声明，即在线服务创建的任何材料在您可能使用的内容的任何后续使用中不会侵犯任何第三方的权利（包括但不限于版权、商标、隐私权和宣传权以及造成诽谤）。您必须根据适用法律和任何第三方权利使用在线服务中的任何内容。此外，您同意赔偿并使 Microsoft 及其关联公司、员工和任何其他代理免受因您使用在线服务（包括您后续使用在线服务中的任何内容以及您违反本条款、Microsoft 服务协议、行为准则或违反适用法律）而产生的或与之相关的任何索赔、损失和费用（包括律师费）。

10. **如果您居住在（或者所在企业的主营业地位于）美国，请阅读 MICROSOFT 服务协议第 15 节中的约束性仲裁条款和集体诉讼弃权声明。其会对与本协议相关的争议的解决方法产生影响。** 

## 行为准则

Bing 图像创建程序和 Bing 的对话体验（以下统称为“在线服务”）的使用受 Microsoft 隐私声明行为准则一节的约束，而本文档就这些在线服务内如何应用行为准则提供了另一个层面的说明。

使用本在线服务，即表明您同意：

- 不得从事对您、在线服务或其他人有害的活动。 不得试图创建或共享可能被用于骚扰、欺凌、虐待、威胁或恐吓其他个人，或以其他方式对个人、组织或社会造成伤害的内容。
- 不得从事侵犯他人隐私的活动。 不得试图创建或共享可能侵犯他人隐私（包括泄露私人信息）的内容（有时称为“人肉搜索”）。
- 不得从事欺诈、虚假或误导性的活动。不得试图创建或共享可能误导或欺骗他人（包括创建虚假信息、内容欺诈或欺骗性冒充）的内容。
- 不得侵犯他人的权利。 不得试图使用在线服务侵犯他人的合法权利，包括知识产权。
- 不得使用服务创建或共享不适当的内容或材料。 Bing 不允许使用在线服务创建或共享成人内容、暴力或血腥、仇恨内容、恐怖主义和暴力极端主义内容、颂扬暴力、儿童性剥削或虐待材料，或其他令人感到不安或冒犯的内容。
- 不得从事任何非法活动。您使用在线服务必须遵守适用法律。

### 内容和审核

在线服务可能会阻止违反行为准则的文本提示，或可能导致创建违反行为准则材料的文本提示。生成的违反行为准则的图像或文本可能会被删除。滥用在线服务（如多次尝试制作禁止内容或其他违反行为准则的行为）可能会导致服务或账户被暂停使用。 用户可以通过反馈或报告问题功能报告有问题的内容。

# [The new Bing: Our approach to Responsible AI.pdf](https://blogs.microsoft.com/wp-content/uploads/prod/sites/5/2023/02/The-new-Bing-Our-approach-to-Responsible-AI.pdf)

<p><img width="19%" src="https://foruda.gitee.com/images/1678393253884135664/436c8cbf_5631341.jpeg" title="The-new-Bing-Our-approach-to-Responsible-AI-1.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1678393262994438316/4bf73141_5631341.jpeg" title="The-new-Bing-Our-approach-to-Responsible-AI-2.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1678393272629411334/488c8bdd_5631341.jpeg" title="The-new-Bing-Our-approach-to-Responsible-AI-3.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1678393283586840280/c59f07f0_5631341.jpeg" title="The-new-Bing-Our-approach-to-Responsible-AI-4.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1678393294816794345/899b4fb4_5631341.jpeg" title="The-new-Bing-Our-approach-to-Responsible-AI-5.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1678393305613818484/783da0f0_5631341.jpeg" title="The-new-Bing-Our-approach-to-Responsible-AI-6.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1678393315832585210/6babb83b_5631341.jpeg" title="The-new-Bing-Our-approach-to-Responsible-AI-7.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1678393325622606083/51bc5161_5631341.jpeg" title="The-new-Bing-Our-approach-to-Responsible-AI-8.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1678393334645738291/3e5366ba_5631341.jpeg" title="The-new-Bing-Our-approach-to-Responsible-AI-9.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1678393343602363919/699a4e80_5631341.jpeg" title="The-new-Bing-Our-approach-to-Responsible-AI-10.jpg"></p>

### 新的必应：我们对负责任的人工智能的方法

<p><img width="19%" src="https://foruda.gitee.com/images/1678393443531193390/9565c825_5631341.jpeg" title="新的必应我们对负责任的人工智能的方法-1.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1678393452153926646/79954d40_5631341.jpeg" title="新的必应我们对负责任的人工智能的方法-2.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1678393462242637463/09152ea1_5631341.jpeg" title="新的必应我们对负责任的人工智能的方法-3.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1678393471160352728/5df919b8_5631341.jpeg" title="新的必应我们对负责任的人工智能的方法-4.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1678393480489767795/c435d261_5631341.jpeg" title="新的必应我们对负责任的人工智能的方法-5.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1678393489481660550/b7603b5d_5631341.jpeg" title="新的必应我们对负责任的人工智能的方法-6.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1678393500104567336/87bd4ce9_5631341.jpeg" title="新的必应我们对负责任的人工智能的方法-7.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1678393509856652046/33d9ce05_5631341.jpeg" title="新的必应我们对负责任的人工智能的方法-8.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1678393519931240073/0351af94_5631341.jpeg" title="新的必应我们对负责任的人工智能的方法-9.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1678393528947458133/5bd23f5a_5631341.jpeg" title="新的必应我们对负责任的人工智能的方法-10.jpg"></p>

# [对 Windows 11 进行重大更新，使日常工作更轻松，包括将新的 AI 驱动的必应带到任务栏](https://blogs.windows.com/windowsexperience/2023/02/28/introducing-a-big-update-to-windows-11-making-the-everyday-easier-including-bringing-the-new-ai-powered-bing-to-the-taskbar/)

由 帕诺斯·帕奈，首席产品官

这是一个激动人心的技术时刻，不仅对我们行业，而且对世界。Windows PC在我们的日常生活中从未如此重要，随着我们接近由AI大规模采用引领的下一波计算浪潮，这种情况越来越多。今天对 Windows 11 的重大更新，我被抽到介绍，满足了这个人工智能的新时代，并重塑和改进了人们在 PC 上完成工作的方式。

Windows 11 在一年多前推出，为电脑提供了现代更新和所有新体验，使我们能够连接、参与以及被看到和听到。自推出以来，Windows 11 用户的参与度一直高于 Windows 10 用户，我们的美国消费者客户满意度高于任何版本的 Windows。

在过去的三周里，我们还为1个国家/地区的169多万人推出了新的AI驱动的必应预览版，并将新的必应扩展到Bing和Edge移动应用程序，并将其引入Skype。 这是搜索、聊天和创作的新时代，借助新的 Bing 和 Edge，您现在拥有自己的网络副驾驶。

![输入图片说明](https://foruda.gitee.com/images/1678394149851439886/f7a04896_5631341.png "屏幕截图")

今天，我们通过在任务栏中实现可键入的 Windows 搜索框和新的 AI 驱动的 Bing 的惊人功能，向前迈出了重要的一步，增加了 Windows PC 令人难以置信的广度和易用性。将 Windows 的所有搜索需求放在一个易于查找的位置。

搜索框是 Windows 上使用最广泛的功能之一，每月有超过五亿用户，现在有了典型的 Windows 搜索框和新的 AI 驱动的必应，你将能够比以往更快地找到你正在寻找的答案。

我们受到人们如何使用新必应的故事的启发。例如，一位来自发展中国家的第一代研究生分享了新必应如何使他能够访问以前无法访问且难以找到的信息和资源。像这样的故事激励着我们，激励着我们。借助 Windows 任务栏中的新必应，您将更有能力利用全球信息。

就我个人而言，这项技术对我的孩子和我与我的父亲，他们的祖父用希腊语交流的方式产生了难以置信的影响。对我们来说，与来自哪里保持联系很重要，包括说语言，通过新的必应聊天体验，我们可以和父亲一起学习和练习用希腊语写作和说话。它鼓舞人心，改变了我们以我们从未想象过的方式沟通和联系的方式。

很快，数以亿计的 Windows 11 用户可以访问这项令人难以置信的新技术，直接从他们的 Windows 任务栏上搜索、聊天、回答问题和生成内容。

如果你使用的是必应预览版，则只需安装今天的 Windows 11 更新即可访问新的搜索框。要加入新的必应预览版，请在候补名单上注册。

我们迫不及待地想看看 Windows 中的新必应如何激发您的灵感。

## 更新中的其他 Windows 11 新功能，旨在让你的日常生活更轻松

我们也很高兴推出来自整个团队的一系列新功能，这些功能将使你在 Windows 11 上的日常工作更轻松。例如，您将能够使用适用于iOS的Phone Link的新预览版将iPhone®移动设备直接链接到Windows 11 PC。此外，你会注意到改进的触摸体验、全屏小组件以及对 Windows 365 应用的快速访问。Windows 11 还包括“开始”屏幕中的新 AI 功能，以及持续更新，以使操作系统更易于访问和可持续，并履行我们对整个系统和应用程序的质量和易用性的持续承诺。我们期待听到您对这些新更新的反馈。

### 预览版 iOS 手机链接简介

![输入图片说明](https://foruda.gitee.com/images/1678394214809971424/40b63cf1_5631341.png "屏幕截图")

> 适用于 iOS 的 Windows 11 电话链接用户界面

---

![输入图片说明](https://foruda.gitee.com/images/1678417821040309438/668d5151_5631341.jpeg "start new bing.jpg")

