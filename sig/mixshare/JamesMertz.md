- [Real Python 团队成员的爱好](https://gitee.com/mixshare/siger/issues/I6MBBW#note_16772135_link)，科幻迷
- [James Mertz @NASA  JPL 的软件保障](https://gitee.com/mixshare/siger/issues/I6MBBW#note_16772136_link)
- [詹姆斯·默茨 充满激情的Python 主义者](https://realpython.com/team/jmertz/)
- [PyCon 2019：快速回顾](https://mertzmusings.com/pycon-2019-a-quick-lookback/)

<p><img width="706px" src="https://foruda.gitee.com/images/1678641478295915115/c261de9d_5631341.jpeg" title="589NASAJPLpythonist副本.jpg"></p>

> James是NASA喷气推进实验室（JPL）的一名充满激情的Python开发人员。当他不工作时，他通常会修补他的Raspberry Pi或Arduino套件之一，并试图让他的两个孩子也对它们感兴趣。

我是被最后一句打动的，NASA JPL 软件工程师，喜欢玩树莓派和ARDUINO，最重要的是他还带娃玩儿，看他的 ADA 水果的背包就知道他有多爱啦。这位Python主义者是一位软件保障工程师，他是 NASA 的 欧罗巴快车计划 （NASA Europa Clipper） 的工程师，一颗将围绕木星运行并接近欧罗巴卫星的卫星。

> 下一期专题知道是什么了吧。

# [Real Python 团队成员的爱好](https://gitee.com/mixshare/siger/issues/I6MBBW#note_16772135_link)，科幻迷

能告诉我 REAL PYTHON 团队的成员都有什么爱好吗？

- Searching for: REAL PYTHON team members hobbies
- Searching for: REAL PYTHON team members hobbies site:realpython.com

Generating answers for you…

> 你好，这是必应。我可以帮你找到 REAL PYTHON 团队的成员都有什么爱好。根据搜索结果1，REAL PYTHON 团队有很多不同的成员，他们除了教授 Python 之外，还有各种各样的兴趣爱好。例如：

> - Christopher Bailey 喜欢创作音乐，他的艺名是 Tripnet1。
> - Christian Mayer 喜欢旅行和摄影1。
> - James Mertz 喜欢跑步和阅读科幻小说1。
> - Sadie Parker 喜欢烘焙和绘画1。

> 你对哪位成员的爱好感兴趣呢？

  - 我想了解更多关于 Christopher Bailey 的音乐创作。
  - 我想了解更多关于 Christian Mayer 的旅行和摄影。
  - 我想了解更多关于 James Mertz 的跑步和科幻小说。

可以列出更多成员的更多爱好吗？

- Searching for: REAL PYTHON team members hobbies site:realpython.com

Generating answers for you…

> 当然可以。根据搜索结果1，REAL PYTHON 团队还有以下一些成员和他们的爱好：

> - Aldren Santos 喜欢打篮球和玩电子游戏1。
> - Joanna Jablonski 喜欢瑜伽和跳舞2。
> - Martin Breuss 喜欢攀岩和滑雪1。
> - Ricky White 喜欢足球和魔术1。

> 你想了解更多关于哪位成员的爱好呢？

能列出全部成员的爱好吗？

Searching for: REAL PYTHON team members hobbies site:realpython.com

> 抱歉，我不能列出全部成员的爱好，因为搜索结果1没有提供完整的信息。你可以访问 REAL PYTHON 的网站1，查看他们的团队介绍，或者加入他们的社区2，和他们交流。你还有其他想要了解的吗？

那我想了解更多关于 James Mertz 的科幻小说。

- Searching for: James Mertz science fiction site:realpython.com

> 好的，我可以帮你找到一些关于 James Mertz 的科幻小说的信息。根据搜索结果1，James Mertz 是 NASA 的喷气推进实验室 (JPL) 的一名 Python 开发者，他喜欢阅读科幻小说，尤其是 Isaac Asimov 和 Arthur C. Clarke 的作品2。他还在 REAL PYTHON 上写了一些教程，比如如何遵循 PEP 8 编写优美的 Python 代码3。你想了解更多关于他的什么呢？

  - 我想知道他最喜欢的科幻小说是什么。
  - 我想知道他在 NASA 做什么样的项目。
  - 我想知道他是怎么学习 Python 的。

我想知道他最喜欢的科幻小说是什么。

- Searching for: James Mertz favorite science fiction site:realpython.com

> 抱歉，我没有找到关于 James Mertz 最喜欢的科幻小说的具体信息。不过，根据搜索结果1，他喜欢 Isaac Asimov 和 Arthur C. Clarke 的作品，所以我猜他可能会喜欢《基地》系列和《2001：太空漫游》这样的经典科幻小说。你有没有读过这些小说呢？

是的，我读过这些小说，非常好看。

> 我很高兴你喜欢这些小说。它们确实是科幻界的经典之作，有很多深刻的思想和想象力。你还有其他想要了解的吗？

我想知道他在 NASA 做什么样的项目。

- Searching for: James Mertz NASA project site:realpython.com

> 根据搜索结果1，James Mertz 是 NASA 的喷气推进实验室 (JPL) 的一名软件保障工程师，他目前参与了欧罗巴快车 (Europa Clipper) 项目，这是一个将围绕木星运行，并且靠近木卫二 (Europa) 的卫星2。这个项目的目的是探索木卫二是否有可能存在生命。你对这个项目感兴趣吗？

是的，我对这个项目非常感兴趣。

- Searching for: Europa Clipper site:nasa.gov

> 我很高兴你对这个项目感兴趣。根据搜索结果1，欧罗巴快车计划于 2024 年 10 月发射，它将携带一系列先进的科学仪器，对木卫二的表面和内部进行详细的探测，以确定这个冰冷的卫星是否有可能存在生命。欧罗巴快车将围绕木星运行，并在接近木卫二时进行近 50 次飞越，最低高度仅为 25 公里。你想知道更多关于这个项目的什么呢？

> Learn more:
> 1. https://europa.nasa.gov/spacecraft/meet-europa-clipper/
> 2. https://europa.nasa.gov/spacecraft/assembly/
> 3. https://europa.nasa.gov/resources/394/nasa-europa-clipper-solar-arrays-assembly/
> 4. https://europa.nasa.gov/
> 5. https://europa.nasa.gov/mission/about/

- 我想知道它是如何运行在太阳能下的。
- 我想知道它是如何避免木星强大的辐射的。
- 我想知道它是如何探测木卫二下面的海洋的。

我想知道 James Mertz 在这个项目中是怎样使用 他的 PYTHON 技能的？

 **Sorry, I think we need to move on! Click “New topic” to chat about something else.** 

# [James Mertz @NASA  JPL 的软件保障](https://gitee.com/mixshare/siger/issues/I6MBBW#note_16772136_link)

我想知道 REAL PYTHON 的 James Mertz 在 NASA 的 欧罗巴快车计划 中是怎样使用 他的 PYTHON 技能的？他是(JPL) 的一名软件保障工程师

- Searching for: James Mertz REAL PYTHON NASA Europa Clipper

> 你好，这是 Bing。我可以帮你找到一些关于 James Mertz 在 NASA 的 欧罗巴快车计划 中使用 Python 的信息。根据搜索结果，James Mertz 是一位在 NASA 的 喷气推进实验室 (JPL) 工作的软件保障工程师123。他目前参与的项目是欧罗巴快车计划，这是一颗将围绕木星运行并接近欧罗巴卫星的卫星1。他也是 Real Python 的一名作者，为 Python 社区提供了很多有用的教程2。

> Learn more:
> 1. https://realpython.com/real-python-pycon-us/
> 2. https://realpython.com/team/jmertz/
> 3. https://www.linkedin.com/in/mertzjames

- 欧罗巴快车计划的目标是什么？
- James Mertz 在 Real Python 上写了哪些教程？
- Python 在 NASA 有哪些应用？

James Mertz 在 Real Python 上写了哪些教程？

- Searching for: James Mertz Real Python tutorials

根据搜索结果，James Mertz 在 Real Python 上写了很多教程，涉及不同的主题和难度。例如，他写了关于如何在 Python 中读写文件1、如何使用文档字符串记录代码2、以及 Python 爱好者的节日愿望清单3 等。[你可以在 Real Python 的网站上查看他的所有教程](https://realpython.com/team/jmertz/)4。

> Learn more:
> 1. https://realpython.com/read-write-files-python/
> 2. https://realpython.com/documenting-python-code/
> 3. https://realpython.com/python-holiday-wish-list/
> 4. https://realpython.com/team/jmertz/
> 5. https://github.com/mertzjames/pcc-cis-012-intro-to-programming-python

4 我找到了他的个人空间在 REALPYTHON 上，非常好，我也喜欢树莓派和ARDUINO。 我想可以问下一个问题啦 我好奇 NASA 的 工程师都用 PYTHON 做些什么？有哪些应用案例？

- Searching for: NASA Python use cases

> NASA 的工程师用 Python 做了很多有趣和重要的事情。根据搜索结果，Python 在 NASA 有以下一些应用案例1234：

> - 研究和科学目的，例如探索恒星、行星、大气、航空、遥感和 GIS 等领域。
> - 服务器端数据分析，例如 Netflix 使用 Python 来处理海量的流媒体数据。
> - 后端应用开发，例如 Reddit 使用 Python 和 web.py 框架来运行其网站。
> - 火星直升机智能的控制软件，例如 F’ (FPrime) 框架支持 Python 库和包，如 SciPy, NumPy, Matplotlib, OpenCV, Elasticsearch 等。

> [你可以在 NASA 的开源代码目录中查看更多使用 Python 的项目](https://code.nasa.gov/?q=python)4。

> Learn more:
> 1. https://www.dataquest.io/blog/real-world-python-use-cases/
> 2. https://thelead.io/data-science/companies-that-uses-python/
> 3. https://learnpython.com/blog/python-on-mars/
> 4. https://code.nasa.gov/?q=python

- 火星直升机智能是什么？
- 我想学习如何使用 SciPy 和 NumPy。
- 你能给我推荐一些其他的 Python 应用案例吗？

太令人兴奋啦，火星直升机智能的控制软件。快快分享下 更详细的信息 尤其是 PYTHON 使用部分，当然飞控部分也是我感兴趣的。能将火星直升机和PYTHON 联系在一起，太让人着迷了。

- Searching for: Python Mars helicopter Ingenuity

> 火星直升机智能是 NASA 的一个技术示范项目，旨在测试在另一个世界上进行有动力、有控制的飞行1。它搭乘着毅力号火星车抵达了火星，然后在一个合适的“机场”位置释放出来，进行了一系列的试验飞行12。

> 火星直升机智能的导航计算机运行着一个嵌入式 Linux 发行版。它的大部分软件是用 C++ 编写的，使用了 JPL 的开源飞控框架 F Prime(F´)3。同时，Python 生态系统在从地面控制到飞行建模到数据处理等方面都发挥了重要作用34。

> 你可以在 GitHub 上查看 F Prime(F´) 的源代码和文档。

> Learn more:
> 1. https://mars.nasa.gov/technology/helicopter/
> 2. https://www.theatlantic.com/science/archive/2021/04/mars-helicopter-ingenuity-nasa/618635/
> 3. https://github.com/readme/featured/nasa-ingenuity-helicopter
> 4. https://www.zdnet.com/article/flying-on-mars-fueled-with-open-source-software/
> 5. https://www.jpl.nasa.gov/missions/ingenuity

- F Prime(F´) 是什么？
- 为什么要在火星上进行有动力、有控制的飞行？
- 你能给我推荐一些关于 Python 和 Linux 的教程吗？

我了解了，机上系统用 C++ 框架更小巧紧凑，效率更高。使用 LINUX 系统更稳定。地上控制主要是决策和数据支持和分析，发挥PY的优势是显而易见的啦。我另外研究 F Prime(F´) 。 咱们可以聚焦下 JPL 的岗位：软件保障工程师 的职责，或许可以更能了解 PYTHON 的司职。

- Searching for: JPL software assurance engineer job description

> [JPL 的软件保障工程师的职责是为 JPL 的太空项目提供适当的质量保证工程要求](https://www.linkedin.com/jobs/view/bt-2073-qa-engineer-sme-part-time-remote-to-support-location-in-al-at-bastion-technologies-inc-3480569058)1。[他们需要生成质量保证程序和指导，执行静态代码分析和测试，与项目经理、任务经理和任务保障经理进行接口，确保软件符合 NASA 和 JPL 的标准](https://www.linkedin.com/jobs/view/bt-2073-qa-engineer-sme-part-time-remote-to-support-location-in-al-at-bastion-technologies-inc-3480569058)12。

> 软件保障工程师需要具备电气工程和软件工程的背景，熟悉 C/C++, Python, Java 等编程语言，以及 Linux/Unix 操作系统2。他们还需要具备批判性、质量导向和怀疑性的思维，能够主动从利益相关者那里获取信息，而不是完全依赖书面规范3。

> 如果你对 JPL 的软件保障工程师的职位感兴趣，你可以在 JPL 的招聘网站上查看更多信息45。

> Learn more:
> 1. [linkedin.com](https://www.linkedin.com/jobs/view/bt-2073-qa-engineer-sme-part-time-remote-to-support-location-in-al-at-bastion-technologies-inc-3480569058)
> 2. https://www.linkedin.com/in/lyle-barner
> 3. https://in.bebee.com/job/20230308-610232f1d12f37c005d13cb9e5374c31
> 4. https://www.jpl.jobs/
> 5. https://www.glassdoor.com/Jobs/NASA-Jet-Propulsion-Laboratory-Software-Engineer-Jobs-EI_IE116768.0,30_KO31,48.htm

可以结合 NASA JPL 的软件标准，介绍下 静态代码分析中 PYTHON 承担的角色？

- Searching for: NASA JPL software standards Python static code analysis

> 静态代码分析是一种在不运行程序的情况下检查代码的质量和安全性的方法1。NASA 的软件项目需要使用静态代码分析来确保代码符合项目团队制定的编码标准/准则12，并在系统集成和测试之前消除常见的编码错误1。

> Python 是一种动态类型的语言，这意味着它在运行时才检查变量的类型。这给静态代码分析带来了一些挑战，因为一些类型错误可能无法在编译时被发现。因此，Python 承担的角色是提供一些工具和技术来帮助开发者进行类型分析、注解和检查3。

> 例如，Python 提供了 typing 模块，可以让开发者为变量、函数参数和返回值指定类型提示3。这样可以提高代码的可读性和可维护性，也可以让静态代码分析工具更容易地检测出潜在的类型错误3。另外，Python 还有一些第三方库，如 mypy 和 pytype ，可以对 Python 代码进行静态类型检查，并生成报告或警告3。

> 总之，Python 承担的角色是通过提供类型相关的工具和库，来辅助 NASA 的软件项目进行静态代码分析，并提高代码的质量和安全性。

> Learn more:
> 1. https://swehb.nasa.gov/display/SWEHBVB/SWE-135+-+Static+Analysis
> 2. https://swehb.nasa.gov/display/SWEHBVC/SWE-061+-+Coding+Standards
> 3. https://sdtimes.com/nasas-10-rules-developing-safety-critical-code/
> 4. https://www.perforce.com/blog/kw/NASA-rules-for-developing-safety-critical-code
> 5. https://trs.jpl.nasa.gov/bitstream/handle/2014/43875/11-2798_A1b.pdf?sequence=1

PYTHON 用于静态代码分析，是只能分析 PYTHON 代码吗？还是在 NASA 和 JPL 会使用 PYTHON 工具分析所有软件的规范在静态下是否合格？

Python 用于静态代码分析，不一定是只能分析 Python 代码。有些 Python 工具可以分析其他语言的代码，比如 C/C++ 或 Java。例如，PyLint 是一个 Python 工具，可以检查 Python 代码的风格、错误和潜在的问题。但是，PyLint 也可以通过插件或扩展来支持其他语言的静态代码分析。

> 在 NASA 和 JPL ，使用 Python 工具分析所有软件的规范在静态下是否合格，可能取决于具体的项目和团队。不同的项目和团队可能有不同的编码标准/准则 ，以及不同的静态代码分析工具和方法。因此，并没有一个统一的答案来说明 NASA 和 JPL 是否会使用 Python 工具来分析所有软件。

> Received message. Python 用于静态代码分析，不一定是只能分析 Python 代码。有些 Python 工具可以分析其他语言的代码，比如 C/C++ 或 Java。例如，PyLint 是一个 Python 工具，可以检查 Python 代码的风格、错误和潜在的问题。但是，PyLint 也可以通过插件或扩展来支持其他语言的静态代码分析。 在 NASA 和 JPL ，使用 Python 工具分析所有软件的规范在静态下是否合格，可能取决于具体的项目和团队。不同的项目和团队可能有不同的编码标准/准则 ，以及不同的静态代码分析工具和方法。因此，并没有一个统一的答案来说明 NASA 和 JPL 是否会使用 Python 工具来分析所有软件。

- 我明白了。那么你觉得哪些语言或工具更适合进行静态代码分析呢？
- 你能给我介绍一下 PyLint 的基本用法吗？

明白了，你回答中的几个关键词，我都想了解下，他们在静态代码分析中的作用？pytype, pytyon 的 typing 模块，还有 PyLint. 

- Searching for: pytype, python typing module, pylint

> 好的，我来简单介绍一下这几个关键词在静态代码分析中的作用：

> - pytype 是一个 Python 工具，可以对 Python 代码进行类型推断和检查，不需要类型注解12。pytype 可以检测出一些常见的编码错误，比如拼写错误、函数调用错误等12。pytype 也可以强制执行用户提供的类型注解12。
> - Python 的 typing 模块是一个标准库模块，可以让开发者为变量、函数参数和返回值指定类型提示3。类型提示可以提高代码的可读性和可维护性，也可以让静态代码分析工具更容易地检测出潜在的类型错误3。typing 模块还提供了一些特殊的类型，比如 Any, Union, Callable , TypeVar, 和 Generic ，来表示更复杂的类型关系3。
> - PyLint 是一个 Python 工具，可以检查 Python 代码的风格、错误和潜在的问题45。PyLint 可以根据 PEP8 等编码规范给出建议和评分，帮助开发者改进代码质量5。PyLint 还可以通过插件或扩展来支持其他语言的静态代码分析4。

> 这些工具和模块都是在静态代码分析中有用的资源，你可以根据你自己的项目需求和喜好来选择使用哪些。

> Learn more:
> 1. [pypi.org](https://pypi.org/project/pytype/)
> 2. [github.com](https://github.com/google/pytype)
> 3. [docs.python.org](https://docs.python.org/3/library/typing.html)
> 4. [docs.pylint.org](https://docs.pylint.org/run.html)
> 5. [lynn-kwong.medium.com](https://lynn-kwong.medium.com/use-black-mypy-and-pylint-to-make-your-python-code-more-professional-b594512f4362)

感谢你的介绍，给我介绍了那么令人激动人心的项目。不早了，我要休息了。改天再请你帮忙。再见。


# [詹姆斯·默茨 充满激情的Python 主义者](https://realpython.com/team/jmertz/)

![输入图片说明](https://foruda.gitee.com/images/1678568470824592647/b7535d96_5631341.png "屏幕截图")

- [关于詹姆斯·默茨](https://realpython.com/team/jmertz/)

唽 | [GitHub](https://github.com/mertzjames) | [个人网站](https://mertzmusings.com/)

James是NASA喷气推进实验室（JPL）的一名充满激情的Python开发人员。当他不工作时，他通常会修补他的Raspberry Pi或Arduino套件之一，并试图让他的两个孩子也对它们感兴趣。


## 詹姆斯的教程：

- [如何充分利用 PyCon US](https://realpython.com/pycon-guide/)
- [在 Python 中读取和写入文件（指南）](https://realpython.com/read-write-files-python/)
- [一个 Python 主义者的假期愿望清单](https://realpython.com/python-holiday-wish-list/)
- [十大必看的 PyCon 演讲](https://realpython.com/must-watch-pycon-talks/)
- [记录 Python 代码：完整指南](https://realpython.com/documenting-python-code/)

## 詹姆斯贡献的教程：

- [Python 3 安装和设置指南](https://realpython.com/installing-python/)
- [真正的Python在PyCon US 2019](https://realpython.com/real-python-pycon-us/)
- [最好的蟒蛇书籍](https://realpython.com/best-python-books/)
- [Python 熊猫：你可能不知道的技巧和功能](https://realpython.com/python-pandas-tricks/)
- [记录 Python 代码：完整指南（课程）](https://realpython.com/courses/documenting-python-code/)
- [在 Python 中读取和写入文件（课程）](https://realpython.com/courses/reading-and-writing-files-python/)

## 个人网站

- [如何更改Circuit Playground Express和Circuit Python中的加速度计灵敏度](https://mertzmusings.com/how-to-change-the-accelerometer-sensitivity-in-circuit-playground-express-and-circuit-python/)  六月 13， 2019

  I recently ran into a situation that I wanted to know if I could adjust the sensitivity of the accelerometer on the Circuit Playground Express board made by Adafruit. Turns out that you can, but requires a little bit of hacking to do so:

  ```
  import adafruit_lis3dh
  from adafruit_circuitplayground.express import cpx
  
  cpx._lis3dh.range = adafruit_lis3dh.RANGE_16_G
  ```

  The accelerometer is the [LIS3DH device developed by ST](https://www.st.com/en/mems-and-sensors/lis3dh.html). There are actually 4 different settings for the accelerometer:

  - ±2g – RANGE_2_G
  - ±4g – RANGE_4_G
  - ±8g (default) – RANGE_8_G
  - ±16g – RANGE_16_G

  The ‘g’ represents gravity (9.8 meters per second^2) and the lower the setting, the higher the sensitivity. Setting it to will enable you to detect smaller changes in acceleration, but will also mean that you miss out on the bigger changes. Vice versa, will desensitize things, meaning you won’t pick up the smaller changes as much, but the bigger changes you’ll see better.`RANGE_2_GRANGE_16_G`

  我最近遇到了一个情况，我想知道我是否可以调整Adafruit制造的Circuit Playground Express板上加速度计的灵敏度。事实证明你可以，但需要一点黑客才能做到这一点：

  加速度计是意法半导体开发的LIS3DH器件。加速度计实际上有 4 种不同的设置：

  “g”表示重力（每秒 9.8 米^2），设置越低，灵敏度越高。将其设置为 将使您能够检测到加速度的较小变化，但也意味着您错过了较大的变化。反之亦然，会使事情脱敏，这意味着你不会接受较小的变化，但更大的变化你会看到得更好。RANGE_2_GRANGE_16_G

- [PyCon 2019：快速回顾](https://mertzmusings.com/pycon-2019-a-quick-lookback/)  五月 15， 2019

  :+1: :+1: :+1:  可以单独开一帖。

<h1 class="post-title">PyCon 2019: A Quick Lookback</h1>

<span class="post-meta">
Published by <span class="author"><a href="https://mertzmusings.com/author/mertzjames/">mertzjames</a></span> on <span class="date"><a href="https://mertzmusings.com/2019/05/">May 15, 2019</a></span></span>

<p class="last-updated">Last updated on May 28, 2019 </p>			

![输入图片说明](https://foruda.gitee.com/images/1678628398763919792/11874bd3_5631341.png "屏幕截图")

<p>This year marked my fourth year going to PyCon and has been by far the best.  I’ve decided to share with you a few of my experiences and recommendations.  To do this, I’ve broken this up into the following sections:</p>



<ul><li><strong>Volunteering:</strong> This year I did more volunteering than before and you should do more too.</li><li><strong>The “People” of PyCon 2019:</strong> I met quite a few people who made an impact on my PyCon experience who I’d like to share with you.</li><li><strong>Talks, Keynotes, and Tutorial Recommendations:</strong> Recommended talks, keynotes, or tutorials that I attended or watched.</li><li><strong>Swag:</strong> A quick look at this year’s swag, well one thing in particular.</li></ul>



<h2>Volunteering</h2>



<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter"><div class="wp-block-embed__wrapper">
<blockquote class="twitter-tweet" data-width="500" data-dnt="true"><p lang="en" dir="ltr">Volunteer opportunities at <a href="https://twitter.com/hashtag/pycon2019?src=hash&amp;ref_src=twsrc%5Etfw">#pycon2019</a> are still available. PyCon is in need of more session chair/runners and tutorial greeters. More info available here: <a href="https://t.co/QpzOUFO8JN">https://t.co/QpzOUFO8JN</a> <a href="https://t.co/SYG4wDVGTL">pic.twitter.com/SYG4wDVGTL</a></p>— PyCon US (@pycon) <a href="https://twitter.com/pycon/status/1118514571944370177?ref_src=twsrc%5Etfw">April 17, 2019</a></blockquote>
</div></figure>



<p>This year, prompted by <a href="https://realpython.com/pycon-guide/">my article on Real Python about PyCon</a>, I decided to do more volunteering at PyCon than I’ve done before and I’ve got to say that it augmented my experience tenfold. Why? First and foremost, I felt like I was no longer just a participant in the conference, but now also a contributor. Next, I was able to meet some people in a more personal manner that otherwise, I might not have been able to meet. Finally, I was given a small glimpse into the inner workings of how a conference of this scale is put together and managed. Hint: strong use of volunteers. </p>



<p>While at PyCon I took on several roles. I want to elaborate on what they were and involved doing and the benefits they brought me while volunteering. </p>



<p><strong>Tutorial Support (aka bouncer):</strong></p>



<p>When supporting a tutorial your job is one akin to that of a bouncer. You’ll check that the person coming in has indeed signed up for the tutorial. Sometimes I would get people that came to the wrong tutorial and would direct them to the correct room. Other times I had to inform people that the tutorials were paid add-on to the conference and that if they didn’t pay for it, they couldn’t attend. </p>



<p>I had the honor of supporting David Beasley’s tutorial this year. Because I was standing in the room helping out, he came up to me and started chatting with me. It was a surreal moment for me; here was a man whom I’ve read his books and listened to his talks in awe, having a casual conversation with me! </p>



<p><strong>Registration/Check-in</strong></p>



<p>When you first come to PyCon, you need to go to registration in order to get your badge, tickets for events and stuff, and a map/schedule of the conference. Almost everyone behind the desk are volunteers. What makes this area so great is that you get to quickly meet all sorts of people as they check in. I even got to check in Michael Kennedy from Python Bytes and Talk Python to Me! </p>



<p><strong>Swag Bag Stuffing Dance Party w/ Larry Hastings</strong></p>



<p>This is something that anyone can help out with and have lots of fun doing.  You simply walk, stroll, dance, or whatever <a href="http://larryhastings.com/mixes/">Larry Hastings’ mixes</a> may inspire you to do while holding a bag for others to stuff with all the good swag that we get.  Not only is this a lot of fun, but you will get an inside look at what we get before anyone else.  You might even get some stuff if there’s still some extra.</p>



<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter"><div class="wp-block-embed__wrapper">
<blockquote class="twitter-tweet" data-width="500" data-dnt="true"><p lang="en" dir="ltr">Come stuff bags <a href="https://twitter.com/pycon?ref_src=twsrc%5Etfw">@pycon</a>, it's a party!<a href="https://twitter.com/hashtag/PyCon2019?src=hash&amp;ref_src=twsrc%5Etfw">#PyCon2019</a> <a href="https://t.co/zFKslmURrb">pic.twitter.com/zFKslmURrb</a></p>— Daniel Wallace (@gtmanfred) <a href="https://twitter.com/gtmanfred/status/1124051906605793280?ref_src=twsrc%5Etfw">May 2, 2019</a></blockquote>
</div></figure>



<p><strong>Session Staff (Runner and Chair)</strong></p>



<p> The other thing that I did with regards to volunteering was being a Session Runner and Chair.  </p>



<p>As a session runner, your job is to make sure that the speaker gets to their assigned speaking room on time.  It’s a pretty simple enough job but a neat experience to see these speakers as they get ready before a talk. </p>



<p>As a session chair, you are the one that gets to introduce the person for the talk.  I decided to try this out to see how well I would do speaking in front of so many people as I have hopes and dreams of giving a talk myself some day.  As the session chair you also get inside access to the speakers and get to know them a little on a personal level.</p>


![输入图片说明](https://foruda.gitee.com/images/1678627924601951837/32dcb732_5631341.png "Me introducing Dr. Rachael Tatman for her talk")

<p>With both the session chair and runner, you also get a walkie talkie and earpiece which gets you inside access to what’s going on behind the scenes of running these talks.  Plus you get to look pretty awesome while walking around with them on.</p>



<h2>The “People” of PyCon</h2>



<p>This year I met and made so many new connections with people.  I want to share with you a few of these connections:</p>



<h3><a href="https://twitter.com/joelgarzatx">Joel Garza</a></h3>



<p>Joel was a fellow tutorial bouncer with me and was pivotal in securing me additional Circuit Playground Expresses for my Boy Scout troop.  Overall just a great guy you should be if you ever get the chance. </p>



<h3><a href="https://twitter.com/WillingCarol">Carol Willing</a></h3>



<p>Carol, I met by simple chance while volunteering.  She was in the Green room with all the other volunteers and I assumed that she was one of them.  She gave me some invaluable advice about volunteering where I live.  To my utter surprise, while <a href="https://youtu.be/8dDp-UHBJ_A?list=PLPbTDk1hBo3xvStrNcDZRUre1yqS4q3jl">watching the Keynote</a>, I found out that she is one of the members of the Python Steering Council!</p>



<h3><a href="https://twitter.com/realpython">Real Python Team</a> and Members</h3>



<p>I’ve been writing for Real Python for about a year now and it’s been amazing to work with some of the smartest minds that are using Python.  But with all that we’ve done working remotely with each other, we really were able to solidify our friendship and camaraderie by meeting up at PyCon and getting to know each other in real life.  It’s made the interactions that I have with them (even those that I didn’t get to mee) all the more enriching.  Besides volunteering, I would say that meeting up with this team was the biggest highlight of my attendance at PyCon. </p>



<p>Made even better though, was that the Real Python team decided to snag an open space for an hour for our first ever Real Python Summit, where people that were readers of Real Python could come and meet Dan Bader and some of the authors. </p>

![输入图片说明](https://foruda.gitee.com/images/1678633751355808406/1c83357b_5631341.jpeg "image-1.jpg")

The Inaugural Real Python Summit



<p>The turnout was far greater than we could’ve ever expected and it was humbling to hear about some of my articles being talked about as tools that people actually used and loved.</p>



<h3><a href="https://twitter.com/chalmer_lowe">Chalmer Lowe</a></h3>



<p>Chalmer and I go way back… all the way to PyCon 2016; He just doesn’t know it.  I first met Chalmer at his Booz|Allen|Hamilton booth that he had in PyCon 2016.  They had a capture the flag contest going at the time and surprisingly enough I won (there weren’t that many participants that year so that’s how I think I won).  I’ve seen him each year since in passing and this year he held a tutorial on Machine Learning using SciKit Learn.  I took the course and highly recommend (it made the list below).  </p>



<h2>Recommended Talks and Tutorials</h2>



<p>Below is a list of talks and tutorials that I attended or have seen since coming home and recommend watching.</p>



https://www.youtube.com/embed/kTdt0P0e3Qc?feature=oembed [BV1Jg4y1474e](https://www.bilibili.com/video/BV1Jg4y1474e/)

Chalmer Lowe - Scikit-learn, wrapping your head around machine learning - PyCon 2019

https://www.youtube.com/embed/qw5dBdTXLEs?feature=oembed  [BV1zT411r7Nn](https://www.bilibili.com/video/BV1zT411r7Nn/)

Rachael Tatman – Put down the deep learning: When not to use neural networks and what to do instead

https://www.youtube.com/embed/98s9YfoXB68?feature=oembed [BV1Lg4y147jn](https://www.bilibili.com/video/BV1Lg4y147jn/)

Geir Arne Hjelle – Plugins: Adding Flexibility to Your Apps

https://www.youtube.com/embed/pkCLMl0e_0k?feature=oembed [BV1do4y1B79P](https://www.bilibili.com/video/BV1do4y1B79P/)

David Beazley – Lambda Calculus From the Ground Up



https://www.youtube.com/embed/_GP9OpZPUYc?feature=oembed  [BV1fv4y1j72n](https://www.bilibili.com/video/BV1fv4y1j72n/)

Raymond Hettinger – Modern Solvers: Problems well-defined are problems solved

https://www.youtube.com/embed/dqdsNoApJ80?feature=oembed [BV1684y1A7Aj](https://www.bilibili.com/video/BV1684y1A7Aj/)

<strong>Anthony Shaw – Wily Python: Writing Simpler and More Maintainable Python</strong>



<h2>Circuit Playground Express (CPX)</h2>



<p>There was a lot of swag that was given out this year, but probably the coolest was the Circuit Playground Express board:</p>

![输入图片说明](https://foruda.gitee.com/images/1678628292187365624/2fbc7c7f_5631341.png "屏幕截图")

<a href="https://blog.adafruit.com/2019/05/02/how-its-made-the-thousands-of-digikey-adafruit-circuit-playgrounds-for-pycon-2019-in-cleveland-ohio-pycon2019-pycon/">https://blog.adafruit.com/2019/05/02/how-its-made-the-thousands-of-digikey-adafruit-circuit-playgrounds-for-pycon-2019-in-cleveland-ohio-pycon2019-pycon/</a>



<p>These board come pre-loaded with Circuit Python and have so many sensors, leds, and buttons theres literally thousands of things that you can do with them.  Turns out there were quite a few of them left over and thanks to Joel for speaking with the PSF team and adafruit, I was able to get 10 of these to take home and use to teach robotics to my local Boy Scout Troop.  Even more, Microsoft donated 10 of the <a href="https://www.adafruit.com/product/2769">CPX kits</a>!</p>


![输入图片说明](https://foruda.gitee.com/images/1678628735164597728/e514423b_5631341.jpeg "UNADJUSTEDNONRAW_thumb_d.jpg")

<p>With that, I’ve decided to work on a project creating lessons on robotics and programming in Python.  You can check out the GitHub page here:</p>



<p><a href="https://github.com/mertzjames/circuit_playground_examples">https://github.com/mertzjames/circuit_playground_examples</a></p>

# [PyCon 2019：快速回顾](https://mertzmusings.com/pycon-2019-a-quick-lookback/)

<span class="post-meta" _msttexthash="44370599" _msthash="30">发布者 <span class="author" _istranslated="1"><a href="https://mertzmusings.com/author/mertzjames/" _istranslated="1">mertzjames</a></span> on <span class="date" _istranslated="1"><a href="https://mertzmusings.com/2019/05/" _istranslated="1">五月 15， 2019</a></span></span>

<div class="post-content">
			<p class="last-updated" _msttexthash="28534116" _msthash="31">最后更新于 May 28， 2019</p>			

![输入图片说明](https://foruda.gitee.com/images/1678628398763919792/11874bd3_5631341.png "在这里输入图片标题")



<p _msttexthash="683428681" _msthash="32">今年是我第四年去PyCon，是迄今为止最好的一年。我决定与大家分享我的一些经验和建议。为此，我将其分为以下几个部分：</p>



<ul><li _msttexthash="170184430" _msthash="33"><strong _istranslated="1">志愿：</strong>今年我做的志愿服务比以前多了，你也应该做更多。</li><li _msttexthash="377129909" _msthash="34"><strong _istranslated="1">PyCon 2019的“人”：</strong>我遇到了不少对我的 PyCon 体验产生影响的人，我想和大家分享一下。</li><li _msttexthash="278281809" _msthash="35"><strong _istranslated="1">讲座、主题演讲和教程推荐：</strong>推荐我参加或观看的演讲、主题演讲或教程。</li><li _msttexthash="138998392" _msthash="36"><strong _istranslated="1">赃物：</strong>快速浏览一下今年的赃物，特别是一件事。</li></ul>



<h2 _msttexthash="4821765" _msthash="37">志愿</h2>



<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter"><div class="wp-block-embed__wrapper">
<blockquote class="twitter-tweet" data-width="500" data-dnt="true"><p lang="en" dir="ltr" _msttexthash="605861893" _msthash="38"><a href="https://twitter.com/hashtag/pycon2019?src=hash&amp;ref_src=twsrc%5Etfw" _istranslated="1">#pycon2019</a>的志愿者机会仍然可用。PyCon需要更多的会话主席/跑步者和教程迎宾员。更多信息可在此处获得：<a href="https://t.co/QpzOUFO8JN" _istranslated="1">https://t.co/QpzOUFO8JN</a> <a href="https://t.co/SYG4wDVGTL" _istranslated="1">pic.twitter.com/SYG4wDVGTL</a></p><font _mstmutation="1" _msttexthash="61884979" _msthash="39">— PyCon US （@pycon） <a href="https://twitter.com/pycon/status/1118514571944370177?ref_src=twsrc%5Etfw" _mstmutation="1" _istranslated="1">17 年 2019 月 &lt;&gt; 日</a></font></blockquote>
</div></figure>



<p _msttexthash="5654455742" _msthash="40">今年，<a href="https://realpython.com/pycon-guide/" _istranslated="1">在我关于PyCon的Real Python文章的</a>推动下，我决定在PyCon做比以前更多的志愿服务，我不得不说它使我的经验增加了十倍。为什么？首先，我觉得我不再只是会议的参与者，而是现在也是一个贡献者。接下来，我能够以更个人的方式结识一些人，否则我可能无法见面。最后，我稍微了解了如何组织和管理这种规模的会议的内部运作。提示：大量使用志愿者。</p>



<p _msttexthash="594390498" _msthash="41">在PyCon期间，我担任了几个角色。我想详细说明他们是什么，参与做什么，以及他们在志愿服务时给我带来的好处。</p>



<p><strong _msttexthash="67442791" _msthash="42">教程支持（又名保镖）：</strong></p>



<p _msttexthash="2440644375" _msthash="43">在支持教程时，您的工作类似于保镖的工作。您将检查进来的人是否确实注册了本教程。有时我会让那些来错教程的人，并引导他们到正确的房间。其他时候，我不得不通知人们，这些教程是会议的付费附加组件，如果他们不付费，他们就不能参加。</p>



<p _msttexthash="1671881458" _msthash="44">今年，我有幸支持大卫·比斯利的教程。因为我站在房间里帮忙，他走到我身边，开始和我聊天。这对我来说是一个超现实的时刻;这是一个人，我读过他的书，敬畏地听过他的演讲，和我闲聊！</p>



<p><strong _msttexthash="31824000" _msthash="45">注册/办理登机手续</strong></p>



<p _msttexthash="2616086460" _msthash="46">当您第一次来到 PyCon 时，您需要进行注册才能获得您的徽章、活动和东西的门票以及会议的地图/时间表。桌子后面的几乎每个人都是志愿者。这个区域之所以如此出色，是因为您可以在他们办理登机手续时快速结识各种各样的人。我甚至不得不从Python Bytes和Talk Python to Me签到Michael Kennedy！</p>



<p><strong _msttexthash="70705531" _msthash="47">赃物袋填充舞会与拉里·黑斯廷斯</strong></p>



<p _msttexthash="4010148350" _msthash="48">这是任何人都可以提供帮助并享受很多乐趣的事情。你只需走路、散步、跳舞，或者拉<a href="http://larryhastings.com/mixes/" _istranslated="1">里·黑斯廷斯（Larry Hastings）的混音</a>可能会激发你的灵感，同时拿着一个袋子让别人塞满我们得到的所有好赃物。这不仅很有趣，而且您将在其他人之前深入了解我们得到的东西。如果还有一些额外的东西，你甚至可能会得到一些东西。</p>



<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter"><div class="wp-block-embed__wrapper">
<blockquote class="twitter-tweet" data-width="500" data-dnt="true"><p lang="en" dir="ltr" _msttexthash="89406980" _msthash="49">来塞袋子<a href="https://twitter.com/pycon?ref_src=twsrc%5Etfw" _istranslated="1">@pycon</a>，这是一个派对！<a href="https://twitter.com/hashtag/PyCon2019?src=hash&amp;ref_src=twsrc%5Etfw" _istranslated="1">#PyCon2019</a> <a href="https://t.co/zFKslmURrb" _istranslated="1">pic.twitter.com/zFKslmURrb</a></p><font _mstmutation="1" _msttexthash="89399661" _msthash="50">— 丹尼尔·华莱士 （@gtmanfred） <a href="https://twitter.com/gtmanfred/status/1124051906605793280?ref_src=twsrc%5Etfw" _mstmutation="1" _istranslated="1">2 年 2019 月 &lt;&gt; 日</a></font></blockquote>
</div></figure>



<p><strong _msttexthash="79484171" _msthash="51">会议工作人员（跑步者和主席）</strong></p>



<p _msttexthash="175525051" _msthash="52">我在志愿服务方面所做的另一件事是成为会议运行者和主席。</p>



<p _msttexthash="1070938713" _msthash="53">作为会话运行者，您的工作是确保演讲者准时到达指定的演讲室。这是一项非常简单的工作，但可以看到这些演讲者在演讲前做好准备，这是一种整洁的体验。</p>



<p _msttexthash="1637375077" _msthash="54">作为会议主席，您是介绍演讲人的人。我决定尝试一下，看看我会在这么多人面前演讲，因为我有希望和梦想有一天自己做一个演讲。作为会议主席，您还可以进入演讲者内部，并在个人层面上了解他们。</p>


![输入图片说明](https://foruda.gitee.com/images/1678627924601951837/32dcb732_5631341.png "在这里输入图片标题")

我介绍Rachael Tatman博士的演讲



<p _msttexthash="916364878" _msthash="56">通过会议主席和跑步者，您还可以获得对讲机和听筒，让您了解运行这些演讲的幕后情况。另外，当你带着它们四处走动时，你会看起来非常棒。</p>



<h2 _msttexthash="16952754" _msthash="57">PyCon的“人民”</h2>



<p _msttexthash="359150077" _msthash="58">今年，我遇到了人们，并与人们建立了许多新的联系。我想与大家分享其中的一些联系：</p>



<h3><a href="https://twitter.com/joelgarzatx" _msttexthash="15695979" _msthash="59">乔尔·加尔萨</a></h3>



<p _msttexthash="923910988" _msthash="60">乔尔是我的一名辅导保镖，在为我的童子军部队确保额外的巡回游乐场快车方面发挥了关键作用。总的来说，如果你有机会，你应该是一个伟大的人。</p>



<h3><a href="https://twitter.com/WillingCarol" _msttexthash="8114405" _msthash="61">卡罗尔·</a></h3>



<p _msttexthash="2104708242" _msthash="62">卡罗尔，我在志愿服务时偶然相遇。她和所有其他志愿者一起在绿色房间里，我以为她是其中之一。她给了我一些关于我住的地方做志愿者的宝贵建议。令我非常惊讶的是，在<a href="https://youtu.be/8dDp-UHBJ_A?list=PLPbTDk1hBo3xvStrNcDZRUre1yqS4q3jl" _istranslated="1">观看主题演讲</a>时，我发现她是 Python 指导委员会的成员之一！</p>



<h3 _msttexthash="39287807" _msthash="63"><a href="https://twitter.com/realpython" _istranslated="1">真正的Python团队</a>和成员</h3>



<p _msttexthash="5418809539" _msthash="64">我已经为Real Python编写了大约一年的时间，与一些使用Python的最聪明的人一起工作真是太棒了。但是，通过我们彼此远程合作所做的一切，我们真的能够通过在 PyCon 见面并在现实生活中相互了解来巩固我们的友谊和友情。它使我与他们之间的互动（甚至是那些我没有接触到的互动）更加丰富。除了志愿服务，我想说的是，与这个团队会面是我参加PyCon的最大亮点。</p>



<p _msttexthash="876249569" _msthash="65">更棒的是，Real Python团队决定为我们有史以来的第一次Real Python峰会腾出一个小时的空地，在那里，Real Python的读者可以来见Dan Bader和一些作者。</p>

![输入图片说明](https://foruda.gitee.com/images/1678633751355808406/1c83357b_5631341.jpeg "在这里输入图片标题")

首届真正的Python峰会



<p _msttexthash="455937482" _msthash="67">投票率远远超过我们的预期，听到我的一些文章被谈论为人们实际使用和喜爱的工具，我感到羞愧。</p>



<h3><a href="https://twitter.com/chalmer_lowe" _msttexthash="13647426" _msthash="68">查尔默·洛</a></h3>



<p _msttexthash="6342796837" _msthash="69">查尔默和我回到了很久以前...一直到PyCon 2016;他只是不知道。我第一次见到查尔默是在他的酒馆|艾伦|汉密尔顿在PyCon 2016上的展位。他们当时有一个夺旗比赛，令人惊讶的是我赢了（那一年没有那么多参与者，所以这就是我认为我获胜的方式）。从那以后，我每年都见过他，今年他举办了一个关于使用SciKit Learn进行机器学习的教程。我参加了这门课程并强烈推荐（它列出了下面的列表）。</p>



<h2 _msttexthash="25574419" _msthash="70">推荐讲座和教程</h2>



<p _msttexthash="201662682" _msthash="71">以下是我回家后参加或看过的讲座和教程列表，并推荐观看。</p>



Chalmer Lowe - Scikit-learn, wrapping your head around machine learning - PyCon 2019

https://www.youtube.com/embed/kTdt0P0e3Qc?feature=oembed

https://www.youtube.com/embed/qw5dBdTXLEs?feature=oembed

Rachael Tatman – 放下深度学习：何时不使用神经网络以及该怎么做

https://www.youtube.com/embed/98s9YfoXB68?feature=oembed

Geir Arne Hjelle – 插件：为您的应用程序增加灵活性


https://www.youtube.com/embed/pkCLMl0e_0k?feature=oembed

David Beazley – Lambda Calculus From the Ground Up


https://www.youtube.com/embed/_GP9OpZPUYc?feature=oembed

雷蒙德·赫廷格 – 现代求解者：定义明确的问题就是解决的问题



https://www.youtube.com/embed/dqdsNoApJ80?feature=oembed

Anthony Shaw – Wily Python：编写更简单、更易于维护的 Python

<h2 _msttexthash="52759525" _msthash="77">赛道游乐场快线 （CPX）</h2>



<p _msttexthash="139012822" _msthash="78">今年有很多赃物，但最酷的可能是Circuit Playground Express板：</p>


![输入图片说明](https://foruda.gitee.com/images/1678628292187365624/2fbc7c7f_5631341.png "在这里输入图片标题")

<a href="https://blog.adafruit.com/2019/05/02/how-its-made-the-thousands-of-digikey-adafruit-circuit-playgrounds-for-pycon-2019-in-cleveland-ohio-pycon2019-pycon/" _msttexthash="15139813" _msthash="80">https://blog.adafruit.com/2019/05/02/how-its-made-the-thousands-of-digikey-adafruit-circuit-playgrounds-for-pycon-2019-in-cleveland-ohio-pycon2019-pycon/</a></figcaption></figure>



<p _msttexthash="3334518018" _msthash="81">这些板子预装了Circuit Python，并且有如此多的传感器，LED和按钮，你可以用它们做成千上万的事情。事实证明，他们中有很多人被留下来，感谢乔尔与PSF团队和adafruit交谈，我能够得到其中的10个带回家，用来教我当地的童子军机器人技术。更重要的是，微软捐赠了 10 个 <a href="https://www.adafruit.com/product/2769" _istranslated="1">CPX 套件</a>！</p>



![输入图片说明](https://foruda.gitee.com/images/1678628735164597728/e514423b_5631341.jpeg "在这里输入图片标题")



<p _msttexthash="11913174" _msthash="82">With that, I’ve decided to work on a project creating lessons on robotics and programming in Python.  You can check out the GitHub page here:</p>



<p><a href="https://github.com/mertzjames/circuit_playground_examples" _msttexthash="2701777" _msthash="83">https://github.com/mertzjames/circuit_playground_examples</a></p>

---

[PyCon Singapore 2019 Registration, Singapore - EventNook](https://www.eventnook.com/event/pyconsingapore2019/)
eventnook.comeventnook.com|2400 × 1200 jpeg|图像可能受版权保护。

![输入图片说明](https://foruda.gitee.com/images/1678635998526969459/2769e9ca_5631341.jpeg "coverimage_19081314142528_pycon_singapore_2019-v5.jpg")