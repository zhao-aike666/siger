##  **$\sin x=2$?**
$~~~~~~$在中学的数学课上，我们学到$\sin x$的值域为[-1,1],$ \sin x$不可能等于2，但是这只是x属于实数的情况，如果将x拓展到复数域，什么时候$ \sin x=2$?
$~~~~~~$我们不妨设$x \in \mathbb{C}$, $ z=a+bi,a、b \in \mathbb{R}$，有
$$ \sin x=c,c>1$$

$~~~~~~$根据欧拉公式$$ \begin{array}{cols}&e^{iz}= \cos z+i\sin z \quad ① \\  
&e^{-iz}= \cos( -z)+ i\sin (-z)= \cos z- i\sin z \quad ② \end{array}$$
$~~~~~~$①-②得$$ \begin{array}{cls}e^{iz}-e^{-iz}=2i \sin z\\
 \sin z= \frac{ \large{e}^{iz}-e^{-iz}}{ \large{2i}}=c \end{array}$$
$~~~~~~$带入$z=a+bi$得
$$  \begin{array}{cols}c&= \frac{ \large{e^{i(a+bi)}-e^{-i(a+bi)}}}{ \large{2i}}\\
&= \frac{ \large{e^{ai}e^{-b}-e^{-ai}e^{b}}}{ \large{2i}}
 \end{array}$$
$~~~~~~$再次使用欧拉公式，带入$e^{ai}= \cos a+i \sin a, e^{-ai}= \cos a-i \sin a$得
$$ \begin{array}{cols} c&=\frac{ \large{e^{-b}( \cos a+i \sin a)-e^b( \cos a-i \sin a)}}{ \large{2i}}\\
&= \frac{ \large{(e^{-b}-e^b) \cos a+(e^{-b}+e^b)i \sin a}}{ \large{2i}} \end{array}$$
$~~~~~~$上下同时除以$i$得
$$ \begin{array}{cls}c&=c+0i\\
&= [\frac{1}{2}(e^b-e^{-b})\cos a]i+\frac{1}{2}(e^{-b}+e^b)\sin a \end{array}$$
$~~~~~~$得到
$$ 
\begin{cases}\frac{1}{2}(e^b-e^{-b}) \cos a=0\\ 
\frac{1}{2}(e^{-b}+e^b) \sin a=c \end{cases}
$$
$~~~~~~$为了满足$(e^b-e^{-b}) \cos a=0$，需要$e^b-e^{-b}=0$,即$b=0$，或$ \cos a=0$，即
$a= \frac{ \pi}{2}+k,k \in \mathbb{Z}$.
$~~~~~~$当$b=0$时
$$ \frac{1}{2}(1+1) \sin a=c$$
$~~~~~~$因为$a \in \mathbb{R},c>1$,所以此时无解。
$~~~~~~$如果$a= \frac{ \pi}{2}+k$,此时$\sin a=1$或 $\sin a=-1$,当$ \sin a=1$时
$$  \begin{array}{cls} \frac{1}{2}(e^{-b}+e^b)=c\\
e^b \times(e^{-b}+e^b-2c)=0 \times e^b\\
1+e^{2b}-2ce^b=0 \end{array}$$
$~~~~~~$设$e^b=u$
$$ \begin{array}{cls}1+u^2-2cu=0\\
u= \frac{ \large{2c \pm \sqrt{4c^2-4}}}{ \large{2}}\\
=c \pm \sqrt{c^2-1}\\
e^b=u=c \pm \sqrt{c^2-1}\\
b= \ln(c \pm \sqrt{c^2-1}) \end{array}$$
$~~~~~~$当$\sin a=-1$时
$$  \begin{array}{cls} -\frac{1}{2}(e^{-b}+e^b)=c\\
e^b \times(e^{-b}+e^b+2c)=0 \times e^b\\
1+e^{2b}+2ce^b=0\\
1+u^2+2cu=0 \\
u= \frac{ \large{-2c \pm \sqrt{4c^2-4}}}{ \large{2}}\\
=-c \pm \sqrt{c^2-1}=e^b<0\end{array}$$
$~~~~~~$因此$ \sin a=-1$时无意义，所以
$$z=a+bi= \frac{ \pi}{2}+2k \pi+ \ln(c \pm \sqrt{c^2-1})i ,k \in \mathbb{Z}$$
$~~~~~~$当$c=2$时
$$z= \frac{ \pi}{2}+2k \pi+ \ln(2 \pm \sqrt{3})i,k \in \mathbb{Z}$$