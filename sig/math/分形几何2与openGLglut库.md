- [分形几何2](https://gitee.com/cchen55/siger/issues/I4SLNS)
- [ubuntu 配置 openGL glut库 ：Mandelbrot.cpp](https://gitee.com/cchen55/siger/issues/I4SLNS#note_8931463_link)
- [基于OpenGL的三维动画编程——太阳系行星转动交互设计](https://gitee.com/cchen55/siger/issues/I4QUJQ)

<p><img width="706px" src="https://foruda.gitee.com/images/1666394609653292776/01972722_5631341.jpeg" title="357分形几何2.jpg"></p>

本专题作为 OPENGL 环境配置的范例收藏，附示范源码和详细的环境配置步骤。

# 分形几何介绍

## 	简介

分形的生成是基于一个不断迭代的方程式。分形有几种类型，可以分别依据表现出的精确自相似性、半自相似性和统计自相似性来定义。

虽然分形是一个数学构造，它们同样可以在自然界中被找到，这使得它们被划入艺术作品的范畴。分形在医学、土力学、地震学和技术分析中都有应用。

## 	产生

在二十世纪七十年代，法国数学家曼德尔勃罗特在他的著作中探讨了英国的海岸线有多长？这个问题依赖于【测量时所使用的尺度】。

如果用公里作测量单位，从几米到几十米的一些曲折会被忽略；改用米来做单位，测得的总长度会增加，但是一些厘米量级以下的就不能反映出来。

由于涨潮落潮使海岸线的水陆分界线具有各种层次的不规则性。海岸线在大小两个方向都有自然的限制，取不列颠岛外缘上几个突出的点，用直线把它们连起来，得到海岸线长度的一种下界。使用比这更长的尺度是没有意义的。还有海沙石的最小尺度是原子和分子，使用更小的尺度也是没有意义的。在这两个自然限度之间，存在着可以变化许多个数量级的“无标度”区，长度不是海岸线的定量特征，就要用分维。

数学家寇赫从一个正方形的“岛”出发，始终保持面积不变，把它的“海岸线”变成无限曲线，其长度也不断增加，并趋向于无穷大。以后可以看到，分维才是“寇赫岛”海岸线的确切特征量，即海岸线的分维均介于1到2之间。

这些自然现象，特别是物理现象和分形有着密切的关系，银河系中的若断若续的星体分布，就具有分维的吸引子。多孔介质中的流体运动和它产生的渗流模型，都是分形的研究对象。这些促使数学家进一步的研究，从而产生了分形几何学。

电子计算机图形显示协助了人们推开分形几何的大门。这座具有无穷层次结构的宏伟建筑，每一个角落里都存在无限嵌套的迷宫和回廊，促使数学家和科学家深入研究。

法国数学家曼德尔勃罗特这位计算机和数学兼通的人物，对分形几何产生了重大的推动作用。他在1975、1977和1982年先后用法文和英文出版了三本书，特别是《分形——形、机遇和维数》以及《自然界中的分形几何学》，开创了新的数学分支——分形几何学。

## 	基本思想

分形几何学的基本思想是：客观事物具有自相似的层次结构，局部与整体在形态、功能、信息、时间、空间等方面具有统计意义上的相似性，称为【自相似性】。

例如，一块磁铁中的每一部分都像整体一样具有南北两极，不断分割下去，每一部分都具有和整体磁铁相同的磁场。这种自相似的层次结构，适当的放大或缩小几何尺寸，整个结构不变。

## 	计算机分形几何典型实例——芒德布罗集

芒德布罗集是最惊人的数学对象之一。图1给出了一个芒德布罗集的图形。仔细观察会发现，每一处边缘都具有“叶子”的特征。留意图1的底部。将其放大，得到图2。现在，新的“叶子”成分出现了。再将图2的底部放大，可观察到图3所示的令人眼花缭乱的结构。图4和图5是放大更多的图形。每副图都显示出了一种丰富的、精细的结构。

![](https://images.gitee.com/uploads/images/2022/0203/222622_829448ad_10096140.png)
	
互联网上有很多计算机程序可用来探索芒德布罗集。随着图形的放大，越来越精细的结构不断出现。

- - - -

本期专题《探索分形几何的奥秘》就来和大家讲讲，如何用程序绘制美丽的分形几何图案，与此同时实现动态且具有一定交互性质的效果。

# 如何用程序实现动态的分形几何图案？

##         开发环境

### 1. 本地环境

Windows10、Visual Studio2019

另附VS2019安装教程：  
[vs2019安装和使用教程（详细）_悲恋花丶无心之人的博客-CSDN博客_vs2019安装教程](https://blog.csdn.net/qq_36556893/article/details/88603729)

### 2. 开发环境

OpenGL

## 调用的库

- GLUT  
说明：本次程序使用到了GLUT的工具包，但GLUT并不是OpenGL所必须的。不过，它会给我们的学习带来一定的方便，因此推荐安装。

### 另附安装教程：

请参考[openGL 初学 glut库的应用_随风奔跑的专栏-CSDN博客_glut库什么作用](https://blog.csdn.net/zhtsuc/article/details/5293963)  
Windows环境下的GLUT下载地址：（大小约为150k）  
http://www.opengl.org/resources/libraries/glut/glutdlls37beta.zip  
无法从以上地址下载的话请使用下面的连接:  
http://upload.programfan.com/upfile/200607311626279.zip

### Windows环境下安装GLUT的步骤：

1. 将下载的压缩包解开，将得到5个文件
2. 在“我的电脑”中搜索“gl.h”，并找到其所在文件夹（如果是VisualStudio2005，则应该是其安装目录下面的“VC/PlatformSDK/include/gl文件夹”）。把解压得到的glut.h放到这个文件夹。
3. 把解压得到的glut.lib和glut32.lib放到静态函数库所在文件夹（如果是VisualStudio2005，则应该是其安装目录下面的“VC/lib”文件夹）。
4. 把解压得到的glut.dll和glut32.dll放到操作系统目录下面的system32文件夹内。（典型的位置为：C:/Windows/System32）

## 代码中包含的主要交互功能

1. 鼠标右键 —— 暂停/继续
2. 方向键 —— 移动
3. PGUP —— 放大
4. PGDN —— 缩小
5. HOIME —— 三维空间放大
6. END —— 三维空间缩小
7. 鼠标左键 —— 移动三维空间

## 如何改变原始图像的效果？
### 1. 尝试改迭代关系

![](%E5%88%86%E5%BD%A2%E5%87%A0%E4%BD%95%E4%BB%8B%E7%BB%8D/C10ED2F4-7B38-444E-BAB3-82BEEC3677EA.png)

对这里的代码作修改，即改变迭代关系，理论上就可以呈现不同的动态效果，大家有兴趣可以自己试试。

### 2、调参

根据分形几何可以实现动态效果的原理：  
【不同的像素坐标映射比例会决定分形图的大小。】  
【不同的像素坐标映射加上偏移量会决定分形图的偏移。】等等……  
大家也可以调整参数设置，从而实现不一样的视觉效果。

### 3、色彩搭配

大家也可以根据自己的审美调整色彩的搭配，方法主要是【调整RGB色彩通道的参数】。

关于得到想要颜色的RGB参数，这里也推荐一个好用的网站：[在线颜色选择器 | RGB颜色查询对照表](http://tools.jb51.net/static/colorpicker/index.html)

## 其他

代码最终呈现效果截图：

![](%E5%88%86%E5%BD%A2%E5%87%A0%E4%BD%95%E4%BB%8B%E7%BB%8D/214706_ca660cb5_10096140.png)
	
开源的代码基本可以实现以上预期的功能，但有一些小bug需要根据自己的开发环境作调整。  
例如，有的环境不适配#include <GL/glut.h>这样的语法，  
我们要将其改成#include <glut>。

- - - -

写在文章最后的温馨提示：新建项目时务必记得提前下载glut包、配置好OpenGL环境！

# [ubuntu配置openGL glut库](https://blog.csdn.net/xiadidi/article/details/50867241) 

xiadidi 于 2016-03-12 20:25:04 发布 5165
分类专栏： openGL 文章标签： ubuntu opengl glut

openGL开发有比较好用的库：glut，本文讲述ubuntu该如何配置：

以下为安装过程（需要root权限）：

```
1、 apt-get install build-essential 
2、 apt-get install libgl1-mesa-dev  
3、 apt-get install libglu1-mesa-dev 
4、 apt-get install libglut-dev 
```

运行4后若出现 E:Unable to locate package libglut-dev, 可安装freeglut库（一个开源的glut库）替代：

```
apt-get install freeglut3-dev
```

安装完上面的库以后就可以写测试程序来验证是否成功安装了，本文以画简单直线来说明，代码如下：


```
#include <GL/glut.h>


void display(void)
{
    glClear(GL_COLOR_BUFFER_BIT);
    glBegin(GL_LINES);

    glVertex2f(0, 0);

    glVertex2f(1.0f, 1.0f);

    glEnd();
    glFlush();
}

int main(int argc,char *argv[])

{
    glutInit(&argc,argv);
    glutInitDisplayMode(GLUT_RGB | GLUT_SINGLE);

    glutInitWindowSize(300,300);
    glutInitWindowPosition(100,100);
    glutCreateWindow("OpenGL Window");
    glutDisplayFunc(display);
    glutMainLoop();

    return 0;
}

```
 
使用gcc 编译并链接需要的openGL库：


```
gcc -o test test.c -lGL -lGLU -lglut
```


成功后便会画出一条直线.

如果用的是c++文件且用到glew库，则是用下面的命令编译：

 

```
apt-get install libglew-dev
g++ -o test test.cpp -lGL -lGLU -lglut -lGLEW
```

---

【[笔记](https://gitee.com/cchen55/siger/issues/I4SLNS#note_8930358_link)】

- [测试分形几何源码 Mandelbrot.cpp](https://gitee.com/cchen55/siger/issues/I4SLNS#note_8930457_link)

- [Windows环境下安装GLUT的步骤](https://gitee.com/cchen55/siger/tree/master/%E5%88%86%E5%BD%A2%E5%87%A0%E4%BD%95#windows%E7%8E%AF%E5%A2%83%E4%B8%8B%E5%AE%89%E8%A3%85glut%E7%9A%84%E6%AD%A5%E9%AA%A4)：（[笔记](https://gitee.com/cchen55/siger/issues/I4SLNS#note_8930558_link)）

  - bing "UBUNTU环境下安装GLUT的步骤："

    - ubuntu配置openGL glut库_xiadidi的专栏-CSDN博客  【[笔记转载](https://gitee.com/cchen55/siger/issues/I4SLNS#note_8930358_link)】  
      https://blog.csdn.net/xiadidi/article/details/50867241

      > 2016-3-12 · Ubuntu下安装OpenGL/Glut库 OpenGL（全写Open Graphics Library）是个定义了一个跨编程语言、跨平台的编程接口的规格，它用于三维图象（二维的亦可）。OpenGL是个专业的图形程序接口，是一个功能强大，调用方便的底层图形库。 ubuntu下安装OpenGL

  1. [apt-get install build-essential](https://gitee.com/cchen55/siger/issues/I4SLNS#note_8930594_link)

  2. [apt-get install libgl1-mesa-dev](https://gitee.com/cchen55/siger/issues/I4SLNS#note_8930605_link)

  3. [apt-get install libglu1-mesa-dev](https://gitee.com/cchen55/siger/issues/I4SLNS#note_8930642_link)

  4. [apt-get install libglut-dev](https://gitee.com/cchen55/siger/issues/I4SLNS#note_8930653_link)

  [sudo apt-get install freeglut3-dev](https://gitee.com/cchen55/siger/issues/I4SLNS#note_8930667_link)

  [![输入图片说明](https://images.gitee.com/uploads/images/2022/0228/193036_0b3fef69_5631341.png "2022-02-28 19-30-15屏幕截图.png")](https://gitee.com/cchen55/siger/issues/I4SLNS#note_8930683_link)

- [apt-get install libglew-dev](https://gitee.com/cchen55/siger/issues/I4SLNS#note_8930992_link)

  - [编译 Mandelbrot.cpp](https://gitee.com/cchen55/siger/issues/I4SLNS#note_8931009_link)

  - [gcc -o Mandelbrot Mandelbrot.cpp -lGL -lGLU -lglut](https://gitee.com/cchen55/siger/issues/I4SLNS#note_8931234_link)

    [bing ubuntu error: ‘CLK_TCK’ was not declared in this scope](https://gitee.com/cchen55/siger/issues/I4SLNS#note_8931267_link)

    - [调试经验——C语言中查看CLK_TCK常数](https://blog.csdn.net/hpdlzu80100/article/details/102112347) （[转载](https://gitee.com/cchen55/siger/issues/I4SLNS#note_8931343_link)）

    - [CLK_TCK](https://blog.csdn.net/weixin_43782908/article/details/100174143) （[转载](https://gitee.com/cchen55/siger/issues/I4SLNS#note_8931392_link)）

- [GNU nano 5.4 Mandelbrot.cpp](https://gitee.com/cchen55/siger/issues/I4SLNS#note_8931463_link)

  https://gitee.com/cchen55/siger/issues/I4R77Y

  ![输入图片说明](https://images.gitee.com/uploads/images/2022/0228/195702_639f640d_5631341.png "2022-02-28 19-55-38屏幕截图.png")

- CLK_TCK_百度百科 - Baidu Baike
  https://baike.baidu.com/item/CLK_TCK

  > 2021-8-3 · CLK_TCK，计算机术语。TC2.0中头文件time.h下宏定义的符号常量。在VC++6.0中也有关于CLK_TCK的宏定义。在两个版本中CLK_TCK的值是不同的。

- 修改迭代关系

  ![](https://images.gitee.com/uploads/images/2022/0228/235815_0e72ccec_10096140.png)

  对这里的代码作修改，即改变迭代关系，可以呈现不同的动态效果。

  > 芒德布罗集是最惊人的数学对象之一。图1给出了一个芒德布罗集的图形。仔细观察会发现，每一处边缘都具有“叶子”的特征。留意图1的底部。将其放大，得到图2。现在，新的“叶子”成分出现了。再将图2的底部放大，可观察到图3所示的令人眼花缭乱的结构。图4和图5是放大更多的图形。每副图都显示出了一种丰富的、精细的结构。

    ![](https://images.gitee.com/uploads/images/2022/0301/000209_c0065ce7_10096140.png) 

  对这期专题的细节越来越清晰了，万物相通的道理通过分形几何体现的淋漓尽致。:pray: