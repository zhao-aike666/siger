<a href="氢科会WYSS2022.md" target="_blank"><img align="left" width="63.99%" src="https://foruda.gitee.com/images/1668315802012081452/6072be89_5631341.jpeg"></a> <a target="_blank" href="https://gitee.com/men-jianwu/menjianwu/issues/I60VIF#note_14403775_link"><img title="445." width="15.56%" src="https://foruda.gitee.com/images/1668570717455082540/3b5ca9b2_5631341.jpeg"></a> <a target="_blank" href="https://gitee.com/men-jianwu/menjianwu/issues/I60VIF#note_14404094_link"><img title="446." width="15.56%" src="https://foruda.gitee.com/images/1668614132937165036/3d127d91_5631341.jpeg"></a> <a target="_blank" href="https://gitee.com/men-jianwu/menjianwu/issues/I60VIF#note_14404394_link"><img title="447." width="15.56%" src="https://foruda.gitee.com/images/1668618414964065939/cca1ca30_5631341.jpeg"></a> <a target="_blank" href="https://gitee.com/men-jianwu/menjianwu/issues/I60VIF#note_14404466_link"><img title="448." width="15.56%" src="https://foruda.gitee.com/images/1668619276462681528/4527e124_5631341.jpeg"></a> <a target="_blank" href="https://gitee.com/men-jianwu/menjianwu/issues/I60VIF#note_14405109_link"><img title="449." width="15.56%" src="https://foruda.gitee.com/images/1668620196025268704/fa03772b_5631341.jpeg"></a> <a target="_blank" href="https://gitee.com/men-jianwu/menjianwu/issues/I60VIF#note_14405152_link"><img title="450." width="15.56%" src="https://foruda.gitee.com/images/1668648934249612511/e6aa8a19_5631341.jpeg"></a> <a target="_blank" href="https://gitee.com/men-jianwu/menjianwu/issues/I60VIF#note_14405189_link"><img title="451." width="15.56%" src="https://foruda.gitee.com/images/1668649989546132534/7a0e634e_5631341.jpeg"></a> <a target="_blank" href="https://gitee.com/men-jianwu/menjianwu/issues/I60VIF#note_14405221_link"><img title="452." width="15.56%" src="https://foruda.gitee.com/images/1668652949337483369/9d191598_5631341.jpeg"></a> 

# 较早前专题

[<img title="303." width="15.693%" src="https://foruda.gitee.com/images/1664711566995128021/1ecbd503_5631341.jpeg">](https://gitee.com/flame-ai/siger/issues/I5SQ7J#note_13473892_link)  [<img title="310." width="15.693%" src="https://foruda.gitee.com/images/1664974326660511233/ece60166_5631341.jpeg">](https://gitee.com/men-jianwu/menjianwu/issues/I5U6BL) [<img title="377." width="15.693%" src="https://foruda.gitee.com/images/1666884471980102377/1f2830be_5631341.jpeg">](https://gitee.com/men-jianwu/menjianwu/issues/I5XOOT) [<img title="417." width="15.693%" src="https://foruda.gitee.com/images/1668188684240766939/c7eaea62_5631341.jpeg">](氢能第一课.md) [<img title="418." width="15.693%" src="https://foruda.gitee.com/images/1668240487450692080/27d7c36b_5631341.jpeg">](https://gitee.com/men-jianwu/menjianwu/issues/I5U83T) [<img title="419." width="15.693%" src="https://foruda.gitee.com/images/1668244844917008775/04fd7291_5631341.jpeg">](氢燃料电池.md) [<img title="420." width="15.693%" src="https://foruda.gitee.com/images/1668248476282486897/0ffa4c95_5631341.jpeg">](https://gitee.com/men-jianwu/menjianwu/issues/I5U83C) [<img title="421." width="15.693%" src="https://foruda.gitee.com/images/1668252043595260123/7d671252_5631341.jpeg">](BYD布局氢燃料电池.md) [<img title="422." width="15.693%" src="https://foruda.gitee.com/images/1668254935236141910/502edb6b_5631341.jpeg">](https://gitee.com/men-jianwu/menjianwu/issues/I5U49L) [<img title="423." width="15.693%" src="https://foruda.gitee.com/images/1668256422891287646/972c84ae_5631341.jpeg">](https://gitee.com/men-jianwu/menjianwu/issues/I5UBYL#note_14340944_link) [<img title="424." width="15.693%" src="https://foruda.gitee.com/images/1668257394843172282/1596eb86_5631341.jpeg">](https://gitee.com/men-jianwu/menjianwu/issues/I5W7GY) [<img title="425." width="15.693%" src="https://foruda.gitee.com/images/1668258877652884043/770ba161_5631341.jpeg">](https://gitee.com/men-jianwu/menjianwu/issues/I5YOVH)

# [氢春号正向我们驶来！](https://gitee.com/men-jianwu/menjianwu/issues/I5U4HE)

> 首期专题的主图，乘风破浪。在众多风平浪静的照片中，我最中意这张，任何的花团锦簇的繁荣的事业都不是一帆风顺的，氢能更是人类在追求能源安全，可持续发展道路上一个里程碑，同样需要不断求索，方能驶得彼岸，而旅程永远没有终点，只有短暂地停靠和加油，这是拥有进取心的航海家的精神所在，以氢春号为名，开启这个崭新的频道，预祝 SIGer 早日真正拥有自己的 “[氢春](https://gitee.com/flame-ai/siger/tree/master/sig/%E6%B0%A2%E6%98%A5)号”。 @[men-jianwu](https://gitee.com/men-jianwu) 

  - 转自：https://gitee.com/flame-ai/siger/issues/I5SQ7J#note_13473892_link

    > @[Yaonunu](https://gitee.com/yaonunu) 任何的花团锦簇的繁荣的事业都不是一帆风顺的，氢能更是人类在追求能源安全，可持续发展道路上一个里程碑

      - 这个发言有高度 :+1: 下面是第一版 《[氢春号船员手册](https://gitee.com/men-jianwu/menjianwu/issues/I5ZXWY)》 :pray: 

### 一，政府政策

<ol>
<li>
<a title="Issue: 5项氢能技术！11项氢能标准！国家能源局近日氢能政策一览" href="https://gitee.com/men-jianwu/menjianwu/issues/I5XOOT">#I5XOOT:5项氢能技术！11项氢能标准！国家能源局近日氢能政策一览</a> ☆</li>
<li>
<a title="Issue: 实现碳中和，中国是认真的" href="https://gitee.com/men-jianwu/menjianwu/issues/I5UBYL">#I5UBYL:实现碳中和，中国是认真的</a> ★</li>
<li><a title="Issue: 推动绿色发展，推进人与自然和谐共生" href="https://gitee.com/men-jianwu/menjianwu/issues/I5WSL7">#I5WSL7:推动绿色发展，推进人与自然和谐共生</a></li>
<li>
<a title="Issue: 钱学森给领导的一封信" href="https://gitee.com/men-jianwu/menjianwu/issues/I5YZ6O">#I5YZ6O:钱学森给领导的一封信</a> ★</li>
<li><a title="Issue: 张家口已形成氢能全产业链发展格局" href="https://gitee.com/men-jianwu/menjianwu/issues/I5WMSX">#I5WMSX:张家口已形成氢能全产业链发展格局</a></li>
<li><a title="Issue: 阿联酋向德国交付首批低碳氨" href="https://gitee.com/men-jianwu/menjianwu/issues/I5XMI0">#I5XMI0:阿联酋向德国交付首批低碳氨</a></li>
</ol>

### 二，制氢

<ol>
<li>
<a title="Issue: 中国石化首套光伏离网质子交换膜（PEM）电解水制氢试验装置成功投运" href="https://gitee.com/men-jianwu/menjianwu/issues/I5YOVH">#I5YOVH:中国石化首套光伏离网质子交换膜（PEM）电解水制氢试验装置成功投运</a> ★</li>
</ol>

### 二，氢交通

<ol>
<li><a title="Issue: 补能3分钟续航超800公里，每公里2毛，日本氢能车会威胁中国吗" href="https://gitee.com/men-jianwu/menjianwu/issues/I5U83X">#I5U83X:补能3分钟续航超800公里，每公里2毛，日本氢能车会威胁中国吗</a></li>
<li>
<a title="Issue: 纯电SUV也落伍了？比亚迪DM-p混动技术落地，王传福又“赌”对了" href="https://gitee.com/men-jianwu/menjianwu/issues/I5WUYE">#I5WUYE:纯电SUV也落伍了？比亚迪DM-p混动技术落地，王传福又“赌”对了</a> ★</li>
<li>
<a title="Issue: 国产氢能自行车量产，换氢只需10秒钟，电动自行车行业或将被颠覆" href="https://gitee.com/men-jianwu/menjianwu/issues/I5U49L">#I5U49L:国产氢能自行车量产，换氢只需10秒钟，电动自行车行业或将被颠覆</a> ★</li>
</ol>

### 三，储氢

<ol>
<li><a title="Issue: 中国储能行业进入快车道" href="https://gitee.com/men-jianwu/menjianwu/issues/I5WUXU">#I5WUXU:中国储能行业进入快车道</a></li>
<li><a title="Issue: 储氢金属" href="https://gitee.com/men-jianwu/menjianwu/issues/I5U83V">#I5U83V:储氢金属</a></li>
<li><a title="Issue: 氢储运安全" href="https://gitee.com/men-jianwu/menjianwu/issues/I5YC02">#I5YC02:氢储运安全</a></li>
<li><a title="Issue: 将氢燃料储存在盐中——向“清洁”能源生产迈进了一步 " href="https://gitee.com/men-jianwu/menjianwu/issues/I5YVZ9">#I5YVZ9:将氢燃料储存在盐中——向“清洁”能源生产迈进了一步 </a></li>
<li><a title="Issue: Netl 的水泥专业知识是地下储氢研究的关键组成部分" href="https://gitee.com/men-jianwu/menjianwu/issues/I5WSIB">#I5WSIB:Netl 的水泥专业知识是地下储氢研究的关键组成部分</a></li>
<li>
<a title="Issue: 高压储氢火炬，冬奥零碳" href="https://gitee.com/men-jianwu/menjianwu/issues/I5W7GY">#I5W7GY:高压储氢火炬，冬奥零碳</a> ★</li>
<li><a title="Issue: 金属储氢原理" href="https://gitee.com/men-jianwu/menjianwu/issues/I5UKEU">#I5UKEU:金属储氢原理</a></li>
</ol>

### 四，电池 ★

<ol>
<li><a title="Issue: 燃料电池" href="https://gitee.com/men-jianwu/menjianwu/issues/I5VYR1">#I5VYR1:燃料电池</a></li>
<li><a title="Issue: 电池基本知识（主要来自高中电化学）" href="https://gitee.com/men-jianwu/menjianwu/issues/I5VX5V">#I5VX5V:电池基本知识（主要来自高中电化学）</a></li>
<li><a title="Issue: 电池的性能参数" href="https://gitee.com/men-jianwu/menjianwu/issues/I5VEAP">#I5VEAP:电池的性能参数</a></li>
<li><a title="Issue: 电池发展历史" href="https://gitee.com/men-jianwu/menjianwu/issues/I5V46W">#I5V46W:电池发展历史</a></li>
</ol>

### 五，氢燃料电池 ★

<ol>
<li><a title="Issue: 一、氢燃料电池发展简史" href="https://gitee.com/men-jianwu/menjianwu/issues/I5X67J">#I5X67J:一、氢燃料电池发展简史</a></li>
<li><a title="Issue: 二、氢燃料电池原理及其特点" href="https://gitee.com/men-jianwu/menjianwu/issues/I5X7YN">#I5X7YN:二、氢燃料电池原理及其特点</a></li>
<li><a title="Issue: 三、氢燃料电池发展现状" href="https://gitee.com/men-jianwu/menjianwu/issues/I5ZS7L">#I5ZS7L:三、氢燃料电池发展现状</a></li>
<li><a title="Issue: 四、氢燃料电池发展前景方向" href="https://gitee.com/men-jianwu/menjianwu/issues/I5ZDAI">#I5ZDAI:四、氢燃料电池发展前景方向</a></li>
</ol>

### 六，普及 ☆

<ol>
<li><a title="Issue: 氢能 科普 中小学" href="https://gitee.com/men-jianwu/menjianwu/issues/I5U83T">#I5U83T:氢能 科普 中小学</a></li>
<li><a title="Issue: 【2022氢能专精特新创业大赛】 中国氢能联盟CHA B站列表" href="https://gitee.com/men-jianwu/menjianwu/issues/I5U83R">#I5U83R:【2022氢能专精特新创业大赛】 中国氢能联盟CHA B站列表</a></li>
<li><a title="Issue: #教育部 #氢能人才" href="https://gitee.com/men-jianwu/menjianwu/issues/I5U83N">#I5U83N:#教育部 #氢能人才</a> ☆ <a href="氢能第一课.md">《氢能第一课》</a></li>
<li><a title="Issue: 10月8日 国际氢能日 2021 氢能科普月 与氢同行 " href="https://gitee.com/men-jianwu/menjianwu/issues/I5U83C">#I5U83C:10月8日 国际氢能日 2021 氢能科普月 与氢同行 </a></li>
</ol>

### 七，其他 ☆

<ol>
<li><a title="Issue: 氢能基础知识" href="https://gitee.com/men-jianwu/menjianwu/issues/I5T8MA">#I5T8MA:氢能基础知识</a></li>
<li><a title="Issue: 氢能前沿信息" href="https://gitee.com/men-jianwu/menjianwu/issues/I5T0IG">#I5T0IG:氢能前沿信息</a></li>
<li><a title="Issue: 你认为氢能发展的难题有哪些" href="https://gitee.com/men-jianwu/menjianwu/issues/I5SZA9">#I5SZA9:你认为氢能发展的难题有哪些</a></li>
<li><a title="Issue: 你认为氢能的未来是什么样的呢？" href="https://gitee.com/men-jianwu/menjianwu/issues/I5SVD3">#I5SVD3:你认为氢能的未来是什么样的呢？</a></li>
<li><a title="Issue: 氢能革命，我国是否具备发展氢能的条件？" href="https://gitee.com/men-jianwu/menjianwu/issues/I5U6BL">#I5U6BL:氢能革命，我国是否具备发展氢能的条件？</a></li>
<li>
<a title="Issue: 氢春号正向我们驶来！" href="https://gitee.com/men-jianwu/menjianwu/issues/I5U4HE">#I5U4HE:氢春号正向我们驶来！</a> ☆</li>
</ol>