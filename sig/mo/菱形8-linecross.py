import pygame

pygame.init()

# Set up the display
screen = pygame.display.set_mode((400, 400))

# Define the points of the diamond
pt1 = (200,100)
pt2 = (300,200)
pt3 = (200,300)
pt4 = (100,200)
points = [pt1,pt2,pt3,pt4]

# Set up the colors
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)

# Fill the background with white
screen.fill(WHITE)

# Draw the diamond
pygame.draw.polygon(screen, BLACK, points, 1)

# Draw cross line.
pygame.draw.line( screen, BLACK, pt1, pt3, 1 )
pygame.draw.line( screen, BLACK, pt2, pt4, 1 )

# Update the screen
pygame.display.update()

# Wait for the user to close the window
while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            exit()
