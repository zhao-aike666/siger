import turtle

def draw_diamond(length):
    t = turtle.Turtle()
    t.speed(0)
    t.color("black")
    t.fillcolor("white")
    t.begin_fill()
    for i in range(4):
        t.forward(length)
        t.right(90)
    t.end_fill()
    t.penup()
    t.goto(0, 0)
    t.pendown()
    for i in range(4):
        t.forward(length / 2)
        t.right(90)
    turtle.done()

length = int(input("请输入菱形的边长："))
draw_diamond(length)
