import turtle
import math

# 创建画布和画笔
canvas = turtle.Screen()
pen = turtle.Turtle()

result1 = math.sqrt(80000)
# 绘制等腰直角三角形
pen.fillcolor("yellow")
pen.begin_fill()
pen.forward(result1)
pen.left(135)
pen.forward(200)
pen.left(90)
pen.forward(200)
pen.end_fill()

result2 = math.sqrt(40000-(result1*0.5)**2)
# 绘制菱形
pen.penup()
pen.goto(result1*0.5, result2)
pen.pendown()
pen.fillcolor("yellow")
pen.begin_fill()
pen.right(90)
pen.forward(100)
pen.right(90)
pen.forward(100)
pen.right(90)
pen.forward(100)
pen.right(90)
pen.forward(100)
pen.end_fill()

result3 = math.sqrt(20000)
# 绘制菱形对角线
pen.penup()
pen.goto(result1*0.25, result2+result3*0.5)
pen.pendown()
pen.goto(result1*0.75, result2+result3*0.5)

pen.penup()
pen.goto(result1*0.25, result2*0.5)
pen.pendown()
pen.goto(result1*0.75, result2*0.5)

pen.penup()
pen.goto(result1*0.5, 0)
pen.pendown()
pen.goto(result1*0.5, result2+result3)

# 隐藏画笔
pen.hideturtle()

# 等待用户关闭画布
canvas.exitonclick()
