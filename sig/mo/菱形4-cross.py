import turtle

def draw_diamond(length):
    t = turtle.Turtle()
    t.speed(0)
    t.color("black")
    t.fillcolor("white")
    t.begin_fill()
    for i in range(4):
        t.forward(length)
        t.right(90)
    t.end_fill()

def draw_cross(length):
    t = turtle.Turtle()
    t.speed(0)
    t.color("black")
    t.penup()
    t.goto(-length / 2, 0)
    t.pendown()
    t.forward(length)
    t.penup()
    t.goto(0, length / 2)
    t.pendown()
    t.right(90)
    t.forward(length)

draw_diamond(90)
draw_cross(90)

turtle.done()
