# 最初找到这张合体的棋盘，是民族棋中国史项目的《民族棋大团结》首篇示范项目，
# 原作作者：郑鸿飞

# 它由两个不同的游戏棋盘合体而成，有共用的部分，玩法规则基本一致。
# 更名为：民族棋大团结.py 就是为了记住这个民族棋社区传播的起点。

import turtle
import math

# 创建画布和画笔
canvas = turtle.Screen()
pen = turtle.Turtle()

result1 = math.sqrt(80000)
# 绘制等腰直角三角形
pen.fillcolor("yellow")
pen.begin_fill()
pen.forward(result1)
pen.left(135)
pen.forward(200)
pen.left(90)
pen.forward(200)
pen.end_fill()

result2 = math.sqrt(40000-(result1*0.5)**2)
# 绘制菱形
pen.penup()
pen.goto(result1*0.5, result2)
pen.pendown()
pen.fillcolor("yellow")
pen.begin_fill()
pen.right(90)
pen.forward(100)
pen.right(90)
pen.forward(100)
pen.right(90)
pen.forward(100)
pen.right(90)
pen.forward(100)
pen.end_fill()

result3 = math.sqrt(20000)
# 绘制菱形对角线
pen.penup()
pen.goto(result1*0.25, result2+result3*0.5)
pen.pendown()
pen.goto(result1*0.75, result2+result3*0.5)

pen.penup()
pen.goto(result1*0.25, result2*0.5)
pen.pendown()
pen.goto(result1*0.75, result2*0.5)

pen.penup()
pen.goto(result1*0.5, 0)
pen.pendown()
pen.goto(result1*0.5, result2+result3)

pen.penup()
pen.goto(result1*0.25, result2+result3*0.5)
pen.pendown()
pen.fillcolor("green")
pen.begin_fill()
pen.right(90)
pen.forward(100)
pen.right(135)
pen.forward(result2)
pen.right(135)
pen.forward(100)
pen.end_fill()

pen.penup()
pen.goto(result1*0.75, result2+result3*0.5)
pen.pendown()
pen.fillcolor("green")
pen.begin_fill()
pen.right(90)
pen.forward(100)
pen.right(135)
pen.forward(result2)
pen.right(135)
pen.forward(100)
pen.end_fill()

pen.penup()
pen.goto(0, result2)
pen.pendown()
pen.goto(result2*2, result2)

# 隐藏画笔
pen.hideturtle()

# 等待用户关闭画布
canvas.exitonclick()
