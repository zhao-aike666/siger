import pygame

pygame.init()

# Set up the display
screen = pygame.display.set_mode((400, 300))

# Set up the colors
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)

# Draw a rectangle
pygame.draw.rect(screen, WHITE, [50, 50, 200, 100])

# Update the screen
pygame.display.update()

# Main loop
while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()
