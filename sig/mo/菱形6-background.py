import pygame

pygame.init()

# Set up the display
screen = pygame.display.set_mode((400, 300))

# Set up the colors
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)

# Fill the background with white
screen.fill(WHITE)

# Draw a rectangle with a thin black line and an empty inner
pygame.draw.rect(screen, BLACK, [50, 50, 200, 100], 1)

# Update the screen
pygame.display.update()

# Main loop
while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()
