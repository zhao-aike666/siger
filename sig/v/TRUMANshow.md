立项成功，一张喜欢的电影海报，一篇自己的观影体验和推荐语。就可以完成MD投稿到电影频道了。 sig/v @hulilili 

<p><img width="706px" src="https://foruda.gitee.com/images/1669557367136358990/543f03f4_5631341.jpeg " title="471楚门的世界副本.jpg"></p>

> 感谢 @hulilili 我和你重温了这个虚假的世界 :+1: 

![输入图片说明](https://foruda.gitee.com/images/1669565631331486098/83a3912c_5631341.png "屏幕截图")

他做到了，我们还有什么可以看的呢？再找找其他台...

> 这一刻我还期待续集，他是真正的超级巨星 :+1: 

[《楚门的世界》如果拍摄续集会是怎样的？](https://www.zhihu.com/question/29264641)

- 《楚门的世界》结尾处：两个警察演员说“看还有什么节目”。换台后看到了楚门的女友在围墙外等他，然后两人一起去了所谓的“斐济”。。。。

- 和初恋女友终于在一起了，在第二个罩子里面。（对，EXIT 是个假门，是另一个罩子的入口）

- 拍了就没意思了。楚门出去后，可能幸福，可能四处碰壁，留给观众一个想象空间更好。

- 两个警察正在找有什么节目看，突然发现有一个人在飞，第二天准备上班的时候发现有人在监视他们，突然撞上了天才发现，其实楚门才是那个基斯督，而那俩警察才是真人秀节目的主角。

- 楚门走出门外，满以为离开了别人为他塑造的假世界，导演却在演播厅笑了起来：“b组演员准备，我们一定让他以为自己走进了真正的世界。”

- 楚门的续集不应该是 黑客帝国 matrix 吗？我们的世界也是假的，母体或更高纬度的存在才是真。或者也是假的，堕入无尽的循环，人类的三维大脑无法想象出的真相。所以，一部楚门的世界就够了。

- 发现现实世界是另一个楚门的世界

- 还记得下集预告说，楚门会迎来第二春，有没有可能他其实没有走出虚拟的世界，虚拟的世界让他以为自己逃脱了，他的初恋女友就是他的第二春，他们还在罩子里，前往“斐济”。

# [电影《楚门的世界》：Truman or True man](https://gitee.com/hulilili/siger/issues/I62395)

“True man”, 意思是“真正的人”。而剧中的主人公“Truman”与其谐音，但生活却与字义大不相同。

主人公楚门从出生就生活在一个巨型的、人造的小镇中，是导演Cristof录制的真人秀的主角。楚门从小到大、方方面面的活动都会通过无处不在的摄像头实时转播到全国观众的电视上。除了楚门，小镇的其他人都是演员——包括楚门的父母、妻子。导演和工作人员工作在小镇的“月亮”里。

可作为“真人秀”，楚门是否“真”呢？为了不让楚门离开，Cristof设计了一场海上风暴，楚门的“父亲”遭遇海难，从此楚门不敢靠近大海；楚门喜欢上演员Sylvia后，导演为了继续执行计划，派人强行拉走女演员，让她离开拍摄现场。

对于楚门来说，自己幸福又平常，有顺心的工作，交心的好朋友，默契的妻子，生活理应如此。但再反过来看，他的工作是否也是导演所安排；妻子与他接吻的照片上，手指交叉，请求上帝原谅，也说明了她是演员；自己的发小在导演的指示下打消了想要离岛的念头，却信誓旦旦地称自己绝不会撒谎背叛......楚门的人际生活、周围环境，既是顺应着生活的发展和自己的成长亲手建立，又是导演无时无刻监视着潜移默化，既像真的，但又是假的。而其中唯一真实鲜活的，或者说导演所呈现出来真实的，就是楚门。

当一系列的“bug”出现后，楚门回想起初恋Sylvia的一番话“Everybody knows about you. They are pretending. This is fake. This is all for you. The sky and the sea, everything... it's a set. It's a show, everybody is watching you. Get out of here, come and find me.”他躲避镜头，克服对大海的恐惧，战胜了导演想要留住他的最高级别的风浪和雷电，驶向了围困住他的第四堵墙。他毅然告别了导演，走出了小镇，打破了第四堵墙，走向外面。

Cristof在整个真人秀中处在重要位置。他根据自己的判断，决定楚门的人生，决定观众的思想。讽刺的是，两个人的名字连起来就成了“Christ of Truman（楚门的救世主）”。导演将楚门看做自己的作品，而不是一个有独立思想的人。他可以高高在上地在月球俯视小镇，他的选择可能会决定楚门重要的命运。他掌控着楚门。而当楚门决定乘船离开，导演却放出了最高级别的暴风雨和雷电折磨楚门，企图让他留下。这就有点像“得不到就毁灭”的病态心理。这既是作为自己的木偶反抗的惩罚，也是向他示威。但楚门并没有因此屈服。

在最后楚门与导演的谈话中，导演始终认为，自己为楚门创造了一个安逸、纯真的生活，在这里他不用担心任何事。而外面的生活与他制作的一样虚无，有一样的谎言的欺诈，自己作为楚门的“救世主”则会“保护”着他。楚门仍旧选择了外面的世界。他不愿再被他人操控，也不想被众人关注，只想做自己。他走出了小镇，“Truman”也真正变成了“True man”。

结尾处，两位保安在《楚门秀》停播后不停换着台，继续寻找新的节目：

— “What else is on?”
— “Let's see what else is on. Where is the TV guide?”

这又让人有些惘然：对于三十年来看着楚门长大，跟他一起感受喜怒哀乐的观众来说，楚门到底算什么？对于他们来说，楚门的结局怎样已经不重要了，他们想看的是小镇的“楚门”，而不是楚门。旧的节目的结束并不会引起他们对于楚门人生的深思，只会又开始寻找新的消遣。楚门并不会给观众们留下什么，只会在某天的茶余饭后，无意中提起，无形中淡去。

![](https://foruda.gitee.com/images/1668947253350909282/dd859802_11997706.png)

![](https://foruda.gitee.com/images/1669545521578179943/0b409118_11997706.jpeg)

老师，这样的图片行吗

# `bing.com` [truman show poster](https://cn.bing.com/images/search?sp=1&pq=truman+show+po&sc=10-14&cvid=A5F718AEF41D49B98AB358938B2C02FB&ghsh=0&ghacc=0&q=truman+show+poster&qft=+filterui:imagesize-wallpaper&form=IRFLTR&first=1&tsc=ImageHoverTitle)

[Nostalgipalatset - TRUMAN SHOW (1998)](http://www.nostalgipalatset.com/se/affischer/filmaffischer-70x100cm/drama/truman-show-1998.html)  
nostalgipalatset.com|2728 × 3961 jpeg|Image may be subject to copyright.

![输入图片说明](https://foruda.gitee.com/images/1669555919188242677/7a2aa4d7_5631341.jpeg "175665487-origpic-ce30c6.jpg")

[crop-203-300-2034-3001-0-Truman-Show_Poster.jpg – filmleser](https://filmleser.com/crop-203-300-2034-3001-0-truman-show_poster-jpg/) 
filmleser.com|2034 × 3001 jpeg|Image may be subject to copyright.

![输入图片说明](https://foruda.gitee.com/images/1669555165153736982/77648d37_5631341.jpeg "crop-203-300-2034-3001-0-Truman-Show_Poster.jpg")

[COVERS.BOX.SK ::: The Truman Show (1998) - high quality DVD / Blueray ...](http://cover.box3.net/index.php?pid=cove2&p=v&rid=132623&&por=1&mod=front)  
cover.box3.net|3240 × 2175 jpeg|Image may be subject to copyright.

[Affiches et pochettes The Truman Show de Peter Weir](https://cinenode.com/film/338/the-truman-show/affiches)  
cinenode.com|1600 × 2473 jpeg|Image may be subject to copyright.

![输入图片说明](https://foruda.gitee.com/images/1669555746282957940/7ae69609_5631341.jpeg "the-truman-show-65128.jpg")

[Truman Show (1998) - film online zdarma](https://filmecheck.cz/film-9893-truman-show)  
filmecheck.cz|1296 × 1820 jpeg|Image may be subject to copyright.

![输入图片说明](https://foruda.gitee.com/images/1669555814648617336/7baef59c_5631341.jpeg "158988851_fc9f76.jpg")

![输入图片说明](https://foruda.gitee.com/images/1669556001558041359/29752ea9_5631341.jpeg "33120393_so.jpg")

[Truman Show, The 1998 Original Movie Poster #FFF-69882 ...](https://fffmovieposters.com/shop/truman-show-the-9/)  
fffmovieposters.com|1500 × 2205 jpeg|Image may be subject to copyright.

![输入图片说明](https://foruda.gitee.com/images/1669556075793627911/04124286_5631341.jpeg "69882.jpg")

[The Truman Show - PosterSpy](https://posterspy.com/posters/the-truman-show-2/)  
posterspy.com|1792 × 2560 jpeg|Image may be subject to copyright.

![输入图片说明](https://foruda.gitee.com/images/1669556365852082053/fa5c069c_5631341.jpeg "The-Truman-Show.jpg")

[The Truman Show Wallpapers - Wallpaper Cave](https://wallpapercave.com/the-truman-show-wallpapers)  
Wallpaper CaveWallpaper Cave|1280 × 1818 jpeg|Image may be subject to copyright.

![输入图片说明](https://foruda.gitee.com/images/1669556204011336394/5833348d_5631341.jpeg "wp4138642.jpg")

[The Truman Show - PosterSpy](https://posterspy.com/posters/the-truman-show-6/)  
posterspy.com|1200 × 1680 jpeg|Image may be subject to copyright.

![输入图片说明](https://foruda.gitee.com/images/1669556308579842913/408b3ec2_5631341.jpeg "El-show-de-trumanFINAL2-1200x1680.jpg")

# [《楚门的世界》另外一个结局是什么？他的孩子会成为下一个楚门](https://new.qq.com/rain/a/20220311A06S4100)

2022/03/11 20:00

电影《楚门的世界》里的“楚门秀”是一场大型电视真人节目，剧组建设一个超大摄影棚，在棚里是一个桃源镇，镇上的居民只有楚门是真实的人（True man），其他人包括楚门的父母、妻子、朋友和同事全部都是演员，他们配合楚门完成这场跨越几十年的24小时真人秀。

![输入图片说明](https://foruda.gitee.com/images/1669564597863849446/25f5faec_5631341.png "屏幕截图")

虽然《楚门的世界》属于剧情片，但其内容几乎相当于科幻片了，因为在现实世界里建设和维护一个桃源镇摄影棚的费用是难以想象的天文数字，光靠卖广告维持不太现实。

![输入图片说明](https://foruda.gitee.com/images/1669564605437248244/15129e07_5631341.png "屏幕截图")

电影很精彩，楚门走出桃源镇的结局也非常完美，这符合绝大多数观众的期待，也算是给“楚门秀该怎样收场”的自问画了一个句号。

不过，我们可以设想一下，如果楚门没有发现摄影棚的秘密而是继续生活在桃源镇，那这个节目又该如何收场呢？

想知道答案的话，我们可以发散思维以下几个问题：

![输入图片说明](https://foruda.gitee.com/images/1669564613607315648/2a8dc877_5631341.png "屏幕截图")

### 一、“楚门秀”的演员平常怎么生活？

楚门秀除了楚门之外全是演员，他们在这三十年里是怎么生活的？总不能真的天天陪着楚门呆在桃源镇这个巨大的摄影棚里吧。

实际上，楚门是一个生活很规律的打工人，上班下班的时间、地点都很固定，而且在剧组的各种心理暗示下从来不敢走出桃源镇这一亩三分地，这样就非常便于剧组安排演员来配合。

![输入图片说明](https://foruda.gitee.com/images/1669564626108572998/146aca21_5631341.png "屏幕截图")

楚门上班时遇到的那些演员也相当于在上班，演一个真人秀嘛。男主角下班后，他的那些邻居、同事、店主这些演员其实也就可以下班离开摄影棚了。因此楚门秀原则上并不会影响大多数演员的日常生活，他们可以在好莱坞有自己的家庭，然后按照楚门秀剧组的安排每天定点到桃源镇扮演自己的角色即可。

有些演员不想干了直接辞职即可，反正在真实的世界里你周围的人也是来来往往。

特殊一些的是楚门的母亲和哥们马龙，他们出镜机会更多，不过他们只是需要比普通演员辛苦一些，可以继续自己的正常生活之外，必须保证能够按照剧组要求随叫随到即可。

![输入图片说明](https://foruda.gitee.com/images/1669564631454487995/6ffbc087_5631341.png "屏幕截图")

经常被剧组安排救场的马龙当然也可以有自己的家庭，也可以出去度假，原则上来讲他只需要在出镜的时候表演，其他时间他也和普通演员没有什么两样。

![输入图片说明](https://foruda.gitee.com/images/1669564637233797471/ca500035_5631341.png "屏幕截图")

最特殊的人当然是楚门的妻子美露，楚门不在家时她可以自由活动，但下班后她就必须和楚门一直在一起，还要负责在生活中做各种植入广告的模特。

![输入图片说明](https://foruda.gitee.com/images/1669564643655735398/a4c72378_5631341.png "屏幕截图")

问题来了，美露既然和楚门是夫妻，为了不引起楚门的怀疑她就必须做一个真正的妻子，那么，夫妻生活要过吗？孩子要生吗？

![输入图片说明](https://foruda.gitee.com/images/1669564656451496212/ffc05d06_5631341.png "屏幕截图")

### 二、失败的美露

美露是一位职业演员，她的生活最特殊，她接触楚门最深，同时也是面对镜头最多的演员，因此她在楚门面前要一直撒谎，这才有了她拍婚纱照时“叠手指”的动作（祈求神的宽恕）

![输入图片说明](https://foruda.gitee.com/images/1669564671589758084/757c1e28_5631341.png "屏幕截图")

《楚门的世界》展示过美露和楚门的婚姻生活，美露穿着有些暴露的睡衣在家里，还故意向楚门放电：我们不打算生儿育女吗？这还不够刺激？

![输入图片说明](https://foruda.gitee.com/images/1669564676950270320/5a8c9d27_5631341.png "屏幕截图")

看到楚门还在纠结，美露甚至直接向楚门发出信号：先脱掉湿了的衣服，上床吧。

![输入图片说明](https://foruda.gitee.com/images/1669564685240713882/c54eeb34_5631341.png "屏幕截图")

这个暗示足够明显了，楚门和美露肯定是过正常夫妻生活的，只是楚门秀不能直播出去罢了，所以每到这个“关键时刻”剧组就要把镜头移开，同时还要播放音乐（为什么播放音乐呢？懂得自然懂）

![输入图片说明](https://foruda.gitee.com/images/1669564693141371534/8e5983d0_5631341.png "屏幕截图")

但是，虽然楚门和美露具备了生孩子的各种条件，楚门秀剧组还通过楚门的“母亲”三番五次暗示楚门赶紧要孩子，但最后美露的要孩子计划还是失败了。

计划失败的根源在楚门的个人感情，美露是剧组在楚门毕业时硬塞给他的女友，在出演这个角色前，美露肯定是签过合约的，哪些事情能做哪些事情不能做肯定都提前约定好了，而且我们还要确认一件事，美露不爱楚门，同时楚门也不爱美露。

![输入图片说明](https://foruda.gitee.com/images/1669564701301972926/d40ef87f_5631341.png "屏幕截图")

美露不爱楚门可以理解，楚门不爱美露则是因为施维亚的存在。

剧组不让楚门和施维亚在一起，原因并不在于施维亚是临时演员，大不了签一份合同取代美露呗，反正当时美露才刚认识楚门。

根源在于施维亚和楚门是有感情的，即使她变成正式演员成了楚门的妻子，她出于感情也会告诉楚门真相，更不会让自己和楚门的孩子继续生活在桃源镇摄影棚里，那整个楚门秀就塌了，因此剧组部只能把施维亚赶走，并且千方百计阻拦楚门。

![输入图片说明](https://foruda.gitee.com/images/1669564713444838693/83df577f_5631341.png "屏幕截图")

​ 但是，楚门对施维亚的一往情深严重影响了他和美露的感情，他不想要孩子，还要去斐济找施维亚，导致了一连串事件的发生，也导致美露和他过不下去了。

于是，美露只能离开楚门，她和楚门生孩子的计划破产。

![输入图片说明](https://foruda.gitee.com/images/1669564720501910057/3153ef43_5631341.png "屏幕截图")

### 三、楚门秀的另一个结局

《楚门的世界》最后的结局是楚门发现了秘密最终走出了摄影棚，那么这个摄影棚就没用了吗？还是再选一个孩子继续从他小时候拍起，重复楚门的故事？

笔者认为，根据剧情推理，如果楚门没有发现摄影棚的秘密，那么剧组已经给他安排好了下一步的剧情，也就是《楚门的世界》另外一个结局。

![输入图片说明](https://foruda.gitee.com/images/1669564737225334730/901cbb1e_5631341.png "屏幕截图")

按照剧组计划，美露将会离开楚门， 剧组给楚门安排了一位新的妻子慧云，先让她成为楚门的同事慢慢发展感情，然后逐步将美露替代。

![输入图片说明](https://foruda.gitee.com/images/1669564746729632306/810cd5de_5631341.png "屏幕截图")

对这个慧云，起码从楚门最开始的表情看，他应该还是比较满意的。

![输入图片说明](https://foruda.gitee.com/images/1669564755599722637/4e8f0a96_5631341.png "屏幕截图")

慧云这位新演员肯定是签了合约进来的，合约内容包括夫妻生活现场直播和生孩子，饰演楚门主管的那位演员有些猥琐的表情估计也是因为这个。

![输入图片说明](https://foruda.gitee.com/images/1669564760908403439/2c9be735_5631341.png "屏幕截图")

楚门秀剧组让慧云和楚门直播夫妻生活无非是为了吸引更高收视率，而执着于让他们生孩子则是为了楚门秀和桃源镇摄影棚的未来考虑。

![输入图片说明](https://foruda.gitee.com/images/1669564767953383200/91b23503_5631341.png "屏幕截图")

生孩子对于楚门秀来说至少有三大好处：

1、拴住楚门的心，让他沉浸在家庭的天伦之乐里无法自拔，也就不会再念叨施维亚和外面的世界。

2、剧组可以增加大量妇婴用品的广告，这可是一个重量级的市场。

3、可以从头再培养一个或几个“小楚门”，如果楚门万一哪天发现了秘密，剧组完全可以让他像楚门父亲一样“消失”，转过来继续拍摄他的孩子，桃源镇摄影棚将会延续使命继续充当楚门秀所属公司的摇钱树。

![输入图片说明](https://foruda.gitee.com/images/1669564777168036465/225864bd_5631341.png "屏幕截图")

​这就是《楚门的世界》另外一个结局，也许是一个循环往复永不停息的“楚门秀”，就像人的生老病死一样。