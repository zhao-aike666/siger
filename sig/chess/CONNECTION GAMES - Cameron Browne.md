- [CONNECTION GAMES - Cameron Browne](https://gitee.com/blesschess/LuckyLudii/issues/I4XTW8)

<p><img width="706px" src="https://images.gitee.com/uploads/images/2022/0318/112511_7b779159_5631341.jpeg" title="connectgames.jpg"></p>

> 为了感谢 Ludii.Games 团队的支持，chess sig 组的第一篇，收录 Cameron Browne 博士的著作，也是为了纪念 FlameChess -> LuckyLudii 的历程，借助 开源向北 的精神，在 SIGer 平台上发扬广大。本期专题，是制作一下海报的成果，分享于此，作为 sig-chess 的起点。之后是笔记的 100% 复制。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0318/114822_67967223_5631341.jpeg "connectgamesposter.jpg")

CONNECTION GAMES
Variations on a Theme
- Cameron Browne

CRC Press
Taylor & Francis Group
AN A K PETERS BOOK

---

CONNECTION GAMES 
Variations on a Theme

Cameron Browne

In this comprehensive study of the connection game genre, Browne provides a survey of known connection games, such as Hex and Y, while exploring common themes and strategies.

Connection Games

- Imposes some structure on this increasingly large family of games
- Defines exactly what constitutes a connection game
- Examines key games in detail
- Provides complete rules for over 200 connection games and variants

Praise for Connection Games

 _The book is an encyclopedic overview of connection games. Idon't know anything which is nearly comparable._ 
 
-- Jorg Bewersdorff, author of Luck, Logic, and White Lies: The Mathematics of Games

This is a great book. Every page of Connection Games works well, making it a classic work in an area that needed one... This woul be an excellent book for motivating a person to like mathematics.

-- Ed Pegg Jr., Mathpuzzle.com

CRC Press
Taylor & Francis Group an Informa business
www.crcpress.com

ISBN 978-1-138-42754-9
9 781138 427549

---

Browne

CONNECTION GAMES
Variations on a Theme

CRC

---

Connection Games
Variations on a Theme

Cameron Brewne

CRC Press
Taylor & Francis Group
Boca Raton London New York

CRC Press is an imprint of the 
Taylor & Francis Group, an **informa** business
AN A K PETERS BOOK

---

Editorial, Sales, and Customer Service Office

CRC Press
Taylor & Francis Group
6000 Broken Sound Parkway NW, Suite 300
Boca Raton, FL 33487-2742

First issued in hardback 2018

(c) 2005 by Taylor & Francis Group, LLC
CRC Press is an imprint of Taylor & Francis Group, an Informa business

No claim to original U.S. Government works

ISBN 13:978-1-138-42754-9(hbk)
ISBN 13:978-1-56881-224-3(pbk)

This book contains information obtained from authentic and highly regarded sources. Reason-able efforts have been made to publish reliable data and information, but the author and publisher cannot assume responsiblility for the validity of all materials of the consequences of their use. The authors and publishers have attempted to trace the copyright holders of all material reproducred in this publication and apologize to copyright holders if permission to publish in this form has not been obtained. If any copuright material has not been acknowledged please write an let us know so we may rectify in any future reprint.

Except as permitted under U.S. Copyright Law, no part of this book may be reprinted, reproduced, transmitted, or utilized in any form by any electronic, mechanical, or other means, now known or hereafter invented, including photocopying, microfilming, and recording, or in any information storage or retrieval system, without written permission from the publishers.

For permission to photocopy or use material electronically from this work, please access www. copyright.com (http://www.copyright.com/) or contact the Copyright Clearance Center, Inc.(CCC), 222 Rosewood Drive, Danvers, MA 01923, 978-750-8400. CCC is a not-for-profit organiza-tion that provides licenses and registration for a variety of users. For organizations that have been granted a photocopy license by the CCC, a separate system of payment has been arranged.

 **Trademark Notice:** Product or corporate names may be trademarks or registered trademarks, and are used only for identification and explanation without intent to infringe.
 
 **Visit the Taylor & Francis Web site at** 
 **http://www.taylorandfrancis.com**
 
 **and the CRC Press Web site at**
 **http://www.crcpress.com**
 
 **Library of Congress Cataloging-in-Publication Data**
 
Browne, Cameron, 1966-.
- Connection games:variations on theme / Cameron Browne.
  - p.cm.
- Includes bibliographical references and index.
- ISBN 1-56881-224-8
  - 1. Connection Games. I. Title
  
GV1469.C66B76 2004
794-dc22

---

Dedicated to Merlin

For always being there... even if he did keep dribbling on the manuscript.

---

List of Games

---

Index

---

<tr>
<td valign="top" height="202"><img src="https://images.gitee.com/uploads/images/2022/0316/082506_f481315d_5631341.png" height="200"></td>
<td valign="bottom" height="140"><p align="right">Cameron Browne</p></td>
</tr>
<hr>
<p class="style11">Games</p>
<p class="style13">I've always had a keen interest in
 abstract board games, puzzles and mathematical recreations
 in general, and started designing my own board games
 around 2000.</p>
<p class="style13">This page lists some of my games. More
 are listed in my <a href="http://www.boardgamegeek.com/boardgamedesigner/3541/cameron-browne"> BoardGameGeek</a>
 entry.</p>

<table><tbody>
<tr>
<td class="style28" width="107" height="127">
<p class="style30" align="center"><a href="akron"><img src="https://images.gitee.com/uploads/images/2022/0316/083157_1471e124_5631341.png" width="100" height="84" border="0"></a><br>
<a href="akron">Akron</a></p>
</td>
<td class="style28" width="113" height="127">
<p class="style30" align="center"><a href="triad"><img src="https://images.gitee.com/uploads/images/2022/0316/083253_64425f1c_5631341.png" width="113" height="100" border="0"><br>
 Triad</a></p>
</td>
<td class="style28" width="100" height="127">
<div class="style30" align="center">
<p align="center"><a href="plex"><img src="https://images.gitee.com/uploads/images/2022/0316/083315_2f1736c6_5631341.png" width="100" height="100" border="0"><br>
 Plex</a></p>
</div>
</td>
<td class="style28" width="100" height="127">
<p class="style30" align="center"><a href="py"><img src="https://images.gitee.com/uploads/images/2022/0316/083338_6e55896b_5631341.png" width="100" height="86" border="0" align="middle"></a><br>
<a href="py"><br>
 Py</a></p>
</td>
<td class="style28" width="105" height="127">
<div class="style30" align="center">
<div align="center"><a href="phwar"><img src="https://images.gitee.com/uploads/images/2022/0316/090639_31e9453e_5631341.jpeg" width="87" height="100" border="0"><br>
 Phwar</a></div>
</div>
</td>
<td class="style28" width="107">
<div align="center"><a href="savoy"><img src="https://images.gitee.com/uploads/images/2022/0316/083918_63fc7a7d_5631341.jpeg" width="99" height="100" border="0"></a><br>
<a href="savoy">Savoy</a></div>
</td>
<td class="style28" height="127">
<div align="center"><a href="esp"><img src="https://images.gitee.com/uploads/images/2022/0316/084018_1b72876c_5631341.jpeg" width="100" height="100" border="0"></a><br>
<a href="esp">ESP</a></div>
</td>
</tr>
<tr>
<td class="style28" height="127">
<div align="center"><a href="celtic"><img src="https://images.gitee.com/uploads/images/2022/0316/084052_de761e01_5631341.jpeg" width="100" height="100" border="0"></a><br>
<a href="celtic">Celtic!</a></div>
</td>
<td class="style28" height="123">
<div class="style30" align="center">
<div align="center"><a href="antipod"><img src="https://images.gitee.com/uploads/images/2022/0316/084121_a981589e_5631341.jpeg" width="107" height="100" border="0"><br>
 Antipod</a></div>
</div>
</td>
<td class="style13" height="123">
<p align="center"><a href="druid"><img src="https://images.gitee.com/uploads/images/2022/0316/084156_bc482b3d_5631341.jpeg" width="102" height="100" border="0"></a></p>
<p align="center"><a href="druid"> Druid</a></p>
</td>
<td class="style28" height="123">
<p class="style30" align="center"><a href="margo"><img src="https://images.gitee.com/uploads/images/2022/0316/084224_16eaff4e_5631341.jpeg" width="100" height="100" border="0"></a><br>
<a href="margo">Margo</a></p>
</td>
<td class="style13" height="123">
<p align="center"><a href="lingo"><img src="https://images.gitee.com/uploads/images/2022/0316/084249_4410cb64_5631341.png" width="100" height="81" border="0"></a><br>
</p>
<p align="center"><a href="lingo"><br>
 Lingo</a></p>
</td>
<td class="style28" height="123">
<p class="style30" align="center"><a href="mambo"><img src="https://images.gitee.com/uploads/images/2022/0316/084313_f17a3aa2_5631341.png" width="105" height="100" border="0"></a><br>
<a href="mambo">Mambo</a></p>
</td>
<td class="style28" width="107">
<div align="center"><a href="trugo"><img src="https://images.gitee.com/uploads/images/2022/0316/084339_be350403_5631341.jpeg" width="100" height="100" border="0"></a><br>
<a href="trugo">Trugo</a></div>
</td>
</tr>
<tr>
<td class="style28" width="107">
<div align="center"><a href="triablo"><img src="https://images.gitee.com/uploads/images/2022/0316/084408_10ab9d91_5631341.png" alt="" width="100" height="100" border="0"></a><br>
<a href="triablo">Triablo</a></div>
</td>
<td class="style28" width="107">
<div align="center"><a href="dragons"><img src="https://images.gitee.com/uploads/images/2022/0316/084434_6558395a_5631341.jpeg" width="100" height="100" border="0"></a><br>
<a href="dragons">Dragons</a></div>
</td>
<td class="style28" height="123">
<p class="style30" align="center"><a href="truchet"><img src="https://images.gitee.com/uploads/images/2022/0316/084501_4f797537_5631341.jpeg" width="100" height="100" border="0"></a><br>
<a href="truchet">Truchet</a></p>
</td>
<td class="style28" height="123">
<div class="style30" align="center"><a href="moloko"><img src="https://images.gitee.com/uploads/images/2022/0316/084525_461081c2_5631341.jpeg" width="100" height="100" border="0"></a><br>
<a href="moloko">Moloko</a></div>
</td>
<td class="style28" height="123">
<div class="style30" align="center"><a href="rombo"><img src="https://images.gitee.com/uploads/images/2022/0316/084553_b8c038ba_5631341.jpeg" width="91" height="100" border="0"></a><br>
<a href="rombo">Rombo</a></div>
</td>
<td class="style28" height="123">
<div class="style30" align="center"><a href="holomino"><img src="https://images.gitee.com/uploads/images/2022/0316/084620_2040cdc5_5631341.jpeg" width="100" height="100" border="0"></a><br>
<a href="holomino">Holomino</a></div>
</td>
<td class="style28" height="123">
<div class="style30" align="center"><a href="yavalath"><img src="https://images.gitee.com/uploads/images/2022/0316/084646_0e7402f9_5631341.png" width="100" height="100" border="0"><br>
 Yavalath</a></div>
</td>
</tr>
<tr>
<td class="style28" width="107">
<div align="center"><a href="pentalath"><img src="https://images.gitee.com/uploads/images/2022/0316/084713_99fc8cae_5631341.png" width="100" height="100" border="0"><br>
 Pentalath</a></div>
</td>
<td class="style28" height="123">
<div align="center"><a href="hopit"><img src="https://images.gitee.com/uploads/images/2022/0316/084739_9ece98f2_5631341.jpeg" width="100" height="100" border="0"></a><br>
<a href="hopit">Hop It!</a></div>
</td>
<td class="style28">
<div align="center"><a href="boloko"><img src="https://images.gitee.com/uploads/images/2022/0316/084807_1f416a4f_5631341.jpeg" width="100" height="100" border="0"></a><br>
<a href="boloko">Boloko</a></div>
</td>
<td class="style28" height="123">
<p class="style30" align="center"><a href="osbo"><img src="https://images.gitee.com/uploads/images/2022/0316/084832_39d066da_5631341.jpeg" width="100" height="100" border="0"><br>
 Osbo</a></p>
</td>
<td class="style28" height="123">
<div class="style30" align="center"><a href="osbox"><img src="https://images.gitee.com/uploads/images/2022/0316/084856_c4db1c91_5631341.png" width="100" height="100" border="0"><br>
 Osbox</a></div>
</td>
<td bordercolor="0" class="style28" height="123">
<div class="style30" align="center"><a href="oxvo"><img src="https://images.gitee.com/uploads/images/2022/0316/084921_73795081_5631341.png" width="100" height="100" border="0"><br>
 Oxvo</a></div>
</td>
<td class="style28" height="123">
<div class="style30" align="center"><a href="octex"><img src="https://images.gitee.com/uploads/images/2022/0316/084945_b81eea5c_5631341.jpeg" width="100" height="100" border="0"></a><br>
<a href="octex">Octex</a></div>
</td>
</tr>
<tr>
<td class="style28" height="123">
<div class="style30" align="center"><a href="hextiles"><img src="https://images.gitee.com/uploads/images/2022/0316/085014_5f3edb92_5631341.png" width="100" height="100" border="0"></a><br>
<a href="hextiles">Hextiles</a></div>
</td>
<td class="style28" width="107">
<div align="center"><a href="halves"><img src="https://images.gitee.com/uploads/images/2022/0316/085039_0abda5ef_5631341.png" width="100" height="87" border="0"></a><br>
<a href="halves">Halves</a></div>
</td>
<td bordercolor="0" class="style28" height="123">
<div align="center"><a href="mutton"><img src="https://images.gitee.com/uploads/images/2022/0316/085105_4a722fe5_5631341.jpeg" width="100" height="100" border="0"></a><br>
<a href="mutton">Mutton</a></div>
</td>
<td class="style28" height="123">
<div align="center"><a href="http://www.boardgamegeek.com/boardgame/55663/rebel-moon-defense"><img src="https://images.gitee.com/uploads/images/2022/0316/085135_9d295ada_5631341.jpeg" width="100" height="100" border="0"></a><br>
<a href="http://www.boardgamegeek.com/boardgame/55663/rebel-moon-defense">R.
 M. D.</a></div>
</td>
<td class="style28" height="123">
<div class="style30" align="center"><a href="chroma"><img src="https://images.gitee.com/uploads/images/2022/0316/085202_4796977e_5631341.jpeg" width="100" height="100" border="0"></a><br>
<a href="chroma">Chroma</a></div>
</td>
<td class="style28" height="123">
<div class="style30" align="center"><a href="ankor"><img src="https://images.gitee.com/uploads/images/2022/0316/085231_c9736208_5631341.jpeg" width="100" height="100" border="0"></a><br>
<a href="ankor">Ankor</a></div>
</td>
<td bordercolor="0" class="style28" height="123">
<div class="style30" align="center"><a href="limit"><img src="https://images.gitee.com/uploads/images/2022/0316/085300_bcf1499b_5631341.jpeg" width="100" height="100"></a><br>
<a href="limit">Limit</a></div>
</td>
</tr>
<tr>
<td class="style28" height="123">
<div class="style30" align="center"><a href="vasco"><img src="https://images.gitee.com/uploads/images/2022/0316/085325_c4556b00_5631341.jpeg" width="100" height="100" border="0"></a><br>
<a href="vasco">Vasco</a><br>
</div>
</td>
<td class="style28" height="123">
<div class="style30" align="center"><a href="marque"><img src="https://images.gitee.com/uploads/images/2022/0316/085349_13e37edf_5631341.png" width="100" height="100" border="0"></a><br>
<a href="marque">Marque</a></div>
</td>
<td class="style28" width="107">
<div align="center"><a href="trilbert"><img src="https://images.gitee.com/uploads/images/2022/0316/085414_88c28c38_5631341.png" width="75" height="100" border="0"><br>
 Trilbert</a></div>
</td>
<td class="style28" height="123">
<div align="center"><a href="boche"><img src="https://images.gitee.com/uploads/images/2022/0316/085441_f74626f4_5631341.jpeg" width="100" height="100" border="0"></a><br>
<a href="boche">Boche</a></div>
</td>
<td class="style28" height="123">
<div align="center"><a href="blobs"><img src="https://images.gitee.com/uploads/images/2022/0316/085508_a8348d4e_5631341.png" width="100" height="100" border="0"></a><br>
<a href="blobs">Blobs</a></div>
</td>
<td class="style28" height="122">
<div class="style30" align="center"><a href="palago"><img src="https://images.gitee.com/uploads/images/2022/0316/085558_b01a94f9_5631341.png" width="100" height="100" border="0"></a><br>
<a href="palago">Palago</a></div>
</td>
<td class="style28" height="122">
<div class="style30" align="center"><a href="che"><img src="https://images.gitee.com/uploads/images/2022/0316/085632_daf72971_5631341.png" width="100" height="84" border="0"></a><br>
<a href="che"><br>
 Che</a></div>
</td>
</tr>
<tr>
<td bordercolor="0" class="style28" height="122">
<div class="style30" align="center"><a href="xutoli"><img src="https://images.gitee.com/uploads/images/2022/0316/085700_19cd8ab1_5631341.png" width="100" height="100" border="0"></a><br>
<a href="xutoli">Xutoli</a></div>
</td>
<td class="style28" height="122">
<div class="style30" align="center"><a href="trichet"><img src="https://images.gitee.com/uploads/images/2022/0316/085726_27c6e19d_5631341.png" width="100" height="100" border="0"></a><br>
<a href="trichet">Trichet</a></div>
</td>
<td class="style28" height="122">
<div class="style30" align="center"><a href="gates"><img src="https://images.gitee.com/uploads/images/2022/0316/085750_f8a316eb_5631341.jpeg" width="100" height="100" border="0"></a><br>
<a href="gates">Gates</a></div>
</td>
<td class="style28" width="107">
<div align="center"><a href="stax"><img src="https://images.gitee.com/uploads/images/2022/0316/085813_29c9b6ef_5631341.png" width="100" height="100"><br>
 Stax</a></div>
</td>
<td class="style28" width="107">
<div align="center"><a href="spargo"><img src="https://images.gitee.com/uploads/images/2022/0316/085840_ecc64723_5631341.png" width="100" height="100"></a><br>
<a href="spargo">Spargo</a></div>
</td>
<td class="style28" width="107">
<div align="center"><a href="ploid"><img src="https://images.gitee.com/uploads/images/2022/0316/085906_18cf715c_5631341.png" width="100" height="100"></a><br>
<a href="ploid">Ploid</a></div>
</td>
<td class="style28" height="122">
<div class="style30" align="center"><a href="nonads"><img src="https://images.gitee.com/uploads/images/2022/0316/091036_22400796_5631341.png" width="100" height="100"></a><br>
<a href="nonads">Nonads</a></div>
</td>
</tr>
<tr>
<td class="style28" height="122">
<div class="style30" align="center"><a href="span"><img src="https://images.gitee.com/uploads/images/2022/0316/085933_b8b483ff_5631341.png" width="100" height="100"></a><br>
<a href="span">Span</a></div>
</td>
<td bordercolor="0" class="style28" height="122">
<div class="style30" align="center"><a href="shibumi"><img src="https://images.gitee.com/uploads/images/2022/0316/085956_b09eacf4_5631341.png" width="90" height="90"></a><br>
 &nbsp;&nbsp;<br>
<a href="shibumi">Shibumi</a></div>
</td>
<td class="style28" height="122">
<div class="style30" align="center"><a href="hanoi"><img src="https://images.gitee.com/uploads/images/2022/0316/090020_cb15f320_5631341.png" width="100" height="100"></a><br>
<a href="hanoi">Hanoi</a></div>
</td>
<td class="style28" height="122">
<div class="style30" align="center"><a href="spoff"><img src="https://images.gitee.com/uploads/images/2022/0316/090118_9ac64ee8_5631341.png" width="100" height="100"></a><br>
<a href="spoff">Spoff</a></div>
</td>
<td class="style28">&nbsp;</td>
<td class="style28">&nbsp;</td>
<td class="style28">&nbsp;</td>
</tr>
</tbody></table>

<hr>
<table width="100%" border="0">
<tbody>
<tr>
<td class="style13" width="1049">
<p class="style11">Shibumi Set</p>
<p>The Shibumi set is a minimalist game system
 created for experiments in automated game design.
 The idea was to create a system so constrained
 that its rule set could be fully defined, giving
 the computer the same access to the design space
 as a human inventor, yet still provide interesting
 games.</p>
<p>We've already asked designers to invent the best
 games they can for the system... soon it will be
 the computer's turn! More details <a href="shibumi/index.html" target="_parent">here</a>.
<br>
</p>
</td>
<td class="style13" width="158">
<div align="right"><a href="shibumi/index.html" target="_parent"><img src="https://images.gitee.com/uploads/images/2022/0316/090147_d7bb7f00_5631341.png" width="130" height="100" border="0"></a></div>
</td>
</tr>
</tbody>
</table>
<hr>
<table width="100%" border="0">
<tbody>
<tr>
<td class="style13" width="1098">
<p><span class="style11">Connection Games</span></p>
<p>My book <i><a href="https://www.amazon.com/Connection-Games-Variations-Cameron-Browne/dp/1568812248">Connection
 Games: Variations on a Theme</a></i> from AK
 Peters has been well received, and is now the
 definitive text on the topic. See my <a href="connection-games">connection games</a>
 page for more details.</p>
<p><a href="hex">Hex</a>, the genesis of most
 connection games, is a remarkable board game with
 some interesting mathematical properties.<br>
</p>
</td>
<td class="style13" width="109">
<div align="right"><a href="http://www.akpeters.com/product.asp?ProdCode=2248"><img src="https://images.gitee.com/uploads/images/2022/0316/090213_37847c74_5631341.jpeg" width="67" height="100" border="0"></a></div>
</td>
</tr>
</tbody>
</table>
<hr>
<p class="style13"><span class="style11">Online Play</span></p>
<p class="style13">Richard Rognlie's<a href="http://www.gamerz.net/pbmserv/">gamerz.net</a> is
 a play-by-email (PBeM) server that coordinates email play
 for a variety of games. Most of the games shown above are
 available for online play there, and many can be played
 through the server's <a href="http://www.gamerz.net/pbmserv/gamerz.php">graphical
 web interface</a>.<br>
<br>
</p>
<hr>
<table width="100%" cellpadding="0" border="0">
<tbody>
<tr>
<td width="592"><img src="https://images.gitee.com/uploads/images/2022/0316/091341_98e939f7_5631341.png" width="142" height="16"></td>
<td width="619">
<div align="right"><span class="style13"><a href="../index.html" target="_parent">home</a>
 &nbsp;&nbsp;| &nbsp;&nbsp;<a href="../cv/index.html" target="_parent">cv</a>
 &nbsp;&nbsp;| &nbsp;&nbsp;games &nbsp;&nbsp;|
 &nbsp;&nbsp;<a href="../gallery/index.html" target="_parent">gallery</a></span></div>
</td>
</tr>

---

![输入图片说明](https://images.gitee.com/uploads/images/2022/0316/093935_3ab2e3b7_5631341.png "屏幕截图.png")

https://www.routledge.com/Connection-Games-Variations-on-a-Theme/Browne/p/book/9781568812243

### Book Description

A comprehensive study of the connection game genre, Connection Games provides a survey of known connection games while exploring common themes and strategies. This book aims to impose some structure on this increasingly large family of games, and to define exactly what constitutes a connection game. Key games are examined in detail and complete rules for over 200 connection games and variants are provided. A connection game is a board game in which players vie to develop or complete a specific type of connection with their pieces. This might involve forming a path between two or more goals, completing a closed loop, or gathering all pieces together into a single connected group.

### Reviews

- " is a great book. Every page of Connection Games works well, making it a classic work in an area that needed one. . . This would be an excellent book for motivating a person to like mathematics. -Ed Pegg Jr., Mathpuzzle.com, November 2004

- book is an encyclopedic overview on connection games. I don't know anything which is nearly comparable. -Jörg Bewersdorff, Author of Luck, Logic, and White Lies, November 2004

- ""It's a fascinating book."" -Rezensionen, February 2005

- Connection Games has become the definitive text on the subject, literally! -Wolfram Research, March 2005

- new book written by Cameron Browne is absolutely a must for anybody interested in abstract strategy games."" -Ralf Gering, Abstract Games Magazine , March 2005

- Of Interest to anyone intrigued by board games, this book provides an incredible summary of connection games and their related strategies... -Ned W. Schillow, Mathematics Teacher, December 2005

- ""This book by C. Browne is truly a masterpiece in an area in which such a thing was desperately needed and there is no doubt that it will become a classic. Moreover, it is likely to attract many people not only to connection games or strategy games, but, in general, to mathematics. What a blessing!"" -Newsletter of the European Mathematical Society , June 2006

- ""In his well organized book the author has collected the rules of about 200 such games, classifies them, and describes their history and what is known about them. This is not a mathematical book on game theory, but a valuable source of relevant information about ""real world"" games."" -SpringerWienNewYork - Monatshefte fuer Mathematik, May 2008" 

### Recommended For You

[![输入图片说明](https://images.gitee.com/uploads/images/2022/0316/093757_efc21b35_5631341.png "屏幕截图.png")](https://www.routledge.com/Hex-Strategy-Making-the-Right-Connections/Browne/p/book/9781568811178?source=igodigital)

---

### [Editorial Reviews](https://www.amazon.com/Connection-Games-Variations-Cameron-Browne/dp/1568812248)

 **Review** 

- " is a great book. Every page of Connection Games works well, making it a classic work in an area that needed one. . . This would be an excellent book for motivating a person to like mathematics. -Ed Pegg Jr., Mathpuzzle.com, November 2004
- book is an encyclopedic overview on connection games. I don't know anything which is nearly comparable. -Jörg Bewersdorff, Author of Luck, Logic, and White Lies, November 2004
""It's a fascinating book."" -Rezensionen, February 2005
- Connection Games has become the definitive text on the subject, literally! -Wolfram Research, March 2005
- new book written by Cameron Browne is absolutely a must for anybody interested in abstract strategy games."" -Ralf Gering, Abstract Games Magazine , March 2005
- Of Interest to anyone intrigued by board games, this book provides an incredible summary of connection games and their related strategies... -Ned W. Schillow, Mathematics Teacher, December 2005
- ""This book by C. Browne is truly a masterpiece in an area in which such a thing was desperately needed and there is no doubt that it will become a classic. Moreover, it is likely to attract many people not only to connection games or strategy games, but, in general, to mathematics. What a blessing!"" -Newsletter of the European Mathematical Society , June 2006
- ""In his well organized book the author has collected the rules of about 200 such games, classifies them, and describes their history and what is known about them. This is not a mathematical book on game theory, but a valuable source of relevant information about ""real world"" games."" -SpringerWienNewYork - Monatshefte fuer Mathematik, May 2008"

 **From the Inside Flap** 

- "The recent explosion of new connection games. . . is far and away the most exciting development in board game theory. Brownes book is a magnificent survey of this vast field, covering more than 200 such games triggered by the Piet Hein/John Nash invention of Hex in 1942."

---Martin Gardner


 **About the Author** 

- Cameron Browne is a software engineer living in Brisbane, Australia. He has been an avid board game player for many years, and developed a special interest in connection games after being introduced to Hex. His earlier book Hex Strategy: Making the Right Connections provided the impetus for this broader study of the connection game family.

### Product details

- Publisher ‏ : ‎ A K Peters/CRC Press (January 3, 2005)
- Language ‏ : ‎ English
- Paperback ‏ : ‎ 414 pages
- ISBN-10 ‏ : ‎ 1568812248
- ISBN-13 ‏ : ‎ 978-1568812243
- Item Weight ‏ : ‎ 1.5 pounds
- Dimensions ‏ : ‎ 6 x 1.1 x 8.9 inches

Best Sellers Rank: #695,971 in Books (See Top 100 in Books)

- #150 in [Game Theory](https://www.amazon.com/gp/bestsellers/books/13922/ref=pd_zg_hrsr_books) (Books)
- #389 in [Math Games](https://www.amazon.com/gp/bestsellers/books/775830/ref=pd_zg_hrsr_books)
- #909 in [Board Games](https://www.amazon.com/gp/bestsellers/books/4403/ref=pd_zg_hrsr_books) (Books)

Customer Reviews: 4.6 out of 5 stars 7 ratings 

---

- 【笔记】[CONNECTION GAMES - Cameron Browne](https://gitee.com/blesschess/LuckyLudii/issues/I4XTW8)

  - [comments](https://gitee.com/blesschess/LuckyLudii/issues/I4XTW8#note_9239242_link) (OCRs)

    <p><img width="15.69%" src="https://images.gitee.com/uploads/images/2022/0315/095252_a6d48195_5631341.jpeg"> <img width="15.69%" src="https://images.gitee.com/uploads/images/2022/0315/095304_1048ab34_5631341.jpeg"> <img width="15.69%" src="https://images.gitee.com/uploads/images/2022/0315/095316_e9db7d0f_5631341.jpeg"> <img width="15.69%" src="https://images.gitee.com/uploads/images/2022/0315/095339_374542a4_5631341.jpeg"> <img width="15.69%" src="https://images.gitee.com/uploads/images/2022/0315/095411_02422463_5631341.jpeg"> <img width="15.69%" src="https://images.gitee.com/uploads/images/2022/0315/095428_0f504c93_5631341.jpeg"> <img width="15.69%" src="https://images.gitee.com/uploads/images/2022/0315/095440_6e0186cb_5631341.jpeg"> <img width="15.69%" src="https://images.gitee.com/uploads/images/2022/0315/095453_225c76fd_5631341.jpeg"> <img width="5.1%" src="https://images.gitee.com/uploads/images/2022/0315/095352_b620c36e_5631341.jpeg"> <img width="27.8%" src="https://images.gitee.com/uploads/images/2022/0315/095504_b4fab3ff_5631341.jpeg"> <img width="27.8%" src="https://images.gitee.com/uploads/images/2022/0315/095517_94fe832c_5631341.jpeg"></p>
  
  - [Cameron Browne Games](https://gitee.com/blesschess/LuckyLudii/issues/I4XTW8#note_9239280_link)

  - [Book Description](https://www.routledge.com/Connection-Games-Variations-on-a-Theme/Browne/p/book/9781568812243) 【[notes](https://gitee.com/blesschess/LuckyLudii/issues/I4XTW8#note_9240725_link)】

    <p><img height="169px" src="https://images.gitee.com/uploads/images/2022/0316/093935_3ab2e3b7_5631341.png"> <img height="169px" src="https://images.gitee.com/uploads/images/2022/0316/093757_efc21b35_5631341.png"></p>

  - Amazon [Editorial Reviews](https://www.amazon.com/Connection-Games-Variations-Cameron-Browne/dp/1568812248) 【[notes](https://gitee.com/blesschess/LuckyLudii/issues/I4XTW8#note_9241830_link)】

  - [backup https://gitee.com/blesschess/siger/第7期 计算机博弈的国际范儿 LudiiGames.md](https://gitee.com/blesschess/LuckyLudii/issues/I4XTW8#note_9290290_link)

    <img src="https://images.gitee.com/uploads/images/2022/0318/091321_8be44c60_5631341.png">
    
    - [head](https://gitee.com/blesschess/LuckyLudii/issues/I4XTW8#note_9290380_link)
    - [table](https://gitee.com/blesschess/LuckyLudii/issues/I4XTW8#note_9290390_link)
    - [end 【notes】](https://gitee.com/blesschess/LuckyLudii/issues/I4XTW8#note_9290402_link)
  