<p><img width="706px" src="https://images.gitee.com/uploads/images/2022/0715/235846_915304bc_5631341.jpeg" title="Wagner副本.jpg"></p>

看完 @muyujia1729 的译文，我的心踏实了，这份笔耕终于守得了民族棋中国史团队对世界民族棋史的贡献。LuckyLudii 真正的立于世界体育人类学的舞台啦。尽管这个学术分类并不准确，弈棋和体育划等号也只是近代所为。棋不只是为了PK这个体育范畴。

> 封面标题除了两个游戏，这份连接只的是弈棋之传统悠久，同一片天空的地球人身处各地，都会被这一形式所吸引，这也是文化火种传递世界的证明。看副标题旁的 MERELS 被冠以 round 还被煞有介事地印刷成明信片，展出于 Ludiigames 的荷兰总部 （LUCKYstar也是同样的殊荣），说明，文化的变迁一定离不开主体玩游戏的人的。要不这颗五角星也不会变成 北京小妞 口中的幸运星，ROTA 也不会成为 AI工程师团队下 棋规数据库里相似规则的 圆形三子棋啦。本段引语，只是为了欢迎更多同学能加入到我们的工作中，世界民族棋史简体中文版，9月1日后，新学期，会有一个大的亮相，邀请你们成为贡献者...

  - 剧透一下，青藏高原，它的名字叫 “[鸽子蛋棋](https://gitee.com/muyujia1729/siger/issues/I5GLQS#note_11595841_link)”

# [Rota 铭刻在古罗马帝国建筑遗迹上的 “轮子” 游戏。](https://gitee.com/muyujia1729/siger/issues/I5GLQS#note_11813002_link)

https://www.ancientgames.org/rota/

这是一个古老的游戏，棋盘被铭刻在了神庙的地面（下节 图1）。玩法肯定失传了。上面的玩法，是2020现代的数学家设计的。
而我之后补充的两个玩法，分别来自，新疆和西藏。民族棋的经典。

这篇文章的翻译，是作为图论的数学科普作品，在假期的社区中传播之用。

罗马帝国是古棋盘游戏的一个摇篮，和古埃及一样。世界的东方中国的棋盘游戏则完全不同的风格。丝绸之路将东西方连接在了一起。并通过棋盘游戏有力的证明了文明是流动的。你玩，我也玩，大家一起玩。

## 罗塔（[Rota](https://www.ancientgames.org/rota/)）

Rota 是一种远古的罗马游戏，在许多古罗马帝国时期的道路和建筑物遗迹中被发现。它被刻在石头上，原始名称未知。Elmer Merrill 在 1916 年发表的文章中给它起名为 Rota，在拉丁语中的意思是“轮子”，使得该游戏引起了世界的关注。Merrill 还根据其他类似的游戏重新创建了游戏规则。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0714/164647_a19096fa_5631341.jpeg "Roman-Rota-Game-in-the-Old-Forum-Leptis-Magna-Libya-Photo-Pablo-Novoa-Alvarez-2011.jpg")

> （图1）利比亚 Leptis Magna 旧公共集会场所的Rota游戏。照片：巴勃罗·诺瓦·阿尔瓦雷斯，2011

奥维德（Ovid）的两首诗中似乎描述了罗塔的规则，Ars Amatoria（《爱的艺术》）（第 362-366 行），写于公元 2 年，以及 Tristia（第 481-483 行）。令人困惑的是，奥维德说游戏的点的个数与日历年中的月份数量相同。罗马历法在朱利叶斯凯撒的儒略改革之前有 10 个月，然而在整个罗马帝国中发现的雕刻成石头的 Rota 棋盘都只有 9 个点，而不是 10 个。因此，Ovid 可能指的是另一个游戏，或者至少是一个不同的具有 10 个单元的 Rota 棋盘变体。但是，他清楚地说明了如何通过 3 个棋子连成一列来赢得比赛。

应该注意的是，这种对似乎是 Rota 的描述仅根据 J. Lewis May 的翻译才有效。根据 Mozley 在 Loeb Classical Library 版中的翻译，这句话指的是两个不同的游戏，其中一个是 Ludus Duodecim Scriptorum (Duodecim Scripta)，在游戏的单个部分有 12 行，指的是奥古斯都后的改革。12 个月的日历。而另一场比赛是“九人莫里斯”（[Nine Men’s Morris](https://www.ancientgames.org/nine-mens-morris/)），它通过在对手的棋子两侧放置两个棋子来捕获对手的棋子，从而形成三个连成一排的棋子。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0722/185321_2f161d26_5631341.jpeg "Rota-carved-into-the-floor-tiles-of-the-Marble-Road-in-Ephesus-Turkey-Photo-Juan-Carlos-Campos-July-2011.jpg")

> （图2）Rota carved into the floor tiles of the Marble Road in Ephesus, Turkey. Photo: Juan Carlos Campos, July, 2011.

当然，另一种可能性是，在两首诗中，奥维德都指的是一个 3×4 的棋盘，里面有 12 个单元，每个玩家有 3 个棋子，玩家必须移动棋子，形成三个棋子连成一排的样式，才能获胜。有点像井字游戏，但要移动棋子。

（跳过的诗歌部分）

- Est genus, in totidem tenui ratione redactum  
Scriptula, quot menses lubricus annus habet:  
Parva tabella capit ternos utrimque lapillos,  
In qua vicisse est continuasse suos.

- Latin text from Mozley, J.H. “Ovid: the Art of love, and other poems.” Loeb Classical Library. Harvard University Press (1957). pp. 144.

- There is another game divided into as many parts as there are months in the year. A table has three pieces on either side; the winner must get all the pieces in a straight line.

- English translation from J. Lewis May, “The love books of Ovid : being the amores, ars amatoria, remedia amoris and medicamina faciei femineae of Publius Ovidius Naso.” New York : Privately printed for Rarity Press, 1930. pp. 165.

- instructa tabella lapillis,  
in qua vicisse est continuasse suos;  
quique alii lusus

- Latin text from Wheeler, Arthur Leslie. “Ovid: Tristia, Ex Ponto.” Loeb Classical Library. Harvard University Press (1939). p. 90.

- how a small board is provided with three men on a side and victory lies in keeping one’s men abreast;

- English translation from Wheeler, Arthur Leslie. “Ovid: Tristia, Ex Ponto.” Loeb Classical Library. Harvard University Press (1939). p. 91.

I will leave the readers of Latin to judge for themselves what they see in Ovid’s text.

### Rota 游戏规则：

1. 比赛有两名玩家。
2. Rota板的外圈由 8 个点组成，点与点之间用线连接，圆圈的中间还有一个点。
3. 游戏开始时每个玩家有 3 个棋子，一方执黑棋，另一方执白棋。所有棋子都在棋盘上，由玩家决定谁先走。
4. 玩家交替走棋，可将 3 个棋子中的任意 1 个移动到有边相连接的点位，中心的点也可以被占用。

   ![输入图片说明](https://images.gitee.com/uploads/images/2022/0722/185942_b1613c99_5631341.jpeg "Rota-Board.jpg")

5. 游戏开始，一旦所有 6 个棋子都放好后，玩家交替走棋，试图在圆圈的直径上形成一排 3 个棋子的样式，即同时占据中心点及一个直径上的两个端点。
6. 玩家不能跳过对手的棋子，也不能将它们从棋盘上击落。棋子只能移动到空的点位上。任意两个棋子不能占据同一个单元格。玩家不能跳过回合。
7. 最先在圆圈的直径上形成三个棋子连成一排样式的玩家获胜。

### 关于策略：

1. Rota 是一款没有任何运气成分的纯策略游戏，任何情况下的概率确定。
2. 玩家必须安排他的棋子，使得他的棋子不会被挡住，且已经占据中心点位。先走的玩家自然会先尝试占领中心点位，而对手要阻止先手玩家将第三个棋子与其他两个棋子排成一排。
3. 游戏时间相对较短。
4. Rota游戏不会平局，总有赢家。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0722/190629_249b3f81_5631341.jpeg "Rota-carved-into-the-floor-tiles-of-Via-Arcadia-in-Ephesus-Turkey-Photo-Juan-Carlos-Campos-3.jpg")

> Rota carved into the floor tiles of Via Arcadia in Ephesus, Turkey. Photo: Juan Carlos Campos, July 2011.

![输入图片说明](https://images.gitee.com/uploads/images/2022/0722/190013_1a545dbb_5631341.jpeg "Rota-carved-in-the-floor-tiles-of-Via-Egnatia-road-in-Philippi-Macedonia-Greece-Photo-Félix-da-Costa-2014.jpg")

> Rota carved into the floor tiles of Via Egnatia road in Philippi, Macedonia (Greece). Photo: Félix da Costa, 2014.


### Bibliography:

1. [Merrill, Elmer Truesdell. “An Old Roman Game.” The Classical Journal 11, no. 6 (1916): 365-366.](http://www.jstor.org/stable/3287728)

# [Wagner 瓦格纳 现代的双人图论游戏](https://gitee.com/muyujia1729/siger/issues/I5GLQS#note_11593399_link)

瓦格纳是一个基于瓦格纳图的两人图论游戏，瓦格纳图是一个有8个顶点的莫比乌斯梯。在这个图中，直径和半径都等于2。 规则 “瓦格纳是一个非捕获游戏。最初，每个玩家在任何顶点（不是中心）放置一块石头以保持图形着色规则（即，没有两个友好的棋子彼此相邻）。每回合一个玩家将友方棋子移至相邻的空位，目标是将所有友方棋子组合成一个组。

> 正是这段文字，将古罗马帝国和图论和热词 莫比乌斯 联系在了一起，成为了优秀的科普素材。

【游戏说明】

Wagner 棋是一个基于 Wagner 图的双人图论游戏，Wagner图是一个由8个顶点，12条边组成的莫比乌斯梯，我们可以规定任意边的长度为2。

【游戏规则】

1. 最初，每个玩家在任何顶点放置一块石头以保持图形着色规则，即没有两个友好的棋子彼此相邻。
2. 每回合轮流移动，一个玩家将友方棋子沿着边移至相邻的空位
3. 目标是将所有友方棋子组合成一个组。

![输入图片说明](https://images.gitee.com/uploads/images/2021/1214/090804_862f3678_10089169.png) 

两个带点儿的白子，可以先走。可以走到临近的灰色棋位中。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0713/142554_a6f71a9c_5631341.png)

黑胜！

【作者】

Tahmina Begum

【创作日期】

2020-04-25

【棋规描述】

Wagner.lud

【数据库编号】

DLP.Games.542

# [独一无二的 Wagner 图](https://gitee.com/muyujia1729/siger/issues/I5GLQS#note_11633773_link)

### 属性

1.　作为一个莫比乌斯梯，瓦格纳图是非平面的。它可以不交叉地嵌入在环面或投影平面上，所以它也是一个环面图。它的周长为4，直径为2，半径为2，色数为3，同时是3-顶点连通和3-边连通图。

2.　Wagner图是一个顶点传递图，但不是边传递图。它的全自同构群与包含旋转和反射对称的八边形的16阶二面体群Dg同构。

3.　Wagner图的特征多项式是 $(x-3)(x-1)^2(x+1)(x^2+2x-1)^2$ ，它是唯一一个具有这个特征多项式的图。

4.　瓦格纳图是无三角形的，并且具有独立数3，提供了Ramsey数 $R(3,4)=9$ 的部分证明。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0714/163854_3910c480_11326491.png)

### 图子式理论（graph minors theory）

莫比乌斯梯在图子式理论中起着重要的作用。1937年，Klaus Wagner定理认为没有子式Ks的图可以通过使用团和运算来组合平面图和莫比乌斯梯M8，M8也被称为Wagner图。

### 画一个Wagner图

![输入图片说明](https://images.gitee.com/uploads/images/2022/0714/163938_e151798c_11326491.png)

Wagner图可以被绘制在一个拓扑莫比乌斯带上，如图所示

瓦格纳图是非平面的，可以将其画在莫比乌斯带上

来源：Wiki

### 提问环节：

- 非平面的图？
- 环面图？
- 顶点传递图？
- 边传递图？
- 它的全自同构群与包含旋转和反射对称的八边形的16阶二面体群Dg同构？
- 特征多项式？干什么用的？
- 无三角形的？
- 独立数3？
- Ramsey数？
- 部分证明？

可怜的人儿，终于知道，每个汉字都认识，连起来就都不认识的感觉啦．

必须出专门的论述和解释，才能理解这个莫比乌斯梯是怎样的玄妙？

图子式理论（graph minors theory）？

莫比乌斯梯？拓扑莫比乌斯带？Wagner图？

整个理解度不到１％　@muyujia1729 求助中．（同学们，跟上，我们[一起学图论](https://gitee.com/muyujia1729/siger/issues/I5GLQS#note_11673683_link)吧）

# [莫比乌斯，莫比乌斯梯，莫比乌斯环？](https://gitee.com/muyujia1729/siger/issues/I5GLQS#note_12020935_link)

### 莫比乌斯环 又称 莫比乌斯带

莫比乌斯环是德国数学家莫比乌斯还有约翰·李斯丁在1858年发现的，把一根纸条扭转一百八十度之后，两头粘起来。就形成了一个纸带圈，普通的纸带有两个面，正面和反面。可是莫比乌斯环只有一个面，小虫在上面爬可以不跨过边缘。(节选：《[彭罗斯阶梯真的走不出去？英国数学家不仅证明它存在，还画了出来](https://new.qq.com/omn/20200401/20200401A0PQ5900.html)》)

![输入图片说明](https://images.gitee.com/uploads/images/2022/0730/063546_865243cb_5631341.png "屏幕截图.png")

1858年, 德国数学家、天文学家莫比乌斯发现：一根纸带扭转180°, 再将两头粘接只形成一个曲面, 我们称此纸带为“莫比乌斯带”, 也叫“莫比乌斯圈”. (节选：《[神奇的莫比乌斯带](sig/math/神奇的莫比乌斯带.md)》【[笔记](https://gitee.com/muyujia1729/siger/issues/I5GLQS#note_12020940_link)】【[原文](https://zhuanlan.zhihu.com/p/358593700)】)

### 莫比乌斯梯

[独一无二的 Wagner 图](https://gitee.com/muyujia1729/siger/issues/I5GLQS#note_11633773_link) 中 画了一个Wagner图 形似一个被180°扭转首尾相连的梯子。这就是传说中的 莫比乌斯梯 吗？看来，本节还是无法满足同学们的好奇心，我的 提问环节：继续保留这个问题给老师们吧。

# 【后记】[考古现场，民族棋中国史](https://gitee.com/muyujia1729/siger/issues/I5GLQS#note_11670738_link)邀你来参加

- [红色例子，也可以说是三个子彼此相连。](https://gitee.com/muyujia1729/siger/issues/I5GLQS#note_11634207_link)
- [怎么看计算机演示的例子？](https://gitee.com/muyujia1729/siger/issues/I5GLQS#note_11634453_link)
- [要翻译的原文链接在哪？](https://gitee.com/muyujia1729/siger/issues/I5GLQS#note_11670738_link)
- [网站的算法错了，这种情况是非法的，但是却误判为胜利](https://gitee.com/muyujia1729/siger/issues/I5GLQS#note_11635164_link)

写到这儿，本期专题是第一个没有结尾的专题，就是因为 提问环节的出现，无休止的问题，会不会成十万个为什么呢？不会成为莫比乌斯问题吧（哈哈，我比喻车轱辘话的自创说法。），实在是无法收场，还是交给 [图论](../math/图论.md) 科普专题吧，请继续移步到 [数学频道](../math/)，会有专门的[图论小游戏](../math/图论.md#图论游戏-pencil-and-paper-games)体验呦。