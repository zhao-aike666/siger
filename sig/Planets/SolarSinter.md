- [The Solar Sinter by Markus Kayser | Dezeen](https://www.dezeen.com/2011/06/28/the-solar-sinter-by-markus-kayser/)
- [Solar Sinter melts sand to print 3D objects](https://www.theverge.com/2011/10/4/2468691/solar-sinter-melts-sand-to-print-3d-objects)
- [Solar-Powered 3D Printer Turns Desert Sand Into Glass](https://inhabitat.com/the-solar-powered-sinter-3d-printer-turns-desert-sand-into-glass/markus-kayser-solar-sinter1/)

<p><img width="706px" src="https://foruda.gitee.com/images/1681614386525611420/3455c8db_5631341.jpeg" title="656SolarSinter.jpg"></p>

> 我找到了一些具有您提到的一些组件的产品。例如，有一种名为“太阳能熔炉”的3D打印机，它使用阳光和沙子在沙漠中制作玻璃物体。另一个产品是一款开源的太阳能3D打印机，可以轻松携带并在零电力环境中运行。但是，我找不到具有您提到的所有组件的产品。您能否澄清您要寻找什么？

太让我刮目相看了，通过标题和几个部件的关键词，AI助手精确地找到了它的出处：

> "The Solar Sinter"是一种3D打印机，使用太阳能和沙子制作玻璃物体。该设备使用大型菲涅耳透镜聚焦阳光束，产生1400至1600摄氏度的温度。这足以熔化硅沙并将其转化为玻璃[^1^][1]。您可以在此链接中了解更多信息：https://www.dezeen.com/2011/06/28/the-solar-sinter-by-markus-kayser/

# [The Solar Sinter by Markus Kayser | Dezeen](https://www.dezeen.com/2011/06/28/the-solar-sinter-by-markus-kayser/)

<p dir="ltr"><strong dir="ltr">Show RCA 2011:</strong> German designer <a href="http://www.markuskayser.com/" rel="noopener noreferrer" dir="ltr">Markus Kayser</a> has built a 3D-printing machine that uses sunlight and sand to make glass objects in the desert.</p><figure><img title="The Solar Sinter by Markus Kayser" alt="The Solar Sinter by Markus Kayser" width="468" height="468" src="https://foruda.gitee.com/images/1681575033469375301/fa4b6067_5631341.png" class="c008"></figure><p>Called The Solar Sinter, the device uses a large Fresnel lens to focus a beam of sunlight, creating temperatures between 1400 and 1600 degrees Celsius.</p><figure><img title="The Solar Sinter by Markus Kayser" alt="The Solar Sinter by Markus Kayser" width="468" height="468" src="https://foruda.gitee.com/images/1681574975575411349/f08625d0_5631341.png" class="c008"></figure><p>This is hot enough to melt silica sand and build up glass shapes, layer by layer, inside a box of sand mounted under the lens.</p><figure><img title="The Solar Sinter by Markus Kayser" alt="The Solar Sinter by Markus Kayser" width="468" height="311" src="https://foruda.gitee.com/images/1681575081580456428/28363937_5631341.png" class="c008"></figure><p>Solar-powered motors move the box on an x and y axis along a computer-controlled path and a new layer of sand is sprinkled on top after each pass of the light beam.</p><figure><img title="The Solar Sinter by Markus Kayser" alt="The Solar Sinter by Markus Kayser" width="468" height="312" src="https://foruda.gitee.com/images/1681575109603359674/6944a35c_5631341.png" class="c008"></figure><p>Light&nbsp;sensors&nbsp;track the sun as it moves across the sky and the whole machine rotates on its base to ensure the lens is always producing the optimum level of heat.</p><figure><img title="The Solar Sinter by Markus Kayser" alt="The Solar Sinter by Markus Kayser" width="468" height="351" src="https://foruda.gitee.com/images/1681575132642286461/35b18b70_5631341.png" class="c008"></figure><p>Once all the layers have been melted into place the piece is allowed to cool and dug out from the sand box.</p><figure><img title="The Solar Sinter by Markus Kayser" alt="The Solar Sinter by Markus Kayser" width="468" height="468" src="https://foruda.gitee.com/images/1681575155642451784/6b9a4360_5631341.png" class="c008"></figure><p dir="ltr">Kayser developed the project while studying on the MA Design Products course at the <a href="http://www.rca.ac.uk/" rel="noopener noreferrer" dir="ltr">Royal College of Art</a>.</p><figure><img title="The Solar Sinter by Markus Kayser" alt="The Solar Sinter by Markus Kayser" width="468" height="313" src="https://foruda.gitee.com/images/1681575180488606060/f0c5b5c9_5631341.png" class="c008"></figure><p dir="ltr">Graduate exhibition <a href="http://www.rca.ac.uk/Default.aspx?ContentID=511825&amp;CategoryID=36775" rel="noopener noreferrer" dir="ltr">Show RCA 2011</a> continues in London until 3 July.</p><figure><img title="The Solar Sinter by Markus Kayser" alt="The Solar Sinter by Markus Kayser" width="468" height="468" src="https://foruda.gitee.com/images/1681575208503172108/af81998c_5631341.png" class="c008"></figure><p>Here are some more details from Kayser:</p><p>In a world increasingly concerned with questions of energy production and raw material shortages, this project explores the potential of desert manufacturing, where energy and material occur in abundance.&nbsp;In this experiment sunlight and sand are used as raw energy and material to produce glass objects using a 3D printing process, that combines natural energy and material with high-tech production technology.&nbsp;Solar-sintering aims to raise questions about the future of manufacturing and triggers dreams of the full utilisation of the production potential of the world’s most efficient energy resource - the sun. Whilst not providing definitive answers, this experiment aims to provide a point of departure for fresh thinking.</p><figure><img title="The Solar Sinter by Markus Kayser" alt="The Solar Sinter by Markus Kayser" width="468" height="468" src="https://foruda.gitee.com/images/1681575233433252083/8ac0612f_5631341.png" class="c008"></figure><p>In the deserts of the world two elements dominate - sun and sand. The former offers a vast energy source of huge potential, the latter an almost unlimited supply of silica in the form of quartz. Silicia sand when heated to melting point and allowed to cool solidifies as glass. This process of converting a powdery substance via a heating process into a solid form is known as sintering and has in recent years become a central process in design prototyping known as 3D printing or SLS (selective laser sintering). These 3D printers use laser technology to create very precise 3D objects from a variety of powdered plastics, resins and metals - the objects being the exact physical counterparts of the computer-drawn 3D designs inputted by the designer. By using the sun’s rays instead of a laser and sand instead of resins, I had the basis of an entirely new solar-powered machine and production process for making glass objects that taps into the abundant supplies of sun and sand to be found in the deserts of the world.</p><p>My first manually operated solar-sintering machine was tested in February 2011 in the Moroccan desert with encouraging results that led to the development of the current larger and fully automated computer-driven version - the Solar-Sinter. The Solar-Sinter was completed in mid-May and later that month I took this experimental machine to the Sahara desert near Siwa, Egypt, for a two week testing period. The machine and the results of these first experiments presented here represent the initial significant steps towards what I envisage as a new solar-powered production tool of great potential.</p><figure><img title="The Solar Sinter by Markus Kayser" alt="The Solar Sinter by Markus Kayser" width="468" height="323" src="https://foruda.gitee.com/images/1681575263578659221/e6b685dd_5631341.png" class="c008"></figure><p>The machine</p><p>The Solar-Sinter machine is based on the mechanical principles of a 3D printer.</p><p>A large Fresnel lens (1.4 x 1.0 metre) is positioned so that it faces the sun at all times via an electronic sun-tracking device, which moves the lens in vertical and horizontal direction and rotates the entire machine about its base throughout the day. The lens is positioned with its focal point directed at the centre of the machine and at the height of the top of the sand box where the objects will be built up layer by layer. Stepper motors drive two aluminium frames that move the sand box in the X and Y axes. Within the box is a platform that can move the vat of sand along the vertical Z axis, lowering the box a set amount at the end of each layer cycle to allow fresh sand to be loaded and levelled at the focal point.</p><p>Two photovoltaic panels provide electricity to charge a battery, which in turn drives the motors and electronics of the machine. The photovoltaic panels also act as a counterweight for the lens aided by additional weights made from bottles filled with sand.</p><figure><img title="The Solar Sinter by Markus Kayser" alt="The Solar Sinter by Markus Kayser" width="468" height="375" src="https://foruda.gitee.com/images/1681575286049942080/fe1aabb3_5631341.png" class="c008"></figure><p>3D printing process with sand and sunlight</p><p>The machine is run off an electronic board and can be controlled using a keypad and an LCD screen. Computer drawn models of the objects to be produced are inputted into the machine via an SD card. These files carry the code that directs the machine to move the sand box along the X, Y coordinates at a carefully calibrated speed, whilst the lens focuses a beam of light that produces temperatures between 1400°C and 1600°C, more than enough to melt the sand. Over a number of hours, layer by layer, an object is built within the confines of the sand box, only its uppermost layer visible at any one time. When the print is completed the object is allowed to cool before being dug out of the sand box. The objects have rough sandy reverse side whilst the top surface is hard glass. The exact colour of the resulting glass will depend on the composition of the sand, different deserts producing different results. By mixing sands, combinatory colours and material qualities may be achieved.</p><figure><img title="The Solar Sinter by Markus Kayser" alt="The Solar Sinter by Markus Kayser" width="468" height="468" src="https://foruda.gitee.com/images/1681575312397221089/353fe661_5631341.png" class="c008"></figure><p>Machine and man</p><p>With the scenario of a single person’s utilisation of the machine in the desert, I play with ideas of how an individual could use the machine to produce objects.</p><p>In this first instance the creation of artefacts made by sunlight and sand is an act of pure experimentation and expression of ‘possibility’, but what of the future?&nbsp;I hope that the machine and the objects it created, stimulate debate about the vast potential of solar energy and naturally abundant materials like silica sand. These first experiments are simply an early manifestation of that potential.</p><figure><img title="The Solar Sinter by Markus Kayser" alt="The Solar Sinter by Markus Kayser" width="468" height="468" src="https://foruda.gitee.com/images/1681575336172925551/035a70b1_5631341.png" class="c008"></figure><p>Machine and community</p><p>In the context of a desert-based community, the Solar-Sinter machine could be used to create unique artefacts and functional objects, but also act as a catalyst for solar innovation for more prosaic and immediate needs. Further development could lead to additional solar machine processes such as solar welding, cutting, bending and smelting to build up a fully functioning solar workshop.</p><p>The vibrant and global ‘open-source’ community is already active in developing software and hardware for 3D printers and could play a key role in the rapid development of these technologies. The Solar-Sinter could simply be the starting point for a variety of further applications.</p><figure><img title="The Solar Sinter by Markus Kayser" alt="The Solar Sinter by Markus Kayser" width="468" height="468" src="https://foruda.gitee.com/images/1681575385373652387/da59c740_5631341.png" class="c008"></figure><p>Machine and manufacture</p><p>In 1933, through the pages of ‘Modern Mechanix’ magazine, W.W. Beach was already imagining canals and "auto roads“ melted into the desert using sunlight focused through immense lenses. This fantastical large-scale approach is much closer to reality today, with ‘desert factories’ using sunlight as their power a tangible prospect. This image of a multiplicity of machines working in a natural cycle from dusk till Dawn presents a new idea of what manufacturing could be.</p><p>The objects could be anything from glass vessels to eventually the glass surfaces for photovoltaic panels that provide the factories power source… and, as Mr. Beach imagined 78 years ago, the water channels and glass roads that service them.</p><figure><img title="The Solar Sinter by Markus Kayser" alt="The Solar Sinter by Markus Kayser" width="468" height="468" src="https://foruda.gitee.com/images/1681575411982479223/939952e2_5631341.png" class="c008"></figure><p>Dreaming of architecture</p><p>Printing directly onto the desert floor with multiple lenses melting the sand into walls, eventually building architecture in desert environments, could also be a real prospect.</p><p>Experiments in 3D printing technologies are already reaching towards an architectural scale and it is not hard to imagine that, if partnered with the solar-sintering process demonstrated by the Solar-Sinter machine, this could indeed lead to a new desert-based architecture.</p>

马库斯·凯瑟的太阳烧结矿 |德泽恩
展会RCA 2011：德国设计师Markus Kayser制造了一台3D打印机，利用阳光和沙子在沙漠中制造玻璃制品。

The Solar Sinter by Markus Kayser
该设备被称为太阳烧结，使用大型菲涅耳透镜聚焦一束阳光，产生1400至1600摄氏度之间的温度。

The Solar Sinter by Markus Kayser
这足以融化硅砂，并在安装在镜头下方的沙盒内一层一层地形成玻璃形状。

The Solar Sinter by Markus Kayser
太阳能电机沿着计算机控制的路径在x轴和y轴上移动盒子，每次光束通过后，都会在顶部撒上一层新的沙子。

The Solar Sinter by Markus Kayser
光传感器跟踪太阳在天空中移动，整个机器在其底座上旋转，以确保透镜始终产生最佳热量水平。

The Solar Sinter by Markus Kayser
一旦所有层都融化到位，就可以让碎片冷却并从沙盒中挖出。

The Solar Sinter by Markus Kayser
Kayser在皇家艺术学院学习MA设计产品课程时开发了该项目。

The Solar Sinter by Markus Kayser
毕业生展览展RCA 2011在伦敦持续至3月<>日。

The Solar Sinter by Markus Kayser
以下是Kayser的更多细节：

在一个越来越关注能源生产和原材料短缺问题的世界中，该项目探索了沙漠制造的潜力，其中能源和材料大量存在。在这个实验中，太阳光和沙子被用作原材料和材料，使用3D打印工艺生产玻璃制品，将自然能源和材料与高科技生产技术相结合。太阳能烧结旨在提出有关制造业未来的问题，并引发充分利用世界上最高效能源 - 太阳能的生产潜力的梦想。虽然没有提供明确的答案，但这个实验旨在为新思维提供一个出发点。

The Solar Sinter by Markus Kayser
在世界沙漠中，有两个元素占主导地位——阳光和沙子。前者提供了具有巨大潜力的巨大能源，后者以石英的形式提供了几乎无限的二氧化硅供应。硅砂加热到熔点并冷却后凝固为玻璃。这种通过加热过程将粉末状物质转化为固体形式的过程称为烧结，近年来已成为设计原型中称为3D打印或SLS（选择性激光烧结）的核心过程。这些3D打印机使用激光技术从各种粉末塑料，树脂和金属中创建非常精确的3D对象 - 这些对象是设计师输入的计算机绘制的3D设计的精确物理对应物。通过使用太阳光线代替激光，用沙子代替树脂，我有了全新的太阳能机器和生产工艺的基础，用于制造玻璃制品，利用世界上沙漠中丰富的阳光和沙子供应。

2011 年 <> 月，我的第一台手动操作的太阳能烧结机在摩洛哥沙漠进行了测试，取得了令人鼓舞的结果，从而开发了当前更大的全自动计算机驱动版本 - Solar-Sinter。Solar-Sinter于五月中旬完成，当月晚些时候，我把这台实验机器带到埃及锡瓦附近的撒哈拉沙漠，进行了为期两周的测试。这台机器和这里介绍的这些初步实验的结果代表了朝着我所设想的具有巨大潜力的新型太阳能生产工具迈出的初步重要步骤。

The Solar Sinter by Markus Kayser
机器

Solar-Sinter机器基于3D打印机的机械原理。

大型菲涅耳透镜（1.4 x 1.0米）通过电子太阳跟踪装置定位，使其始终面向太阳，该装置在垂直和水平方向上移动透镜，并使整个机器全天围绕其底座旋转。镜头定位时，其焦点指向机器的中心和沙盒顶部的高度，物体将逐层堆积。步进电机驱动两个铝制框架，在 X 轴和 Y 轴上移动沙箱。盒子内有一个平台，可以沿垂直Z轴移动沙桶，在每个层循环结束时将盒子降低一定量，以便在焦点处装载和调平新鲜的沙子。

两个光伏板提供电力为电池充电，电池反过来驱动机器的电机和电子设备。光伏板还充当透镜的配重，并由装满沙子的瓶子制成的额外配重辅助。

The Solar Sinter by Markus Kayser
沙子和阳光的3D打印过程

该机器由电子板运行，可以使用键盘和LCD屏幕进行控制。计算机绘制的待生产物体模型通过SD卡输入机器。这些文件携带的代码指示机器以仔细校准的速度沿X，Y坐标移动沙盒，而镜头聚焦一束产生1400°C至1600°C温度的光束，足以融化沙子。在几个小时内，一层一层地构建一个物体，在任何时候都只能看到它最上面的一层。打印完成后，物体在从沙盒中挖出之前被允许冷却。物体有粗糙的沙质反面，而顶面是硬玻璃。所得玻璃的确切颜色将取决于沙子的成分，不同的沙漠产生不同的结果。通过混合沙子，可以实现组合的颜色和材料质量。

The Solar Sinter by Markus Kayser
机器和人

在一个人在沙漠中使用机器的场景中，我玩弄了一个人如何使用机器来生产物体的想法。

首先，用阳光和沙子创造人工制品是一种纯粹的实验和“可能性”的表达，但未来呢？我希望这台机器和它创造的物体能激发人们对太阳能和硅砂等天然丰富材料的巨大潜力的争论。这些最初的实验只是这种潜力的早期表现。

The Solar Sinter by Markus Kayser
机器和社区

在沙漠社区的背景下，Solar-Sinter机器可用于创造独特的人工制品和功能对象，也可以作为太阳能创新的催化剂，以满足更平淡无奇和直接的需求。进一步发展可能会导致额外的太阳能机器工艺，如太阳能焊接、切割、弯曲和冶炼，以建立一个功能齐全的太阳能车间。

充满活力的全球“开源”社区已经积极开发3D打印机的软件和硬件，并且可以在这些技术的快速发展中发挥关键作用。太阳能烧结矿可以简单地作为各种进一步应用的起点。

The Solar Sinter by Markus Kayser
机器和制造

1933年，通过“现代机械”杂志的页面，W.W.海滩已经想象运河和“汽车道路”利用通过巨大透镜聚焦的阳光融化到沙漠中。这种奇幻般的大规模方法更接近今天的现实，以阳光为动力的“沙漠工厂”是一个切实的前景。这幅从黄昏到黎明的多种机器在自然循环中工作的图像提出了制造业可能是什么的新想法。

这些物体可以是任何东西，从玻璃容器到最终为工厂提供电源的光伏板的玻璃表面......而且，正如比奇先生78年前想象的那样，为他们服务的水道和玻璃道路。

马库斯·凯瑟的《太阳烧结矿》
建筑的梦想

用多个镜头直接打印到沙漠地面上，将沙子融化成墙壁，最终在沙漠环境中建造建筑，也可能是一个真正的前景。

3D打印技术的实验已经达到了建筑规模，不难想象，如果与太阳能烧结机展示的太阳能烧结工艺合作，这确实可能导致一种新的基于沙漠的建筑。


- 搬运工：[3D打印机使沙子变玻璃](https://www.huxiu.com/article/464053.html)

  - https://www.bilibili.com/video/BV1gE411x7LS/

    > 会玩！牛人利用阳光将沙子作为原料，通过3D打印制作出来一个玻璃物体

  - https://www.bilibili.com/video/BV1Z4411r7r8

    > 沙子可以做产品，沙子3d打印太奇妙，似乎沙漠也能让你发大财

- The-Solar-Sinter-by-Markus-Kayser , what's the design details?

  > "The Solar Sinter" is based on the mechanical principles of a 3D printer. It is made up of seven stations: a photovoltaic panel, the focal point for drawing the sun’s rays, a sun tracker, fresnel lens (for magnifying the rays), a battery, controlling electronics, and finally a silver tent dubbed the “office,” where Kayser can shield himself from the hot sun while monitoring the process[^1^][1]. You can find more details about it in this link: https://inhabitat.com/the-solar-powered-sinter-3d-printer-turns-desert-sand-into-glass/

# [Solar Sinter melts sand to print 3D objects](https://www.theverge.com/2011/10/4/2468691/solar-sinter-melts-sand-to-print-3d-objects)

By JOSEPH PARISH

Source POPULAR SCIENCE

Oct 5, 2011, 5:04 AM GMT+8|0 Comments
Share this story

![输入图片说明](https://foruda.gitee.com/images/1681576741957825797/3c41f71d_5631341.png "屏幕截图")

Solar Sinter

London design student Markus Kayser has taken playing with a magnifying glass to a whole new level with his solar-powered 3D sand-printer. The "Solar Sinter" uses photovoltaic cells to provide power for the electronics, and a giant lens focuses the sun's energy to a point so hot, it melts sand into glass. The machine prints 3D objects designed in a CAD system by moving a box of sand around under the focused light. Each layer of sand must be poured on and leveled by hand as the object is melted into existence from the bottom up — motors and sensors keep the lens and solar cells pointed directly at the sun during the lengthy process. This thing would never work in foggy England, so Kayser tested the apparatus in the Sahara for obvious reasons. Check out the video of this contraption in action below.

太阳能烧结矿融化沙子以打印3D物体

作者：约瑟夫·帕里什

来源 科普

5年2011月5日 上午04：8 GMT+<>|评论0 评论

伦敦设计专业学生Markus Kayser通过他的太阳能3D沙盘打印机将放大镜的玩法提升到了一个全新的水平。“太阳能烧结”使用光伏电池为电子设备提供电力，一个巨大的透镜将太阳的能量聚焦到一个如此热的点，它将沙子融化成玻璃。该机器通过在聚焦光下移动一盒沙子来打印在CAD系统中设计的3D对象。当物体自下而上融化成存在时，必须用手浇注和调平每一层沙子——在这个漫长的过程中，电机和传感器使透镜和太阳能电池直接指向太阳。这个东西在雾蒙蒙的英格兰永远行不通，所以凯瑟出于显而易见的原因在撒哈拉沙漠测试了该设备。在下面查看这个装置的视频。

- http://player.vimeo.com/video/25401444

https://inhabitat.com/the-solar-powered-sinter-3d-printer-turns-desert-sand-into-glass/ 
"Solar-Powered 3D Printer Turns Desert Sand Into Glass"

https://inhabitat.com/the-solar-powered-sinter-3d-printer-turns-desert-sand-into-glass/markus-kayser-solar-sinter1/

# [Solar-Powered 3D Printer Turns Desert Sand Into Glass](https://inhabitat.com/the-solar-powered-sinter-3d-printer-turns-desert-sand-into-glass/markus-kayser-solar-sinter1/)

![输入图片说明](https://foruda.gitee.com/images/1681577097151732163/1e850802_5631341.png "屏幕截图")

 _The sun's rays can be harnessed to power everything from homes to gadgets, but one graduate student is using the sun to create a super-printer capable of printing elaborate glassware. Markus Kayser took his graduate project all the way to the sands of a_ 

### Markus Kayser Solar Sinter 1 of 7

The sun's rays can be harnessed to power everything from homes to gadgets, but one graduate student is using the sun to create a super-printer capable of printing elaborate glassware. [Markus Kayser](http://www.markuskayser.com/) took his graduate project all the way to the sands of the Sahara in Egypt to create his innovative idea dubbed the 'Solar Sinter'. The incredible design uses a [3D digital printer](http://inhabitat.com/dirk-van-der-kooij-creates-his-modern-endless-chairs-from-recycled-refrigerators/) and the sun’s rays to turn the sand into incredible glass bowls and sculptures that are out of this world.

![输入图片说明](https://foruda.gitee.com/images/1681577125044550378/f00a3f18_5631341.png "屏幕截图")

 _The Solar Sinter is made up of seven stations- a photovoltaic panel, the focal point for drawing the sun’s rays, a sun tracker, fresnal lens (for magnifying the rays), a battery, controlling electronics, and finally a silver tent dubbed the where can_ 

### Markus Kayser Solar Sinter 2 of 7

The Solar Sinter is made up of seven stations- [a photovoltaic panel](http://inhabitat.com/voltaic-unveils-a-solar-charger-case-for-the-eco-minded-ipad-user/), the focal point for drawing the sun’s rays, a sun tracker, fresnal lens (for magnifying the rays), a battery, controlling electronics, and finally a silver tent dubbed the “office,” where Kayser can shield himself from the hot sun while monitoring the process.

![输入图片说明](https://foruda.gitee.com/images/1681577243617494259/7bf88b25_5631341.png "屏幕截图")

> Dragging the mini station far into the desert, Kayser utilizes the abundant natural energy. The 3D printer has the capability of creating any shape - be it bowls, sculptures and even furniture.

### Markus Kayser Solar Sinter 3 of 7

Dragging the mini station far into the desert, Kayser utilizes the abundant natural energy. The 3D printer has the capability of creating any shape - be it bowls, sculptures and even furniture.

![输入图片说明](https://foruda.gitee.com/images/1681577272494648030/613710ff_5631341.png "屏幕截图")

> The Solar Sinter turns a remote desert area into a high tech production facility for high-end design.

### Markus Kayser Solar Sinter 4 of 7

The Solar Sinter turns a [remote desert area](http://inhabitat.com/google-ups-research-development-to-make-solar-cheaper-than-coal/) into a high tech production facility for high-end design.

![输入图片说明](https://foruda.gitee.com/images/1681577303656135030/69d8afb5_5631341.png "屏幕截图")

 _Inside the solar powered 3D printer, the sands of the desert replace traditional resin in the production process. The sands are melted and molded into varying shapes, and bowls using the 3D printer._ 

### Markus Kayser Solar Sinter bowl 5 of 7

Inside the [solar powered](http://inhabitat.com/qsolar-kristal-colored-solar-panels-could-replace-walls-and-windows/) 3D printer, the sands of the desert replace traditional resin in the production process. The sands are melted and molded into varying shapes, and [bowls](http://inhabitat.com/the-100-mile-design-challenge-forces-students-to-think-local-with-their-designs/) using the 3D printer.

![输入图片说明](https://foruda.gitee.com/images/1681577351593236854/b5be6a81_5631341.png "屏幕截图")

 _The entire process is as beautiful as the objects themselves, beautifully glowing and transforming as the sands melt together, evoking footage of the birth of stars in space._ 

### Markus Kayser, Solar Sinter, melting process 6 of 7

The entire process is as beautiful as the objects themselves, beautifully glowing and transforming as the sands melt together, evoking footage of the birth of stars in space.

![输入图片说明](https://foruda.gitee.com/images/1681577380624731809/8ca79b50_5631341.png "屏幕截图")

 _Kayser is dedicated to creating projects that seek to prove the endless possibility of solar and desert production- and its endless supply of raw solar power. His previous project, Sun Cutter, used the sun’s rays to make delicate cuts in wood, like a_ 

### Markus Kayser Solar Sinter 7 of 7

Kayser is dedicated to creating projects that seek to prove the endless possibility of [solar](http://inhabitat.com/white-house-fails-to-put-up-solar-panels-by-their-own-deadline/) and desert production- and its [endless supply of raw solar power](http://inhabitat.com/video-gemasolar-plant-in-spain-is-the-worlds-first-24hr-solar-plant/). His previous project, Sun Cutter, used the sun’s rays to make delicate cuts in wood, like a fine laser. The Solar Sinter is currently on display at the Royal College of Art.

# Amazing Solar-Powered 3D Printer Showcased at the London Design Festival

![输入图片说明](https://foruda.gitee.com/images/1681577450621409246/72e3aad3_5631341.png "屏幕截图")

 _Markus Kayser's brilliant solar-powered 3D printer works using two natural resources from the world's deserts that are in no jeopardy of being used up any time soon: sun and sand. The printer beams the sun's rays onto sand, which is then transformed into fascinating glass objects of an almost unlimited variety. We previously brought you a few images of the 3D printer being tested in Egypt, but today we snapped a few close-ups of the homegrown machine at the London Design Festival, where the Solar..._ 

### Solar Sinter by Markus Kayser at London Design Festival 1 of 10

[Markus Kayser's brilliant solar-powered 3D printer](http://www.markuskayser.com/) works using two natural resources from the world's deserts that are in no jeopardy of being used up any time soon: sun and sand. The printer beams the sun's rays onto sand, which is then transformed into fascinating glass objects of an almost unlimited variety. We previously brought you a few images of the [3D printer being tested in Egypt](http://inhabitat.com/the-solar-powered-sinter-3d-printer-turns-desert-sand-into-glass/), but today we snapped a few close-ups of the homegrown machine at the [London Design Festival](http://www.londondesignfestival.com/), where the Solar Sinter is proudly displayed at the Royal College of Art - [Markus Kayser's](http://www.markuskayser.com/) alma mater.

![输入图片说明](https://foruda.gitee.com/images/1681577532647473290/f5b0f529_5631341.png "屏幕截图")

 _The new images show details of this incredible machine's minimalist parts: a fresnel lens that concentrates the solar beams, a couple of small photovoltaic panels, an electric box/battery covered in aluminum foil, and a sun tracker._ 

### Solar Sinter by Markus Kayser at London Design Festival 2 of 10

The new images show details of this incredible machine's minimalist parts: a fresnel lens that concentrates the solar beams, a couple of small [photovoltaic panels](http://inhabitat.com/breaking-solar-power-breakthrough-could-render-solar-cells-obsolete/), an electric box/battery covered in aluminum foil, and a sun tracker.

![输入图片说明](https://foruda.gitee.com/images/1681577570030618943/cbb53d18_5631341.png "屏幕截图")

 _On the tray where the sun is beamed across the sand is an interesting silica sculpture ostensibly left over from Kayser's last desert adventure._ 

### Solar Sinter by Markus Kayser at London Design Festival 3 of 10

On the tray where the sun is beamed across the sand is an interesting silica sculpture ostensibly left over from Kayser's last desert adventure.

![输入图片说明](https://foruda.gitee.com/images/1681577594826837136/8cddb292_5631341.png "屏幕截图")

 _Kayser has expressed an interest in developing this technology to such an extent that it could be used to create sustainable construction materials for future buildings._ 

### Solar Sinter by Markus Kayser at London Design Festival 4 of 10

Kayser has expressed an interest in developing this technology to such an extent that it could be used to create [sustainable construction materials](http://inhabitat.com/tag/sustainable-construction/) for future buildings.

![输入图片说明](https://foruda.gitee.com/images/1681577635632337932/fe5308df_5631341.png "屏幕截图")

 _If you're in the city checking out the London Design Festival, we highly recommend a trip over to the Royal College of Art's SUSTAIN exhibit to catch a glimpse of this award-winning design._ 

  
### Solar Sinter by Markus Kayser at London Design Festival 5 of 10

If you're in the city checking out the [London Design Festival](http://inhabitat.com/gigantic-timber-wave-installation-welcomes-the-london-design-festival-to-va/), we highly recommend a trip over to the [Royal College of Art's SUSTAIN exhibit](http://inhabitat.com/sustain-7-exciting-green-designs-from-the-royal-college-of-art-at-the-london-design-festival/) to catch a glimpse of this award-winning design.

![输入图片说明](https://foruda.gitee.com/images/1681577684473217562/de3033fb_5631341.png "屏幕截图")

 _These glass objects are remarkably durable._ 

### Solar Sinter by Markus Kayser at London Design Festival 6 of 10

These glass objects are remarkably durable.

![输入图片说明](https://foruda.gitee.com/images/1681577707932734038/f35e4ebb_5631341.png "屏幕截图")

 _On another table a bowl is displayed along with a plate._ 

### Solar Sinter by Markus Kayser at London Design Festival 7 of 10

On another table a bowl is displayed along with a plate.

![输入图片说明](https://foruda.gitee.com/images/1681577730308949579/c23cf922_5631341.png "屏幕截图")

 _The device is currently on display at Kayser's alma mater - the Royal College of Art - as part of the 2011 London Design Festival._ 

### Solar Sinter by Markus Kayser at London Design Festival 8 of 10

The device is currently on display at Kayser's alma mater - the Royal College of Art - as part of the 2011 London Design Festival.

![输入图片说明](https://foruda.gitee.com/images/1681577761055177909/e0d6c53d_5631341.png "屏幕截图")

 _This project's reliance on only sun and sand gives it an incredible design edge._ 

### Solar Sinter by Markus Kayser at London Design Festival 9 of 10

This project's reliance on only sun and sand gives it an incredible design edge.

![输入图片说明](https://foruda.gitee.com/images/1681577784581821975/d7e7e6bd_5631341.png "屏幕截图")

 _These natural resources that is not in jeopardy of being used up any time soon!_ 

### Solar Sinter by Markus Kayser at London Design Festival 10 of 10

These natural resources that is not in jeopardy of being used up any time soon!

# SUSTAIN: 7 Exciting Green Designs from the Royal College of Art at the London Design Festival

![输入图片说明](https://foruda.gitee.com/images/1681577848159724309/b5281e29_5631341.png "屏幕截图")

 _Our coverage of the 2011 London Design Festival will be drawing to a close pretty soon (sadly - it's been so much fun!), but we definitely plan to go out with a bang. That's easy to do with Sustain, an exhibit of several remarkable designs from London's_ 

  
### Sustain at London Design Festival 2011 1 of 7

Our coverage of [the 2011 London Design Festival](http://inhabitat.com/tag/2011-london-design-week/) will be drawing to a close pretty soon (sadly - it's been so much fun!), but we definitely plan to go out with a bang. That's easy to do with Sustain, an exhibit of several remarkable designs from London's Royal College of Art. Although we are keen to showcase everything from materials to funky green cars, we were especially excited to stumble across a couple of previously-covered projects in person, including the amazing [Solar Sinter 3D Printer](http://www.markuskayser.com/) (seen above). Skip past the jump for our exclusive Design Festival pictures straight from the [Royal College of Art](http://www.londondesignfestival.com/events/rca-ldf)!

太阳光线可以用来为从家庭到小工具的所有东西提供动力，但一名研究生正在利用太阳来制造一种能够打印精致玻璃器皿的超级打印机。马库斯·凯瑟（Markus Kayser）将他的研究生项目一直带到了沙滩上。

马库斯·凯瑟 太阳烧结矿 1 的 7
太阳光线可以用来为从家庭到小工具的所有东西提供动力，但一名研究生正在利用太阳来制造一种能够打印精致玻璃器皿的超级打印机。Markus Kayser将他的研究生项目一路带到埃及撒哈拉沙漠的沙滩上，创造了他被称为“太阳烧结”的创新想法。令人难以置信的设计使用3D数字打印机和太阳光线将沙子变成令人难以置信的玻璃碗和雕塑，这些玻璃碗和雕塑是这个世界之外的。

输入图片说明

太阳烧结矿由七个站组成 - 一个光伏板，绘制太阳光线的焦点，太阳跟踪器，微透镜（用于放大光线），电池，控制电子设备，最后是一个被称为“Where Can”的银色帐篷

马库斯·凯瑟 太阳烧结矿 2 的 7
太阳烧结矿由七个站组成 - 一个光伏板，绘制太阳光线的焦点，太阳跟踪器，微光透镜（用于放大光线），电池，控制电子设备，最后是一个被称为“办公室”的银色帐篷，Kayser可以在监控过程的同时保护自己免受烈日照射。

输入图片说明

将迷你站拖入沙漠深处，凯瑟利用了丰富的自然能量。3D打印机能够创建任何形状 - 无论是碗，雕塑甚至家具。

马库斯·凯瑟 太阳烧结矿 3 的 7
将迷你站拖入沙漠深处，凯瑟利用了丰富的自然能量。3D打印机能够创建任何形状 - 无论是碗，雕塑甚至家具。

输入图片说明

太阳能烧结矿将偏远的沙漠地区变成了高端设计的高科技生产设施。

马库斯·凯瑟 太阳烧结矿 4 的 7
太阳能烧结矿将偏远的沙漠地区变成了高端设计的高科技生产设施。

输入图片说明

在太阳能3D打印机内部，沙漠的沙子在生产过程中取代了传统的树脂。沙子被熔化并模制成不同的形状，并使用3D打印机和碗。

马库斯·凯瑟太阳能烧结碗 5 的 7
在太阳能3D打印机内部，沙漠的沙子在生产过程中取代了传统的树脂。沙子被熔化并模制成不同的形状，并使用3D打印机和碗。

输入图片说明

整个过程与物体本身一样美丽，随着沙子融化在一起而美丽地发光和转变，唤起太空中恒星诞生的镜头。

Markus Kayser， Solar Sinter， 熔化过程 6 of 7
整个过程与物体本身一样美丽，随着沙子融化在一起而美丽地发光和转变，唤起太空中恒星诞生的镜头。

输入图片说明

Kayser致力于创建项目，旨在证明太阳能和沙漠生产的无限可能性 - 以及其无穷无尽的原始太阳能供应。他之前的项目“Sun Cutter”利用太阳光线在木材上进行精细的切割，就像

马库斯·凯瑟 太阳烧结矿 7 的 7
Kayser致力于创建项目，旨在证明太阳能和沙漠生产的无限可能性 - 以及其无穷无尽的原始太阳能供应。他以前的项目Sun Cutter利用太阳光线在木材上进行精细切割，就像精细的激光一样。太阳烧结矿目前正在皇家艺术学院展出。

令人惊叹的太阳能3D打印机在伦敦设计节上展出
输入图片说明

Markus Kayser出色的太阳能3D打印机使用来自世界沙漠的两种自然资源，这两种自然资源不会很快被耗尽：阳光和沙滩。打印机将太阳光线照射到沙子上，然后变成几乎无限种类的迷人玻璃物体。我们之前为您带来了一些在埃及进行测试的3D打印机的图像，但今天我们在伦敦设计节上拍摄了家用机器的一些特写镜头，在那里太阳能...

Markus Kayser在伦敦设计节上的太阳能烧结矿 1 / 10
Markus Kayser出色的太阳能3D打印机使用来自世界沙漠的两种自然资源，这两种自然资源不会很快被耗尽：阳光和沙滩。打印机将太阳光线照射到沙子上，然后变成几乎无限种类的迷人玻璃物体。我们之前为您带来了一些在埃及进行测试的3D打印机的图像，但今天我们在伦敦设计节上拍摄了这台本土机器的一些特写镜头，在那里太阳烧结矿自豪地在皇家艺术学院展出 - 马库斯·凯瑟的母校。

输入图片说明

新图像显示了这台令人难以置信的机器极简部件的细节：一个集中太阳光束的菲涅耳透镜，几个小光伏板，一个覆盖铝箔的电箱/电池和一个太阳跟踪器。

Markus Kayser在伦敦设计节上的太阳能烧结矿 2 / 10
新图像显示了这台令人难以置信的机器极简部件的细节：一个集中太阳光束的菲涅耳透镜，几个小光伏板，一个覆盖铝箔的电箱/电池和一个太阳跟踪器。

输入图片说明

在阳光照射在沙滩上的托盘上，有一个有趣的二氧化硅雕塑，表面上是凯瑟最后一次沙漠冒险留下的。

Markus Kayser在伦敦设计节上的太阳能烧结矿 3 / 10
在阳光照射在沙滩上的托盘上，有一个有趣的二氧化硅雕塑，表面上是凯瑟最后一次沙漠冒险留下的。

输入图片说明

Kayser表示有兴趣开发这项技术，使其可用于为未来的建筑创造可持续的建筑材料。

Markus Kayser在伦敦设计节上的太阳能烧结矿 4 / 10
Kayser表示有兴趣开发这项技术，使其可用于为未来的建筑创造可持续的建筑材料。

输入图片说明

如果您在伦敦参加伦敦设计节，我们强烈建议您前往皇家艺术学院的SUSTAIN展览，一睹这一屡获殊荣的设计。

Markus Kayser在伦敦设计节上的太阳能烧结矿 5 / 10
如果您在伦敦参加伦敦设计节，我们强烈建议您前往皇家艺术学院的SUSTAIN展览，一睹这一屡获殊荣的设计。

输入图片说明

这些玻璃物体非常耐用。

Markus Kayser在伦敦设计节上的太阳能烧结矿 6 / 10
这些玻璃物体非常耐用。

输入图片说明

在另一张桌子上，一个碗和一个盘子一起展示。

Markus Kayser在伦敦设计节上的太阳能烧结矿 7 / 10
在另一张桌子上，一个碗和一个盘子一起展示。

输入图片说明

该设备目前正在Kayser的母校皇家艺术学院展出，作为2011年伦敦设计节的一部分。

Markus Kayser在伦敦设计节上的太阳能烧结矿 8 / 10
该设备目前正在Kayser的母校皇家艺术学院展出，作为2011年伦敦设计节的一部分。

输入图片说明

该项目仅依靠阳光和沙子，使其具有令人难以置信的设计优势。

Markus Kayser在伦敦设计节上的太阳能烧结矿 9 / 10
该项目仅依靠阳光和沙子，使其具有令人难以置信的设计优势。

输入图片说明

这些自然资源不会很快被耗尽！

Markus Kayser在伦敦设计节上的太阳能烧结矿 10 / 10
这些自然资源不会很快被耗尽！

SUSTAIN：伦敦设计节上皇家艺术学院的7个令人兴奋的绿色设计
输入图片说明

我们对2011年伦敦设计节的报道即将结束（可悲的是 - 这太有趣了！），但我们绝对计划轰轰烈烈地出去。这很容易通过Sustain来实现，这是伦敦的几个杰出设计的展览。

Sustain at London Design Festival 2011 1 of 7
我们对2011年伦敦设计节的报道即将结束（可悲的是 - 这太有趣了！），但我们绝对计划轰轰烈烈地出去。这很容易通过Sustain来实现，这是伦敦皇家艺术学院的几个杰出设计的展览。虽然我们热衷于展示从材料到时髦的绿色汽车的所有内容，但我们特别兴奋地偶然发现了几个以前涵盖的项目，包括令人惊叹的Solar Sinter 3D打印机（见上图）。跳过跳跃，直接从皇家艺术学院获得我们独家设计节图片！

# kayserworks.com

### Solar Sinter

2011 Markus Kayser, Royal College of Art, London | Egypt | Morocco

In a world increasingly concerned with questions of energy production and raw material shortages, this project explores the potential of desert manufacturing, where energy and material occur in abundance.

In this experiment sunlight and sand are used as raw energy and material to produce glass objects using a 3D printing process, that combines natural energy and material with high-tech production technology.

Solar-sintering aims to raise questions about the future of manufacturing and triggers dreams of the full utilisation of the production potential of the world’s most efficient energy resource - the sun. Whilst not providing definitive answers, this experiment aims to provide a point of departure for fresh thinking.

### 太阳烧结矿

2011 马库斯·凯泽，皇家艺术学院，伦敦 |埃及 |摩洛哥

在一个越来越关注能源生产和原材料短缺问题的世界中，该项目探索了沙漠制造的潜力，其中能源和材料大量存在。

在这个实验中，太阳光和沙子被用作原材料和材料，使用3D打印工艺生产玻璃制品，将自然能源和材料与高科技生产技术相结合。

太阳能烧结旨在提出有关制造业未来的问题，并引发充分利用世界上最高效能源 - 太阳能的生产潜力的梦想。虽然没有提供明确的答案，但这个实验旨在为新思维提供一个出发点。

![输入图片说明](https://foruda.gitee.com/images/1681578410149569487/0fb96149_5631341.png "屏幕截图")![输入图片说明](https://foruda.gitee.com/images/1681578437209473212/08da9f08_5631341.jpeg "MarkusKayser_SolarSinter_05.jpg")![输入图片说明](https://foruda.gitee.com/images/1681578448050302674/dc442e2d_5631341.png "屏幕截图")![输入图片说明](https://foruda.gitee.com/images/1681578453404398589/5c9720f2_5631341.png "屏幕截图")![输入图片说明](https://foruda.gitee.com/images/1681578457059674844/84b71497_5631341.png "屏幕截图")![输入图片说明](https://foruda.gitee.com/images/1681578462281168428/6e4dd974_5631341.png "屏幕截图")![输入图片说明](https://foruda.gitee.com/images/1681578468034258361/f197145c_5631341.png "屏幕截图")![输入图片说明](https://foruda.gitee.com/images/1681578473093431252/5f618276_5631341.png "屏幕截图")![输入图片说明](https://foruda.gitee.com/images/1681578477683929345/c0531012_5631341.png "屏幕截图")![输入图片说明](https://foruda.gitee.com/images/1681578483372667604/bf6bb28a_5631341.png "屏幕截图")![输入图片说明](https://foruda.gitee.com/images/1681578489589022976/cead31f0_5631341.png "屏幕截图")![输入图片说明](https://foruda.gitee.com/images/1681578505333658119/a65a7312_5631341.png "屏幕截图")![输入图片说明](https://foruda.gitee.com/images/1681578508706735150/973eff19_5631341.png "屏幕截图")![输入图片说明](https://foruda.gitee.com/images/1681578516428161408/3f3472f6_5631341.png "屏幕截图")![输入图片说明](https://foruda.gitee.com/images/1681578521504810545/85e4f275_5631341.png "屏幕截图")![输入图片说明](https://foruda.gitee.com/images/1681578525852593808/575e16e5_5631341.png "屏幕截图")![输入图片说明](https://foruda.gitee.com/images/1681578530456855071/ec4eef19_5631341.png "屏幕截图")

---

> 最后要感谢下，线索提供者未来大陆，由于本次学习笔记并未引用他的文章，仅列出转载邀约的目录，以示感谢。 :pray:

【笔记】[Solar3D-printer](https://gitee.com/yuandj/siger/issues/I6W0TQ)

<p><img width="19%" src="https://foruda.gitee.com/images/1681573373548838102/1628d9e0_5631341.jpeg" title="435235182_0000_图层 4.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1681573382321655710/caabc538_5631341.jpeg" title="435235182_0001_图层 3.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1681573390824531974/b1d55b0e_5631341.jpeg" title="435235182_0002_图层 2.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1681573454831911200/6fcd3762_5631341.jpeg" title="435235182_0003_图层 1.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1681573398611732480/eeb3224d_5631341.jpeg" title="435235182_0004_背景.jpg"></p>

利用阳光和沙子作为原始能源和材料，通过3D打印工艺生产玻璃制品，将自然能源和材料与高科技生产技术相集合

Solar 3D-printer
- OLTAIC PANEL
- FOCAL POINT
- SUN TRACKER
- FRESNEL LENS
- BATTERY
- ELECTRONIC

![输入图片说明](https://foruda.gitee.com/images/1681614218904050931/bf2281d1_5631341.png "屏幕截图")

- [全球首枚3D打印火箭今天实现了惊人的发射，但未能入轨 未知大陆-数字制造云平台](https://www.unlands.com/m/news/item.aspx?id=9067)
- [关于用于3D打印的ABS塑料，您需要知道的一切 未知大陆-数字制造云平台](https://www.unlands.com/m/news/item.aspx?id=9080)
- [有史以来第一款3D打印火箭将于当地3月22日再次尝试发射](http://mp.weixin.qq.com/s?__biz=MjM5MTkwOTU4Ng==&mid=2650572236&idx=1&sn=d5e6203712a7571e908ffc895fed9ee9)
- [一分钟教你玩转未知大陆3D打印在线下单](http://mp.weixin.qq.com/s?__biz=MjM5MTkwOTU4Ng==&mid=2650572109&idx=1&sn=4c6be0d9eefe17e659f89079a3b3fd4f)
- [短暂点火后，Relativity Space再次取消3D打印火箭的发射](http://mp.weixin.qq.com/s?__biz=MjM5MTkwOTU4Ng==&mid=2650572079&idx=1&sn=1a1af21c2ef30825118e2426859b8378)
- [Relativity Space推迟全球首枚3D打印火箭的发射尝试](http://mp.weixin.qq.com/s?__biz=MjM5MTkwOTU4Ng==&mid=2650572023&idx=1&sn=571c252e4dcf799bb010d948398e909e)
- [世界首枚3D打印火箭，今日发射！](http://mp.weixin.qq.com/s?__biz=MjM5MTkwOTU4Ng==&mid=2650572006&idx=1&sn=9dcdc1aca501285ad5e8d69207abeb5d)
- [巴斯夫PP粉末系列3D打印助力打造新一代废水过滤装置](http://mp.weixin.qq.com/s?__biz=MjM5MTkwOTU4Ng==&mid=2650571961&idx=2&sn=622fab429864ecaf69d4e01b6e26480f)