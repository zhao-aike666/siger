- [月壶尊缔造者，《华中科技大学学报（自然科学版）》将推出“地外建造”专题](月壶尊华中科技大地外专刊征稿.md) （截止时间为2023年10月31日）
- [2023年第一届天文科普作品征集活动通知](https://www.nao.cas.cn/ncb/gz/202305/t20230525_6762965.html) （截止时间为2023年7月31日）

<table>
<tr>
<td width="36.69%">
<p align="center">
<a target="_blank" href="https://gitee.com/gu-shuting/siger/issues/I6K386">
<img width="95%" src="https://foruda.gitee.com/images/1678011710146807466/c692bea2_5631341.jpeg" title="行星际探索如何开始.jpg"></a></p>
</td>
<td width="36.69%">
<p align="center">
<a target="_blank" href="第一站，月球.md">
<img width="95%" src="https://foruda.gitee.com/images/1685447167416960829/5a88a604_5631341.jpeg" title="第一站，月球"></a></p>
</td>
<td width="18.39%">
<p align="center">
<a target="_blank" href="planets%20exploration.md">
<img width="66%" src="https://foruda.gitee.com/images/1678128321237086614/82510d04_5631341.jpeg" title="planets exploration.jpg"></a><br>
<a target="_blank" href="月球资源利用相关研究.md">
<img width="66%" src="https://foruda.gitee.com/images/1678826561141087346/bec1b0a2_5631341.jpeg" title="月球资源利用相关研究.jpg"></a><br>
<a target="_blank" href="科学通报58.md#深空探测将回答哪些科学问题">
<img width="66%" src="https://foruda.gitee.com/images/1678122662988415018/558aab78_5631341.jpeg" title="深空探测将回答哪些科学问题.jpg"></a>
</p>
</td>
</tr>
</table>

2023.5.30 开启奔赴星辰大海的新征程！ 《[第一站，月球](第一站，月球.md)》

> 行星际探索如果从仰望星空开始，人类从未停止过。如果问如何开始？打开好奇心，就 **从仰望星空开始** 吧。  
>  @[米粒](https://gitee.com/gu-shuting)  这符合天文频道，也同样是科幻，也也人类成为多行星物种的开始。  
> 而对 SIGer 而言，我们已经有了起点，且看： sig/天文/[宇宙级浪漫.md](../天文/宇宙级浪漫.md) | sig/天文/[暗能量宇宙.md](../天文/暗能量宇宙.md)

2023.3.5 [行星际探索]()如何开始？[How to start the planets exploration?](https://gitee.com/gu-shuting/siger/issues/I6K386) It was [starting](https://gitee.com/yuandj/siger/issues/I6JRLU)...

> 而上面这行字，以及下面的9期专题，只源于平劲松老师推荐的最新一期《中国科学 物理学 力学 天文学》，让我重温了这篇《[SKA低频成像管线并行优化](https://www.sciengine.com/doi/10.1360/SSPMA-2022-0262)》并行计算的FPGA优化，这是 SCIENGINE 的标准推荐的结果。老师一如既往的支持 “投喂” 下，《行星际探索》的子刊雏形逐渐清晰在脑海中，乘兴分享了我的想法，并得到了老师的肯定：“日贡一卒，确定一个年度有限目标，逐步推进。” 就有了接下来的工作，正巧 3月5日前，《行星际探索》筹备的项目发出了，我喜欢开篇的深邃的星海... 让接下来的自由畅想有了行动的理由和动力，看这9期专题，沿着 笔记的思路，逐渐展开，清晰的直接成刊，未完待续的后续整理，对于《自然中国》，更多的是学习，如同《深空探索》《中国科学》，距离 2023 深化 SIGer 团队建设的目标，又迈出了坚实的一步 ...

  [<img title="573." width="23.69%" src="https://foruda.gitee.com/images/1678106693016077092/e9d0499a_5631341.jpeg">](青少年FAST观测方案征集.md) [<img title="575." width="23.69%" src="https://foruda.gitee.com/images/1678125860174142690/44b1ff0a_5631341.jpeg">](国家天文台——最大的科学传播资源.md) [<img title="577." width="23.69%" src="https://foruda.gitee.com/images/1678128859673798036/be413ae1_5631341.jpeg">](https://gitee.com/yuandj/siger/issues/I6JRLU#note_16630498_link) [<img title="578." width="23.69%" src="https://foruda.gitee.com/images/1678136800262484959/ad55916b_5631341.jpeg">](中国SKA区域中心数据处理超算.md) [<img title="579." width="23.69%" src="https://foruda.gitee.com/images/1678162407001526678/ef0edd41_5631341.jpeg">]() [<img title="580." width="23.69%" src="https://foruda.gitee.com/images/1678135393761978452/4e756f8d_5631341.jpeg">](https://gitee.com/yuandj/siger/issues/I6JRLU#note_16630026_link) [<img title="581." width="23.69%" src="https://foruda.gitee.com/images/1678136186698128426/3ac76596_5631341.jpeg">](http://www.naturechina.com) [<img title="627." width="23.69%" src="https://foruda.gitee.com/images/1679432214909563181/960bb730_5631341.jpeg">](https://gitee.com/yuandj/siger/issues/I6KTVE)

2023.3.23.24 [行星际探索]()起航！[月球我们来啦](月球我们来了.md)！

[<img title="636." width="9.99%" src="https://foruda.gitee.com/images/1680656639511972827/c96beb3f_5631341.jpeg">](https://gitee.com/yuandj/siger/issues/I6SC0A#note_17229172_link) [<img title="637." width="9.99%" src="https://foruda.gitee.com/images/1680672604409598691/1f338150_5631341.jpeg">](https://gitee.com/yuandj/siger/issues/I6SC0A#note_17285056_link) [<img title="638." width="9.99%" src="https://foruda.gitee.com/images/1680679993768673533/7e44b042_5631341.jpeg">](https://gitee.com/yuandj/siger/issues/I6SC0A#note_17286563_link) [<img title="639." width="9.99%" src="https://foruda.gitee.com/images/1680704254774969795/aa4589f2_5631341.jpeg">](https://gitee.com/yuandj/siger/issues/I6SC0A#note_17285101_link) [<img title="640." width="9.99%" src="https://foruda.gitee.com/images/1680710633026684238/7d8f93e2_5631341.jpeg">](https://gitee.com/yuandj/siger/issues/I6SC0A#note_17229194_link) [<img title="641." width="9.99%" src="https://foruda.gitee.com/images/1680713619276064803/fb0b3668_5631341.jpeg">](https://gitee.com/yuandj/siger/issues/I6SC0A#note_17229212_link) [<img title="642." width="9.99%" src="https://foruda.gitee.com/images/1680743870598766471/7ad1ab01_5631341.jpeg">](https://gitee.com/yuandj/siger/issues/I6SC0A#note_17229213_link) [<img title="643." width="9.99%" src="https://foruda.gitee.com/images/1680745747449670484/feea5963_5631341.jpeg">](https://gitee.com/yuandj/siger/issues/I6SC0A#note_17229215_link) [<img title="644." width="9.99%" src="https://foruda.gitee.com/images/1680750670775515888/44cc3b89_5631341.jpeg">](https://gitee.com/yuandj/siger/issues/I6SC0A#note_17229219_link) [<img title="645." width="9.99%" src="https://foruda.gitee.com/images/1680752872620502546/14ed150e_5631341.jpeg">](https://gitee.com/yuandj/siger/issues/I6SC0A#note_17297784_link) [<img title="646." width="9.99%" src="https://foruda.gitee.com/images/1680754166709013308/07d2761b_5631341.jpeg">](https://gitee.com/yuandj/siger/issues/I6SC0A#note_17229224_link) [<img title="647." width="9.99%" src="https://foruda.gitee.com/images/1680773243411111437/dd6dfbf3_5631341.jpeg">](https://gitee.com/yuandj/siger/issues/I6SC0A#note_17229228_link) [<img title="648." width="9.99%" src="https://foruda.gitee.com/images/1680772034362881953/a7a64dd9_5631341.jpeg">](https://gitee.com/yuandj/siger/issues/I6SC0A#note_17229232_link) [<img title="649." width="9.99%" src="https://foruda.gitee.com/images/1680774485082319033/81f756d8_5631341.jpeg">](https://gitee.com/yuandj/siger/issues/I6SC0A#note_17310855_link) [<img title="650." width="9.99%" src="https://foruda.gitee.com/images/1680776895377841065/543233fe_5631341.jpeg">](https://gitee.com/yuandj/siger/issues/I6SC0A#note_17229594_link) [<img title="651." width="9.99%" src="https://foruda.gitee.com/images/1680784608381398182/d1744105_5631341.jpeg">](https://gitee.com/yuandj/siger/issues/I6SC0A#note_17229666_link) [<img title="652." width="9.99%" src="https://foruda.gitee.com/images/1680787574224524829/3b1902db_5631341.jpeg">](https://gitee.com/yuandj/siger/issues/I6SC0A#note_17229725_link) [<img title="653." width="9.99%" src="https://foruda.gitee.com/images/1680797908090582374/b98af559_5631341.jpeg">](https://gitee.com/yuandj/siger/issues/I6SC0A#note_17317827_link) 

[videos](https://space.bilibili.com/293861366/video)...

[<img width="23.69%" src="https://foruda.gitee.com/images/1680800105503423763/8504cc0c_5631341.png" title="CleanSpace One: A giant Pac-Man to gobble up space debris">](https://www.bilibili.com/video/BV1TV4y1X7Jq/) [<img width="23.69%" src="https://foruda.gitee.com/images/1680800229307263182/9f0b3e41_5631341.png" title="A Visual Journey: NASA’s Exploration Mission-1">](https://www.bilibili.com/video/BV1Cs4y1U749/) [<img width="23.69%" src="https://foruda.gitee.com/images/1680800267266513903/c0b01083_5631341.png" title="LCROSS Media Animations">](https://www.bilibili.com/video/BV1BT411s7uM/) [<img width="23.69%" src="https://foruda.gitee.com/images/1680800293570100942/14f0cad2_5631341.png" title="LCROSS Media Preview">](https://www.bilibili.com/video/BV11j411w7hD/) [<img width="23.69%" src="https://foruda.gitee.com/images/1680800321060519380/d42c8e69_5631341.png" title="LCROSS Simulated Impact">](https://www.bilibili.com/video/BV1Es4y1S7cc/) [<img width="23.69%" src="https://foruda.gitee.com/images/1680800349920079930/a6fe2774_5631341.png" title="Lunar Surface Innovation CONSORTIUM">](https://www.bilibili.com/video/BV1Zm4y1q7Mj/) [<img width="23.69%" src="https://foruda.gitee.com/images/1680800384918102783/b54ea542_5631341.png" title="欧洲太空探索愿景（EVSE）视频演示">](https://www.bilibili.com/video/BV1us4y1E7EA/) [<img width="23.69%" src="https://foruda.gitee.com/images/1680800433241555168/bd7be4ba_5631341.png" title="ATHLETE Rover Busts a Move: A Dancing Robot">](https://www.bilibili.com/video/BV1km4y1B7g2/)