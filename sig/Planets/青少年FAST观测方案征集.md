- [征集活动｜中国天眼开放1%的观测时间给青少年](https://mp.weixin.qq.com/s?__biz=MzUzNzA4MTIwNA==&mid=2247496545&idx=2&sn=3f874b0d637500f79a4eeecedf8828fe)
- [关于开展2022年“科创筑梦”青少年FAST 观测方案征集活动的通知](https://mp.weixin.qq.com/s?__biz=MzUzNzA4MTIwNA==&mid=2247496256&idx=1&sn=ebdf10ccc8e16d7962f82c1436678d53)
- [青少年FAST观测方案征集指南-中学版](https://mp.weixin.qq.com/s?__biz=MzUzNzA4MTIwNA==&mid=2247496256&idx=2&sn=1a32567a8a32a556ea289933045d16ab)
- [青少年FAST观测方案征集指南-小学版](https://mp.weixin.qq.com/s?__biz=MzUzNzA4MTIwNA==&mid=2247496256&idx=3&sn=63ba2e7f5128222fbb59da105cd4cdb4)

<p><img width="706px" src="https://foruda.gitee.com/images/1678106693016077092/e9d0499a_5631341.jpeg" title="573青少年FAST观测副本.jpg"></p>

> 当我看到 FAST 观测方案征集面向中小学时，我激动了，这可是科普工作者梦寐以求的，不只是青少年，所有人都同样期待。而看到内文底图《来自FAST的一封信》，火炬在暗夜里画出的 FAST 字样是如此耀眼，这可是SIGer 天文初建时就立下的 FLAG 啊。今天感觉就要实现了一样。这篇专题可以作为 国家天文台 的科普教育资源的一个范例，可以说，大科学装置是为全人类建造的仰望星海的利器。是最好的科普工具，没有之一。

# [征集活动｜中国天眼开放1%的观测时间给青少年](https://mp.weixin.qq.com/s?__biz=MzUzNzA4MTIwNA==&mid=2247496545&idx=2&sn=3f874b0d637500f79a4eeecedf8828fe)

原创 国家天文台 中国科学院国家天文台 2022-06-01 16:41 发表于北京

[![输入图片说明](https://foruda.gitee.com/images/1678101012124243915/70950010_5631341.png "屏幕截图")](http://v.qq.com/txp/iframe/player.html?origin=https%3A%2F%2Fmp.weixin.qq.com&containerId=js_tx_video_container_0.75076462122099&vid=d3340qbk0em&width=677&height=380.8125&autoplay=false&allowFullScreen=true&chid=17&full=true&show1080p=false&isDebugIframe=false)

> 央视新闻在儿童节报道  
> “科创筑梦”青少年FAST观测方案征集活动

在这儿要祝所有的小朋友们节日快乐，那么，这里有一条，对于热爱科学的，你们的一条好消息。首先要来关注的是中国天眼对孩子们开放的消息，科创筑梦青少年fast观测方案征集活动，正在进行当中，中国天眼将1%的时间开放给了青少年，开展科学观测科创筑梦青少年fast观测方案征集活动，由中国科协青少年科技中心、中国科学院国家天文台、中国青少年科技。辅导员协会联合组织，将面向全国中小学生开放，1%的观测时间约为50小时左右。小孩子们对这个天文，对宇宙都很有兴趣，特别是一讲到faster，小孩子都会问，Fast能不能收到外星人的信号，Fast能不能看黑洞，Fast能不能看到宇宙大爆炸的火球等等，这些问题，我们在想，那小孩子，就是没有任何的疏忽。

那小孩子的想法会什么样子？他们是不是有一些想法是我们想不到的，我们能把他们的想法变成我们可以观测的方案。青少年fast观测方案征集将于今年六月底截止，天文学家将通过评审，分小学、初中、高中三个组别，遴选出优秀方案，选出50位想法非常好，可能会进入观测的这个同学们。到fast现场来，我们组织一个暑期班，让同学们看看大国重庆，看看天文学家怎么工作，然后真好，真正的天文学家在一起。把自己的方案变成可以观测的方案，然后在这50位同学中，我们再优选一次，使得他的方案真正能够最终进入fast实时观测。据介绍，中国天眼开放1%的观测时间，给青少年实现天文梦想是一个全新的尝试，旨在激发青少年的好奇心和探索精神，带动支持一批科技辅导员、科技工作者和中小学联合开展天文科普，

并探索国家重大科学装置支持科普教育的创新模式。最终的想法可能是一篇论文，可能是一个报道，可能是给大家一个启迪经验，做一个尝试。如果这些都很成功，我们积累了很好的经验，就将对全世界的小朋友们开放，使得全世界的中小学生都能使用发起的实现他们的观测梦想。

<p><img width="23.69%" src="https://foruda.gitee.com/images/1678103898652052027/b562561c_5631341.png"> <img width="23.69%" src="https://foruda.gitee.com/images/1678103708215336090/27c30dc1_5631341.png"> <img width="23.69%" src="https://foruda.gitee.com/images/1678103707352552546/77794975_5631341.png"> <img width="23.69%" src="https://foruda.gitee.com/images/1678103695306102919/27643da0_5631341.png"> <img width="23.69%" src="https://foruda.gitee.com/images/1678103692112291858/63eef658_5631341.png"> <img width="23.69%" src="https://foruda.gitee.com/images/1678103687195909027/3fc02e20_5631341.png"> <img width="23.69%" src="https://foruda.gitee.com/images/1678103677465818264/d7400344_5631341.png"> <img width="23.69%" src="https://foruda.gitee.com/images/1678103673013748311/59c27b36_5631341.png"> <img width="23.69%" src="https://foruda.gitee.com/images/1678103665064814719/060accb1_5631341.png"> <img width="23.69%" src="https://foruda.gitee.com/images/1678103659514663479/fc0470f0_5631341.png"> <img width="23.69%" src="https://foruda.gitee.com/images/1678103656525173911/3f7ff589_5631341.png"> <img width="23.69%" src="https://foruda.gitee.com/images/1678103647955423766/55e4bf2b_5631341.png"></p>

![输入图片说明](https://foruda.gitee.com/images/1678101056886011477/3747c4d2_5631341.png "屏幕截图")

> 活动海报

### 相关阅读：

- [关于开展2022年“科创筑梦”青少年FAST 观测方案征集活动的通知](http://mp.weixin.qq.com/s?__biz=MzUzNzA4MTIwNA==&mid=2247496256&idx=1&sn=ebdf10ccc8e16d7962f82c1436678d53)
- [青少年FAST观测方案征集指南-中学版](http://mp.weixin.qq.com/s?__biz=MzUzNzA4MTIwNA==&mid=2247496256&idx=2&sn=1a32567a8a32a556ea289933045d16ab)
- [青少年FAST观测方案征集指南-小学版](http://mp.weixin.qq.com/s?__biz=MzUzNzA4MTIwNA==&mid=2247496256&idx=3&sn=63ba2e7f5128222fbb59da105cd4cdb4)
- [FAST新发现一大批暗弱脉冲星](http://mp.weixin.qq.com/s?__biz=MzUzNzA4MTIwNA==&mid=2247493218&idx=1&sn=0b553551de8fdf8287e498196a079eeb)
- [FAST捕获3例新的高色散快速射电暴](http://mp.weixin.qq.com/s?__biz=MzUzNzA4MTIwNA==&mid=2247492376&idx=1&sn=02ae0478fd64e11e746664de1afdec3f)
- [观天者说 | 四龄童勇担重任，FAST大显身手](http://mp.weixin.qq.com/s?__biz=MzUzNzA4MTIwNA==&mid=2247491655&idx=1&sn=895e82701bcb321f474c71af322ae662)
- [观天者说 | 无线电揭示宇宙广大，FAST探索参数空间](http://mp.weixin.qq.com/s?__biz=MzUzNzA4MTIwNA==&mid=2247491658&idx=1&sn=03bcfb9844dd871600e809e0704e2074)
- [FAST助力中国科学家“明察秋毫”](http://mp.weixin.qq.com/s?__biz=MzUzNzA4MTIwNA==&mid=2247491294&idx=1&sn=86d3d8ce07580da693562a62893e6f37)
- [FAST天眼能看什么？](http://mp.weixin.qq.com/s?__biz=MzUzNzA4MTIwNA==&mid=2247488290&idx=1&sn=f5932ed10d6eb3a94fb887298a9bcea7)

### 中国天眼FAST官网： https://fast.bao.ac.cn

这里介绍了FAST的参数、科学成果、科学目标。

![输入图片说明](https://foruda.gitee.com/images/1678101200999276265/e31f2ed6_5631341.png "屏幕截图")

> 查询FAST的科研成


![输入图片说明](https://foruda.gitee.com/images/1678101212256405889/f74555ee_5631341.png "屏幕截图")

> 查询FAST的科学目标

### 中国天眼FAST相关视频：

![输入图片说明](https://foruda.gitee.com/images/1678101308501601438/13e232e8_5631341.png "屏幕截图")

> 逐渐睁开的中国天眼-上

![输入图片说明](https://foruda.gitee.com/images/1678101376910270757/fc4a7ec9_5631341.png "屏幕截图")

> 逐渐睁开的中国天眼-下

![输入图片说明](https://foruda.gitee.com/images/1678101411918990761/8cad34a9_5631341.png "屏幕截图")

> 当机器人遇见中国天眼

- 国家天文台信息化与科学传播中心
- 责编：袁凤芳
- 审阅：田斌

 **《中国国家天文》2022年5月刊** 

> 由于疫情物流原因，五月刊推迟寄送~  
> ——请多见谅——  
> 欢迎点击【阅读原文】订购  
> 国际刊号：ISSN 1673-6672  
> 国内刊号：CN11-5468/P  
> 邮发代号：80-602

![输入图片说明](https://foruda.gitee.com/images/1678101482479808009/1f428b81_5631341.jpeg "640.jpg")

小提示：想第一时间获取中国科学院国家天文台文章的小伙伴注意啦！进入“中国科学院国家天文台”微信公众号→点击右上角···→点击设置星标就可以啦，精彩不容错过~

![输入图片说明](https://foruda.gitee.com/images/1678101497075020421/0d43ca99_5631341.png "屏幕截图")


# [关于开展2022年“科创筑梦”青少年FAST 观测方案征集活动的通知](https://mp.weixin.qq.com/s?__biz=MzUzNzA4MTIwNA==&mid=2247496256&idx=1&sn=ebdf10ccc8e16d7962f82c1436678d53)

中国科学院国家天文台 2022-05-07 16:36 发表于北京

各省、自治区、直辖市科协青少年科技教育活动部门单位,新疆生产建设兵团科协科普部，各地天文相关机构，中国青辅协各理事单位：

为深入学习贯彻科学家座谈会上的重要讲话，充分发挥国家大科学装置在培养青少年科技创新后备人才方面的重要作用，服务科技工作者开展青少年科普，激发和培养广大青少年的科学兴趣和创新精神，中国科协青少年科技中心、中国科学院国家天文台、中国青少年科技辅导员协会面向全国中小学生举办“科创筑梦”青少年FAST观测方案征集活动。现将有关事项通知如下。

### 一、组织机构

 **主办单位：** 

- 中国科协青少年科技中心
- 中国科学院国家天文台
- 中国青少年科技辅导员协会

 **媒体支持：** 

- 《中国国家天文》
- 《中国科技教育》
- 《课堂内外》

### 二、活动内容

中国科学院国家天文台开放1%的FAST观测时间，提供给全国中小学生。2022年6月30日前全国中小学学生均可登录“科创筑梦”全国青少年科技创新服务云平台（网址: https://www.cyscc.org）提交观测方案参与活动。活动官网提供活动指南和天文科普资源。

天文专家将按照小学、初中、高中学段遴选优秀观测创意方案，有关结果将于7月底在活动官网公示。优秀方案作者及指导教师将获得主办单位颁发的证书。国家天文台专家将对部分优秀方案进一步指导完善，与方案作者组成联合研究团队，根据方案共同完善的情况开展科学观测。

积极组织学生参与并有入选优秀方案的学校将由主办单位评定为“全国天文科普教育特色校”。积极动员科技工作者、支持帮扶农村地区开展活动的科协组织、青少年辅导员协会、天文相关机构由各省科协青少年科技教育活动部门推荐参评优秀组织单位。

### 三、方案要求

（一）方案须从科学目标、技术方案设想、预期结果和参考资料四个方面进行撰写。小学生不少于300字，中学生不少于500字，可配图以进行论证说明。方案以PDF格式提交，大小在50M以内。

（二）方案限1名学生完成，须确认是本人原创设想，不侵犯他人知识产权或其他合法利益。

（三）1名学生可提交多个观测方案。方案最多可有1名指导教师。

### 四、组织实施要求

（一）强化对科技工作者和青少年的“双引领双服务双获得”。各单位要组织天文科技专家走进中小学，为学生做科普报告，参与天文兴趣小组辅导活动。积极推动天文场馆、天文观测台站、科研机构等科普设施和科技资源提高开发服务质量，为中小学提供专门的活动。各省青少年辅导员协会要积极与专业学会、科研机构合作，共同做好科技教师培训。 

（二）加强宣传，动员学校积极参与。各单位要做好活动在各地中小学的宣传工作，特别是广泛组织“‘科创筑梦’助力‘双减’科普行动”试点学校、科技创新教育特色校、“英才计划”参与中学、天文特色校等积极参加，把活动作为“双进”促“双减”、培养科技创新后备人才的具体措施。

（三）拓展活动范围，普惠广大青少年。围绕活动内容既要重点支持有兴趣特长的学生开展深入的学习研究，也要组织丰富多样的普及活动，扩大青少年受益面。要帮助边远农村和少数民族地区学校与教育资源丰富地区学校结对交流，促进活动普惠普及。

中国科协青少年科技中心   

中国科学院国家天文台

中国青少年科技辅导员协会

2022年4月19日

联系人：刘天扬  王晓萌  

010-68512519

![输入图片说明](https://foruda.gitee.com/images/1678100804087366354/1f6ac339_5631341.png "屏幕截图")


# [青少年FAST观测方案征集指南-中学版](https://mp.weixin.qq.com/s?__biz=MzUzNzA4MTIwNA==&mid=2247496256&idx=2&sn=1a32567a8a32a556ea289933045d16ab)

国家天文台 中国科学院国家天文台 2022-05-07 16:36 发表于北京

![输入图片说明](https://foruda.gitee.com/images/1678100273740499587/2ce1500e_5631341.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1678100276625415484/e60aaf97_5631341.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1678100282132903907/79fffb15_5631341.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1678100289616307836/24e48bb7_5631341.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1678100294703400923/cf0face1_5631341.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1678100300723932565/043d67ff_5631341.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1678100308418937125/c5207cc0_5631341.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1678100316874689148/e98b9b58_5631341.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1678100321031495559/340cca64_5631341.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1678100328180246799/6a8b1f1b_5631341.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1678100331861807453/9f82ca95_5631341.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1678100352468473486/f61127b2_5631341.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1678100360159200326/1c74db3f_5631341.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1678100372139092252/c96c1743_5631341.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1678100382022003507/ccfe1b73_5631341.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1678100389082574190/b8a7b2ff_5631341.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1678100401577356623/6a7db273_5631341.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1678100409584732866/d4e57b3a_5631341.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1678100410621666753/7d462124_5631341.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1678100423701608886/6cc67594_5631341.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1678100428187541492/7d81ab59_5631341.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1678100430660203363/b1e1309c_5631341.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1678100446129883683/2bc02f0b_5631341.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1678100451216022184/598a451a_5631341.png "屏幕截图")



# [青少年FAST观测方案征集指南-小学版](https://mp.weixin.qq.com/s?__biz=MzUzNzA4MTIwNA==&mid=2247496256&idx=3&sn=63ba2e7f5128222fbb59da105cd4cdb4)

国家天文台 中国科学院国家天文台 2022-05-07 16:36 发表于北京

#天文科普活动 99 #天文大科学装置：中国天眼 47

![输入图片说明](https://foruda.gitee.com/images/1678099942989950985/577560b6_5631341.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1678099945765293219/894b0c7a_5631341.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1678099953735491234/a5deee5d_5631341.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1678099974378913698/549028bc_5631341.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1678099970513935447/968ed044_5631341.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1678099979217000367/a575f52b_5631341.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1678099987705238284/4891d253_5631341.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1678099999906489261/bb8f0862_5631341.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1678100000046450798/c958974a_5631341.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1678100003513061460/8979c5ca_5631341.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1678100010260212671/a9bfbcee_5631341.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1678100020971468581/765cda32_5631341.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1678100029173373433/d493023d_5631341.png "屏幕截图")
![](https://foruda.gitee.com/images/1678100036149417122/dc9396a3_5631341.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1678100041205527081/84514e22_5631341.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1678100043375944835/b9ed3c0f_5631341.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1678100065707488717/06456588_5631341.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1678100083785730780/2cfb7091_5631341.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1678100077198001252/f25ed394_5631341.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1678100083948644513/b69b5ed9_5631341.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1678100093902758828/9a096656_5631341.png "屏幕截图")
![](https://foruda.gitee.com/images/1678100095985584774/37f1e452_5631341.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1678100102332459186/71b429e9_5631341.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1678100117664431015/c6069d5a_5631341.png "屏幕截图")

<p align="right">国家天文台信息化与科学传播中心</p>

《中国国家天文》2022年4月刊

> #现货#太阳咳一咳，地球抖三抖  
> 欢迎点击【阅读原文】订购  
> 国际刊号：ISSN 1673-6672  
> 国内刊号：CN11-5468/P  
> 邮发代号：80-602

![输入图片说明](https://foruda.gitee.com/images/1678100167262467439/36844af7_5631341.png "屏幕截图")

小提示：想第一时间获取中国科学院国家天文台文章的小伙伴注意啦！进入“中国科学院国家天文台”微信公众号→点击右上角···→点击设置星标就可以啦，精彩不容错过~

![输入图片说明](https://foruda.gitee.com/images/1678100173943480813/bc47891c_5631341.png "屏幕截图")

#天文科普活动 99