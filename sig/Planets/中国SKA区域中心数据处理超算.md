- 上海天文台“建成世界上首台SKA区域中心原型机”成果荣获 ...
  http://www.shao.cas.cn/2020Ver/xwdt/tpxw/202006/t20200624_5610973.html
  > 2020年6月18日 · 上海天文台“建成世界上首台SKA区域中心原型机”成果荣获2019年度十大天文科技进展. 为促进我国天文学科的进步和发展，鼓励和表彰取得突出成果的天文科研和 …

- 中国SKA区域中心超算原型机系统实测结果公布 实现天文 ...
  https://www.huawei.com/cn/news/2019/11/taishan-edge-computing-ska
  > 2019年11月29日 · 2019年11月29日. [中国 上海，2019年11月29日] 今日，在SKA上海大会暨第六届SKA工程大会上，华为携手中国科学院上海天文台（以下简称“上海天文台”）打造 …

- [SKA区域中心数据处理系统和科学应用专题](https://www.sciengine.com/SSPMA/issue/53/2) 《[中国科学 物理学 力学 天文学](https://www.sciengine.com/SSPMA/home)》2023, 第53卷, 第2期
  https://www.sciengine.com/SSPMA/issue/53/2
  > 《中国科学 物理学 力学 天文学》是中国科学院和国家自然科学基金委员会共同主办、《[中国科学](https://www.sciengine.com/)》杂志社出版的学术刊物. 力求及时报道物理学、力学和天文学基础研究与应用研究方面等方面具有创新性和高水平的最新研究成果, 月刊. （主编：谢心澄）

  - [SKA低频成像管线并行优化](https://www.sciengine.com/doi/10.1360/SSPMA-2022-0262)
  - [地月空间探测器星间测距自主定轨](https://www.sciengine.com/doi/10.1360/SSPMA-2022-0176)

<p><img width="706px" src="https://foruda.gitee.com/images/1678136800262484959/ad55916b_5631341.jpeg" title="578SKAhuawei副本.jpg"></p>

> 在一篇《中国科学 物理学 力学 天文学》的搜索推荐中《[SKA低频成像管线并行优化](https://www.sciengine.com/doi/10.1360/SSPMA-2022-0262)》引起了我的注意，这是一个标准的算法优化方案，我也第一时间分享给了平劲松老师，他建议我和上海天文台取得联系，带着这个疑问，我做了这期专题，SKA这个大科学装置浮现在我的面前，他仍然是一个射电望远镜系统，被称为最后一代，足见它的先进性，而其海量并发的数据量，需要超算这样的大杀气来搞定，HPC 的 sig 组是 华为 OE 社区的一个新 SIG 数据处理能力是它的标配之一，尽管它和 SKA 的关联难免多了几层，但对于我而言，反而更近了。没成想，这一通功课下来，竟然斩获了这么些学习笔记。大部分内容还是需要之后同学们继续研读。这里抛砖引玉，连封面都是 SKAo.int 的背景天河计算机。傲娇了我...

# [上海天文台“建成世界上首台SKA区域中心原型机”成果荣获](http://www.shao.cas.cn/2020Ver/xwdt/tpxw/202006/t20200624_5610973.html) ...
  
发布时间：2020-06-18  |  【 大  中  小 】  |  【打印】 【关闭】

为促进我国天文学科的进步和发展，鼓励和表彰取得突出成果的天文科研和技术人员，扩大天文学科在社会上的广泛影响，中国天文学会与中国科学院天文大科学研究中心在全国范围内评选 2019 年度十大天文科技进展，评选活动由中国科学院天文大科学研究中心综合管理部具体组织。目前，此次评选活动已圆满结束。经评审专家网络投票，共评选出了10个获奖成果（具体信息可访问十大天文科技进展网页：http://159.226.88.6/top10/）。 

![输入图片说明](https://foruda.gitee.com/images/1678167064204876200/c2277bfe_5631341.png "屏幕截图")

上海天文台申报一项成果为“建成世界上首台SKA区域中心原型机”，该成果荣获了2019年度十大天文科技进展。以下是获奖成果简介： 

平方公里阵列 (简称SKA) 射电望远镜是天文学家将要建造的最大科学装置，已被列入国家科技创新“十三五”规划。SKA建成后，其数据的深度分析和科学活动将围绕几个分布全球的SKA区域中心开展，区域中心直接关系到最终的科学成果，中国正开展相关筹建工作。 

中国团队在2019年11月SKA工程大会上发布了中国SKA区域中心原型机，并展示了利用该原型机做出的多项实验成果，国内外媒体做了集中报道。SKA总部专门组织了对该原型机的国际专家咨询会，SKA区域中心指导委员会主席Peter Quinn和SKA国际组织总干事Philip Diamond牵头撰写的咨询报告高度评价了中国在原型机建设方面取得的成果，指出这是国际上首台SKA区域中心原型机（“this excellent, and standard setting, efforts surrounding the first international proto-SRC”），为推进SKA的进程做出了实质性贡献。中国SKA科学计算团队在平台架构、存储、网络等方面先行先试，做出了包含多项集成创新的有益尝试，为国际上其他区域中心提供了借鉴和参考，在即将筹建的SKA区域中心全球网络中发挥了引领性作用。 

SKA区域中心不仅仅是数据中心，它还是培养中国SKA科研人才、服务全球科学用户的综合性研究平台。天文学、计算机、信息通信等多学科的专家队伍密切合作，加快了国内科学数据处理队伍的成长。SKA总干事Diamond教授和华为轮值主席胡厚崑先生在华为全连接大会的主题演讲中高度赞扬了上海天文台与华为公司合作在SKA数据处理和原型机建设方面的深度合作，是产学研结合的典型案例，将工业界的先进技术快速应用到天文研究，并现场演示了全球最快的AI集群对SKA先导望远镜数据开展图像搜索的应用案例，10秒钟完成了过去几十天才能完成的天体搜索工作，展示了人工智能在天文领域中的应用潜力。澳大利亚、美国和中国的团队还联合利用世界上最快的超算SUMMIT完成了最大规模的SKA数据处理流程，创造了天文领域科学计算的巅峰应用。 

依托中国SKA区域中心原型机，中澳加团队共同发起了ERIDANUS项目，启动亚太SKA区域中心的概念性研究，不仅为全球SKA区域中心提供了“中国模式”，也为SKA先导望远镜的科学运行注入强劲动力。 

### 相关链接：  

1. An Tao, Wu Xiang-Ping, Hong Xiaoyu, SKA data take centre stage in China, Nature Astronomy, 3, 1030, 2019 

2. 2019年11月12日，新华社：“我国科学家成功研制平方公里阵列射电望远镜（SKA）区域中心原型机” 

3. 2019年11月11日，China Daily：Chinese scientists completes first SKA regional center prototype 

4. 上海天文台与华为公司联合发布SKA人工智能科学应用成果，人民日报的报道：华为发布最快AI训练集群：20万星体中10秒找出目标 

5. Ruonan Wang, Andreas Wicenec*, Tao An*, SKA shakes hands with Summit. Science Bulletin, 65, 337-339, 2020

# [中国SKA区域中心超算原型机系统实测结果公布 实现天文](https://www.huawei.com/cn/news/2019/11/taishan-edge-computing-ska) ...
  
2019年11月29日
  
[中国 上海，2019年11月29日] 今日，在SKA上海大会暨第六届SKA工程大会上，华为携手中国科学院上海天文台（以下简称“上海天文台”）打造的中国SKA区域中心超算原型机系统实测结果公布，在对SKA先导望远镜MWA产生的海量连续谱数据成像方面相较于传统平台性能提升2.23倍。

本次大会由国家科技部和SKA组织主办，国家遥感中心(SKA中国办公室)与上海天文台共同承办，吸引了来自世界各地19个国家及地区数百名工程师、科学家和行业代表，首次探讨世界最大射电望远镜的建设和运行，研讨主题涉及采购、调试和运行以及有关科学议题的讨论。

![输入图片说明](https://foruda.gitee.com/images/1678167204551178389/eb51e60f_5631341.png "屏幕截图")

> 测试结果发布现场

在大会现场演讲环节，来自上海天文台的安涛研究员公布了超算原型机系统测试结果，表示基于华为自研鲲鹏处理器的TaiShan服务器在内存带宽、多核、低能耗、开放生态等方面有较明显的特色。其中，特别在以MWA GLEAM成像流程为样例的测试中，TaiShan服务器的测试性能较传统计算平台性能提升了2.23倍；在众多SKA的高吞吐量类型的应用场景中，兼容ARM架构的服务器将有广阔的应用前景。

SKA项目是天文领域最大的国际合作科学项目之一，其观测设施将分布在南半球、两个大洲，为人类详尽认识宇宙提供了重大机遇。预估，SKA项目将产生10倍于目前全球互联网的数据流量，面对EP级的计算和EB存储需求，超算架构面临可扩展性、经济成本可承受、稳定性和对先进算法的灵活适应性等方面的挑战。华为提供了包括TaiShan HPC和Atlas 900 AI集群在内的解决方案用于SKA的天文数据分析。其中，基于TaiShan HPC解决方案制造的原型机采用基于鲲鹏处理器的TaiShan 200服务器为主计算节点和管理节点，并可兼容基于昇腾AI处理器的Atlas异构节点、x86节点混合部署共同处理宇宙再电离、谱线，连续谱成像、脉冲星搜寻、星系分类、暂现源搜寻等工作，数据岛内实现独立共享存储与任务调度，针对多样性业务采用多样性平台，数据岛间通过高速网络互连，灵活重组资源，实现高度灵活性与稳定性，联合SKA社区自研的数据存储管理系统和执行框架等软件，共同应对海量天文数据的处理。

![输入图片说明](https://foruda.gitee.com/images/1678167215212130157/6fd64f64_5631341.png "屏幕截图")

> 华为TaiShan服务器

同时，得益于AI在天文领域的加速应用，SKA也将同时采用基于Atlas 900的云服务用于射电望远镜的数据分析，将原来耗时169天的特征星体检索任务缩短到10秒级别。Atlas 900代表了当今全球AI计算的领先水平，它由数千颗华为自研昇腾910 AI处理器构成，总算力达到256P～1024P FLOPS @FP16，相当于50万台PC的计算能力。

中国是SKA天文台的重要创始成员国，在SKA工程技术和科学研究等方面做出了卓越贡献，目前上海天文台安涛研究员作为SKA区域中心中方代表参与全球SKA区域中心总体设计、并完成了世界上首台SKA区域中心原型机。华为与上海天文台基于各自领域优势资源共享和交流，结合双方在射电天文、大数据、云计算、高性能计算以及人工智能领域的优势，共同推进中国SKA区域中心建设，将其逐步发展成为世界一流的SKA主要学术中心和技术研发中心。

# [SKA区域中心数据处理系统和科学应用专题](https://www.sciengine.com/SSPMA/issue/53/2) 

编者按 | 天体物理学 |  免费获取 专题集

### [SKA区域中心数据处理系统和科学应用专题·编者按](https://www.sciengine.com/doi/10.1360/SSPMA-2022-0449)

- 安涛
- 2023年, 第53卷, 第2期, 229501
- https://doi.org/10.1360/SSPMA-2022-0449

[下载 PDF](https://www.sciengine.com/doi/pdf/D319CF87FC4D4A108D2097192E41FA04) | 在线阅读 |  收藏 |  引用出口 | 文章推荐

论 文 |  免费获取 专题集

### [中国SKA区域中心跨洲际高速数据传输进展及展望](https://www.sciengine.com/doi/10.1360/SSPMA-2022-0260)

- 郭绍光, 安涛, 徐志骏, 劳保强, 陈肖, 陆扬, 吕唯佳, 伍筱聪
- 2023年, 第53卷, 第2期, 229502
- https://doi.org/10.1360/SSPMA-2022-0260

[下载 PDF](https://www.sciengine.com/doi/pdf/7DB4E312002C490A9C75DCC9002FC137) | 在线阅读 |  收藏 |  引用出口 | 文章推荐

 **摘要** 

> 平方公里阵列(Square Kilometre Array, SKA)作为最大的射电望远镜,其观测产生的数据将首先由澳大利亚和南非两个台址传输到几百公里以外的科学数据处理中心,然后通过跨洲际高速互联网分发到上万公里距离的全球各个SKA区域中心(SKA Regional Centre, SRC).在SKA第一阶段,每年预计有710 PB的数据需要通过至少100 Gbps的网络分发到各个SRC,如此高的网络带宽和数据规模对数据的传输与分发带来极大挑战.本文通过对TCP/UDP/HTTP等多种网络协议的分析,使用当前射电天文领域不同的软件测试研究了当前10 Gbps网络基础设施下的最佳传输方案参数,讨论了影响高速传输的因素, 提出了相应的性能优化策略.这将为中国在SKA第一阶段正式观测前的网络建设和布局提供技术基础, 也是未来SKA区域中心全球联网运行的技术积累.本文所描述的技术细节和方法对依赖大数据量跨国际节点交互的相关科学应用至关重要.

 **科大讯飞翻译** 

平方公里阵列射电望远镜(SKA)已于2021年7月开工, 预计2029年完成第一期建设, 是中国参与的第二个国际大科学工程. SKA是一个具有划时代意义的大科学装置, 它将在探测宇宙形成和演化规律的深度、广域、全貌和全时域等方面达到前所未有的高度, 给射电天文学领域带来巨大的变革和机遇. 中国作为主要成员参加SKA, 既是对中国天文学未来发展的长远规划, 也是践行人类命运共同体理念, 为探索宇宙奥秘和推动自然科学进步履行大国的责任. SKA的第一阶段(总规模的10%)完成后, 每年将产生接近1 EB的科学数据, 这些海量数据的深度处理、科学分析和长期存储, 将由各成员建设的SKA区域中心组成的分布式协同网络共同完成. 建设国际一流水平的中国SKA区域中心, 不仅是作为创始成员的国际贡献, 而且是中国参与SKA获得科学回报的重要保障.

SKA未来能否取得预期的科学成果, 很大程度上取决于SKA区域中心的能力和水平. 地理上分布的SKA区域中心为本国或本地区的科学家获取SKA数据, 开展科学研究提供了最大便利. 在SKA建成之前, SKA天文台已开始筹建SKA区域中心原型系统, 开展关键技术验证、科学测试、国际联测等工作, 为几年后的全面建设做准备. 中国团队于2019年底率先建成首个SKA区域中心原型机, 得到国际同行的高度关注. 本专题详细介绍了中国SKA区域中心的研发进展、整体性能和科学软件系统, 展示了为应对SKA跨洲高速数据传输的挑战而开展的技术研发和数据模拟, 并以脉冲星搜索、连续谱成像和快速射电暴三个科学实例, 介绍了根据具体数据和处理方法的特点在软硬件定制设计方面所做的努力和有益探索.

本专题的作者均为天文学和高性能计算领域专业科技人员, 亲身参与了中国SKA区域中心原型机的建设、测试、升级和科学应用, 也是国际SKA区域中心工作组的核心成员, 深度参与国际合作, 熟悉相关前沿科学和技术.

编者希望通过本专题的出版, 帮助读者详细了解中国SKA区域中心原型系统的整体性能和工作能力, 吸引科研人员积极参与SKA数据处理方法研究、分布式科学计算流程的研发和优化、人工智能在天文学的拓展应用等, 激发天文学家的科学灵感, 利用SKA探路者望远镜的数据开展关键科学项目的预先研究, 为SKA建成后取得第一批科学发现做好准备.

最后感谢所有参与审稿的专家的宝贵意见, 以及专题中各位作者的辛勤付出.

The Square Kilometre Array Radio Telescope (SKA), which started construction in July 2021 and is expected to complete the first phase of construction in 2029, is the second major international scientific project in which China has participated. SKA is an epoch-making scientific device, which will reach an unprecedented height in exploring the depth, wide area, panorama and full time domain of the formation and evolution of the universe, and bring great changes and opportunities to the field of radio astronomy. China's participation in SKA as a major member is not only a long-term plan for the future development of Chinese astronomy, but also a practice of the concept of a community with a shared future for mankind, fulfilling the responsibility of a major country to explore the mysteries of the universe and promote the progress of natural science. After the completion of the first phase of SKA (10% of the total scale), nearly 1 EB of scientific data will be generated every year. The deep processing, scientific analysis and long-term storage of these massive data will be completed by the distributed collaborative network composed of SKA regional centers built by various members. Building a world-class China SKA regional center is not only an international contribution as a founding member, but also an important guarantee for China's participation in SKA to obtain scientific returns.

Whether the SKA can achieve the expected scientific results in the future depends largely on the ability and level of the SKA regional center. Geographically distributed SKA regional centers provide the greatest convenience for scientists in their own countries or regions to obtain SKA data and carry out scientific research. Before the completion of SKA, the SKA Observatory has begun to prepare for the construction of the prototype system of SKA Regional Center, to carry out key technical verification, scientific testing, international joint testing and other work, and to prepare for the full construction in a few years. The Chinese team took the lead in building the first SKA regional center prototype at the end of 2019, which attracted great attention from international counterparts. This special topic introduces the research and development progress, overall performance and scientific software system of China SKA Regional Center in detail, and demonstrates the technical research and development and data simulation carried out to meet the challenges of SKA transcontinental high-speed data transmission.Pulsar search, continuum imaging and fast radio bursts are taken as three scientific examples to introduce the efforts and beneficial explorations made in the customized design of software and hardware according to the characteristics of specific data and processing methods.

The authors of this topic are all professional scientists and technicians in the field of astronomy and high-performance computing. They have personally participated in the construction, testing, upgrading and scientific application of the prototype of China SKA Regional Center. They are also core members of the International SKA Regional Center Working Group. They are deeply involved in international cooperation and are familiar with relevant frontier science and technology.

Through the publication of this topic, the editor hopes to help readers understand the overall performance and working ability of the prototype system of China SKA Regional Center in detail, and attract researchers to actively participate in the research of SKA data processing methods, the development and optimization of distributed scientific computing processes, and the application of artificial intelligence in astronomy.Inspire astronomers to carry out pre-research on key scientific projects using data from the SKA Pathfinder Telescope to prepare for the first scientific discoveries after the completion of the SKA.

Finally, I would like to thank all the experts who participated in the review for their valuable opinions, as well as the authors in the special topic for their hard work.

论 文 | 天体物理学 |  免费获取 专题集

### [SKA低频成像管线并行优化](https://www.sciengine.com/doi/10.1360/SSPMA-2022-0262)

- 韦建文, 张晨飞, 劳保强, 林新华, 安涛
- 2023年, 第53卷, 第2期, 229503
- https://doi.org/10.1360/SSPMA-2022-0262

[下载 PDF](https://www.sciengine.com/doi/pdf/882770AD72E84783A92B8847F061B78D) | 在线阅读 |  收藏  |  引用出口 | 文章推荐 

 **摘要** 

> 平方公里阵列(Square Kilometre Array, SKA)射电望远镜的数据处理是通过管线方式进行的, 管线的执行效率是SKA区域中心考虑的重要因素. 连续谱成像观测是SKA的主要观测模式之一, 也是许多科学工作的基础. 本文以SKA低频先导设备(Murchison Widefield Array, MWA)的成像管线为例, 在中国SKA区域中心原型机(China SKA Regional Centre Prototype, CSRC-P)上进行并行处理管线优化. 以往的优化方案都集中在少数性能热点, 缺乏对整体管线的系统优化, 导致整体加速比相对较低. 针对这一问题, 本文提出一种全局优化方案, 针对管线使用多种编程语言和图像数据可独立处理的特点, 综合使用C+多线程、Python多进程和Shell多任务并行等优化方法, 并验证了优化结果的准确性. 实验表明, 优化后的代码在CSRC-P的x86节点和ARM (Advanced RISC Machine)节点上分别获得了2.7和2.4倍加速, 运行时间分别从7479和9666 s, 降低为2759和4061 s. ARM计算节点展现出对SKA应用良好的适应性. 本文的优化策略和方法也适用于其他SKA科学应用, 对SKA先导望远镜的科学运行和未来的运行也有帮助.

论 文 | 天体物理学 |  免费获取 专题集

### [面向SKA1时代的科学数据流及阵列模拟分析](https://www.sciengine.com/doi/10.1360/SSPMA-2022-0261)

- 郭绍光, 陆扬, 安涛, 劳保强, 徐志骏, 伍筱聪, 吕唯佳
- 2023年, 第53卷, 第2期, 229504
- https://doi.org/10.1360/SSPMA-2022-0261

[下载 PDF](https://www.sciengine.com/doi/pdf/7371DE9A826343E7AFA4EEC9832197B9) | 在线阅读 |  收藏 |  引用出口  | 文章推荐

 **摘要** 

> 作为下一代射电望远镜, 平方公里阵列望远镜(Square Kilometre Array, SKA)经过多年的筹备,第一阶段(SKA Phase 1, SKA1)已经在2021年7月开工建设,SKA1正式运行后预计每年将产生710 PB的科学归档数据,这些数据将存储在世界各地的SKA区域中心供科研工作者使用.本文将textcolorblack

论 文 |  免费获取 专题集

### [一个面向原始数据搜寻的快速射电暴数据集](https://www.sciengine.com/doi/10.1360/SSPMA-2022-0258)

- 徐志骏, 安涛, 郭绍光, 劳保强, 吕唯佳, 伍筱聪
- 2023年, 第53卷, 第2期, 229505
- https://doi.org/10.1360/SSPMA-2022-0258

[下载 PDF](https://www.sciengine.com/doi/pdf/63CFA5EA71774660870142382B3E53F9) | 在线阅读 |  收藏 |  引用出口 | 文章推荐

 **摘要** 

> 快速射电暴是目前国际天文学新兴前沿热点, 随着海量观测数据带来的处理和分析的挑战, 亟需开展快速射电暴信号智能搜寻和甄别的研究. 为了加速快速射电暴搜寻研究, 开发了一套基于机器学习的快速射电暴数据集, 它可以训练机器学习算法以搜寻原始数据中的快速射电暴. 目前数据集有8020个快速射电暴、4010个背景噪声和4010个射频干扰仿真图像, 这些图像是根据开放的快速射电暴观测结果构建的, 并可根据需要扩展数量. 本研究旨在为最先进的人工智能算法提供开源数据集, 以测试和比较快速射电暴识别算法. 该数据集为卷积神经网络(Convolutional Neural Network, CNN)和经典机器学习算法提供图像和numpy格式的文件. 数据集可以实现快速射电暴和非快速射电暴分类, 或快速射电暴、射频干扰和背景噪声分类. 在本例中, 使用有预训练模型的31种经典CNN算法. 在快速射电暴/非快速射电暴分类中, 第一个历元训练中达到90%以上的准确率, 在真实数据测试中达到99.8%的最大准确率.

论 文 |  免费获取 专题集

### [射电脉冲星搜索的优化方法](https://www.sciengine.com/doi/10.1360/SSPMA-2022-0264)

- 韦建文, 张晨飞, 张仲莉, 余婷, 林新华, 安涛
- 2023年, 第53卷, 第2期, 229506
- https://doi.org/10.1360/SSPMA-2022-0264

[下载 PDF](https://www.sciengine.com/doi/pdf/552B6A0B6AA1493BBD18703907BA6148) | 在线阅读 |   收藏 |  引用出口 | 文章推荐

 **摘要** 

> 天文学计算通常具有数据量大、计算量多的特点. 平方公里阵列射电望远镜(Square Kilometre Array, SKA)等装置的建设和高性能计算平台的发展促进了天文学与高性能计算之间的联系. 脉冲星搜索是SKA的主要科学方向之一. 本文介绍了一种基于OpenMP多线程和Multiprocessing多进程技术加速脉冲星搜索管线的方案, 提出了一种解决负载不平衡问题的方法, 并成功地将优化后的管线安装于中国SKA区域中心原型机的x86和ARM计算节点. 在默奇森大视场阵列望远镜(Murchison Widefield Array, MWA)的3个脉冲星搜索样例上的测试显示, 与原始单核心管线相比, 优化后的管线在x86和ARM节点上分别获得10.4–12.2倍和24.5–25.8倍的加速, 并行效率达到37.1%–43.6%和24.5%–26.9%. 并且ARM节点比x86节点的计算快1.1–1.3倍, 显示出国产计算设备在SKA数据处理方面的巨大潜力. 这项应用优化成果, 近期将用于加速MWA南天快速两米巡天(Southern-sky MWA Rapid Two-metre, SMART)项目的脉冲星搜寻工作.

进 展 | 天体物理学 |  免费获取 专题集

### [中国SKA区域中心原型系统——软件平台](https://www.sciengine.com/doi/10.1360/SSPMA-2022-0257)

- 劳保强, 张迎康, 安涛, 徐志骏, 郭绍光, 伍筱聪, 吕唯佳
- 2023年, 第53卷, 第2期, 229507
- https://doi.org/10.1360/SSPMA-2022-0257

[下载 PDF](https://www.sciengine.com/doi/pdf/B8FDC2B5A73D4E549BE2316CC6E28883) | 在线阅读 |  收藏 |  引用出口 | 文章推荐

 **摘要** 

> 平方公里阵列(Square Kilometre Array, SKA)射电望远镜将在多个科学方向取得革命性的突破, 而SKA软件系统是影响科学产品的关键因素之一. SKA区域中心是天文学家进行SKA数据分析、科学研究和学术交流的平台. 处理SKA科学数据的软件环境需要具备通用性、灵活性和高适应性. 中国科学家已经建成了中国SKA区域中心原型机, 部署了被大型超级计算机广泛使用的作业调度系统, 并安装了能够处理当前主流射电望远镜观测数据的天文软件, 还部署了多个科学数据处理管线, 以方便不同科学方向的观测数据的自动化并行处理. 本文介绍了中国SKA区域中心原型机的软件平台和处理SKA先导望远镜数据的管线, 包括低频连续谱成像管线、谱线成像管线以及甚长基线干涉测量数据处理管线. 国内外用户已经基于该平台成功开展了SKA相关科学研究. 该平台的建设和运行为未来全面建设中国SKA区域中心提供了宝贵的实践经验.

论文 | 量子物理 |  免费获取

### [基于两光子偏振-空间模超纠缠态的高容量的量子秘密共享协议](https://www.sciengine.com/doi/10.1360/SSPMA-2022-0198)

- 刘锋, 杨宇光
- 2023年, 第53卷, 第2期, 220311
- https://doi.org/10.1360/SSPMA-2022-0198

[下载 PDF](https://www.sciengine.com/doi/pdf/438F411D0ACD428180AFBF764E40B5C1) | 在线阅读 |  收藏 |  引用出口 | 文章推荐

 **摘要** 

> 基于两光子偏振-空间模超纠缠态, 本文提出了一种可行的高容量三方量子秘密共享协议. 秘密所有者Alice制备偏振-空间模超纠缠光子对并分别分发给两个代理人Bob和Charlie. 两个代理人利用空间模纠缠辅助的非局域完全Bell态分析技术, 通过协作可以明确区分Alice的偏振Bell态. 仅需线性光学元件来构造代理人的测量设备, 这使得该协议在实验实现时更可行. 与现有基于两光子纠缠的量子秘密共享协议相比, 本方案不需要双向量子通信, 且由于一个偏振-空间模超纠缠光子对可传输两个比特的信息, 因此具有更高的容量. 纠错和秘密放大技术保证了所提出的量子秘密共享协议是安全和可靠的.

论文 | 流体力学 |  免费获取

### [超临界CO2渗流的流-固-热多场耦合机理研究](https://www.sciengine.com/doi/10.1360/SSPMA-2022-0226)

- 白冰, 陈勉, 金衍
- 2023年, 第53卷, 第2期, 224711
- https://doi.org/10.1360/SSPMA-2022-0226

[下载 PDF](https://www.sciengine.com/doi/pdf/FCB04513C2194C0AA8512D170239C1C0) | 在线阅读 |  收藏 |  引用出口 | 文章推荐

 **摘要** 

> 页岩地层遇水易发生黏土矿物膨胀、分散等水化现象, 导致常规水基钻井液、压裂液作用下易发生井壁失稳或压裂增产效果差等难题. 超临界CO2可以有效避免黏土矿物水化和地层物性损伤, 基于此, 学者们开展了利用超临界CO2开发页岩油气资源的基础研究. 超临界CO2的物性随温度、压力变化较大, 描述水基流体渗流过程中的多物理场耦合模型不适用于描述超临界CO2渗流. 为建立适用于描述超临界CO2开发油气资源过程中的多物理场耦合特征模型, 本文在考虑CO2的运移特性和热力学特性的基础上, 结合超临界CO2对页岩力学特性影响规律, 构建了超临界CO2渗流条件下的流-固-热多场耦合模型, 基于有限元数值计算方法研究了超临界CO2渗流条件下的地层温度、孔隙压力和地层应力的时空分布规律, 同时与水基钻井液渗流条件下的多物理场进行了对比分析. 计算结果表明: 在相同时间和井周方位的情况下, 相比于水渗流, 超临界CO2渗流条件下的地层温度变化更大, 孔隙压力更低, 最小水平地应力方向的井周应力差更大, 最大水平地应力方向的周向应力更小. 该研究可为开展超临界CO2钻井的井壁稳定性分析、超临界CO2压裂设计和评价提供理论基础.

论文 | 天体测量学 |  免费获取

### [主小行星带对火星轨道长期动力学影响评估](https://www.sciengine.com/doi/10.1360/SSPMA-2022-0119)

- 刘山洪, 吴正楷, 曹建峰, 鄢建国, 李勰
- 2023年, 第53卷, 第2期, 229511
- https://doi.org/10.1360/SSPMA-2022-0119

[下载 PDF](https://www.sciengine.com/doi/pdf/A777804E601B499A8D85C4CC1CA7EC7D) | 在线阅读 |  收藏 |  引用出口 | 文章推荐

 **摘要** 

> “天问一号”任务的成功实施标志着我国正迈向月球以外更远的深空, 面临着更为复杂的空间动力学环境, 探测难度将不断增大. 小行星带毗邻火星, 不完全考虑其产生的摄动会严重影响火星轨道动力学参数解算精度, 进而间接影响太阳系历表构建和高精度火星探测任务实施. 为研究主带小行星动力学影响, 本文先分析了主带小行星的分布特性, 建立了太阳系动力学积分环境, 结果表明: 除火星外的内太阳系行星轨道积分10年与DE421的差异小于15 m, 火星轨道积分差异约百米量级. 基于点质量模型、单环模型和最新六环模型估算了其对火星轨道的影响, 积分10年误差分别约为2 km, 31.3 m, 2 m. 最后讨论了环模型与太阳系行星轨道参数共同解算情况, 证实了环模型参数修正后火星距离观测量残差理论上可进一步降低至亚米级, 同时发现仅利用距离观测量行星初始轨道根数在Z轴方向上解算误差较大.

论文 | 天体物理学 |  免费获取

### [基于分形布朗运动的斯隆数字巡天中类星体光变特性](https://www.sciengine.com/doi/10.1360/SSPMA-2022-0101)

- 唐洁
- 2023年, 第53卷, 第2期, 229512
- https://doi.org/10.1360/SSPMA-2022-0101

[下载 PDF](https://www.sciengine.com/doi/pdf/57BB9D9EC4E04AB090F1137956488410) | 在线阅读 |  收藏  |  引用出口 | 文章推荐

 **摘要** 

> 在类星体多个波段都可以观测到剧烈的光变现象, 研究类星体光变有助于更深入地了解其内在的物理机制. 本文从斯隆数字巡天(Sloan Digital Sky Survey, SDSS)项目中选取具有光变的类星体作为分析样本, 采用重标极差方法对SDSS stripe 82天区中的类星体光变的分形特性进行分析. 类星体光变的Hurst指数结果表明: 类星体光变具有自相似性和长程相关性, 存在长期的记忆性和相关性. 类星体光变服从有偏的随机游走模型, 其变化类似于分形布朗运动. 通过Hurst指数转换得到的功率谱指数可知, 大部分类星体的光变曲线可以近似用阻尼随机游走模型来拟合, 但小部分样本存在较大偏差.

论文 | 天体测量学 |  免费获取

### [地月空间探测器星间测距自主定轨](https://www.sciengine.com/doi/10.1360/SSPMA-2022-0176)

- 黄勇, 杨鹏, 陈艳玲, 李培佳, 周善石, 唐成盼, 胡小工
- 2023年, 第53卷, 第2期, 229513
- https://doi.org/10.1360/SSPMA-2022-0176

[下载 PDF](https://www.sciengine.com/doi/pdf/44A799E9E34E48229343764EEA1E7250) | 在线阅读 |  收藏  |  引用出口 | 文章推荐

 **摘要** 

> 地月空间导航、定位和授时均离不开探测器的精密轨道信息, 目前在地月空间主流探测器精密定轨仍依赖地基测量, 包括地基测距、测速和干涉测量等技术. 星间测距技术已经在北斗等卫星导航系统中得到充分应用, 测量精度高且易于实现. 由于引力对称性, 地球附近卫星仅利用星间测距不能实现自主定轨, 而地月空间存在众多引力非对称性区域, 可以实现基于星间测距技术的自主高精度定轨. 本文推导了相对论框架下的地月空间探测器星间双单向测距数据处理算法; 针对地月空间平动点和DRO等区域典型轨道探测器, 利用仿真数据分析了基于星间测距的地月空间探测器定轨精度. 分析结果表明, 引力非对称性强弱和轨道动力学约束是影响地月空间探测器定轨精度的重要因素. 仿真分析结果还表明, 1 m精度的探测器星间测距可以实现优于10 m的地月平动点和DRO探测器定轨精度. 引入地球MEO卫星后一方面可以提高定轨精度, 另一方面也有利于平动点探测器的快速轨道恢复.

 **地址** :
1. 中国科学院上海天文台, 上海 200030;
2. 中国科学院大学天文与空间科学学院, 北京 100049;
3. 中国科学院上海天文台, 上海市空间导航与定位技术重点实验室, 上海 200030;
4. 中国科学院上海天文台, 中国科学院行星科学重点实验室, 上海 200030

 * 联系人, E-mail: yongh@shao.ac.cn

收稿时间: 2022年05月08日   接受时间: 2022年06月24日   发布时间: 2023年01月05日 

 **1 引言** 

地月空间是近地轨道空间的自然延伸, 包括空间对抗、在轨服务与维护、空间科学与应用、支持近地轨道以远载人探索等应用. 在地月空间探索的各个阶段, 高精度的导航、定位和授时(PNT)具有重要应用意义.

我国探月工程从2004年正式实施以来, 已经快速发展了近20年, 圆满完成了月球探测“绕-落-回”三部曲工程目标. 自2007年发射“嫦娥一号”(CE-1)探测器, 至今成功发射了CE-1, CE-2 (2010年), CE-3 (2013年), CE-5T1 (2014年), CE-4 (2018年)和CE-5 (2020年)月球探测器, 实现了对月球的环绕、着陆与采样返回探测[1–6].

2018年1月, 国际空间探索协调组(ISECG)发布了第三版全球空间探索路线图(Global Exploration Roadmap, GER), 呈现出“近地轨道-地月空间-火星”的深空探测发展态势. 月球探测作为深空探测的前沿, 正成为主要深空探测国家的首要探测目标. 美国、俄罗斯、日本、欧空局等国家和组织2025年前将实施多次月球探测任务, 并把在月面建立大型科研设施、开展长期月球探测作为远期目标[7–9].

月球探测任务中的精密定轨和定位是一项基础性研究工作, 精密定轨定位不仅保证空间探测器的正常运行, 也是分析有效载荷数据的必要前提. 到目前为止, 无论是我国还是国外的月球探测器, 其测轨手段基本上还是依赖地基测量技术, 包括地基无线电测距测速和干涉测量, 利用地基测定轨系统获取高精度轨道需长时间连续跟踪, 尤其是地月之间的转移轨道段, 地基连续跟踪时间要求更长, 而且探测器距离地球越远地基测量的几何构型越差, 由此也限制了基于地基测量技术的月球探测器定轨精度和适用范围.

在探月工程的推动下, 我国逐步建成了用于月球探测器轨道测量的地基综合测量系统, 主要由深空站和VLBI台站构成, 采用测距测速+干涉测量的联合测定轨方案. 我国深空网有三个站: 佳木斯深空站(66 m)、喀什深空站(35 m)和南美萨帕拉深空站(35 m), 在之前的嫦娥系列任务中, 喀什(12/18 m)、青岛(12/18 m)、三亚(15 m)、圣地亚哥(12 m)以及纳米比亚(12 m)也参与过测轨任务. 该系统实现的测距(X频段)精度可优于0.3 m, 测速(X频段, 积分时间为1 s)精度可优于0.1 mm/s. 从CE-1到目前的CE-5和“天问一号”火星探测器任务, VLBI测轨技术处于不断进步中. VLBI测量的精度从CE-1的约10 ns水平提高到HX-1的优于0.2 ns水平[1,10,11]. 美国、欧洲深空探测器的ΔDOR测量精度最好可以优于0.1 ns[12].

未来的月球探索活动对月球探测器定轨精度、实时性和适用场景均提出了更高的要求. 目前我国已批复探月工程四期任务, 探月工程四期包括嫦娥六号、嫦娥七号和嫦娥八号三个探测器, 这三个任务将在未来十年之内陆续实施, 重点进行月球南极探测和月球科研站建设, 为更好地探索月球环境和资源打下坚实基础. 未来我国还将实施载人登月[9]. 从全球航天的发展趋势看, 当前正在大规模地转向地月空间及更远的深空探测. 未来相当长的一段时间内, 地月空间仍然是航天任务活动的主要目的地和前哨基地, 以此为基础可以探索更远的深空, 可以预见我国航天活动将进一步聚焦于地月空间, 主要集中在近地空间、月球引力空间和地月转移空间.

随着地月空间探测的开展, 测控手段将从目前以地基测量技术为主转向天基测量技术, 包括星间测距技术和星载GNSS漏信号技术, 这些技术在目前的地球卫星导航系统中已经得到了充分应用, 扩展到地月空间是发展趋势. 而且随着天基测量技术的成熟应用, 有利于实现自主实时导航. 天基导航系统, 可以有效降低地基测定轨系统对布站几何、设备性能和工作弧段的要求, 同时可以和地基系统互为备份、融合数据处理, 从而进一步提升导航的可靠性和精度[5].

美国全球导航卫星系统(GPS)从Block-IIR卫星开始增加星间链路, 无地面主控站支持情况下可自主维持180 d, 显著提高了GPS在战争及其他特殊环境中的可用性[13]. 我国北斗导航卫星系统BDS已在新一代试验卫星星座上成功实现了星间链路(ISL)相互伪距测量. 北斗新一代导航卫星具有Kα波段星间双向测距技术, 集中式自主定轨试验表明星间双向测距数据有助于提高定轨精度[14].

随着地月空间探索的发展, 构建地月空间导航系统也逐步提上日程, 近地卫星在各类卫星导航系统(GNSS, 包括GPS/GLONASS/GALILEO/BD等)支持下保障导航性能, 但是由于现有导航卫星的局限性, 目前尚不能对月球等深空探测器提供高精度导航支持, 单独利用GNSS技术应用于月球距离探测器存在信号弱、可见卫星数少、精度因子(DOP)差等缺点. 2007年, Hill[15]提出将导航星座布设到地月系共线平动点周期轨道上, 从而为地月空间探测器提供导航服务的设想. 近年来, 许多方案延伸到绕月远距离逆行轨道(DRO)和地月系三角平动点轨道, 国内多位学者也开展了针对性的研究, 包括基于月球平动点轨道的星座自主定轨研究[16]、平动点探测器与地球导航卫星联合自主定轨技术研究[17]、平动点与月球轨道探测器自主定轨研究[18]、地月三角平动点的探测器自主定轨[19], 王文彬[20]对基于DRO-LEO编队的地月探测器自主导航与授时系统进行了系统性的阐述.

本文重点分析星间测距技术在地月空间探测器自主定轨中的应用, 根据已有研究成果, 在地月空间中存在多个引力非对称性较强的区域, 比如平动点L1, L2, DRO轨道等, 理论上只要有一个探测器处于引力非对称性较强区域, 就可以实现基于星间测距的探测器自主定轨[15,20]. 通过星间测距建立地月空间通信导航系统时空基准, 并通过和北斗卫星的星间链路将地月时空基准溯源统一至北斗导航系统, 实现高精度时空基准建立、维持和传递. 由于地月空间和近地空间的差异性, 在进行星间测距数据处理时, 需要有针对性地进行处理, 本文推导了相对论框架下地月空间星间双向测距的处理算法, 并分析了典型场景下基于星间测距的地月空间探测器自主定轨精度.


 **2 地月空间典型区域星间测距自主定轨可行性** 

圆形限制性三体问题存在五个特解, 称为平动解[18]. 在旋转坐标系下, 地月平动点与地球和月球的位置如图1所示, L4和L5是三角平动点, 具有线性稳定性; L1, L2和L3是共线平动点, 具有不稳定性, 地月系平动点距离估算值见表1.

![输入图片说明](https://foruda.gitee.com/images/1678169228650005592/e260df50_5631341.png "屏幕截图")

> 图 1 (网络版彩图)地月平动点位置示意图

 **5 结论** 

由于引力对称性, 地球附近卫星仅利用星间测距不能实现自主定轨, 而地月空间存在众多引力非对称性区域, 可以实现基于星间测距技术的自主高精度定轨. 本文以平动点探测器、DRO探测器和MEO卫星构成的混合星座作为典型场景分析了基于星间测距的地月空间探测器自主定轨精度.

利用地月空间探测器之间的双向对发星间测距数据, 通过数据处理解耦测距和时间同步信息, 在目前数据处理精度下需要考虑相对论影响. 利用星间测距数据可以实现地月探测器间的高精度自主定轨结果, 1 m测距误差条件下平动点定轨位置精度最好优于10 m, 优于地基测距数据定轨精度.

利用星间测距自主定轨, 影响定轨精度的主要因素有: 引力非对称性影响、动力学约束、动力学模型误差和测量精度等. 引入MEO卫星后一方面可以提高定轨精度, 另一方面也有利于提高轨道快速恢复能力.

本文分析结果表明星间测距技术在地月空间导航系统构建中有着巨大的应用潜力, 既可以通过星间链路实现地月空间导航系统时空基准的高精度自主维持, 也可以通过导航卫星和用户之间的星间链路实现用户的高精度导航、定位和授时服务.

1. 引言
2. 地月空间典型区域星间测距自主定轨可行性
3. 基于星间测距的地月空间探测器自主定轨算法
4. 仿真分析
5. 结论

 **致谢** 

感谢北京航天飞行控制中心曹建峰博士的有益讨论; 感谢南京大学侯锡云教授提供了平动点和DRO探测器初值.

 **参考文献** 

1. Yan J G, Ping J S, Li F, et al. Chang’E-1 precision orbit determination and lunar gravity field solution. Adv Space Res, 2010, 46: 50 -57 (1)

   [CrossRef](https://doi.org/10.1016/j.asr.2010.03.002) [ADS](http://adsabs.harvard.edu/abs/2010AdSpR..46...50J) Google Scholar

2. Li P J, Hu X G, Huang Y, et al. Orbit determination for Chang’E-2 lunar probe and evaluation of lunar gravity models. Sci China-Phys Mech Astron, 2012, 55: 514 -522 (2)

   [CrossRef](https://doi.org/10.1007/s11433-011-4596-2) [ADS](http://adsabs.harvard.edu/abs/2012SCPMA..55..514L) Google Scholar

3. Huang Y, Chang S Q, Li P J, et al. Orbit determination of Chang’E-3 and positioning of the lander and the rover. Chin Sci Bull, 2014, 59: 3858 -3867 (3)

   [CrossRef](https://doi.org/10.1007/s11434-014-0542-9) [ADS](http://adsabs.harvard.edu/abs/2014ChSBu..59.3858H) Google Scholar

4. Li P J, Huang Y, Chang S Q, et al. Positioning for the Chang’E-3 lander and rover using Earth-based observations (in Chinese). Chin Sci Bull, 2014, 59: 3162 -3173 (4)

   [CrossRef](https://doi.org/10.1360/N972014-00207) Google Scholar

5. Fan M, Hu X G, Dong G, et al. Orbit improvement for Chang’E-5T lunar returning probe with GNSS technique. Adv Space Res, 2015, 56: 2473 -2482 (5)

   [CrossRef](https://doi.org/10.1016/j.asr.2015.09.010) [ADS](http://adsabs.harvard.edu/abs/2015AdSpR..56.2473F) Google Scholar

6. Li P J, Huang Y, Fan M, et al. Orbit determination for Chang’E-5 mission in rendezvous and docking (in Chinese). Sci Sin-Phys Mech Astron, 2021, 51: 119508 (6)

   [CrossRef](https://doi.org/10.1360/SSPMA-2021-0081) [ADS](http://adsabs.harvard.edu/abs/2021SSPMA..51k9508L) Google Scholar

7. Landgraf M. Pathways to sustainability in lunar exploration architectures. J Spacecraft Rockets, 2021, 58: 1681 -1693 (7)

   [CrossRef](https://doi.org/10.2514/1.A35019) [ADS](http://adsabs.harvard.edu/abs/2021JSpRo..58.1681L) Google Scholar

8. Kim S, Kim K J, Yi Y. Investigation on lunar landing candidate sites for a future lunar exploration mission. Int J Aeronaut Space Sci, 2022, 23: 221 -232 (8)

   [CrossRef](https://doi.org/10.1007/s42405-021-00433-4) [ADS](http://adsabs.harvard.edu/abs/2022IJASS..23..221K) Google Scholar