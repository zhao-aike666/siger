- [Overview | Planets – NASA Solar System Exploration](https://solarsystem.nasa.gov/planets/overview)

<p><img width="706px" src="https://foruda.gitee.com/images/1678128321237086614/82510d04_5631341.jpeg" 
 title="576NASAplanets.jpg"></p>

> 真是宝藏 @[明启](https://gitee.com/Lukas-Friedrich/) 啊！《[行星际探索启程篇](https://gitee.com/Lukas-Friedrich/siger/issues/I6L448)》，和之前我的 SNA 笔记的一篇完全重合，可以合体啦。它们都是讨论我们的探索深空的基础，行星所在的太阳系，然后是系外行星，所以基础篇就是解决我们在哪里？要到哪里去的问题？而出发前，一定要尽可能地了解未知，这就离不开我们的工具 “[望远镜](#1詹姆斯韦布空间望远镜)”。当然已经超越了可见光部分，全频谱探测。而探测设备就会受到[太阳辐射](#3太阳风)的干扰，这就有了太空气象站。（串联起来了！）。然后，人类准备登录行星之前，还要派遣机器人[火星车](#2火星探测)前往探究一番。

  1. 基础知识科普

     1. 行星际空间介绍
     2. 行星际物质介绍
     3. 太阳风
     4. 太阳黑子
     5. 太阳风暴

  2. 前沿事件跟踪

     1. 詹姆斯·韦布空间望远镜
     2. 火星探测

# [planets exploration](https://gitee.com/yuandj/siger/issues/I6JRLU#note_16630616_link)

- Overview | Planets – NASA Solar System Exploration
  https://solarsystem.nasa.gov/planets/overview
  > There are more planets than stars in our galaxy. The current count orbiting our star: eight.. The inner, rocky planets are Mercury, Venus, Earth, and Mars.NASA's newest rover — …

  - What is a Planet https://solarsystem.nasa.gov/planets/in-depth
    > What is a Planet? Introduction. This seemingly simple question doesn't have …
  - Mars https://solarsystem.nasa.gov/planets/mars/overview
    > Mars is one of the most explored bodies in our solar system, and it's the only planet …
  - Mercury https://solarsystem.nasa.gov/planets/mercury/overview
    > Mercury is the fastest planet in our solar system – traveling through space at …
  - Uranus https://solarsystem.nasa.gov/planets/uranus/overview
    > Uranus is the seventh planet from the Sun, and has the third-largest diameter in our …
  - Saturn https://solarsystem.nasa.gov/planets/saturn/overview/
    > Pop Culture. Pop Culture. Perhaps the most iconic of all the planets in our solar …
  - Earth https://solarsystem.nasa.gov/planets/earth/overview/
    > Exploration. Galleries. Overview. Our home planet is the third planet from the Sun, …
  - Jupiter https://solarsystem.nasa.gov/planets/jupiter/overview
    > Exploration. Galleries. Jupiter has a long history of surprising scientists – all the …
  - Neptune https://solarsystem.nasa.gov/planets/neptune/overview
    > Pop Culture. Pop Culture. Even though Neptune is the farthest planet from our …
  - Sun https://solarsystem.nasa.gov/solar-system/sun/overview
    > Overview. Our Sun is a 4.5 billion-year-old star – a hot glowing ball of hydrogen and …

  来自 solarsystem.nasa.gov 的其他内容

  - [Sun - Overview](https://solarsystem.nasa.gov/solar-system/sun/overview) | Planets – NASA Solar System Exploration
  - [NASA Science - Overview](https://science.nasa.gov/) | Planets – NASA Solar System Exploration
  - [Uranus Moons - Overview](https://solarsystem.nasa.gov/moons/uranus-moons/overview) | Planets – NASA Solar System Exploration

- Home – NASA Solar System Exploration
  https://solarsystem.nasa.gov
  > NASA’s real-time science encyclopedia of deep space exploration. Our scientists and far-ranging robots explore the wild frontiers of our solar system. NASA. Solar ... that there is …

  | <img width="999px" src="https://foruda.gitee.com/images/1677982119090249444/2819a8c6_5631341.png"> | <img width="999px" src="https://foruda.gitee.com/images/1677982126554736950/3df6bf0c_5631341.png"> | <img width="999px" src="https://foruda.gitee.com/images/1677982133506050596/4aa84f2c_5631341.png"> | <img width="999px" src="https://foruda.gitee.com/images/1677982141967049631/4629048f_5631341.png"> | <img width="999px" src="https://foruda.gitee.com/images/1677982150013935835/e061fc50_5631341.png"> |
  |---|---|---|---|---|
  | [Mars](https://solarsystem.nasa.gov/planets/mars/overview/) | [Sun](https://solarsystem.nasa.gov/solar-system/sun/overview/) | [Our Solar System](https://solarsystem.nasa.gov/solar-system/our-solar-system/overview/) | [Mercury](https://solarsystem.nasa.gov/planets/mercury/overview/) | [Earth](https://solarsystem.nasa.gov/planets/earth/overview/) |
  | nasa.gov | nasa.gov | nasa.gov | nasa.gov | nasa.gov |
  | <img width="999px" src="https://foruda.gitee.com/images/1677982158299550495/45e5c341_5631341.png"> | <img width="999px" src="https://foruda.gitee.com/images/1677982165975329446/668b0e99_5631341.png"> | <img width="999px" src="https://foruda.gitee.com/images/1677982240791514200/34251f78_5631341.png"> | <img width="999px" src="https://foruda.gitee.com/images/1677982181282551699/8caaf7ab_5631341.png"> | <img width="999px" src="https://foruda.gitee.com/images/1677982192801334030/01f76855_5631341.png"> |
  | [Jupiter](https://solarsystem.nasa.gov/planets/jupiter/overview/) | [Earth's Moon](https://solarsystem.nasa.gov/moons/earths-moon/overview/) | [Asteroids](https://solarsystem.nasa.gov/asteroids-comets-and-meteors/asteroids/overview/) | [Voyager 1's Pale Blue Dot NASA Solar System Exploration](https://solarsystem.nasa.gov/resources/536/voyager-1s-pale-blue-dot/) | [Meteors & Meteorites](https://solarsystem.nasa.gov/asteroids-comets-and-meteors/meteors-and-meteorites/overview/) |
  | nasa.gov | nasa.gov | nasa.gov | nasa.gov | nasa.gov |

- Exoplanet Exploration: Planets Beyond our Solar …  <img align="right" width="36%" src="https://foruda.gitee.com/images/1677982671546456221/c2e2b54c_5631341.png">
  https://exoplanets.nasa.gov
  > 2023年3月2日 · Exoplanet Exploration Program. NASA's science, technology and mission management office for the exploration of exoplanets. The program's primary goals, as described in the 2014 NASA …

- planets exploration- 必应在线翻译
  cn.bing.com/translator
  > 行星探索

- Exploration | Our Solar System – NASA Solar … <img align="right" width="36%" src="https://foruda.gitee.com/images/1677982794113380190/9b1fc6bb_5631341.png">
  https://solarsystem.nasa.gov/solar-system/our-solar-system/exploration
  > 2018年5月4日 · Explore in 3D—Eyes on the Solar System. Eyes on the Solar System lets you explore the planets, their moons, asteroids, comets and the spacecraft exploring them from 1950 to 2050. Ride with the …

- Exoplanet Catalog | Discovery – Exoplanet Exploration: …
  https://exoplanets.nasa.gov/discovery/exoplanet-catalog
  > 2023年3月2日 · NASA’s Exoplanet Exploration Program, the search for planets and life beyond our solar system. ... Click on a planet’s name to see 3D model of each planet and …

# 1 基础知识科普

### (1)行星际空间介绍：

从广义上讲行星际空间是指任何行星系所在宇宙空间。行星际空间由太阳风来定义，来自太阳连绵不绝的带电粒子创造了稀薄的大气圈(称为太阳圈)，深入太空中数十亿英里。太阳圈的距离和强度与太阳风活动的程度息息相关。在行星际空间中，银河系的环境开始影响到伴随着太阳磁场的粒子流量，并且超越太阳磁场成为主导。行星际空间包含太阳生成的磁场，也有行星生成的磁场，像是木星、土星和地球自身的磁场。它们的形状都受到太阳风的影响，而类似泪滴的形状，有着长长的磁尾伸展在行星的后方。这些磁场可以捕获来自太阳风和其它来源的粒子，创造出如同范艾伦带的磁性粒子带。没有磁场的行星，像是火星和水星，但是金星除外，它们的大气层都逐渐受到太阳风的侵蚀。

### (2)行星际物质介绍：

行星际物质是填充在太阳系的物质，太阳系内较大的天体，如行星，小行星和彗星都运行在其间。行星际空间虽然空空荡荡，但并非真空，其中分布着极稀薄的气体和极少量的尘埃。在地球轨道附近的行星际空间中，每立方厘米平均约含有五个正离子（绝大部分为质子）和五个电子。此外，还充斥着来自太阳、行星以及太阳系以外的电磁波。

### (3)太阳风：

是指从太阳上层大气射出的超声速等离子体带电粒子流。在不是太阳的情况下，这种带电粒子流也常称为“恒星风”。这种物质虽然与地球上的空气不同，不是由气体的分子组成，而是由更简单的比原子还小一个层次的基本粒子——质子和电子等组成，但它们流动时所产生的效应与空气流动十分相似，所以称它为太阳风。太阳风（solar wind）的密度与地球上风密度相比是非常稀薄而微不足道的。一般情况下，在地球附近的行星际空间中，每立方厘米有几个到几十个粒子，而地球上风的密度则为每立方厘米有2687亿亿个分子。然而太阳风虽十分稀薄，但它刮起来的猛烈劲，远远胜过地球上的风。在地球上，12级台风的风速是每秒32.5米以上，而太阳风的风速，在地球附近却经常保持在每秒350～450千米，是地球风速的上万倍，最猛烈时可达每秒800千米以上。

![](https://foruda.gitee.com/images/1678269063279086438/5e5430e3_12576525.jpeg)

### (4)太阳黑子：

是指太阳的光球表面有时会出现一些暗的区域，它是磁场聚集的地方。黑子是太阳表面可以看到的最突出的现象。一个中等大小的黑子大概和地球的大小差不多。太阳黑子存在于太阳光球表面，是磁场的聚集之处。其数量和位置每隔一段时间会发生周期性变化。根据统计，地球上天气或气候反常均与太阳黑子活动有密切关系。黑子的形成和消失要经历几天到几个星期不等。当强磁场浮现到太阳表面，该区域的背景温度缓慢地从6000摄氏度降至4000摄氏度，这时该区域以暗点形式出现在太阳表面。在黑子中心最黑的部分被称作本影，本影是磁场最强的区域。本影周围不太黑、呈条纹状的区域被称为半影。黑子随太阳表面一起旋转，大约经过27天完成一次自转。 长期的观测发现，黑子多的时候，其他太阳活动现象也会比较频繁。黑子附近的光球中总会出现光斑，黑子上空的色球中总会出现谱斑，其附近经常有日珥（暗条）。同时，绝大多数的太阳爆发活动现象也发生在黑子上空的大气中。因此，从太阳大气低层至高层，以黑子为核心形成一个活动中心——太阳活动区。黑子既是活动区的核心，也是活动区最明显的标志。

![](https://foruda.gitee.com/images/1678269111575438133/080982ca_12576525.png)

### (5)太阳风暴：

太阳风暴指太阳在黑子活动高峰阶段产生的剧烈爆发活动。爆发时释放大量带电粒子所形成的高速粒子流，严重影响地球的空间环境，破坏臭氧层，干扰无线通信，对人体健康也有一些的危害。太阳会在太阳黑子活动的高峰时产生太阳风暴，它是由美国“水手2号”探测器于1962 年发现的，它是太阳因能量的增加而使得自身活动加强，从而向广袤的空间释放出大量带电粒子所形成的高速粒子流，科学家把这一现象比喻为太阳打喷嚏。由于太阳风中的气团主要内容是带电等离子体，并以每小时150万到300万公里的速度闯入太空，因此它会对地球的空间环境产生巨大的冲击。

![](https://foruda.gitee.com/images/1678269152169823884/41ba2bff_12576525.png)

# 2 前沿事件跟踪

### (1)詹姆斯·韦布空间望远镜：

詹姆斯·韦布空间望远镜（James Webb Space Telescope，缩写JWST）是美国航空航天局、欧洲航天局和加拿大航空航天局联合研发的红外线观测用太空望远镜，为哈勃空间望远镜的继任者，于2021年12月25日发射升空；2022年1月24日顺利进入围绕日地系统第二拉格朗日点的运行轨道。

![](https://foruda.gitee.com/images/1678269182686447224/a9475459_12576525.png)

詹姆斯·韦布空间望远镜的质量为6.2吨，约为哈勃空间望远镜（11吨）的一半。主反射镜由铍制成，直径达到6.5米，面积为哈勃太空望远镜的5倍以上。它还能在近红外波段工作、能在接近绝对零度（相当于零下273.15摄氏度）的环境中运行。

2022年7月中旬，詹姆斯·韦布空间望远镜将正式开工，拍摄第一批用于科学研究的照片。当地时间7月8日，美国宇航局（NASA）公布了詹姆斯·韦布太空望远镜(JWST)拍摄到照片的首批天体名单，包括星系、星云和太阳系外巨行星。当地时间7月12日，美国国家航空航天局展示了詹姆斯·韦布空间望远镜拍摄的高分辨率全彩色照片。

![](https://foruda.gitee.com/images/1678269200578158390/1e86aedb_12576525.png)

![](https://foruda.gitee.com/images/1678269270485733493/1fde4394_12576525.png)

2022年7月20日，据央视新闻，据法新社报道，詹姆斯·韦伯太空望远镜可能发现了宇宙中已知最早的星系，该星系已经存在135亿年。8月1日，媒体报道，望远镜发现一个几乎没有重元素的奇怪遥远星系。

美国国家航空航天局2022年9月1日说，詹姆斯·韦布空间望远镜首次拍摄到一颗系外行星的直接图像。美国航天局发布消息说，这颗被命名为HIP 65426 b的系外行星是一颗不宜居住的气态巨行星。它的质量是木星的6到12倍，年龄在1500万年到2000万年之间，比地球年轻得多。

### (2)火星探测：

火星探测是指人类通过向火星发射空间探测器，对火星进行的科学探测活动。大约每隔26个月就会发生一次火星冲日，火星冲日意味着这时可以使用较小花费将探测器送往火星，因此人类的火星探测活动通常也会每隔26个月出现一次高潮。美国国家航空航天局两位科学家曾向美国太空官员透露，他们已经掌握了强有力的证据证明火星上有生命存在，而且这些生命体很可能都躲藏在火星地表以下的山洞当中。探测的火星是太阳系八大行星之一，按离太阳由近及远的次序排列为第四颗。

2019年11月14日，中国火星探测任务在位于河北怀来的地外天体着陆综合试验场首次公开亮相。据介绍，中国首次火星探测任务计划于2020年择机实施。 2020年4月24日，中国行星探测任务被命名为“天问系列”，首次火星探测任务被命名为“天问一号”，后续行星任务依次编号。 7月23日12时41分，长征五号遥四运载火箭托举着中国首次火星探测任务“天问一号”探测器，在中国文昌航天发射场点火升空。

![](https://foruda.gitee.com/images/1678269309036876978/0db3eb88_12576525.jpeg)

2021年4月24日，中国第一辆火星车命名揭晓，名称为“祝融” 。2021年5月15日7时18分，科研团队根据“祝融号”火星车发回遥测信号确认，天问一号着陆巡视器成功着陆于火星乌托邦平原南部预选着陆区，我国首次火星探测任务着陆火星取得圆满成功。

![](https://foruda.gitee.com/images/1678269359717372089/44d18e88_12576525.png)

2021年6月11日，国家航天局在京举行天问一号探测器着陆火星首批科学影像图揭幕仪式，公布了由祝融号火星车拍摄的着陆点全景、火星地形地貌、“中国印迹”和“着巡合影”等影像图。

---

[Beyond Our Solar System Poster - Version B](https://solarsystem.nasa.gov/resources/939/beyond-our-solar-system-poster-version-b/) | NASA Solar System Exploration  
NASANASA|1200 × 1855 jpeg|图像可能受版权保护。

![输入图片说明](https://foruda.gitee.com/images/1678127411119562328/7adddf80_5631341.jpeg "939_poster_beyond_front_b.jpg")

[Solar System and Beyond Poster Set](https://solarsystem.nasa.gov/resources/925/solar-system-and-beyond-poster-set/)

![输入图片说明](https://foruda.gitee.com/images/1685638300075686380/88e564e5_5631341.jpeg "image001.jpg")

# [Our Solar System Poster - Version A](https://solarsystem.nasa.gov/resources/2283/our-solar-system-poster-version-a/)

![输入图片说明](https://foruda.gitee.com/images/1685638585620099759/b5fbfa58_5631341.jpeg "poster_ss_front_a2m.jpg")