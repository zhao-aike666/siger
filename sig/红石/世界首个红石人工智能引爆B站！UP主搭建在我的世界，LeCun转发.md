# [世界首个红石人工智能引爆B站！UP主在我的世界搭建，LeCun转发](http://mp.weixin.qq.com/s?__biz=MzI3MTA0MTk1MA==&mid=2652204123&idx=1&sn=29967b32b9eeca502d27e8130a673ac7)

新智元 新智元 2022-07-04 13:35 发表于北京

![输入图片说明](https://images.gitee.com/uploads/images/2022/0706/234434_0e9fa9dd_5631341.png "屏幕截图.png")

- 微信号: 新智元 AI_era
- 功能介绍: 智能+中国主平台，致力于推动中国从互联网+迈向智能+新纪元。重点关注人工智能、机器人等前沿领域发展，关注人机融合、人工智能和机器人革命对人类社会与文明进化的影响，领航中国新智能时代。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0706/234507_558cc51c_5631341.png "屏幕截图.png")

新智元报道  
> 编辑：如願 好困 桃子

>  **【新智元导读】** 可能你都想不到，一位不是计算机专业的UP主竟在「我的世界」里搭建出世界首个红石人工智能，就连图灵奖得主LeCun转发称赞。

耗时半年，B站UP主终于在「我的世界」实现了首个红石人工智能。

可以说，这是世界上第一个纯红石神经网络。

不仅可以实现15×15手写数字识别，最重要的是，准确率能够达到80%！

![输入图片说明](640%20(7).gif)

目前，这个视频已经有106万播放量，收获无数网友好评关注。

就连图灵奖得主，LeNet架构提出者，Yann LeCun还在社交媒体上转发了这个视频。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0706/234737_432baf62_5631341.png "屏幕截图.png")

### 首个「红石人工智能」

红石是我的世界中的一种独特的材料。

它是传递红石信号的主要元器件，开关、红石火把和红石块等能对导线或物体提供类似电流的能量。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0706/234756_341e8e73_5631341.png "屏幕截图.png")

而红石电路能够为玩家建造起来可以用于控制或激活其他机械的结构。

电路本身既可以被设计为用于响应玩家的手动激活，也可以让其自动工作——或是反复输出信号，或是响应非玩家引发的变化。

因此，在我的世界中，能够被红石控制的机械类别几乎覆盖了你能够想象到的极限。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0706/234833_63a756b4_5631341.gif "640 (8).gif")

小到最简单的机械（如自动门与光开关），大到占地巨大的电梯、自动农场、小游戏平台，甚至游戏内建的计算机。

也就是说，想要扩大在我的世界中可游玩的深度，对红石电路的玩转一定要了如指掌。

UP主 @辰占鳌头 创建的世界首个红石神经网络就是个典例。

作者玩梗写了一个摘要，提到

> 我们使用非传统的计算方式——随机计算来实现神经网络，使得设计和布局上比传统的全精度计算简单许多，且单次理论识别时间仅为5分钟。

  ![输入图片说明](https://images.gitee.com/uploads/images/2022/0706/234922_3cc65cc5_5631341.png "屏幕截图.png")

就拿作者演示手写的2、5来说吧，可以看出其识别程度和置信度直接拉满。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0706/234957_62d01a14_5631341.gif "640 (9).gif")

在具体实现过程中，作者在视频里展示了所包含的元素：

单个神经元：接受多个输入，产生一个输出

乘法器：仅使用随机数和单个逻辑门运算小数乘法

神经元阵列：输出识别结果或传递到下一层

![输入图片说明](640%20(10).gif)

各数字置信度；手写板15×15

![输入图片说明](640%20(11).gif)

另外，作者介绍卷积层是用来提取笔画特征。

全连接第一层：压缩信息并分类；激活函数阵列：将数据非线性地映射到高维特征空间；全连接第二、三层：进一步分类并输出识别结果。

![输入图片说明](640%20(12).gif)

不过，UP主提到，受限于Minecraft的运算能力，实际识别时间超过20分钟。

尽管如此，这仍是红石数电领域的重大突破，也可能启发现实中的硬件神经网络。

### 背后原理

从实际出发考虑，识别手写数字的任务，仅使用深度神经网络即可。

但是为了尽可能压缩计算量和参数量，作者使用了经典的「LeNet-5」卷积神经网络。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0706/235206_6dbd1f10_5631341.png "屏幕截图.png")

「LeNet-5」架构最初就是为了实现手写数字识别而生。不过，为了进一步减小计算开销，作者将网络的5层结构分别设定成15×15→7×7→30→30→10。

权重也被相应的压缩到[-1,1]。虽然这样的做法会将识别率从90%降到50%，但是如果网络的输入还算规整，那么它依旧可以得到一个很不错的识别率。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0706/235223_81fa947d_5631341.png "屏幕截图.png")

由于在Minecraft中实现反向传播是不现实的，而且权重都是使用pytorch训练好之后，再通过代码编辑到文件中，随后导入Minecraft。

由于权重也是随机串，所以只要编辑随机数生成装置即可。

那么随机数生成的原理又是什么呢？

![输入图片说明](https://images.gitee.com/uploads/images/2022/0706/235252_b82fcd30_5631341.gif "640 (13).gif")

其实就是因为投掷器投掷物品的随机性，借此可以搭一个随机数生成器。

例如，在这个视频中：将两个投掷器脸对脸放置，用比较器检测其中的一个内容物。一个放入不可堆叠的物品（矿车），另一个放入可堆叠的物品（混凝土）。

设置矿车输出2格信号强度，混凝土只输出1格强度。再将一个侦测器对准一个投掷器，便可以随机切换另一个侦测器里边的物品，此时有50%的概率可以检测到是矿车。

因此，会得到一个「0」、「1」各占一半的串。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0706/235328_bba685c1_5631341.gif "640 (14).gif")

通过调整可堆叠物品和不可堆叠物品的比例，可以产生分数概率的随机数。将两个随机数生成器连接到同门上，便可计算二者的乘积。

经过简单的优化后，就可以得到一个2tick输出一次的高效随机数生成器。它将生成网络权重。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0706/235354_a8f99656_5631341.gif "640 (15).gif")

随后，激活函数会将神经网路中的输入数据非线性地映射到高维度特征空间，然后再进行线性分割。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0706/235418_28da48d4_5631341.gif "640 (16).gif")

作者使用的「LeNet-5」架构自然也少不了应用激活函数进行非线性映射。

在该过程中，首先先使用卷积提取笔画特征。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0706/235445_12efa4c8_5631341.gif "640 (17).gif")

再将提取出来的特征放进全连接层进行分类。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0706/235508_0fddd6de_5631341.gif "640 (18).gif")

上述过程中的每一层都使用了「非线性映射-线性分割」进行分类。

红石神经网络的宏观流程：

映入眼帘的是输入设备，它包括一个单脉冲式压力板手写板和15x15的坐标屏，手写板每次产生2tick的坐标信号，然后由屏幕绘制手写的数字。

![输入图片说明](640%20(19).gif)

对应到其微观原理的实现，手写的数字会先进入卷积层进行卷积运算，并将特征图结果送入下一层。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0706/235601_7c294453_5631341.gif "640 (20).gif")

为了保证网络的非线性输出，输出结果前还会经过ReLU激活函数。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0706/235624_a6e02235_5631341.gif "640 (21).gif")

由于卷积核大小为3x3，所以可以直接使用模电运算，并且在输出端自动实现非线性化，之后通过硬连线接到手写板输入上。

![输入图片说明](640%20(22).gif)

全连接层的每一层由若干个神经元构成，每个神经元的输入和输出是「多对一」的关系。神经元会将每个输入加权求和实现「线性分割」，再经过激活函数进行「维度提升」。此处使用tanh作为激活函数。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0706/235712_d6c36804_5631341.gif "640 (23).gif")

在实际的神经元电路中，输入部分为加权求和，输出便是激活函数。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0706/235737_6378ed80_5631341.gif "640 (24).gif")

其中，每一层的权重参数（随机串）会被存放在投掷器，然后输入和权重相乘后再通过模电累加，

![输入图片说明](https://images.gitee.com/uploads/images/2022/0706/235805_8ad74293_5631341.gif "640 (25).gif")

接下来，通过模电计算加法，而后转为数电信号，全加器则改装了2tick流水线加法器，堆叠神经元构成一个全连接层。

![输入图片说明](640%20(26).gif)

最后一层的输出使用一个模电计数器，这个计数器的容量可以达到1024。

![输入图片说明](640%20(27).gif)

在输出层，计数器的高四位会被连接到计数板上，电路随后选取最大的值并显示到输出面板上。

![输入图片说明](640%20(28).gif)

一项震惊眼球的工程便得以实现！

最后，我们看一下网络部署

![输入图片说明](https://images.gitee.com/uploads/images/2022/0706/235947_85816aec_5631341.png "屏幕截图.png")

首先使用PyTorch在「ImageNet」数据集上训练，之后将权重部署到模拟器上调整网络的参数，再将训练好的参数填入.schem文件，随后用小木斧将.schem文件粘贴至存档，粘贴完成后，检查权重是否正确部署，终极网络版本结构，即15×15→7×7→30→30→10。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0707/000000_18567f8f_5631341.png "屏幕截图.png")

### 小彩蛋

视频最后，作者投了一个小彩蛋。

在更新的1.19手写板中，特意安装了幽匿感测器，能够侦测出声音。

更好玩儿的是，可以直接往这个幽匿感测器触摸屏扔雪球输入图像。

![输入图片说明](640%20(29).gif)

砰砰砰几下，一个手写非常潦草的6就出来了，识别置信度达到了75%。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0707/000110_52734020_5631341.gif "640 (30).gif")

### 创作团队

除了up主，还有4位团队成员一起完成了这个红石神经网络的构建。

值得一提的是，up主 不是计算机专业的，而且神经网络原理也是从头学起。

在与小伙伴一同合作下，编写1000+行代码训练和模拟红石神经网络，阅读50+页英文文献。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0707/000259_9ac04a3b_5631341.png "屏幕截图.png")

### 网友热评

看完真正的红石人工智能后，网友调侃道，会红石和会人工智能的都沉默了。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0707/000322_646e0210_5631341.png "屏幕截图.png")

还有人称Pytorch和TensorFlow都过时了！以后自己写网络就说用的是Minecraft库。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0707/000329_499aad80_5631341.png "屏幕截图.png")

网友还给其取了一个名，「红石卷积神经网络，简称RCNN」。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0707/000339_9e4243c9_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0707/000349_277df4fc_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0707/000359_f4705fde_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0707/000409_e0f7280c_5631341.png "屏幕截图.png")

### 参考资料：

https://b23.tv/TGELQpU

https://twitter.com/ylecun/status/1543309427893309440?s=21&t=okc8RM9FJdzPgUHXQMLYrw

https://minecraft-archive.fandom.com/wiki/Redstone

https://www.bilibili.com/read/cv17165331

https://alantian.net