# 【Minecraft】[红石神经网络教程第一节——随机数生成、运算及计数](https://www.bilibili.com/video/BV1qW4y117Xv)

作为系列教程的第一发，这次讲一些简单的

![输入图片说明](https://images.gitee.com/uploads/images/2022/0726/035052_e7c738e9_5631341.png "屏幕截图.png")

（白色混凝土块儿上左到右依次：红石比较器、投掷器2个、侦测器）

![输入图片说明](https://images.gitee.com/uploads/images/2022/0726/035439_18151c3a_5631341.png "屏幕截图.png")

（投掷器里面）
里面放入可堆叠的不可堆叠物品
注意左边的投掷器里只能放一个物品（钻石剑）
可堆叠物品每种只能放一个（右面放面包和生猪排）


左边投掷器里是钻石剑（不可堆叠）时，比较器输出强度2

![输入图片说明](https://images.gitee.com/uploads/images/2022/0726/035715_1e096ccc_5631341.png "屏幕截图.png")

（比较器左边，放红石粉，显示2）

如果时可堆叠物品则输出1

![输入图片说明](https://images.gitee.com/uploads/images/2022/0726/040223_c7a8bd90_5631341.png "屏幕截图.png")

（比较器，环绕红石粉电路2X2水泥上。）
这里侦测器接一个2tick周期的红石高频。

这里插一嘴：这个随机数生成器的原理类似瞬间投掷器链

![输入图片说明](https://images.gitee.com/uploads/images/2022/0726/040333_c590d4f1_5631341.png "屏幕截图.png")

（再放上红石块，开始输出）
相比较投掷器漏斗组合，它的速度更快，可以每2tick 输出一次

![输入图片说明](https://images.gitee.com/uploads/images/2022/0726/040558_bced79ef_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0726/041202_9bfcc12a_5631341.gif "投掷器随机物品.gif")

这里可以看到投掷器里的物品再随机切换

假设钻石剑有 a 个，可堆叠物品共有 b 个
则被检测的投掷器里是钻石剑的概率是 a/(a+b)
这个结论是显然的，吧

![输入图片说明](https://images.gitee.com/uploads/images/2022/0726/042415_5dc9edcc_5631341.gif "随机数输出三分之一.gif")

像这样1个钻石剑，1个面包，1个生猪排，输出的概率是1/3

![输入图片说明](https://images.gitee.com/uploads/images/2022/0726/042811_35e85d14_5631341.png "屏幕截图.png")

（把红石敲掉，暂停输出）
如果调整为2个钻石剑，1个面包，1个生猪排，1个熟猪排

![输入图片说明](https://images.gitee.com/uploads/images/2022/0726/042710_9bc8b66e_5631341.png "屏幕截图.png")

（再加上红石，测试）
则输出的概率是2/5

将输出从模电转成数电，最简单的办法就是用比较器

（木桶里放一个物品产生信号强度1、红石粉）

![输入图片说明](https://images.gitee.com/uploads/images/2022/0726/043144_828189fc_5631341.png "屏幕截图.png")

这样可堆叠物品输出0，不可堆叠物品输出1

![输入图片说明](https://images.gitee.com/uploads/images/2022/0726/043338_1d8829b4_5631341.png "屏幕截图.png")

（再把输出的红石粉换成比较器，当输出为0，比较器就灭，输出1时亮）

类似地在另一侧搭一个随机数生成器

这里顺便讲一下随机串的表示

假设随机数生成器输出的概率是 p, 则它表示的数是 2p-1

也就是所谓的双极表示。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0726/043841_cef142f6_5631341.png "屏幕截图.png")

(在另一侧复制了一套随机数发生器)  
比方说，输出概率为 1/2 表示的数是 2X(1/2)-1 = 0

![输入图片说明](https://images.gitee.com/uploads/images/2022/0726/044048_06556e48_5631341.png "屏幕截图.png")

输出概率为1/4表示的数是 2x(1/4)-1 = -1/2

### 接下来搭建一个异或门，用于计算乘法

当然啦，异或门计算的是 -x1 X x2

想要去掉负号需要在输出端加一个非门（这里就不加了）

![输入图片说明](https://images.gitee.com/uploads/images/2022/0726/044400_7f4b1d3e_5631341.png "屏幕截图.png")

这儿就是异或门的输出

![输入图片说明](https://images.gitee.com/uploads/images/2022/0726/044607_bcf14191_5631341.png "屏幕截图.png")

我们调整一下数据（投掷器里的物品）
这里是总共3个钻石剑，1个熟猪排，
输出概率为3/4，表示的数是 2x(3/4)-1 = 1/2

（另一次新建的）
这里是1个钻石剑，和总共3个克堆叠物品，
输出概率为1/4，表示的数是 2x(1/4)-1 = -1/2

![输入图片说明](https://images.gitee.com/uploads/images/2022/0726/044834_0f3cd000_5631341.png "屏幕截图.png")

那么异或门计算的结果应该是 -x1 X x2 = 1/4

![输入图片说明](https://images.gitee.com/uploads/images/2022/0726/045629_40274728_5631341.gif "随机串计算.gif")

那么怎么得出输出的概率是多少呢？

我们可以数数，即统计输出的1占总时长的比例

因为输出的频率是 5Hz (2tick一次）

所以我们只要简单做两个高频

用高频控制投掷器扔物品

然后比较两边扔出来的物品数量即可。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0726/050046_6717c57b_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0726/050131_a6ef5f59_5631341.png "屏幕截图.png")

连接异或门输出的高频只在异或门输出1时闪烁，

而另一个则会不停闪烁

![输入图片说明](https://images.gitee.com/uploads/images/2022/0726/050411_bf0a0e87_5631341.png "屏幕截图.png")

把两个投掷器填满

![输入图片说明](https://images.gitee.com/uploads/images/2022/0726/050454_4df5acc0_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0726/050618_960bcfd4_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0726/050742_c5b523e5_5631341.png "屏幕截图.png")

敲掉这个红块，开始计时！

右边的木桶每2tick 增加一个红石，左边的会慢一些

加速一下，（看效果）

![输入图片说明](https://images.gitee.com/uploads/images/2022/0726/051022_1e5bdbb0_5631341.png "屏幕截图.png")

放回红石块停止计数。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0726/051119_0d59ab42_5631341.png "屏幕截图.png")

数一下两个木桶里的物品数量。右边 5x64+39=359

![输入图片说明](https://images.gitee.com/uploads/images/2022/0726/051243_13fde790_5631341.png "屏幕截图.png")

左边是 3X64 + 48=240。 输出概率为 240/359 = 0.67

![输入图片说明](https://images.gitee.com/uploads/images/2022/0726/051435_9e7b6ea3_5631341.png "屏幕截图.png")

输出概率为0.67，则表示的数为 0.67x2-1 = 0.34

和预计的结果 1/4 （=2.25）相差不大

计数时间越长，结果越准确。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0726/051823_59f47638_5631341.gif "FINAL-随机数计算概率.gif")

那么第一节内容到这里就结束了，谢谢观看！