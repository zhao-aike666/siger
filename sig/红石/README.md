- [【Minecraft】世界首个纯红石神经网络！真正的红石人工智能(中文/English)(4K)](https://www.bilibili.com/video/BV1yv4y1u7ZX)
- [世界首个红石人工智能引爆B站！UP主搭建在我的世界，LeCun转发](世界首个红石人工智能引爆B站！UP主搭建在我的世界，LeCun转发.md)

<table>
<tr>
<td width="59%">
<p align="center">
<img width="95%" src="https://images.gitee.com/uploads/images/2022/0706/184526_868c174a_5631341.jpeg" title="辰占鳌头追求光明与革新.jpg"></p>
</td>
<td width="29%">
<p align="center">
<a target="_blank" href="EE数电工程师的电路绘制之旅.md">
<img width="66%" src="https://images.gitee.com/uploads/images/2022/0729/085245_bfa28c10_5631341.jpeg" title="红石电路随机数生成.jpg"></a><br>
<a target="_blank" href="红石卷积神经网络——原理.md">
<img width="66%" src="https://images.gitee.com/uploads/images/2022/0717/225519_2d5939b5_5631341.jpeg" title="红石卷积神经网络——原理.jpg"></a><br>
<a target="_blank" href="我们距离RISCV红石CPU还有多远.md">
<img width="66%" src="https://images.gitee.com/uploads/images/2022/0709/122524_3fd43711_5631341.jpeg" title="我们距离RISCV红石CPU还有多远.jpg"></a>
</p>
</td>
</tr>
</table>

> 很棒，用 “追求光明与革新” 为题，开启 SIGer 的红石频道。和大部分媒体的科普职能不同，SIGer 融入了我的个人兴趣和方向，我要更关注这个作品，经过多遍重温和考古，原来我和UP主有着同样的志趣，《[红石数电评论](https://space.bilibili.com/481708744)》映入眼帘的那一刻我惊叫地恨不得隔着屏幕伸出握手。这篇采访稿酝酿了不到24小时，已经有说不完的话再等待着 UP 主回答。他的秒回显然是深思熟虑的，透露出一位 UP 主的科普担当。就像他回答的第一个问题，收到约稿，我又补了一万字（这里先省略，以免喧宾夺主。）SIGer 红石频道就此开张啦。:pray:

感谢 @辰占鳌头 接受 SIGer 采访：

1. 首先响应号召：“[也可能启发现实中的硬件神经网络](https://www.bilibili.com/video/BV1yv4y1u7ZX)” 前来考古， **[BV1yv4y1u7ZX](https://www.bilibili.com/video/BV1yv4y1u7ZX)**  观影过后除了震撼和身心愉悦，这个号召，是否就是UP主团队既定的现实目标呢？或着学习和研究目标？

    **其实不是特定的目标，但有的时候无心插柳柳成荫。一个不相干的东西也可能启发一些有益的思考。** 

2. 红石一直以来都有各种数电作品的诞生，多以费时费力的复刻为观者（能力所限）所持的态度，“现实中的硬件神经网络” 是尤为突出的特点。入坑门槛之高，各种小白的赞叹和膜拜之情可见一般。就本作难度，与过往出现的红石作品，或者说神级作品比较，是一个什么高度？可以推荐一些入眼的神作吗？

    **其实没有想象中那么难。从难度来讲，基本低于搭建一台红石CPU。“神级”作品可以参考[BV1U34y1X7cZ](https://www.bilibili.com/video/BV1U34y1X7cZ)** 

3. 最初之作 [BV1yM4y1M7ZZ](https://www.bilibili.com/video/BV1yM4y1M7ZZ) 首次发布于 ARS，我也是按图索骥找到组织的。一番考古，反复观看作品，才注意到最初之作。它是原理篇的一个片段，就是随机数产生的原理。但最初之作的开篇是 “只用一个与门就完成小数乘法计算”，这之间的关联，还望科普一二？

    **其实“最初之作”展示了随机计算在特定场景下的优势，也就是体积小，并行度高。在此基础上，我们设计了基于随机计算的神经网络。也就是说，“一个与门完成小数乘法”不是最重要的，它体现出的随机计算可能的优势是最重要的。** 

4. 组织群里有一张 “[红石数电科技树](https://images.gitee.com/uploads/images/2022/0707/004657_6e3cbe92_5631341.jpeg)” 每一个节点都有多个实现，在红石圈，会用 “发明” 来标注红石电路的作者。这显然不是简单的现实复刻，从专业角度，红石创作的难点或者，红石玩家应该具备的能力在那些方面？知识储备的全面性不言而喻，和这个科技树比对，是否有一张技能树？知识树？

    **我觉得，高阶的红石玩家对Minecraft的特性是较为熟悉的，对红石原件的性质也应当是熟悉的。他们同时精通布线的技巧并且熟练掌握诸如WorldEdit之类的工具。在更高的层次上，他们通常了解计算机体系结构，各种硬件算法，单片机原理等。至于技能树，基本就是我刚刚提到的这几点。** 

5. 初次访问时存的私心 “用FPGA复刻本作”，“可照着文献直接复刻（好像有FPGA的）”的回复，当时坚持的理由是 完成科普的目的，多少有蹭热度之嫌。但现在觉得，MC广阔的空间是立体的。现实中 FPGA 电路的模拟和实现是用 eda 工具在平面展开的。学习红石视频中，也有类似的点，横向水平发展，或者纵向立体发展。这会是 FPGA 复刻的难点吗？对于红石工程师 犹如用一粒一粒沙完成集成电路的排布，而非集成电路工程师用工具链快速实现原型。此问，再次请求，给予FPGA复刻的工程给予一些建议 :pray:

    **我不太了解FPGA。我只知道这些文献确实是用FPGA验证了随机计算神经网络的可行性。Minecraft有原生的随机数生成，这在FPGA上是没有的。** 

6. 就科普的角度，能将红石实现，与FPGA 实现做一个比对，是不同实现，加深理解的好方法，以此延伸，红石的创作，是具有科普意义的，这和 MC的编程版不同，空间更大？对青少年的技能培养和思维拓展大有裨益，可否就科普这个方面，给予小白的成长路径给予一些建议。师生同学已经是当下教育的常态，UP主就是我们共同的导师 :pray:

    **我给不出太多的建议，能给的建议就是多看多学多想。我觉得红石神经网络更多的是一个契机，可以引导观众去学习了解神经网络、随机计算和FPGA。而真正的知识不是我三言两语能在视频里讲清楚的。从工程的角度来讲，人们往往想法很多，但真正能落实并完成的很少，这需要经验、毅力、决心以及良好的习惯。** 

7. 现实应用有一个案例非常符合本作的工程场景：单一的输入，清晰的工程实现方案，保证唯一的输出。手写体到识别的数字。现实应用有一个听障人群（儿童先天，老人后天）的助听器算法处方的应用：人耳听力缺失不是线性的，是人耳生理损伤的不确定造成的，不同的人对不同频率的接收是不一致的，这造成了无法简单通过音量（早期）来改善听觉效果的现象，现在发展为听觉改善，也是技术进步到了一个阶段。通过神经网络和深度学习，能够实现人耳个性化适配的处方开具。基本工程场景和本作手写识别非常一致，现实工程也有硬件神经网络的实现，目前阶段为较大的模型，导致小型化难题，还无法走出实验室。红石音乐之前也是我关注的内容，多为大作复刻，是否从理论上，红石能实现声音信号的处理？具体选题，可以之后再提。这个选题无论是科普还是公益都是有现实意义的。（之前参加过一个国际爱耳日的活动了解到的这个场景。）

    **其实作为up主，流量是我不能不考虑的。之所以确定做手写数字识别，是因为这是没有人做过且很容易展示的。而红石音乐虽然可以被数字化（以及有人做了），但进行信号处理很难展示给观众。如果观众不理解，那么便起不到科普的作用。而且红石音乐和现实中的音频差距还是很大的。关于语音的神经网络模型，我知道的有RNN（循环神经网络）和Reservoir Computing（储备池计算），可以了解一下。** 

8. 组织群文件中有大量的各类数电的学习资料，以及红石实现。其中包括CPU ，甚至 RISCV的指令集 V1,V2压缩指令集都有，SIGER 学习小组就有 RISCV小组，红石能有实现 16条指令的 8位机，实现RVI32(现在以64为主)的 I型指令，应该没问题吧。看UP主有在其他神作基础上增加 L1L2缓存的实力。结合工程实现可以给些具体建议吗？比如位宽对工程量的影响。riscv只规定了指令集，并不限制具体实现，前作神作是否可以有现成可复用的红石电路？

    **理论上是没问题的。位宽主要影响延迟和游戏卡顿。如果想要达到1Hz时钟，那么位宽导致的延迟（一般是6tick）就会严重影响设计。很多模块是已经做好的，如果时钟比较慢的话，只要拼一拼就好了。** 

9. 红石电路的输入输出，都需要在MC世界中，是否有外接IO的模组或者实验项目存在？有网友评论区讲，模拟器上的模拟器再跑个虚拟CPU，这样层层套外的工程学意义在哪里？作为资深红石工程师，能否介绍和红石玩家的区别？当然这样称呼，是源自红石数位评论的推荐视频，open redstone engineers CCA加法器教程！世界上最好的红石加法器(BV1Sh41197uE) 

    **外接IO的想法很早就有了，实际上也有相关的工具。至于工程学意义，我想可能就是好玩外加炫技。我觉得和普通玩家的区别无非是懂的多一点做的多一点而已。** 

10. 考古了您的空间，现在香港求学，这和媒体上介绍的团队信息并不吻合，或者说媒体报道有失真。被 LeCun转发 的是 油管的版本吧，油管的版本，是其他UP主转发的吧，在公开媒体报道中，还对其作了介绍（把视频转推的同学的原名叫Yingtao Tian）。这个是乌龙吗？还是UP主在油管的分身？希望正是介绍下咱们团队，是如何聚到一起的，以及日常协作的场景？

     **Yingtao博士已经联系我们了。他已经要求讹传的公众号改正。LeCun转发的是b站的版本，是个截图。至于团队，实际上不能算团队，只是我和同学合作，外加和群里的朋友要了些红石组件。** 

11. [【Minecraft】基于随机计算的纯红石卷积神经网络——概览](https://www.bilibili.com/read/cv17165331)，是否可以转载？作为 siger/红石 频道的开篇。作为最高规格的兴趣，我相信会获得很多同学的青睐，届时邀请 UP主和团队，担任 SIGER 大使为同学们答疑解惑，都能追粉，成为 《红石数位评论》 的忠粉，也是本期创刊封面隆重推荐的原因。下面的摘录，就是您的专业解答，相信同学们包括我也需要好好补课才能理解。

    > 我在去年7月份投稿了视频红石随机计算，里面提到了如何使用随机串计算小数乘法。这种计算方式不是精确的，所以不能用来搭计算器。当时，我便萌生了用随机计算搭神经网络的想法，因为神经网络不需要精确的结果。另一方面，随机计算实现乘法实在是太容易了。对于单极(unipolar)表示，只要一个与门；对于双极(bipolar)表示，只要一个同或。我随后去找相关论文，发现随机计算神经网络模型已经有人研究过了[1]，也在FGPA（现场可编程逻辑门阵列）上跑过了。论文里提出的一个算法可以很容易的计算tanh这个激活函数，只需一个全加器。试想，用传统方式计算tanh(x)=(e^x-e^-x)/(e^x+e^-x)需要多么复杂的电路。年初的时候，我简要地做了一个估算。使用全精度计算（乘法器和加法器）去实现神经网络的复杂度和开销都远大于使用随机计算[2]。同时，随机数的生成也很方便，只要投掷器就可以。这便是基于随机计算的红石神经网络的想法的来源。

     **可以** 

12. 在原理篇中，介绍数学原理的动图，是用什么软件制作的？SIGER 之前出过一个[数学专题](../../STEM/最难科普的数学.md)，介绍了 manim 的动图制作。是一位工学博士开设的专栏，很好的数学工具。

     **是manim** 

13. 本次采访还作了很多功课，比如现在正是高考季，无数少年的未来梦就此展开，我也找到一张您的同学五湖四海的地图，您只身前往香港求学的心路历程？就您的专栏文章的造诣，绝对不是媒体标题的非专业人士。可以为同学们介绍一下您的学研方向吗？

     **理论物理（凝聚态理论）。对于计算机专业，自然就是外行。虽然我自学过很多计算机的知识，但到底不是科班出身。** 

14. SIGER 有一个标准提问《我的兴趣爱好》？在UP主的作品中，都会选择BGM，史诗的，舒缓的，音乐方面有什么爱好吗？或者整体为同学们介绍下学习之外的兴趣爱好。

     **我本人偏好古典和现代严肃音乐（主要是极简主义），其他的音乐也常常听。但是选曲必须考虑大众因素，所以我是请群里的朋友帮忙挑的。当然，不能太俗套。** 

15. 本次采访，不能采访整个创作团队了，希望 本期专题上线后，SIGER/红石频道能够顺利发布，届时再邀请整个团队分享更多红石心得，以惨读者。封面标题，是每次采访的标配，我拟选了两个题目？您也可以自己拟定一个本次采访的题目。或者采访结束后，我再通过文字提取一个选题。这最后一个问题，就留给您推荐《红石数位评论》吧，定位，畅想，任何？这一定也是SIGER读者最希望了解的。

     **我们不是一个真正意义上的团队，电路和视频都是我制作的。但他们提供了很多帮助，所以按学术惯例，列为共同作者。对于后一个问题，可以。**   
     **先进红石数电技术研讨组(ARS) 的GitHub 地址：[Releases](https://github.com/ARS-MC/RRDC/releases) · [ARS-MC](https://github.com/ARS-MC/RRDC)/[RRDC](https://github.com/ARS-MC)** 

