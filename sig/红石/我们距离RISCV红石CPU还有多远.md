- [我的世界红石计算机教程1,《我的世界》红石电脑制作原理及使用教程](https://blog.csdn.net/weixin_36294980/article/details/118628306)
- [在《我的世界》里从零打造一台计算机有多难？复旦本科生大神花费了一年心血...](https://blog.csdn.net/yH0VLDe8VG8ep9VGe/article/details/90357077)
- 红石数电开源教程：[Minecraft-Digital-Circuit](https://gitee.com/RV4Kids/Minecraft-Digital-Circuit) 
  - [说明文档(简要)](https://gitee.com/RV4Kids/Minecraft-Digital-Circuit/blob/master/docs/说明文档简要.md)
  - [CPU说明](https://gitee.com/RV4Kids/Minecraft-Digital-Circuit/blob/master/docs/CPU说明.md)
  - [教学存档单元和模块列表](https://gitee.com/RV4Kids/Minecraft-Digital-Circuit/blob/master/docs/教学存档单元和模块列表.md)
  - [异或门和全加器最简结构设计原理](https://gitee.com/RV4Kids/Minecraft-Digital-Circuit/blob/master/docs/异或门和全加器最简结构设计原理.md) |

<p><img width="706px" src="https://images.gitee.com/uploads/images/2022/0709/122524_3fd43711_5631341.jpeg" title="特别策划：红石RISCV.jpg"></p>

> 连续两周都以红石为题，显然我已经着魔了。但这就是新知的魅力，如题的 Flag 是延续第一期的访谈，@辰占鳌头 对公开媒体已经预报了这个大项目，7级流水线啊！就是一生一芯的同学们也才刚刚从三级提升到五级，不过半年时间。但看了 他推荐的 神作（本期也再结尾精读）就知道，RISCV 早就具备了拥抱 “元宇宙” 的实力了。片头不适合剧透太多，同学们异步结尾的笔记才是我最希望的，它记录了我距离 RISCV 红石 CPU 的距离在一点一点缩短，山是要爬的，但不一定非要是珠穆朗玛峰，难爬的山可不只这一座，甚至最难得都不是最高的。现在的回答有两个版本：

  1. “指差一次通读RISCV超标量流水线的时间”
  2. 翻过自己心中最难的那座山的时间。

本期开篇依然是障目的媒体，就好比最快报道的会犯低级错误一样，中规中矩的一片报道不能满足真正想登山的同学一样，但这是我们探索起点，就是下面这篇标题瞩目的文章，在阅读他的 GITHUB 分享，我决定 clone 一份完成一次 md 改造，这些周边工作，对《红石数电评论》SIGer 也会协助，就好像珠峰大本营一样，我希望有更多的同学，能够得到支持，从此开始攀登自己心中的高峰。主体推荐是 《[从零开始搭一台计算机](#从零开始搭一台计算机-minecraft)》，结尾是《[前所未见的1Hz超强红石电脑](#minecraft这是你从未见过的1hz超强红石电脑字幕)》。

> 标题中我们的立场，是以中国的红石社区为出发点的发愿，同时也是 SIGer 的起点，同学们可以看笔记中记录了关于[手写识别](#笔记手写数字识别数据集-mnist)的实践，也是一位中学生在 SIGer 留下的笔记 “[用钢丝做一台RISCV架构的计算机并跑个linux](https://gitee.com/flame-ai/siger/issues/I4UFSE)”

# [在《我的世界》里从零打造一台计算机有多难？复旦本科生大神花费了一年心血...](https://blog.csdn.net/yH0VLDe8VG8ep9VGe/article/details/90357077)

量子位

于 2019-05-19 12:07:37 发布

472
 收藏 2
栗子 晓查 发自 凹非寺 
量子位 报道 | 公众号 QbitAI

![输入图片说明](https://images.gitee.com/uploads/images/2022/0709/020449_1d99b71e_5631341.gif "e7a1ebff9fd3b044bdb8faa55780307e.gif")

一块小小的CPU里有多少个晶体管？几十亿个。

单枪匹马造出一个CPU乃至完整的电脑需要多长时间？有位大牛在《我的世界》游戏里用实际行动回答了这个问题：可能要花费一年多。

这篇造计算机的教程一经转载就在知乎上火了。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0709/020508_e1bdfd3e_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0709/020515_91917a8f_5631341.png "屏幕截图.png")

这并不是一篇游戏攻略，而是来自复旦大学的季文瀚，写的一篇课程论文。他在大二时就有了大胆的设想，经过一年的精心营造，建起了一个计算机雏形，取名Alpha21016。

虽然它不能与现实中的计算机相比，只能实现一些简单的功能，但这台计算机体积惊人，光看它复杂的结构就已经能感受工程量的巨大。

有网友感叹，发课程论文可惜了，简直可以发学术论文啊。

### 这台计算机能做什么
季文瀚计算机使用的是哈佛结构，而非更常见的冯·诺依曼结构。程序储存器和数据储存器分开放置。程序储存器1kb，数据储存器0.5kb。

它可以实现各种函数运算：加减乘除、三角函数还有矩阵运算。它包含一个16bit的CPU和一个32bit的浮点运算单元 (FPU) 。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0709/020529_fb52613c_5631341.png "屏幕截图.png")

从硬件上看，它是个超大规模集成电路，逻辑门总数大概在5万-10万门之间。光是存储器堆叠起来就有8层。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0709/020537_b084fda8_5631341.png "屏幕截图.png")

要造出这样一台计算机，数字电路、微机原理、汇编语言、编译原理都不能少。想想你挂过哪几门课，从学会到熟练运用就更难了。



![输入图片说明](https://images.gitee.com/uploads/images/2022/0709/020543_5c5032f6_5631341.png "屏幕截图.png")

有了专业知识的支持，就能将计算机拆解成基本的部件。

我们都知道计算机的基础是数字电路，数字电路的基础是“门”，季文瀚用游戏里基本的“红石电路”搭建出了逻辑门。

从逻辑门出发，再搭建出组合电路、时序电路、触发器，有了这些就能组成CPU的一些基本单元，最终造出整个计算机。

现实世界中，晶体管是数字电路的基础；在《我的世界》中，红石电路是构成复杂电路的基本单元。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0709/020552_8c34caa0_5631341.png "屏幕截图.png")

红石电路玩家，只用火把和方块，就能造出基本的逻辑门：或门和非门。或门和非门的组合可以造出与门、异或门等任意逻辑门。

但仅仅知道怎么制造逻辑门离造出计算机还很远，可能大致相当于造出汉字笔画到写出《红楼梦》的距离。

季文瀚先给自己的CPU架构画了一个草图：

![输入图片说明](https://images.gitee.com/uploads/images/2022/0709/020600_91b974df_5631341.png "屏幕截图.png")

其中每一个方框都代表一个或若干个硬件单元，小一点的大约一两百个门电路，大的有几千个门电路。这个密密麻麻的部分，也只是架构的右半部分而已：

![输入图片说明](https://images.gitee.com/uploads/images/2022/0709/020612_9c94b036_5631341.png "屏幕截图.png")

知道了CPU的基本架构，再按照架构图分别造出每个部分，比如CPU的重要模块“算数逻辑单元”（ALU）和“指令寄存器”（IR），工程量很大。

算数逻辑单元还能进一步拆解，它的加法器由数个全加器组成，上面基本的逻辑门可以组成加法器中最基本的全加器 (下图) 。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0709/020620_e66fd94c_5631341.png "屏幕截图.png")

全加器也是计算机的一个核心部件。

同时，《我的世界》还提供的基于活塞机械的断路，用信号控制电路的通断，也就是继电器。利用继电器和逻辑门的组合可以造出存储器。

![输入图片说明](https://img-blog.csdnimg.cn/img_convert/e19b0a5d652150ba08ee9887ef7f8088.gif "在这里输入图片标题")

### 计算器→单片机→计算机
大概是因为太复杂，季文瀚一开始也没想直接搭个计算机。

最初，他的目标是造出一台16 bit的简单计算器。

但做到一半，他就觉得可以实现更复杂的东西，于是想改成单片机：这是具有“图灵完备性”，可以执行一切计算机程序的简单计算机。

他规划了指令集架构，储存器架构，以及指令发射方式等等。

后来，触发器、可读写储存器、缓冲队列等等重要电路，季文翰都设计成功了。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0709/020702_54f7d2c8_5631341.png "屏幕截图.png")

有了这些，少年又做了更雄伟的计划：做个16 bit的CPU。

CPU旁边，还有一个包含超越函数的单精度32 bit浮点处理器 (FPU) 。

这里，计算器作为片外系统，并没有被抛弃。季文翰把16 bit计算器，改成了完全时序逻辑电路控制、且有溢出判断的计算器——这在Minecraft红石电路玩家里，已是前所未有。

它借用CPU的ALU部分进行运算，并经过总线传输数据。

CPU和计算器的大部分硬件，都在这张表格里：

![输入图片说明](https://images.gitee.com/uploads/images/2022/0709/020716_e2f01914_5631341.png "屏幕截图.png")

表上的40个硬件，除了指令译码器、指令发射端、异常中断响应没有做完，其他都做好了。还有一些小的硬件单元没有列出来。

目前，CPU的ALU、主储存器、和寄存器等EU部分已经完工，内部环状总线已竣工，CU部分，也就是最繁琐的部分，还没有完工。

![输入图片说明](https://img-blog.csdnimg.cn/img_convert/03ae91abac4d976250310e122bbab637.gif "在这里输入图片标题")

### 肉眼可见的威力
季文翰说，虽然还没完全竣工，但CPU已经可以执行许多种机器指令 (以MOV为主) ：通用寄存器赋值，按字/字节+立即数/间接/直接寻址。

其中，最容易用肉眼感受到威力的，还是借用CPU的ALU完成运算的计算器。

他在视频里展现了加减乘除，正余弦，以及平方根的计算。

![输入图片说明](https://img-blog.csdnimg.cn/img_convert/fe7657d1baf60289529b87cdba0ea5d9.gif "在这里输入图片标题")

从养着小猪的地方走楼梯下来，就是计算器的所在地了。这里有两排按钮，还有显示屏，如上图。

![输入图片说明](https://img-blog.csdnimg.cn/img_convert/42db356a5870e43113734bfbc6f2e90a.gif "在这里输入图片标题")

屏幕后面，可以看到运转的电路。

先做加减乘除。比如加法：

减法也是同理。只不过，负号和减号在这里分成了两个按钮。

乘法和除法的运算量比较大：三位数乘三位数，大概需要20秒；除法更慢一些，电脑还会卡。

下图就是除法，因为打了反除号 (\) ，所以被除数在右边。左下是商，右下是余数。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0709/020825_af471913_5631341.png "屏幕截图.png")

空间限制了算力，所以计算器要有溢出判断，超过±32627的范围就会报错，显示“E”。

不论是输入的数还是计算结果，超出范围都会报错：

![输入图片说明](https://img-blog.csdnimg.cn/img_convert/b947a974d318c728e6cc88377b433525.gif "在这里输入图片标题")

除以“0”，也会报错。

注意，计算机用二进制来计算，算好之后还要从二进制转成十进制，才是最终的答案。这里用到了BCD/BIN转换算法，把二进制BIN码，转成十进制BCD码。

四则运算做完了，还有正余弦，用的是Cordic旋转迭代算法：

![输入图片说明](https://images.gitee.com/uploads/images/2022/0709/020843_1d3c73d0_5631341.png "屏幕截图.png")

需要多次迭代，所以运算比较慢，大概花了两分钟。

相比之下开根号就快许多，用的是快速平方根算法：

![输入图片说明](https://images.gitee.com/uploads/images/2022/0709/020850_72a8d767_5631341.png "屏幕截图.png")

20秒 (就) 开好了。

计算能力就展示到这里。

而机智的你可能已经也感受到了，显示器对于一台计算机有多重要。那么：

### 显示器怎么做？
游戏空间太狭窄，造显卡是不现实的：2×2个红石灯，就是游戏能控制的最小像素了。

所以，季文翰做了字符显示器。

首先，用七段显示器来表示数字。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0709/020910_00eac351_5631341.png "屏幕截图.png")

△一个“日”字，是7根小棒组成的

比如，“4”就有左上、右上、中、右下，一共四根小棒。

每根小棒又由三个方块组成。把这些方块的活塞往回抽，就显示出凹陷的“4”了。

而每个十进制数，都可以对应二进制的四位数，比如3是0011，9是1001。输入二进制数，屏幕就能显示成十进制。

数字搞定了，还有其他字符。季文翰用了自己设计的缩减版ASCII码，只有不到64个字符：

![输入图片说明](https://images.gitee.com/uploads/images/2022/0709/020920_e5d43910_5631341.png "屏幕截图.png")

给每个字符编个号：0，1，2，…，63。每个号码，都可以转成二进制数00000-111111。

然后，显示出来长这样：

![输入图片说明](https://images.gitee.com/uploads/images/2022/0709/020931_47931092_5631341.png "屏幕截图.png")

打开夜视，萤火一般，美不胜收。

其实，这些字是“印”在了显示器的键盘上，白天长这样：

![输入图片说明](https://images.gitee.com/uploads/images/2022/0709/020941_cba787b9_5631341.png "屏幕截图.png")

也就是说，计算机有了，显示器有了，键盘也有了。

而这样的杰作，居然来自一位“业余选手”。

### “我学的不是计算机”
现在来回顾一下，从逻辑门到计算机，都要经历什么：


```
或门，非门

→与门，异或门

→全加器，信号长度转换器，多态选择器，储存器单元，译码器单元，求补码单元，移位器单元

→可读写储存器，译码器，加法器，移位器，时钟发生器

→加减法器，乘法器，除法器，可读写储存器阵列，寄存器，程序计数器

→总线，ALU，CU

→计算机
```


令人意外的是，造出这项复杂工程的季文瀚，是复旦大学2011级生命科学学院的本科生，没有受过系统地计算机科学专业教育。他说，看到国外玩家的作品很感兴趣，才自学了一些专业课。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0709/021018_dbb5db9c_5631341.png "屏幕截图.png")

大二便启动了Alpha21016计算机的开发，作为《网络虚拟环境与计算机应用》这门课的项目来做的。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0709/021035_3c4b0e68_5631341.png "屏幕截图.png")

从他对技术细节的解读来看，那时的季文翰，已经硬件和软件上拥有无比充分的准备。

 **普通人的话** ，可能了解逻辑电路的基础。普通红石玩家的话，可以把逻辑电路的基础知识，用来搭建简单或复杂的红石电路。

高阶红石玩家，也曾经在季文瀚的项目开始之前，造出过计算器。

但制造一台计算机，并没有多少人敢想。季文翰不但想到，还用了一整年去实现，几近完成。

毕竟，如果有个容量惊人的大脑，总归要拿来用的吧。

[640?wx_fmt=gif](https://img-blog.csdnimg.cn/img_convert/d2e986f8b2987f9e88738801c90ed191.gif)

技术博客原文传送门：

http://blog.renren.com/blog/263123705/911088369

一期视频传送门：
https://v.youku.com/v_show/id_XNTkyNTg0NTEy.html

二期视频传送门：
https://www.bilibili.com/video/av4221161/

— 完 —

[订阅AI内参](https://images.gitee.com/uploads/images/2022/0709/021116_eb853597_5631341.png)，获取AI行业资讯

### 加入社群

量子位AI社群开始招募啦，量子位社群分：AI讨论群、AI+行业群、AI技术群；

欢迎对AI感兴趣的同学，在量子位公众号（QbitAI）对话界面回复关键字“微信群”，获取入群方式。（技术群与AI+行业群需经过审核，审核较严，敬请谅解）

### 诚挚招聘

量子位正在招募编辑/记者，工作地点在北京中关村。期待有才气、有热情的同学加入我们！相关细节，请在量子位公众号(QbitAI)对话界面，回复“招聘”两个字。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0709/021142_82a1fa07_5631341.png "屏幕截图.png")
量子位 QbitAI · 头条号签约作者

վ'ᴗ' ի 追踪AI技术和产品新动态

喜欢就点「好看」吧 !

---

@[辰占鳌头](https://space.bilibili.com/172508474) 章鱼肝 9
> 十八岁是最信尼采的年纪。

TA的合集和视频列表
# [从零开始搭一台计算机 Minecraft](https://space.bilibili.com/172508474/channel/seriesdetail?sid=1804891)

【红石教程】【Minecraft】从零开始搭一台计算机！播放列表 2020 整理

| 时长 | 序号 | 名称 | 播放 | 日期 | 
|---|---|---|---|---|
| 02:15 | 红石随机计算 | [Stochastic Computing in Minecraft](https://www.bilibili.com/video/BV1yM4y1M7ZZ) | 4802 | 21-7-8 |
| 03:44 | 第0x11集 | [串行乘法优化](https://www.bilibili.com/video/BV1ay4y1s7LQ?spm_id_from=333.999.0.0) | 2236 | 21-4-25 |
| 01:35 | 第0x12集 | [8位红石CPU(自制)](https://www.bilibili.com/video/BV1Fz4y1D78W) | 4700 | 21-1-22 |
| 10:23 | 第0x13集 | [CSA乘法器](https://www.bilibili.com/video/BV1nv411t7pk) | 2844 | 21-1-1 |
| 03:06 | 第0x10集 | [串行乘法](https://www.bilibili.com/video/BV19Z4y1T7Rf) | 1849 | 8-5 |
| 05:26 | 第F集 | [拓展字符集（汉字输出）](https://www.bilibili.com/video/BV1qk4y1z7ZT) | 2609 | 6-24 |
| 02:54 | 第E集 | [同字优化](https://www.bilibili.com/video/BV1EA411i7JY) | 1195 | 6-16 |
| 09:43 | 第D集 | [控制](https://www.bilibili.com/video/BV1pz411i7Mo) | 1422 | 6-7 |
| 08:08 | 第C集 | [队列](https://www.bilibili.com/video/BV1V5411s7D3) | 2825 | 5-21 |
| 08:20 | 第B集 | [并行](https://www.bilibili.com/video/BV1Bk4y1r7bg) | 2689 | 4-24 |
| 05:03 | 第A集 | [字符集](https://www.bilibili.com/video/BV1Wt4y1U7KM) | 3716 | 4-12 |
| 06:57 | 第9集 | [显示屏单元](https://www.bilibili.com/video/BV19741117s6) | 6613 | 3-24 |
| 05:07 | 第8集 | [通用显示器原理](https://www.bilibili.com/video/BV1KE411N7mq) | 8698 | 3-20 |
| 05:54 | 第7集 | [全加器的改进](https://www.bilibili.com/video/BV1LE411u7U5) | 2364 | 3-10 |
| 09:42 | 第6集 | [二进制转十进制](https://www.bilibili.com/video/BV1qE411H7SN) | 5944 | 2-29 |
| 04:07 | 第5集 | [数码管](https://www.bilibili.com/video/BV1Bj411f7Pw) | 2622 | 2-23 |
| 04:47 | 第4集 | [减法与补码](https://www.bilibili.com/video/BV1f7411c74C) | 2463 | 2-21 |
| 07:39 | 第3集 | [全加器](https://www.bilibili.com/video/BV1y7411j7oi) | 4911 | 2-17 |
| 07:47 | 第2集 | [性能评估与无延时逻辑门](https://www.bilibili.com/video/BV1P7411n79g) | 5202 | 2-16 |
| 03:30 | 第1集 | [基础逻辑门](https://www.bilibili.com/video/BV1j7411g7GE) | 7702 | 2-14 |
| 02:17 | 第0集 | [红石进阶](https://www.bilibili.com/video/BV1G7411g7KZ) | 5833 | 2-14 |

## [第0集 红石进阶](https://www.bilibili.com/video/BV1G7411g7KZ)

> 无基础的同学请移步P2

写在前面：这个系列将从头开始搭建一台计算机，并逐步讲解各个元器件的原理和作用。红石进阶的目的在于深入了解元器件工作方式，为设计与调试提供指导

中继器除延时外，会将较短的脉冲延长至与自身延时相同。利用这一原理，可以制造 1tick 到 4tick 脉冲发生器。

粘性活塞在响应 1tick 脉冲时，会推出而不收回。这也是活塞部件故障的主要原因。

2tick 脉冲是安全的，活塞推出瞬间，前方方块会变成 36 号透明方块，可以用该特性设计无延时电路。

类似地，红石火把也不能响应 2tick 以下的脉冲。红石火把可以响应 2tick 脉冲，并在延时 1tick 后输出等长脉冲。连续红石火把也可以中继，然而由于红石更新的存在，一些看似合理的结构实际上无法响应 2tick 脉冲。

3tick 脉冲是可行的。

比较器处于比较模式，若输入强度不小于比较前度，则维持输出强度，否则不输出。调整为减法模式，输出强度变为输入端减比较端。

比较器可以中继信号，延时 1tick, 输出强度等于输入强度。

（下一集预告：基础逻辑门）

### P2. [红石基础讲解](https://www.bilibili.com/video/BV1G7411g7KZ?p=2)

红石块，红石火把，拉杆产生 15 信号强度，而后沿红石线递减直到 0。一般将红石线亮称为高电平，灭称为低电平。

中继器可以中继信号，但会有相应延时，从 1tick 到 4tick。一般地，tick 是红石元件的最小更新周期。

红石线对指向和下方方块弱充能。与方块相连的活塞和中继器对此响应。点状红石指向四周方块。

中继器对指向方块强充能。强充能方块除保留前述特性外，还能点亮上下和周围红石线。

红石火把所在方块充能时，火把熄灭

红石火把指向的方块被强充能

频繁快速闪动会导致红石火把熄灭

## [第1集：基础逻辑门](https://www.bilibili.com/video/BV1j7411g7GE)

逻辑门是计算机的基础，其数学本质是对真值（即真和假的运算）。这里将用真值表表示每种运算的结果。1表示真，0表示假。

### 非门

非门（输出与输入相反）
记号：┐p 或者 NOT p
真值表：
| p | ┐p |
|---|---|
| 0 | 1 |
| 1 | 0 |

红石火把所在方块被充能时，红石火把熄灭，这就构成了非门。钻石块表示输入端，金块表示输出端。

### 或门

或门（一个为真即为真）
记号：pVq 或者 p OR q
真值表：
| p | q | pVq |
|---|---|---|
| 0 | 0 | 0 |
| 0 | 1 | 1 |
| 1 | 0 | 1 |
| 1 | 1 | 1 |

Minecraft 中的或门是平凡的，不是指数学上的平凡，因为它真的过于简单了。

### 与门

与门（全部为真才为真）
记号：pΛq 或者 p AND q
真值表：

| p | q | pΛq |
|---|---|---|
| 0 | 0 | 0 |
| 0 | 1 | 0 |
| 1 | 0 | 0 |
| 1 | 1 | 1 |

或和非可以实现与。
根据 De Morgan's Law，我们有：
pΛq = ┐(┐p V ┐q)

 **p AND q = NOT((NOT p) OR (NOT q))** 

- 分别搭建 NOT p 和 NOT q
- 这里则是与门 OR
- 外层的非门 NOT
- 金块
- 与门便完成了

输入一个 1 为 0，输入两个 1 才为 1

同样遵循 De Morgan's Law， **这个设计更简单小巧** 

### 异或门

异或门（不同为真，相同为假）
记号：p⊕q 或者 p XOR q
真值表：

| p | q | p⊕q |
|---|---|---|
| 0 | 0 | 0 |
| 0 | 1 | 1 |
| 1 | 0 | 1 |
| 1 | 1 | 0 |

根据定义，我们有，
```
p⊕q = (┐pΛq)V(pΛ┐q)
```

- 这里我先摆出常规设计
- 注意这里是遵循 De Morgan's Law 的变体与门
- 异或门便完成了

输入一个1结果为1，输入两个1结果为0

为什么这个异或门是合理的？我们需要将其抽象
- 与门
- 或门&非门

表达式为： ┐((pΛq)V┐p) V  ┐((pΛq)V┐q)

推导过程：

```
 ┐((pΛq)V┐p)  V  ┐((pΛq)V┐q)
= (┐(pΛq) Λ p)  V (┐(pΛq) Λ q)
= ((┐pΛ┐q) Λ p)  V ((┐pΛ┐q) Λ q)
= ((┐pΛp) Λ (┐qΛp))  V ((┐pΛq)Λ(┐qΛq))
= (F Λ (┐qΛp))  V ((┐pΛq)ΛF)
= (┐qΛp)  V (┐pΛq)
= p⊕q
```


Q.E.D.

下集预告：性能评估与无延时逻辑门

## [第2集：性能评估与无延时逻辑门](https://www.bilibili.com/video/BV1P7411n79g)

### 为什么要做性能评估？

通过性能评估，我们了解红石元件的工作方式、延时特征，便于模块化管理。这对于信号系统和同步控制的设计是不可或缺的。

### 怎么做性能评估？

1. 延时分析，
2. 工况分析。

方便起见，我们用 3个参数描述元件的性能：
- 上升沿延时、
- 下降沿延时、
- 最短响应脉冲（时长）。

（接下来解释这几个概念。）

这是一个脉冲。左端（由暗到亮）称为上升沿，右端（由亮到暗）称为下降沿。
（一般的元件接收一个脉冲，仍输出一个脉冲，然而脉冲的上下沿会有相应的延时。）

回顾第0集的内容。譬如这个非门，上升下降沿延时都是 1tick （以下简写为 1t）

上升沿延时和下降沿延时不一定相同。比如这个活塞中继器。伸出时瞬间接通线路，收回则要 1t。因此它的上升沿延时为 0t，下降沿则为 1t。

### 什么时最短响应脉冲呢？

是一个元件能响应并稳定工作的最短的脉冲称为最短响应脉冲（时长）。

- 红石火把不能响应 2t 以下脉冲。因此其最短响应脉冲时长为 2t。

- 粘性活塞能响应 1t 脉冲，但不会收回。这往往引发错误。所以其最短响应脉冲为 2t。

- 有时事情会出乎意料。比如这个元件的最短响应脉冲理应是 2t，但由于红石更新，实际上是 3t。

为了方便叙述，我们使用（1/1/2）这样的三元组表示上升沿延时、下降沿延时、最短响应脉冲（时长）这三个参数。这里遵循惯例将短于 1t 的脉冲记为 0.5t。除此之外，部分元件还需要额外的描述。（显然，参数越小，性能越好）

- 平凡的非门：（1/1/2）
- 火把与门：（2/2/2）

除分析外，最好能做实验。
- 给这个异或门一个附加检测装置
- 这里加一个可调脉冲发生器来检测最短响应脉冲
- 加中继器测量延时
- 用锁存器保存状态
- 为了测定延时标度，在旁边搭一个对照器
- 这个中继器作为基准延时

仿照左侧搭建，只不过中间用红石线（0延时）替代，这样这要在基准延时上加延时，就可以得到异或门的延时。
- 经过测试，加一个 4t 基准延时

外加 2t 延时
- 经过测试，2t 延时是合适的

果真是这样吗？如果是输入两个1呢？
- 错了。应该是 0。
- 出错的原因很简单：红石火把有延时
- 它先输出1，再迅速修正为0。

调整 4t 延时，结果正确
- 然而这样的调整导致输入一个 1 时错误
- 因此我们再做一些调整：把脉冲调为 3t，延时调整为 5t。
- 这样，对所有输入都是正确的。

### 因此我们总结，

异或门：（5/5/3），并伴有异常脉冲（可见其性能是很差的）

 **因此要做无延时！** 

### 什么是无延时？

中继器和红石火把有固有延时。但是活塞推出瞬间，其前方方块会变为透明方块，而透明方块不会阻碍红石信号传递。这就是无延时。

令人意外的是（并不），无延时其实是一项上古技术。具体可以参考 defanive2 的帖子。他也是较早（如果不是第一个的话）提出这一技术的人。

- 基于透明方块效应的上升沿无延时中继器：参数（0/1/2）

- 下降沿无延时中继器，红石火把只能对不透明方块充能，参数（1/0/2）

- 双边无延时中继器（设计来自 defanive2 致敬D大），利用红石火把的延时特性将前两种设计融合，天才般的设计，参数（0/0/2）

- 双边无延时中继器，利用了活塞更新和 BUD 特性（没有找到出处，如知道请留言）与前一个相比，体积小，但性能略差，参数（0/0/4）

- 上升沿无延时非门，参数（0/1/2）

- 下降沿无延时非门，参数（1/0/2）

- 双边无延时非门（设计上参考了双边无延时中继器的设计），参数（0/0/2）

  > 我们称性能达到（0/0/2）的元件是无副作用的 （No Side Effect）。因为我们几乎可以任意地将它们插入电路而不影响原有的同步或脉冲设计。

- 上升沿无延时与门（简单利用了活塞通断特性），参数（0/1/2）

- 上升沿无延时与非门，参数（0/1/2）

  > 与非门：p NAND q = ┐(pΛq) （参看真值表：001、011、101、110）

- 在与非门基础上搭建上升沿无延时异或门，因此只要在全0输入时阻断输出即可，理论参数（0/1/2），实际参数（0.5/1/2）

  > 当两端同步输入1时，输出端产生 0.5t 脉冲

- 下降沿无延时异或门，采用了一些 BUD 原理，我们将在补码器中见到它。参数（1/0/2）

  > 输入一个1，结果为1,，输入两个1，结果为0；无输入，输出0


下集预告：全加器

## [第3集：全加器](https://www.bilibili.com/video/BV1y7411j7oi)

### 原理

全加器是计算机的基础部件，通过逻辑单元计算两个无符号二进制数的和全加器由两个半加器组成。先附上原理图，再做解释。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0711/165102_66b8413e_5631341.png "屏幕截图.png")

> 半加器 & 全加器

考虑两个数相加

| A | B | A+B |
|---|---|---|
| 0 | 0 | 0 |
| 0 | 1 | 1 |
| 1 | 0 | 1 |
| 1 | 1 | 10 |

用 S 表示原位， C表示进位

| A | B | C | S |
|---|---|---|---|
| 0 | 0 | 0 | 0 |
| 0 | 1 | 0 | 1 |
| 1 | 0 | 0 | 1 |
| 1 | 1 | 1 | 0 |

可见：S=A⊕B，C=AΛB

对照异或门和与门的真值表

| A | B | C | S | A⊕B | AΛB |
|---|---|---|---|---|---|
| 0 | 0 | 0 | 0 | 0 | 0 |
| 0 | 1 | 0 | 1 | 1 | 0 | 
| 1 | 0 | 0 | 1 | 1 | 0 | 
| 1 | 1 | 1 | 0 | 0 | 1 |

我们便得到了包含原位与进位的半加器

![输入图片说明](https://images.gitee.com/uploads/images/2022/0711/165931_e2230514_5631341.png "屏幕截图.png")

考虑竖式加法 11+11，

![输入图片说明](https://images.gitee.com/uploads/images/2022/0711/170109_6b430e9a_5631341.png "屏幕截图.png")

计算低位得 10，

![输入图片说明](https://images.gitee.com/uploads/images/2022/0711/170225_d542c872_5631341.png "屏幕截图.png")

第二位为 1+1+1，因此至多进位1

![输入图片说明](https://images.gitee.com/uploads/images/2022/0711/170352_65797fd9_5631341.png "屏幕截图.png")

以此类推，任何一位进位至多为1

将两个半加器组合起来，分别处理原位加法和进位加法，产生两个进位 C1，C2

![输入图片说明](https://images.gitee.com/uploads/images/2022/0711/170546_effc91e6_5631341.png "屏幕截图.png")

因为每一位至多进位1，因此C1，C2任何一个有进位都表明该位进位为1，因此用或门连接，得到全加器（full adder）

![输入图片说明](https://images.gitee.com/uploads/images/2022/0711/170740_8e205e55_5631341.png "屏幕截图.png")

### 搭建全加器

搭建上一集的异或门，X2 速度播放，在此基础上加一个与门变成半加器，回顾上一集，这个异或门是搭在与非门上的，通过布线，把与非端输出引出来。再加上一个无延时非门，变成与门。

半加器完成。（左）S，（右）C

在S位基础上再搭一个异或门处理低位进位，（X2速度播放），金块是输出端。

类似地，从这个异或门中引出与非门，配合无延时非门变成与门，再用或门与前一个半加器的进位连接起来。

全加器完成，这是进位端，金块则是输出端。

可以使用 /clone 指令复制方块，但是 /clone 指令并不方便，而且不能撤销。

（实现用蓝色方块标注，要复制的区域，点选）

- /clone 458 7 -1548 465 14 -1544 458 7 -1544
  > 源坐标和目标坐标不能重叠
- /clone 458 7 -1548 465 14 -1544 458 7 -1543
  > 已复制 320 个方块

（删除标志用的方块）

为了让工作轻松一些，我倾向于使用 WorldEdit 插件。这样，复制、移动和清除方块会容易很多。

- first position set to (458.0, 18.0, -1548.0) (72).
- Second position set to (465.0, 6.0, -1545.0) (256).

 _因为 WE 不提供新的特性和方块，因此它不妨碍这个系列成为 “原版教程”。毕竟，使用 /clone 指令或者直接手动搭也能达到同样的效果。如需要，可以在网上找对应版本 WorldEdit 下载。需要安装 Forge。_ 

复制另一组半加器，加上溢出端（最高位的进位端），加上最低位输出。因为最低位不需要处理更低进位，所以一个半加器就可以。

这样，一个8位全加器便完成了。

延时分析：上升沿是0t；下降沿，与非门是 1t，进位线上的非门是 1t，因此一个半加器累积 2t 延时。两个半加器构成全加器，下降沿延时累积为 4t，参数（0/4/2）

让我们仔细看看。线输入一组信号。（X5慢放。可以看到活塞逐个收回。）

### 还能更快吗？

我们希望在输入结束后自动断开两组半加器之间的联系。这样，两组半加器分别复位，速度更快。我们采用最简单的活塞断路，并配合无延时非门。能保证瞬间通路，并花费 2t 时间断路。

复制一下。

我们可以把非门换成无副作用的（双边无延时）这样断路延时减为 1t 。

为了紧密排布，布线方面有一些技巧。需要严格控制红石线朝向。

复制以下。

回顾上一集，搭一个检测器。在 +6t 基准延时 后锁存信号。输入端，输入 2t 脉冲，钻石和青金石分别代表两个二进制数。中继器表示这一位输入1，否则输入0。

测试以下。输入 111+000，结果是 011。错了。

为什么呢？我们来看看第三位发生了什么。（X5慢放，我们发现右边活塞比左边收回得慢）。发生这种状况，多半是因为 BUD （方块更新检测）。

我们将这个石英块换成半砖，避免 BUD。重新测试。结果正确。进行几组测试。

- 111+001（7+1），结果是 1110。错了，应该是 1000（8）。

  > 这是因为输出端产生了 0.5t 脉冲。因此我们再加 1t 延时。输出 1000，正确。（从右向左读）

- 尝试一下极端情况。11111111+00000001

  > 现实中采用超前进位技术解决这一问题。 Mi	necraft 中也可以使用无延时电路做伪超前进位。这个全加器，从一开始就是无延时的，不必考虑这一点。 **这为同步电路的设计提供极大便利** 。

  - 输出 100000000。正确

### 总结：全加器（0/3/2）

上次提到，当两个高电平同时输入异或门时，会产生 0.5t 脉冲输出。

这是因为，再全部活塞同时收回的瞬间，尽管方框内红石线失去信号源，但它仍是亮的，所以会传递 0.5t 信号给右侧红石。

给异或门加上断路器和检测器。断路器自然就是全加器上的断路器。

+4t 基准延时。先敲掉断路器测试以下。
- 同步输入两个 2t 脉冲。结果是1。错误。

加上断路器
- 结果为0。正确。

因为活塞比红石线慢，因此 0.5t 脉冲被阻断了。我们可以说，加上断路器后，异或门是严格的（0/1/2）。

> 异常脉冲处理，我们还会遇到。

下集预告：减法与补码

---

 **其实没有想象中那么难。从难度来讲，基本低于搭建一台红石CPU。“神级”作品可以参考 [BV1U34y1X7cZ](https://www.bilibili.com/video/BV1U34y1X7cZ)** 

# [【Minecraft】这是你从未见过的1Hz超强红石电脑](https://www.bilibili.com/video/BV1U34y1X7cZ)

![输入图片说明](https://images.gitee.com/uploads/images/2022/0710/152918_d5bf0b63_5631341.png "屏幕截图.png")

https://www.youtube.com/watch?v=FDiapbD0Xfg  
CHUNGUS 2: Electric Boogaloo - 一台可以运行俄罗斯方块、贪吃蛇、connect4、图形渲染程序等的CPU

CHUNGUS即Computational Humongous Unconventional Number and Graphics Unit（新型大型计算型数字图像处理单元） by Sammyuri.
这个CPU很大
为了在如此大的体积下做到10tick的时钟周期，这个CPU使用到了诸如流水线、自动缓存数据和简单的分支预测的技术

`-------------------------------------------------------------------`

CPU参数
- 8位数据位宽，16位固定指令长度
- 1Hz时钟，4级流水线（取值-译码-执行-写回）
- 64字节全关联cache，8个cache块256字节内存
- 最多256个可寻址的I/O端口
- 7个通用寄存器
- ALU指令超过40条，包括硬件桶移、乘法器、除法器和开方运算器
- 128字节程序指令页面，一共32页，共计4KiB的指令内存

视频中使用到的其他硬件
- 带有缓冲的32x32点阵屏
- 12x2 ASCII字符显示器
- 2x8bit 整数显示器（可显示有符号数或无符号数）
- 8输入的NES风格的控制器
- 3bit 伪随机数生成器

`-------------------------------------------------------------------`

链接：  
指令集 - https://docs.google.com/spreadsheets/...  
汇编器和实例程序 - https://github.com/sammyuri/chungus-2...

`-------------------------------------------------------------------`

完整的CPU解析和每个程序的独立展示和解析将会在之后到来  
想要亲眼看看这个CPU？进入mc.openredstone.org服务器，输入/build和/warp CHUNGUS2  
实例程序的运行使用MCHPRS录制，MCHPRS是一个可以加速红石高达180倍的服务器，它对于一些运行很慢的游戏（比如俄罗斯方块）在现实时间下可玩十分重要  
https://github.com/MCHPR/MCHPRS  
使用Replay Mod录制程序运行

`-------------------------------------------------------------------`

0:00 介绍  
0:43 展示  
2:42 汇编器  
3:02 程序运行

背景音乐: MDK - Jelly Castle (Evan King retro mix)

https://www.kidzsearch.com/kidztube/chungus-2-a-very-powerful-1hz-minecraft-cpu_d90f24159.html

## [CHUNGUS 2 - A very powerful 1Hz Minecraft CPU](https://www.kidzsearch.com/kidztube/chungus-2-a-very-powerful-1hz-minecraft-cpu_d90f24159.html)

DESCRIPTION  
CHUNGUS 2: Electric Boogaloo - A Minecraft CPU capable of running Tetris, snake, connect 4, graph rendering... and more!

CHUNGUS stands for Computational Humongous Unconventional Number and Graphics Unit by Sammyuri.  
The CPU is also very large.  
In order to achieve a 10 tick clock speed despite its enormous size, the CPU makes use of techniques such as an instruction pipeline, automatic data cache and simple branch prediction.

`-------------------------------------------------------------------`

CPU specs
- 8 bit data, 16 bit fixed size instruction length
- 1Hz clock speed, 4 stage instruction pipeline (fetch - decode - execute - writeback)
- 64 byte automatic 8-way associative data cache and 256 bytes RAM
- Up to 256 addressable I/O ports
- 7 general purpose registers
- Over 40 ALU functions, including a hardware barrel shifter, multiplier, divider and square rooter
- 32x128 byte program pages for a total of 4KiB program storage

Other hardware used in video
- 32x32 buffered pixel screen, draw and erase pixels, rectangles, up to 8 4x4 sprites
- 12x2 character ASCII text display
- 2x 8-bit integer display (signed or unsigned)
- 8-input NES-style controller
- 3 bit pseudo-RNG

`-------------------------------------------------------------------`

Links  
Instruction set - https://docs.google.com/spreadsheets/d/10_ZERVmsKr0uqQXXbHxMQW-aBpHn6tl5L6Mn-zm57O4  
Assembler and example programs - https://github.com/sammyuri/chungus-2-assembler

`-------------------------------------------------------------------`

Full CPU explanation as well as individual program showcase and explanation coming soon™  
Want to see the CPU itself? Hop onto mc.openredstone.org, head to /build and teleport to /warp CHUNGUS2

Example programs were recorded on MCHPRS, a server that speeds up redstone by up to 180x, which proved essential to make some of the slower games (such as Tetris) playable in real time.  
https://github.com/MCHPR/MCHPRS  
Replay mod was also used to record programs running.

`-------------------------------------------------------------------`

0:00 Intro  
0:43 Showcase  
2:42 Assembler  
3:02 Programs

Music: MDK - Jelly Castle (Evan King retro mix)  
I do NOT own the music

#Minecraft #Redstone

### clone & fork

- https://gitee.com/RV4Kids/MCHPRS
- https://github.com/RV4Kids/chungus-2-assembler

### 关于部分问题的回复：

1. CPU的原作者是欧洲红石大佬sammyuri，sammyuri精通数电和械电，拥有ORE服务器engineer组权限和多项械电活塞门世界记录，我和sammy为朋友关系，该视频的属性为授权转载，我已事前告知sammy并取得同意，我只对视频进行了翻译和字幕添加的工作
2. 视频中的演示内容都进行了加速，倍数从数百倍到几千倍不等，程序运行在MCHPRS(Minecraft High Performance Redstone Server)之上，MCHPRS是使用rust编写的专门针对数电红石进行优化的服务端，能够加速红石运行速度高达180倍，服务端的跳转链接在简介里
3. sammy表示他不会打算为这个CPU录制教程，主要是考虑到该CPU技术性相对较高，讲解难度较大，尤其是面对没有任何体系结构基础的观众；此外，sammy目前正在为这个CPU制作另一个程序，使用到了一个大型的显示器，他表示该程序会比曼德勃罗集更加震撼，有关于该程序的具体信息会在新视频发布时揭晓；我也会在寒假和sammy合作一个红石项目，敬请期待
4. sammy没有在原视频给出存档链接，故我也不会给出存档链接，该CPU在ORE服务器内(mc.openredstone.org)，进服后输入/build前往build子服再输入/warp CHUNGUS2即可观摩该CPU
5. 对红石数电感兴趣的朋友，也欢迎加入另一位红石大佬 @辰占鳌头 的群1076278498 一起来学习交流红石

## 【Minecraft】[这是你从未见过的1Hz超强红石电脑，字幕](https://gitee.com/RV4Kids/StochasticNet/issues/I5G9TZ#note_11551468_link)

> NSSPU(prototype)

7months of work...
Years of dreaming...
What can redstone do...
when pushed to the limit?

七个月的付出，数年以来的梦想，红石究竟能达到什么地步？当我们把它运用到极致。

sammyuri presents... 
Computational Humongous Unconventional Number and Graphics Unit by Sammyuri 2
CHUNGUS2

为您带来... CHUNGUS2

计算型 大型 新型 数字 图像 处理单元 （由 sammyuri 制作）第二代。

### 参数性能：

Overall specs
- 10 tick(1HZ) clock
- 4 stage instruction pipeline: fetch, decode, execute, writeback
- 3-operand RISC architecture
- 16 bit instructions with 5 bit opcode
- 67 unique instructions
- instruction set designed for ease of programming

总体参数
- 10tick 时钟长度
- 4级流水线：取指、译码、执行、写回
- 3操作数的精简指令集架构
- 16位指令长度，5位操作吗
- 67条指令
- 以简化程序设计为目标而设计的指令集

Registers
- 7 general purpose dual read registers (R1-R7)
- R0 (zero register) reads constant zero, writes are ignored
- All registers can be used as pointer

寄存器
- 7个通用双读端口寄存器（R1-R7）
- R0 (零寄存器) 读取的值始终为0，无法写入
- 所有的寄存器可以被用作指针

ALU
- Add or subtract (with carry) and all bitwise operations
- Barrel shift left, shift right, rotate right, arithmetic shift right
- Many operations support immediates

算术逻辑单元
- 加法、减法运算（包含进位）和所有位运算
- 桶式左移、桶式右移、循环右移、算术右移
- 大部分运算都支持立即数

Flags and Branch logic
- 3 flags: carry (or overflow), zero and even
- 13 unique branch conditions
- Simple branch predictor - 1 cycle faster on correct prediction

标志位和分支逻辑：
- 3个标志位：进位（溢出）、零、相等
- 13种不同的分支条件
- 简单的分支预测器 - 预测成功加速1周期

Big math units
- Multiply (16 bit product), divide, module, square root
- Count leading zeroes, trailing zeroes, count ones
- Stalls between 1 and 6 cycles depending on operation

大型数学运算单元
- 乘法（16位结果）、除法、取模、开方
- 计算二进制位中前导0，末尾0或1的个数
- 根据运算的类型停顿1至6个周期

I/O Ports
- Support for up to 256 ports (8 built-in to the CPU )
- Stalls on port load
- Ports can be addressed using pointer

输入/输出端口
- 最多支持256个端口（CPU中内置8个）
- 在读端口时会产生停顿
- 端口可以用指针寻址

RAM
- 256 bytes of data RAM
- Stored as 8 byte binary serial for high density
- Can load to RAM from an external drive

内存
- 256字节数据内存
- 每8字节作为一个单元串行存储，以实现高存储密度，
- 可以通过外置驱动向内存加载数据

Data cache
- 64 byte data cache, instant read/write on cache hit
- Fully automatic 8-way LRU cache controller
- Stalls for 5 cycles on cache miss

数据缓存
- 64字节数据缓存，缓存命中时没有流水线停顿
- 使用 LRU 算法的全关联缓存控制器
- 缓存未命中时停顿 5 周期

Cache controller
- 64 byte data cache, instant read/write on cache hit
- Fully automatic 8-way LRU cache controller
- Stalls for 5 cycles on cache miss

缓存控制器
- 64字节数据缓存，缓存命中时没有流水线停顿
- 使用LRU算法的全关联缓存控制器
- 缓存未命中时停顿5周期

Stack pointer
- Stack access is the same speed as regular memory access
- Supports push, pop, push/pop + immediate without updating SP
- Can also add/subtract/set stack pointer to immediate

栈指针
- 栈空间的访问速度和访存速度相等
- 支持压栈、出栈、在不更新栈指针的情况下出栈并压入立即数
- 支持将栈指针加上、减去、设为立即数

Instruction cache
- 128 byte instruction cache, split into 2x64 bytes
- Reads two bytes(one 16bit instruction) at the same time
- Page swaps stall for 10 clock cycles

指令缓存
- 128字节指令缓存，分成2组，每组64字节
- 同时读取两字节（一条16位的指令）
- 页面切换停顿10周期

Program counter
- Conditional branching within a page
- Automatically detects when page swaps are required
- hardware loop counter accelerates fixed-lenght loops

程序计数器（PC）
- 支持当前页面内的条件分支
- 自动检测页面切换
- 硬件集成循环计数器，可用于加速定长循环

Call stack
- Allows recursion up to 31 deep
- Same speed as regular unconditional jump
- Does not waste space in RAM

函数调用栈
- 支持31级递归深度
- 和常规的无条件跳转速度相等
- 避免了内存空间的浪费

Control unit
- PLA decoder for more commonly used control lines
- Torch ROM for ALU control lines allows all bitwise logic
- Placement in the middle of the CPU means lower latency

控制单元
- 常用的控制线路使用可编程逻辑阵列
- ALU 的位运算控制线使用火把ROM
- 放在CPU的中部意味着更低的延时

And of course ...
Control lines spanning every corner of the CPU

并且，毫无疑问，控制线路覆盖了CPU的每个角落。

CHUNGUS 2 supports up to 4KiB of attachable program memory
（That's as much as an Atari 2600）

支持最多 4kib 的附加指令内存
（这和 Atari 2600 的内存一样大）

This allows you to run very large programs...
far more complex than what would be expected of a Minecraft CPU.

这使得大型程序能够得以运行
大到远远超过人们对于 Minecraft CPU 的期待

### external assembler 外置的汇编器

In order to create such programs...
A full external assembler
The assembler generates a schematic that can be pasted directly into the program memory
（Allowing you to assemble and run programs in seconds）

为了编写这样的程序
一个完全外置的汇编器
这个汇编器能够生成可以直接粘贴进指令内存的 SCHEM 文件
（使得您可以极快地编写汇编代码并且运行程序。）

### So what programs can it actually run? 它究竟可以运行什么程序？

Many Minecraft CPUs are limited by their memory. But not the CHUNGUS 2.

- Tetris (*score display broke due to bug, has since been fixed.)
- Snake
- Breakout
- Connect 4
- Graph rendering
- Conway's Game of Life
- Mandelbrot Set (20 iterations)

许多 Minecraft CPU 受限于它们的内存。但是 CHUNGUS2 并不在其列

- 俄罗斯方块 （计分板由于 Bug 坏了，在之后它被修复了）
- 贪吃蛇
- 打砖块
- 四子棋
- 图形渲染
- 康威生命游戏 
- 曼德勃罗集（20次迭代）

All these programes ran on exactly the same hardware. The Only thing that changed was the program itself.

How much more is possible? Try programming it yourself! Links to the assembler and example programs in the description.

Music: MDK - Jelly Castle (Evan King retro mix)

所有这些程序运行在相同的硬件之上，唯一不同的只是程序本身，还有多少可能？试着你自己写一个程序！汇编器和示例程序的链接在简介中。

Want to check out the CPU itself? Head on over to the ORE Minecraft server (mc.openredstone.org)

Massive thanks to the ORE community for helping me learn about how computer architectures work and supporting me along this journey!

想亲自接触 CPU? 请前往 ORE 服务器（mc.openredstone.org）十分感谢 ORE 社区在我学习计算机体系结构时提供的帮助和一直以来的支持。

Thanks for watching!
感谢您的观看！

---

# 【[笔记](https://gitee.com/RV4Kids/StochasticNet/issues/I5G9TZ)】我的世界红石计算机教程1

[我的世界红石计算机教程1,《我的世界》红石电脑制作原理及使用教程](https://blog.csdn.net/weixin_36294980/article/details/118628306)

<p><img width="38.8%" src="https://img-blog.csdnimg.cn/img_convert/53a46af3a784d94fc0e7f9acb74653ce.gif"> <img width="43.9%" src="https://images.gitee.com/uploads/images/2022/0709/020449_1d99b71e_5631341.gif"></p>

- [在《我的世界》里从零打造一台计算机有多难？复旦本科生大神花费了一年心血...](https://blog.csdn.net/yH0VLDe8VG8ep9VGe/article/details/90357077)

  本期封面的原型就是这个。

  - 现已开源存档，并制作了一个全新的数字电路教学存档，都放到github上，地址在 https://github.com/Alpha21016/Minecraft-Digital-Circuit

    <p><img height="99px" src="https://images.gitee.com/uploads/images/2022/0709/035535_57b287a1_5631341.jpeg"> <img height="99px" src="https://images.gitee.com/uploads/images/2022/0709/105928_581191ef_5631341.jpeg"></p>

    [封面和红石开源教程，就在本期片头一样！](https://gitee.com/RV4Kids/StochasticNet/issues/I5G9TZ#note_11518442_link)

  - [@辰占鳌头 的合集和视频列表从零开始搭一台计算机 Minecraft](https://space.bilibili.com/172508474/channel/seriesdetail?sid=1804891)

    - [【Minecraft】从零开始搭一台计算机！附二——“满五加三”算法的原理](https://www.bilibili.com/read/cv4942059) 【[笔记](https://gitee.com/RV4Kids/StochasticNet/issues/I5G9TZ#note_11526072_link)】
    - [【Minecraft】从零开始搭一台计算机！附一——上升沿无延时逻辑门理论分析](https://www.bilibili.com/read/cv4868223) 【[笔记](https://gitee.com/RV4Kids/StochasticNet/issues/I5G9TZ#note_11526125_link)】
    - [从零开始搭一台计算机 Minecraft](https://space.bilibili.com/172508474/channel/seriesdetail?sid=1804891) 【[笔记](https://gitee.com/RV4Kids/StochasticNet/issues/I5G9TZ#note_11525596_link)】
      - [【Minecraft】从零开始搭一台计算机！第0x11集：串行乘法优化](https://www.bilibili.com/video/BV1ay4y1s7LQ) 【[摘要](https://gitee.com/RV4Kids/StochasticNet/issues/I5G9TZ#note_11527864_link)】
      - [【Minecraft】8位红石CPU(自制)](https://www.bilibili.com/video/BV1Fz4y1D78W) 【[摘要](https://gitee.com/RV4Kids/StochasticNet/issues/I5G9TZ#note_11527885_link)】

      <p><img height="99px" src="https://images.gitee.com/uploads/images/2022/0710/160304_45ace35b_5631341.png"> <img height="99px" src="https://images.gitee.com/uploads/images/2022/0710/160702_de5cf8e0_5631341.png"> <img height="99px" src="https://images.gitee.com/uploads/images/2022/0710/170717_211dd2f0_5631341.png"></p>

    - [【Minecraft】历时3天建造全站最小红石四则计算器 (32位)](https://www.bilibili.com/video/BV1Gf4y1J7eb)
    - [【Minecraft】全站首个红石汉字编码全像素显示屏 (4K)](https://www.bilibili.com/video/BV1wP4y1s7jy)
    - [【红石电路】在Minecraft中打印Hello,world! 红石显示器](https://www.bilibili.com/video/BV1aJ411E76G)

      <p><img height="99px" src="https://images.gitee.com/uploads/images/2022/0710/110042_f288442c_5631341.png"> <img height="99px" src="https://images.gitee.com/uploads/images/2022/0710/111250_c6b20609_5631341.png"> <img height="99px" src="https://images.gitee.com/uploads/images/2022/0710/111823_fa55c23f_5631341.png"></p>

  - [用几个月时间做出的纯红石Bad Apple 第四代！！](https://www.bilibili.com/video/BV1CZ4y1C7iB)

    <p><img height="99px" src="https://images.gitee.com/uploads/images/2022/0710/154025_92f7f323_5631341.png"></p>

    **更详细的介绍** ：

    > 纯红石Bad Apple第四代分辨率为256×192像素，帧率30fps，相比上代再次将像素数量提升4倍。视频效果并非实时录屏，实际制作时由于机器运行过慢，需要一帧一帧截图再连起来才能得到视频相较于前三代的22方块每像素，这代将单个像素面积缩小到了一格，否则1922=384将超出世界限高（第四代开始设计时1.18正式版尚未发布）。同时，屏幕的控制方式由通过有无红石信号控制像素亮灭改为通过脉冲信号切换像素亮灭，这使得侦测器可以用于信号传输，在节约空间的同时还能大大降低渲染负载数据存储与读取原理和第三代一致，即使用雪球和石头代表数据中的0和1（上代“1”代表某像素在这一帧亮起，本代“1”代表某像素亮/灭状态在这一帧发生改变），雪球和石头使用潜影盒+箱子套娃存储，箱子及潜影盒中的物品非手动放置，而是使用函数放置带nbt的方块实现，函数文件由自研python程序根据画面生成。读取时雪球被砸烂，石头被水冲到压力板上触发信号。存储单元被完全重新设计，体积较上代缩小70.6%，同时输出信号也改为经侦测器传输的正脉冲以适应屏幕上的改变第三代在工作模式为数据异步读取 画面同步刷新（当时没有介绍），本代规模提升4倍，为优化性能进一步将画面改为异步刷新，即屏幕上的不同部分不同时切换到下一帧，只不过最终成品是一帧一帧截图再拼起来的，所以看不出画面不同位置不同步。异步读取 刷新的设计一方面是为了优化性能，另一方面，根据minecraft wiki的资料，java版Minecraft最多在1gt中执行65536个计划刻，而本作仅像素数量就达到49152，若不同部分同时运行，则必定超过计划刻最大数量，导致不可预知的后果，出于这一点，即使没有性能问题也必须使用异步读取 刷新的设计由于运行过慢且需要逐帧截图，本作不依靠时钟电路进行播放，而是由程序操作，自动在输入端的侦测器前制造方块更新并在画面刷新完成后截图。每一帧需要约6~7分钟完成播放并截图，整个视频共计6573帧，仅播放就需消耗数百个时，历史数月完成（不是一次性放完的，所以实际耗时有几个月，毕竟我不可能一个月不用电脑，期间也要更新其他视频）

    - 第一代：[【纯红石播放bad apple】当你做出一个能播放bad apple的红石机器！](https://www.bilibili.com/video/BV18A411M7Q8)
    - 第二代：[【纯红石播放Bad Apple第二代！】更清晰 更流畅](https://www.bilibili.com/video/BV1dA411N7vN)
    - 第三代：[【纯红石播放Bad Apple第三代】1.2万像素、30fps！！](https://www.bilibili.com/video/BV1NU4y187z3)

  - [[红数搬][powsi]体积非常小的模电数字显示器](https://www.bilibili.com/read/cv7433954)

    <p><img height="99px" src="https://images.gitee.com/uploads/images/2022/0710/184250_7cdeb958_5631341.png"> <img height="99px" src="https://images.gitee.com/uploads/images/2022/0710/184307_1dd1ef10_5631341.png"> <img height="99px" src="https://images.gitee.com/uploads/images/2022/0710/184312_16ec7d07_5631341.png"> <img height="99px" src="https://images.gitee.com/uploads/images/2022/0710/184806_ecf8adc8_5631341.png"></p>

  - [我的世界：基岩版红石科技教学，3年老玩家教你，快速成红石大神](https://new.qq.com/omn/20201226/20201226A0AIHH00.html)

    <img src="https://images.gitee.com/uploads/images/2022/0710/190810_848b3400_5631341.png">

  - [16位红石计算机，具有配套的编译器与类汇编语言](https://www.bilibili.com/video/BV1o5411V7V9)

    <img height="99px" src="https://images.gitee.com/uploads/images/2022/0710/233112_9837f623_5631341.png"> <img height="99px" src="https://images.gitee.com/uploads/images/2022/0710/233222_6899d169_5631341.png">

    - 红石计算机CPU名单：

      1. [【MC超级工程】你从未见过的4核红石电脑](https://www.bilibili.com/video/BV13t4y1Q7a5)
      1. [【MC超级工程】 - 蓝石计算机 BlueStone Computer](https://www.bilibili.com/video/BV1i54y127Qb)
      1. [QCPU - 1.25Hz主频的8位处理器CPU，在MC中打造！！！！！！！！！！！！！！！！！！！](https://www.bilibili.com/video/BV1kk4y11761)
      1. [鸡翻字幕][9年前就可以做红石CPU？？据我所知最早的红石CPU：来自9年前的上古红石CPU（p2有运行演示）](https://www.bilibili.com/video/BV1H54y1i7bC)
      1. [MC][比活塞门还薄的红石CPU！！！！](https://www.bilibili.com/video/BV1Rg4y1b7Qe)
      1. 【延迟摄影】[带你20分钟看完整个8位红石计算机的建造过程！！！！！](https://www.bilibili.com/video/BV17Z4y1T7Cf)
      1. [介系你从未见过的船新64位红石电脑](https://www.bilibili.com/video/BV1sW411c7nF)
      1. [我的世界][TheRedPixel] [32位红石电脑 RSC-3230](https://www.bilibili.com/video/BV1Qs411z7h5)
      1. [你们要的红石CPU运行视频教程？](https://www.bilibili.com/video/BV1sW411a7zg)
      1. 【minecraft】[16位红石CPU](https://www.bilibili.com/video/BV1is411X7xT)
      1. 【Minecraft】[可移动红石CPU](https://www.bilibili.com/video/BV11Z4y1j7AW)
      1. 【minecraft】[16位红石CPU一块](https://www.bilibili.com/video/BV1YW411m75X)
      1. 【ERC】[16位红石CPU](https://www.bilibili.com/video/BV1Ex411g7ag)
      1. [16位无延时简易红石电脑](https://www.bilibili.com/video/BV1ti4y1E7qS)
      1. [我的世界/红石][RX0308mc八位红石CPU](https://www.bilibili.com/video/BV1J441167ST)
      1. 【我的世界/红石CPU】[8位红石CPU](https://www.bilibili.com/video/BV1NW411D75h)
      1. [simd架构八位红石cpu](https://www.bilibili.com/video/BV1E7411o7gx)
      1. [4位纯数电红石静音CPU](https://www.bilibili.com/video/BV1FE411c7Ut)

    - 教材

      1. [《红石逻辑电路: 从入门到精通》第一部分：红石数字电路基础（正式版）](https://www.bilibili.com/read/cv6754204)
      1. [《红石逻辑电路：从入门到精通》第二部分（内容写完了，勘误中）2020年10月6日更新](https://www.bilibili.com/read/cv7256083)
      1. [红石小白也要做CPU! - 直接教你做个最简单的红CPU! 甚至5分钟就能做出来!](https://www.bilibili.com/read/cv6489321)
      1. [魔法般的红石加法器 - 封闭进位加法器CarryCancelAdder  : 基本阐述与进位原理解析](https://www.bilibili.com/read/cv6437186)
      1. [在魔法里施法 - 更好的封闭进位加法器(目前最优方案)](https://www.bilibili.com/read/cv6578989)

    - 【延迟摄影】[带你20分钟看完整个8位红石计算机的建造过程！！！！！] 6. (https://www.bilibili.com/video/BV17Z4y1T7Cf)
    -  [介系你从未见过的船新64位红石电脑](https://www.bilibili.com/video/BV1sW411c7nF) 7.

      <p><img width="13.69%" src="https://images.gitee.com/uploads/images/2022/0710/233413_6c6f728d_5631341.png"> <img width="13.69%" src="https://images.gitee.com/uploads/images/2022/0710/233512_f7442dae_5631341.png"> <img width="13.69%" src="https://images.gitee.com/uploads/images/2022/0710/233642_b6dfec33_5631341.png"> <img width="13.69%" src="https://images.gitee.com/uploads/images/2022/0710/233742_b0ec2dd6_5631341.png"> <img width="13.69%" src="https://images.gitee.com/uploads/images/2022/0710/233826_20553c31_5631341.png"> <img width="13.69%" src="https://images.gitee.com/uploads/images/2022/0710/233920_8adae8f0_5631341.png"></p>

      > 终于填完了这个史诗巨坑QWQ  
存档在这↓↓要用mcedit打开  
链接：/s/1NRn-97cA1-W_p-tD1jeNqg 密码：53oq  
整个存档无命令方块

      > 原帖地址：http://tieba.baidu.com/p/5752882167  
模块介绍：http://tieba.baidu.com/p/5751797329  
存档用法：某64位红石计算机的使用说明  
这块cpu的字长是64位qwq  
用的是哈佛架构，程序存储器堆了2kb，数据存储器堆了1kb，总共3kb运存  
指令集稍微借鉴了一下arm  
这块u比较有特色的地方就是它实现了7级指令流水线  
而且它还能支持指令乱序发射  
这得益于一种叫Tomasulo的算法  
想看运行效果的话请直接跳到2:00_(:3」∠)_

      > 高端1m制程，1hz高主频，支持mcx86指令集，avx指令集，支持mctv-x虚拟化技术，超线程技术，Minecraft睿频加速技术，tdp低至1kw，你值得拥有

---

### 【[笔记](https://gitee.com/RV4Kids/Handwritten-digital-translation/issues/I5GD92)】手写数字识别数据集 Mnist

- [手写数字识别数据集 Mnist](https://www.jianshu.com/p/d282bce1a999)

  - [手写数字识别 python pytorch 一 之训练](https://blog.csdn.net/weixin_44021118/article/details/107852466)
  - [手写数字识别 python pytorch 二 之手写数字预测](https://blog.csdn.net/weixin_44021118/article/details/107853422)

  ![输入图片说明](https://images.gitee.com/uploads/images/2022/0711/011717_2bbe93c3_5631341.png "屏幕截图.png") = 6

  <p><img height="99px" src="https://images.gitee.com/uploads/images/2022/0711/011027_b623eb29_5631341.png" title="MNIST数据集介绍及读取"></p>

  - [随机深度神经网络？](https://www.zhihu.com/question/53371907)
  - [权重是多少? ](https://gitee.com/RV4Kids/Handwritten-digital-translation/issues/I5GD92#note_11530075_link)
  - [创世神mod导出的schem的文件该如何使用](https://www.mcbbs.net/thread-1082802-1-1.html) 【[笔记](https://gitee.com/RV4Kids/Handwritten-digital-translation/issues/I5GD92#note_11530081_link)】
  - [SC-DCNN: Highly-Scalable Deep Convolutional Neural Network using Stochastic Computing](https://dl.acm.org/doi/10.1145/3037697.3037746)
  - [SC-DCNN：高度可扩展的深度卷积使用随机计算的神经网络](https://gitee.com/RV4Kids/Handwritten-digital-translation/issues/I5GD92#note_11530093_link) | 摘要 | 引言 | 相关工作 | 结论 | 确认 |

  <p><img height="99px" src="https://images.gitee.com/uploads/images/2022/0711/024739_a2fdcb32_5631341.png"> <img height="99px" src="https://images.gitee.com/uploads/images/2022/0711/025353_4c00123a_5631341.png"> <img height="99px" src="https://images.gitee.com/uploads/images/2022/0711/025104_baaad4c3_5631341.png"> <img height="99px" src="https://images.gitee.com/uploads/images/2022/0711/030202_8606e889_5631341.png"></p>

  - [第0集 红石进阶](https://www.bilibili.com/video/BV1G7411g7KZ)
  - [第1集：基础逻辑门](https://www.bilibili.com/video/BV1j7411g7GE)
  - [第2集：性能评估与无延时逻辑门](https://www.bilibili.com/video/BV1P7411n79g)
  - [第3集：全加器](https://www.bilibili.com/video/BV1y7411j7oi)