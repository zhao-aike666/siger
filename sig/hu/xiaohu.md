![](https://foruda.gitee.com/images/1687977474779576109/6e6273bc_5631341.png)

在软件上花钱，中国公司才有机会

- https://www.huxiu.com/article/1090017.html

![](https://foruda.gitee.com/images/1687977556749131972/f6f30e0e_5631341.png)

没有开源，就没有中国互联网

- https://www.huxiu.com/article/1587677.html

> 我们新出了一期开源的视频，袁老师感兴趣可以看看呀[呲牙]

@yuandj 2023-05-22 11:39

> SIGER可以开一个频道 HU  
> 立个牌子给老师们，这是科技媒体的信仰  
> 我们不缺信仰，缺的是坚信  
> OPENAI不OPEN就走向歧途  
> 公益之心不分享了，就不叫志愿者啦  
> 媒体不能独立发声，就是缺乏公信力啦  
> 虎学值得青年追随，希望榜样不会变样  
> 坚持自己的价值观  
> 不能只翔瘦创新之名，不行创新之实  
> 看看这些庙堂的神，还有样嘛？  
> 什么叫有样学样[得意]  
> SIGER HU 目标培养小虎，未来的森林之王[得意]  
> 以此为开篇

11:49

> 回头请老师们写寄语，编辑实习小组就成立啦。  
> 回头给老师们投稿  
> 我们就叫 [SIGER 小虎](https://gitee.com/yuandj/siger/issues/I7GVHV)  
> SIGER 小虎的样子  
> 学生作品

[![](https://foruda.gitee.com/images/1688021076656386704/c2794f14_5631341.jpeg)](https://foruda.gitee.com/images/1688021285954641248/c598cdd9_5631341.jpeg)

### [虎学研究](https://www.huxiu.com/member/3198204.html)

| date | contents |
|---|---|
| 7:44 |  | 
| 2023-6-21 | 毕业了，然后呢？以后大学生的境遇会怎么样？ | 
|  | 以后大学生毕业了有哪些选择 | 
| 7:35 |  | 
| 2023-6-20 | 人不行，非赖多巴胺？ | 
|  | 多巴胺的意义，远不止欲望与奖赏 | 
| 15:51 |  | 
| 2023-6-16 | 中国科研的底气是什么？ | 
|  | 中国最厉害的理工类院校和专业都在哪里 | 
|  |  | 
| 2023-6-9 | 一个好老师，定义学术上限还是下限？丨虎学看片团 | 
|  | 高考结束，应该怎么选大学选专业？ | 
| 15:30 |  | 
| 2023-6-2 | 学术师承，对科研学术研究的影响有多大？ | 
|  | 名师就能出高徒吗？ | 
|  |  | 
| 2023-5-25 | 大厂拥抱开源，从微软开始丨 虎学看片团 | 
|  | 不搞开源是不行的 | 
| 16:04 |  | 
| 2023-5-19 | 没有开源，就没有中国互联网 | 
|  | 没有开源，就没有一切 | 
| 1:58 |  | 
| 2023-5-12 | 1分钟总结往期爆款，此时优秀的甲方应该…… | 
|  | 深浅适当，有底线，不掉价 | 
| 14:21 |  | 
| 2023-5-5 | 人类还管得了人工智能吗？ | 
|  | 人工智能的底线在哪，人类还能说了算吗？ | 
|  |  | 
| 2023-4-26 | 国产科学仪器被卡脖子，不是因为技术丨虎学看片团 | 
|  | 想要弯道超车，国产科学仪器需要做到这点 | 
| 14:26 |  | 
| 2023-4-21 | 90%依赖进口，国产科研仪器出路在哪？ | 
|  | 国产科学仪器如何绝地反击？ | 
|  |  | 
| 2023-4-13 | 被西门子们把控的中国工业软件大军，正在寻求破局 | 
|  | 国产工业软件，何时才能雄起？ | 
| 14:52 |  | 
|  | 在软件上花钱，中国公司才有机会 | 
|  | 工业软件为什么不行 | 
|  |  | 
| 2023-3-30 | 让AI说中文，总共分几步？ | 
|  | 让人工智能说中文真的有那么难吗？ | 
| 11:59 |  | 
| 2023-3-24 | 中文在人工智能大潮中注定落后吗？ | 
|  | 难也得整，要不然咋整 | 
| 14:20 |  | 
| 2023-3-10 | AI能成为医生吗？ | 
|  | 全球科技骤然转向，AI扮猪吃老虎，秒杀元宇宙 | 
| 15:21 |  | 
| 2023-2-24 | 癌症疫苗是智商税吗？ | 
|  | 传言2030年上市的癌症疫苗，真的靠谱吗 | 
| 13:50 |  | 
| 2023-2-10 | 一条视频讲明白新冠“特效药” | 
|  | 目前新冠特效药和爱情一样，根本不存在 | 
| 17:35 |  | 
| 2023-1-20 | 从退烧药看农村医药供需，这一切比你我想得还要复杂 | 
|  | 关于退烧药的一切问题，看这只视频就够了 | 
| 6:23 |  | 
| 2023-1-9 | 我们和汽车一起裸奔的时代到了吗？ | 
|  | 你的车联网了吗 | 
| 7:32 |  | 
| 2022-12-30 | 手机销量暴跌，苹果创新乏力，MR行业开始内卷，头显时代真的来了吗？ | 
|  | 手机消失倒计时 | 
| 7:48 |  | 
| 2022-12-21 | 特斯拉FSD很强，但中国还是得有自己的种子选手 | 
|  | 2023，自动驾驶，更接地气 | 
| 5:04 |  | 
| 2022-12-16 | 月薪5w的程序员和日薪100的人工智能训练师，谁会创造未来？ | 
|  | 人工智能，仍是人肉智能 | 
| 7:14 |  | 
| 2022-12-9 | AI和老师傅，谁开出租更靠谱？无人出租是骗补还是新风口？ | 
|  | 无人出租车你敢坐吗？ | 
| 6:15 |  | 
| 2022-12-2 | 6分钟，让你知道中国自动驾驶为什么必须走这条路 | 
|  | 中国自动驾驶为啥要搞特殊 | 
| 8:15 |  | 
| 2022-11-25 | 制造业自动化时代，机器会取代人成为新蓝领吗？ | 
|  | 劳动力真的会过剩吗 | 
| 5:41 |  | 
| 2022-11-18 | 究竟谁来保证我们的牛奶质量？ | 
|  | 一款好牛奶有哪些标准 | 
| 6:37 |  | 
| 2022-11-11 | 资本热捧的人造肉没消息了？只是暂时的 | 
|  | 现在是2022年，请大口吃肉 | 
| 5:46 |  | 
| 2022-11-4 | 中国终于有了自己的白羽鸡 | 
|  | 让我们实现吃鸡自由的白羽鸡，竟然是尖端基因作品 | 
| 7:10 |  | 
| 2022-10-21 | 中国农业如何改变非洲？ | 
|  | 农业去非洲，给全人类的“绿色基建” | 
| 7:33 |  | 
| 2022-10-14 | 资本入局农业，一个未知的大浪潮已经出现 | 
|  | 资本搞农业，想干什么？ | 
| 7:22 |  | 
| 2022-10-8 | 中国农业现代化必将实现，只是没有捷径 | 
|  | 现代化农场适合我们吗？ | 
| 10:23 |  | 
| 2022-9-30 | 从依赖进口到太空育种，中国如何打破种子垄断？ | 
|  | 种子是农业芯片 | 
| 7:49 |  | 
| 2022-9-23 | 我们扫过的每一次码，可能都救过别人的命 | 
|  | 数字追踪制造透明世界 | 
| 7:48 |  | 
| 2022-9-16 | 互联网安全进化论：生物识别凭什么消灭密码？ | 
|  | 比密码更危险的是生物识别 | 
| 7:46 |  | 
| 2022-9-9 | 世界上有公平的算法吗？ | 
|  | 算法偏见源于人吗 | 
| 12:04 |  | 
| 2022-9-2 | 产前基因检测火爆，智商能决定出生权吗？ | 
|  | 警惕人工选择完美婴儿 | 
| 7:53 |  | 
| 2022-8-26 | 去西部，不去西方，中国超算走自己的路 | 
|  | 人均算力决定人均收入 | 
| 8:58 |  | 
| 2022-8-19 | 无人驾驶，基因工程，物联网……工业4.0来了？会怎样改变我们的社会？ | 
|  | 工业4.0，驱动社会5.0 | 
| 8:50 |  | 
| 2022-8-12 | 年老、被裁、拉黑、算法歧视……你我都可能成为数字弃民 | 
|  | 你是下一个数字弃民吗？ | 
| 8:12 |  | 
| 2022-8-5 | 印度、阿联酋都加入了，登月竞争有多激烈？ | 
|  | 月球村不是梦 | 
| 8:33 |  | 
| 2022-7-21 | 随着技术的发展，我们有可能长生不老吗？ | 
|  | 我不想死 | 
| 9:00 |  | 
| 2022-7-15 | 马斯克反对的氢能源，真的是骗局吗？ | 
|  | 说不氢 | 
| 8:44 |  | 
| 2022-7-7 | 堂食重要吗？堂食的“堂”究竟意味着什么？ | 
|  | 社交比食物重要？ | 
| 8:23 |  | 
| 2022-6-23 | 为什么说容貌焦虑是消费陷阱？ | 
|  | 医美：女中茅台 | 
| 8:23 |  | 
| 2022-6-16 | 满地甄学，怀旧泛滥，娱乐业已经废了吗？ | 
|  | 内娱怀旧潮只是口红效应？ | 
| 8:47 |  | 
| 2022-6-2 | 为什么你玩魂系越菜越爱玩？多大瘾？ | 
|  | 魂系游戏有多迷人 | 
| 7:45 |  | 
| 2022-5-19 | 粮食安全，对我们到底意味着什么？ | 
|  | 粒粒皆辛苦 | 
| 6:58 |  | 
| 2022-5-12 | 从理论上的高收入，到实际上的高收入还有多远？ | 
|  | 怎样才算高收入国家 | 
| 6:16 |  | 
| 2022-4-27 | 谁在监控上班族的想法？ | 
|  | 办公监控，确保员工永远在场 | 
| 6:40 |  | 
| 2022-4-21 | 黑婚介泛滥，没有买卖，就没有对象 | 
|  | 你用过婚介平台找对象吗？ | 
| 10:12 |  | 
| 2022-4-9 | 神曲毁了华语乐坛？ | 
|  | 华语乐坛的“繁荣式衰败”，只是一个开始 | 
| 8:04 |  | 
| 2022-4-1 | 中国能在太空领地胜出吗？ | 
|  | 太空争夺战，国网VS星链，SpaceX会被反超吗？ | 
| 7:36 |  | 
| 2022-3-24 | 你交的退休金都在哪？ | 
|  | 你会算养老金到底能拿多少吗？ | 
| 7:14 |  | 
| 2022-3-17 | 进大厂、拿期权，“暴富梦”终于幻灭了吗？ | 
|  | 期权造富，梦醒了吗？ | 
| 7:20 |  | 
| 2022-3-10 | “尊严死”该不该合法化？ | 
|  | 你了解尊严死吗？ | 
| 6:03 |  | 
| 2022-3-3 | 生育率低的原因是什么？ | 
|  | 生育率低，原因不是你想的那样 | 
| 5:23 |  | 
| 2022-2-9 | 气候变暖是肯定的，但指责我国碳排放是不公平的 | 
|  | 大自然不需要人类，人类需要大自然 | 
| 6:13 |  | 
| 2022-1-26 | 网红减肥法，别试 | 
|  | 过年再胖三斤，也别信生酮能减肥 | 
| 7:09 |  | 
| 2022-1-18 | 知识产权是恶法吗？ | 
|  | “技术让知识越来越便宜，而垄断让价格越来越昂贵。” | 
| 6:44 |  | 
| 2022-1-13 | 老干妈这样的“真国潮”，不是用钱堆出来的 | 
|  | 希望再过几十年，我们国家一些品牌被人提到的时候，大家可以说：“哦，它是国际化百年老字号了。” | 
| 5:40 |  | 
| 2022-1-12 | 宇宙尽头是考编，编制尽头是什么？ | 
|  | 稳住 | 
| 5:43 |  | 
| 2022-1-10 | 宠物、动物、人，权利如何排序？ | 
|  | 为宠物立法过了吗？ | 
| 5:52 |  | 
| 2022-1-6 | 你抢占的农村市场，是我抛弃的家乡 | 
|  | 你没见过的山寨产品，都去了农村 | 
| 8:20 |  | 
| 2022-1-5 | 现在聊元宇宙还来得及吗？ | 
|  | 是谁毁了赛博理想国？ | 
| 5:17 |  | 
| 2022-1-4 | 消费也是一种“必要的恶”吗？ | 
|  | 应该反对的不是消费主义，而是消费能力不均 | 
| 6:01 |  | 
| 2021-12-29 | 请互联网别再总结“年轻人” | 
|  | 年轻人需要指点，但是不需要指指点点 | 
| 5:34 |  | 
| 2021-9-28 | 富不过三代，中国富豪的传承是绝路吗？ | 
|  | 富不过三代，这个定律还没有被打破过。 | 
| 6:07 |  | 
| 2021-9-23 | 玩家有风险，入场需谨慎 | 
|  | 其实资本市场也没有像他说的那么无耻，只是比他说的还要无耻四倍。 | 
| 5:25 |  | 
| 2021-9-15 | 风投堕落史 | 
|  | 在中国，有3万家类似的投资机构，想成为红杉资本这样的明星，然而真正走出来的有几何呢？ | 
| 5:50 |  | 
| 2021-9-14 | 谁为剥削背锅？ | 
|  | 剥削不是资本原罪，剥削是人类原罪。钱只是一个工具而已，钱这么纯洁能有什么错呢？ | 
| 3:59 |  | 
| 2021-8-19 | 加班：我自愿的！ | 
|  | 互联网人困在工位上 | 
| 4:20 |  | 
| 2021-8-17 | 想去大厂“财富自由”？可能太晚了 | 
|  | 从大厂员工到互联网自媒体，不过是从左手逃到右手，终究是围着流量打转。 | 
| 5:10 |  | 
| 2021-8-12 | 大厂割据时代结束，互联网版图融合，普通用户何去何从？【虎学研究】 | 
|  | 互联网与互联网大厂，正如爱情与婚姻。 | 
| 3:54 |  | 
| 2021-8-9 | 互联网大厂，是怎么走到这一步的？ | 
|  | 大厂的花名，是一种精神破冰 | 
| 3:33 |  | 
| 2021-7-12 | 奥特曼卡牌越来越贵，中国的IP为啥没机会？ | 
|  | 一代人有一代人的社交货币 | 
| 3:08 |  | 
| 2021-7-6 | 分房子、分股票，董明珠“疯了”吗？丨虎学研究 | 
|  | 她知道自己在干什么吗？ | 
| 4:29 |  | 
| 2021-7-1 | 河南爆火背后，没那么简单丨虎学研究 | 
|  | 相比起河南付出和奉献的那些，我们给她的鼓励和赞美，还远远不够 | 
| 4:15 |  | 
| 2021-6-29 | 有机会被你骂的谍战剧，都是优秀作品 | 
|  | 进了剧组，大家都是牲口 | 
| 4:20 |  | 
| 2021-6-25 | 看完了我也想去废弃医院露营丨虎学研究 | 
|  | 出了帐篷，全宇宙都是厕所 | 
| 3:09 |  | 
| 2021-6-18 | 爱资病到底好了没？ | 
|  | “我和你困觉，我和你困觉！ | 
| 4:20 |  | 
| 2021-6-11 | 大象是这样离开北京的 | 
|  | 我们本是大象的国度 | 
| 3:24 |  | 
| 2021-6-4 | 为什么高考分数线男女不一样？ | 
|  | 祝即将高考的你，无敌！ | 
| 3:01 |  | 
| 2021-6-1 | 我们紧急汇总了一下，同事们说这样可以生三胎 | 
|  | 大胆假设，合理幻想。 | 
| 5:29 |  | 
| 2021-5-23 | 看完对袁隆平所有的质疑，就会更加尊重他 | 
|  | 缅怀，致敬！ | 
| 3:34 |  | 
| 2021-5-20 | 凭什么#520#不能申遗？ | 
|  | 申遗只需这五步 | 
| 2:38 |  | 
|  | 为何“打底裤羞辱”如此泛滥？ | 
|  | 打底裤引发的身体羞辱，是一种网暴，也是一种集体纵容的赛博性行为。 | 
|  |  | 
| 2021-4-22 | 中国虎学研究在世界上究竟处于什么位置？ | 
|  | 这事眼熟，“竟”是这样？ | 

