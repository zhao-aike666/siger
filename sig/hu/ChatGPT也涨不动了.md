![输入图片说明](https://foruda.gitee.com/images/1688010341049245607/a2c82223_5631341.png "屏幕截图")

2023-06-25 14:12

# [ChatGPT也涨不动了](https://www.huxiu.com/article/1723389.html)

深燃 认证作者 Lv.4作·嗅之星奖 22次

> 本文来自微信公众号：深燃（ID：shenrancaijing），作者：李秋涵，编辑：魏佳，头图来自：unsplash

你还在用ChatGPT吗？



在全球掀起AI热潮的ChatGPT，似乎进入了瓶颈期。



首先是关于ChatGPT的使用率，有不利的数据出现。6月初，摩根士丹利发布的一项调查显示，只有19%的受访者表示他们之前使用过ChatGPT，只有4%的人表示依赖ChatGPT。调查表示，比例低得惊人。



这项调查在今年4月进行，涉及人数为2000人。不过，在全球78亿人口面前，这样的样本量不算大，也一定程度上降低了其参考性。



还有一个基数更广泛的数据值得关注，ChatGPT的增长已经明显放缓。



根据网站数据分析工具SimilarWeb数据，前期ChatGPT的访问量增长率惊人，1月份的环比增长率为131.6%，2月份为62.5%，3月份为55.8%，在4月份明显放缓，环比增长率为12.6%，到了5月，这个数字已经变为了2.8%。



随着ChatGPT普及，参考基数变大，增长率放缓是正常现象，不过，根据目前已有的趋势来看，6月的环比增长率也有可能为负数。



今年年初，ChatGPT像一声惊雷，让全球见识到了生成式AI的威力，也让背后的GPT（生成式预训练Transfomer模型）大火，掀起大模型创业潮。它刷新了很多数字，最让人印象深刻的，是史上用户增长速度最快的消费级应用，在推出仅两个月后，ChatGPT的月活用户已经突破了1亿。



但对它未来的发展，即便是创造者也很难给出明确答案。此前OpenAI董事会成员海伦·托勒就曾表示，“甚至创造它们的人也不知道它们能做什么，不能做什么。我预计，我们真正了解GPT-4能做和不能做的所有事情，可能还需要几年时间。”



ChatGPT现在的天花板，不代表就是GPT的天花板，但作为目前最厉害的大语言模型支撑的产品，ChatGPT的走向，也能成为观察GPT应用的一个窗口。关于AI的狂想还在继续，时间已经过去了近半年，我们好奇的是，ChatGPT的使用情况到底如何？它被高估了吗？



### 一、ChatGPT真的很多人用吗？



对于ChatGPT的使用体验，不同行业、不同人答案不同。有人把它当玩具，使用了一两次后就不再登录；有人努力把它变为工具，在工作中以提高效率；也有人，在努力将ChatGPT变为工具的过程中失败了，因为觉得它“不够好用”。



夏楠属于第三种。她从事外贸行业，会用ChatGPT写工作邮件，也会让它解答一些生活中的烦恼，为了能更好地使用ChatGPT，她都是用英文发指令。



从2月开始使用ChatGPT，她的体验分为了三个阶段，最开始是好奇，很多问题都想抛给ChatGPT，看它怎么回答，探索它。从5月开始，她感觉ChatGPT“变笨了”，之前能做到的，现在做不到了。现在，她对ChatGPT的评价是，“不好用”。



比如最近，他们公司接了一个炒菜机器人的ODM（原始设备制造）业务，她想让ChatGPT提供这个市场的预测数据，在反复推拉后，ChatGPT还是没有给她答案。而在写工作邮件上，经过调教，ChatGPT也只给到她一个命令式的跟进邮件，文字官方式的长篇大论，也不是她想要的，她希望“它能写出有礼貌的，信息表达很明确的邮件”。



她觉得，ChatGPT之所以做不到，短板在于“不懂得人情世故”。得不到想要的结果，她的使用频次也降低了，从一个星期五六次变为了一周一次。



当然，之所以会有这样的体验，背后综合影响因素很多，和使用者是否问到了ChatGPT擅长的领域有关，也要看使用者有没有找到与ChatGPT合适的交流方式。



正在澳大利亚生活的Lucy，从去年底ChatGPT一推出后就开始用英文使用它。现在，她日常用ChatGPT来整理学术研究上的思路、学习语言，是生活里必不可少的工具。只是，准确性的问题一直让她烦恼，文献参考需要自己找，“如果我质疑它的回答，它就会顺着我的思路回答”。



 **除了ChatGPT的使用体验感受不一，从数据来看，ChatGPT的普及率，暂时也还没有想象中的广。** 

![输入图片说明](https://foruda.gitee.com/images/1688010441698944646/6480de36_5631341.png "屏幕截图")



> 摩根士丹利相关报告截图



除了摩根士丹利的报告，还有一些数据可以作为佐证。根据SimilarWeb数据，3月-5月，美国、日本是全球为ChatGPT贡献流量份额最高的国家，分别位列第一、第三。不过，最近，美国的流量已经下滑了10.28%。


![输入图片说明](https://foruda.gitee.com/images/1688010452758315032/3a329d21_5631341.png "屏幕截图")


> ChatGPT的全球流量分布 来源 / SimilarWeb数据



美国方面，在5月底，皮尤研究中心发布了一份调查，他们在今年3月中旬在1万多名美国成年人当中展开调查，18%的人听说过很多关于ChatGPT的事情，39%的人听说过一点，42%的人根本没有听说过。



而在日本，根据日本ICT市场调查咨询机构MM总研的最新调查报告，5月24日-31日期间，以日本及美国企业所属的13814名员工（其中，日本13412人、美国402人）为对象，进行的网络调查结果是，日本企业的ChatGPT使用率仅7%，与美国企业的51%使用率相比，两者相差高达44个百分点。



日本企业中，近半数员工（46%）回答“不知道”ChatGPT，而即便知道ChatGPT，但回答“未使用”的比重也达42%。



这些都是最近的报告，样本量在1万人左右。不过ChatGPT大热后，全球关于它的使用报告众多，观点不一，甚至有的得出的是相反结论。上述报告有一定参考性，但也因地域和人群的差别，不一定能完全反映真实情况。



还有更明确的值得参考的整体性数据，可以帮助我们认识ChatGPT的应用现状。



根据SimilarWeb，ChatGPT增长明显放缓，尤其到6月，截至6月20日，6月已经过去三分之二，访问量比5月少了38%左右，粗略推算，到6月31日如果没有特别新的刺激，6月的环比流量或将下降。


![输入图片说明](https://foruda.gitee.com/images/1688010472984778908/ab3ea6c7_5631341.png "屏幕截图")


> ChatGPT近期流量变化 来源 / SimilarWeb数据



同时，还可以参考的是，根据SimilarWeb，在5月，ChatGPT的跳出率是12.59%，低于谷歌、Youtube等，而在6月24日，跳出率已经上升到37.37%。平均访问持续时间也从8分32秒，下降到7分48秒。



另一个数据是，接入GPT大模型后Bing的市场份额变化。



![输入图片说明](https://foruda.gitee.com/images/1688010488456787079/c73508e3_5631341.png "屏幕截图")

> 来源 / Statcounter



Bing的市场占有率，在二三月份刚引入GPT时引发关注，根据网站通讯流量监测机构Statcounter，2023年3月Bing的市场份额是2.86%，5月是2.77%，不仅占比没有提高，甚至还有下降趋势。



### 二、是什么限制了ChatGPT？



关于ChatGPT应用上的问题，已经老生常谈，不过这些问题对它应用普及上的影响，或许比想象中广。



 **首先是“变笨”这件事。** 



6月初，“ChatGPT变笨了”的声音引发过讨论。不过OpenAI开发者推广大使Logan Kilpatrick曾出面回应，表示自3月14日发布GPT-4以来，大模型的本体一直处于静态，不存在大量外部数据污染模型的情况。同时他也承认，大模型本身存在不稳定性，因此对相同的提示词，存在回答前后不一致的情况。



一位AI从业者告诉深燃，5月就有国外从业者在OpenAI论坛里分享论证GPT变笨的文章。最近，他用GPT-4的API做了测试，让它做简单的计算题。从结果准确度来看，GPT-4-0314取得了满分，GPT-4取得了80分，GPT-4-0613，勉强可以拿50分。其中，0314、0613即指3月14号、6月13号的快照（指整个系统在某个时间点上的状态）。这个结果，给他一种GPT-4在被削弱的感受。



根据监管机构NewsGuard的专家分析，OpenAI最新版本的GPT-4，在输出信息方面，比GPT-3.5要糟糕。在今年3月份发布的报告中，NewsGuard提到，GPT-4不仅在其研究人员的提示下回答了完全虚假的新闻叙述，而且比GPT-3.5回答的内容更差。



在上述AI行业从业者看来， **这样变化的结果是，用户需要变得更为具体和主动地引导GPT-4，才能获得与过去相当的回答质量。** 



这也再度影响ChatGPT的使用门槛，而这与ChatGPT的初衷有所背离。



最开始ChatGPT爆火，就有业内人士对深燃分析，它带来的影响是将通用人工智能放在了每个用户面前，也把人机交互的门槛，打到了最低点。



但目前来看，门槛还是存在。从ChatGPT的用户画像，也能看出这个产品的普及情况。根据SimilarWeb数据，使用者主要分布在计算机电子与技术行业，其中，编程和软件开发占比最大。其他行业里，只有游戏行业中的视频游戏机和配件的从业者占比较多。



在使用ChatGPT的体验上，一位工程师给深燃的反馈最为积极，表示一直在用，“能帮我解决小的程序上的问题”。



尽管都说，“淘汰你的不是AI，是会用AI的人”，如果普通人使用起来门槛越来越高，那也一定程度上脱离了ChatGPT的初衷。



还有两个问题，是ChatGPT从最开始就面临的质疑，即 **准确率和隐私** 保护。



根据上述日本相关机构的报告，在被问到今后若要继续/扩大使用ChatGPT，需要解决什么问题时，日本企业、美国企业分别有高达49%、45%的回答是“案件的精度”，其次为“个人资料等隐私（日企34%、美企35%）”，和“对问题的理解程度（日企33%、美企34%）”。



准确度方面，OpenAI的CEO山姆·奥特曼，也作出过解释，这个程序会自信地宣称一些东西是事实，但其实它是编造出来的，就像谎话连篇的政客一样。他给这种现象起了个名字——“幻觉难题”。



总之，准确率要实现起来非常不容易。原因就是因为它不是靠记忆，而是凭借演绎推理能力来工作。“大语言模型靠的是推理的拼字游戏，不可能像数据库一样完全准确，人也不能完全保证准确”，关注AI行业的工程师杨阳告诉深燃。



隐私方面，OpenAI至今还没有给出明确的解决方案。在加拿大工作的小虹就告诉深燃，公司特地发邮件通知，让大家谨慎使用ChatGPT。



基于这些限制，ChatGPT的应用场景也受限制。



关注产业的投资人陈默默告诉深燃，它其实适合“生产力驱动”的内容生产，而不是“创意驱动”的内容生产，在前者上，能替代不少有重复经验累积的人力。



用户洛洛从4月开始使用ChatGPT，她开了会员，主要用于写脚本和文案，“只要能给它正确的公式，基本可以反馈给我任意思维的脚本，只是拿到手要改”。她表示，产出的脚本比较基础，没法做成爆款，但它逻辑没有问题，“像公司日常的一些大量的视频输出，是可以支持的”，她现在的ChatGPT使用频率，基本上一周3次以上。



现在夏楠调整了策略，只问它一些流程式的问题，比如开ebay网店的流程。尽管这类问题也可以问谷歌、百度，但“ChatGPT的回答更好”，她举例，最近她去德国旅行，让ChatGPT给她安排旅游计划，给出的答案有参考性，交通安排的也很清楚。



这些长期使用ChatGPT的用户，不论对ChatGPT的使用体验满意与否，他们都提到，ChatGPT更像是一个升级了的谷歌、百度，带来了一定助力。



### 三、ChatGPT，象征意义高过实质意义？



最近，OpenAI推出了类似于LLM版的App Store，加速生态的建设，还被曝光了一些功能上的优化。这背后也隐藏着一个信号，GPT4暂时已经到天花板，要加速生态建设，在GPT5出不来之前，先做一些体验上的优化。



早在4月，山姆·奥特曼就表示，还没有开始研究GPT-5，也没有立即开始的打算，还曾表示，“大型模型时代已经到头了。”



据OpenAI官网，GPT模型参数数量（可以理解为喂养模型的语言材料）在不断提升。GPT-1是1.17亿，GPT-2有15亿，到了GPT-3，飙升至1750亿，GPT-4，根据国外媒体机构Semafor的一份报告，比GPT-3大六倍左右，具有1万亿个参数。



此前杨阳也对深燃表示，或许GPT-4就成长到头了，语料是一个原因，“人类历史上创造出的优秀资源就这些了”，模型本身的限制也是一个原因。在他看来，现在GPT-4被限制了，应该有能力没有被完全开发出来。



近日，Facebook母公司Meta首席人工智能科学家Yann LeCun就指出，ChatGPT背后的生成式人工智能技术已进入了死胡同，拥有太多的局限性。



出于巨头间的竞争关系，或许很难将这话作为客观参考。但可以肯定的是，ChatGPT的确遇到了瓶颈。



要让大语言模型有更好的应用，不少人把视线投向垂直领域的应用上。



AI行业从业者秦凯对深燃做了一个比喻，ChatGPT这类泛化型的人工智能，应用宽泛时，能力就像是高中生、大学生，和垂直场景结合时，fine-tuning（在自然语言处理中使用的技术，也叫微调）的数据足够精确和贴合场景，能力就能变为硕士、博士，能解决更具体的需求。



杨阳也认同这一看法。他提到，目前的模型最多只能进行一倍左右的优化，“大家有一个基础共识，GPT-5来了，也不会带来颠覆性的进化”，短期内不可能达到AGI（通用人工智能）水平。



不过，他表示，现在做具体垂类应用，首先是费用高，训练模型对公司来说依旧是不小的成本；其次是数据安全、数据隔离的问题，目前采用的办法是，“在大模型基础上套小模型”，但带来的问题是，现在的底层技术还在变化，“没有人知道下一个模型，更优秀的模型什么时候出现”，这个中间阶段让大家很迷茫，“如果三五年后才出现，那现在基于大模型做垂类的产品是不亏的，场景落地后有机会回收资金。但如果很快就出现了，那大家现在做的外挂型垂类产品，是没有多少意义的。”



投资人陈默默表示，这是一个“先有鸡还是先有蛋”的问题，他们还是愿意去看相关项目，在细分领域里切一个特定场景的应用，因为“哪怕未来的底层有变化，只要行业没有变动，在应用层的行业理解上还是会有沉淀”。



但他们在看项目时遇到的问题是，很难有人可以明确告诉他们，产品能节省多少人力成本。“看下来，还是得给机器配个人”，她举例，关注赋能研发端论文筛选归纳相关的垂类产品，实际使用也还是需要一个人顺着机器的结果，再做进一步验真、开发和研究，实际上很难说效率有特别好的优化，所以现在，一些投资人都会倾向于再观望观望。



在关注垂类领域的AI创业公司的产品时，她的感受是，“我们对技术带来的产业升级机会保持谨慎乐观，目前可能它的市场意义，高过实质意义”。



秦凯总结，人们对ChatGPT的期望过高，但有两个瓶颈。首先，下一代大语言模型通过更大的参数规模和更强算力的收益边际递减，人们的期望可能无法很快满足。其次，当前的大语言模型是泛化的，需要很长时间来解决特定、真实的问题。目前垂直领域的生成式AI，已经变成了针对具体企业做定制化需求和私有部署的体力活，“底层模型依靠transformer方式缺乏解决非常复杂问题的能力，现在的应用情况与预期的水平相距甚远”。



应用还在继续，技术还在发展，关于ChatGPT的应用及潜力还需要再观望。即便是这样，ChatGPT已经让一些人的生产效率，有了一个数量级的提高，即便当下有瓶颈，“ChatGPT已经是很伟大的产品，这就够了。”杨阳表示。



> 应受访者要求，文中夏楠、Lucy、杨阳、小虹、洛洛为化名。


![输入图片说明](https://foruda.gitee.com/images/1688010583379384055/b0847f02_5631341.png "屏幕截图")

本文来自微信公众号：深燃（ID：shenrancaijing），作者：李秋涵，编辑：魏佳

- 本内容为作者独立观点，不代表虎嗅立场。未经允许不得转载，授权事宜请联系hezuo@huxiu.com
- 如对本稿件有异议或投诉，请联系tougao@huxiu.com
- 正在改变与想要改变世界的人，都在 虎嗅APP

