# [开源芯片技术生态论坛丨2023 RISC-V中国峰会同期活动报名开启！](https://mp.weixin.qq.com/s?__biz=MzkzNTM5NDI0Mg==&mid=2247484705&idx=1&sn=178a91e286ff956aea53b794dd8f4fc8)

BOSC 北京开源芯片研究院 2023-08-11 20:02 发表于北京

![输入图片说明](https://foruda.gitee.com/images/1691765506858222101/8fd20179_5631341.png "屏幕截图")

- 微信号：北京开源芯片研究院 gh_272a7ab004ba
- 功能介绍：开源芯片 普惠世界

![输入图片说明](https://foruda.gitee.com/images/1691765530344096392/7283a79d_5631341.png "在这里输入图片标题")

![输入图片说明](https://foruda.gitee.com/images/1691819371423665969/6a532f74_5631341.jpeg "640 (2)副本.jpg")

北京开源芯片研究院 | BOSC | BEIJING INSTITUTE OF OPEN SOURCE CHIP 

### 2023年RISC—V中国峰会同期活动 开源芯片技术生态论坛（原“一生一芯”技术论坛）

- 论坛时间：2023年8月25日全天
- 会议地点：北京市香格里拉饭店—三层翡翠厅
- 直播地址：https://live.bilibili.com/24416626 

### 论坛介绍

开源芯片技术生态论坛聚焦开源芯片技术生态（使用开源EDA和开源IP设计开源芯片），涵盖开放指令集（RISC—V）及其软硬件系统、开源芯片设计、开源EDA工具链、开源IP、芯片人才培养等主题， 旨在为相关爱好者提供交流平台，围绕开源芯片分享技术工具、学习经验和心得感想。 同时设置学生专场，鼓励学生围绕RISC—V开展相关工作并分享。

### 论坛议程 

| 报告时间 | 报告名称 | 报告人及单位 |
|---|---|---|
| | 上午议程 | |
| 9:00-9:05 | 论坛致辞 | 包云岗 中国科学院 计算技术研究所 |
| 9:05-9:10 | 授牌仪式 | 待定 |
| | Session1：主旨报告 | |
| 9:10-9:35 | 待定 | 包云岗 中国科学院 计算技术研究所 |
| 9:35-10:00 | 标准引领，教学相长，构建RISC—V教育生态 | 陈炜 平头哥 |
| 10:00-10:25 | 一生可以托付的“芯” — 冲向高端的RISC—V | 姚永斌 奕斯伟计算 |
| 10:25-10:50 | RISC—V虚拟化技术发展及其演进 | 笨叔（张天飞） 奔跑吧 Linux 社区 |
| 10:50-11:10 | 茶歇 |
| 11:10-11:35 | [开源硬件与敏捷方法](../../RISC-V/2023/开源芯片技术生态论坛：开源硬件与敏捷方法.md) | [王璞 达坦科技](../../RISC-V/2023/开源芯片技术生态论坛：开源硬件与敏捷方法.md#达坦科技创业是一场灵魂深处的摇滚 "达坦科技创业是一场灵魂深处的摇滚") |
| 11:35-12:00 | Chisel5：下一代的 RTL 设计与验证语言 | 刘玖阳 Chips Alliance & PLCT实验室 |
| | 下午议程
| | Session2：开源EDA 
| 14:00-15:00 | iEDA-Tutorial：开源iEDA 平台介绍和使用实践 | 黄增荣 鹏城实验室 |
| 15:00-15:15 | 基于yosys＋iSTA的数字前端时序评估 | 陶思敏 鹏城实验室 |
| | Session3：“一生一芯”SIG小组
| 15:15-15:20 | 开源IP组件库 | 缪宇飏 中国科学院 计算技术研究所 |
| 15:20-15:25 | 开源芯片数据集 | 李子龙 杭州电子科技大学 |
| 15:25-15:30 | 高性能体系结构模拟器 | 段震伟 北京开源芯片研究院 |
| 15:30-15:35 | 开源RTL仿真器 | 陈璐 中国科学院 计算技术研究所 |
| 15:35-15:40 | 国际交流与翻译 | 缪宇飏 中国科学院 计算技术研究所 |
| 15:40-16:00 | 茶歇
| | Session4：“一生一芯”学生分享
| 16:00-16:10 | “一生一芯”计划：构筑本科生基础能力，培养跨学科人才 | 张宇驰 太原理工大学 |
| 16:10-16:20 | 学习·探索·创新：我与“一生一芯”的故事 | 烟雨松 北京一零一中学 |
| 16:20-16:30 | “一生一芯”中基于模拟的设计空间探索初尝试 | 曾宇航 东华理工大学 |
| 16:30-16:40 |  “一生一芯”学习感受：从逃避问题到面对问题 | 孟沛 电子科技大学 |
| 16:40-16:50 | “一生一芯”：个人成长与专业发展的里程碑 | 袁永强 西安邮电大学 |
| 16:50-17:00 | “一生一芯”学习分享 | 王睿 中北大学 |
| 17:00-17:10 |  “一生一芯”中对处理器设计的科学调试方法 | 潘岩 哈尔滨工业大学 |
| 17:10-17:20 | 简单的RISC-V多核系统实现 | 史历 卡内基梅隆大学 |
| 17:20-17:25 | 论坛总结 | 包云岗 中国科学院 计算技术研究所 |
| | 论坛结束

### 参会须知：

- 现场参会均可获得伴手礼一份
- 现场有机会获得嘉宾亲笔签名书籍，数量有限
- 峰会报名日期截止时间为8月14日，请在报名链接中勾选本次同期活动，方便我们了解您的报名情况。

本次同期活动在会议翡翠厅内以及香格里拉饭店2层展区均设有展台欢迎参观交流！

扫码注册参会

扫码加入 截止日期8月14日

参会微信群 

### 关于北京开源芯片研究院

近年来，RISC-V快速发展，已经成为当前国际科技竞争的焦点。为提升我国集成电路设计水平，建设与国际开源社区对接的技术平台，北京市和中科院高度重视RISC-V发展，组织国内一批行业龙头企业和顶尖科研单位于2021年12月6日发起成立北京开源芯片研究院。研究院以开源开放凝聚产业发展共识，以协同创新激发应用牵引潜力，着力推进RISC-V创新链和产业链的加速融合，加速科技创新成果产业化落地，加快打造全球领先的RISC-V产业生态。

![输入图片说明](https://foruda.gitee.com/images/1691765654509705898/430287d9_5631341.gif "在这里输入图片标题")