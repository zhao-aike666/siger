# 石榴派 “一生一芯” 寒假集训，开锣喽！

[!40 RISC-V联盟2021年会观感](https://gitee.com/flame-ai/siger/pulls/40) 来自 @[Lisa Zhang](https://gitee.com/lisazyt) 创建于 2022-01-24 20:56 的 PR 正式拉开了 [石榴派 “一生一芯” 寒假集训](https://gitee.com/xiaoxu-tongxue/siger/issues/I4RDZV) 的大幕，一个旨在向中学生科普科技最前沿的计划，在[前辈们](../../RISC-V/baoyungang%2010%20years%20diffrent.md)的[关怀](https://images.gitee.com/uploads/images/2021/1027/040645_d9fe3307_5631341.png)下，终于迈出了第一步。也恭喜 @[Lisa Zhang](https://gitee.com/lisazyt) 第一个脚印是你的，:D 多少尺码的鞋呢？一个石榴派。:pray: 

今天正式拉起 sig 组，你将和其他同学共同耕耘，以期假期结束时，我们能出一期 “一生一芯” 为主题的学习笔记，为我国科技人才队伍的建设尽一份绵薄之力，也期待同学们中能诞生出投身我国科技基础设施建设的排头兵，未来的科技之星就是你们啦！怀着激动的心情，我将昨晚接受 PR 时的笔记抄录如下：

> @[lisazyt](https://gitee.com/lisazyt) LISA 很高兴，在两个平行空间，我们一同观看了 [RISC-V联盟2021年会](../../RISC-V/Ni%20Guang%20Nan.md#%E4%B8%AD%E5%9B%BD%E5%BC%80%E6%94%BE%E6%8C%87%E4%BB%A4%E7%94%9F%E6%80%81risc-v%E8%81%94%E7%9B%9F2021%E5%B9%B4%E4%BC%9A) 除了《[痴心不改，大国匠“芯”,致敬倪光南院士!](../../RISC-V/Ni%20Guang%20Nan.md#%E7%97%B4%E5%BF%83%E4%B8%8D%E6%94%B9%E5%A4%A7%E5%9B%BD%E5%8C%A0%E8%8A%AF%E8%87%B4%E6%95%AC%E5%80%AA%E5%85%89%E5%8D%97%E9%99%A2%E5%A3%AB)》，[<img src="https://images.gitee.com/uploads/images/2022/0124/222133_4a9261c8_5631341.png" height="19px" title="RISC-V联盟2021年会">](https://images.gitee.com/uploads/images/2022/0124/222133_4a9261c8_5631341.png) 这幅 “[吉祥数独](https://images.gitee.com/uploads/images/2022/0124/222133_4a9261c8_5631341.png)” 也是为年会准备的贺礼，是年会开始前，用石榴派开发板运算出来的。想着送给年会，由包云岗老师代收，既然你来了，就由你代表RV新势力，新青年，收下这份新年礼物吧 :pray: 这是全体火种志愿者聚合的 [99 个祝福](https://gitee.com/blesschess/luckystar/issues/I4D7DW#note_8441412_link)。

> - 你可以抽空，完成这组吉祥数独，可以掐个表，计下时间，这也是社区火种队，志愿者入伙的岗前测试。
> - 用老师给的更多线索，可以写一份学习计划，还可以谈谈吉祥数独和你平日里玩的数独有那些不同？

> 很高兴，我们的点亮星光计划，正是开始了，感谢你的 PR，它就像两个星系的首次交汇，它必将成为 RV 星光岛上璀璨的一幕，载入[宇宙](../../%E7%AC%AC8%E6%9C%9F%20%E6%98%9F%E8%BE%B0%E5%A4%A7%E6%B5%B7II%20-%20%E5%AE%87%E5%AE%99.md)星河的历史，也祝愿你成为其中，最璀璨的一颗 [恒星](../../Tools/GitSTAR.md)！:pray:

### 学习笔记 

- @[lisazyt](https://gitee.com/lisazyt) 2022-01-24 20:56 提交 [RISC-V联盟2021年会观感.md](RISC-V联盟2021年会观感.md) 献礼 [RISC-V联盟2021年会](../../RISC-V/Ni%20Guang%20Nan.md#%E4%B8%AD%E5%9B%BD%E5%BC%80%E6%94%BE%E6%8C%87%E4%BB%A4%E7%94%9F%E6%80%81risc-v%E8%81%94%E7%9B%9F2021%E5%B9%B4%E4%BC%9A)
- @[yuandj](https://gitee.com/yuandj) 2022-01-25 17:29 创建 一生一芯 sig 组，[石榴派 “一生一芯” 寒假集训](https://gitee.com/xiaoxu-tongxue/siger/issues/I4RDZV) 正式开启 :pray:
- @[alan段](https://gitee.com/duan-haotian) [2022-02-13 15:08](https://gitee.com/flame-ai/siger/pulls/45) 提交 [插上RISC-V的翅膀，迎上软硬件齐飞的新时代.md](插上RISC-V的翅膀，迎上软硬件齐飞的新时代.md) 迎接 第四期一生一芯 的开启。
- @[yuandj](https://gitee.com/yuandj) 2022-02-18 12:03:01 正式提交 [RISC-V Luckies v0.1.69](./RISC-V%20Luckies%20v0.1.69.md) 给 [一生一芯](https://oscpu.github.io/ysyx/) 项目组。
- @[5pik](https://gitee.com/g5pik) 2022-03-10 13:22 分享《[提问的智慧](./《提问的智慧》读后感.md)》，我们主张学习民主，共建良好的学习氛围！
- @[5pik](https://gitee.com/g5pik) 2022-03-22 00:19 [”一生一芯“科普学习小组组长](https://gitee.com/g5pik/siger/issues/I4YW6W) [上岗](https://gitee.com/flame-ai/siger/pulls/56) 啦，撰文《[君子不器](《君子不器》“一生一芯”%20科普学习小组组长宣言.md)》做宣言 :pray:
- @[yuandj](https://gitee.com/yuandj) 2022-04-05 14:53:05 收录 SIGer #81《[回归学习](回归学习.md)》，[陈春昀](https://www.bilibili.com/video/BV1PU4y1V7X3?p=11) [博士第一学年回顾](https://mp.weixin.qq.com/s/7l3YB2VI386MKL-ae5_XPw) & 2022 书单
- @[yuandj](https://gitee.com/yuandj) 2022-04-09 16:03:46 [RISCV五级流水CPU设计](https://blog.csdn.net/weixin_41871524/article/details/116890604) 作者 [杨乐](https://github.com/yang-le/riscv_cpu)爱学习《[想学自然就去学了，反正也没人会出题考我😂](想学自然就去学了，反正也没人会出题考我😂.md)》
- @[yuandj](https://gitee.com/yuandj) 2022-04-20 23:56 增加两篇 ”北向学习“ 资料：《[听障关怀](../../RISC-V/hearing%20care.md)》、《[蓝信封](../../生活/蓝信封.md)》云邮筒；5-28 增补《[我们以书信为伴](../../生活/我们以书信为伴.md)》
- @[yuandj](https://gitee.com/yuandj) 2022-06-09 16:29 [第四期 “一生一芯” 招生招聘宣讲会](%E7%AC%AC%E5%9B%9B%E6%9C%9F%E2%80%9C%E4%B8%80%E7%94%9F%E4%B8%80%E8%8A%AF%E2%80%9D%E6%8B%9B%E7%94%9F%E6%8B%9B%E8%81%98%E5%AE%A3%E8%AE%B2%E4%BC%9A.md) 包云岗老师鼓励：经过“一生一芯”锤炼过的同学，素养更出众 :pray:
- @[yuandj](https://gitee.com/yuandj) 2022-06-13 22:59 【转载】《[心目中的教育学博士余子豪——“一生一芯”课程的设计者](心目中的教育学博士余子豪——“一生一芯”课程的设计者.md)》
- @[yuandj](https://gitee.com/yuandj) 2022-06-13 23:59 【[香山双周报](https://mp.weixin.qq.com/mp/appmsgalbum?__biz=Mzg5MTY4MjgyNg==&action=getalbum&album_id=2441611750769819649&scene=173&from_msgid=2247485721&from_itemidx=1&count=3&nolastread=1#wechat_redirect)】[20220613期](【香山双周报】20220613期.md)
- @[yuandj](https://gitee.com/yuandj) 2022-8-6 23:27 [RVSC 2022 - 第二届 RISC-V 中国峰会 专题策划 开题](../../RISC-V/README.md)，承担本次任务的是 [RISCV 频道](../../RISC-V/)！
- @[yuandj](https://gitee.com/yuandj) 2022-8-28 10:17 [一生一芯第五期启动会](https://oscc.cc/events/ysyx-2022)，共同见证《[SIGer少年芯.md](../../RISC-V/SIGer少年芯.md)》贺礼技术论坛，纸版 [SIGer少年芯.pdf](../../RISC-V/SIGer少年芯.pdf) 发布。