![输入图片说明](https://foruda.gitee.com/images/1693312492894789826/b4b47341_5631341.jpeg "ByteFloat机制：基于“一生一芯”的卷积神经网络部署方案.jpg")

# [ByteFloat 机制：基于“ 一生一芯” 的卷积神经网络部署方案](https://www.bilibili.com/video/BV1Ld4y1V7DA/ "一生一芯B站：BV1Ld4y1V7DA")

- 叶从容 [1][2]  黄斌 [3]
- [1] 中国科学院  计算技术研究所
- [1] 中国科学院大学  计算机科学与技术学院
- [3] 集美大学  计算机工程学 院

![输入图片说明](https://foruda.gitee.com/images/1693311562078660879/6a53f97d_5631341.png "屏幕截图") ![输入图片说明](https://foruda.gitee.com/images/1693311568128399052/b4a70246_5631341.png "屏幕截图") ![输入图片说明](https://foruda.gitee.com/images/1693311572863357647/68026fad_5631341.png "屏幕截图")

### 摘要

以图像处理为主要应用的卷积神经网络算法在社会经济中日益普及。在卷积神经网络中，用于提取局部特征的神经元所使用的数据格式通常是单精度浮点数或是定点数。传统的浮点数格式尽管精度较高，但也由于其并未针对深度神经网络进行专门设计，导致了其运算过程的高开销及硬件实现的复杂。本文提出一种新型浮点数据格式ByteFloat及其相应的乘法运算器、加法运算器的设计方法，既能有效地表示浮点数，又能降低浮点乘法的开销；并且使用RISC-V自定义指令在一生一芯芯片这一完全不支持浮点数的计算系统上验证成功，为卷积神经网络算法在低成本、低功耗设备上的部署提供了一种解决方案。

### 背景

![输入图片说明](https://foruda.gitee.com/images/1693311659174134726/4b32bb00_5631341.png "屏幕截图")

一生一芯个人处理器：
1. 仅支持RV64I，甚至不包含M
2. 没有浮点运算单元FPU
3. 软浮点效率低下

![输入图片说明](https://foruda.gitee.com/images/1693311685173001495/18809bf1_5631341.png "屏幕截图")

手写体识别应用：
1. 包含大量浮点运算
2. 存在明显数据冗余

在卷积神经网络领域，以IEEE754为代表的浮点数标准，存在精度溢出，运算开销大等问题，尤其是浮点数乘法运算，很难在一周期内完成运算。bfloat16、Posit、INT8等具有尾数形式的数据格式均存在类似问题：

![输入图片说明](https://foruda.gitee.com/images/1693311758353908913/3eff3ba5_5631341.png "屏幕截图")

### ByteFloat 定义

用一组无限长度的整数数列an，来拟合一个正实数F，其中数列与F满足：

![输入图片说明](https://foruda.gitee.com/images/1693311820674346355/c860fba7_5631341.png "屏幕截图")

例如：
- 数列：-1，-2，-3，-4，...
表示实数：1.0
- 数列：-1，-3，-4，-5，...
表示实数：0.75

> 在实际的工程中只能保存有限位，因此需要规定进位与舍去的规则。假如保留两项，不妨设进位误差为Ce，舍去误差为De，分析后发现，当a2与a3相邻的时候，也即a2=a3+1，满足：

  ![输入图片说明](https://foruda.gitee.com/images/1693311875953019447/b8a39cb9_5631341.png "屏幕截图")

> 即进位误差Ce恒小于等于舍弃误差De。基于这一规律，ByteFloat进位策略可以归纳为“邻进远舍”，即在保留K项整型数的时候，检查ak与次项是否相邻，是则进位，否则舍去。

用两个整型数a1、a2，来表示一个正实数F，满足：

![输入图片说明](https://foruda.gitee.com/images/1693311914697057461/5a7cf96a_5631341.png "屏幕截图")

例如：
- 数列：0，-∞
表示实数：1.0
- 数列：-1，-2
表示实数：0.75

> 不难注意到，a1永远大于a2，这里存在1bit的信息冗余，也即a1存储在前还是a2存储在前是无关紧要的。有趣的是，符号位正好需要1bit信息。因此，很自然地，我们使用a1和a2的序来表示符号。

  例如：
  - 数列：-1，-2
表示实数：0.75
  - 数列：-2，-1
表示实数：-0.75

> ByteFloat趣闻一则：
> - 命题A：不存在任何实数，是ByteFloat所无法表示的，即便是π，e，ln2，或者更一般的数。
> - 命题B：整数集的幂集与实数集等势。
> 证明命题A之前要先证明命题B，而命题B早在1963年就被证明是不可证明的。

### ByteFloat 误差分析

![输入图片说明](https://foruda.gitee.com/images/1693312028718585665/809141cc_5631341.png "屏幕截图")

### ByteFloat 运算

ByteFloat数间的乘法： 
 **(a1 ， a2) x (b1 ， b2) = (a1+b1 ， a1+b2 ， a2+b1 ， a2+b2)** 
1. 二元组相乘得到四元组；
2. 乘法器本质就是四个整型加法器；
3. 加法器间是可全并行的。

![输入图片说明](https://foruda.gitee.com/images/1693312108151453935/2b91e279_5631341.png "屏幕截图")

ByteFloat数间的加法：
 **(a1 ， a2) + (b1 ， b2) = (a1 ， b1 ， a2 ， b2)** 
1. 二元组相加得到四元组；
2. 加法器可以直接拼接，示数逻辑不变；
3. 有时需要化简。
例如（-1，-3，-3，-4）= （-1，-2，-4）

![输入图片说明](https://foruda.gitee.com/images/1693312137569667400/48b14c9c_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1693312144889344082/00b0cbb8_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1693312151086665811/5f1f984d_5631341.png "屏幕截图")

### 实验

ByteFloat乘加均单周期完成，可以直接将运算器嵌入ALU，低处理器微架构的改动很小。

设计自定义指令
- BFM：0000000 01011 01010 000 01010 0101011
- BFA：0000000 01011 01010 000 01010 1011011

对模型数据进行预处理，将IEEE754格式的浮点数转换成ByteFloat数：

![输入图片说明](https://foruda.gitee.com/images/1693312243163655037/e612a7be_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1693312249683603882/c0860e4b_5631341.png "屏幕截图")

-> 

![输入图片说明](https://foruda.gitee.com/images/1693312258259250834/4ba69e7d_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1693312280614220395/1cc4584f_5631341.png "屏幕截图")

与IEEE754标准相比：ByteFloat数方案：
1. 几乎没有精度损失；
2. 运算开销大幅减小；

ByteFloat机制或可为卷积神经网络算法在低成本、低功耗设备上的部署提供了一种解决方案。


