[BFS解数独](https://gitee.com/zzyhd/sudoku)

- [BFS——广度优先算法（Breadth First Search）](https://blog.csdn.net/g11d111/article/details/76169861)
- [Codeheir: What is Breadth-First Search (BFS)](https://codeheir.com/2021/06/05/what-is-breadth-first-search-bfs/)
- [PostgreSQL机器学习插件MADlib实践 Breadth-First Search篇](https://blog.geohey.com/postgresqlji-qi-xue-xi-cha-jian-madlibshi-jian-breadth-first-searchpian/)

<p><img width="50%" src="https://foruda.gitee.com/images/1665964553588114396/9d7929a9_5631341.jpeg" title="341bfsgify副本.jpg"><img width="50%" src="https://foruda.gitee.com/images/1665966916313996352/85af9b61_5631341.gif" title="341bfsgify.gif"></p>

> 这是第一张动图封面，稍微有个瑕疵，全彩的背景被灰了，如果有开源版本一定要研究下，争取再提个 PR 给上游社区。不能不说 ps 家族的 imageready 帮我实现了这个功能。而真正打动我的有两个，除了本作作者同学的小傲娇（看正文），就是外媒科普的细致入微，这动图是直接引用的。不管怎么说，今天都是一个愉快的日子，阳光明媚，周末收入麾下小码王一枚，火焰棋实验室添砖加瓦的步伐又可以快一步啦。issues 嗖嗖地飞出 ... 同学们，请欣赏。 :pray:

# [BFS解数独](https://gitee.com/zzyhd/sudoku)

 **BFS 解数独程序** 
    
响应 @yuandj 袁老师的号召，把我以前做的BFS数独程序整理后开源发布。

### 开发原因

当时记得小学六年级的时候，班里数学老师布置了数学创新活动，每周给小组分配了一道数独题目，要求在一周类完成。

正好那个时候我开始学到了 BFS 算法，想着干脆用C++来写一个程序让机器来算。 

什么是 BFS　算法，这里可以阅读我写的　[what is Breadth First Search](https://gitee.com/zzyhd/siger/blob/master/sig/sudoku/What%20is%20Breadth-First%20Search%20(BFS).md)

### 下载运行

下载本仓库所有代码后，bfs_shudu.cpp 就是主程序，

在装有DevC++ 的windwos下可以直接运行。

如果是 NOI_Linux 下 可以使用如下命令运行：

 g++ bfs_shudu.cpp -o bfs_shudu 

 ./bfs_shudu


### 数据导入

注意要先把原始数独数据录入到case1.in文件里, 该文件要和 bfs_shudu.cpp 在同一个目录

代码中第69行里也可以修改文件名来更换文件。

```
freopen("case1.in","r",stdin);  //读数独初始文件 
```

文件录入数据对应如下图： 注意如果空的位置需要用 . 来代替

![](https://gitee.com/zzyhd/sudoku/raw/master/pic1.png)

### 运行结果

如果找到任意一种合理的解法，会直接在屏幕上输出结果 

![](https://gitee.com/zzyhd/sudoku/raw/master/pic2.png)



如果因为输入数据有问题而无法找到解，会输出no answer. 

![](https://gitee.com/zzyhd/sudoku/raw/master/pic3.png)


如果输入数据中空格较多，有可能程序耗时会极长. 因为时间度具体我也算不清，但大概应该是 O(n^9) 



### 编程中遇到的困难

编程这个程序大概花了四个多小时，主要的难点就是如何判断一种填好的方案是否合法。

横行竖行都比较好判断，循环读一行或读一列然后用 set 数据结构判断是否有重复的 1，2，3.... 数字即可。

但怎么判断九个宫里每个宫是否有重复的数字比较麻烦，我想了好久也没想好用数学公式直接表达，

![](https://gitee.com/zzyhd/sudoku/raw/master/pic0.png)

后来无奈之下只好写了一个二维数组直接把数据位置全标出来，如下所示：

```
int gong[9][9]={{0,1,2,9,10,11,18,19,20},  //九个宫的位置 
		{3,4,5,12,13,14,21,22,23},
                {6,7,8,15,16,17,24,25,26},
		{27,28,29,36,37,38,45,46,47},
		{30,31,32,39,40,41,48,49,50},
		{33,34,35,42,43,44,51,52,53},
		{54,55,56,63,64,65,72,73,74},
		{57,58,59,66,67,68,75,76,77},
		{60,61,62,69,70,71,78,79,80}}; 
```


如果哪位同学有更好的办法，欢迎帮我改正。

### 最后想说的是

虽然我那时候这个程序拿到班上后在班级引起轰动，其它小组的同学都跑来借用我的程序，当时算是在同学们中间小秀了一把。

可惜很快数学创新活动结束，我这个程序也就没用了。 

这里开源出来，希望能有更多的同学来共同学习C++编程。

### 文件列表

| filename | comments | upload date |
|---|---|---|
| LICENSE | add LICENSE. | 19小时前 |
| bfs_shudu.cpp | 更新无法读入输入文件判断 | 19小时前 |
| case1.in | 初次上传 | 20小时前 |
| case2.in | 初次上传 | 20小时前 |
| case3.in | 初次上传 | 20小时前 |
| pic0.png | 初次上传 | 20小时前 |
| pic1.png | 初次上传 | 20小时前 |
| pic2.png | 初次上传 | 20小时前 |
| pic3.png | 初次上传 | 20小时前 |
| readme.md | 更新介绍 | 19小时前 |

### [bfs_shudu.cpp](https://gitee.com/zzyhd/sudoku/raw/master/bfs_shudu.cpp)

```
#include <iostream>
#include <queue>//队列，先进先出
#include <set>//集合，元素不重复
using namespace std;

//结构体，用于存储当前的一个解（可以是未完成的，但是经过了check，一定是合乎规则的）
struct S {   
    int a[81];  
};
S node; 
//任务队列
queue<S> q;

//区分整个81个格子，是在9宫中的哪一个宫
int gong[9][9]={{0,1,2,9,10,11,18,19,20},  //nine gong 
			{3,4,5,12,13,14,21,22,23},
            {6,7,8,15,16,17,24,25,26},
			{27,28,29,36,37,38,45,46,47},
			{30,31,32,39,40,41,48,49,50},
			{33,34,35,42,43,44,51,52,53},
			{54,55,56,63,64,65,72,73,74},
			{57,58,59,66,67,68,75,76,77},
			{60,61,62,69,70,71,78,79,80}}; 

//检查是否符合规则
bool check(int a[]) {  // recive a[0],a[1].....a[80]  
	set<int> s;  
	//每行无重复吗？
	for(int i=0;i<9;i++) {   // row 
		s.clear(); 
		for(int j=0;j<9;j++) {
			if(a[i*9+j]>0) {
				if (s.count(a[i*9+j]))
					return false;
				s.insert(a[i*9+j]);
			}
		} 
	}
	//每列无重复吗？
	for(int i=0;i<9;i++) {   // col 
		s.clear(); 
		for(int j=0;j<9;j++) {
			if(a[i+j*9]>0) {
				if (s.count(a[i+j*9]))
					return false;
				s.insert(a[i+j*9]);
			}	
		} 
	}
	//九宫中每宫无重复吗？
	for(int i=0;i<9;i++) {  // nine gong
		s.clear(); 
		for(int j=0;j<9;j++) {
			if(a[gong[i][j]]>0) {
				if (s.count(a[gong[i][j]]))
					return false;
				s.insert(a[gong[i][j]]);
			}		
		} 
	}
	//全部3项检查通过
	return true;
}

//打印某个解
void print(int a[]) {    // print  
	cout << "a complete success !" << endl;
	cout <<"----------------------"<<endl;
    for(int i = 0; i < 9; i++) {
        for(int j = 0; j < 9; j++) {
            cout << a[i*9+j] << " ";
            if((j+1) % 3 == 0)  cout << "| ";
        }
        cout << endl;
        if(i==2 || i==5)  cout << "----------------------|"<<endl;
    }
}

//主函数
int main() {
    //读取一道题，存入node中
    freopen("case1.in","r",stdin);  
    int step =0 ;   
    for(int i = 0; i < 81; i++) {  //read a[0]...a[80] 
        char c;
        cin >> c;
        if (c=='.') {
            node.a[i] = 0; 
            step++;   
        } else {
            node.a[i] = c - '0';  
        }
    }
    //如果读到了什么就认为输入正常，显示当前的题目；否则退出
    if (step) {
        cout << "solving data ." << endl;
    } else {
        cout << "no input file." << endl;
        return 0;
    }
    bool flag = false;
    //任务队列中第一个就是当前题目
    q.push(node);    
    while( !q.empty()) {  // bfs
        //取出任务队列中第一个
        node = q.front();   
        q.pop();
        //找到第一个没有确定的数字的位置
        int num = -1;
        for(int i=0;i<81;i++) { // find next
            if(node.a[i]==0) {
                num = i;
                break;
            }     
        }
        if(num == -1) {
            //全部数字都填完了打印当前解，退出循环
            print(node.a);   
            flag = true;  // win 
            break;    
        }
        //在这个位置上，试填1-9，如果合乎规则，作为下一步的任务放到队列里
        for(int i = 1; i <= 9; i++) {  
            node.a[num] = i;
            if (check(node.a)) {  // if  true
                q.push(node);     
            }    
        }
    }
    if (!flag) cout << "no answer." << endl;  
    return 0;
}
```

# [**BFS 解数独程序** 响应 @yuandj 袁老师的号召，把我以前做的BFS数独程序整理后开源发布。](https://gitee.com/zzyhd/siger/issues/I5VXZI)

> 追忆袁老师的社区起点，可是有两大法宝，一个是叫火焰棋，国际象棋变体，一个就是数独变体，小骄傲下，首届北京市数独锦标赛，袁老师带队的光华路小学夺得了团体第2名，之后一路开挂，多届比赛，团体个人获奖无数。现在有一个王牌在手，数独变体，火焰数独，吉祥数独，故事太长，有机会再唠嗑。

> 看着 @zzyhd 的朝气蓬勃，就满心欢喜。大大地分享起来吧。我想想看怎么送上一份礼物呢？

> ![输入图片说明](https://gitee.com/zzyhd/suduku/raw/master/pic0.png "在这里输入图片标题")

> 一看就是自己个的作品，配个啥封面呢？再想想看。 @zzyhd 应该建立自己的个人频道了，多些其他方面的分享，解数独这个是头条，封面故事。

- [BFS——广度优先算法（Breadth First Search）](https://gitee.com/zzyhd/sudoku/issues/I5VY3Z)
- [Codeheir：What is Breadth-First Search (BFS)](https://gitee.com/zzyhd/sudoku/issues/I5VY57)
- [PostgreSQL机器学习插件MADlib实践 Breadth-First Search篇](https://gitee.com/zzyhd/sudoku/issues/I5VY5G)

### BFS——广度优先算法（Breadth First Search）

sooner高

于 2017-07-27 10:56:16 发布

![输入图片说明](https://foruda.gitee.com/images/1665904129808210496/f64b3300_5631341.png "屏幕截图")


```
class Solution(object):
    def ladderLength(self, beginWord, endWord, wordList):
    # beginWord为开始的单词，endWord为目标单词，wordList为可供转换的元素组成的列表
        def construct_dict():
            d = {}
            for word in wordList:
                for i in xrange(len(word)):
                    s = s[:i] + "_" + s[i+1:]
                    d[s] = d.get(s, []) + [word]
            return d

        def bfs(begin, target, d):
            queue, visited = [(begin, 1)], set()
            while len(queue) > 0:
                word, steps = queue.pop(0)
                if word not in visited:
                    visited.add(word)
                    if word == target:
                        return steps
                    for i in xrange(len(word)):
                        s = word[:i] + "_" + word[i+1:]
                        for j in d.get(s, []):
                            # 如果j在visited中，说明j已经被访问过了，不需要再次进行访问。
                            if j not in visited:
                                queue.append((j, steps+1))
            return 0

        d = construct_dict()
        return bfs(beginWord, endWord, d)


```


56320
 收藏 175
————————————————
版权声明：本文为CSDN博主「sooner高」的原创文章，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接及本声明。
原文链接：https://blog.csdn.net/g11d111/article/details/76169861

### Codeheir
All the programming knowledge, in one bloody brilliant site.
Skip to content
Blog
Newsletter
Contact
About
5TH JUNE 2021LUKEGARRIGAN

 **[What is Breadth-First Search (BFS)](https://codeheir.com/2021/06/05/what-is-breadth-first-search-bfs/)** 

Introduction
In computer science, a search algorithm is a series of steps that can be used to find the desired state or a path to a particular state. In most scenarios, there will be additional constraints that will need to be fulfilled such as the time taken to reach the desired state, memory availability, maximum number of moves.

A classic example in the AI literature of pathfinding problems are the sliding-tiles puzzles such as the 3 × 3 8-puzzle, the 4 × 4 15-puzzle and the 5 × 5 24-puzzle. The 8-puzzle consists of a 3 × 3 grid with eight numbered square tiles and one blank. The blank is used to slide other tiles that are horizontally or vertically adjacent into that position in an attempt to reach the goal state. The objective is to rearrange the tiles from some random configuration to a specified goal configuration. The number of possible solvable states for the 8-puzzle is 9!/2 = 181440 so can be solved by means of brute-force search. However for the 15-puzzle with 16!/2 ≈ 1.05×1013 and 24-puzzle with 25!/2 ≈ 7.76×1024 a more sophisticated informed search is required.

Uninformed Search
Uninformed or brute-force search is a general problem-solving technique that consists of systematically enumerating all the possible states for a given solution and checking to see whether that given state satisfies the problem’s statement. All that is required to execute a brute-force search is some legal operators, an initial state and an acknowledged goal state. Uninformed search generates the search tree without using any domain-specific knowledge.

Completeness and optimality
Often in search, the input may be an implicit representation of an infinite graph. Given these conditions, a search algorithm is characterised as being complete if it is guaranteed to find a goal state provided one exists. Breadth-first search is complete and when applied to infinite graphs it will eventually find the solution. Depth-first search is not complete and may get lost in parts of the graph that do not contain a goal state.

Breadth-First Search
Breadth-first search is one of the simplest algorithms for searching a graph, it expands the nodes in a tree in the order of their given distance from the root, so it expands all the neighbouring nodes before going to the next level of the tree. The algorithm doesn’t trawl to the deeper levels of the tree without first expanding the lower levels thus ensures the finding of the shortest path.

The space requirement of breadth-first search is its largest deficiency. The 8-tile has a search space of 9!/2 = 181,400 states with a maximum number of 31 moves to solve. In terms of practicality, with larger problem states such as the 15-tile puzzle, a breadth-first search will exhaust the available memory rather quickly with its 16!/2 = 10,461,394,944,000 solvable states and a maximum number of 80 moves.

The below image, taken from the blog BFS vs DFS is great way of visualising how the different algorithms expand a tree:

BFS vs. DFS - Open4Tech
Implementation
To demonstrate breadth-first search I’ve implemented the sliding-tile puzzle, all the source code for the project can be found here.

manually moving 8 tile
Which also scales:

Manually moving 15 tile
The algorithm
The algorithm is really simple, each state is just an array, so the goal state is [0, 1, 2, 3, 4, 5, 6, 7, 8]. To begin with each state gets added to a queue and a seen array. For a given state from the queue we add its neighbours to the queue which’ll eventually get evaluated too. The seen array is just to ensure we don’t add stuff to the queue that we’ve already seen – (There’s multiple ways to get to the same state). Each state is compared with the goal state, and if it’s the same then we return.

 solve(puzzle, goal) {
        let seen = [puzzle];
        let queue = [puzzle];
        while(queue.length > 0) {
            let current = queue.shift();

            if (this.isEqual(current, goal)) {
                return current;
            }

            for (let neighbour of Puzzle.getNeighbours(current)) {
                if (!this.isInSeen(seen, neighbour)) {
                    seen.push(neighbour);
                    queue.push(neighbour);
                } 
            }
        }
    }
Testing our algorithm
8 TILE
Let’s start with the 8 tile, and create a problem state that is 10 moves away from the goal state:

BFS solved the problem in 0.014s with the optimal number of moves (10). Only having to expand 1060 states.

8 tile 10 moves away bfs
Next I’m going to up the number of random moves from the goal state to 20:

8 tile 20 moves away bfs
Notice how this time it only took 16 moves even though I randomly walked 20 moves from the goal state, implying it found a better solution than the path the random walker took.

The number of states expanded rose sharply to 16000. You can see how this could get out of hand very quickly.

15 TILE
Let’s try the same experiments on the 15 tile problem. With the algorithm being ran in the browser, my assumption is that we’ll exceed the memory limit and likely crash the browser – worth a shot anyway.

10 random moves from the goal

15 tile 10 moves away BFS
9246 expanded states, not too bad.

20 random moves from the goal

15 tile bfs crashing browser
Just as I had expected, it crashed the browser, and also crashed my website so I lost part of my blog!

Informed search
As mentioned previously in order to solve the 15 tile – and even difficult configurations of 8 tile – we’d need to utilise an informed search algorithm. Uninformed search often expands states that pursue a direction alternative to the goal path, which can lead to searches taking an extensive amount of time and/or space. Informed search attempts to minimise this by producing intelligent choices for each selected state. This implies the use of a heuristic function which evaluates the likelihood that a given node is on the solution path. A heuristic is a function that ranks possible moves at each branching step to decide which branch to follow.

The goal of a heuristic is to produce a fast estimation of the cost from the current state to the desired state, the closer the estimation is to the actual cost the more accurate the heuristic function. In the context of the sliding-tile puzzle, to find best move from a set configuration the heuristic function is executed on each of the child states, the child state with the smallest heuristic value is chosen.

My next blog will be solving the sliding-tile puzzle using informed search, particularly the A* algorithm.

Check out my previous blog What Is Simulated Annealing? – that was a really fun one.

Treat me to a coffee ☕


![输入图片说明](https://foruda.gitee.com/images/1665905433583627345/a75a2a12_5631341.gif "bfsgify.gif")

### [PostgreSQL机器学习插件MADlib实践 Breadth-First Search篇](https://blog.geohey.com/postgresqlji-qi-xue-xi-cha-jian-madlibshi-jian-breadth-first-searchpian/)

![输入图片说明](https://foruda.gitee.com/images/1665905136366282620/29a97409_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1665905142828003594/6a8f7bb6_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1665905358273258789/450ec4f5_5631341.png "屏幕截图")