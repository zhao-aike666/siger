- 【[折纸-教程](https://www.bilibili.com/video/BV1NA4y1o716)】[端午粽香，赛龙舟](https://gitee.com/yuandj/siger/issues/I4UTYE)！
-   [ORIPA 折纸图案编辑器](https://gitee.com/yuandj/siger/issues/I4UTYE#note_10685413_link)：[绘制燕式纸飞机](https://gitee.com/yuandj/siger/issues/I4UTYE#note_10701874_link)
-   [有哪些折纸大师和其作品？](https://gitee.com/yuandj/siger/issues/I4UTYE#note_10702835_link)[知乎](https://www.zhihu.com/question/29295619)
- 《[折叠之间](https://gitee.com/yuandj/siger/issues/I4UTYE#note_10703279_link)》[Origami - Between The Folds](https://www.bilibili.com/video/BV1Dx411w7mk)
- 《[华丽的折纸背后的数学](https://gitee.com/yuandj/siger/issues/I4UTYE#note_10703376_link)》[Robert Lang](https://www.bilibili.com/video/BV144411v7bw)
- 《[可编程折纸超材料设计](https://mp.weixin.qq.com/s?__biz=MzU5MzQ0OTc2Mg==&mid=2247484394&idx=1&sn=8562c698901e14dc02436a113f68ccec)》[陈焱（天津大学教授）](https://gitee.com/yuandj/siger/issues/I4UTYE#note_10703908_link)
- 《[现代折纸艺术](https://mp.weixin.qq.com/s?__biz=MzA4OTA3MDYwNA==&mid=2651308503&idx=1&sn=0febcf65b66f7ce80e7563210fac8677)》[首屈一纸](https://gitee.com/yuandj/siger/issues/I4UTYE#note_10702757_link)（[天大优秀社团](https://gitee.com/yuandj/siger/issues/I4UTYE#note_10703617_link)）

<p><img width="706px" src="https://images.gitee.com/uploads/images/2022/0604/012903_d92a0a62_5631341.jpeg" title="折叠宇宙副本.jpg"></p>

下面两段话分别出自《[折叠之间](https://www.bilibili.com/video/BV1Dx411w7mk)》的片尾以及观众热评，其中片尾反复重复的 “喜欢” “就是喜欢” 就是 SIGer 的样貌啦，说出这话的小伙子，12岁大学毕业，20岁博士学位，麻省理工学院 MIT 有史以来最年轻教授，他的榜样就是他的父亲 Martin Demaine（雕塑家&工程师），用折纸设计的太空折叠望远镜已经飞上太空，完美地诠释了艺术和科技的连接。而观众热评，说出了所有人的心声，心生对科学的向往 —— 数学，计算几何 ……

>  **你为什么这么痴迷，这么喜欢她？** 就因为她有趣？我研究它只是因为它有趣，没有其他原因。即使有，如果他令人厌烦，我也不会做，他必须能吸引。这是我的生存哲学，水玻璃因为它有趣，写喜剧因为它有趣，折纸也因为他有趣。我最初研究者只是因为我对其中的数学原理很感兴趣。现在我折纸完全因为折纸本身就。很好玩儿以说一个人一个活法，但我仍觉得人人都应该做自己觉得有趣的事。两父子的经历告诉我们，艺术家和科学家似乎并不像我们想的一样南辕北辙，他们都在研究我们所处的世界，我们共同分享的世界。（Erik Demaine）

>  **薄荷微苦** : 看了这个视频，我对数学有了新的看法。数学不是自己想的那么狭隘，不是那么的惹人厌烦。数学有这样或那样的用途。或者说，每个领域都有其存在的意义。这个世界，我们人类生活了那么久，仍旧没有摸透它。每个人徜徉在知识海洋中，了解的也许连一滴水都没有。但是，借着前辈的肩膀，必然有人会用自己一滴水不到的知识和短暂的生命，绽放一次焰火。

同样的一段采访《[科技日报：陈焱： 把“纸”从理论“折”进应用](http://www.tju.edu.cn/info/1182/3772.htm)》，让我们看到了同样的热爱，同样的榜样：

> 陪孩子玩，我觉得非常开心，完全不觉得累。现在儿子已经14岁了，我平时还会抽空做做他的几何题。儿子如果发现我找到了更简便的解题方法，就会主动找我切磋切磋。（前面那对MIT父子共同解题，“ **[一刀剪](http://www.mathchina.com/bbs/forum.php?mod=viewthread&tid=2047191)** ”—— **经过有限次折叠，再一刀剪下去，可以得出任何图形** 其中包括 “五角星” five-pointed star ）

最近我在读《[钱学森的航天岁月](https://mp.weixin.qq.com/mp/homepage?__biz=MjM5NjUwMDYyMQ==&hid=1&sn=56c217a8c24f6ab07438d0eb0c0e62b3&scene=18&devicetype=android-28&version=28001657&lang=zh_CN&nettype=WIFI&ascene=59&session_us=gh_1308f462066c&pass_ticket=4S0nSn3bEnRqWrJsu9m5z5x17sMvvBKWqlvjarcUuYWr7C8Tk9oUnB9RA9GNvrH%2B)》，同为力学研究，同样在航天领域的应用，钱老的遗憾就是没能给儿子每周出一道数学题，“否则，任何一所高校，任由儿子上！”。写到这里，我有一个冲动，希望能找陈焱老师要些她给儿子出的几何题 :pray: 

无论如何，这期专题我要感谢我家小袁和妹妹，端午节因为封控，他们派爸爸代表，回天津陪奶奶过端午，临行前我问他们有什么礼物带给奶奶，就有了开篇的《[端午粽香，赛龙舟](https://gitee.com/yuandj/siger/issues/I4UTYE#note_10677488_link)》，而这个专题的立项时间是 2月14日 前夜，[看那满桌的玫瑰](https://gitee.com/yuandj/siger/issues/I4UTYE)，就知道是干啥用的啦 :heart: 这就是生活的味道，香甜软糯。我希望通过这个专题，能够有更多同学爱上折纸，无论是生活，是艺术，还是科学，它都能给你带来无尽的快乐和满足感 :+1: 

> ps: 本期封面没有选用 科技主题的元素，用最传统的 纸鹤与幸运星，因为一切皆源自对生活的热爱 :pary: