- [马斯克：如果重回20岁，我给自己的三个建议](https://www.bilibili.com/video/BV1z24y197U9)
- ['Liftoff': Eric Berger's tale of the wild years of SpaceX's youth](https://www.space.com/liftoff-spacex-early-days-book-eric-bergeR)
- [See the evolution of SpaceX rockets in pictures](https://www.space.com/40547-spacex-rocket-evolution.html)
- [《Liftoff》节选一 SpaceX招人](https://www.bilibili.com/read/cv12843562)
- [LIFTOFF 马斯克（Elon Musk）与SpaceX早期创业史(4): 首飞失利](https://www.bilibili.com/read/cv12508979)

<p><img width="706px" src="https://foruda.gitee.com/images/1661590880420391907/315ac83a_5631341.jpeg" title="255LIFTOFF.jpg"></p>

> 就剩下这个读书笔记啦。！

昨天整理个人的 Issues 写下的这句，今天就实现了，越来越喜欢火星学会的群，各种仰望星辰大海的 “仁人志士” 均在群里分享。本片读书笔记注水了，皆来自网友的摘录，最纠结的是分明注释了禁止转载，还是掩耳盗铃，是 B站的默认选项，因此，请同学们按图索骥，前往 UP主的空间阅读，点赞吧。

> 本片笔记的标题应该叫《和优秀的人一起品读生活》，这个概括来自昨天群里网友分享的《马斯克：如果重回20岁，我给自己的三个建议》，刚完成了 OPEN AI DAY 2022 的专题报道，这个提问环节一下抓住了我的心，这不就是 SIGer 努力营造的学习环境吗？而读书的理解，我从前两人 “星际航行” 的果老师处体会到了，写书的人都会将内心的凝练沉淀其中，无论是知识，还是眼界，凡是能著书立说之人，皆是优秀的啦。这里自夸下，制作封面，写摘要，成为了我这个 SIGer 代理主编的嗜好，就是为了留下些想法，这样说来，所有转载，并无冒犯之意，算是旁征博引，借花献佛的引用啦。直接上文... 

  （PS：我转载的时候，喜欢逐个段落复制，保留原文的 BOLD 等格式信息，这是强迫自己再扫一遍，再思考一遍，与其说转载，不如说是在思考，最终一篇篇靓丽封面，成为了我的话语，传递给同学们，连接起 SIGer 星云中诞生的你们 耀眼的群星。:pray:）

![输入图片说明](https://foruda.gitee.com/images/1665451520450699154/6df3159b_5631341.png "屏幕截图")

# [马斯克：如果重回20岁，我给自己的三个建议](https://www.bilibili.com/video/BV1z24y197U9)

### 问：

我很好奇，如果你能回到20岁  
I'm curious uh if you are back to your 20s uh

有哪些事情是你当时希望你知道的  
what are some of the things you wish you knew back then

你会给年轻时的自己什么建议？  
what are some advice you would give to your younger self

### 马斯克：

我想，首先与优秀的人同行，越多越好  
I think just don't try to expose yourself to as many smart people as possible

然后，多读书，阅读大量的书籍  
and read a lot of books

第三，我觉得很多人的生活不应该过度紧张  
I think there's some America to just also like not being like necessarily too intense

也要适当的享受当下的时光  
and and I like enjoying the moment a bit more

我会对20岁的我，或20多岁的年轻人说：  
I would say to 20 or 20 somebody like me

你知道的  
just a you know

心有猛虎，也要细嗅蔷薇  
just stop and smell the roses occasionally

或许这将会是一个很好的想法  
would probably be a good idea

就像我们研制猎鹰一号火箭一样  
It's like when we're developing the falcon one rocket

在四边形的塔尖上  
and on the quadriline attall

我们在这个美丽的小岛上开发火箭  
and we had this beautiful little island that were developing the rocket on

在那时候，我竟然一次都没有  
and not once during that entire time

在那美丽的海滩上喝杯啤酒  
did I even have a drink on the beach

我想，我应该适度享受下，去沙滩上喝杯啤酒  
I'm like well I should have had a drink on the beach

当时如果这样的话就更棒了  
that would have been fine

# ['Liftoff': Eric Berger's tale of the wild years of SpaceX's youth](https://www.space.com/liftoff-spacex-early-days-book-eric-bergeR)

By Elizabeth Howell published March 04, 2021

![输入图片说明](https://foruda.gitee.com/images/1661589311322172215/5f24a23d_5631341.jpeg "EXv5zkMkSobidrnvoUVggM.jpg")

> "Liftoff: Elon Musk and the Desperate Early Days That Launched SpaceX" by Eric Berger (William Morrow, 2021). (Image credit: William Morrow/Amy Carson Photography)

Long before SpaceX's self-landing rockets, Tesla-riding space mannequin or Starship prototype tests for future Mars missions, the California company was already doing daring things in space exploration.

Veteran Houston-based space reporter Eric Berger, now of [Ars Technica](https://arstechnica.com/author/ericberger/) and formerly of [The Houston Chronicle](https://www.houstonchronicle.com/author/eric-berger/), tackles the early years of SpaceX in his new book "[Liftoff: Elon Musk and the Desperate Early Days That Launched SpaceX](https://www.amazon.com/Liftoff-Desperate-Early-Launched-SpaceX/dp/0062979973)" (William Morrow, 2021). The book, while focusing heavily on SpaceX's early years, shows the roots of the daring steps the now-famous company is taking into even newer frontiers: human missions and Mars exploration.

You'll read about the development of SpaceX's first rocket, the Falcon 1, built at a time when few companies dared to create these flying machines themselves. It took four tries to even get the rocket safely into orbit, and Berger's book shows the conversations and innovation that [SpaceX](https://www.space.com/18853-spacex.html) embraced over several years to get Falcon 1 launched safely.

Related: [See the evolution of SpaceX's rockets in pictures](https://www.space.com/40547-spacex-rocket-evolution.html)

![输入图片说明](https://foruda.gitee.com/images/1665452476821785252/0c2582e3_5631341.png "屏幕截图")

> Elon Musk watches the liftoff of Falcon1 rocket from Omelek Island in the Kwajalein Atoll, on Sept. 29, 2008.  (Image credit: Axel Koester/Corbis via Getty Images)

Berger told Space.com that when [Falcon 1 finally made it to space](https://www.space.com/5905-spacex-successfully-launches-falcon-1-rocket-orbit.html) on Sept. 28, 2008, he didn't notice. But he did have a valid excuse — Hurricane Ike had just hit Houston and he, along with many other reporters in the city, pulled many extra hours to keep the community informed. 

"I was completely swamped in coverage for that storm and I was completely oblivious," he told Space.com in an interview. Even Berger's home field of space was busy, as at least [one space station delivery](https://www.space.com/5833-hurricane-ike-delays-space-station-delivery.html) was delayed due to Ike and NASA was working extra hours itself to [keep space shuttle flights running on time](https://www.space.com/5846-nasa-hurricane-won-delay-shuttle-flights.html) — and safely.

But Berger has been following SpaceX closely in the years since, and decided to write the book after witnessing the spectacular debut launch of the company's new [Falcon Heavy rocket](https://www.space.com/39779-falcon-heavy-facts.html) at NASA's Kennedy Space Center in Florida, in 2018. That mission saw the booster rockets safely self-land near the launch site, as a Tesla soared into space with a [mannequin nicknamed "Starman](https://www.space.com/39947-spacex-falcon-heavy-starman-launch-inspiring-video.html)."

"I realized not just that SpaceX was a super interesting company, but this really was a transformative space company of my generation," Berger said. "There's been a lot of efforts in the past to do what SpaceX has done, but they failed. I wanted to see how they succeeded."

Berger focused on the Falcon 1's development, testing and launch as SpaceX — being such a small company at the time — didn't receive nearly as much media coverage about that rocket as subsequent generations. He recalls, like many other reporters of the era, having a "healthy skepticism of all the grand claims" SpaceX was discussing at the time — like building a spaceship called [Dragon](https://www.space.com/18852-spacex-dragon.html) (which has now flown 20  cargo missions and two crewed missions to the International Space Station and counting) or making launches happen with reusable, landable rockets — now a fairly routine thing after many failed, explosive tests.

Looking back at SpaceX's claims, Berger says he has come to realize those claims had value and vision: "It wasn't going to be done on time, but they probably would get there," he said. Berger urged everyone to therefore keep an eye on the early Starship development; while [prototypes are exploding now](https://www.space.com/spacex-starship-sn9-test-flight-launch-explosion), SpaceX is just getting started.

Fortunately for the book, the early part of Berger's research in 2019 took place well before the novel [coronavirus pandemic](https://www.space.com/topics/coronavirus), allowing Berger to make several visits to the SpaceX factory in Hawthorne, California and to do interviews in person. Once the pandemic erupted in the spring of 2020, naturally he pivoted the remaining work to phone interviews and remote research. Berger added he was able to find time for writing in between his normal duties at Ars Technica, as he has a flexible work schedule — but a lot of writing also took place at night, away from family duties that normally take up much of his time.

Readers new to SpaceX may be disappointed that Berger stops the story after Falcon 1's first flight, but he said he hopes to one day carry on the tale to the modern day in more depth; the book briefly touches upon the last decade of SpaceX work, but more could be said.

"I think the key message is the world's most interesting space company almost didn't exist, and if it hadn't been for these crazy adventurers — for a small group of engineers — SpaceX would not exist," Berger added. "You wouldn't see drone ship landings on the Atlantic Ocean, or the Falcon Heavy launches, or Crew Dragon missions to the International Space Station. SpaceX is a great American success story and it was fun to go back and find out how it happened."

![输入图片说明](https://foruda.gitee.com/images/1665452835076988866/7eb4637a_5631341.png "屏幕截图")

> Elon Musk sits at his desk in El Segundo, Los Angeles, on March 19, 2004. (Image credit: Paul Harris/Getty Images)

You can buy "Liftoff: Elon Musk and the Desperate Early Days That Launched SpaceX" by Eric Berger on Amazon.com as [hardcover book](https://www.amazon.com/Liftoff-Desperate-Early-Launched-SpaceX/dp/0062979973) ($23.79), [Kindle e-book](https://www.amazon.com/Liftoff-Desperate-Early-Launched-SpaceX-ebook/dp/B088FQK2K2/ref=tmm_kin_swatch_0?tag=georiot-us-default-20) ($14.99), or an [audiobook](https://www.amazon.com/Liftoff-Desperate-Early-Launched-SpaceX/dp/B089QRXBXB/ref=tmm_aud_swatch_0?tag=georiot-us-default-20) narrated by Rob Shapiro ($21.55). You can also [read excerpts of "Liftoff" on Ars Technica](https://arstechnica.com/science/2021/03/after-two-failures-spacex-needed-a-win-in-2008-would-it-get-one/) and [Space News](https://spacenews.com/book-excerpt-liftoff-elon-musk-and-the-desperate-early-days-that-launched-spacex/). 

 _Follow Elizabeth Howell on Twitter @howellspace. Follow us on Twitter @Spacedotcom and on Facebook.  _ 

[Join our Space Forums](https://forums.space.com/) to keep talking space on the latest missions, night sky and more! And if you have a news tip, correction or comment, let us know at: community@space.com.

---

 **[Elizabeth Howell](https://www.space.com/author/elizabeth-howell)** 
- Staff Writer, Spaceflight

Elizabeth Howell, Ph.D., is a staff writer in the spaceflight channel since 2022. She was contributing writer for [Space.com](http://space.com/) for 10 years before that, since 2012. As a proud Trekkie and Canadian, she also tackles topics like diversity, science fiction, astronomy and gaming to help others explore the universe. Elizabeth's on-site reporting includes two human spaceflight launches from Kazakhstan, three space shuttle missions in Florida, and embedded reporting from a simulated Mars mission in Utah. She holds a Ph.D. and M.Sc. in Space Studies from the University of North Dakota, and a Bachelor of Journalism from Canada's Carleton University. Elizabeth is also a post-secondary instructor in communications and science since 2015. Her latest book, Leadership Moments from NASA, is co-written with astronaut Dave Williams. Elizabeth first got interested in space after watching the movie Apollo 13 in 1996, and still wants to be an astronaut someday.

['Liftoff': Eric Berger's tale of the wild years of SpaceX's youth | Space](https://www.space.com/liftoff-spacex-early-days-book-eric-bergeR)  
space.com|1541 × 1125 jpeg|Image may be subject to copyright.

> 就剩下这个读书笔记啦。！

# [See the evolution of SpaceX rockets in pictures](https://www.space.com/40547-spacex-rocket-evolution.html)

By Elizabeth Howell published May 21, 2020

![输入图片说明](https://foruda.gitee.com/images/1665453359182374676/34af566f_5631341.png "屏幕截图")

> SpaceX

SpaceX was born in 2002, when its founder, billionaire Elon Musk, took the first steps in his grand ambition to send a mission to Mars. Today, the company is way beyond the space startup stage.

The Hawthorne, California-based company regularly reuses rockets, sends cargo missions to the International Space Station with its Dragon spacecraft and will fly astronauts for NASA and others, too. SpaceX has also launched the massive Falcon Heavy and has plans for an even larger rocket to reach the moon, Mars and beyond: the Starship and its Super Heavy booster.

Read more about SpaceX's history of rockets and spacecraft development in the following slideshow. 

 **Editor's note** : This story, first published May, 10 2018, was updated on May 20, 2020.

### FALCON 1: SPACEX'S FIRST ROCKET

The Falcon 1 was the first rocket manufactured by SpaceX. It had a proposed capacity to carry 670 kilograms (1,480 lbs.) to low Earth orbit, and it flew between 2006 and 2009. 

After three launch failures, Falcon 1 [sent a dummy payload to space on Sept. 29, 2008](https://www.space.com/5905-spacex-successfully-launches-falcon-1-rocket-orbit.html). Its fifth and final launch, on July 14, 2009, sent RazakSAT, a Malaysian Earth-observation satellite, into orbit. 

![输入图片说明](https://foruda.gitee.com/images/1665453458040074330/249b700e_5631341.png "屏幕截图")

> SpaceX

Falcon 1 rockets launched from Omelek Island, part of the Kwajalein Atoll in the Pacific Ocean. The 68 feet tall (21 meters) rocket was powered by a single engine (hence the "1" in its name) and ran on liquid oxygen and rocket-grade kerosene.

And in case you're wondering, Musk named the Falcon rockets after the Millennium Falcon ship from "Star Wars."

### DEVELOPING FALCON 9: SPACEX'S ROCKET SEQUEL TO FALCON 1

![输入图片说明](https://foruda.gitee.com/images/1665453487766763108/ec41af60_5631341.png "屏幕截图")

> SpaceX

SpaceX soon received interest from several companies looking for a heavier-lift rocket. The company had considered developing an intermediary rocket [called the Falcon 5](https://www.space.com/18962-spacex-falcon-9.html), but instead skipped ahead and began work on the [Falcon 9](https://www.space.com/18962-spacex-falcon-9.html) (because its first stage used a cluster of nine engines.

This rocket can send a payload to low Earth orbit weighing up to 28,991 lbs. (13,150 kg). It is a two-stage rocket that at stands 230 feet (70 meters) tall and is 12 feet (3.7 m) wide. [SpaceX first advertised plans for the Falcon 9](https://www.spacex.com/falcon9) in 2005 and sent the debut Falcon 9 aloft on June 7, 2010, from Cape Canaveral Air Force Station in Florida. 

Early customers of the rocket included Bigelow Aerospace; Avanti Communications; and MacDonald, Dettwiler and Associates.

### REUSING ROCKETS: SPACEX'S GOAL TO REDUCE SPACE COSTS

![输入图片说明](https://foruda.gitee.com/images/1665453598363661275/6f5b18ff_5631341.png "屏幕截图")

> SpaceX

Right from the beginning of Falcon 9's history, SpaceX was interested in reusing the first stage of the rocket to [save on launch costs](https://www.space.com/25562-spacex-falcon-9-reusable-rocket-test.html). 

Early tests of the landing were unsuccessful, however. SpaceX made attempts on the first, second and sixth launches of the Falcon 9 to control the booster's landing, but in each case, the stage slammed down into the ocean. You can watch a supercut of those failed landings from SpaceX below.

SpaceX finally achieved a [controlled ocean landing](https://www.space.com/25636-spacex-reusable-rocket-test-elon-musk.html) on Falcon 9's ninth launch (the fourth controlled-landing attempt), on April 18, 2014. This was an important stepping stone in the path to eventual reusability.

The [first successful landing of a Falcon 9 rocket](https://www.space.com/31420-spacex-rocket-landing-success.html) occurred on Dec. 21, 2015 at Landing Zone 1, a SpaceX pad at the Cape Canaveral Air Force Station in Florida.

### DRAGON DREAMS: SPACEX'S FIRST SPACE CAPSULE

![输入图片说明](https://foruda.gitee.com/images/1665453802298334941/0fbbccda_5631341.png "屏幕截图")

> SpaceX

SpaceX kept the first 18 months of the [Dragon cargo ship](https://www.space.com/18852-spacex-dragon.html)'s development under wraps. Then, in March 2006, the company officially made Dragon public when the company submitted a proposal for NASA's Commercial Orbital Transportation Services (COTS) demonstration program. The ultimate goal was to develop a private spacecraft to ferry cargo to the International Space Station.

After Spacex hit several milestones, [NASA selected SpaceX's Dragon in December 2008](https://www.space.com/6257-nasa-taps-spacex-orbital-sciences-haul-cargo-space-station.html) to be one of the companies providing commercial resupply services to the space station. (The other company was Orbital Sciences, which later became Orbital ATK and now Northrop Grumman.) 

SpaceX's contract value at that time was a minimum of $1.6 billion, with options to extend to $3.1 billion; the company has since received a new contract for cargo launch services. 

Musk has confirmed that he named Dragon after "Puff the Magic Dragon.

### EARLY DRAGON FLIGHTS: SPACEX BEGINS NASA WORK

![输入图片说明](https://foruda.gitee.com/images/1665453869636964202/254779e6_5631341.png "屏幕截图")

> SpaceX

Dragon made a successful maiden flight on Dec. 8, 2010, from Cape Canaveral Air Force Station. Then, on May 22, 2012, [Dragon launched for an important test](https://www.space.com/15805-spacex-private-capsule-launches-space-station.html): an attempt to berth the spacecraft with the International Space Station. 

Dragon [made it to the station safely on May 25](https://www.space.com/15874-private-dragon-capsule-space-station-arrival.html) of that year, despite experiencing some problems with a laser system that was supposed to judge the craft's distance to the orbiting complex. The milestone prompted worldwide acclaim. It was the first time a private spacecraft docked with the space station. 

SpaceX has since upgraded its uncrewed Dragon cargo ships to be reusable for at least two flights.

### GRASSHOPPER: SPACEX TESTS ROCKET REUSABILITY

[Grasshopper](https://www.space.com/26042-spacex-grasshopper.html) was a 100-foot-tall rocket prototype flown at SpaceX's McGregor, Texas, proving grounds to give the company more experience in landing boosters vertically. 

While Grasshopper did not get as much media attention as SpaceX's other programs, it was key to furthering the development of Falcon 9's reusable first stage. 

![输入图片说明](https://foruda.gitee.com/images/1665454000468973929/2f985f80_5631341.png "屏幕截图")

> Chris Thompson/SpaceX

The Grasshopper rocket [made eight test flights between 2012 and 2013](https://www.space.com/23193-spacex-grasshopper-rocket-highest-hop-video.html), with the final flight seeing Grasshopper soar to 2,440 feet (744 meters).

The Grasshopper program was then retired so SpaceX could focus more resources on Falcon 9's development.

### FALCON 9 REUSABLE: THE F9R DEVELOPMENT ROCKET

SpaceX announced [the Falcon 9 Reusable Development Vehicle](https://www.space.com/25765-spacex-falcon9r-reusable-rocket-video.html) in 2012, which was based on the first stage of the Falcon 9. 

The company made five flights of this system at the SpaceX McGregor site between April and August 2014, with the maximum altitude on some flights exceeding 3,280 feet (1,000 m). 

The last booster, which launched on Aug. 22, 2014, exploded due to a blocked sensor.

### LANDING ZONE 1: SPACEX'S FIRST ROCKET LANDING PAD

![输入图片说明](https://foruda.gitee.com/images/1665454104984654427/a7060ec8_5631341.png "屏幕截图")

> SpaceX

This picture shows Landing Zone 1, a ground landing zone for the Falcon 9's first stage at Cape Canaveral Air Force Station. This is where SpaceX [made the first of its controlled ground landings](https://www.space.com/31420-spacex-rocket-landing-success.html), on Dec. 21, 2015. 

The company built the pad on land leased from the U.S. Air Force on Cape Canaveral Air Force Station. (Landing Zone 1 is on land that used to host Launch Complex 13.) This landing was extra-sweet because the previous Falcon 9 flight, in June 2015, [ended catastrophically with an explosion](https://www.space.com/29994-spacex-rocket-explosion-cause-faulty-strut.html).

### GROUND-BASED ROCKET LANDINGS FOR FALCON 9

![输入图片说明](https://foruda.gitee.com/images/1665454165159851158/3acb8cba_5631341.png "屏幕截图")

> SpaceX

[SpaceX's first successful Falcon 9 landing](https://www.space.com/31420-spacex-rocket-landing-success.html) in Landing Zone 1, on Dec. 21, 2015, was hailed as a milestone for rocket reusability. However, the company still tried to improve on that achievement. 

SpaceX experienced a mix of successful and [failed ocean landings in 2014 and 2015](https://www.space.com/38155-spacex-rocket-landing-explosions-blooper-video.html). 

 **[SpaceX's Epic Falcon 9 Rocket Landing in Pictures](https://www.space.com/31440-spacex-falcon9-rocket-landing-pictures.html)** 

In 2015, SpaceX was also trying to land on drone ships in the ocean. While these landings kept ending with failure, Musk would post the videos and pictures on his Twitter feed, acknowledging mistakes made, and the company would work on improving for the next flight.

### SPACEX'S 1ST DRONE SHIP LANDING

![输入图片说明](https://foruda.gitee.com/images/1665454245721162534/3f7a79c2_5631341.png "屏幕截图")

> SpaceX

The persistence exhibited by Musk and his employees finally paid off on April 8, 2016, [when a Falcon 9 first stage touched down softly on a drone ship](https://www.space.com/32517-spacex-sticks-rocket-landing-sea-dragon-launch.html) called "Of Course I Still Love You" in the Atlantic Ocean. The Dragon spacecraft that this Falcon 9 carried aloft also had a milestone flight, delivering an inflatable module — the [Bigelow Expandable Activity Module](https://www.space.com/32485-beam-bigelow-expandable-activity-module-pictures.html) — to the International Space Station.

SpaceX's success rate with drone landings improved drastically after the April 2016 flight, although some boosters still missed the mark from time to time. The company's Falcon 9 flight success rate is also strong; its last failure, in September 2016, saw a rocket explode on the launch pad before taking off. 

SpaceX has a second drone ship, "Just Read the Instructions," which is used for Pacific Ocean landings after launches from [Vandenberg Air Force Station](https://www.space.com/34147-vandenberg-air-force-base.html) in California. Both ships are named for fictional starships in the science fiction books of Iain M. Banks.

### FALCON HEAVY: SPACEX'S FIRST HEAVY-LIFT ROCKET

SpaceX's [Falcon Heavy](https://www.space.com/39779-falcon-heavy-facts.html) rocket, a heavy-lift version of the Falcon series, [made its  successful debut flight on Feb. 6, 2018](https://www.space.com/39607-spacex-falcon-heavy-first-test-flight-launch.html), launching from NASA's Pad 39A at the Kennedy Space Center in Florida. 

The Falcon Heavy is currently the most powerful rocket in use today. It consists of three first-stage core boosters based on SpaceX's workhorse Falcon 9 rocket and a powerful upper stage.

![输入图片说明](https://foruda.gitee.com/images/1665454417883469312/8d2180a3_5631341.png "屏幕截图")

> SpaceX

The rocket successfully [launched a Tesla car into space](https://www.space.com/39720-spacex-starman-elon-musk-roadster-journey-video.html) and a spacesuit clad mannequin called Starman on its first flight. (Musk is also the CEO of Tesla Motors.) The Falcon Heavy is 230 feet tall (70 m) and can lift nearly 141,000 lbs. (64 metric tons) of payload to low Earth orbit. 

This is twice what its closest competitor, the United Launch Alliance's Delta IV Heavy, can hoist into orbit.

### FALCON HEAVY ROCKET LANDINGS: SPACEX'S BALLET

This picture shows [Falcon Heavy's twin side boosters landing successfully](https://www.space.com/39618-elon-musk-falcon-heavy-spacex-reaction.html) after the rocket's maiden flight, on Feb. 6, 2018. 

While the booster stages touched down safely at Landing Zones 1 and 2 at Cape Canaveral Air Force Station, near the Kennedy Space Center, the rocket's core stage hit the ocean at high speed. 

The rocket stage carrying the Tesla car underwent one last burn to send the car toward the orbit of Mars. However, as Space.com sister site Live Science reported, [radiation could destroy the car within a year](https://www.space.com/39624-will-spacex-roadster-survive-in-space.html).

### CREW DRAGON: SPACEX'S NEW ASTRONAUT CAPSULE

![输入图片说明](https://foruda.gitee.com/images/1665454515844766466/5b19f09a_5631341.png "屏幕截图")

> SpaceX

While launching commercial missions, SpaceX also began developing a human-rated version of the Dragon spacecraft to bring astronauts to the International Space Station. 

The company [received a contract in 2014](https://www.space.com/18852-spacex-dragon.html) valued at a maximum of $2.6 billion for these launch services. In September 2015, SpaceX [showed the world the inside of the crew quarters](https://www.space.com/30518-spacex-crew-dragon-spacecraft-interior-video.html). The minimalist design has white walls, black bucket seats, several flat-panel displays and four windows for passengers to see outside.

The first uncrewed Crew Dragon test flight launched in March 2019 on a succesful trip to the International Space Station and back.

### SPACEX'S CREW DRAGON VS. CARGO DRAGON

![输入图片说明](https://foruda.gitee.com/images/1665454569862132898/6b815b8a_5631341.png "屏幕截图")

SpaceX intended that the crew and cargo versions of Dragon be very similar, in order to speed up development of the crewed ship. 

"This commonality simplifies the human-rating process, allowing systems critical to crew and space station safety to be fully tested on unmanned cargo flights.," [SpaceX stated](https://www.space.com/18852-spacex-dragon.html).

### SPACEX CREW DRAGON TEST FLIGHTS

![输入图片说明](https://foruda.gitee.com/images/1665454609128703065/1de45a38_5631341.png "屏幕截图")

> Ben Cooper/SpaceX

SpaceX is hard at work on developing the crewed Dragon for two commercial crew test-flights, which will [fly no earlier than spring 2020](https://www.space.com/spacex-boeing-commercial-crew-slip-summer-2020-nasa-oig-report.html). 

NASA is trying to wean itself off dependence on the Russian Soyuz vehicle that currently ferries all astronauts to the International Space Station. Each astronaut seat on the Soyuz costs NASA millions of dollars. 

Also, the agency tries to use U.S. launch services for launch when possible. The last crewed launch from U.S. soil took place in 2011, during the last flight of the space shuttle program.

### SPACEX'S FALCON HEAVY ENTERS SERVICE

Falcon Heavy launches are sold for about $90 million apiece, compared with $62 million for Falcon 9 launches. 

SpaceX has launched two more Falcon Heavy missions in 2019: [one carrying the Arabsat 6A communications satellite](https://www.space.com/spacex-falcon-heavy-triple-rocket-landing-success.html) in April and the other [hoisting Space Test Program 2 for the U.S. Air Force](https://www.space.com/spacex-falcon-heavy-stp2-launch-success.html) (along with the Planetary Society's LightSail 2) in June of that year.

### SPACEX UNVEILS FALCON 9 BLOCK 5

![输入图片说明](https://foruda.gitee.com/images/1665454716633089811/13c69332_5631341.png "屏幕截图")

> Elon Musk/Instagram

In May 2018, SpaceX unveiled the fifth and final version of the company's Falcon 9 rocket: the Block 5 booster. Designed for maximum reusability (the target is at least 10 flights), this booster will launch astronauts into space on Dragon capsules for NASA. 

The first Falcon 9 Block 5 rocket [was built to launch Bangabandhu-1](https://www.space.com/40545-spacex-new-falcon-9-rocket-launch-landing-success.html), the first communications satellite for the country of Bangladesh. That mission launched in May 2018, with the Block 5 booster used on that flight later [launching an Indonesian satellite in August of 2018](https://www.space.com/41395-spacex-launches-used-block-5-rocket-nails-landing.html).

### STARSHIP AND SUPER HEAVY: SPACEX'S MEGAROCKET

In September 2019, SpaceX founder Elon Musk announced a completely new vehicle for his company: a fully reusable, massive rocket and booster designed to eventually launch people to Mars. And so, the [Starship and Super Heavy](https://www.space.com/spacex-starship-super-heavy.html)  were born. 

Originally called the Interplanetary Transport System, and later Big Falcon Rocket (or Big F****** Rocket, as Musk has hinted), the futuristic system intended for Mars exploration, but could be applied to the moon, other deep-space destinations and point-point trips around Earth. 

Musk has tweaked the design several times over the last few years, rolling out different specifications in 2017 and 2018, ultimately settling on a design in 2019. 

![输入图片说明](https://foruda.gitee.com/images/1665454799576848966/6396d704_5631341.png "屏幕截图")

> SpaceX (Image credit: SpaceX)

In its current configurations, the Starship and its Super Heavy booster will stand 387 feet (118 m) tall (including the spaceship) and capable of bringing 110 tons (100 metric tons) to low Earth orbit. 

Each rocket will carry about 100 people, and the rocket will be fully reusable. Musk said he plans to use this rocket in fleets, bringing hundreds or thousands of passengers at a time to Mars. In the 2020s, Musk plans to halt all Falcon lines except for Super Heavy, which would perform all sorts of missions. Destinations would range from Mars to the International Space Station to orbits that would launch satellites near Earth.

### STARHOPPER: SPACEX'S TESTBED FOR STARSHIP

In 2019, [SpaceX launched Starhopper](https://www.space.com/spacex-starhopper-final-landing-photo.html), a prototype for its Starship, in a series of test hops at the company's launch site in Boca Chica, Texas. 

Starhopper is the Starship-equivalent of the Grasshopper prototype SpaceX used to develop the technology behind its reusable Falcon 9 rockets. Made of stainless steel, the squat, three-legged vehicle carried a single Raptor rocket engine and made a series of test firings and tethered hops that culminated in [a single grand hop on Aug. 27, 2019](https://www.space.com/spacex-starhopper-aces-final-test-launch-landing.html). 

![输入图片说明](https://foruda.gitee.com/images/1665454863233355726/3b2f8000_5631341.png "屏幕截图")

> (Image credit: SpaceX)

During that big hop, Starhopper lifted off from a SpaceX pad, reached an altitude of about 500 feet (150 m) and translated sideways to a landing pad a short distance away. The entire flight took about one minute. 

After that hop, the fourth and biggest test for Starhopper, SpaceX retired the vehicle and pushed ahead with its Starship project.

### STARSHIP MK 1: SPACEX'S FIRST STARSHIP PROTOTYPE

![输入图片说明](https://foruda.gitee.com/images/1665454890532050846/5f3af82f_5631341.png "屏幕截图")

> (Image credit: SpaceX via Twitter)

On Sept. 28, 2019, after months of anticipation, [SpaceX unveiled its first Starship prototype](https://www.space.com/elon-musk-unveils-spacex-starship-2019-update.html), the Starship Mark 1 (or Starship Mk1). 

Like Starhopper, the Starship Mk1 was assembled at SpaceX's Boca Chica site in Texas and is made of stainless steel. The vehicle reflects more design changes for Starship, with two fins (down from three), and is built for uncrewed test flights only. 

In 2019, SpaceX hopes to launch Starship Mk1 on a 12-mile-high test flight, with the goal of reaching orbit in 2020. 

SpaceX has since built several versions of the Starship prototypes (the current one is called Starship SN4, or Serial No. 4). SN4 is undergoing tests for a planned "hop" flight in 2020. 

### CREW DRAGON ENDEAVOUR: SPACEX'S 1ST FOR ASTRONAUTS

![输入图片说明](https://foruda.gitee.com/images/1665454933383194600/9bc5bb5a_5631341.png "屏幕截图")

> SpaceX (Image credit: SpaceX/Twitter)

SpaceX's first Crew Dragon to carry astronauts is scheduled to launch May 27. That mission will launch [NASA astronauts Bob Behnken and Doug Hurley](https://www.space.com/spacex-crew-dragon-astronauts-launch-site-arrival-demo-2.html) to the International Space Station on a mission that will last up to four months. 

The test flight, [called Demo-2](https://www.space.com/spacex-crew-dragon-demo-2-step-by-step.html), follows a series of other tests. In March 2019, SpaceX flew an unpiloted Crew Dragon test flight to the station under its Demo-1 mission. In January 2020, the company launched an In-Flight Abort test that demonstrated the Crew Dragon's emergency escape system for launch emergencies. 

The company has also tested parachutes and other vital systems for Crew Dragon. 

[Visit here for Space.com's complete coverage](https://www.space.com/topics/spacex) of the SpaceX Crew Dragon mission. 

 _[Join our Space Forums](https://forums.space.com/) to keep talking space on the latest missions, night sky and more! And if you have a news tip, correction or comment, let us know at: community@space.com._ 

---

[Elizabeth Howell](https://www.space.com/author/elizabeth-howell)
- Staff Writer, Spaceflight

Elizabeth Howell, Ph.D., is a staff writer in the spaceflight channel since 2022. She was contributing writer for [Space.com](http://space.com/) for 10 years before that, since 2012. As a proud Trekkie and Canadian, she also tackles topics like diversity, science fiction, astronomy and gaming to help others explore the universe. Elizabeth's on-site reporting includes two human spaceflight launches from Kazakhstan, three space shuttle missions in Florida, and embedded reporting from a simulated Mars mission in Utah. She holds a Ph.D. and M.Sc. in Space Studies from the University of North Dakota, and a Bachelor of Journalism from Canada's Carleton University. Elizabeth is also a post-secondary instructor in communications and science since 2015. Her latest book, Leadership Moments from NASA, is co-written with astronaut Dave Williams. Elizabeth first got interested in space after watching the movie Apollo 13 in 1996, and still wants to be an astronaut someday.

# [《Liftoff》节选一 SpaceX招人](https://www.bilibili.com/read/cv12843562)

自然 2021-08-24 17:21215 阅读 · 17喜欢 · 8评论 | [SpaceX小工](https://space.bilibili.com/1481473047)

此行最后是顺利的。他们会见的绿湾公司无法帮助SpaceX，但它推荐了密尔沃基附近的另一家制造商Spincraft。SpaceX找到了它的燃料箱供应商。

这样的旅行有很多次，帮助马斯克与高层领导建立了联系。当然，为他工作可能很难。但他早期的员工可以立即看到为一个希望把事情做好的人工作的好处，经常当场做出决定。当马斯克决定Spincraft可以以合理的价格制造出好的燃料箱时，就是它了。不需要委员会。不需要报告。就这样，成交。

这种果断的风格在埃尔塞贡多办公室的会议中延续了下来。马斯克会把他的不同团队召集在一个小会议室里，包括推进、结构或航空电子设备方面的工程师，然后总结主要问题。如果工程师面临棘手的问题，马斯克希望有机会解决它。他会提出建议，给他的团队一两天的时间解决问题，然后向他汇报。在此期间，如果他们需要指导，他们被告知可以直接给马斯克发邮件，不分昼夜。他通常会在几分钟内做出回应。在一次会面的过程中，马斯克可能时而滑稽，时而严肃，时而犀利，时而严厉，时而沉思，时而执着于火箭科学的最精细细节。但最重要的是，他引导了一股超自然的力量，推动了事情的发展。埃隆·马斯克只是想把事情做好。

坐在会议桌周围的那些座位上的工程师们也有一定程度的狂热。首先，他们必须接受马斯克雄心勃勃的愿景，如果不是完全不可能的话。但这需要一种更罕见的品质，当有人敦促它们时，它们能快速地穿过技术难题的丛林，速度越来越快。马斯克最有价值的技能之一是判断一个人是否符合这种模式的能力。他的手下必须才华横溢。他们必须努力工作。不可能有任何废话。

马斯克在谈到自己面试工程师的方式时说:“虚张声势的人很多，真正有才能的人很少。”“我通常十五分钟就能看出来，和他们一起工作几天就肯定能看出来。”马斯克把招聘作为首要任务。他亲自会见了公司前3000名员工的每一个人。这需要熬夜和周末的时间，但他觉得为公司找到合适的人很重要。

拿菲尔卡苏夫的例子来说，在科尼格斯曼加入SpaceX几周后，他需要雇佣一名电气工程师来帮助设计和制造猎鹰1号机载电脑的印刷电路板。这个德国人是在那年早些时候卡苏夫在微观世界实习时认识他的。卡苏夫并不担心，这个21岁的早熟少年已经习惯了艰难困苦。他在饱受战争蹂躏的黎巴嫩长大，离开家人来到美国上大学。他有头脑，但钱很少。卡苏夫没有钱上麻省理工学院或哈佛大学，于是接受了南卡大学提供的全额奖学金。他还没有完成本科学业，科尼格斯曼就催促他去参观公司在埃尔塞贡多的新办公室。

在他的参观中没多久，卡苏夫就发现自己正坐在这位企业家的对面，他盯着他看，喜欢让面试对象分心。作为面试过程的一部分，马斯克希望测试的不是一个人的知识，而是他或她的思考能力。因此，马斯克向卡苏夫提出的第一个问题是一个工程难题。

你在地球上的某个地方，”马斯克说。“你有一面旗和一个指南针。你把旗插在地上。你看一下指南针，它指向南方。往南走一英里。然后你转身向东走一英里。然后你转身向北走一英里。让你惊讶的是，你又回到了旗子前。你在哪儿?”

卡索夫想了想。他不可能在赤道，因为他要走一个正方形。从指南针的方向来看，也不可能是南极。因此，它一定是北极，因为在那里90度的转弯最终在球体的顶部形成了一个三角形的三条边。这是正确答案。马斯克开始问下一个问题，但卡索夫打断了他。"等等，你可以去另一个地方"

现在马斯克有点兴趣了。

“如果你在南极以北，”卡苏夫继续说，“有一个地方，地球的周长正好是一英里。如果你从那里以北一英里开始，向南一英里，绕地球一圈，然后再向上一英里，你会在同一个地方。”

马斯克承认，这是事实。然后他不再问卡苏夫谜语般的面试问题，而是开始讨论柯尼格斯曼需要什么帮助。卡苏夫只有21岁，没有大学学位，这些都无关紧要。他能胜任这份工作。

当马斯克确定想要聘用的人时，他可能会毫不留情。2004年春天，布伦特·阿尔坦(Bulent Altan)几乎完成了斯坦福大学(Stanford)的航空学硕士学位。他打算在旧金山湾区找份工作，他的妻子蕾切尔·瑟尔斯(Rachel Searles)已经在谷歌找到了一份称心如意的工作。然而，阿尔坦在研究生院的几个工程学朋友最近搬到了洛杉矶，在SpaceX工作。其中一名员工史蒂夫·戴维斯(Steve Davis)给阿尔坦发短信说，他很喜欢这家公司，应该来参观一下。

他是土耳其人，说话带有明显的口音，两年前才搬到美国。在德国学习计算机科学之后，他发现北加州符合他的喜好。这么快就变动工作地点，尤其是搬到拥挤、烟雾弥漫的洛杉矶，对他没有什么吸引力。所以他这次旅行就是为了看看戴维斯和他的其他朋友。然而，当他在埃尔塞贡多工厂拜访他们时，阿尔坦很快就被卷入了SpaceX的神秘之中，因为该公司正忙于完成其首个准备飞行的猎鹰1号火箭。在与马斯克会面时，阿尔坦意识到他想在那里工作。但是旧金山湾区的计划呢?

戴维斯预料到了他朋友的问题。在说服马斯克他们需要从土耳其请来这位才华横溢的年轻工程师之后，重要的是如何解决这个问题。他妻子在旧金山有份工作?她在洛杉矶也需要吗?戴维斯说:“这些都是可以解决的问题，埃隆比几乎任何人都更擅长解决问题。”

因此，马斯克中途加入了阿尔坦的面试。谈到一半的时候，马斯克对Altan说:“我听说你不想搬到洛杉矶，其中一个原因是你的妻子在谷歌工作。我刚和拉里谈过，他们要把你妻子调到洛杉矶去。你现在什么打算?”

为了解决这个问题，马斯克打电话给他的朋友拉里·佩奇，谷歌的联合创始人。阿尔坦目瞪口呆地沉默了一会儿。但后来他回答说，考虑到所有这些，他认为自己会来SpaceX工作。

第二天，西尔斯去谷歌上班，她的经理说刚发生了一件最奇怪的事情。拉里·佩奇打电话说，如果她愿意，她现在可以在公司的洛杉矶办公室工作。

节选自Eric Berger的新书《Liftoff》序言章节，是了解早期SpaceX很好的来源，目前还没有官方的中文版，翻译了一段和大家分享。

# [LIFTOFF 马斯克（Elon Musk）与SpaceX早期创业史(4): 首飞失利](https://www.bilibili.com/read/cv12508979)

学习 2021-08-05 21:0386阅读 · 5喜欢 · 3评论 | [蜂蜜饼gnn](https://space.bilibili.com/25852367)

![输入图片说明](https://foruda.gitee.com/images/1665456781261618211/8848c002_5631341.png "屏幕截图")

> 小岛上的简易发射台

本文整理自Eric Berger《LIFTOFF》，为自己阅读后归纳整理的情节大意，非原文。

### 【控制系统主管Koenigsmann】

Koenigsmann很喜欢Kwajalein岛的军事氛围。他是德国人，航空专业出身，后来喜欢上了航天。早年他在德国的大学里工作时，带领五个人的项目组，研制了一个小卫星。当时NASA正在开展航天飞机的国际合作项目，他们的小卫星被选中了，搭载在1994年2月的“发现号”航天飞机上发射。

为了这个项目，Koenigsmann去美国出了几趟差，参观了一些NASA的设施，包括肯尼迪航天中心等。他很喜欢在美国经历的一切，妻子也乐意去美国，于是一家人搬去了洛杉矶。Koenigsmann进了一家名叫Microcosm的私营航天公司（和Shotwell、Chinnery是同事），那里做的都是一些小的探空火箭。在此之前Koenigsmann对火箭没多少经验，他在这里手把手学到了一些火箭控制的技术。

Koenigsmann还在Microcosm时，曾经试图把马斯克拉来投资自己的项目，因为当时公司非常缺钱。但最后他自己被挖去了SpaceX。Koenigsmann正需要找一份收入更高的工作来养家，所以很快就同意跳槽去SpaceX，他的要求是能多休假，以便回德国看望家人。马斯克爽快地答应了这个请求，因为一旦加入了SpaceX，Koenigsmann会变得超级忙，给他假期他也没工夫休。

### 【在Omelek岛建发射台】

把火箭和各种设施搬去岛上费了很大的劲。一位员工带着火箭的小部分关键组件转了两天飞机到达岛上，火箭剩下的大部分东西通过船运，用了一个月时间运到岛上。此外还有发射要用的各种设施，员工们把工具、木弟子、管道、电脑等等各种东西塞进集装箱，总共向岛上运了30吨设施，有的通过海运，有的走军事空运。

团队住在Kwajalein岛的军队基地，每天早上坐船去发射台所在的Omelek岛工作，晚上再由船接他们回去。有些人比如Chinnery就一直住在基地，还有的人是基地和洛杉矶总部两头跑。

一开始岛上几乎没有任何设施。团队只能自己建了一个混凝土发射台，建了存放火箭的库房，运来了一个大型发电机。

Bjelde还需要确保测控间与火箭的联系。因为Omelek岛太小了，发射前所有人必须撤到Kwajalein岛上去。而Omelek处于Kwajalein岛的地平线外，通讯效果不好。后来Bjelde拿着设备升上了几十英尺的高梯，大约是猎鹰-1竖起后火箭天线的高度，通讯效果就很好了，节省了建造中继通讯站的费用。

Omelek岛上没有空调，唯一能乘凉和遮荫的地方是混凝土建筑边上，可以在那里吹吹风。他们只花了4个月时间，到2005年秋天时，发射用的基础设施就建好了。Chinnery认为，他们能做得这么快，是因为在范登堡建发射台时积累了经验，并且岛上的军队对他们管理得比较宽松。

受到条件的制约，SpaceX没有能把火箭运到发射台的车，于是他们用了个原始的办法：他们给火箭做了一个“摇篮”，把火箭水平挂在一个装有轮子的架子上。由于岛上净是珊瑚、沙滩和杂草，他们在地上铺了许多板子，让轮子可以通过。到了发射台以后，再把“摇篮”连着火箭一起竖起来。

Koenigsmann回忆起当年的疯狂经历，自己都不敢相信做了这么多事。

2005年9月份，猎鹰-1的一级和二级先后运到岛上。每当在测试中遇到零件故障、损坏等问题，还得再从洛杉矶生产新的运来才能继续用，特别麻烦。

因为工作太多，赶不上回基地的船，SpaceX在Omelek岛上放了房车，这样就能在车上过夜。发动机副主管Hollman带领的团队日夜驻扎在岛上时，经历了艰难的时期：通常是待在Kwajalein基地的人与洛杉矶总部沟通技术问题，再把要求下给Omelek岛上的工程师。在这个过程中，岛上的工程师一直在被提要求，却没机会表达自己的想法，感觉被边缘化了。再加上Omelek岛上条件不好，供给时常不足，大家没吃没喝、夜以继日干活，还被要求完成各种艰难的任务，整个团队抱怨不断，处在叛变的边缘。

直到有一天，供给船没带来吃的，只运来一堆文件的时候，Hollman爆发了。他打电话给Kwajalein岛上的Buzza（发射主管）说：“没吃的怎么干活！受够了！”

Buzza赶紧安排一架直升机送食物和香烟过去，但飞行员说岛上正在建脐带塔，在那降落不安全，Buzza好说歹说才把飞行员劝去。因为不能降落，直升机盘旋在Omelek岛上空，直接把东西抛下去。岛上的员工们像野生动物一样一拥而上，拿到了吃的、抽上了烟，叛变的情绪居然就这么平复了。

![输入图片说明](https://foruda.gitee.com/images/1665456843738338207/088577b9_5631341.png "屏幕截图")

> 马斯克在测控间，旁边是发动机主管Mueller

### 【静态点火试验】

2005年11月底，SpaceX准备好了。11月27日，军队给了SpaceX六个小时的窗口，先进行静态点火试验。一开始，把增压氦气输进火箭的过程意外地不顺利；接着又发现，岛上一个液氧贮罐上的阀门本应是关的，却处在开的状态。SpaceX只能暂停工作，请军方派船把人送到岛上去关阀。接下来还得重新加注，然后箭上计算机又出了问题，时间不够了。

12月20日进行了第二次尝试，结果这天岛上刮起了大风，超出了发射的安全标准，只能改天再试。就在卸煤油的过程中，Thompson（箭体结构主管、首飞总指挥）发现拍摄的画面有些不对劲，先是出现一个阴影，然后越来越大。当时一个增压阀出了故障，导致煤油泄出时燃料箱里形成了负压，一级燃料箱开始向内坍塌，测控间里的人都吓坏了，赶紧停止卸煤油，最后煤油被缓慢地卸完了。

当天晚些时候，Thompson、马斯克和其他几个人坐船去看火箭的情况。半路上遇到了大浪，Thompson 在颠簸中被掀到半空又摔下来撞伤了腿。火箭的状况也很糟糕，整个一级燃料箱都弯曲变形了。

2006年初，新的燃料贮箱运到了岛上。军队给了他们两周时间完成静态点火测试，此后发射场需要关闭维护一段时间。这次测试必须成功，否则会耽搁更长的时间。

在测试电气系统时，出了很多问题。此外团队还决定把供电电压提高，然而火箭毫无反应。检查发现，配电箱里的一个电容不能适应提高的电压，需要更换新的电容。

岛上什么都没有，更不会有电容卖。留给他们的时间不多了，SpaceX只好上演了一场换电容的生死时速：电气工程师Altan赶上了Kwajalein岛飞往火奴鲁鲁的航班（一周只有三趟），在等待转机去洛杉矶的期间，火奴鲁鲁机场下班关门了，他在航站楼门外等了一夜。与此同时洛杉矶总部的一个员工乘马斯克的私人飞机飞去明尼苏达的厂家拿到电容，再飞回洛杉矶和Altan会合，他们再和马斯克一起坐私人飞机飞去岛上。在回岛上的路上，Altan因为旅途中一直没休息，本想睡会，结果一路上马老板不断问他问题，问他电气系统为什么会出故障，等等。到了岛上，Altan立马装上新电容进行测试，结果一切正常，终于松了口气，到那为止Altan已经快三天没睡了。

### 【猎鹰-1首飞】

2006年3月，发射场重新开放，这次SpaceX终于准备好了。

后来的SpaceX会在发射前降低一下大家的期望，但当时的马斯克还没有这个觉悟。猎鹰-1首飞前，马斯克还在记者采访里自信地说，猎鹰-1发射成功的概率在90%以上。

发射前，马斯克在控制间里踱步。即使在这样紧张的时刻，他还在操心未来的事情，他的脑子习惯了多线程工作。马斯克时不时把首飞指挥Thompson叫起来，问他后续研制猎鹰-5的事情。

倒数到-30min的时候，马老板又一次来到Thompson的座位旁边，针对猎鹰-5贮箱材料采购进度的事情，和他进行了激烈的交谈。Thompson正在首飞前倒数，他满脑子都是首飞的事，根本没来得及管材料采购的事。

尽管受到了马老板的干扰，倒数的过程倒是很顺利，中途没有中断。终于，大家看着火箭起飞了。

几秒之后，Tom Mueller最先发现了问题。Mueller：“Oh! Shit!”只见发动机似乎着火了。

火箭很快就掉了下来，砸到了岛边的珊瑚礁里，溅起的海浪把大半个Omelek岛淹了，火箭碎片基本全泡在了海水里。

![输入图片说明](https://foruda.gitee.com/images/1661589795206283497/82c9aa9b_5631341.png "屏幕截图")

> 封面这张图可以看出，发动机起火了

当天晚上，马斯克的弟弟Kimbal Musk（SpaceX的早期投资者）做了顿晚饭安慰大家。大家席地围成一圈吃着饭，马斯克一直在开玩笑逗大家笑来缓解气氛。

第二天早上，SpaceX团队出发时，惊讶地看到码头上来了100多个当地人。Kwajalein岛上的平民总共1000人左右，大多是为军队工作的。当地人为了表示对这个小公司的支持，自告奋勇去帮忙捞火箭残骸。

猎鹰-1的载荷，FalconSAT-2卫星是一个重40磅、成本仅7.5万美元、由空军学院的学生研制的卫星。最初计划搭载哥伦比亚号航天飞机发射，但航天飞机失事后计划中止。几年后，DARPA购买了猎鹰-1来发射这个卫星。

首飞失败后，这颗卫星掉到了和火箭残骸相距不远的地方，砸穿了仓库的房顶，又回到了原来存放的地方，几乎完好无损。后来卫星作为教学用具，放在了学校博物馆。

大家收集着残骸，一边在仓库里把残骸拼回原位。随着时间流逝，大家心情渐渐平复，可以一边寻找残骸一边说笑了。但Koenigsmann在之后的一段时间一直很沮丧，他一整天泡在海水里，找到了一级火箭的降落伞，死命想拉上岸却拉不动。之后的一个月也一直埋头加班，不怎么说话。

![输入图片说明](https://foruda.gitee.com/images/1665456914251904072/be83a304_5631341.png "屏幕截图")

> 马斯克与首飞火箭的残骸

马斯克意识到失败会让员工在情感上受到打击。事后，他给大家群发了一封邮件，用振奋人心的语气夸赞了每个分系统的性能，也提出了对发动机起火故障的初步判断。他希望六个月之内再尝试一次发射。他还提到，许多著名的火箭，比如阿里安、联盟、质子号、早年的宇宙神等，在早期的飞行中都遭遇失败。

他写道：“我知道入轨有多艰辛。我特别佩服那些坚持开发火箭、并使这些火箭成为航天发射中流砥柱的人们。SpaceX还有很长的路要走。无论遇到什么困难，我们都要让它成功。”

在猎鹰-1首飞失败后，马斯克会逗大家笑。但这并不是好笑的事。他更想知道，为什么会发生这样的故障？到底是谁搞砸了？

### 【[共5篇相关文章]((https://space.bilibili.com/25852367/article))】

- [LIFTOFF 马斯克(Elon Musk)与SpaceX早期创业史(5)：销售火箭](https://www.bilibili.com/read/cv12632578) | 78 | 6 | 1 | 2021-8-12

  > （本文总结自Eric Berger《LIFTOFF》一书，为阅读后整理归纳的情节大意，不是原文翻译。）Gwynne Shotwell，“射得好”阿姨在Microcosm工作的时候，Koenigsmann和他的同事Gwynne Shotwell成了好朋友。他跳槽去SpaceX之后，把Shotwell也介绍给了马斯克认识。后来马斯克决定雇用一个专职销售火箭的人，并鼓动Shotwell加入。Shotwell被马斯克改变业界的雄心壮志所吸引，最后决定跳槽去SpaceX。加入SpaceX后，Shotwel

- [LIFTOFF 马斯克（Elon Musk）与SpaceX早期创业史(4): 首飞失利](https://www.bilibili.com/read/cv12508979) 87 | 5 | 3 | 2021-8-5

  > 小岛上的简易发射台本文整理自Eric Berger《LIFTOFF》，为自己阅读后归纳整理的情节大意，非原文。【控制系统主管Koenigsmann】Koenigsmann很喜欢Kwajalein岛的军事氛围。他是德国人，航空专业出身，后来喜欢上了航天。早年他在德国的大学里工作时，带领五个人的项目组，研制了一个小卫星。当时NASA正在开展航天飞机的国际合作项目，他们的小卫星被选中了，搭载在1994年2月的“发现号”航天飞机上发射。为了这个项目，Koenigsmann去美国出了几趟差，参观了一些NA

- [LIFTOFF 马斯克与SpaceX早期创业史(3)：Kwajalein岛](https://www.bilibili.com/read/cv11289216) | 67 | 5 | 1 | 2021-5-15

  > （本文总结自Eric Berger《LIFTOFF》一书，为自己阅读后概括的情节大意，不是原文翻译。）第三章：Kwaj【Chinnery的加入】负责发射工作的Anne Chinnery，从小经历了美国航天快速发展的时代。耳濡目染之下，她进了空军学校，毕业后为空军工作，也在范登堡基地协助发射。30几岁时，她想跳出一眼看到头、充满繁文缛节的空军工作，开始寻找私营航天的职位。后来她去了一家私营航天公司Microcosm。当时Koenigsmann（控制系统主管）和Gwynne Shotwell也在这个

- [LIFTOFF 马斯克与SpaceX早期创业史(2)：Merlin发动机](https://www.bilibili.com/read/cv11047554) | 96 | 9 | 1 | 2021-4-27

  > （本文总结自Eric Berger《LIFTOFF》一书，为自己阅读后概括的情节大意，不是原文翻译。）第二章：MerlinMerlin第一次推力室试车是在2003年3月，也是发动机主管Tom Mueller的42岁生日。Mueller提前准备了一瓶酒（从办公室里找到的，说是某个会议后剩下的）带去了McGregor（试车台所在地）。 晚上9点，第一次时长半秒的试车很快结束了，推力室没炸。作为庆祝，Mueller拿出酒给大家都倒了一点。喝完之后众人才想起来，他们还得自己开车回住的地方，抽签决定让其中

- [LIFTOFF 马斯克(Elon Musk)与SpaceX创业史(1)：Early Days](https://www.bilibili.com/read/cv11047403) | 98 | 7 | 1 | 2021-4-27

  > （本文总结自Eric Berger《LIFTOFF》一书，为自己阅读后概括的情节大意，不是原文翻译。）第一章：Early Days马斯克一直有着登上火星的远大目标。最初，想去毛子那购买现成火箭。对方大概觉得他人傻钱多，不断涨价。感觉被耍了。于是马斯克决定自己干，提出了造火箭的设想，结果大家都笑了，劝他别闹。其中一人特意剪辑了个很长的火箭事故录像集，请马斯克认真观看，试图劝阻。马斯克只好在一大群反对的人当中说服少数愿意支持他的人。但加入SpaceX意味着要搬家换城市，一些人因为不想拖家带口搬家而拒