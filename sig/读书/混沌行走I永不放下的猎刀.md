- The Knife of Never Letting Go 的 书评 《[气死我了](https://book.douban.com/review/13319801/)》 闻夕felicity 评论 2021-03-14 23:34:30

<p><img width="706px" src="https://foruda.gitee.com/images/1674521242593016920/4d384693_5631341.jpeg" title="513混沌行走1副本.jpg"></p>

> 这是一篇纯转载自《豆瓣读书》的书评，也是这篇书评，让我更加理解了评注中关于 “青少年科幻” 的定义，也更加理解了作者的各种设定，和他最擅长的本领 “讲故事”。原本的科幻电影题材，变身成了青少年最希望获得的能力，能够听到别人在想什么。整部电影的科幻镜头真是少的可怜，真是一部低成本科幻作品，和春节档上映的硬科幻完全不是一个层次的。（选用《月球陨落》和果老师的《读书笔记》没有直接联系，只是时间点的重叠）

### [混沌行走](https://v.qq.com/x/cover/mzc00200hdpza6k/x0040ha2xx8.html)？

Based upon the book

"THE KNIFE OF NEVER LETTING GO" 

by PATRICK NESS

根据派崔克·奈斯的小说《混沌行走：永不放下的猎刀》改编

### 重现步骤

- https://chaoswalking.fandom.com/wiki/The_Knife_of_Never_Letting_Go

  ![输入图片说明](https://foruda.gitee.com/images/1674481424523734143/19772dd6_5631341.png "屏幕截图")

  > 电影改编自《混沌行走:永不放手的刀》已被改编成电影《混沌行走》(2021年)，是《混沌行走》系列计划从书到电影三部曲的一部分。然而，新冠肺炎疫情和电影的负面评论阻碍了将这三部小说都搬上大银幕的可能性。这部电影由汤姆·霍兰德饰演托德·休伊特，黛西·雷德利饰演维奥拉·伊德，麦兹·米克尔森饰演大卫·潘提斯，尼克·乔纳斯饰演大卫·潘提斯，辛西娅·艾瑞沃饰演Mathidle，大卫·奥伊罗饰演Aaron，兰博基尼饰演Manchee。电影对原著进行了重大修改，导致影评人和忠实粉丝对原著的负面评价。

  除非另有说明，社区内容在CC-BY-SA下可用。

- https://book.douban.com/subject/4567571/

  <p><img width="326jpx" src="https://foruda.gitee.com/images/1674483183572049032/db530381_5631341.jpeg" title="knife-of-never-letting-go.jpg"></p>


### 豆瓣读书

 **内容简介**   · · · · · ·

Todd Hewitt is the only boy in a town of men. Ever since the settlers were infected with the Noise germ, Todd can hear everything the men think, and they hear everything he thinks. Todd is just a month away from becoming a man, but in the midst of the cacophony, he knows that the town is hiding something from him -- something so awful Todd is forced to flee with only his dog, whose simple, loyal voice he hears too. With hostile men from the town in pursuit, the two stumble upon a strange and eerily silent creature: a girl. Who is she? Why wasn't she killed by the germ like all the females on New World? Propelled by Todd's gritty narration, readers are in for a white-knuckle journey in which a boy on the cusp of manhood must unlearn everything he knows in order to figure out who he truly is.

 **作者简介**   派崔克•奈斯（Patrick Ness）

1971年生于美国维吉尼亚州贝尔沃堡陆军基地，6岁之前在夏威夷度过。之后和家人移居华盛顿州，从小最大的嗜好就是编故事。大学就读美国南加大英语文学系。在牛津大学教授创意写作三年，他的学生往往比这位老师年纪还大。他还为英国最大的报纸诸如《泰晤士报》、《每日电讯报》、《卫报》等专业媒体撰写书评。混沌行走三部曲是他首次尝试青少年小说，推出后随即大受好评，《出版家周刊》称之为“近年来最重要的青少年科幻小说”。此外，他身上有个犀牛纹身，跑过两次马拉松，是具有专业水准的潜水员，曾被电影学院录取，但为了学习写作而放弃这个机会。

《The Knife of Never Letting Go》的 **原文摘录** 

- 晨光明媚

  > It's weird, it is, out there, hiding somewhere, in the trees or somewhere outta sight, a spot where your ears ad your mind are telling you there's no noise....which is impossible. There ain't nothing but noise in this world, nothing but the constant thoughts of men and things coming at you and at you and at you. 

    —— 引自第13页  2016-01-22 08:32:37

- 乐浪公主 

  > “连活命都成问题的时候，没人关心历史。”我一字一顿地说。 “越是这种时候，历史越重要。”希尔迪说。 

    —— 引自第161页 2021-01-06 20:33:43

  > 倘若选了左边的路，也许我们的故事就会完全不同；也许后来我们遭遇的那些坏事情就不会发生；也许左边岔路尽头等待着我们的是幸福，是一个温暖的地方，爱我们的人都住在那里，没有声流，也没有寂静，只有充足的食物；那里的人都不会死，谁都不会死，永远不会死。 也许。 但是我怀疑根本不存在这样的“也许”。 我可不是一个幸运的人。 

    —— 引自第215页 2021-01-08 00:55:28

  > 我只认识她三天，你们知道吗？我活到现在，认识她的时间只他妈的三天。可是，我感觉认识她之前自己的生活都不作数，就好像此前我一直活在一个巨大的谎言之中，现在才发现真相。不，不是像，我的确活在巨大的谎言中，现在才开始真正的生活——真正的生活没有安全，也没有答案，只有不断地逃，永远地逃。 

    —— 引自第226页 2021-01-08 01:01:56

  > 我们这儿的人当然也会撒谎。新世界和我来的那个地方（不可以说那个名字，不可以想到那个名字）充斥着谎言，就好像除了谎言别无他物。但是她的谎言不同。我说过，人们总是在撒谎，对自己撒谎，对他人撒谎，甚至对整个世界撒谎。可是当你的谎言只是诸多谎言中的一个，而真相飘在不知道什么地方，你又怎么分辨什么是谎言呢？人人都知道你在撒谎，但是其他人也都在撒谎。所以撒谎有什么关系？对事情会有什么改变呢？谎言只是男人的一部分，他的声流的一部分，有时候你能分辨出来，有时候不能。 男人撒不撒谎都是男人。 

    —— 引自第239页 2021-01-08 09:17:20

  > 他摇摇头：“那你觉得是什么让你一直走下去的？是什么让你逃到现在的？” “恐惧。”薇奥拉说。 “绝望。”我说。 “不，”他说着，把我俩揽进怀中，“不不不，你们走过的路比这颗星球上大多数人一辈子走的路都要多。你们克服了诸多困难，度过了不少危险，也摆脱了许多本来可能致命的麻烦。你们把一支军队和一个疯子甩在了身后，没有被疾病撂倒，还见识过大多数人永远没机会见到的事物。如果不是希望，那你们觉得，还有什么能支撑你们走到现在呢？” 薇奥拉和我交换了一个眼神。 我开口了：“我明白你想说的，本。” “希望，”他说这个词的时候特意捏了捏我的胳膊，“就是希望。我现在直视着你的眼睛，告诉你，你还有希望；希望不仅属于你，也属于你们两个人。”他抬头看看薇奥拉，再看看我，“希望会在路的尽头等着你们。” “是不是真这么回事你又不知道。”薇奥拉说。尽管我不想，但我的声流也表示同意。 “我确实不知道，”本说，“但我相信。我相信你们是有希望的。正是因为相信，人才有希望。” 

    —— 引自第375页 2021-01-10 23:45:29

- 闻夕felicity

  > 我边想边揉麦奇的耳朵一一这条傻狗帮了我大忙。以前我并不想养它，可它还是留在了我身边，跟我进了沼泽地，在阿隆想掐死我的时候咬了他一口，还在薇奥拉失踪的时候带我找到了她。这条傻狗正用它那条粉红色的小舌头舔着我的手，它的眼睛依然因为小普伦提斯先生踢的那一脚而难以睁开，它的尾巴被马修・菜尔砍了刀比以前短了一大截一一这都是因为我的狗一一我的狗一一为了追咬那个手握大砍刀的男人。麦奇一次又一次地救了我。它总是在我逃离黑暗的时候给我帮助，还在我遗忘自己是谁的时候一遍遍地告诉我我是谁。 

    —— 引自第332页  2021-03-11 23:53:09

  > 它就在刚掉下来的饲料卷后面，一个漆黑的角落里。我向它跑去 陶德？”我靠近它的时候，它说，“尾巴，陶德？” 麦奇？”很黑，我在它旁边蹲下之后オ看清楚它的情况。它的尾巴只有以前的三分之二长，到处都是血，但是上帝保佑，它还能摇尾巴。 “疼，陶德？” “没事的，麦奇。”我说。我松了ロ气，原来只是尾巴受伤了，但我的声音和声流都带着哭腔，“我很快就帮你止血。” “好吗，陶德？”“我没事。”我说着揉了揉它的脑袋。它咬了我的手一下，我知道它不是故意的，只是因为尾巴上的伤太疼了。然后它抱歉地舔了舔我，可紧接着又咬了一下。“疼，陶德。”它说。 

    —— 引自第200页 2021-03-14 23:04:33

  > “等等。”我撤回饲料堆处，找到了还在角落里蜷缩着舔尾巴的麦奇。它抬头看看我，叫了几声，声音微弱，不成句子。“我现在把你抱起来，别太使劲咬我哦，好吗？”我对它说。 “好的，陶德。”它鸣咽着，每次摇动那条短粗的尾巴都疼得直叫唤。 我弯下腰，伸出双臂托住它的肚子，将它抱到自己胸前。它大叫一声，咬住我的手腕，然后又赶紧松开嘴舔舔我。 “没关系。”我尽可能轻轻地抱住它。 

    —— 引自第202页 2021-03-14 23:05:11

  > 对不起！”我向越来越远的岸边喊道，声音断断续续，但仿佛有撕裂一切的力量。我的胸口绷得紧紧的，几乎不能呼吸。“麦奇，对不起！” “陶德？”它的叫声中充满了困惑和恐惧，眼睁睁地看着我离它远去，“陶德？” “麦奇！”我大叫。 阿隆空闲的那只手慢慢伸向我的狗。 “麦奇！” “陶德？” 阿隆的两只胳膊用力一扭，咔嚓一声，然后是一声尖叫和然而止的犬吠。我的心被这动静永永远远撕成了两半 

    —— 引自第348页 2021-03-14 23:07:08

 **短评** 

- 陈德柱  2015-12-08 23:52:41

  > 儿童文学，不要浪费时间。

- sanzo  2014-04-19 22:42:09

  > 一整天读完第一本，作者好残忍啊，running for nothing at all and he lost everything.

The Knife of Never Letting Go 的 **书评**  

<img width="169px" align="right" src="https://foruda.gitee.com/images/1674519453012697765/46146c21_5631341.png">

- 作者: [英] 帕特里克·内斯
- 出版: 北京联合出版公司
- 装帧: 平装
- 时间: 2022-12-1

[气死我了](https://book.douban.com/review/13319801/) | 闻夕felicity 评论 2021-03-14 23:34:30

我快要被这本小说的主人公气死了。

这个故事的主人公叫陶德，是一个十三岁的少年。他生活在一个很奇怪的城镇。在这里，一年有13个月，男孩13岁成年。在这里，每个人都能听到别人的心声，甚至连动物的心声都能听到。比如陶德的狗麦奇就是一个整天想着吃饭、拉粑粑、出去散步的铁憨憨。作者没告诉我们麦奇是什么品种，但在我心里，它大概率是只拉布拉多，活泼，忠诚，可爱。

更奇怪的是，这个城镇没有女性。陶德父母双亡，被母亲的好友养大。他所见过的唯一女性是存在于男人们脑中的意淫。作为这个城镇最后一个未成年的男孩，陶德每天都期盼着自己成年礼的到来。但就在成年礼到来之前，他的养父突然给了他一把猎刀、一本他看不懂的日记，叫他走上逃亡之路。陶德还在懵逼，追杀他的人就扛着枪打上了他家的门，于是懵逼的陶德就带着麦奇（和懵逼的读者们）一起走上了逃亡之路。

说实话，主角需要逃亡的科幻小说我也看了不少，这本可能是我看得最憋屈的。陶德的逃亡，不仅没吃没穿，徒步逃跑的速度远不及追他的人，并且战斗力还远不如追杀他的人。他的逃亡，逃得就像啥装备也没有的游戏小白被人民币氪金玩家一路狂砍狂杀。这一路上随便出现一个路人，就能把陶德揍得鼻青脸肿。这一路上唯一对陶德死心塌地、保护他尽心尽力的，就是陶德的狗麦奇：陶德被反派追杀，追得穷途末路，虚弱到昏死过去，是麦奇一次次舔醒他；陶德没吃的，饿得出现了幻觉，是麦奇抓了老鼠给他吃；陶德被人堵在仓库里暴揍得快死了，是麦奇冲上去护着他，结果自己的尾巴被活生生切了下来。

仓库里这一段，真是读得我心都碎了：

> 它就在刚掉下来的饲料卷后面，一个漆黑的角落里。我向它跑去。     

> ”陶德？”我靠近它的时候，它说，“尾巴，陶德？”     

> “麦奇？”

> 很黑，我在它旁边蹲下之后オ看清楚它的情况。它的尾巴只有以前的三分之二长，到处都是血，但是上帝保佑，它还能摇尾巴。     
“疼，陶德？”    

>  “没事的，麦奇。”我说。我松了ロ气，原来只是尾巴受伤了，但我的声音和声流都带着哭腔，“我很快就帮你止血。”    

>  “好吗，陶德？”

> “我没事。”我说着揉了揉它的脑袋。它咬了我的手一下，我知道它不是故意的，只是因为尾巴上的伤太疼了。然后它抱歉地舔了舔我，可紧接着又咬了一下。

> “疼，陶德。”它说。

所有养过狗的人都知道，这段描写有多真实。狗在极度疼痛的时候本能的反应就是用牙齿保护自己，因此就算主人是为了救它而弄疼了它，狗也会本能地龇牙。但同时狗对意识到它对主人的爱，这种爱会让它对自己的本能感到愧疚。因此狗会龇牙，但同时努力不真正弄疼它所爱的人。这种超越了本能的爱，让我更爱这种忠诚而可爱的动物。

> “等等。”我撤回饲料堆处，找到了还在角落里蜷缩着舔尾巴的麦奇。它抬头看看我，叫了几声，声音微弱，不成句子。

> “我现在把你抱起来，别太使劲咬我哦，好吗？”我对它说。     

> “好的，陶德。”它鸣咽着，每次摇动那条短粗的尾巴都疼得直叫唤。     

> 我弯下腰，伸出双臂托住它的肚子，将它抱到自己胸前。它大叫一声，咬住我的手腕，然后又赶紧松开嘴舔舔我。    

>  “没关系。”我尽可能轻轻地抱住它。

可以说，在陶德生活的这个变态世界里，麦奇是对他最好的人（狗）。一路逃亡，是他俩相依为命。

可惜，麦奇对陶德的超越了本能的爱，到底是错付了。

陶德这个该死的仁慈之心，一次次跟反派正面冲突，一次次有机会一刀结果掉对方，可就是下不了手。麦奇一次次勇敢地冲在前面，瘸着腿、被打瞎了一只眼，一度昏死过去，却还勇敢地把反派的脸都给撕了下来。可惜，再勇敢的狗也拦不住主人的傻逼。陶德这个傻叉不仅一次次把反派给放跑了，搞得自己和麦奇一次次从占尽优势到落到下风，还出了个馊主意，让麦奇去攻击反派、把反派引开，他好去英雄救美。结果呢，反派抓住了麦奇，而陶德这个傻逼竟然带着姑娘就跑了！他竟然跑了！！可怜的麦奇死在了反派手里，临死前还难以置信自己的小主人竟然没有如约来营救自己！


> “对不起！”我向越来越远的岸边喊道，声音断断续续，但仿佛有撕裂一切的力量。我的胸口绷得紧紧的，几乎不能呼吸。“麦奇，对不起！”

>  “陶德？”它的叫声中充满了困惑和恐惧，眼睁睁地看着我离它远去，“陶德？” 

> “麦奇！”我大叫。     

> 阿隆空闲的那只手慢慢伸向我的狗。    

>  “麦奇！”   

>   “陶德？”     

> 阿隆的两只胳膊用力一扭，咔嚓一声，然后是一声尖叫和然而止的犬吠。

> 我的心被这动静永永远远撕成了两半。

我四十米的大刀在哪里？！！我特么要砍了这个傻逼主角！什么样的主人让自己的狗冲在前面而自己缩在后面！！什么样的主人会派自己的狗去拖住坏蛋而自己撒腿就跑？！！！

啊！真是气死我了！

我看到这里的时候气得把我的狗一把薅了过来疯狂地撸，疯狂地撸狗也没平息我心头之恨！！！

（我家狗：？？？）

当然，冷静下来想想，我也没法怪陶德。毕竟人家只是个十三岁都不到的小孩子。就算他所在的城镇在另一个星球，按照地球时间来计算，他也才十四岁。我十四岁的时候也是个很不负责任的狗主人，那会儿也是我的狗在迁就我，而不是我在照顾它。十四岁的我是把狗当做伙伴和兄弟。兄弟，也许真的是可以背叛的吧，如果面前有异性的诱惑的话——所谓见色忘义嘛。而现在的我，把狗当毛小孩，把自己当它们的妈妈。一个母亲决不允许任何人伤害她的孩子。要动我的狗，除非从我尸体上踩过去！

这么一想我特么就更生气了。

《混沌行走》的作者有很大的野心，不仅故事背景很复杂，需要读者从对话和情节推动中慢慢拼凑陶德的星球的特殊设定，还暗示了很多寓言。比如陶德虽然拿到了母亲的日记，但却读不懂，因为他的城镇推行反智文化，鼓吹极端，且物化女性。简单说，就是陶德没上过学，他不认字儿，并且成长在类似于反人类洗脑文化里。虽然他的本能里残留着人性之光，但他需要努力对抗十三年来接受的变态洗脑，才能成长为一个拥有正常三观的人。随着他的逃亡，陶德开始意识到，他十三年来所接受的信息几乎都是谎言。而在他从一个城镇逃亡另一个城镇的过程中，他也经历了不同的体制，有些宣扬人性但由于制度而缺乏迅速的反应能力，有些把女性视为战利品和保护对象，也有些由女性领导。可以说，作者的世界观和想要讨论的公共话题是相当有深度的。

然而这些优点仍然无法平息我心头翻滚的杀狗之恨。我仍然想给作者寄刀片。哼！

[The knife of never letting go- review](https://book.douban.com/review/10181383/) | 曈阅读 2019-05-15 06:22:48

“Prentisstown isn’t like other towns.”

Expect that Prentisstown is pretty similar to every other town/settlement in the book, though that line, which was the first line on my blurb did make me want to read this book. I have heard a bit about this book and now there is also a movie in production that stars the Mary Sue of Star Wars and Spider-Man, not what I envisioned in my head, so I pray that they don’t mess the movie up, because gosh, the book is actually pretty darn great.

I loved the fact that this book felt like that it was on such a small scale due to the size of the settlements and the very small population centres, I mean, look at this. ”Populayshun 147 and falling, falling, falling.” That’s not a lot of people for a town, and then there’s later where they say in terms of numbers it is impossible for them to muster an army of even one thousand people. Yet still, they are on a colony started by the Old World(probably Earth), and there are also really high tech technology at parts, which makes it not only a depressed dystopian farming society, but also a really cool sci-fi/fantasy world as well.

The entire noise thing is actually pretty darn terrifying in my opinion. How crazy would it actually to be able to hear the thoughts of everyone around you? Just thinking about it drives me mad, and the visual representations in the book really shows how messed up the world is. It’s a [表情] for originality from me.

One thing that did bother me slightly was the grammar and punctuation errors and the often paragraph size sentences that made it extremely, not difficult, but, still sort of annoying. Now, before anyone asks, I know it is to add to the effect that our main character can’t read, and that the really long sentences is a train of thought while lots of stuff is going on, but it still kind of bothers me?

Then there’s the characters. Todd’s great and I absolutely loved his dog as well. In fact, his constant conflict in himself of not being able to kill someone is new as often characters brutally murder people, because why not. Then there’s female lead…

Viola. Ummm, what? Simply because of that name, I can’t take her seriously as a character despite how strong of a person she is, and it’s all because I’ve made too much fun the of the instrument before. Yep, that’s why, because of the four stringed instrument that is obviously worse than the violin. I’m sorry, I can’t help myself!

I will be continuing on with the rest of the series, I hope, if I don’t get distracted by something else and I will be interested to find out what happens next. 8.5/10

### [丛书信息](https://book.douban.com/series/52941)

<img width="123px" align="right" src="https://foruda.gitee.com/images/1674519453012697765/46146c21_5631341.png">

[混沌行走Ⅰ : 永不放下的猎刀](https://book.douban.com/subject/35139959/)

- [英] 帕特里克·内斯 / 万洁 / 北京联合出版公司 / 2022-12-1

  > 嘈杂让人不安，但有一种无声更加令人毛骨悚然。 继《饥饿游戏》《分歧者》后又一部席卷全球的科幻神作！ 获欧美20项图书大奖或提名：卡内基文学奖 亚瑟·C．克...

  纸质版 42.60元

<img width="123px" align="right" src="https://foruda.gitee.com/images/1674522424596243701/05f78fc5_5631341.png">

[混沌行走Ⅱ : 重返新世界](https://book.douban.com/subject/35139960/)

- [英] 帕特里克·内斯 / 李娟 / 北京联合出版公司

  > 当你能听到所有人的想法，谎言还会存在吗？ 继《饥饿游戏》《分歧者》后又一部席卷全球的科幻神作！ 获欧美20项图书大奖或提名：卡内基文学奖 亚瑟·C．克拉克...

  纸质版 45.60元

<img width="123px" align="right" src="https://foruda.gitee.com/images/1674522430483824164/8c943d42_5631341.png">

[混沌行走Ⅲ : 燃烧的土地](https://book.douban.com/subject/35218172/)

- [英] 帕特里克·内斯 / 孟思 / 北京联合出版公司

  > 人类这种生物，渺小又伟大，残忍又慈悲。 继《饥饿游戏》《分歧者》后又一部席卷全球的科幻神作！ 获欧美20项图书大奖或提名：卡内基文学奖 亚瑟·C．克拉克科...

  纸质版 45.40元

  - 内容简介  · · · · · ·

    人类这种生物，渺小又伟大，残忍又慈悲。

    继《饥饿游戏》《分歧者》后又一部席卷全球的科幻神作！

    获欧美20项图书大奖或提名：卡内基文学奖 亚瑟·C．克拉克科幻小说奖 亚马逊年度选书

    同名电影由《蜘蛛侠》“荷兰弟”汤姆·霍兰德、《星球大战》主演黛西·雷德利、《汉尼拔》米斯·米科尔森联袂出演

    “战争终于开始了。”市长说，听起来像是他所有的心愿都成真了。六个星期后移民舰队即将抵达。此刻，火焰翻腾着从山上滚落，点燃了整个星球。陶德紧盯普伦提斯市长，想要再一次阻止战争，而外星种族斯帕克人已经悄无声息地举起武器，瞄准无知无畏的侵略者。三方势力同时逼向新普伦提斯，这一场对决，唯有信任能让陶德和薇奥拉全身而退。紧要关头，陶德的“声流”突然消失，变成未知的空洞，信任的大门也随之关闭。

    在不为人知的遥远岁月中，这座星球孕育了对抗“声流”的伟大智慧。无知的新访客在这片古老的土地上大开杀戒、为所欲为之时，天地间响起某种包罗万象的声音。

    如果这个世界会把人变成怪物，我们该怎样维持生命的善意？

  - 作者简介  · · · · · ·

    帕特里克•内斯，英国著名作家、编剧、文学评论家。

    他的代表作有“混沌行走”三部曲、《当怪物来敲门》和《神秘博士》衍生篇剧本等，这些作品是欧美科幻、奇幻领域文学奖项的常客，曾获得亚瑟·C．克拉克科幻小说奖、英国科幻协会奖、詹姆斯·提普奇科幻奖、《卫报》青少年小说奖等。其中“混沌行走”三部曲、《当怪物来敲门》被改编成电影。他在2011年和2012年连续获卡耐基文学奖，是第二个连续两年获得该奖的作家。

    除了作家，他还是一名具有专业水准的潜水员，身上有个犀牛文身，跑过两次马拉松。他不吃洋葱。

---

【笔记】《[混沌行走：永不放下的猎刀](https://gitee.com/yuandj/siger/issues/I6B6FJ)》

<p><img height="269px" src="https://foruda.gitee.com/images/1674483279566780782/eede8065_5631341.jpeg" title="e02174d2.jpg"> <img height="269px" src="https://foruda.gitee.com/images/1674483366232682852/8e6f5483_5631341.jpeg" title="10-Science-Fiction-Films-Not-To-Be-Missed-in-2019-3.jpg"> <img height="269px" src="https://foruda.gitee.com/images/1674486538296056764/e9fef60f_5631341.jpeg" title="the-knife-of-never-letting-go.jpg"></p>