@平劲松 :  https://scienceline.org

> Another “classic example,” according to Mancinelli, is pharmaceuticals: “If you go to Mars, you can’t take a pharmacy with you.” But you can take fungi. More than you could anticipate. Probably, more than you wanted.

- [Why fungi adapt so well to life in space](https://scienceline.org/2018/03/fungi-love-to-grow-in-outer-space/#)
- [汤姆斯河](https://book.douban.com/subject/26341543/)：[科学和救赎的故事](https://zhuanlan.zhihu.com/p/19749443)

<p><img width="706px" src="https://foruda.gitee.com/images/1676920764864868492/b57377b5_5631341.jpeg" title="556TOMSRIVER.jpg"></p>

> 这本书有两个交集，一个是直击科学网站来自天文学家的推荐，一个是《三体》中关于《寂静的春天》的隐喻，让笔记出自科幻频道变得自然贴切，本身科幻天文就不是分家的，统统是遥望星海的存在。而直击科学的主编的著作自然成为被推荐的理由啦。因为《[为什么真菌能如此好地适应太空生活](../starship/Why%20fungi%20adapt%20so%20well%20to%20life%20in%20space.md)》正文被另一个频道引用，主体被拿出，本期专题成为一个专门推荐一个网站的专题，推荐方式则是它的主编，以及主编的著作。 :+1: :+1: :+1:

![输入图片说明](https://foruda.gitee.com/images/1676831358680174581/6a4d6249_5631341.jpeg "physical-science.jpg")

# [Science line](https://scienceline.org)

<p><img width="13.69%" src="https://foruda.gitee.com/images/1676920100325179110/f049036d_5631341.jpeg" title="default.jpg"> <img width="13.69%" src="https://foruda.gitee.com/images/1676920110027545236/396ae6dc_5631341.jpeg" title="environment.jpg"> <img width="13.69%" src="https://foruda.gitee.com/images/1676920122468537913/9fff4263_5631341.jpeg" title="exolab_singleplate.jpg"> <img width="13.69%" src="https://foruda.gitee.com/images/1676920133551878732/66fdfb3a_5631341.jpeg" title="health.jpg"> <img width="13.69%" src="https://foruda.gitee.com/images/1676920147823344392/f2588a79_5631341.jpeg" title="life-science.jpg"> <img width="13.69%" src="https://foruda.gitee.com/images/1676920157460429706/124a847c_5631341.jpeg" title="social-science.jpg"> <img width="13.69%" src="https://foruda.gitee.com/images/1676920168185403282/a4c2d78c_5631341.jpeg" title="tech.jpg"></p>

> THE SHORTEST DISTANCE BETWEEN YOU AND SCIENCE  
> a project of nyu's science, health and environmental reporting program

<ul id="menu-main-navigation" class="menu"><li id="menu-item-29450" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-29450"><a href="#">Topics</a>
<ul class="sub-menu">
	<li id="menu-item-29445" class="category-environment menu-item menu-item-type-taxonomy menu-item-object-category menu-item-29445"><a href="https://scienceline.org/topic/environment/">Environment</a></li>
	<li id="menu-item-29446" class="category-life-science menu-item menu-item-type-taxonomy menu-item-object-category menu-item-29446"><a href="https://scienceline.org/topic/life-science/">Life Science</a></li>
	<li id="menu-item-29444" class="category-health menu-item menu-item-type-taxonomy menu-item-object-category menu-item-29444"><a href="https://scienceline.org/topic/health/">Health</a></li>
	<li id="menu-item-29577" class="category-social-science menu-item menu-item-type-taxonomy menu-item-object-category menu-item-29577"><a href="https://scienceline.org/topic/social-science/">Social Science</a></li>
	<li id="menu-item-29447" class="category-physical-science menu-item menu-item-type-taxonomy menu-item-object-category current-post-ancestor current-menu-parent current-post-parent menu-item-29447"><a href="https://scienceline.org/topic/physical-science/">Space, Physics, and Math</a></li>
	<li id="menu-item-29451" class="category-tech menu-item menu-item-type-taxonomy menu-item-object-category menu-item-29451"><a href="https://scienceline.org/topic/tech/">Tech</a></li>
	<li id="menu-item-29452" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-29452"><a href="https://scienceline.org/topic/video/">Video</a></li>
	<li id="menu-item-29453" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-29453"><a href="https://scienceline.org/topic/audio/">Audio</a></li>
	<li id="menu-item-29454" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-29454"><a href="https://scienceline.org/series/">Series</a></li>
</ul>
</li>
<li id="menu-item-29455" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-29455"><a href="#">Special Projects</a>
<ul class="sub-menu">
	<li id="menu-item-34627" class="menu-item menu-item-type-taxonomy menu-item-object-special_project menu-item-34627"><a href="https://scienceline.org/special-project/plastic/">Plastic</a></li>
	<li id="menu-item-29787" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-29787"><a target="_blank" rel="noopener" href="https://nyujournalismprojects.org/pixel/">Pixel</a></li>
	<li id="menu-item-29449" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-29449"><a href="https://scienceline.org/topic/field-trips/">Field Trip</a></li>
	<li id="menu-item-29448" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-29448"><a href="https://scienceline.org/topic/blogs/para-2016/">|para| 2016</a></li>
	<li id="menu-item-29456" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-29456"><a target="_blank" rel="noopener" href="http://projects.nyujournalism.org/dormancy/">Dormancy</a></li>
	<li id="menu-item-32185" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-32185"><a href="https://scienceline.org/special-project/distanced/">Distanced</a></li>
</ul>
</li>
<li id="menu-item-29457" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-29457"><a href="https://scienceline.org/about/">About</a></li>
<li id="menu-item-29458" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-29458"><a href="https://scienceline.org/subscribe/">Subscribe</a></li>
<li id="menu-item-29459" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-29459"><a href="mailto:scienceline@gmail.com">Contact</a></li>
</ul>

SPACE, PHYSICS, AND MATH

## [Why fungi adapt so well to life in space](https://scienceline.org/2018/03/fungi-love-to-grow-in-outer-space/#)

>  _In many ways, these little microbes are better prepared for space travel than we are_ 

MATTHEW PHELAN • MARCH 7, 2018

![输入图片说明](https://foruda.gitee.com/images/1676825120666555060/9e5cd19f_5631341.png "屏幕截图")

> Crew members routinely take air, water and surface samples on the International Space Station to monitor changes in the microbiome for their personal health and safety. [Image credit: NASA | Public Domain]

sig/starship/[Why fungi adapt so well to life in space.md](../starship/Why%20fungi%20adapt%20so%20well%20to%20life%20in%20space.md)


## SCIENCELINE STAFF

- EDITOR IN CHIEF : [Ellyn Lapointe](https://scienceline.org/author/elapointe/)

  - MANAGING EDITOR : [Emily Driehaus](https://scienceline.org/author/edriehaus/)
  - MULTIMEDIA EDITOR : [Timmy Broderick](https://scienceline.org/author/tbroderick/)

- SECTION EDITORS

  - ENVIRONMENT : [Madison Goldberg](https://scienceline.org/author/mgoldberg/) · [Kiley Price](https://scienceline.org/author/kjprice/)
  - HEALTH : [Lori Youmshajekian](https://scienceline.org/author/lyoumshajekian/)
  - PHYSICS & TECH : [Calli McMurray](https://scienceline.org/author/cmcmurray/)

- ENGAGEMENT EDITOR : [Alice Sun](https://scienceline.org/author/asun/)

  - FACT CHECKING : [Anna Gibbs](https://scienceline.org/author/agibbs/) · [Gina Jimenez](https://scienceline.org/author/gjimenez/)
  - COPY EDITOR : [Gwendolyn Rak](https://scienceline.org/author/grak/) · [Marlowe Starling](https://scienceline.org/author/mstarling/)

- FACULTY ADVISOR ： [Ivan Oransky, MD](https://journalism.nyu.edu/about-us/profile/ivan-oransky-md/)

  - Distinguished Writer in Residence, New York University

- PUBLISHER : [Dan Fagin](https://journalism.nyu.edu/about-us/profile/dan-fagin/)

  - Professor, New York University
  - Director, SHERP

### SCIENCELINE员工

- 总编辑 : 埃林Lapointe
  - 主编 : 艾米丽Driehaus
  - 多媒体编辑器 : 提米布罗德里克

- 段编辑器 
  - 环境 : 麦迪逊戈德堡 | Kiley价格
  - 健康 : Lori Youmshajekian 
  - 物理与科技 : 愈伤组织McMurray

- 参与编辑 :   爱丽丝的太阳
  - 事实检查 : 安娜·吉布斯 | 吉娜·吉梅内斯
  - 文字编辑 : 格温多林爱你 | 马洛燕八哥

- 教师顾问 :  Ivan Oransky，医学博士
  - 纽约大学特聘作家

- 出版商 : 丹老坏蛋
  - 纽约大学教授
  - 导演,SHERP

### 丹老坏蛋

- SHERP 教授，主任
- 电子邮件: dan.fagin@nyu.edu

丹·费金是科学、健康和环境报道项目的主任，他在该项目中教授环境报道和科学、健康和环境新闻的当前话题。他也是纽约大学科学传播研讨会的创始人和主任。

丹的最新作品《汤姆斯河:科学与救赎的故事》获得了2014年普利策非虚构类奖，被《纽约日寸报》称为“科学报道的新经典”。《汤姆斯河》是时代周刊的畅销书，还获得了纽约公共图书馆的海伦·伯恩斯坦新闻奖、国家科学院的传播奖和环境记者协会的雷切尔·卡森图书奖。《汤姆斯河》被列入了今年的几本最佳书籍名单，包括美国国家公共电台(NPR)将《汤姆斯河》描述为“扣人心弦、令人翻页的环境惊悚小说”，以及《柯克斯评论》(Kirkus Reviews)，称其“迷人”、“有力”。2015年，《汤姆斯河》出版了两个中文版，并被《新京报》和《中国时报》评为年度图书。


费金最近的署名包括《纽约日寸报》、《自然》、《科学美国人》和《石板》。他的下一本书是关于帝王蝶和人类世生物多样性的未来。

14年来，费金一直是《新闻日报》的环境记者，他是两个进入普利策奖决赛的报道团队的主要成员。2003年，他关于癌症流行病学的报道获得了美国科学促进会和国家科学作家协会颁发的两项美国最著名的科学新闻奖。他还是《有毒的欺骗》(1997)一书的合著者，该书入围了年度调查记者和编辑书籍奖。

费金一直是剑桥大学坦普尔顿-剑桥科学与宗教研究员，还曾在马萨诸塞州伍兹霍尔的海洋生物实验室和阿拉斯加的北极生物研究所担任研究员。他是拥有1200名成员的环境记者协会的前任主席。

有关他的书籍和文章的更多信息，请访问他的网站。 http://danfagin.com/

 **发表作品** 

- The Washington Post 2021年5月28日
  > [这些长着翅膀的禾多民变成了难民——逃离了我们](https://www.washingtonpost.com/opinions/2021/05/28/how-amazing-monarch-butterfly-migrants-became-refugees-us/)

- The New York Times 2016年4月29日
  > [土也雷vs.百万君主](http://www.nytimes.com/2016/05/01/opinion/sunday/in-a-poor-mexican-town-saving-butterflies-or-creating-jobs.html?_r=0)

- The New York Times 2014年4月27日
  > [有毒的繁荣](http://www.nytimes.com/2014/04/26/opinion/sunday/switzerlands-toxic-prosperity.html?hpw&rref=opinion)

- Scientific American 2013年10月24日
  > [为什么我们应该接受转基因标签](http://www.scientificamerican.com/article.cfm?id=why-we-science-should-accept-gmo-labelings)

- Slate 2013年7月10日
  > [live和Tweeting](http://www.slate.com/articles/health_and_science/medical_examiner/2013/07/social_media_communication_facebook_twitter_linkedin_thought_i_was_dying.html)

- The New York Times 2013年1月11日
  > [癌症循环，从这里到中国](http://www.nytimes.com/2013/01/12/opinion/a-cycle-of-contamination-and-cancer-that-wont-end.html?hp)

- Nature 2012年10月25日
  > [毒理学:学习曲线](http://www.nature.com/news/toxicology-the-learning-curve-1.11644)

- Scientific American   2008年8月1日
  > [中国的污染正在毒害儿童吗?](http://www.scientificamerican.com/article.cfm?id=chinas-children-of-smoke)

- Scientific American  2007年12月16日
  > [对氟化物的三思而行](http://www.scientificamerican.com/article.cfm?id=second-thoughts-on-fluoride)

- Nieman Foundation  2005年12月1日
  > [科学和新闻无法结合](http://www.nieman.harvard.edu/reports/05-4NRwinter/Fagin-NRw05.pdf)

# [汤姆斯河](https://book.douban.com/subject/26341543/)

<img align="right" width="169px" src="https://foruda.gitee.com/images/1676828309503349856/adbde02a_5631341.png">

- 作者: [美] 丹·费金
- 出版社: 上海译文出版社
- 副标题: 一个美国"癌症村"的故事
- 原作名: Toms River: A Story of Science and Salvation
- 译者: 王雯
- 出版年: 2015-5
- 页数: 502
- 定价: 59.00
- 装帧: 平装
- 丛书: 译文纪实
- ISBN: 9787532769117

### 内容简介

它曾是一个人口不到两万的农业小镇，名字取自于那条流经当地的小河。

从莱茵河辗转俄亥俄河谷扩张近百年后，1952年，世界三大化工巨头——汽巴、嘉基和山德士——来到汤姆斯河镇，成为当地最大的私人雇主，也使小镇发展成全美经济增长最快的地区之一。

1957年，镇上的供水系统第一次检测出了化学污染物。

1967年，对镇上的供水系统做出调查后，当地水务公司和化工厂达成秘密协议，并发布调查结果：“镇上的饮用水绝对安全。”

1974年，居民再次发现饮用水味道不对，当地报纸开始关注污染和健康的“传闻”，县卫生部调查后不了了之。1975年，公司再次做出回应：“饮用水绝对安全。”

1982年，13岁的兰迪被确诊患上了成神经管细胞瘤。纽约医院的医生发出感慨：“又一个从汤姆斯河镇来的。”当地居民谈癌色变。此后，环保组织“绿色和平”介入调查。

1984年，汽巴-嘉基排污管道破裂，天机泄露。

1986年，新泽西州卫生部展开全面调查。

1991年，汽巴-嘉基关闭了汤姆斯河镇的排污管道。1996年，汤姆斯河镇化工厂关闭。1997年，瑞士总部把化工生产迁移到了中国和印度。

目前，中国是世界上最大的化工产品生产国和使用国。1996-2010年，中国的苯、乙烯和硫酸的产量翻了两番。巴斯夫，目前世界上最大的化工公司，在中国有7000名员工和40家工厂。陶氏化学，在中国有4000名员工和20家工厂。

### 作者简介 

- 作者简介：

  丹•费金，纽约大学新闻系副教授，阿瑟•卡特新闻研究院“科学、健康和环境报告项目”主任。费金曾担任《华盛顿新闻报》环境记者长达十五年，两度获得普利策新闻奖；他关于癌症流行病学的文章，荣获美国科学促进会科学新闻奖，美国国家科学作家协会社会科学奖；2013年出版的《汤姆斯河——一个美国“癌症村”的故事》，获2014年非虚构类普利策图书奖、2014年蕾切尔•卡森奖等多个奖项。

- 译者介绍：

  王雯，笔名“白鸟”，环境科学博士，高校教师，科学传播团体“科学松鼠会”成员，《冷浪漫》作者之一。作品发表于《新发现》、《瞭望东方周刊》等多家刊物。

### 目录

- 目录

- 序幕 等待

  > 某些东西，某些人，应该为他的癌症，和他痛苦的人生负责。。。。。。还有很多事情有待发掘，一旦揭开，那将是爆炸性的。

- 第一部分 冰激凌店

  - 第一章 海盗
    > 在汤姆斯河镇，历史经常是可交易商品。
  - 第二章 看不见，摸不着
    > 无法感知的事物中始终存有敌意，这些矿产的蒸汽也能把人杀了。
  - 第三章 第一枚指纹
    > 染色的河水、污染的水井、有色的烟雾和难闻的气味——汤姆斯河镇的居民开始后悔了。
  - 第四章 秘密
    > 如果希望有一个持续增长的繁荣地区，我们必须明白：周围的环境必然会改变。”“尽管人人都向往田园生活，恐怕没人愿意过得像印第安人。
  - 第五章 夏基和科伦坡在乡土酒吧
    > 这致命的烟雾。。。。。。在伦敦城上空盘旋了几个世纪，如同一个死亡天使。但它可以被法律驱散。
  - 第六章 细胞们
    > 神秘和恐怖经常是癌症的亲密伙伴。。。。。。在孩子身上发现恶性肿瘤特别令人难过，其中一个原因是这太罕见了。通常，癌症都是上年纪人得的病。
  - 第七章 卡迪纳尔大道上
    > 住在卡迪纳尔大道上，家里有个患了癌症的孩子，不联想到汤姆斯河镇化工厂是不可能的。

- 第二部分 决裂

  - 第八章 水和盐
    > 所有好的作品都出自对管束的对抗。我们挑战的是公司、政客和政府部门全部的社会结构。
  - 第九章 厨房里的嬉皮士
    > 从政府向着正规管理化工行业第一步开始，科学就将既做武器，又当靶子。
  - 第十章 填色比赛
    > 它是这个区域最重要的雇主，没人质疑过他们做的任何事。
  - 第十一章 病例
    > 一个四十五岁的男人走进办公室，说他尿血。。。。。。过去十二个月，这已是辛辛那提化工厂工人中检出的第四例膀胱癌了。
  - 第十二章 可接受风险
    > 汽巴-嘉基正在终止汤姆斯河镇几乎所有的化工生产。。。。。染料原料的生产将转移到亚洲，那里，工资和监管也更低。
  - 第十三章 朋友和邻居们
    > 对快乐平原的地下水进行适当的清理需要十一年的时间，大约需要花费六百万美元。。。。。。汽巴-嘉基预计将花费三倍的时间，花费三十五倍的成本。

- 第三部分 清算

  - 第十四章 两间病房，双重打击
    > 有一段时间，我们每周都会收治一名从汤姆斯河镇来的患者。这成了病房里的一种黑色幽默。。。。。。不知道汤姆斯河镇的下一个病人啥时候到？
  - 第十五章 癌症集群“退散”
    > 1980年至1988年间，欧申县十四岁以下儿童中确诊的脑和神经系统肿瘤病例有三十七例，这个数据比预期值高了百分之七十。
  - 第十六章 一往无前
    > 如果原因真的在于污染，受害最深的可能是幼儿。
  - 第十七章 看不见的伤害
    > 汤姆斯河的怒火将不止倾泻在排污者的头上，还会波及多年来让他们逃离法网的政客和管理人员。
  - 第十八章 海里的木塞
    > 汤姆斯河镇的居民在自家水龙头里喝到了低浓度的有毒废水。这不容辩驳。
  - 第十九章 期待
    > 这些家庭期待着能揭开癌症集群的原因。他们的要求持久而有力：我们要的是答案。

- 第四部分 真相

  - 第二十章 局外人
    > 很多人真心希望整个事情快点过去。
  - 第二十一章 斡旋
    > 在排除各种癌症集群原因的过程中，水和大气污染的因素日益凸显出来。
  - 第二十二章 血液分析
    > 如果癌症与年龄密切相关，又为什么会有儿童癌症？
  - 第二十三章 关联
    > 汤姆斯河一案 “过去了”，但它“没有解决”。
  - 第二十四章 遗留问题
    > 污染——排入河流和地下水的污染物，工厂排放的废气——使癌症成为中国主要的死亡原因。


- [Toms River: A Story of Science and Salvation (EXCERPT) | HuffPost](https://www.huffingtonpost.com/dan-fagin/toms-river-book_b_2867565.html)  
  HuffPostHuffPost|1875 × 2850 jpeg|Image may be subject to copyright.

  ![输入图片说明](https://foruda.gitee.com/images/1676918983104454871/270509cb_5631341.jpeg "2013-03-13-TomsRiverCover.jpg")


![输入图片说明](https://foruda.gitee.com/images/1676829052799046753/9d63c3af_5631341.png "屏幕截图")

# [汤姆斯河：科学和救赎的故事](https://zhuanlan.zhihu.com/p/19749443)

[郑惠文](https://www.zhihu.com/people/zhenghuiwen) : 产品营销经理 - 亚马逊 (Amazon.com)

汤姆斯河：科学和救赎的故事

采访/翻译：@[郑惠文](https://www.zhihu.com/people/f034f7ba2626e4dddcdc3a2eb95ca402)

纽约大学纽约大学亚瑟-卡特新闻学院位于库柏广场20号，正面对汤姆·梅恩设计的库柏联盟学院新教学楼。和这位2005年普利兹克奖得主的大胆设计相比，新闻学院的教学楼显得面目模糊——只是普通的七层建筑，甚至看不出是学校。

经保安登记，可从电梯上六楼。新闻学院只占两层，上七楼只能走楼梯。丹·费金教授的办公室在七楼朝南的一间，门口贴着他的作品《汤姆斯河：科学和救赎的故事》打印海报，而窗外是灰蒙蒙的春雨里的曼哈顿下城。这本书荣获普利策新闻奖非虚构类作品奖，中文翻译版本大概年内面世。

《汤姆斯河：科学和救赎的故事》讲述汤姆斯河，一个深受工业污染之害的新泽西小镇的真实故事。这里人口不到十万人。上世纪50年代起，瑞士化工企业汽巴·嘉基在此建立了化工厂，使这个农业小镇工业化。但化工厂直接向河流排放污水，废品直接焚烧，使得河流、土壤和空气都遭到污染。与五十年的危害相比，短暂繁荣简直不值一提。直到九十年代，汽巴·嘉基彻底结束生产，而至少70户家庭受害，儿童癌症集中爆发。癌症儿童家庭与汽巴·嘉基和联合碳化物等化工企业达成总金额高达3500万美元的庭外和解而告终。

从2006年到2012年，丹·费金历时六年深入该地写作调查报道，完整而科学地呈现汤姆斯河小镇的受害与斗争——贪婪的商人、渎职的监管、无奈但沉默的居民，这桩悲剧绝对具有警世意义。

普利策奖的颁奖词说：“本书巧妙地将调查报道和历史研究相结合”，而《纽约日寸报》更评论本书是“科学写作的新经典”。记者于4月30日下午对费金教授进行专访。


1. 您的写作初衷是以汤姆斯河的悲剧警示世界。这件事的曝光已经近20年了，您怎么考虑这件事的时效性和效果？

我想让更多人了解汤姆斯河的悲剧与经验。可能新泽西人对这件事早有耳闻，但这个世界的其他大部分对方对它一无所知。我认为这是一个重要而有意义的故事。即便我所报道的事故距今已久——长的40至50年，短的也有20年——它们和今天的世界依然紧密地关联着。


2. 当面对“新泽西乡下的事情跟我何干”的质疑时您会如何回应？

我正希望通过呈现汤姆斯河这个缩影来解释更大的问题。新闻写作所想要呈现的“缩影”正是小而具体的。事实发掘越详细越好，用人们理解的方式写作，并让他们迁移思考到自己的生活，延展特殊事例的广大意义。写作汤姆斯河的故事，我想让读者们建立联系，以同理心思考“这样的事情在我的家乡也会发生”。


3. 140人受访，您能不能回忆一下这些采访对象中有多少人是居民？能举例谈谈某一次对本书影响非常的采访？您如何处理这样巨量的信息？

140人种有一半人是居民，另一半是对事件发展非常重要的专家、律师等局外人。我并没有把70个家庭逐个采访过来，只是有选择并保护隐私地采访了六七户。

令我印象最深的是一个叫雷·林沃斯的父亲，他们家就住在工厂旁边，而他的儿子兰迪·林沃斯因此而夭折。他带我进家里参观，展示昔日兰迪的房间和照片，认真回答我的各种问题。我知道他心里一定很不好受，但他很配合调查。我自己也有两个孩子，简直无法想象同样的事情发生在我身上会怎么样。平时连我的小女儿弄伤了脚趾我都会非常着急，因为我无论如何不能失去她。他们愿意相信我，我非常感激。既然他们愿意告诉我发生在他们身上的故事，那么我就对他们负有责任，我就一定要如实呈现故事，不使他们的信任落空。


> 雷·林沃斯《第七章 在卡迪诺车道》
> 
> “雷·林沃斯和他的妻子雪莉在卡迪诺车道定居的时间更长—他们最初在1986年搬进这里的一间错层式砖房时，房屋后还没有那些围栏。他们家的院子有一条小路，一直通向林子里，这使得他们这处四分之一亩大的房产显得别具牧场风格。即使在上个世纪70年代初期，院子后面的围栏建起来的时候，林沃斯家的孩子——1967年出生的吉尔和1969年出生的兰迪——也从未觉得那道围栏是他们不被允许翻越的障碍。“你问我兰迪是不是曾经翻越围栏跑到汽巴·嘉基的林子里面玩？当然了。住在我们这片的孩子们都会这样。”林沃斯回忆道。那些还在还会在附近的河里游泳。林沃斯一家人知道这片土地属于汤姆斯河化工厂，但是他们并不觉得这有什么不妥。相反，他们还挺喜欢这所私有企业的。有时候这里的空气会弥漫着一股难闻的味道——通常是在晚上，因为晚上正是化工厂的那些看不见但闻得着的烟囱最忙碌的时候。他们家朝西的那扇窗子正对着化工厂，从肉眼就可以看出西窗比屋子里其他的窗子要粗糙得多，它的窗沿就像是被砂子磨了一样。但这并没有对住在屋子里的人造成什么大碍，因此很容易被忽略。
> 
> “在卡迪诺车道，人们有时会谈论这片区域里奇怪的疾病，就像一英里之外的普兰兹牛寺普林路一样，那里的人们也生活在一家位于赖克农场的化工污水厂的阴影之下。但与那里的居民不同的是，卡迪诺车道和橡树岗其他地方的人们都是从汤姆斯和水公司的管道取水，并没有直接从他们后院的水井里打水。这片区域在60年代就用上了公共自来水，大多数时候，人们都觉得这些水喝起来很正常。但还有一些家庭仍然从他们的后院钻井打水，因为这样可以节约一点水费。他们用从井里打上来的水浇灌草坪和花园，也用它来给自家的游泳池注水，但他们几乎不饮用井水，也不用它来洗澡，因为井里的水总是有一股怪味，闻起来像涂料稀释剂一样。”


我把所有的文档都保存在电脑里，方便随时追查。找到不同的文件，建立其关联。这一次我只用了文本文档和交叉查询表格，没有使用数据挖掘技术手段，但我想下次会试试。我基本是孤身一人调研和写作，往返于纽约和汤姆斯河。自从2006年第一次到汤姆斯河以来，我为新书提案查资料和做采访，然后和兰登书屋接洽，再编辑名单逐个采访。我还在海洋县图书馆里呆了很长时间，查询旧报纸、档案和报告。我就是从这里发现各种不同的主题和人物，然后再一个个找到他们，尽量做面访。


4. “没有证据显示当时有人曾对化工厂提出反对意见”这句话多次出现在书中。请问您觉得这种一边倒的意见或表态的根本缺失是怎么造成的？

一方面，这件事毕竟发生在太久以前。工厂是五十年代建成的，那时候的人们对化工厂的态度和现在完全不同。他们对污染所知甚少，环境问题基本还没上升到公共议题的程度。另一方面，汤姆斯河小镇当时非常穷，只有家禽养殖和夏天的少量观光客。所以当时人们对化工厂建成的前景欢呼雀跃，期待它增加就业、带来财富。每个人都觉得这是好事，而且化工厂一开始确实也为当地社区做了很多，它们甚至为小镇建了建设高尔夫球俱乐部，使其发展。


5. 当时正文府的反应是怎样的？新泽西州正文府似乎一直深受贪污恶名的困扰。

州正文府和当地镇正文府对这个公司非常渴求。我在书中曾几次描述人们对此可能有担心和反对，但是公司和正文府官员一口否认工厂会带来任何问题。因为有利于经济，正文府选择站在了企业的一边。事实上这件事在世界上的其他地方也是这样的。确实新泽西正文府名声糟糕。但是我不希望人们单单因此觉得事情只会在新泽西发生。新泽西之外的美国，美国之外的世界，钱权勾结的戏码处处上演。


6. 您在展开调查时遇到过哪些方面的阻力？最大的困难是什么？

太多困难了。第一件就是时间久远。许多当事人都去世了，无法采访。我只能依赖于文档资料，但是很难搞到，许多文档还是保密的。我只能尽量查阅公开文档，或尽量请人帮我拿到。第二件是人们的抵触情绪。很多人不想再次揭开当年的疮疤，不想回忆痛苦，希望继续新生活。第三件是经济发展。当地人很担心负面报道会影响当地的工业和地产。应对这些挑战，我能做的只是说服他们：记录真实，寻找历史真相绝对是正确而必须的。而且幸运的是，汤姆斯河有许多人不想让事故被淡忘，也不想他们的教训在其他地方重演。他们对我的调查大有帮助。


7. 中国作家鲁迅曾说过：“可怜之人必有可恨之处”，在书中您也表达了类似“汤姆斯河镇居民因为短期利益一叶障目对现在的灾难性环境恶果难辞其咎”的类似观点。请问您觉得这场沉痛的环境灾难给人们上了怎样的一课？

真实世界里没有绝对的好与坏黑与白，记者要做的就是如实呈现这样的复杂真相。企业主的表现糟糕，他们在事故里应承担主要责任，因为其追求绝对的经济利益而不顾环境影响，造成不可挽回的灾难。比如自来水公司，想方设法证明受污染的水没有问题，而且发现了问题也不及时解决。当地人也犯了短视的错误，经济发展使之盲目乐观，没有停下来想想到底发生了什么。即便喝了味道奇怪的受污染的自来水，但他们也没有提出质疑。


> 自来水公司：《第四章 秘密》
> 
> “早在1965年的八月中旬，汤姆斯河自来水公司就已经知晓偶氮染料对三口70英尺的浅井的污染事实，这三口井为全镇7000个家庭和商家供水。正文府没有预警，而报纸也没有一篇报道。汤姆斯河的生活一如往常，即便水尝起来不对劲，但因为过热的天气使它们不得不继续大量摄水。只有两家单位知道到底发生了什么——自来水公司和化工厂——但它们是多年的老朋友，合作紧密，甚至有时偷偷摸摸。
> 
> “根据一份1965年3月23日汤姆斯河自来水公司的内部报告指出，一口井里的水散发强烈意味，并且有可见的显著颜色，并查出污染源是工业染料。但它们所做的补救措施是朝水中添加更多的氯来掩盖颜色，剂量之大已经绝对超过了安全标准。而干燥的夏天使这种情况更加恶化。”

当地人确实应为不当行为承担责任，但事情的转折同样也是因他们的担当而起。不同的人，不同的时间，不同的境遇。我在书中描述了最坏的人性之恶，也发现了最好的人性之善。汤姆斯河的许多人都很勇敢，从集体的角度想问题，不仅仅关注个人利益。如果他们没这样做的话，这个事件可能永远不会曝光，永远不会变好。

汤姆斯河留给世界的教训很多。首先，提出质疑，加强公共参与。永远不要依赖别人来发现问题做出改变，而要自己亲身参与，要足够警惕。这是民主社会里的公民责任，需要关注社会问题，需要积极表达。事实上汤姆斯河的问题开始好转就是当普通人也开始发声，开始从集体的角度考虑问题。其次，科学调研应该独立，不要受利益集团的制约。书中太多次描述到，科学被用作武器来支持某种论断，而非进行调查和呈现真相。科学决不能被政治左右，也决不能跟利益勾结。我们需要强而独立的科学调查，不受其他因素羁绊。再次，流行病学是研究疾病发生的规律，要将环境问题和健康问题建立关联。我们虽然有癌症登记处，也要求企业报告他们的排污状况，但是并没有将这些信息结合来发现问题。只有当人们已经生病或去世，人们才在事后进行处理。调查机构的参与太滞后了，因为悲剧已定。


8. 可是您是否信息披露造成公众的恐慌？

是的，恐慌是我最不想看见的事情。这不仅仅关系到正文府监管和公司道德，更重要的是公民要学习如何正确应对信息，如何更理性地应对和解决问题。当汤姆斯河的问题初次曝光，当地人简直出离愤怒，几乎引起了马蚤乱。事情逐渐平静的转机出现在人们开始花时间了解真相，而正文府不再试图隐瞒。公司和正文府最糟糕的作为就是将人们隔绝在信息之外，因为这样只会让人们做出最坏的假设。你越想隐瞒，人们越不顾一切地想知道你在躲躲藏藏些什么。我想现在在中国发生的事情也是这样。审核越严格，人们越想方设法的交流信息。而正文府其实做不到控制一切。

9. 您是否了解中国的环境问题和环境调查？

事实上我只在互联网上读过一些文章，与其中一些记者打过交道。但我想一方面可能是发展过速，另一方面可能是监管体系不完善。确实中国现在发生了很多问题，但我不认为这是中国独有的问题。只是因为制造业集中于中国，所以我尤其关注到底中国发生了什么。

我在几年前曾去中国旅行，去了上海、杭州和重庆。或许你知道重庆铜梁？那是一个十万人的灰色小县城，因为火电厂排污使空气污染。哥伦比亚大学儿童环境健康研究中心的报告显示，在母体内即暴露于受污染空气，婴儿的DNA可能会被改变，使他们更容易患上发育性疾病。那个火电厂于2004年已经关闭，不过科研仍在继续。

> 中国重庆：《第二十五章 遗留问题》
> 
> “39岁的刘玉书（音）长着一张59岁的脸。她脸上的皮肤很松弛，看起来就像是长期睡眠不足一样。每天晚上，她都在重庆儿童医院的血液科病房地板上铺上自己的毯子，睡在她9岁孩子病床旁狭小的空间里。在中国中部地区的这家医院里，还有很多病人家属，甚至是患者，都睡在住院部拥挤的走廊里，所以刘玉书觉得自己算是幸运的了。刘玉书和她的丈夫都是建筑工人；丈夫负责砌砖，刘玉书就负责混合胶黏剂。一年前，他们把孩子留给婶婶照顾，俩人则前往几千英里之外的广州找工作。在经济繁荣的南方地区，外资企业如同雨后春笋般迅速地增长。刘玉书在广州找到了工作，但她不能一直待在那里。留在家里的孩子一直高烧不退，去附近的诊所检查，检验结果表明他的血小板含量异常的低，孩子处于危险状态。后来他们又去了重庆儿童医院进行骨髓化验，在那里，他们得到了同样的结果：急性淋巴细胞性白血病，和其他儿童癌症一样，难以治愈。
> 

> 
> “重庆儿童医院的病房里，以及中国其他地区都蒙上了一层阴影，这不仅仅是因为中国的癌症治疗标准远远低于美国和欧洲的治疗标准。在过去的几年里，与汤姆斯河区域流行的相同的儿童疾病在中国广东、湖南、重庆频频出现。中国的独立记者和积极分子们不断地在寻找非官方、未经科学证实的关于污染和癌症的数据，自己制作“癌症村”地图，将它们发布到互联网上。他们的调查显示，受灾最严重的地区往往都在城市之外，因为中国最具污染性的工厂通常都建立在郊区，同时，因为郊区的社区还是有足够的社会稳定性，所以居民们能够意识到他们所处的危险环境。（非传染性的疾病在闹市区几乎是无法被察觉到的。）就连卫生部也承认，污染——包括排放进河流和地下水的废料、工厂和电站排放到空气中的废气——已经使得癌症成为了中国人口死亡的第一大原因。但由于在中国癌症病例不会被记录在案，正文府也几乎不会去调查癌症集发的地区，那些非官方的“癌症村”地图通常就成为了获得当地癌症发病规律的最好资料，包括医生在内的一些研究人员都会给予这些数据很大的信任。刘晓梅（音）是重庆儿童医院血液科的一名外科医生，说她的病人中有很多都是工厂工人的孩子。问她是否认为是污染导致这些孩子患病，她说：“我认为是的。但是要肯定这一点是非常困难的。”
> 
> “不可否认的一点是，中国如今已超过美国成为了世界上最大有毒化学产品的制造市场和消费市场。其他发展中国家的化工产业也在蓬勃发展，但中国在其中的地位是不可忽视的。1996年，汤姆斯河工厂关闭，在这一年，美国和欧洲国家分别生产了多于中国八倍多的塑料；而到了2010年，中国的塑料产量已经与美国和欧洲的塑料产量之和持平。在同时期，中国的笨、乙烯和硫酸产量上涨了四倍。其他化工原料的产量同样也有大幅上升，包括自1856年就开始作为染料剂的苯胺。正是那些熟悉的企业造就了化工产业的繁荣。巴斯夫，原德国苯胺制造商如今已经成为了世界上最大的化工公司，在中国拥有7000多名员工和40多个工厂。现在，巴斯夫正在重庆建造一个占地50多公顷的工厂，用于制造苯胺、石肖基苯和亚甲基二苯——其中亚甲基二苯的原料是一种毒性很大的气体，碳酰氯，当年汤姆斯河就是用它来制作含氯染料的。”

我很尊敬那些在中国从事独立调查的记者，感谢他们付出的努力。至于具体操作性的建议，最重要的就是尽可能多地学习科学，不能轻易做任何假设。去尽量多地了解事实真相，去了解到底危险和威胁是什么。越多了解科学，越能做出更好的科学报道。基于事实和科学的报道最是重要，不能只写作个人的意见和观点。其次是不要吝惜和同行分享信息，因为集体的效力会比个人单打独斗更有效。

我只在大学里选过自然科学课程。有趣的是，我现在当科学写作的教授，所有的学生都有很强的科学背景，而我自己习得知识的途径都是课堂之外的。


10. 就您的经验而言，如何客观写作？当在采访中遇到大量的偏见和情绪表达时，如何辨清真相？

首先，我们每个人其实生来即带有偏见。重要的是能够意识到偏见的存在，并将它们也同样纳入考虑范围。再者，最好的环境报道一定也要同时考虑读者愤怒冲动的情绪，这些反应都是自然而然的。因而，写作者确实需要以通情理的方式呈现真相，以适合方式切入，呈现真相。我想做的事情也是如此，基于事实，兼容情绪。


11. 环境写作有一本经典是《寂静的春天》，请问您对这本书怎么看？你又怎么看这本书后来造成的争议和恐慌？这或许是调查报道激发环保抗议的最大型活动。您是否担心您的书也会给人带来对发展和环境矫枉过正的担忧？

雷切尔·卡森是一个非常好的作者，写作优美流畅的文字。我也确信她是个出色的科学家。有些人低估她“过于夸张和溢出”，但我从不这样认为。因为方法格不同，我不会像她那样写作，但我可能也就不会她那样影响无数人。不过她也因此做到了更了不起的事——影响世界。她的积极作用远远大于她的消极影响，无论如何她都功大于过。她对农药滥用的断言很大程度上是正确的，尽管人们一度对她群起而攻之。即便风格抒情，她的科学推断非常扎实可信。她是英雄，我很喜欢这本书。


12. 对您影响最大的作家或记者是谁？

约翰·马克非。他是杰出的科学写作者，我非常喜欢她的风格和态度。他非常严谨地做研究，不在笔尖流露情绪，并且通俗易懂。


去年费金教授曾为《纽约日寸报》撰文，表达他对中国环境文提的担忧。在这篇名为《工业致癌的悲剧在中国重演》的文章里有一句话：“然而，21世纪全球化的现实是，我们任何一个人都不能认为，把化学工业赶出我们的社区，我们就终止了他们的危险行为。这些起源于巴塞尔的化工行业工作，后来搬到了辛辛那提及汤姆斯里弗，现在又到了山西及中国其他产煤大省。”让记者很快想到书中的另一句话：“人类和污染的纠缠一直在轮回：富人扔掉他们不想要的，而穷人不顾一切地将他们所能得到的东西拥入怀中。”


书籍摘要翻译/编辑：邓楚阳

龙韵对本文亦有贡献

编辑于 2015-05-07 12:41

---

<img align="right" width="99px" src="https://foruda.gitee.com/images/1676831044831849719/e6b5e880_5631341.png">
《寂静的春天》是美国科普作家 蕾切尔·卡逊 创作的科普读物，首次出版于1962年。 [1] 在这本书中，卡逊以生动而严肃的笔触，描写因过度使用化学药品和肥料而导致环境污染、生态破坏，最终给人类带来不堪重负的灾难，阐述了农药对环境的污染，用生态学的原理分析了这些化学杀虫剂对人类赖以生存的生态系统带来的危害，指出人类用自己制造的毒药来提高农业产量，无异于饮鸩止渴，人类应该走“另外的路”。 

- [寂静的春天（美国蕾切尔·卡逊著科普读物）](https://baike.baidu.com/item/寂静的春天/821673)_百度百科  
  baike.baidu.com/item/寂静的春天/821673

- [『读书』蕾切尔·卡森《寂静的春天》——这是一个没有声音的春天](https://www.jianshu.com/p/3c19a3bf799a) - 简书  
  jianshu.com|2000 × 1500 jpeg|Image may be subject to copyright.

  ![输入图片说明](https://foruda.gitee.com/images/1676831245731290972/a46cc2ae_5631341.jpeg "10811533-fdd0ad25859c0d0e.jpg")