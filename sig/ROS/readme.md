<p><img width="706px" src="https://images.gitee.com/uploads/images/2022/0705/044518_66933a69_5631341.jpeg" title="icra2022.jpg"></p>

### 最新专题 [F1TENTH](F1TENTHabout.md)

 **https://f1tenth.org/about.html** 

![输入图片说明](https://images.gitee.com/uploads/images/2022/0705/034924_6e1c71c3_5631341.jpeg "upenn-ppl.jpg")

F1TENTH is an international community of researchers, engineers, and autonomous systems enthusiasts. It was originally founded at the University of Pennsylvania in 2016 but has since spread to many other institutions worldwide. Our mission is to foster interest, excitement, and critical thinking about the increasingly ubiquitous field of autonomous systems. We fulfill this mission with four pillars:

1.  **[Build](https://f1tenth.org/build.html)**  - We designed and maintain the F1TENTH Autonomous Vehicle System, a powerful and versatile open-source platform for autonomous systems research and education.

2.  **[Learn](https://f1tenth.org/learn.html)**  - We create courses that teach the foundations of autonomy but also emphasize the analytical skills to recognize and reason about situations with moral content in the design of autonomous.

3.  **[Race](https://f1tenth.org/race.html)**  - We bring our international community together by holding a number of autonomous race car competitions each year where teams from all around the world gather to compete.

4.  **[Research](https://f1tenth.org/research.html)**  - Our platform is powerful and versatile enough to be used for a variety of research that includes and is not limited to autonomous racing, reinforcement learning, robotics, communication systems, and much more.

![输入图片说明](https://images.gitee.com/uploads/images/2022/0705/174939_3f2a5022_5631341.png)

 **[10th F1TENTH Autonomous Grand Prix](https://icra2022-race.f1tenth.org/)** 

IEEE International Conference on Robotics and Automation (ICRA) 2022
- May 23th - May 25th 2022  
- Philadelphia, USA

![输入图片说明](f1tenth_video.gif) <img height="293px" src="basic_sim_f1tenth.gif">

- [ABOUT](ICRA2022.md#about)
- [TIMELINE](ICRA2022.md#timeline)
- [RACE RESOURCES](ICRA2022.md#race-resources)
- [GETTING STARTED](ICRA2022.md#getting-started)
- [Participants](ICRA2022.md#participants)
- [RESULTS/STREAM](ICRA2022.md#resultsstream)


