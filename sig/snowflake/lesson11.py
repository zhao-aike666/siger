a=1
b=10.0
c="hello"

print( a, b, c )

a = b = c = 1

print( a, b, c )

a, b, c = 1, 2, "hello"

print( a, b, c )

a, b, c, d = 20, 5.5, True, 4+3j

print( type(a), type(b), type(c), type(d) )

str = 'hello world!'

print( str )