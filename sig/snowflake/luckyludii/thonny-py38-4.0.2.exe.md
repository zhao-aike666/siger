thonny-py38-4.0.2.exe was downloaded from offical website thonny.com, url is following:  
https://github.com/thonny/thonny/releases/download/v4.0.2/thonny-py38-4.0.2.exe

that's testing record was written at [issues](https://gitee.com/siger62/siger/issues) [#I6UTSO](https://gitee.com/siger62/siger/issues/I6UTSO)

### step by step manual:

![输入图片说明](https://foruda.gitee.com/images/1681303417481125445/eb3f2e1e_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1681303456863510122/b8e1cca4_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1681303537618031124/811b6d20_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1681303583177521386/b5a118fe_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1681303646401930385/26ce7640_5631341.png "屏幕截图")

the main interface is above, pls using default seting include path. pls must selected create desktop icon.

### prepare the path using the bat.

the console command is python normally used. and the path include the python.exe is important for pip the libs.

```
path=path;C:\Users\hf\AppData\Local\Programs\Thonny;
python -m pip config set global.index-url https://pypi.tuna.tsinghua.edu.cn/simple
python -m pip install pygame
python -m pip install --upgrade pip
```

sig/snowflake/luckyludii/[pypath.bat](pypath.bat) above is the path preparing. each line comment:
1. adding the python.exe directory in path.
2. prepare the pip lib source to tinghua server image.
3. install the pygame lib, it's used by [jiu.py](../jiu.py) for the luckystar games.
4. pip upgrade command sample, that's the important skills for students.

### each step before every lessons

because the xiaoyuan computer envirment is unique. each computer installed the same os and softwares only according to their lesson. and students can't install by themself. so eachtime before the lessons, the students must install first python and envirments. the path.bat is used to this purpose.

1. running the thonny-py38-4.0.2.exe following the above step by steps.
2. start the thonny from desktop success (prehaps the os and softwares envirments maybe not support the thonny version)
3. running the [pypath.bat](pypath.bat)
4. double click the [jiu.py](jiu.py) is same as the [sig/chess/贤者九子棋.py](../../chess/贤者九子棋.py) programed by other students

![](https://foruda.gitee.com/images/1681222439792930610/81c73341_5631341.png)

the success image of jiu.py in the issues [#I6UTSO](https://gitee.com/siger62/siger/issues/I6UTSO) is nessary to starting this lesson.

good luck. 

> the computer teacher need to know the steps in this article. the computer softwares and hardwares envirment was diffrent between schools.
