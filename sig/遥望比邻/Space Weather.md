- [SPACE WEATHER, VOL. 6, S09003, doi:10.1029/2008SW000437, 2008](https://agupubs.onlinelibrary.wiley.com/toc/15427390/2008/6/9)

  1. [Space Weather for Kids](https://agupubs.onlinelibrary.wiley.com/doi/10.1029/2008SW000437)
  1. [New Project Will Measure Electric Currents in Near-Earth Space](https://agupubs.onlinelibrary.wiley.com/doi/10.1029/2008SW000435)
  1. [Sunspot Record Reveals Little to Space Weather Watchers](https://agupubs.onlinelibrary.wiley.com/doi/10.1029/2008SW000440)
  1. [Space Weather as a Hybrid of Basic Research and Applied Science](https://agupubs.onlinelibrary.wiley.com/doi/10.1029/2008SW000423)
  1. [Attenuation of GPS scintillation in Brazil due to magnetic storms](https://agupubs.onlinelibrary.wiley.com/doi/10.1029/2006SW000285)
  1. [Man-made space weather](https://agupubs.onlinelibrary.wiley.com/doi/10.1029/2008SW000406)
  1. [Importance of predicting the dose temporal profile for large solar energetic particle events](https://agupubs.onlinelibrary.wiley.com/doi/10.1029/2008SW000393)
  1. [Validation of the Coupled Thermosphere Ionosphere Plasmasphere Electrodynamics model: CTIPE-Mass Spectrometer Incoherent Scatter temperature comparison](https://agupubs.onlinelibrary.wiley.com/doi/10.1029/2007SW000364)

![输入图片说明](https://foruda.gitee.com/images/1676366186740813219/eb75cb7a_5631341.png "屏幕截图")

# [Space Weather](https://agupubs.onlinelibrary.wiley.com/toc/15427390/2008/6/9)

 **Space Weather**  is a gold open access journal that publishes original research articles and commentaries devoted to understanding and forecasting space weather and other interactions of solar processes with the Earth environment, and their impacts on telecommunications, electric power, satellite navigation, and other systems. 

![输入图片说明](https://foruda.gitee.com/images/1676366161415112314/78b4f441_5631341.png "屏幕截图")

- Download PDF(s)

<div class="issue-items-container bulkDownloadWrapper">
<h3 id="heading-level-1-1" class="toc__heading section__header to-section" title="Feature">Feature</h3>
<div class="issue-item">
<div class="pull-left"><div class="doi-access-wrapper"><div class="free-access access-type"><i aria-hidden="true" class="icon-icon-lock_open"></i><div class="doi-access">Free Access</div></div></div></div>
<div class="clearfix"></div>
<div class="clearfix"></div>
<div class="bulkDownloadInput"><input title="Select article for bulk download or export: Space Weather for Kids" type="hidden" value="10.1029%2F2008SW000437" data-has-pdf="true"><div class="hidden">free</div>
</div><a class="issue-item__title visitable" href="/doi/10.1029/2008SW000437">
<h2>Space Weather for Kids</h2></a><div style="float: none; position: static; list-style:none;" class="loa comma loa-authors-trunc">
<div class="comma__list"><span class="comma__item"><a href="/action/doSearch?ContribAuthorRaw=Kumar%2C+Mohi" title="Mohi Kumar"><span class="author-style">
 Mohi Kumar</span></a><span class="comma-separator">,&nbsp;</span></span></div>
</div>
<ul style="float: none; list-style: none;" class="rlist--inline separator issue-item__details">
<li class="ePubDate" style="display: inline-block;margin-left: 0;"><span>First Published:</span><span>11 September 2008</span></li>
</ul>
<div class="content-item-format-links">
<ul class="rlist--inline separator issue-item__links">
<li><a title="First page image" href="/doi/abs/10.1029/2008SW000437">First Page</a></li>
<li><a title="
            Full text
        " href="/doi/full/10.1029/2008SW000437">
                    Full text
</a></li><li class="PdfLink"><a title="EPDF" href="https://agupubs.onlinelibrary.wiley.com/doi/epdf/10.1029/2008SW000437">PDF</a></li><li><a href="https://agupubs.onlinelibrary.wiley.com/action/rightsLink?doi=10.1029%2F2008SW000437" target="_blank">Request permissions</a></li></ul>
</div>
</div>
</div>
<div class="issue-items-container bulkDownloadWrapper">
<h3 id="heading-level-1-2" class="toc__heading section__header to-section" title="News">News</h3>
<div class="issue-item">
<div class="pull-left"><div class="doi-access-wrapper"><div class="free-access access-type"><i aria-hidden="true" class="icon-icon-lock_open"></i><div class="doi-access">Free Access</div></div></div></div>
<div class="clearfix"></div>
<div class="clearfix"></div>
<div class="bulkDownloadInput"><input title="Select article for bulk download or export: New Project Will Measure Electric Currents in Near-Earth Space" type="hidden" value="10.1029%2F2008SW000435" data-has-pdf="true"><div class="hidden">free</div>
</div><a class="issue-item__title visitable" href="/doi/10.1029/2008SW000435">
<h2>New Project Will Measure Electric Currents in Near-Earth Space</h2></a><div style="float: none; position: static; list-style:none;" class="loa comma loa-authors-trunc">
<div class="comma__list"><span class="comma__item"><a href="/action/doSearch?ContribAuthorRaw=Kumar%2C+Mohi" title="Mohi Kumar"><span class="author-style">
 Mohi Kumar</span></a><span class="comma-separator">,&nbsp;</span></span></div>
</div>
<ul style="float: none; list-style: none;" class="rlist--inline separator issue-item__details">
<li class="ePubDate" style="display: inline-block;margin-left: 0;"><span>First Published:</span><span>04 September 2008</span></li>
</ul>
<div class="content-item-format-links">
<ul class="rlist--inline separator issue-item__links">
<li><a title="
            Full text
        " href="/doi/full/10.1029/2008SW000435">
                    Full text
</a></li><li class="PdfLink"><a title="EPDF" href="https://agupubs.onlinelibrary.wiley.com/doi/epdf/10.1029/2008SW000435">PDF</a></li><li><a href="https://agupubs.onlinelibrary.wiley.com/action/rightsLink?doi=10.1029%2F2008SW000435" target="_blank">Request permissions</a></li></ul>
</div>
</div>
<div class="issue-item">
<div class="pull-left"><div class="doi-access-wrapper"><div class="free-access access-type"><i aria-hidden="true" class="icon-icon-lock_open"></i><div class="doi-access">Free Access</div></div></div></div>
<div class="clearfix"></div>
<div class="clearfix"></div>
<div class="bulkDownloadInput"><input title="Select article for bulk download or export: Sunspot Record Reveals Little to Space Weather Watchers" type="hidden" value="10.1029%2F2008SW000440" data-has-pdf="true"><div class="hidden">free</div>
</div><a class="issue-item__title visitable" href="/doi/10.1029/2008SW000440">
<h2>Sunspot Record Reveals Little to Space Weather Watchers</h2></a><div style="float: none; position: static; list-style:none;" class="loa comma loa-authors-trunc">
<div class="comma__list"><span class="comma__item"><a href="/action/doSearch?ContribAuthorRaw=Klotz%2C+Irene" title="Irene Klotz"><span class="author-style">
 Irene Klotz</span></a><span class="comma-separator">,&nbsp;</span></span></div>
</div>
<ul style="float: none; list-style: none;" class="rlist--inline separator issue-item__details">
<li class="ePubDate" style="display: inline-block;margin-left: 0;"><span>First Published:</span><span>20 September 2008</span></li>
</ul>
<div class="content-item-format-links">
<ul class="rlist--inline separator issue-item__links">
<li><a title="
            Full text
        " href="/doi/full/10.1029/2008SW000440">
                    Full text
</a></li><li class="PdfLink"><a title="EPDF" href="https://agupubs.onlinelibrary.wiley.com/doi/epdf/10.1029/2008SW000440">PDF</a></li><li><a href="https://agupubs.onlinelibrary.wiley.com/action/rightsLink?doi=10.1029%2F2008SW000440" target="_blank">Request permissions</a></li></ul>
</div>
</div>
</div>
<div class="issue-items-container bulkDownloadWrapper">
<h3 id="heading-level-1-3" class="toc__heading section__header to-section" title="Opinion">Opinion</h3>
<div class="issue-item">
<div class="pull-left"><div class="doi-access-wrapper"><div class="free-access access-type"><i aria-hidden="true" class="icon-icon-lock_open"></i><div class="doi-access">Free Access</div></div></div></div>
<div class="clearfix"></div>
<div class="clearfix"></div>
<div class="bulkDownloadInput"><input title="Select article for bulk download or export: Space Weather as a Hybrid of Basic Research and Applied Science" type="hidden" value="10.1029%2F2008SW000423" data-has-pdf="true"><div class="hidden">free</div>
</div><a class="issue-item__title visitable" href="/doi/10.1029/2008SW000423">
<h2>Space Weather as a Hybrid of Basic Research and Applied Science</h2></a><div style="float: none; position: static; list-style:none;" class="loa comma loa-authors-trunc">
<div class="comma__list"><span class="comma__item"><a href="/action/doSearch?ContribAuthorRaw=Behnke%2C+Richard+A" title="Richard A. Behnke"><span class="author-style">
 Richard A. Behnke</span></a><span class="comma-separator">,&nbsp;</span></span></div>
</div>
<ul style="float: none; list-style: none;" class="rlist--inline separator issue-item__details">
<li class="ePubDate" style="display: inline-block;margin-left: 0;"><span>First Published:</span><span>25 September 2008</span></li>
</ul>
<div class="content-item-format-links">
<ul class="rlist--inline separator issue-item__links">
<li><a title="
            Full text
        " href="/doi/full/10.1029/2008SW000423">
                    Full text
</a></li><li class="PdfLink"><a title="EPDF" href="https://agupubs.onlinelibrary.wiley.com/doi/epdf/10.1029/2008SW000423">PDF</a></li><li><a href="https://agupubs.onlinelibrary.wiley.com/action/rightsLink?doi=10.1029%2F2008SW000423" target="_blank">Request permissions</a></li></ul>
</div>
</div>
</div>
<div class="issue-items-container bulkDownloadWrapper">
<h3 id="heading-level-1-4" class="toc__heading section__header to-section" title="Technical Article">Technical Article</h3>
<div class="issue-item">
<div class="pull-left"><div class="doi-access-wrapper"><div class="free-access access-type"><i aria-hidden="true" class="icon-icon-lock_open"></i><div class="doi-access">Free Access</div></div></div></div>
<div class="clearfix"></div>
<div class="clearfix"></div>
<div class="bulkDownloadInput"><input title="Select article for bulk download or export: Attenuation of GPS scintillation in Brazil due to magnetic storms" type="hidden" value="10.1029%2F2006SW000285" data-has-pdf="true"><div class="hidden">free</div>
</div><a class="issue-item__title visitable" href="/doi/10.1029/2006SW000285">
<h2>Attenuation of GPS scintillation in Brazil due to magnetic storms</h2></a><div style="float: none; position: static; list-style:none;" class="loa comma loa-authors-trunc">
<div class="comma__list"><span class="comma__item"><a href="/action/doSearch?ContribAuthorRaw=Bonelli%2C+E" title="E. Bonelli"><span class="author-style">
 E. Bonelli</span></a><span class="comma-separator">,&nbsp;</span></span></div>
</div>
<ul style="float: none; list-style: none;" class="rlist--inline separator issue-item__details">
<li class="ePubDate" style="display: inline-block;margin-left: 0;"><span>First Published:</span><span>04 September 2008</span></li>
</ul>
<div class="content-item-format-links">
<ul class="rlist--inline separator issue-item__links">
<li><a title="Abstract" href="/doi/abs/10.1029/2006SW000285">Abstract</a></li>
<li><a title="
            Full text
        " href="/doi/full/10.1029/2006SW000285">
                    Full text
</a></li><li class="PdfLink"><a title="EPDF" href="https://agupubs.onlinelibrary.wiley.com/doi/epdf/10.1029/2006SW000285">PDF</a></li>
<li><a title="References" href="/doi/full/10.1029/2006SW000285#reference">References</a></li><li><a href="https://agupubs.onlinelibrary.wiley.com/action/rightsLink?doi=10.1029%2F2006SW000285" target="_blank">Request permissions</a></li></ul>
</div>
</div>
<div class="issue-item">
<div class="pull-left"><div class="doi-access-wrapper"><div class="free-access access-type"><i aria-hidden="true" class="icon-icon-lock_open"></i><div class="doi-access">Free Access</div></div></div></div>
<div class="clearfix"></div>
<div class="clearfix"></div>
<div class="bulkDownloadInput"><input title="Select article for bulk download or export: Man-made space weather" type="hidden" value="10.1029%2F2008SW000406" data-has-pdf="true"><div class="hidden">free</div>
</div><a class="issue-item__title visitable" href="/doi/10.1029/2008SW000406">
<h2>Man-made space weather</h2></a><div style="float: none; position: static; list-style:none;" class="loa comma loa-authors-trunc">
<div class="comma__list"><span class="comma__item"><a href="/action/doSearch?ContribAuthorRaw=Mendillo%2C+Michael" title="Michael Mendillo"><span class="author-style">
 Michael Mendillo</span></a><span class="comma-separator">,&nbsp;</span></span><span class="comma__item"><a href="/action/doSearch?ContribAuthorRaw=Smith%2C+Steven" title="Steven Smith"><span class="author-style">
 Steven Smith</span></a><span class="comma-separator">,&nbsp;</span></span><span class="comma__item"><a href="/action/doSearch?ContribAuthorRaw=Coster%2C+Anthea" title="Anthea Coster"><span class="author-style">
 Anthea Coster</span></a><span class="comma-separator">,&nbsp;</span></span><span class="comma__item"><a href="/action/doSearch?ContribAuthorRaw=Erickson%2C+Philip" title="Philip Erickson"><span class="author-style">
 Philip Erickson</span></a><span class="comma-separator">,&nbsp;</span></span><span class="comma__item"><a href="/action/doSearch?ContribAuthorRaw=Baumgardner%2C+Jeffrey" title="Jeffrey Baumgardner"><span class="author-style">
 Jeffrey Baumgardner</span></a><span class="comma-separator">,&nbsp;</span></span><span class="comma__item"><a href="/action/doSearch?ContribAuthorRaw=Martinis%2C+Carlos" title="Carlos Martinis"><span class="author-style">
 Carlos Martinis</span></a><span class="comma-separator">,&nbsp;</span></span></div>
</div>
<ul style="float: none; list-style: none;" class="rlist--inline separator issue-item__details">
<li class="ePubDate" style="display: inline-block;margin-left: 0;"><span>First Published:</span><span>11 September 2008</span></li>
</ul>
<div class="content-item-format-links">
<ul class="rlist--inline separator issue-item__links">
<li><a title="Abstract" href="/doi/abs/10.1029/2008SW000406">Abstract</a></li>
<li><a title="
            Full text
        " href="/doi/full/10.1029/2008SW000406">
                    Full text
</a></li><li class="PdfLink"><a title="EPDF" href="https://agupubs.onlinelibrary.wiley.com/doi/epdf/10.1029/2008SW000406">PDF</a></li>
<li><a title="References" href="/doi/full/10.1029/2008SW000406#reference">References</a></li><li><a href="https://agupubs.onlinelibrary.wiley.com/action/rightsLink?doi=10.1029%2F2008SW000406" target="_blank">Request permissions</a></li></ul>
</div>
</div>
<div class="issue-item">
<div class="pull-left"><div class="doi-access-wrapper"><div class="free-access access-type"><i aria-hidden="true" class="icon-icon-lock_open"></i><div class="doi-access">Free Access</div></div></div></div>
<div class="clearfix"></div>
<div class="clearfix"></div>
<div class="bulkDownloadInput"><input title="Select article for bulk download or export: Importance of predicting the dose temporal profile for large solar energetic particle events" type="hidden" value="10.1029%2F2008SW000393" data-has-pdf="true"><div class="hidden">free</div>
</div><a class="issue-item__title visitable" href="/doi/10.1029/2008SW000393">
<h2>Importance of predicting the dose temporal profile for large solar energetic particle events</h2></a><div style="float: none; position: static; list-style:none;" class="loa comma loa-authors-trunc">
<div class="comma__list"><span class="comma__item"><a href="/action/doSearch?ContribAuthorRaw=Neal%2C+John+S" title="John S. Neal"><span class="author-style">
 John S. Neal</span></a><span class="comma-separator">,&nbsp;</span></span><span class="comma__item"><a href="/action/doSearch?ContribAuthorRaw=Nichols%2C+Theodore+F" title="Theodore F. Nichols"><span class="author-style">
 Theodore F. Nichols</span></a><span class="comma-separator">,&nbsp;</span></span><span class="comma__item"><a href="/action/doSearch?ContribAuthorRaw=Townsend%2C+Lawrence+W" title="Lawrence W. Townsend"><span class="author-style">
 Lawrence W. Townsend</span></a><span class="comma-separator">,&nbsp;</span></span></div>
</div>
<ul style="float: none; list-style: none;" class="rlist--inline separator issue-item__details">
<li class="ePubDate" style="display: inline-block;margin-left: 0;"><span>First Published:</span><span>18 September 2008</span></li>
</ul>
<div class="content-item-format-links">
<ul class="rlist--inline separator issue-item__links">
<li><a title="Abstract" href="/doi/abs/10.1029/2008SW000393">Abstract</a></li>
<li><a title="
            Full text
        " href="/doi/full/10.1029/2008SW000393">
                    Full text
</a></li><li class="PdfLink"><a title="EPDF" href="https://agupubs.onlinelibrary.wiley.com/doi/epdf/10.1029/2008SW000393">PDF</a></li>
<li><a title="References" href="/doi/full/10.1029/2008SW000393#reference">References</a></li><li><a href="https://agupubs.onlinelibrary.wiley.com/action/rightsLink?doi=10.1029%2F2008SW000393" target="_blank">Request permissions</a></li></ul>
</div>
</div>
<div class="issue-item">
<div class="pull-left"><div class="doi-access-wrapper"><div class="free-access access-type"><i aria-hidden="true" class="icon-icon-lock_open"></i><div class="doi-access">Free Access</div></div></div></div>
<div class="clearfix"></div>
<div class="clearfix"></div>
<div class="bulkDownloadInput"><input title="Select article for bulk download or export: Validation of the Coupled Thermosphere Ionosphere Plasmasphere Electrodynamics model: CTIPE-Mass Spectrometer Incoherent Scatter temperature comparison" type="hidden" value="10.1029%2F2007SW000364" data-has-pdf="true"><div class="hidden">free</div>
</div><a class="issue-item__title visitable" href="/doi/10.1029/2007SW000364">
<h2>Validation of the Coupled Thermosphere Ionosphere Plasmasphere Electrodynamics model: CTIPE-Mass Spectrometer Incoherent Scatter temperature comparison</h2></a><div style="float: none; position: static; list-style:none;" class="loa comma loa-authors-trunc">
<div class="comma__list"><span class="comma__item"><a href="/action/doSearch?ContribAuthorRaw=Codrescu%2C+M+V" title="M. V. Codrescu"><span class="author-style">
 M. V. Codrescu</span></a><span class="comma-separator">,&nbsp;</span></span><span class="comma__item"><a href="/action/doSearch?ContribAuthorRaw=Fuller-Rowell%2C+T+J" title="T. J. Fuller-Rowell"><span class="author-style">
 T. J. Fuller-Rowell</span></a><span class="comma-separator">,&nbsp;</span></span><span class="comma__item"><a href="/action/doSearch?ContribAuthorRaw=Munteanu%2C+Vlad" title="Vlad Munteanu"><span class="author-style">
 Vlad Munteanu</span></a><span class="comma-separator">,&nbsp;</span></span><span class="comma__item"><a href="/action/doSearch?ContribAuthorRaw=Minter%2C+C+F" title="C. F. Minter"><span class="author-style">
 C. F. Minter</span></a><span class="comma-separator">,&nbsp;</span></span><span class="comma__item"><a href="/action/doSearch?ContribAuthorRaw=Millward%2C+G+H" title="G. H. Millward"><span class="author-style">
 G. H. Millward</span></a><span class="comma-separator">,&nbsp;</span></span></div>
</div>
<ul style="float: none; list-style: none;" class="rlist--inline separator issue-item__details">
<li class="ePubDate" style="display: inline-block;margin-left: 0;"><span>First Published:</span><span>25 September 2008</span></li>
</ul>
<div class="content-item-format-links">
<ul class="rlist--inline separator issue-item__links">
<li><a title="Abstract" href="/doi/abs/10.1029/2007SW000364">Abstract</a></li>
<li><a title="
            Full text
        " href="/doi/full/10.1029/2007SW000364">
                    Full text
</a></li><li class="PdfLink"><a title="EPDF" href="https://agupubs.onlinelibrary.wiley.com/doi/epdf/10.1029/2007SW000364">PDF</a></li>
<li><a title="References" href="/doi/full/10.1029/2007SW000364#reference">References</a></li><li><a href="https://agupubs.onlinelibrary.wiley.com/action/rightsLink?doi=10.1029%2F2007SW000364" target="_blank">Request permissions</a></li></ul>
</div>
</div>
</div>

---

<p><img height="99px" src="https://foruda.gitee.com/images/1676366779235702094/24343720_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1676366781349228340/e81adc7f_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1676366785780017524/21c23641_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1676366791617110042/1d18926b_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1676366795868537325/aae7654a_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1676366800485772781/d1d95c82_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1676366804303601721/0a136ac8_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1676366812353766599/1df2e4d0_5631341.png"></p>

1. [Space weather: Storms from the Sun | National Oceanic and Atmospheric](http://www.noaa.gov/explainers/space-weather-storms-from-sun) ...
NOAANOAA|2550 × 1650 jpeg|Image may be subject to copyright.

   ![输入图片说明](https://foruda.gitee.com/images/1676368508172671391/d31aceed_5631341.jpeg "INFOGRAPHIC-space-weather-NOAA-061716-2550x1650-original.jpg")

1. [NASA - Getting Ready for Solar Max: Separating Fact from Fiction on](https://www.nasa.gov/mission_pages/sunearth/multimedia/agu-fall2011-media_prt.htm) ...
NASANASA|1600 × 2160 jpeg|Image may be subject to copyright.

   ![输入图片说明](https://foruda.gitee.com/images/1676367329049884133/ce1a04be_5631341.jpeg "525202main_FAQ19.jpg")

1. [Space weather forecast: Big storms ahead | Science News for Students](https://www.sciencenewsforstudents.org/article/space-weather-forecast-big-storms-ahead)
sciencenewsforstudents.orgsciencenewsforstudents.org|1214 × 1200 png|Image may be subject to copyright.

   ![输入图片说明](https://foruda.gitee.com/images/1676367346904184230/0080aa72_5631341.png "FULL_impact_of_space_weather.png")

1. [ESA - Space weather effects](http://www.esa.int/ESA_Multimedia/Images/2018/01/Space_weather_effects)
ESAESA|5032 × 3579 jpeg|Image may be subject to copyright.

   ![输入图片说明](https://foruda.gitee.com/images/1676367790601352089/a9025739_5631341.jpeg "Space_weather_effects.jpg")

1. [Space Weather | Science Mission Directorate](https://science.nasa.gov/heliophysics/focus-areas/space-weather)
NASANASA|1421 × 2000 jpeg|Image may be subject to copyright.

   ![输入图片说明](https://foruda.gitee.com/images/1676367453818102243/6a1d5989_5631341.jpeg "LWS-Space-Weather-Web-Image_0.jpg")

1. [An introduction to Space Weather](http://www.land-of-kain.de/docs/spaceweather/)
land-of-kain.de|1536 × 1201 jpeg|Image may be subject to copyright.

   ![输入图片说明](https://foruda.gitee.com/images/1676367465174839489/6c4a5d52_5631341.jpeg "nasa_spaceweather.jpg")

1. [ESA - Space for Kids - Space Weather Week!](https://www.esa.int/kids/en/learn/Our_Universe/The_Sun/Space_Weather_Week)
ESAESA|3579 × 5032 jpeg|Image may be subject to copyright.

   ![输入图片说明](https://foruda.gitee.com/images/1676368473737704363/b444cebe_5631341.jpeg "Space_weather_effects.jpg")

1. [Space in Images - 2013 - 01 - Affected by space weather](http://www.esa.int/spaceinimages/Images/2013/01/Affected_by_space_weather)
ESAESA|2635 × 3504 jpeg|Image may be subject to copyright.

   ![输入图片说明](https://foruda.gitee.com/images/1676368417778098113/dbb34467_5631341.jpeg "Affected_by_space_weather.jpg")

---

[![输入图片说明](https://foruda.gitee.com/images/1676368991832119201/6c715af7_5631341.png "屏幕截图")](https://agupubs.onlinelibrary.wiley.com/doi/pdf/10.1002/swe.20034)

https://agupubs.onlinelibrary.wiley.com/doi/pdf/10.1002/swe.20034

<p><img width="15.69%" src="https://foruda.gitee.com/images/1676370127414730665/eb627ed2_5631341.jpeg" title="Space Weather - 2013 - Looper - The radiation environment near the lunar surface  CRaTER observations and Geant4-1.jpg"> <img width="15.69%" src="https://foruda.gitee.com/images/1676370139526383023/0a457b23_5631341.jpeg" title="Space Weather - 2013 - Looper - The radiation environment near the lunar surface  CRaTER observations and Geant4-2.jpg"> <img width="15.69%" src="https://foruda.gitee.com/images/1676370156412613326/7e9a9f41_5631341.jpeg" title="Space Weather - 2013 - Looper - The radiation environment near the lunar surface  CRaTER observations and Geant4-3.jpg"> <img width="15.69%" src="https://foruda.gitee.com/images/1676370170981093068/7d1fb67d_5631341.jpeg" title="Space Weather - 2013 - Looper - The radiation environment near the lunar surface  CRaTER observations and Geant4-4.jpg"> <img width="15.69%" src="https://foruda.gitee.com/images/1676370188463895367/cf9b4270_5631341.jpeg" title="Space Weather - 2013 - Looper - The radiation environment near the lunar surface  CRaTER observations and Geant4-5.jpg"> <img width="15.69%" src="https://foruda.gitee.com/images/1676370207513172970/f3d245dd_5631341.jpeg" title="Space Weather - 2013 - Looper - The radiation environment near the lunar surface  CRaTER observations and Geant4-6.jpg"> <img width="15.69%" src="https://foruda.gitee.com/images/1676370223942814310/a805a139_5631341.jpeg" title="Space Weather - 2013 - Looper - The radiation environment near the lunar surface  CRaTER observations and Geant4-7.jpg"> <img width="15.69%" src="https://foruda.gitee.com/images/1676370248729326570/32433bcd_5631341.jpeg" title="Space Weather - 2013 - Looper - The radiation environment near the lunar surface  CRaTER observations and Geant4-8.jpg"> <img width="15.69%" src="https://foruda.gitee.com/images/1676370264790216742/387d172f_5631341.jpeg" title="Space Weather - 2013 - Looper - The radiation environment near the lunar surface  CRaTER observations and Geant4-9.jpg"> <img width="15.69%" src="https://foruda.gitee.com/images/1676370277499292244/649d60c3_5631341.jpeg" title="Space Weather - 2013 - Looper - The radiation environment near the lunar surface  CRaTER observations and Geant4-10.jpg"> <img width="15.69%" src="https://foruda.gitee.com/images/1676370290702707518/18e2a26f_5631341.jpeg" title="Space Weather - 2013 - Looper - The radiation environment near the lunar surface  CRaTER observations and Geant4-11.jpg"></p>

--- 

SPACE WEATHER, VOL. 6, S09003, doi:10.1029/2008SW000437, 2008

# Space Weather for Kids

- Mohi Kumar
- Published 11 September 2008.

Citation: Kumar, M. (2008), Space Weather for Kids, Space Weather, 6, S09003, doi:10.1029/2008SW000437.

This school year, students in Cheryl Williams’s class of fourth through sixth graders may be able to look into the night sky and observe something that few American children have seen: the brightly lit, swirling colors of aurorae.

“Some of my students can look from their porches at night and see the colors dance,” said Williams, who teaches at the Youth Education and Support Service (YESS) elementary school in Fairbanks, Alaska. “They come in the next morning, and they ask questions. Knowing that, I’ve planned activities to try and get them answers while still meeting our educational standards.”

Farther south, Tom Traeger also wrestles with how to engage students while meeting grade requirements. Traeger teaches Earth science and geology at La Cañada High School in La Cañada Flintridge, Calif. “Education standards in the state of California require that high school students know about nuclear fusion within the Sun, about the magnetic field of planets,” Traeger explained. “I use those standards to weave in topics related to space weather.”

Williams and Traeger are two of relatively few teachers in the United States who introduce concepts related to space weather into their classrooms, discussing with their students not just how solar storms are created but also general concepts about how such storms can disrupt satellite streams, zap spacecraft circuitry, and damage power grids.

Williams’s students visit NASA Web sites and download photographs to keep sunspot journals. With their art teacher, they use pastels to create their own pictures of auroral streamers. “Our school focuses on special needs kids. Some are bipolar, some have Attention Deficit Disorder, many are foster kids,” Williams explained. “I have to inexpensively engage them with activities that grab their attention.”

Traeger’s students use their classroom’s magnetometer and solar telescope to delve into space weather subjects. “We discuss the 11-year sunspot cycle, how moving electrons induce a magnetic field and how magnetic fields are generated from the Earth’s core,” he said. “And of course, we surf the Web a lot.”

Williams’s and Traeger’s experiences highlight two important challenges to educating elementary, junior, and high school students about space weather. First, because there are no national mandatory education standards in the United States, curricula are up to the discretion of states, school districts, and individual teachers. Second, to ensure that most teachers can benefit from an activity, the space weather lesson plans must be easily understood, actively engaging, and inexpensive to implement.

Over the years, several innovative approaches have developed to meet these challenges. These programs share a common thread: They link space weather science to standards in an interactive way.

### Getting Creative With Curricula

Although no mandatory national standards exist, in 1996 the U.S. National Academies published National Science Education Standards (NSES), meant to serve as guidelines for states and school districts as they develop their science education requirements. These standards suggest that students be taught that the Sun is a star and that the Sun is the source of the light and heat that warm the Earth and produce our climate.

Because space weather is not explicitly addressed in NSES, curriculum developers have to get creative. “The light piece gives teachers a window into space weather, to X rays and science missions associated with solar flares,” said Roberta Johnson, executive director of the National Earth Science Teachers Association (NESTA) and a space weather scientist at the U.S. National Center for Atmospheric Research’s High Altitude Observatory. But perhaps a more definitive link comes through electricity and magnetism within the physical science standards, she explained: “Magnets, magnetism, the planets and Sun as magnetic bodies—focusing on magnetism, primarily phenomenologically, is very effective because kids love it. It’s invisible forces—it’s like magic.”

Johnson coordinates an online portal for information about the Earth and space sciences. Called “Windows to the Universe,” the portal contains primers and video clips on the Earth and space sciences, with a focus on science in action. Created in 1995, the Web site (http://www.windows.ucar.edu) contains more than 8000 pages written at three levels of content (upper elementary, middle, and high school), with specific notes to teachers about how information relates to NSES standards.

Space weather content on Windows to the Universe is funded by the Center for Integrated Space Weather Modeling (CISM), a U.S. National Science Foundation Science and Technology Center at Boston University. Through this content, students and teachers can read up on the ionosphere and aurorae, search for magnetic north with an interactive compass, solve space weather–themed word searches, and construct flip books to show the evolution of a solar flare. “Our pages related to space weather research and spacecraft get about 2 to 3 million hits every year,” Johnson said.

Paralleling this example, several other Web sites seek to match teachers to NSES standards through interactive lesson plans about space weather. For example, on the Web site of the Space Weather Prediction Center, a branch of the U.S. National Oceanic and Atmospheric Administration (NOAA), teachers can download lesson plans referenced to standards that instruct students on how to build a spectroscope, measure the solar constant, and even simulate the effect of the solar wind on the geomagnetic field (see http://www.swpc.noaa.gov/Education/index.html).

Stanford University’s Solar Online Activity Resources (SOLAR) Center provides NSES-referenced classroom projects that show students how to measure the angular velocity and rotational period of the Sun, design a solar cooker, and detect the Sun’s effect on communications systems using AM radio reception (see http://solar-center.stanford.edu/). Students can also listen to helioseismological hums, read up on solar folklore and ancient observatories, and take Web-based quizzes about the Sun. “We want to show that the Sun, besides making our lives possible, has provided inspiration for arts and culture,” said John Beck, SOLAR Center’s education and public outreach scientist.

With such resources, teachers are encouraged make ties to other important standards set forth in NSES. “Space weather is well suited to educational connections dealing with the impacts of natural events on society,” explained Johnson. “There are also standards relating to technology, the history of science, science as a process. Hooks are there—we just have to use them.”

### Kids as Real-Time Observers

A question that plagues curricula developers who focus on space weather is how to give form to intangible concepts, such as the radiation belts and the solar wind. A few years ago, education specialists at NASA’s Sun-Earth Connection Education Forum (SECEF) realized that the answer lay inside computers, within the vast streams of data that NASA receives from its satellites and spacecraft.

“The imagery from NASA missions is simply phenomenal,” said Troy Cline, an educational technology expert at SECEF. “Combine that with newly available technologies and the speed of communication, and we’ve got a way to put information instantly at our fingertips.”

Cline and his colleague Elaine Lewis, a science curriculum coordinator at SECEF, work on developing content for NASA’s Space Weather Action Center (SWAC; http://SunEarthday.nasa.gov/swac/). Launched in 2006, the program allows students to easily track solar storms online from their inception to their impacts at Earth (see Figure 1).

![输入图片说明](https://foruda.gitee.com/images/1676400028135832965/27de2a4a_5631341.png "在这里输入图片标题")

> Figure 1. An example of a Space Weather Action Center that a teacher might set up in his or her classroom.

“When students go to our Web site, they see a huge resource, a tool with about 36 different data sets from various satellites and ground-based observatories,” explained Lewis. “Through this and our simple setup guide, students can use remote sensing to collect results in near real time, with a lag of only about 2 hours.”

The program works through a series of simple steps. First, students monitor for sunspots to see whether they are growing or changing. They then see if sunspot activity is generating storm signals. “Students can actually listen to radio waves from the Sun to hear if that sunspot is really active. So if they hear a storm, they can start monitoring the Earth’s magnetic field for aurorae,” Cline said.

After answering a few simple questions every day or every week, the students are ready for the last step of the program: developing their own space weather report. “Using really simple and inexpensive software, a webcam, and a piece of cloth to make a bluescreen, teachers can quickly set up a little TV studio,” said Cline. “Soon kids can start generating CNN-style news reports on space weather.” The opportunity for students to act like they are on television is what captivates children the most. “I’ve seen terribly shy kids lining up in droves to try this when we visit schools for demonstrations,” Lewis explained.

SWAC is part of a broader effort by the SECEF to get students immersed in space weather. Their main focus is SunEarth Day, an event that happens each year on or near the vernal equinox. Sun- Earth Day started in 2000 as a 1-day event—webcasts, broadcasts, scientific interviews—focused on the theme “Have a Solar Blast!”

Other themes throughout the years have included 2003’s “Live From the Aurora,” where students in the San Francisco, Baltimore, and Anchorage areas packed into their local science centers overnight to watch live video streams of aurorae in action. In 2005, the theme “Ancient Observatories, Timeless Knowledge” featured video and webcast programming of solar alignments with structures that mark equinoxes and solstices. Sites visited included Chaco Canyon (New Mexico) and Chichen Itza (Mexico).

“The first year we had more than 80,000 people downloading and viewing. Now we’re into the millions,” explained Cline. “People started using our materials whenever they 
could, even during summer. So now the program is yearround.”

SECEF has just wrapped up Sun-Earth Day 2008, “Space Weather Around the World,” which included live coverage of the solar eclipse seen over China in the days preceding the Olympic games. In support of the upcoming International Year of Astronomy, the Sun-Earth Day theme for 2009 is “Our Sun, Yours to Discover.”

“We’re going to focus on missions such as Ulysses and Voyager, as well as on space weather on other planets,” explained Cline. “The goal is not only to highlight important concepts in solar physics but also to suggest that there are many stars like our Sun in the universe.”

The power of SECEF activities is that students are viewing real data, in near real time. “Students know they are seeing the same data that scientists look at,” said Lewis. “They won’t ever feel like teachers are creating something at a lower level just for them to understand,” Cline added.

Moreover, students can interact with the data in ways that appeal to several different learning styles. “If students can read it, write about it, touch it, listen to it, be involved with it, and express themselves through it, then a teacher is making content approachable to all the kids in the classroom,” Lewis said. “That’s our goal.”

![输入图片说明](https://foruda.gitee.com/images/1676431228880731126/64f1d58d_5631341.png "comicbooks.png")

### Space Weather Comic Books

To help make basic concepts of space weather come alive for children, some groups have introduced space weather-themed comic books. Featuring principle characters who are female -- to help draw girls into science -- the comic books take readers on a journey through the solar-terrestrial environment.

 **Mol and Her Robotic Dog Mirubo** 

Through a series of nine comic books, Mol and Mirubo explore their curiosity about space weather and the Sun-Earth relationship. Developed by Nagoya University's Solar-Terrestrial Environment Laboratory, these comic books, originally published in Japanese are now being translated into about 20 languages.

 **Cindi and Her Space Dogs**

In "Cindi in Space," an android girl and her two space dogs vault through the near-Earth space environment to capture and count ions and neutral particles. Developed by scientists at the University of Texas at Dallas, Cindi represents an instrument on board a recently launched space weather monitoring satellite.

 **Zillian and the Alien** 

> In NOAA's comic book "Space Weather," scientist Zillian and an alien stowaway travel from the Earth to the Sun to learn why weather in space has zapped Zillian's television.

### Comic Books

Not all efforts to introduce students to space weather involve high-tech, hands-on activities. Instead, some groups are turning to comic books to communicate information to children (see “Space Weather Comic Books” sidebar).

In 2001, NOAA released a comic book called “Space Weather,” illustrated by Zander Cannon (http://www.bigtimeattic.com). Several years later, borrowing from the strong Japanese tradition of manga, Yohsuke Kamide, then at Nagoya University’s Solar-Terrestrial Environment Laboratory (STEL), began producing comic books about the Sun-Earth relationship.

In these books, the Japanese girl Mol and her robotic dog Mirubo explore their curiosity about space weather. Illustrated by Hayanon, a leading science cartoonist in Japan, and originally published in Japanese, these nine manga have been intensely promoted by the international Scientific Committee on Solar-Terrestrial Physics (SCOSTEP). “So far, these comic books have recorded more than 2 million hits per year at STEL and SCOSTEP,” said Kamide, now at Kyoto University but still involved with STEL outreach activities.

Inspired by NOAA’s comic book, scientists at the Center for Space Sciences at the University of Texas at Dallas developed a comic book in 2005 called “Cindi in Space” to highlight the Coupled Ion Neutral Dynamics Investigation (CINDI), a joint NASA/ U.S. Air Force–funded project. Built by the Center for Space Sciences, the CINDI instrument consists of two sensors, one for ions and one for neutral particles, designed to analyze their composition and dynamics in the ionosphere. CINDI is operating on the Air Force’s Communication/Navigation Outage Forecast Satellite (C/NOFS), which launched in April 2008.

The book, illustrated by Erik Levold, features an android girl named Cindi who searches space for space dogs: dogs with positively charged bodies and negatively charged tails. A dog with its tail is average, neutral, sedate. But if the dog eats an energy biscuit, his tail flies off, and the dog becomes positive and upbeat. This creates a quantity of dog tails floating around in space, searching for new owners. If a tail finds a new owner, the dog becomes neutral again and expels his energy biscuit. Cindi has special nets to count neutral and positive dogs, analogous to the two distinct CINDI instruments.

Why do comic books have so much appeal? “Comic books are easy for a general audience and kids to approach space weather and science in general,” Kamide said. “We use a girl, Mol, to try to encourage females to focus on science.” Kamide explained that Mol loves science and isn’t afraid to ask her teacher questions. “In this way, readers feel that science is everywhere in daily life.”

### Keeping It Local

While many efforts to engage students in learning about space weather focus on ties to national standards, some groups instead focus their program more locally.

For example, Mary Urquhart, an associate professor of science and mathematics education at the University of Texas at Dallas, prepares educational materials for CINDI’s outreach pages (http://cindispace.utdallas.edu/education/index.html), which contain education materials specifically tailored for Texas state standards. But beyond simply supplying information on a Web site, Urquhart has integrated Web site materials into her university’s Regional Collaborative for Excellence in Science Teaching, a professional development program for teachers. “Each teacher is supposed to go out and train additional teachers after attending our workshops,” Urquhart said.

This is very different from just downloading something off the Web. “We found that actually trying something in a workshop makes a big difference as to whether or not it is actually used in the classroom or after-school science program,” Urquhart said.

### A Focus on What Students Will Understand

Back in Alaska, Williams continues to prepare her lesson plans. “Ultimately, I have to pick activities that students will understand,” she said. “If I navigate to a Web site and I see that its content is too complex, I know that it will frustrate kids too much. So I’ll either modify or simplify the content, or I’ll skip it.”

For Traeger, in California, one of the hardest parts of his job is that several of his students are new to physics. “They don’t understand magnetic induction, so it’s hard to grasp how magnetic fields are made within the Earth,” he said. “Getting them to grasp what is meant by the solar wind is also difficult—they think that it is like the wind on Earth.”

Bringing abstract space weather concepts down to a level that kids can understand is, according to Johnson, the basic challenge for teaching kids about space weather. Easily understood Web resources (see “Educational Materials” sidebar) are critical. Nonetheless, “Many scientists have a hard time discerning the boundary between all the knowledge they have and what an eighth grader can understand,” Johnson said. “That is why people who focus on education in the geosciences, who are well grounded but have a love, passion, and talent for education, are so desperately important.”

 **Mohi Kumar** _is a staff writer for the American Geophysical Union._

### Educational Materials

 _In addition to the Web sites listed in this article, several more can also help teachers introduce space weather to their students._ 

- National Science Education Standards (U.S.)  
  http://www.nap.edu/openbook.php?record_id=4962

- Solar and Heliospheric Observatory  
  http://sohowww.nascorn.nasa.gov/classroom/for_students.html

- Sun|trek  
  http://www.suntrek.org/

- Solar-Terrestrial Environment Laboratory, Nagoya University  
  http://www.stelab.nagoya-u.ac.jp/ste-www1/doce/outreach.html#anc_booklets

- Space Weather Center  
  http://www.spaceweathercenter.org/

- Spaceweather.com  
  http://www.spaceweather.com/

空间气象，VOL.6，S09003，doi:10.1029/2008SW000437，2008

> 版权归美国地球物理联盟所有，2008年

# 儿童儿童空间天气

- 莫希 Kumar
- 发表于 2008年9月11日。

引用 :Kumar，M.(2008)，《儿童空间天气》，空间天气，6 ，S09003 ，doi:10.1029/2008SW 000437。

本学年，谢丽尔·威廉姆斯 (Cheryl Williams) 四年级到六年级的学生们可能可以仰望夜空，观察到一些很少有美国孩子看到过的东西 : 明亮的、漩涡状颜色的极光。

威廉姆斯在阿拉斯加州费尔班克斯的青年教育和支持服务小学任教，他说 : ”我的一些学生晚上可以从门廊上看到颜色跳舞。”“他们第二天早上来到学校，问问题。知道了这一点，我计划了一些活动，试图在满足我们教育标准的同时，让他们回答问题。”

在更远的南方，汤姆崔格 (Tom Traeger) 也在努力解决如何在满足成绩要求的同时吸引学生的问题。崔格在加州拉加拿大弗林特里奇的拉加拿大高中教授地球科学和地质学。“加州的教育标准要求高中生了解太阳内部的核聚变，以及行星的磁场，”崔格解释说。“我用这些标准来编织与太空天气相关的话题。”

威廉姆斯和崔格是美国为数不多的将与太空天气相关的概念引入课堂的教师，他们不仅与学生讨论太阳风暴是如何产生的，而且还讨论了这种风暴如何破坏卫星流、 zap 航天器电路和破坏电网的一般概念。

威廉姆斯的学生访问美国宇航局的网站，下载照片以保存太阳黑子日志。和他们的艺术老师一起，他们用粉彩创作自己的极光飘带照片。“我们学校关注的是有特殊需要的孩子。有些是双相情感障碍，有些是注意力缺陷障碍，许多是寄养儿童，”威廉姆斯解释说。“我必须让他们参与一些吸引他们注意力的活动，而且费用不高。”

崔格的学生使用教室里的磁强计和太阳望远镜来研究太空天气科目。他说 : “我们讨论了 11 年的太阳黑子周期，运动的电子如何诱导磁场，以及磁场是如何从地核产生的。”当然，我们也经常上网冲浪。”

威廉姆斯和崔格的经历凸显了对小学、初中和高中学生进行太空天气教育的两个重要挑战。首先，由于美国没有全国性的义务教育标准，课程由各州、学区和个别教师自行决定。其次，为了确保大多数教师能从一项活动中受益，太空天气课程计划必须易于理解、积极参与、实施成本低廉。

多年来，为了应对这些挑战，已经发展了几种创新方法。这些项目有一个共同的主线 : 它们以互动的方式将空间气象科学与标准联系起来。

### 课程创新

虽然没有强制性的国家标准，但美国国家科学院于1996年发布了《国家科学教育标准》 (national Science Education standards，NSES) ，旨在作为各州和学区制定科学教育要求的指南。这些标准建议教育学生，太阳是一颗恒星，太阳是光和热的来源，使地球变暖，产生我们的气候。

由于 NSES 中没有明确提到空间天气，课程开发人员必须有创造性。[NATIONAL EARTH SCIENCE TEACHERS ASSOCIATION](https://www.nestanet.org/) 执行主任、美国国家大气研究中心高空观测站的空间天气科学家罗伯特·约翰逊 (Roberta Johnson) 说 : ”光的部分为教师提供了一个了解空间天气、 X 射线和与太阳耀斑有关的科学任务的窗口。”但也许更明确的联系来自物理科学标准内的电和磁，她解释说 : “磁体、磁性、行星和太阳作为磁性物体——主要从现象上关注磁性，是非常有效的，因为孩子们喜欢它。这是一种无形的力量，就像魔法一样。”

约翰逊协调一个在线门户获取信息关于地球和空间科学。这个名为“宇宙之窗”的传送门包含关于地球和空间科学的入门教程和视频剪辑，重点是行动中的科学。该网站创建于1995 年(http://www.windows.ucar.edu) 包含超过 8000 页的内容，分为三个层次 (小学、初中和高中) ，并向教师提供了关于信息如何与 NSES 标准相关的具体说明。

宇宙之窗上的空间天气内容是由波士顿大学美国国家科学基金会科学技术中心综合空间天气建模中心 (CISM) 资助的。通过这一内容，学生和教师可以阅读电离层和极光，用交互式指南针搜索磁北，解决空间天气主题的词汇搜索，构建展示太阳耀斑演化的手翻书。约翰逊说 : ”我们与空间天气研究和航天器有关的页面每年大约有 200万到300万点击。”

与此类似，其他几个网站也试图通过关于太空天气的互动教学计划，将教师与 NSES 标准相匹配。例如，在美国国家海洋和大气管理局 (NOAA) 分支机构空间天气预测中心的网站上，教师可以下载参考标准的教课计划，指导学生如何建造分光镜，测量太阳常数，甚至模拟太阳风对地磁场的影响 ( 见 http://www.swpc.noaa) 。政府 / 教育 / index . html) 。

斯坦福大学的太阳能在线活动资源 (SOLAR) 中心提供nses 参考的课堂项目向学生展示如何测量太阳的角速度和旋转周期，设计太阳灶，并使用调 幅无 线电接 收检测 太阳 对通信 系统的 影响 ( 见http://solar-center.stanford.edu/) 。学生还可以听日震学的嗡嗡声，阅读关于太阳的民间传说和古代天文台，并在网上进行关于太阳的测验。 SOLAR 中心的教育和公共推广科学家约翰·贝克说 : ”我们想表明，太阳除了使我们的生活成为可能外，还为艺术和文化提供了灵感。”

有了这些资源，鼓励教师与 NSES 中规定的其他重要标准建立联系。约翰逊解释说 : ”空间天气非常适合处理自然事件对社会的影响的教育联系。”“还有与技术、科学史、科学作为一个过程相关的标准。钩子就在那里，我们只需要使用它们。”

### 孩子们是实时观察者

关注太空天气的课程开发人员面临的一个问题是，如何赋予辐射带和太阳风等无形概念以形式。几年前， NASA 日地连接教育论坛 (SECEF) 的教育专家意识到，答案就在计算机中，在 NASA 从卫星和航天器接收到的大量数据流中。

SECEF 的教育技术专家特洛伊克莱因说 : “ NASA 任务中的图像简直就是 phenom- enal 。”“再加上新出现的技术和通讯速度，我们就有了一种将信息即时放在我们指尖的方法。”

Cline 和他的同事 Elaine 刘易斯，SECEF 的科学课程协调员，致力于为 NASA 空间天气行动中心 (SWAC;http://Su-nEarthday.nasa.gov swac /) 。该项目于 2006 年启动，学生们可以轻松地在线追踪太阳风暴从开始到对地球的影响 (见图1) 。

> 图1 所示。一个老师可能在他或她的教室里设置的空间天气行动中心的例子。

刘易斯解释说 : “当学生们访问我们的网站时，他们看到了一个巨大的资源，一个包含来自各种卫星和地面天文台的大约 36 个不同数据集的工具。”“通过这个和我们简单的设置指南，学生们可以使用遥感几乎实时地收集结果，延迟只有大约 2 小时。”

该程序通过一系列简单的步骤运行。首先，学生们监测太阳黑子，看看它们是否在生长或变化。然后观察太阳黑子活动是否会产生风暴信号。“学生实际上可以通过听来自太阳的无线电波来了解太阳黑子是否真的活跃。因此，如果他们听到风暴，他们可以开始监测地球磁场的极光，”克莱因说。

在每天或每周回答几个简单的问题后，学生们已经为pro- 的最后一步做好了准备格拉姆 : 开发他们自己的太空天气报告。克莱恩说 : “使用简单廉价的软件，一个网络摄像头，一块布做一个蓝屏，老师们可以很快地建立一个小电视演播室。”“很快，孩子们就可以开始制作 cnn 风格的太空天气新闻报道。“学生有机会表现得像在电视上一样，这是最吸引孩子的地方。刘易斯解释说 : “当我们参观学校进行演示时，我看到非常害羞的孩子成群结队地排队尝试这种方法。”

SWAC 是 SECEF 让学生沉浸在太空天气中的更广泛努力的一部分。他们主要关注的是太阳 - 地球日，这是一个每年在春分或春分附近发生的事件。太阳 - 地球日始于 2000 年，最初是一个为期一天的活动，包括网络广播、广播和科学采访，主题是“有一个太阳爆炸 ! ”

这些年来的其他主题包括 2003 年的“极光现场”，旧金山、巴尔的摩和安克雷奇地区的学生们连夜挤在当地的科学中心，观看极光活动的视频直播。 2005 年的主题是“古老的天文台， 永恒的 知识 ” (Ancient Observatories, TimelessKnowledge) ，该主题以视频和网络广播节目为特色，介绍了太阳与标志着春分和至日的结构的排列。参观的地点包括查科峡谷 ( 新墨西哥 ) 和奇琴伊察 ( 墨西哥 ) 。

“第一年有超过 8 万人下载和观看。现在我们已经达到了数百万人。克莱恩。“人们开始一有机会就使用我们的材料，甚至在夏天。所以现在这个项目是全年的。”

SECEF 刚刚结束了“ 2008 年日地日”，即“全球空间天气”，其中包括对奥运会前几天在中国上空看到的日食的现场报道。为支持即将到来的国际天文学年， 2009 年日地日的主题是“我们的太阳，你来发现”。

克莱恩解释说 : “我们将专注于尤利西斯号和旅行者号等任务，以及其他行星的空间天气。”“我们的目标不仅是强调太阳物理学中的重要概念，而且要表明宇宙中有许多像我们的太阳一样的恒星。”

SECEF 活动的力量在于，学生几乎是实时地查看真实的数据。刘易斯说 : ”学生们知道他们看到的是科学家们看到的相同的数据。克莱恩补充说 : ”他们永远不会觉得老师只是为了让他们理解而创造了一些较低水平的东西。”

此外，学生可以通过多种不同的学习方式与数据进行交互。刘易斯说 : ”如果学生可以读它，写它，触摸它，听它，参与它，并通过它表达自己，那么教师就是在让课堂上所有孩子都能接触到内容。”“这就是我们的目标。”

### 太空天气漫画

为了让孩子们更容易理解太空天气的基本概念，一些团体推出了以太空天气为主题的漫画书。漫画书的主要角色都是女性，这有助于吸引女孩进入科学领域。漫画书带领读者踏上了一段穿越太阳-地球环境的旅程。

 **摩尔和她的机器狗 Mirubo** 

通过9本系列漫画书， Mol 和 Mirubo 探索了他们对太空天气和日地关系的好奇心。这些漫画由名古屋大学日地环境实验室开发，最初以日文出版，现在被翻译成大约20种语言。

 **辛迪和她的太空狗** 

在《Cindi In Space》中，一个机器人女孩和她的两只太空狗穿越近地空间环境，捕捉和计数离子和中性粒子。由科学家开发，在达拉斯的德克萨斯大学，Cindi 代表了最近发射的空间天气检测卫星上的一种仪器。

 **齐里安和外星人** 

在美国国家海洋和大气管理局的漫画书《太空天气》中，科学家齐里安和一名外星偷渡者从地球到太阳旅行，以了解为什么太空天气会破坏齐里安的电视。

### 漫画书

并非所有向学生介绍太空天气的努力都涉及高科技、动手实践活动。相反，一些团体正转向漫画书向儿童传递信息 ( 见“空间天气漫画书”侧边栏 ) 。

2001 年，美国国家海洋和大气管理局(NOAA)出版了一本名为《空间天气》(Space Weather)的漫画书，由詹德·坎农(Zander Cannon)绘图。几年后，借鉴日本漫画的强大传统，当时在名古屋大学日地环境实验室(STEL)工作的神出阳介开始创作关于日地关系的漫画书。

在这些书中，日本女孩 Mol 和她的机器狗 Mirubo探索着他们对太空天气的好奇。这九部漫画由日本著名科学漫画家 hayon 绘制，最初以日文出版，得到了国际日地物 理科学委 员会 (SCOSTEP) 的大力 推广。“到目前为止，这些漫画书在 STEL 和 SCOSTEP 每年的点击量超过 200 万次，”神出阳介说，他现在在京都大学，但仍参与 STEL 的推广活动。

受 NOAA 漫画书的启发，德克萨斯大学空间科学中心的科学家们在达拉斯， 2005 年开发了一本名为“ Cindi 在太空”的漫画书， 以强 调耦合 离子 中性 动力学 调查 (CINDI) ，这是NASA/ 美国空军资助的一个联合项目。 CINDI 仪器由空间科学中心建造，由两个传感器组成，一个用于离子，一个用于中性粒子，旨在分析它们在电离层中的组成和动力学。CINDI 是在 2008 年 4 月发射的空军通信 / 导航中断预测卫星 (C/NOFS) 上运行的。

这本书由埃里克·莱瓦尔德 (Erik Levold) 配图，讲述了一个名叫辛蒂 (Cindi) 的机器人女孩在太空寻找太空狗的故事 : 这些狗的身体带正电，尾巴带负电。长着尾巴的狗一般、中性、稳重。但如果狗吃了一块能量饼干，它的尾巴就会飞掉，狗就会变得积极向上。这就造成了大量的狗尾巴漂浮在太空中，寻找新的主人。如果尾巴找到了新主人，狗狗会再次变得中立，并排出能量饼干。辛蒂有专门的网来计数中性和阳性的狗，类似于两种不同的 CINDI 仪器。

为什么漫画书有这么大的吸引力 ?Kamide 说 : ”漫画书对普通观众和孩子来说很容易了解太空天气和科学。””我们使用一个女孩， Mol ，试图鼓励女性专注于科学。 Kamide 解释说， Mol 热爱科学，不怕问老师问题。“通过这种方式，读者会觉得科学在日常生活中无处不在。”

### 保持局部性

虽然许多让学生学习太空天气的努力都集中在与国家标准的联系上，但一些团体反而把他们的项目更多地放在当地。

例如，德克萨斯大学达拉斯分校的科学和数学教育副教授玛丽·厄克特为 CINDI 的推广页面准备了教育材料，其中包含了专门为德克萨斯州标准量身定制的教育材料。但是，除了简单地在网站上提供信息外，厄克特还将网站材料集成到她所在大学的卓越科学教学区域合作项目中，这是一个针对教师的专业发展项目。厄克特说 : ”每个教师都应该在参加我们的讲习班后出去培训更多的教师。”

这与单纯从网上下载东西是非常不同的。厄克特说 : “我们发现，在工作坊中实际尝试一些东西，对于它是否真的用于课堂或课外科学项目有很大的不同。”

### 重点在于学生能理解什么

回到阿拉斯加，威廉姆斯继续准备她的课程计划。“最终，我必须选择学生们能理解的活动，”她说。“如果我浏览一个网站，发现它的内容太复杂，我知道这会让孩子们太沮丧。所以我要么修改或简化内容，要么跳过它。”

对于在加州的 Traeger 来说，他工作中最难的部分之一是他的几个学生是物理学的新手。他说 : “他们不了解磁感应，所以很难掌握地球内部磁场是如何形成的。”“让他们理解太阳风的含义也很困难——他们认为太阳风就像地球上的风一样。”

根据约翰逊的说法，将抽象的空间天气概念降低到孩子们可以理解的水平，是教孩子们关于空间天气的基本挑战。容易理解的网络资源 ( 参见“教育材料”侧栏 ) 是至关重要的。尽管如此，“许多科学家很难分辨他们所掌握的所有知识与八年级学生所能理解的知识之间的界限，”约翰逊说。“这就是为什么那些关注地球科学教育的人是如此的重要，他们有良好的基础，但对教育有热爱、激情和天赋。”

 **莫希·库马尔(Mohi Kumar)是《美国地球物理》特约撰稿人联盟。** 

### 教育材料

除了本文列出的网站外，还有几个网站也可以帮助教师向学生介绍空间天气。

- 国家科学教育标准（美国）
- 太阳和日光层天文台
- Sun|trek
- 名古屋大学日地环境实验室
- 天空天气中心