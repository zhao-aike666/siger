<p><img width="706px" src="https://foruda.gitee.com/images/1669051267363749104/d6452d14_5631341.jpeg" title="458CANSATarduino.jpg"></p>

> 这期专题是纯学习笔记，不分先后，转载内容从多个角度为同学们展示了 CANSAT 的面貌。如题，之所以选择 ARDUINO 是因为它上手易，在上一期 [ESA cansat resources](https://gitee.com/flame-ai/siger/blob/master/sig/%E9%81%A5%E6%9C%9B%E6%AF%94%E9%82%BB/Cansat%20resources%20ESA.md) 的专题中，100%转载的上手指南中，推荐的 “星上计算机” 也是 ARDUINO。本期将其他开源的 CANSAT 项目以及CANSAT的相关资料汇集一处，能够给同学们一个概貌，作为进入航天领域的一个敲门砖。同时，本期专题还回答了 WHY CANSAT? 的问题，除了 [SIGER #5 第5期](https://gitee.com/flame-ai/siger/blob/master/%E7%AC%AC5%E6%9C%9F%20CANSAT%20%E6%88%91%E7%9A%84%E8%88%AA%E5%A4%A9%E6%A2%A6.md) 的呼应，还有 OPEN1059 立项后的学习线索的延续，要点如下：

- 风马牛团队将独家开发一套用于卫星信号接收的硬件设备，并与国内顶尖的青少年航天教育机构合作，打造一系列基于卫星信号接收的课程与兴趣活动，激活大众、特别是少年儿童对航空航天事业的兴趣，让卫星所产生的社会意义和价值最大化。（转自：[《全球业余卫星介绍》（五）中国业余卫星（下）](http://www.srrc.org.cn/article24578.aspx)）

  > 这个信息就对上啦。

- 国家无线电监测中心作为无线电管理技术机构，指导下属单位北京东方波泰无线电频谱技术研究所专门建设了业余无线电科普研学基地。[图7](https://foruda.gitee.com/images/1669052445660116894/d9197d5f_5631341.png)  研究所建设的业余无线电科普基地（转自：[《全球业余卫星介绍》（四）中国业余卫星（上）](http://www.srrc.org.cn/article24576.aspx)）有了国家无线电监测中心的技术支持，于2020年11月投入试运行的业余无线电科普研学基地，今后将在无线电通信技术、天文知识等多领域科学普及中发挥更大作用，通过科普带动大家的科研探索兴趣，努力让公众理解科学、热爱科学、探究科学，培养科研人才，助力科技强国建设。

  > EME这个基地也是有验证成功的经验，说是和俄罗斯连上过。[官方科普基地](http://www.srrc.org.cn/article24641.aspx)。  
  > EME的译文视频找到了。非常科普，SIGER专题已经立项。射电望远镜的诞生在月面反射通讯之后[强]

- 紫金山一号卫星主要目的在于构建飞行软件在轨试验平台，用于分析空间单粒子效应对FPGA软件功能和性能的影响。（转自：[《全球业余卫星介绍》（五）中国业余卫星（下）](http://www.srrc.org.cn/article24578.aspx)）紫丁香一号的特别之处在于，该卫星由哈尔滨工业大学的15名大学生设计建造，是大中小学生参与航天探索的有益尝试。

  > FPGA安全培训中有三模态的方案，作为解决方案。之后会有专题介绍。

- 之前哈工大那边做小卫星确实蛮多，跟航天那边也很熟。北航我记着也做过。xw-4和梦天的业余无线电载荷是两个不同的东西，xw-4应该快了

  > 希望四号卫星，会通过梦天的卫星发射机制发射。是我国新一代业余卫星。（[《全球业余卫星介绍》（五）中国业余卫星（下）](http://www.srrc.org.cn/article24578.aspx)）这个以操作为主，介绍了ISS的业余无线电和航天员通话的可能性非常小，主要还是时间不同步。很难遇上。咱们的梦天估计应该是同样为内定方式，天地通话。

CANSAT是在火箭滞空的10几分钟期间验证卫星功能。  
之前调研飞控，引到无人机上去了。CANSAT的调试也会用到无人机。  
无人机飞控，已有开源方案由专业公司主导的案例，也是学习飞控的重要解决案例，很多无人机竞赛也有大量采用。

> OPEN1059以开源全栈学习为出发点。围绕FPGA展开业余卫星星上系统，可作为下一个方向。目前SIGER有过一期CANSAT调研的基础，有开源案例学生作品。再结合业余卫星通讯的应用案例，做一个初版方案。
> 
> 目前使用新技术包括成熟的商业民用方案在飞控和航天的应用案例，在民营公司中皆有使用。可以作为OPEN1059的一方借鉴。
> 
> CANSAT和业余卫星通常选用成熟的MCU，围绕其设计PCB和功能。OPEN1059为什么以FPGA为起点除“高速并行”飞控的设想外，也是基于FPGA软件便于迭代的原因。

  - 以上腹稿，作为一个起点。开源CANSAT替换成FPGA软核的MCU，是第一版 OPEN1059。
  - 原本OPEN1059应该有火箭部分，但相比业余卫星通讯，更接近青少年。也是103的分享成果 :pray: 

> 关于 FPGA 的挑战，并行处理能力，肯定是它的强项，石榴派的原型开发板最初设计的目标也是用来多外设控制用的，30个串口，我觉得符合 多传感器，多数据采集的星上需求。可以作为 OPEN1059的一个特点。对于 FPGA 的 ARDUINO 也是成熟的技术，很多arduino 开发板本身就是 FPGA 芯片。也有MCU类的开源软核可以使用。

> 微纳卫星和开放原子基金会合作的鸿蒙系统上星计划，其中就看重了鸿蒙系统的可裁剪能力。今年还没有发布什么成果。就 FPGA 的适应能力，应该不是在通用系统上，而应该是航天应用的多数据并发的能力上，鸿蒙如何对接，应该是鸿蒙的课题，而不应该是航天行业的课题。OPEN1059的开源着力点应该是在应用上。

> CANSAT 的传感器和应用场景和真卫星肯定有相当的差距，它的科普学习目的的定位是和 OPEN1059 一致的。也是 UK 的 CANSAT 调研成果。

- 老师的想法很棒

  > 两步走：
    1. 搜集足够多的开源的星上计算机的方案，包括电路图等资料。
    2. 面向业余无线电组织征询意见和【贡献】

# [第5期 CANSAT 我的航天梦.md](https://gitee.com/flame-ai/siger/blob/master/第5期%20CANSAT%20我的航天梦.md#step-3) 摘要：

### Materials(click for image):

1. Arduino Pro mini 3.3v 8Mhz atmega328 (1)
1. IMU GY-80(1)
1. 10k 1/4watt resistor (1).
1. Push button (1)
1. 2.54mm angle 90 header (6)
1. 2.54mm header(2)
1. Molex connector 7 pin, male(1). Really important... 7 PIN
1. FTDI 3.3v Cable

一个 CANSAT 选手的分享。

### COMPONENTS

| numbers | | components |
|---|---|---|
| 1 | × |  **Headers** <br>2.54mm male pin header |
| 2 | × |  **2mm 10pin Xbee Socket** <br>Xbee Socket |
| 1 | × |  **Socket 2.54mm** <br>Standart socket for headers |
| 1 | × |  **103 ceramic capacitor** <br>.01uF ceramic |
| 8 | × |  **7 pin molex conector** <br>Male and female |
| 1 | × |  **MicroSwitch** <br>For PCB |
| 1 | × |  **Jumper** <br>For 2.54mm header |
| 1 | × |  **Arduino pro mini 3.3v 8mhz** <br>An Arduino |
| 1 | × |  **GY-80 IMU** <br>10DOF |
| 1 | × |  **DC-DC Boost** <br>lm2577 |
| 1 | × |  **Regulator 3.3 AMS1117** <br>A linear regulator |
| 1 | × |  **Lipo Battery Charger** <br>TLIP1330 |
| 1 | × |  **Lipo Battery** <br>1200mAh 3.7V 1C |
| 1 | × |  **GPS** <br>GP-635T |
| 1 | × |  **Xbee Pro s2b** <br>wired antenna |
| 1 | × |  **Xbee Xplorer USB** <br>Xbee xplorer |

### [DETAILS](https://gitee.com/flame-ai/siger/blob/master/%E7%AC%AC5%E6%9C%9F%20CANSAT%20%E6%88%91%E7%9A%84%E8%88%AA%E5%A4%A9%E6%A2%A6.md#cansat---exchangeable-payloads-arduino-and-edison-%E7%AC%94%E8%AE%B0)

I have two payloads:

- PL-01: Measures C02, Humedity and temperature
- PL-02: Measures the particles in the air (smog, dust and other solids in PMM).

<h1>CanSat Kit Library</h1>

<p class="categories">
          
<mark>Other</mark>
          
</p>

<p class="description">
          Library for CanSat Kit.
<br>
          
          Contains libraries for SX1278 and BMP280.
          
</p>

        
<p class="author">
          Author:<span>Grzegorz Gajoch</span>
</p>
<p class="maintainer">
          Maintainer:<span>Grzegorz Gajoch</span>
</p>
        

        
<p><a href="https://github.com/CanSatKit/CanSatKitLibrary">Read the documentation</a></p>
        

        

        

<h3>Compatibility</h3>

        

        
<p class="compatibility">This library is compatible with the<b>samd</b>
          architecture so you
          should be able to use it on the
          following Arduino boards:</p>
<ul>
          
<li><a href="https://store.arduino.cc/arduino-mkr-fox-1200-1408">Arduino MKR FOX 1200</a></li>
          
<li><a href="https://store.arduino.cc/arduino-mkr-gsm-1400-1415">Arduino MKR GSM 1400</a></li>
          
<li><a href="https://store.arduino.cc/arduino-mkr-nb-1500-1413">Arduino MKR NB 1500</a></li>
          
<li><a href="https://store.arduino.cc/arduino-mkr-vidor-4000">Arduino MKR VIDOR 4000</a></li>
          
<li><a href="https://store.arduino.cc/arduino-mkr-wan-1300-lora-connectivity-1414">Arduino MKR WAN 1300 (LoRa connectivity)</a></li>
          
<li><a href="https://store.arduino.cc/mkr-wan-1310">Arduino MKR WAN 1310</a></li>
          
<li><a href="https://store.arduino.cc/arduino-mkr-wifi-1010">Arduino MKR WiFi 1010</a></li>
          
<li><a href="https://store.arduino.cc/arduino-mkr-zero-i2s-bus-sd-for-sound-music-digital-audio-data">Arduino MKR ZERO (I2S bus &amp; SD for sound, music &amp; digital audio data)</a></li>
          
<li><a href="https://store.arduino.cc/arduino-mkr1000-wifi">Arduino MKR1000 WIFI</a></li>
          
<li><a href="https://store.arduino.cc/arduino-nano-33-iot">Arduino Nano 33 IoT</a></li>
          
<li><a href="https://store.arduino.cc/arduino-zero">Arduino Zero</a></li>
          
</ul>
<h4>Compatibility Note</h4>
<p>
<b>Note:</b> while the library is supposed to compile correctly on these architectures,
          it might require specific hardware features that may be available only on some boards.
</p>
        

<h3>Releases</h3>

<p>To use this library, open the<a href="https://www.arduino.cc/en/Guide/Libraries">Library Manager</a> in
          the
          Arduino IDE and install it from there.</p>

<ul>
          
<li>
<a href="https://downloads.arduino.cc/libraries/github.com/CanSatKit/CanSat_Kit_Library-1.3.1.zip">1.3.1</a>
            (latest)
</li>
          
<li>
<a href="https://downloads.arduino.cc/libraries/github.com/CanSatKit/CanSat_Kit_Library-1.3.0.zip">1.3.0</a>
            
</li>
          
<li>
<a href="https://downloads.arduino.cc/libraries/github.com/CanSatKit/CanSat_Kit_Library-1.2.0.zip">1.2.0</a>
            
</li>
          
<li>
<a href="https://downloads.arduino.cc/libraries/github.com/CanSatKit/CanSat_Kit_Library-1.1.0.zip">1.1.0</a>
            
</li>
          
<li>
<a href="https://downloads.arduino.cc/libraries/github.com/CanSatKit/CanSat_Kit_Library-1.0.0.zip">1.0.0</a>
            
</li>
          
</ul>

<div class="content">

          

</div>

<div class="section" id="radio-library">
<h1>Radio Library<a class="headerlink" href="#radio-library" title="Permalink to this headline">¶</a></h1>
<dl class="class">
<dt id="_CPPv3N9CanSatKit5RadioE">
<span id="_CPPv2N9CanSatKit5RadioE"></span><span id="CanSatKit::Radio"></span><span class="target" id="class_can_sat_kit_1_1_radio"></span><em class="property">class </em><code class="descname">Radio</code><a class="headerlink" href="#_CPPv3N9CanSatKit5RadioE" title="Permalink to this definition">¶</a><br></dt>
<dd><div class="breathe-sectiondef docutils container">
<p class="breathe-sectiondef-title rubric">Public Functions</p>
<dl class="function">
<dt id="_CPPv3N5Radio5RadioEiif9Bandwidth15SpreadingFactor10CodingRate">
<span id="_CPPv2N5Radio5RadioEiif9Bandwidth15SpreadingFactor10CodingRate"></span><span id="Radio::Radio__i.i.float.Bandwidth.SpreadingFactor.CodingRate"></span><span class="target" id="class_can_sat_kit_1_1_radio_1a207ff151a394e603ae1f9cb010a2a871"></span><code class="descname">Radio</code><span class="sig-paren">(</span>int <em>pin_cs_</em>, int <em>pin_dio0_</em>, float <em>frequency_in_mhz</em>, Bandwidth <em>bandwidth</em>, SpreadingFactor <em>spreadingFactor</em>, CodingRate <em>codingRate</em><span class="sig-paren">)</span><a class="headerlink" href="#_CPPv3N5Radio5RadioEiif9Bandwidth15SpreadingFactor10CodingRate" title="Permalink to this definition">¶</a><br></dt>
<dd><p>Construct a new <a class="reference internal" href="#class_can_sat_kit_1_1_radio"><span class="std std-ref">Radio</span></a> object. </p>
<p>Settings should be the same on the receiver and transmitter. Make sure that you comply with CanSat and local regulations. Settings reflect on bitrate and link budget. Bitrate = bandwidth/(2**spreadingFactor) * codingRate</p>
<p></p><dl class="docutils">
<dt><strong>Parameters</strong></dt>
<dd><ul class="breatheparameterlist first last simple">
<li><code class="docutils literal notranslate"><span class="pre">pin_cs_</span></code>: Arduino pin number connected to radio CS pin. Set to <code class="docutils literal notranslate"><span class="pre">Pins::Radio::ChipSelect</span></code> if you use CanSatKit. </li>
<li><code class="docutils literal notranslate"><span class="pre">pin_dio0_</span></code>: Arduino pin number connected to radio DIO0 pin. Set to <code class="docutils literal notranslate"><span class="pre">Pins::Radio::DIO0</span></code> if you use CanSatKit. </li>
<li><code class="docutils literal notranslate"><span class="pre">frequency_in_mhz</span></code>: Set radio center frequency. </li>
<li><code class="docutils literal notranslate"><span class="pre">bandwidth</span></code>: Set module radio bandwidth. </li>
<li><code class="docutils literal notranslate"><span class="pre">spreadingFactor</span></code>: Set module spreading factor. </li>
<li><code class="docutils literal notranslate"><span class="pre">codingRate</span></code>: Set module coding rate. </li>
</ul>
</dd>
</dl>
<p></p>
</dd></dl>

</div>
<div class="breathe-sectiondef docutils container">
<p class="breathe-sectiondef-title rubric">Public Static Functions</p>
<dl class="function">
<dt id="_CPPv3N5Radio5beginEv">
<span id="_CPPv2N5Radio5beginEv"></span><span id="Radio::begin"></span><span class="target" id="class_can_sat_kit_1_1_radio_1a3e5ca3b4018ffabe06d0ed332d5db1c8"></span>bool <code class="descname">begin</code><span class="sig-paren">(</span><span class="sig-paren">)</span><a class="headerlink" href="#_CPPv3N5Radio5beginEv" title="Permalink to this definition">¶</a><br></dt>
<dd><p>Start communication with radio module. </p>
<p>Sets proper radio settings and starts module in receive mode.</p>
<p></p><dl class="docutils">
<dt><strong>Return</strong></dt>
<dd><code class="docutils literal notranslate"><span class="pre">true</span></code>: Communication succeeded, module initialised properly. <code class="docutils literal notranslate"><span class="pre">false</span></code>: Module initialisation failed. </dd>
</dl>
<p></p>
</dd></dl>

<dl class="function">
<dt id="_CPPv3N5Radio13disable_debugEv">
<span id="_CPPv2N5Radio13disable_debugEv"></span><span id="Radio::disable_debug"></span><span class="target" id="class_can_sat_kit_1_1_radio_1a5922cb2a9cac8ddeb201a4c7fb1bec2f"></span>void <code class="descname">disable_debug</code><span class="sig-paren">(</span><span class="sig-paren">)</span><a class="headerlink" href="#_CPPv3N5Radio13disable_debugEv" title="Permalink to this definition">¶</a><br></dt>
<dd><p>Disable debug messages on SerialUSB. </p>
</dd></dl>

<dl class="function">
<dt id="_CPPv3N5Radio8transmitE5Frame">
<span id="_CPPv2N5Radio8transmitE5Frame"></span><span id="Radio::transmit__Frame"></span><span class="target" id="class_can_sat_kit_1_1_radio_1a7c66e71a82a418e1d10d75bb8b8aaa8d"></span>bool <code class="descname">transmit</code><span class="sig-paren">(</span>Frame <em>frame</em><span class="sig-paren">)</span><a class="headerlink" href="#_CPPv3N5Radio8transmitE5Frame" title="Permalink to this definition">¶</a><br></dt>
<dd><p>Put frame into the transmit buffer. </p>
<p></p><dl class="docutils">
<dt><strong>Return</strong></dt>
<dd><code class="docutils literal notranslate"><span class="pre">true</span></code> if frame put into buffer. <code class="docutils literal notranslate"><span class="pre">false</span></code> if not enough space in the buffer. </dd>
</dl>
<p></p>
</dd></dl>

<dl class="function">
<dt id="_CPPv3N5Radio8transmitE6String">
<span id="_CPPv2N5Radio8transmitE6String"></span><span id="Radio::transmit__String"></span><span class="target" id="class_can_sat_kit_1_1_radio_1a31b9bcdc97af4e50902c9326b0159588"></span>bool <code class="descname">transmit</code><span class="sig-paren">(</span>String <em>str</em><span class="sig-paren">)</span><a class="headerlink" href="#_CPPv3N5Radio8transmitE6String" title="Permalink to this definition">¶</a><br></dt>
<dd><p>Put String str into the transmit buffer. </p>
<p></p><dl class="docutils">
<dt><strong>Return</strong></dt>
<dd><code class="docutils literal notranslate"><span class="pre">true</span></code> if frame put into buffer. <code class="docutils literal notranslate"><span class="pre">false</span></code> if not enough space in the buffer. </dd>
</dl>
<p></p>
</dd></dl>

<dl class="function">
<dt id="_CPPv3N5Radio8transmitEPKc">
<span id="_CPPv2N5Radio8transmitEPKc"></span><span id="Radio::transmit__cCP"></span><span class="target" id="class_can_sat_kit_1_1_radio_1add1b5d4ea1060c0c1f37473560143db6"></span>bool <code class="descname">transmit</code><span class="sig-paren">(</span><em class="property">const</em> char *<em>str</em><span class="sig-paren">)</span><a class="headerlink" href="#_CPPv3N5Radio8transmitEPKc" title="Permalink to this definition">¶</a><br></dt>
<dd><p>Put string str into the transmit buffer. </p>
<p></p><dl class="docutils">
<dt><strong>Return</strong></dt>
<dd><code class="docutils literal notranslate"><span class="pre">true</span></code> if frame put into buffer. <code class="docutils literal notranslate"><span class="pre">false</span></code> if not enough space in the buffer. </dd>
</dl>
<p></p>
</dd></dl>

<dl class="function">
<dt id="_CPPv3N9CanSatKit5Radio8transmitEPKNSt7uint8_tENSt7uint8_tE">
<span id="_CPPv2N9CanSatKit5Radio8transmitEPKNSt7uint8_tENSt7uint8_tE"></span><span id="CanSatKit::Radio::transmit__std::uint8_tCP.std::uint8_t"></span><span class="target" id="class_can_sat_kit_1_1_radio_1a2831545aabc08c33a48154adc60048c1"></span><em class="property">static</em> bool <code class="descname">transmit</code><span class="sig-paren">(</span><em class="property">const</em> std::uint8_t *<em>data</em>, std::uint8_t <em>length</em><span class="sig-paren">)</span><a class="headerlink" href="#_CPPv3N9CanSatKit5Radio8transmitEPKNSt7uint8_tENSt7uint8_tE" title="Permalink to this definition">¶</a><br></dt>
<dd><p>Put binary data into the transmit buffer (byte table of length length). </p>
<p></p><dl class="docutils">
<dt><strong>Return</strong></dt>
<dd><code class="docutils literal notranslate"><span class="pre">true</span></code> if frame put into buffer. <code class="docutils literal notranslate"><span class="pre">false</span></code> if not enough space in the buffer. </dd>
</dl>
<p></p>
</dd></dl>

<dl class="function">
<dt id="_CPPv3N5Radio5flushEv">
<span id="_CPPv2N5Radio5flushEv"></span><span id="Radio::flush"></span><span class="target" id="class_can_sat_kit_1_1_radio_1a7b65475e981802c13da1e96466663417"></span>void <code class="descname">flush</code><span class="sig-paren">(</span><span class="sig-paren">)</span><a class="headerlink" href="#_CPPv3N5Radio5flushEv" title="Permalink to this definition">¶</a><br></dt>
<dd><p>Waits until all frames in the transmit buffer are transmitted. </p>
</dd></dl>

<dl class="function">
<dt id="_CPPv3N5Radio9availableEv">
<span id="_CPPv2N5Radio9availableEv"></span><span id="Radio::available"></span><span class="target" id="class_can_sat_kit_1_1_radio_1aebe3e9e77127ca01020f9bb149cc4a57"></span>std::uint8_t <code class="descname">available</code><span class="sig-paren">(</span><span class="sig-paren">)</span><a class="headerlink" href="#_CPPv3N5Radio9availableEv" title="Permalink to this definition">¶</a><br></dt>
<dd><p>Get number of frames in receive buffer. </p>
<p></p><dl class="docutils">
<dt><strong>Return</strong></dt>
<dd>std::uint8_t Number of received frames. </dd>
</dl>
<p></p>
</dd></dl>

<dl class="function">
<dt id="_CPPv3N5Radio7receiveEPc">
<span id="_CPPv2N5Radio7receiveEPc"></span><span id="Radio::receive__cP"></span><span class="target" id="class_can_sat_kit_1_1_radio_1a3a7c53e478dc95faad0328e3dec8217d"></span>void <code class="descname">receive</code><span class="sig-paren">(</span>char *<em>data</em><span class="sig-paren">)</span><a class="headerlink" href="#_CPPv3N5Radio7receiveEPc" title="Permalink to this definition">¶</a><br></dt>
<dd><p>Get character string from receive buffer. </p>
<p></p><dl class="docutils">
<dt><strong>Parameters</strong></dt>
<dd><ul class="breatheparameterlist first last simple">
<li><code class="docutils literal notranslate"><span class="pre">data</span></code>: pointer to fill with data </li>
</ul>
</dd>
</dl>
<p></p>
</dd></dl>

<dl class="function">
<dt id="_CPPv3N9CanSatKit5Radio7receiveEPNSt7uint8_tERNSt7uint8_tE">
<span id="_CPPv2N9CanSatKit5Radio7receiveEPNSt7uint8_tERNSt7uint8_tE"></span><span id="CanSatKit::Radio::receive__std::uint8_tP.std::uint8_tR"></span><span class="target" id="class_can_sat_kit_1_1_radio_1a8f958995748e4c97402f12c6c7bd722b"></span><em class="property">static</em> void <code class="descname">receive</code><span class="sig-paren">(</span>std::uint8_t *<em>data</em>, std::uint8_t &amp;<em>length</em><span class="sig-paren">)</span><a class="headerlink" href="#_CPPv3N9CanSatKit5Radio7receiveEPNSt7uint8_tERNSt7uint8_tE" title="Permalink to this definition">¶</a><br></dt>
<dd><p>Get binary data from receive buffer. </p>
<p></p><dl class="docutils">
<dt><strong>Parameters</strong></dt>
<dd><ul class="breatheparameterlist first last simple">
<li><code class="docutils literal notranslate"><span class="pre">data</span></code>: pointer to fill with data </li>
<li><code class="docutils literal notranslate"><span class="pre">length</span></code>: length of received frame </li>
</ul>
</dd>
</dl>
<p></p>
</dd></dl>

<dl class="function">
<dt id="_CPPv3N5Radio13get_rssi_lastEv">
<span id="_CPPv2N5Radio13get_rssi_lastEv"></span><span id="Radio::get_rssi_last"></span><span class="target" id="class_can_sat_kit_1_1_radio_1a5d3e07ff32db84f15020239f2124dcf5"></span>int <code class="descname">get_rssi_last</code><span class="sig-paren">(</span><span class="sig-paren">)</span><a class="headerlink" href="#_CPPv3N5Radio13get_rssi_lastEv" title="Permalink to this definition">¶</a><br></dt>
<dd><p>Get the RSSI of last frame. </p>
<p></p><dl class="docutils">
<dt><strong>Return</strong></dt>
<dd>int RSSI in dBm </dd>
</dl>
<p></p>
</dd></dl>

<dl class="function">
<dt id="_CPPv3N5Radio12get_rssi_nowEv">
<span id="_CPPv2N5Radio12get_rssi_nowEv"></span><span id="Radio::get_rssi_now"></span><span class="target" id="class_can_sat_kit_1_1_radio_1a376105d60e5fcb5d8ea230bc20959b9f"></span>int <code class="descname">get_rssi_now</code><span class="sig-paren">(</span><span class="sig-paren">)</span><a class="headerlink" href="#_CPPv3N5Radio12get_rssi_nowEv" title="Permalink to this definition">¶</a><br></dt>
<dd><p>Get the actual RSSI value. </p>
<p></p><dl class="docutils">
<dt><strong>Return</strong></dt>
<dd>int RSSI in dBm </dd>
</dl>
<p></p>
</dd></dl>

</div>
</dd></dl>

<div class="section" id="possible-settings">
<h2>Possible settings:<a class="headerlink" href="#possible-settings" title="Permalink to this headline">¶</a></h2>
<p>Bandwidth:</p>
<div class="wy-table-responsive"><table border="1" class="docutils">
<colgroup>
<col width="26%">
<col width="74%">
</colgroup>
<thead valign="bottom">
<tr class="row-odd"><th class="head">Bandwidth</th>
<th class="head">Constant</th>
</tr>
</thead>
<tbody valign="top">
<tr class="row-even"><td>7800 Hz</td>
<td><cite>CanSatKit::Bandwidth_7800_Hz</cite></td>
</tr>
<tr class="row-odd"><td>10400 Hz</td>
<td><cite>CanSatKit::Bandwidth_10400_Hz</cite></td>
</tr>
<tr class="row-even"><td>15600 Hz</td>
<td><cite>CanSatKit::Bandwidth_15600_Hz</cite></td>
</tr>
<tr class="row-odd"><td>20800 Hz</td>
<td><cite>CanSatKit::Bandwidth_20800_Hz</cite></td>
</tr>
<tr class="row-even"><td>31250 Hz</td>
<td><cite>CanSatKit::Bandwidth_31250_Hz</cite></td>
</tr>
<tr class="row-odd"><td>41700 Hz</td>
<td><cite>CanSatKit::Bandwidth_41700_Hz</cite></td>
</tr>
<tr class="row-even"><td>62500 Hz</td>
<td><cite>CanSatKit::Bandwidth_62500_Hz</cite></td>
</tr>
<tr class="row-odd"><td>125000 Hz</td>
<td><cite>CanSatKit::Bandwidth_125000_Hz</cite></td>
</tr>
<tr class="row-even"><td>250000 Hz</td>
<td><cite>CanSatKit::Bandwidth_250000_Hz</cite></td>
</tr>
<tr class="row-odd"><td>500000 Hz</td>
<td><cite>CanSatKit::Bandwidth_500000_Hz</cite></td>
</tr>
</tbody>
</table></div>
<p>Spreading Factor:</p>
<div class="wy-table-responsive"><table border="1" class="docutils">
<colgroup>
<col width="36%">
<col width="64%">
</colgroup>
<thead valign="bottom">
<tr class="row-odd"><th class="head">Spreading Factor</th>
<th class="head">Constant</th>
</tr>
</thead>
<tbody valign="top">
<tr class="row-even"><td>7</td>
<td><cite>CanSatKit::SpreadingFactor_7</cite></td>
</tr>
<tr class="row-odd"><td>8</td>
<td><cite>CanSatKit::SpreadingFactor_8</cite></td>
</tr>
<tr class="row-even"><td>9</td>
<td><cite>CanSatKit::SpreadingFactor_9</cite></td>
</tr>
<tr class="row-odd"><td>10</td>
<td><cite>CanSatKit::SpreadingFactor_10</cite></td>
</tr>
<tr class="row-even"><td>11</td>
<td><cite>CanSatKit::SpreadingFactor_11</cite></td>
</tr>
<tr class="row-odd"><td>12</td>
<td><cite>CanSatKit::SpreadingFactor_12</cite></td>
</tr>
</tbody>
</table></div>
<p>Coding rate:</p>
<div class="wy-table-responsive"><table border="1" class="docutils">
<colgroup>
<col width="36%">
<col width="64%">
</colgroup>
<thead valign="bottom">
<tr class="row-odd"><th class="head">Coding rate</th>
<th class="head">Constant</th>
</tr>
</thead>
<tbody valign="top">
<tr class="row-even"><td>4/5</td>
<td><cite>CanSatKit::CodingRate_4_5</cite></td>
</tr>
<tr class="row-odd"><td>4/6</td>
<td><cite>CanSatKit::CodingRate_4_6</cite></td>
</tr>
<tr class="row-even"><td>4/7</td>
<td><cite>CanSatKit::CodingRate_4_7</cite></td>
</tr>
<tr class="row-odd"><td>4/8</td>
<td><cite>CanSatKit::CodingRate_4_8</cite></td>
</tr>
</tbody>
</table></div>
</div>
</div>
<div class="section" id="radio-frame">
<h1>Radio Frame<a class="headerlink" href="#radio-frame" title="Permalink to this headline">¶</a></h1>
<dl class="class">
<dt id="_CPPv3N9CanSatKit5FrameE">
<span id="_CPPv2N9CanSatKit5FrameE"></span><span id="CanSatKit::Frame"></span><span class="target" id="class_can_sat_kit_1_1_frame"></span><em class="property">class </em><code class="descname">Frame</code> : <em class="property">public</em> Print<a class="headerlink" href="#_CPPv3N9CanSatKit5FrameE" title="Permalink to this definition">¶</a><br></dt>
<dd><p><a class="reference internal" href="#class_can_sat_kit_1_1_frame"><span class="std std-ref">Frame</span></a> object represents one radio frame to be send over the RF link. </p>
<p>Use it the same as Serial/SerialUSB devices. <a class="reference internal" href="#class_can_sat_kit_1_1_frame"><span class="std std-ref">Frame</span></a> should consist of ASCII-characters only. Remembers data in internal buffer, to be send using radio.transmit() method. Maximum data length is 254 bytes (+1 byte of null termination). </p>
<div class="breathe-sectiondef docutils container">
<p class="breathe-sectiondef-title rubric">Public Functions</p>
<dl class="function">
<dt id="_CPPv3N9CanSatKit5FramecvPKcEv">
<span id="_CPPv2N9CanSatKit5FramecvPKcEv"></span><span id="CanSatKit::Frame::castto-cCP-operator"></span><span class="target" id="class_can_sat_kit_1_1_frame_1a572fe50115f7bf17835052afb8044f12"></span><code class="descname">operator const char *</code><span class="sig-paren">(</span><span class="sig-paren">)</span><a class="headerlink" href="#_CPPv3N9CanSatKit5FramecvPKcEv" title="Permalink to this definition">¶</a><br></dt>
<dd><p>Returns null-terminated string object. </p>
<p></p><dl class="docutils">
<dt><strong>Return</strong></dt>
<dd>const char* string object </dd>
</dl>
<p></p>
</dd></dl>

<dl class="function">
<dt id="_CPPv3N9CanSatKit5Frame5clearEv">
<span id="_CPPv2N9CanSatKit5Frame5clearEv"></span><span id="CanSatKit::Frame::clear"></span><span class="target" id="class_can_sat_kit_1_1_frame_1a30eab7c5929789c5d5858dcd6d86db6d"></span>void <code class="descname">clear</code><span class="sig-paren">(</span><span class="sig-paren">)</span><a class="headerlink" href="#_CPPv3N9CanSatKit5Frame5clearEv" title="Permalink to this definition">¶</a><br></dt>
<dd><p>Clear frame. </p>
</dd></dl>

</div>
<div class="breathe-sectiondef docutils container">
<p class="breathe-sectiondef-title rubric">Public Members</p>
<dl class="member">
<dt id="_CPPv3N9CanSatKit5Frame4sizeE">
<span id="_CPPv2N9CanSatKit5Frame4sizeE"></span><span id="CanSatKit::Frame::size__std::uint8_t"></span><span class="target" id="class_can_sat_kit_1_1_frame_1acf6078715e7809ab2f6b34fa920afa05"></span>std::uint8_t <code class="descname">size</code><a class="headerlink" href="#_CPPv3N9CanSatKit5Frame4sizeE" title="Permalink to this definition">¶</a><br></dt>
<dd><p>actual size of the frame (string length) </p>
</dd></dl>

</div>
</dd></dl>

</div>
<h1>BMP280<a class="headerlink" href="#bmp280" title="Permalink to this headline">¶</a></h1>
<dl class="class">
<dt id="_CPPv3N9CanSatKit6BMP280E">
<span id="_CPPv2N9CanSatKit6BMP280E"></span><span id="CanSatKit::BMP280"></span><span class="target" id="class_can_sat_kit_1_1_b_m_p280"></span><em class="property">class </em><code class="descname">BMP280</code><a class="headerlink" href="#_CPPv3N9CanSatKit6BMP280E" title="Permalink to this definition">¶</a><br></dt>
<dd><p><a class="reference internal" href="#class_can_sat_kit_1_1_b_m_p280"><span class="std std-ref">BMP280</span></a> pressure sensor base class. </p>
<div class="breathe-sectiondef docutils container">
<p class="breathe-sectiondef-title rubric">Public Functions</p>
<dl class="function">
<dt id="_CPPv3N6BMP2806BMP280Ev">
<span id="_CPPv2N6BMP2806BMP280Ev"></span><span id="BMP280::BMP280"></span><span class="target" id="class_can_sat_kit_1_1_b_m_p280_1a3ca058a8acf0ee964ef3cccc903861a9"></span><code class="descname">BMP280</code><span class="sig-paren">(</span><span class="sig-paren">)</span><a class="headerlink" href="#_CPPv3N6BMP2806BMP280Ev" title="Permalink to this definition">¶</a><br></dt>
<dd><p>Construct a new <a class="reference internal" href="#class_can_sat_kit_1_1_b_m_p280"><span class="std std-ref">BMP280</span></a> object. </p>
</dd></dl>

<dl class="function">
<dt id="_CPPv3N6BMP2805beginEv">
<span id="_CPPv2N6BMP2805beginEv"></span><span id="BMP280::begin"></span><span class="target" id="class_can_sat_kit_1_1_b_m_p280_1a433ee6ee89702933e27128eacc717bf3"></span>bool <code class="descname">begin</code><span class="sig-paren">(</span><span class="sig-paren">)</span><a class="headerlink" href="#_CPPv3N6BMP2805beginEv" title="Permalink to this definition">¶</a><br></dt>
<dd><p>Initialize sensor and calibrate. </p>
<p></p><dl class="docutils">
<dt><strong>Return</strong></dt>
<dd><code class="docutils literal notranslate"><span class="pre">true</span></code> if initialization suceeded, <code class="docutils literal notranslate"><span class="pre">false</span></code> otherwise (eg. i2C connection problem) </dd>
</dl>
<p></p>
</dd></dl>

<dl class="function">
<dt id="_CPPv3N6BMP28015setOversamplingE7uint8_t">
<span id="_CPPv2N6BMP28015setOversamplingE7uint8_t"></span><span id="BMP280::setOversampling__uint8_t"></span><span class="target" id="class_can_sat_kit_1_1_b_m_p280_1a42f9e48492460d490cfe0cb67b3f59a3"></span>void <code class="descname">setOversampling</code><span class="sig-paren">(</span>uint8_t <em>oversampling</em><span class="sig-paren">)</span><a class="headerlink" href="#_CPPv3N6BMP28015setOversamplingE7uint8_t" title="Permalink to this definition">¶</a><br></dt>
<dd><p>Set oversampling ratio. </p>
<p>Should be one of the following: 0, 1, 2, 3, 4, 16.</p>
<p></p><div class="wy-table-responsive"><table border="1" class="docutils">
<colgroup>
<col width="39%">
<col width="61%">
</colgroup>
<thead valign="bottom">
<tr class="row-odd"><th class="head">Oversampling</th>
<th class="head">Conversion time (ms)</th>
</tr>
</thead>
<tbody valign="top">
<tr class="row-even"><td>0</td>
<td>8</td>
</tr>
<tr class="row-odd"><td>1</td>
<td>10</td>
</tr>
<tr class="row-even"><td>2</td>
<td>15</td>
</tr>
<tr class="row-odd"><td>3</td>
<td>24</td>
</tr>
<tr class="row-even"><td>4</td>
<td>45</td>
</tr>
<tr class="row-odd"><td>16</td>
<td>80</td>
</tr>
</tbody>
</table></div>
<p></p>
<p>Higher oversampling means longer measurement, but more accurate result</p>
<p></p><dl class="docutils">
<dt><strong>Parameters</strong></dt>
<dd><ul class="breatheparameterlist first last simple">
<li><code class="docutils literal notranslate"><span class="pre">oversampling</span></code>: ratio </li>
</ul>
</dd>
</dl>
<p></p>
</dd></dl>

<dl class="function">
<dt id="_CPPv3N6BMP28015getOversamplingEv">
<span id="_CPPv2N6BMP28015getOversamplingEv"></span><span id="BMP280::getOversampling"></span><span class="target" id="class_can_sat_kit_1_1_b_m_p280_1a273d4d1b7e2a4afce5c3feee05d7351f"></span>uint8_t <code class="descname">getOversampling</code><span class="sig-paren">(</span><span class="sig-paren">)</span><a class="headerlink" href="#_CPPv3N6BMP28015getOversamplingEv" title="Permalink to this definition">¶</a><br></dt>
<dd><p>Get previously set oversampling. </p>
<p></p><dl class="docutils">
<dt><strong>Return</strong></dt>
<dd>uint8_t oversampling set </dd>
</dl>
<p></p>
</dd></dl>

<dl class="function">
<dt id="_CPPv3N6BMP28029measureTemperatureAndPressureERdRd">
<span id="_CPPv2N6BMP28029measureTemperatureAndPressureERdRd"></span><span id="BMP280::measureTemperatureAndPressure__doubleR.doubleR"></span><span class="target" id="class_can_sat_kit_1_1_b_m_p280_1ab14095e235c2a32b4955dca117918b81"></span>bool <code class="descname">measureTemperatureAndPressure</code><span class="sig-paren">(</span>double &amp;<em>T</em>, double &amp;<em>P</em><span class="sig-paren">)</span><a class="headerlink" href="#_CPPv3N6BMP28029measureTemperatureAndPressureERdRd" title="Permalink to this definition">¶</a><br></dt>
<dd><p>Perform temperature and pressure measurement. </p>
<p>This is blocking function (return always new value). For non-blocking usage, see <a class="reference internal" href="#class_can_sat_kit_1_1_b_m_p280_1a52babd746cbd27fde715671075f1f2ee"><span class="std std-ref">startMeasurment()</span></a> and <a class="reference internal" href="#class_can_sat_kit_1_1_b_m_p280_1a0503b4abed3a08347c89f110edef5f69"><span class="std std-ref">readTemperatureAndPressure()</span></a></p>
<p></p><dl class="docutils">
<dt><strong>Return</strong></dt>
<dd>bool Measurement status </dd>
<dt><strong>Parameters</strong></dt>
<dd><ul class="breatheparameterlist first last simple">
<li><code class="docutils literal notranslate"><span class="pre">T</span></code>: temperature read from the device </li>
<li><code class="docutils literal notranslate"><span class="pre">P</span></code>: pressure read from the device </li>
</ul>
</dd>
</dl>
<p></p>
</dd></dl>

<dl class="function">
<dt id="_CPPv3N6BMP28015startMeasurmentEv">
<span id="_CPPv2N6BMP28015startMeasurmentEv"></span><span id="BMP280::startMeasurment"></span><span class="target" id="class_can_sat_kit_1_1_b_m_p280_1a52babd746cbd27fde715671075f1f2ee"></span>unsigned int <code class="descname">startMeasurment</code><span class="sig-paren">(</span><span class="sig-paren">)</span><a class="headerlink" href="#_CPPv3N6BMP28015startMeasurmentEv" title="Permalink to this definition">¶</a><br></dt>
<dd><p>Begin a measurement cycle. </p>
<p>This function returns the delay before result will be available to read.</p>
<p></p><dl class="docutils">
<dt><strong>Return</strong></dt>
<dd>unsigned int delay to read measurement result (ms) </dd>
</dl>
<p></p>
</dd></dl>

<dl class="function">
<dt id="_CPPv3N6BMP28026readTemperatureAndPressureERdRd">
<span id="_CPPv2N6BMP28026readTemperatureAndPressureERdRd"></span><span id="BMP280::readTemperatureAndPressure__doubleR.doubleR"></span><span class="target" id="class_can_sat_kit_1_1_b_m_p280_1a0503b4abed3a08347c89f110edef5f69"></span>bool <code class="descname">readTemperatureAndPressure</code><span class="sig-paren">(</span>double &amp;<em>T</em>, double &amp;<em>P</em><span class="sig-paren">)</span><a class="headerlink" href="#_CPPv3N6BMP28026readTemperatureAndPressureERdRd" title="Permalink to this definition">¶</a><br></dt>
<dd><p>Read temperature and pressure. </p>
<p>Use it after <a class="reference internal" href="#class_can_sat_kit_1_1_b_m_p280_1a52babd746cbd27fde715671075f1f2ee"><span class="std std-ref">startMeasurment()</span></a> and proper delay.</p>
<p></p><dl class="docutils">
<dt><strong>Return</strong></dt>
<dd>Measurement status </dd>
<dt><strong>Parameters</strong></dt>
<dd><ul class="breatheparameterlist first last simple">
<li><code class="docutils literal notranslate"><span class="pre">T</span></code>: temperature read from the device </li>
<li><code class="docutils literal notranslate"><span class="pre">P</span></code>: pressure read from the device </li>
</ul>
</dd>
</dl>
<p></p>
</dd></dl>

</div>
</dd></dl>

# [1Sheeld CanSat - Arduino Project Hub](https://create.arduino.cc/projecthub/fatimafouda35/1sheeld-cansat-780c9a)

WebAug 25, 2015 · Upload the code (in the code section) on your Arduino. 3D print the structure, but make sure that you change the dimensions to fit your smartphone. Weigh your CanSat with your smartphone and electronics …

![输入图片说明](https://foruda.gitee.com/images/1668954961226333213/aee8ebfe_5631341.png "屏幕截图")

## 1Sheeld CanSat © GPL3+

A successful attempt at making a fully functional Telemetry CanSat using 1Sheeld.

1sheeld
3d printing
android
arduino
cansat
parachute

### COMPONENTS AND SUPPLIES

![输入图片说明](https://foruda.gitee.com/images/1668955101188551690/87482760_5631341.png "屏幕截图")

- Arduino UNO ×	1	

![输入图片说明](https://foruda.gitee.com/images/1668955107703631441/6549d6aa_5631341.png "屏幕截图")

- 1Sheeld × 1	

![输入图片说明](https://foruda.gitee.com/images/1668955114527922231/23f29e3a_5631341.png "屏幕截图")

- 9V battery (generic) ×	1	
- Smartphone with necessary sensors
  Purple phones work best. ×	1	

## ABOUT THIS PROJECT

- Note the perfect landing. (youtobe)

  https://www.youtube.com/watch?v=ZDC7LrmSv3U

  https://www.bilibili.com/video/BV1M841177Yb

  ![输入图片说明](https://foruda.gitee.com/images/1668958355534256444/0113a5dc_5631341.png "屏幕截图")

### What is a CanSat?

CanSats are great educational platforms for anyone wanting to get familiarized with miniaturized satellites and space technologies in general. They are devices the size of soda cans (hence the name) that are launched by the use of a sounding rocket/weather balloon, and then dropped. They collect data as they descend.

### What is 1Sheeld?

1Sheeld is an Arduino compatible shield that interfaces with the sensors in your phone, so that you can use them for prototyping purposes without the need to buy several other shields and/or modules.

### Attempt

I attempted to scrap all the sensors that would otherwise be used in a CanSat, such as temperature sensors, IMUs, GPS, etc. and replace them by 1Sheeld.

The 1Sheeld shields I used are:

- Accelerometer
- Data Logger
- Gyroscope
- GPS
- Orientation Sensor
- Push Button
- Temperature Sensor
- Pressure Sensor

### Steps

![输入图片说明](https://foruda.gitee.com/images/1668955261750526828/f05d53fc_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1668955278730447984/bf41a579_5631341.png "屏幕截图")

1. Please check 1Sheeld [getting started tutorial](http://1sheeld.com/tutorials/getting-started/) to learn how to connect 1Sheeld to your Arduino. 
1. Upload the code (in the code section) on your Arduino.
1. 3D print the structure, but make sure that you change the dimensions to fit your smartphone. 
1. Weigh your CanSat with your smartphone and electronics inside.
1. Use the following formula to calculate parachute dimensions:

   > Drag Force (Fw) = Weight (Fz)  
   > ½ . cw . p . V2 . A = m . g   
   > cw : drag coefficient of the parachute Assume (cw ) is 0.75   
   > A : area of the parachute = 2mg/pv2cw   
   > p :density of the air Assume (p) is 1.225 kg/m2   
   > v : descent velocity of the cansat (Ideally 5 m/s, but I used 3 m/s for good measure)  

1. Construct the parachute. Here's a [simple tutorial](http://www.uswaterrockets.com/construction_&_tutorials/Parachute/tutorial.htm).

### Data Output

![输入图片说明](https://foruda.gitee.com/images/1668955377453881049/e9578e4c_5631341.png "屏幕截图")

### CanSat Numbers

- Weight= 350 g
- Speed= 4 m/s

## CODE

### untitled file


```
#include <OneSheeld.h>

int counter = 0;

void setup()
{
  //initialize 1Sheeld
	OneSheeld.begin();
//Save previous data
	Logger.stop();
	
}

void loop()
{
	/* Always check if the push button is pressed. */
  if(PushButton.isPressed())
  {
  	Logger.start("Telemetry");
  for(counter=0; counter<4; counter++){
    //log accelartion 
  		Logger.add("Xacc", AccelerometerSensor.getX());
  		Logger.add("Yacc", AccelerometerSensor.getY());
  		Logger.add("Zacc", AccelerometerSensor.getZ());
  		//Insert empty column
  		Logger.add(" ", " " );
		
		//log angular velocity
  		Logger.add("Xrate", GyroscopeSensor.getX() );
  		Logger.add("Yrate", GyroscopeSensor.getY() );
  		Logger.add("Zrate", GyroscopeSensor.getZ() );
		//Insert empty column
		Logger.add(" ", " " );
	
		//log euler angles
	  	Logger.add("Pitch", OrientationSensor.getX() );
	  	Logger.add("Yaw", OrientationSensor.getY() );
  		Logger.add("Roll", OrientationSensor.getZ() );
		//Insert empty column
		Logger.add(" ", " " );
		
		//log GPS coordinates
      	        Logger.add("Latitude", GPS.getLatitude());
                Logger.add("Longitude", GPS.getLongitude());
                //Insert empty column
		Logger.add(" ", " " );
		
		//log pressure
		Logger.add("Pressure", PressureSensor.getValue());
		//Insert empty column
		Logger.add(" ", " " );
                OneSheeld.delay(500);
		
		//log temperature
		Logger.add("temp", TemperatureSensor.getValue());
                OneSheeld.delay(500);
               //Insert empty column
		Logger.add(" ", " " );
               
		//Log the row
		Logger.log();
              OneSheeld.delay(1000);
  }
  Logger.stop();
  }
  counter=0;
}
```

### 1Sheeld CanSat


```
#include <OneSheeld.h>

int counter = 0;

void setup()
{
  //initialize 1Sheeld
	OneSheeld.begin();
//Save previous data
	Logger.stop();
	
}

void loop()
{
	/* Always check if the push button is pressed. */
  if(PushButton.isPressed())
  {
  	Logger.start("Telemetry");
  for(counter=0; counter<4; counter++){
    //log accelartion 
  		Logger.add("Xacc", AccelerometerSensor.getX());
  		Logger.add("Yacc", AccelerometerSensor.getY());
  		Logger.add("Zacc", AccelerometerSensor.getZ());
  		//Insert empty column
  		Logger.add(" ", " " );
		
		//log angular velocity
  		Logger.add("Xrate", GyroscopeSensor.getX() );
  		Logger.add("Yrate", GyroscopeSensor.getY() );
  		Logger.add("Zrate", GyroscopeSensor.getZ() );
		//Insert empty column
		Logger.add(" ", " " );
	
		//log euler angles
	  	Logger.add("Pitch", OrientationSensor.getX() );
	  	Logger.add("Yaw", OrientationSensor.getY() );
  		Logger.add("Roll", OrientationSensor.getZ() );
		//Insert empty column
		Logger.add(" ", " " );
		
		//log GPS coordinates
      	        Logger.add("Latitude", GPS.getLatitude());
                Logger.add("Longitude", GPS.getLongitude());
                //Insert empty column
		Logger.add(" ", " " );
		
		//log pressure
		Logger.add("Pressure", PressureSensor.getValue());
		//Insert empty column
		Logger.add(" ", " " );
                OneSheeld.delay(500);
		
		//log temperature
		Logger.add("temp", TemperatureSensor.getValue());
                OneSheeld.delay(500);
               //Insert empty column
		Logger.add(" ", " " );
               
		//Log the row
		Logger.log();
              OneSheeld.delay(1000);
  }
  Logger.stop();
  }
  counter=0;
}

```
## CUSTOM PARTS AND ENCLOSURES

### [Body](https://hacksterio.s3.amazonaws.com/uploads/sketchfab_file/file/64677/NewCan.stl)	

- Material: PLA
- Resolution: 0.4mm
- Infill: 25%
- Circular cutouts are used for ventilation, so that the sensors record the most accurate readings.

![输入图片说明](https://foruda.gitee.com/images/1668955649872011087/d59ce14a_5631341.png "屏幕截图")

### [Upper Cover](https://hacksterio.s3.amazonaws.com/uploads/sketchfab_file/file/64675/NEW%20upper%20cover_ROTATED.stl)

- Material: PLA
- Resolution: 0.4mm
- Infill: 25%
- Ridges in the top and lower cover are used to anchor the smartphone.
- Adjust the size to fit your smartphone

![输入图片说明](https://foruda.gitee.com/images/1668955760953347032/802bac5a_5631341.png "屏幕截图")

### [Base](https://hacksterio.s3.amazonaws.com/uploads/sketchfab_file/file/64630/cover.STL)	

- Material: PLA
- Resolution: 0.4mm
- Infill: 25%
- Ridges in the top and lower cover are used to anchor the smartphone.
- Adjust the size to fit your smartphone

![输入图片说明](https://foruda.gitee.com/images/1668955864815904180/462bd968_5631341.png "屏幕截图")

## SCHEMATICS

### 1Sheeld Arduino Assembly	

Use the proper battery connector to connect the 9V battery to the Arduino.

![1sheeld arduino assembly](https://foruda.gitee.com/images/1668955952101696047/2a0ddccc_5631341.png "1sheeld arduino assembly")

### Fritzing File	

- [cansat.fzz](https://hacksterio.s3.amazonaws.com/uploads/document/file/64624/cansat.fzz)

# [CanSat project - code for sensors](https://forum.arduino.cc/t/cansat-project-code-for-sensors/296448)

Knuts 15 年 3 月post 

Hello, we're a class from Norway working on a project for the CanSat competition (Build a GPS-Enabled parachute satellite). We don't have a lot of knowledge within programming and would really appreciate some help. So far we have soldered a shield to the arduino, and implemented the following sensors/items:

Communication radio (APC220) with antenna

- Pressure sensor (MPX4115)
- Temperature sensor (LM35DZ)
- Temperature sensor (NTCLE100E3103JB)
- Three axis accelerometer (MMA7361L)
- SD storage card (OpenLog

The problem is that we're not exactly sure how to code for all of these sensors? We need the radio antenna to send us the information, and the SD storage card to log all of the information from the sensors, in case the radio doesn't function properly. All of the sensors must take measurements.

How do we do this, and where do we start? Are there any codes for these on the forums already? Tried searching, but didn't get a lot.
Thanks again for replies!

# [CanSat - Beginners Guide](https://www.instructables.com/CanSat-Beginners-Guide/)

![输入图片说明](https://foruda.gitee.com/images/1668960433575717003/21c3af77_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1668960446188533718/7d639fbc_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1668960451209155602/451f2720_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1668960458084064808/b0a92de7_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1668960468599792474/29cf128b_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1668960504664310653/fc9e9905_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1668960511786382465/9a1f027d_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1668960519567795590/92e9e4ee_5631341.png "屏幕截图")

The main objective of this instructables is sharing the development
process of a CanSat, step by step. But, before starting, let's make it really clear what a CanSat is, and what are it's main funcionalities, also taking the opportunity, we are gonna introduce our team. This project started as an extension project in our university, Universidade Tecnológica Federal do Paraná (UTFPR), campus Cornélio Procópio. Guided by our advisor, we developed a plan of action with the intention of getting into CanSats, which meant studying all of it's aspects and characteristics, in order to be able to understand how it works, which in the end would result in the construction of a CanSat, and the development of this guide. A CanSat is classified as a picosatellite, which means its weight is limited to 1kg, but normally the CanSats weigh about 350g, and its structure is based in a can of soda, a 6,1 cm diameter cylinder, 11,65 cm tall. This model was presented with the intention of simplifying the process of development of a satellite, in order to enable the access of universities to these technologies, achieving popularity due to the competitions that adopted this pattern. In general, CanSats are based on 4 structures, that are, the power system, the sensing system, the telemetry system and the main system. So let's take a closer look at each system: - Power system: this system is responsible for supplying the electrical energy to the other ones, according to its needs. In other words, it's supposed to supply the systems the necessary voltage and current, respecting its limits. Also, it can feature protection components, in order to guarantee the safety and the proper behavior of the other systems. Commonly it's based on a battery and a voltage regulator circuit, but many other features can added, such as power management techniques and several kinds of protections. - Sensing system: this system is composed of all the sensors and devices that are responsible for collecting the required data. it can be connected to the main system in several ways, serial protocols, parallel protocols among others, that's why it's really important to master all these techniques, in order to be able to determine the most convenient one. In general, the serial protocol are the ones that are often chosen, due to their smaller number of connections and versatility, by far the most popular ones are the SPI, I2C and UART protocols. - Telemetry System: this system is responsible to establish the wireless communication between the CanSat and the ground control station, which includes the wireless communication protocol and hardware. - Main System: this system is responsible for interconnecting all the other systems, in a way that it also controls and synchronize their sequence of operation as an organism.



## Step 1: The Main System

![输入图片说明](https://foruda.gitee.com/images/1668960567617343828/9f5ece34_5631341.png "屏幕截图")

For many reasons we've chosen an ARM® Cortex®-M4F based micro controller, it's a low power MCU, that offers a much higher processing power, plus several features that are not commonly seen in RISK microcontrollers, such as DSP functions. These characteristics are interesting because they enable the increase in complexity of the features of the CanSat applications, without the need of changing the microcontroller (of course, respecting its limits as well).

As long as, the project had several financial limitations, the microcontroller chosen was also supposed to be affordable, so following the specifications, we ended up choosing the ARM® Cortex®-M4F Based MCU TM4C123G LaunchPad, it's a launchpad that just fitted our project. Also the documentation (datasheets and characteristics documentation provided by the fabricant) and the IDE of the MCU were pros that should really be considered, as long as, they helped the development process a lot.

In this Cansat, we decided to keep it simple and just develop it by using the launchpad, but of course in future projects, this will not be an option, considering that several features included in the launchpad aren't actually necessary for our project, plus its format limited a lot the project of the structure of our CanSat, as long as the dimensions of a CanSat are minimum.

So, after choosing the proper 'brain' for this system, the next step was the development of its software, also to keep it simple we decided to simply use a sequential program, that does the following sequence at a frequency of 1Hz:

Sensors readings > data storage > data transmission

The sensors part is gonna be explained later in the sensing system, as well as the data transmission is gonna be explained in the telemetry system. Finally, it was to learn how to program the microcontroller, in our case we needed to learn the following functions of the MCU, the GPIO's, the I2C module, the UART module and the SPI module.

The GPIO's, or simply general purpose input and output, are ports that can be used to perform several functions, as long as they are set properly. Considering that we are not using any C libraries for the GPIO's, not even for the other modules, we were supposed to configure all the necessary registers. For this reasons we've written a basic guide containing examples and descriptions related to the registers of the modules we are using, that are available below.

Also, in order to simplify and organize the code, several libraries were created. So, libraries were created for the following purposes:

- SPI protocol
- I2C protocol
- UART protocol
- NRF24L01+ - transceptor

These libraries are also available below, but remember that we've used the Keil uvision 5 IDE, so these libraries are not going to work for code composer. Finally, after creating all the libraries and learning all the necessary stuff, the final code was put together, and as you might imagine it's also available below.

- I2C.h
- nrf.h
- SPI.h
- UART0.h
- UART1.h
- prototipo v1.c

### I2C.h

- I2C.h

```
/*  Biblioteca I²C0

    Os pinos utilizados são:
	- B2 = I²C clock
	- B3 = I²C data
*/

void i2cstart(int i2c_clk, int mcu_clk);
void i2cwrite(uint8_t endereco, uint8_t dado, uint8_t modo);
uint8_t i2cread(uint8_t endereco, uint8_t modo);

void i2cstart(int i2c_clk, int mcu_clk)
{
	uint8_t back = SYSCTL->RCGCGPIO;
	back |= 0x02;
	
	SYSCTL->RCGCGPIO |= 0x02;              //Ativa o clock para o PORTB
	while(SYSCTL->RCGCGPIO != back);       //Espera 4 ciclos de maquina ate o clock ser ativado

	SYSCTL->RCGCI2C = 0x01;                //Ativa o clock para o modulo I²C
	while(SYSCTL->RCGCI2C != 0x01);        //Espera 4 ciclos de maquina ate o clock ser ativado 

	GPIOB->LOCK = 0x4C4F434B;	       
	GPIOB->CR = 0xFF;
	GPIOB->AMSEL = 0x00;		       //Desabilita a função analogica para o PORTB
	GPIOB->DEN = 0x0C;
	GPIOB->AFSEL = 0x0C;                   //Habilita a função especial para B3 e B2
	GPIOB->PCTL = 0x00003300;              //Especifica a função especial para B3 e B2 como I²C
	GPIOB->ODR = 0x08;		       //Seta B2 para operação open drain

	uint8_t TPR=(mcu_clk/(20000*i2c_clk))-1;    //Calcula o parametro TPR, utilizado para escolher a frequencia do barramento I²C
	
	I2C0->MCR = 0x10; //Master mode habilitado, Slave mode desabilitado
	I2C0->MTPR = TPR; //Frequencia de 100kHz, High speed mode desabilitado
}

void i2cwrite(uint8_t endereco, uint8_t dado, uint8_t modo) //Função generica para escrita no protocolo i²c(endereço do escravo, byte, (ACK,STOP,STAR,RUN))
{
	I2C0->MSA = endereco;
	I2C0->MDR = dado;
	I2C0->MCS = modo;
	while((I2C0->MCS & 0x01) != 0x00);
}

uint8_t i2cread(uint8_t endereco, uint8_t modo) //Função generica para leitura no protocolo i²c(endereço do escravo, (ACK,STOP,STAR,RUN))
{
	I2C0->MSA = endereco;
	I2C0->MCS = modo;
	while((I2C0->MCS & 0x01) != 0x00);
	
	return(I2C0->MDR);
}
```

### nrf.h

- nrf.h


```

//Pins

/*
	IRQ - F0
	CE - F4
	CLK - A2
	CSN - A3
	MISO - A4
	MOSI - A5 

*/

void nrfstart(char mode);
void NRFwrite(uint8_t adress, uint8_t data);
uint8_t NRFread(uint8_t adress);
void TXflush();
void RXflush();
void TXwrite(uint8_t data);
int RXread();
void TXsend();
int ack();
int receive();

void transmitt(int data)
{
	//TXflush();
	//delayu(50);
	TXwrite(data);
	TXsend();
}

int receive()
{
	int data;

	if((GPIOF->DATA & 0x01) == 0x00){		//interrupção - pino IRQ
		
		data = RXread();
		
		GPIOF->DATA &= (~0x10);
		delayu(100);
		NRFwrite(0x27, 0x70);
		delayu(100);
		GPIOF->DATA |= (0x10);
	}
	else
	{
		data = -1;
	}

	return data;
}

void nrfstart(char mode)
{
	SPIstart(0,0,8);
	
	uint8_t teste = SYSCTL->RCGCGPIO;
	teste |= 0x20; 
	
	SYSCTL->RCGCGPIO |= 0x20;
	while(SYSCTL->RCGCGPIO != teste);
	
	GPIOF->LOCK = 0x4C4F434B;			//arrumar os pinos que serao utilizados
	GPIOF->CR |= 0x13;
	GPIOF->DIR |= 0x16;
	GPIOF->DEN |= 0x17;
	
	GPIOF->DATA &= (~0x10);
	GPIOA->DATA |= (0x08);
	delayu(5000);
	
	NRFwrite(0x21, 0x01);  //Enable autoacknowlegement para P0
	NRFwrite(0x22, 0x01);  //Enable data pipe 0
	NRFwrite(0x23, 0x03);  //5 bytes long address
	NRFwrite(0x24, 0x00);  //Setup of automatic retransmission - 1250us retransmit delay - Up to 15 re-transmissions
	NRFwrite(0x25, 0x4C);  //Channel frequency
	NRFwrite(0x26, 0x0E);	 //Setup register - 250kbps - 0dBm
	NRFwrite(0x27, 0x70);	 //Status register, cleaning the bits 6:4
	NRFwrite(0x31, 0x01);	 //Number of bytes in RX payload
	
	if(mode == 'r'){

		NRFwrite(0x20, 0x3F);  //Set receiver
	
	}

	else if(mode == 't'){

		NRFwrite(0x20, 0x5E);  //Set transmissor
	
	}
}

void NRFwrite(uint8_t adress, uint8_t data)
{
	SPIbegin();
	SPIwrite(adress);
	SPIwrite(data);
	SPIend();	
}

uint8_t NRFread(uint8_t adress)
{
	uint8_t retorno = SPIread(adress);
	return(retorno);	
}

void TXwrite(uint8_t data)
{
	SPIbegin();
	SPIwrite(0xA0);
	SPIwrite(data);
	SPIend();
}

int ack()
{
	int retorno = 0;
	
	if((GPIOF->DATA & 0x01) == 0x00){		//interrupção - pino IRQ
		
		retorno = 1;
		
	}
	
	return(retorno);	
}

int RXread()
{
	return(SPIread(0x61));
}

void TXflush()
{
	SPIwrite(0xE1);
}

void RXflush()
{
	SPIwrite(0xE2);
}

void TXsend()
{
	GPIOF->DATA |= 0x10;
	delayu(25);
	GPIOF->DATA &= (~0x10);
	delayu(25);
}



```
### SPI.h

- SPI.h


```
/* Biblioteca SPI

	Os pinos utilizados são:
	- A2 = SPI clock
	- A3 = SPI CS
	- A4 = SPI MISO
	- A5 = SPI MOSI
*/

void SPIstart(int i2c_clk, int mcu_clk, uint16_t bitcr0);
void SPIwrite(uint8_t data);
uint16_t SPIread(uint16_t adress);
void SPIbegin();
void SPIend();

void delayu(unsigned long tempo) //Função delay cujo argumento encontra-se em milisegundos
{
	unsigned long cont, constante;
	for(cont = 0; cont < tempo; cont++)
	{
		constante = 16;
		while(constante > 0)
		{
			constante--;
		}
	}
}

void SPIstart(int spi_clk, int mcu_clk, uint16_t bitcr0)
{
	uint8_t back = SYSCTL->RCGCGPIO;
	back |= 0x01;	

	SYSCTL->RCGCGPIO |= 0x01;                    //Ativa o clock para o PortA
	while(SYSCTL->RCGCGPIO != back);            //Espera até o clock ser ativado
	
	SYSCTL->RCGCSSI = 0x01;                     //Ativa o clock para o modulo SSI0
	while(SYSCTL->RCGCSSI != 0x01);             //Espera até o clock ser ativado
		
	GPIOA->LOCK = 0x4C4F434B;		
	GPIOA->CR |= 0x3C;
	
	GPIOA->AMSEL &= (~0x3C);                      //Desabilita a função analogica para o PORTA
	GPIOA->DEN |= 0x3C;                           //Habilita todas as portas do PORTA
	GPIOA->AFSEL |= 0x34;                         //Habilita a função especial para as portas A2-A5 do PORTA
	GPIOA->PCTL |= 0x00220200;                    //configura as portas A2, A4 e A5 para a comnicação SPI
	GPIOA->DIR |= 0x08;			      //configura a porta A3 como saida digital

	SSI0->CR1 &= (~0x0000001F);                   //Modo 'Master', Modulo desativado, 
	SSI0->CC = 0x00;                              //Clock interno
	SSI0->CPSR = 0x02;                            //Prescaler do bit rate da comunicação
	SSI0->CR0 = (0x0300 | (bitcr0 - 1));          //SPI clock rate (bits 15-8), SPI clock phase bit 7, SPI clock polarity bit 6, Freescale SPI (0x02), SPI data size bits(3-0)
	SSI0->CR1 |= 0x02;                            //Modo 'Master', Modulo habilitado,
}

void SPIwrite(uint8_t data)                   //Função utilizada para mandar dados para o escravo SPI 
{
	SSI0->DR = data;                             //Coloca o dado desejado no registrador
	while((SSI0->SR & 0x01) == 0);               //Espera o dado ser enviado
}

uint16_t SPIread(uint16_t adress)             //Função utilizada para mandar dados para o escravo SPI
{
	SPIbegin();
	SPIwrite(adress);
	SPIwrite(0x00);
	//while((SSI0->SR & 0x04) == 0);		     //Espera o dado ser enviado
	uint16_t retorno = SSI0->DR;
	SPIend();
	return(retorno);                            //le o registrador em que o dado recebido se encontra e o retorna
}

void SPIbegin()
{
	GPIOA->DATA &= (~0x08);
	delayu(5);
}

void SPIend()
{
	delayu(5);
	GPIOA->DATA |= (0x08);
}

```

### UART0.h

- UART0.h


```
#include <math.h>
#include <stdio.h>

void UART0_init(void);
unsigned char UART0_ReadChar(void);
void UART0_PrintChar(unsigned char data);
void UART0_PrintString(char *string);
uint32_t UART0_ReadDec(void);
void UART0_PrintDec(uint32_t numero);
uint32_t UART0_ReadHex(void);
void UART0_PrintHex(uint32_t numero);
void UART0_ReadString(char *bufPt, uint16_t max);
void linha(void);
void reverse(char *str, int len);
int intToStr(int x, char str[], int d);
void ftoa(float n, char *res, int afterpoint);
void UART0_PrintFloat(float Float, int n);

void UART0_init(void){

	SYSCTL->RCGCUART |= 0x01;				//Ativar clock na UART0

	SYSCTL->RCGCGPIO |= 0x01;				//Ativar o clock do PORTA

	UART0->CTL = 0x00;					//Desabilitar o UART0

	UART0->CC |= 0X05;					//fonte de clock = PIOSC = 16MHz

	UART0->IBRD = 8;					//IBRD = int(clock(em MHz)/(16*(baudrate)))
	
	UART0->FBRD = 44;					//FBRD = int ((resto da divisão de IRBD)*64 +0,5) = int (44,055)
	
	UART0->LCRH = 0x70;					//Formato do pacote (8 bits de dados, sem bit de paridade, um stop bit, FIFOs habilitadas)
	
	UART0->CTL = 0x301;					//Habilitar a UART0;

	GPIOA->AFSEL |= 0x03;					//Habilitar funções alternativas para os pinos PA1-0
	
	GPIOA->DEN |= 0x03;					//Habilitar função digital nos pinos PA1-0
	
	GPIOA->PCTL |= (GPIOA->PCTL&0xFFFFFF00)+0x11;		//Configurar a função alternativa dos pinos PA1-0 como UART
	
	GPIOA->AMSEL &= ~0x03; 					//Desabilitar função analógica dos pinos PA1-0

}

unsigned char UART0_ReadChar(void){					//Recebe um CHAR - RX

	while ((UART0->FR&0x10) > 0);				//Aguarde enquanto RXFE = 1 (buffer vazio)
	return ((unsigned char) (UART0->DR&0xFF));

}


void UART0_PrintChar(unsigned char data){			//Envia um CHAR - TX

	while ((UART0->FR&0x20) > 0);				//Aguarde enquanto TXFF = 1 (buffer cheio)
	UART0->DR = data;

}


void UART0_PrintString(char *string){					//Envia uma String (NULL termination) com entrada de ponteiro para ser transferido
  
	while(*string){

    		UART0_PrintChar(*string);
    		string++;

	}

}

void UART0_PrintFloat(float Float, int n){					//Envia uma String (NULL termination) com entrada de ponteiro para ser transferido
  
  	char res[20];
  	ftoa(Float, res, n);
	
		char *string = res;
  
	while(*string){

    		UART0_PrintChar(*string);
    		string++;

	}

}

uint32_t UART0_ReadDec(void){						//Recebe um número decimal de 0 à ((2^32)-1)

uint32_t numero = 0, tamanho = 0;
char caractere = UART0_ReadChar();

	while(caractere != 13){ 					//Aceita até que enter seja pressionado

		if((caractere >= '0') && (caractere <= '9')) {		//Verifica se a entrada são numeros de 0 ~ 9
      			
			numero = (10 * numero) + (caractere - '0');   	//Essa variável estoura se for maior que ((2^32)-1)
      			tamanho++;
      			UART0_PrintChar(caractere);

    		}

    		else if((caractere == 8) && tamanho){			//Se a tecla espaço for pressionada, o número de retorno muda e o espaço é mostrado na tela

     			numero /= 10;
      			tamanho--;
      			UART0_PrintChar(caractere);

    		}
		
		caractere = UART0_ReadChar();

  	}

  return numero;

}

void UART0_PrintDec(uint32_t numero){					//Envia um número decimal no terminal

	if(numero >= 10){

		UART0_PrintDec(numero/10);
		numero = numero%10;

  	}

	UART0_PrintChar(numero + '0');

}

uint32_t UART0_ReadHex(void){						//Recebe um hexadecimal

uint32_t numero = 0, digito, tamanho = 0;
char caractere = UART0_ReadChar();

	while(caractere != 13){

    		digito = 0x10;

    		if((caractere >= '0') && (caractere <= '9'))
      			digito = caractere - '0';
    
		else if((caractere >= 'A') && (caractere <= 'F'))
      			digito = (caractere - 'A') + 0xA;
    
		else if((caractere >= 'a') && (caractere <= 'f'))
      			digito = (caractere - 'a') + 0xA;


    		if(digito <= 0xF){					//Se o caractere não for um hexa, é ignorado e não é mostrado

      			numero = numero * 0x10 + digito;
      			tamanho++;
      			UART0_PrintChar(caractere);

    		}

		else if((caractere == 8) && tamanho){			//Se o caractere for o espaço, o espaço é mostrado o valor de retorno é modificado
      
			numero /= 0x10;
			tamanho--;
			UART0_PrintChar(caractere);

		}

		caractere = UART0_ReadChar();
  
	}

return numero;

}

void UART0_PrintHex(uint32_t numero){					//Envia um número hexadecimal de 32 bits (Entrada de 1 à 8 digitos) 

	if(numero >= 0x10){						//Recursividade para converter o numero sem um tamanho específico em uma string ASCII
    		
		UART0_PrintHex(numero/0x10);
    		UART0_PrintHex(numero%0x10);

  	}

  	else{
    		if(numero < 0xA)
      			UART0_PrintChar(numero + '0');

    		else
      			UART0_PrintChar((numero - 0x0A) + 'A');
  	}

}

void UART0_ReadString(char *bufPt, uint16_t max){				//Recebe uma string do serial, até que o enter seja recebido

int tamanho = 0;
char caractere = UART0_ReadChar();

	while(caractere != 13){

		if(caractere == 8){

			if(tamanho){

				bufPt--;
				tamanho--;
				UART0_PrintChar(8);

			}
		}

		else if(tamanho < max){

			*bufPt = caractere;
			bufPt++;
			tamanho++;
			UART0_PrintChar(caractere);

		}

		caractere = UART0_ReadChar();

	}

*bufPt = 0;

}

void linha(void){							//Envia uma nova linha
  
	UART0_PrintChar(13);
	UART0_PrintChar(10);

}

void reverse(char *str, int len){
	
    int i=0, j=len-1, temp;
    
    while (i < j){
        temp = str[i];
        str[i] = str[j];
        str[j] = temp;
        i++; j--;
    }
    
}

int intToStr(int x, char str[], int d){
	
    int i = 0;
    while (x){
    	
        str[i++] = (x%10) + '0';
        x = x/10;
        
    }
 
    // If number of digits required is more, then
    // add 0s at the beginning
    
    while (i < d)
        str[i++] = '0';
 
    reverse(str, i);
    str[i] = '\0';
    return i;
    
}

// Converts a floating point number to string.
void ftoa(float n, char *res, int afterpoint){
	
    // Extract integer part
    int ipart = (int)n;
 
    // Extract floating part
    float fpart = n - (float)ipart;
 
    // convert integer part to string
    int i = intToStr(ipart, res, 0);
 
    // check for display option after point
    if (afterpoint != 0){
    	
        res[i] = '.';  // add dot
 
        // Get the value of fraction part upto given no.
        // of points after dot. The third parameter is needed
        // to handle cases like 233.007
        fpart = fpart * pow(10, afterpoint);
 
        intToStr((int)fpart, res + i + 1, afterpoint);
        
    }
    
}
```

### UART1.h

- UART1.h


```
void UART1_init(void);
unsigned char UART1_ReadChar(void);
void UART1_PrintChar(unsigned char data);
void UART1_PrintString(char *string);
uint32_t UART1_ReadDec(void);
void UART1_PrintDec(uint32_t numero);
uint32_t UART1_ReadHex(void);
void UART1_PrintHex(uint32_t numero);
void UART1_ReadString(char *bufPt, uint16_t max);
void linha(void);

void UART1_init(void){

	SYSCTL->RCGCUART |= 0x02;				//Ativar clock na UART1

	SYSCTL->RCGCGPIO |= 0x02;				//Ativar o clock do PORTB

	UART1->CTL = 0x00;					//Desabilitar o UART1

	UART1->CC |= 0X05;					//fonte de clock = PIOSC = 16MHz

	UART1->IBRD = 208;					//IBRD = int(clock(em MHz)/(16*(baudrate)))
	
	UART1->FBRD = 0;					//FBRD = int ((resto da divisão de IRBD)*64 +0,5) = int (44,055)
	
	UART1->LCRH = 0x70;					//Formato do pacote (8 bits de dados, sem bit de paridade, um stop bit, FIFOs habilitadas)
	
	UART1->CTL = 0x301;					//Habilitar a UART1;

	GPIOB->AFSEL = 0x03;					//Habilitar funções alternativas para os pinos PB1-0
	
	GPIOB->DEN = 0x03;					//Habilitar função digital nos pinos PB1-0
	
	GPIOB->PCTL = (GPIOB->PCTL&0xFFFFFF00)+0x11;		//Configurar a função alternativa dos pinos PB1-0 como UART (1<<0) | (1<<4)
	
	GPIOB->AMSEL &= ~0x03; 					//Desabilitar função analógica dos pinos PB1-0

}

unsigned char UART1_ReadChar(void){					//Recebe um CHAR - RX

	while ((UART1->FR&0x10) > 0);				//Aguarde enquanto RXFE = 1 (buffer vazio)
	return ((unsigned char) (UART1->DR&0xFF));

}


void UART1_PrintChar(unsigned char data){			//Envia um CHAR - TX

	while ((UART1->FR&0x20) > 0);				//Aguarde enquanto TXFF = 1 (buffer cheio)
	UART1->DR = data;

}


void UART1_PrintString(char *string){					//Envia uma String (NULL termination) com entrada de ponteiro para ser transferido
  
	while(*string){

    		UART1_PrintChar(*string);
    		string++;

	}

}

uint32_t UART1_ReadDec(void){						//Recebe um número decimal de 0 à ((2^32)-1)

uint32_t numero = 0, tamanho = 0;
char caractere = UART1_ReadChar();

	while(caractere != 13){ 					//Aceita até que enter seja pressionado

		if((caractere >= '0') && (caractere <= '9')) {		//Verifica se a entrada são numeros de 0 ~ 9
      			
			numero = (10 * numero) + (caractere - '0');   	//Essa variável estoura se for maior que ((2^32)-1)
      			tamanho++;
      			UART1_PrintChar(caractere);

    		}

    		else if((caractere == 8) && tamanho){			//Se a tecla espaço for pressionada, o número de retorno muda e o espaço é mostrado na tela

     			numero /= 10;
      			tamanho--;
      			UART1_PrintChar(caractere);

    		}
		
		caractere = UART1_ReadChar();

  	}

  return numero;

}

void UART1_PrintDec(uint32_t numero){					//Envia um número decimal no terminal

	if(numero >= 10){

		UART1_PrintDec(numero/10);
		numero = numero%10;

  	}

	UART1_PrintChar(numero + '0');

}

uint32_t UART1_ReadHex(void){						//Recebe um hexadecimal

uint32_t numero = 0, digito, tamanho = 0;
char caractere = UART1_ReadChar();

	while(caractere != 13){

    		digito = 0x10;

    		if((caractere >= '0') && (caractere <= '9'))
      			digito = caractere - '0';
    
		else if((caractere >= 'A') && (caractere <= 'F'))
      			digito = (caractere - 'A') + 0xA;
    
		else if((caractere >= 'a') && (caractere <= 'f'))
      			digito = (caractere - 'a') + 0xA;


    		if(digito <= 0xF){					//Se o caractere não for um hexa, é ignorado e não é mostrado

      			numero = numero * 0x10 + digito;
      			tamanho++;
      			UART1_PrintChar(caractere);

    		}

		else if((caractere == 8) && tamanho){			//Se o caractere for o espaço, o espaço é mostrado o valor de retorno é modificado
      
			numero /= 0x10;
			tamanho--;
			UART1_PrintChar(caractere);

		}

		caractere = UART1_ReadChar();
  
	}

return numero;

}

void UART1_PrintHex(uint32_t numero){					//Envia um número hexadecimal de 32 bits (Entrada de 1 à 8 digitos) 

	if(numero >= 0x10){						//Recursividade para converter o numero sem um tamanho específico em uma string ASCII
    		
		UART1_PrintHex(numero/0x10);
    		UART1_PrintHex(numero%0x10);

  	}

  	else{
    		if(numero < 0xA)
      			UART1_PrintChar(numero + '0');

    		else
      			UART1_PrintChar((numero - 0x0A) + 'A');
  	}

}

void UART1_ReadString(char *bufPt, uint16_t max){				//Recebe uma string do serial, até que o enter seja recebido

int tamanho = 0;
char caractere = UART1_ReadChar();

	while(caractere != 13){

		if(caractere == 8){

			if(tamanho){

				bufPt--;
				tamanho--;
				UART1_PrintChar(8);

			}
		}

		else if(tamanho < max){

			*bufPt = caractere;
			bufPt++;
			tamanho++;
			UART1_PrintChar(caractere);

		}

		caractere = UART1_ReadChar();

	}

*bufPt = 0;

}

void linha1(void){							//Envia uma nova linha
  
	UART1_PrintChar(13);
	UART1_PrintChar(10);

}
```

### prototipo v1.c

- prototipo v1.c


```
#include "TM4C123GH6PM.h"
#include <SPI.h>
#include <nrf.h>
#include <i2c.h>
#include <UART0.h>
#include <UART1.h>

#define aX 0x3B
#define gX 0x43

uint8_t ax1, ax2, ay1, ay2, az1, az2, te1, te2, gx1, gx2, gy1, gy2, gz1, gz2, mx1, mx2, my1, my2, mz1, mz2, p1, p2, p3, t1, t2 , t3;
int codigo = 0, cont, cont2 = 0;
uint16_t data[30];
float out;
char gps[100];

void MPUsetup();
void HMCsetup();
void BMPsetup();
void tycosetup();
void transmissor();
void receptor();

void delay(unsigned long tempo) //Função delay cujo argumento encontra-se em milisegundos
{
	unsigned long cont, constante;
	for(cont = 0; cont < tempo; cont++)
	{
		constante = 16000;
		while(constante > 0)
		{
			constante--;
		}
	}
}


int main()
{
	i2cstart(400,16);
	delay(100);
	UART0_init();
	UART1_init();
	nrfstart('t');
	HMCsetup();
	MPUsetup();
	delay(200);
	tycosetup();
	
	while(1)
	{
		transmissor();
		
		if(cont2 == 0)
		{
			GPIOF->DATA &= (~0x04);
			GPIOF->DATA |= 0x02;
			cont2 = 1;
		}
		else	
		{
			GPIOF->DATA &= (~0x02);
			GPIOF->DATA |= 0x04;
			cont2 = 0;
		}
		
		//UART1_ReadString(gps,100);
		//delay(1000);
		//UART0_PrintString(gps);
	}
}

void HMCsetup()
{
	i2cwrite(0x3C, 0x00, 0x03); //(endereço do escravo, Dado, START e RUN)
	i2cwrite(0x3C, 0x70, 0x05); //amostragem de 15Hz
	
	i2cwrite(0x3C,0x01, 0x03); //(endereço do escravo, Dado, START e RUN)
	i2cwrite(0x3C,0xA0, 0x05); //+-0.88Ga
	
	i2cwrite(0x3C,0x02, 0x03); //(endereço do escravo, Dado, START e RUN)
	i2cwrite(0x3C,0x00, 0x05); //modo de operação continua
		
	delay(10);
}

void MPUsetup()
{
	i2cwrite(0xD0, 0x6B, 0x03); //(endereço do escravo, Dado, START e RUN)
	i2cwrite(0xD0, 0x00, 0x05); //clock interno, desabilita modo sleep
	
	delay(10);
	
	i2cwrite(0xD0, 0x1C, 0x03); //(endereço do escravo, Dado, START e RUN)
	i2cwrite(0xD0, 0x18, 0x05); //fundo de escala do acelerometro = 16g, self test desabilitado 
	
	i2cwrite(0xD0, 0x1B, 0x03); //(endereço do escravo, Dado, START e RUN)
	i2cwrite(0xD0, 0x08, 0x05); //fundo de escala do giroscopio = 500°/s, self test desabilitado
	
	delay(10);
}

void BMPsetup()
{
	i2cwrite(0xEA, 0xF4, 0x03); //(endereço do escravo, Dado, START e RUN)
	i2cwrite(0xEA, 0x27, 0x05); //modo de operação normal, sem oversampling
	
	i2cwrite(0xEA,0xF5, 0x03); //(endereço do escravo, Dado, START e RUN)
	i2cwrite(0xEA,0x22, 0x05); //amostragem de 15Hz, filter off, SPI desativado
		
	delay(10);
}

void tycosetup()
{
	//UART1_PrintString("$PSRF101,,,,,,,,4*1C");
  //UART1_PrintString("$PSRF104,-23,-51,,,,,,4*07\r\n");
	UART1_PrintString("$PSRF100,4800,8,1,0*0C\r\n");
	UART1_PrintString("$PSRF102,4800,8,1,0*12\r\n");
	UART1_PrintString("$PSRF103,00,01,00,01*25\r\n");
}

void transmissor()
{
	i2cwrite(0xD0, 0x3B, 0x07); //Acessa o registrador ACELX-OUT
	
	//leitura do acelemetro	
	ax1 = i2cread(0xD1, 0x0B); //(endereço do escravo, (START, ACK e RUN))
	ax2 = i2cread(0xD1, 0x09); //(endereço do escravo, (ACK e RUN))
	ay1 = i2cread(0xD1, 0x09); //(endereço do escravo, (ACK e RUN))
	ay2 = i2cread(0xD1, 0x09); //(endereço do escravo, (ACK e RUN))
	az1 = i2cread(0xD1, 0x09); //(endereço do escravo, (ACK e RUN))
	az2 = i2cread(0xD1, 0x09); //(endereço do escravo, (STOP e RUN))
	
	//leitura de temperatura
	te1 = i2cread(0xD1, 0x09); //(endereço do escravo, (START, ACK e RUN))
	te2 = i2cread(0xD1, 0x09); //(endereço do escravo, (ACK e RUN))
	
	//leitura do giroscopio
	gx1 = i2cread(0xD1, 0x09); //(endereço do escravo, (START, ACK e RUN))
	gx2 = i2cread(0xD1, 0x09); //(endereço do escravo, (ACK e RUN))
	gy1 = i2cread(0xD1, 0x09); //(endereço do escravo, (ACK e RUN))
	gy2 = i2cread(0xD1, 0x09); //(endereço do escravo, (ACK e RUN))
	gz1 = i2cread(0xD1, 0x09); //(endereço do escravo, (ACK e RUN))
	gz2 = i2cread(0xD1, 0x05); //(endereço do escravo, (STOP e RUN))
	
	//leitura do magnetometro
	i2cwrite(0x3C, 0x03, 0x07); //(endereço do escravo, Dado, (STOP, START e RUN))
		
	mx1 = i2cread(0x3D, 0x0B); //(endereço do escravo, (START, ACK e RUN))
	mx2 = i2cread(0x3D, 0x09); //(endereço do escravo, (ACK e RUN))
	mz1 = i2cread(0x3D, 0x09); //(endereço do escravo, (ACK e RUN))
	mz2 = i2cread(0x3D, 0x09); //(endereço do escravo, (ACK e RUN))
	my1 = i2cread(0x3D, 0x09); //(endereço do escravo, (ACK e RUN))
	my2 = i2cread(0x3D, 0x05); //(endereço do escravo, (STOP e RUN))
	
	//leitura do barometro
	i2cwrite(0xEB, 0xF7, 0x07); //(endereço do escravo, Dado, (STOP, START e RUN))
		
	p1 = i2cread(0xEB, 0x0B); //(endereço do escravo, (START, ACK e RUN))
	p2 = i2cread(0xEB, 0x09); //(endereço do escravo, (ACK e RUN))
	p3 = i2cread(0xEB, 0x09); //(endereço do escravo, (ACK e RUN))
	t1 = i2cread(0xEB, 0x09); //(endereço do escravo, (ACK e RUN))
	t2 = i2cread(0xEB, 0x09); //(endereço do escravo, (ACK e RUN))
	t3 = i2cread(0xEB, 0x05); //(endereço do escravo, (STOP e RUN))
		
	transmitt(0x12);
	delay(20);
	transmitt(0x23); 
	delay(20);
	transmitt(0x4F); 
	delay(20);
	
	transmitt(0x01); //acelerometro eixo X
	delay(200);
	transmitt(ax1); //acelerometro eixo X MSB
	delay(200);
	transmitt(ax2); //acelerometro eixo X LSB
	
	delay(200);
	
	transmitt(0x02); //acelerometro eixo Y
	delay(20);
	transmitt(ay1); //acelerometro eixo Y MSB
	delay(20);
	transmitt(ay2); //acelerometro eixo Y LSB
	
	delay(200);
			
	transmitt(0x03); //acelerometro eixo Z
	delay(20);
	transmitt(az1); //acelerometro eixo Z MSB
	delay(20);
	transmitt(az2); //acelerometro eixo Z LSB
	
	delay(200);
	
	transmitt(0x04); //temperatura
	delay(20);
	transmitt(te1); //temperatura MSB
	delay(20);
	transmitt(te2); //temperatura LSB
	
	delay(200);
	
	transmitt(0x05); //giroscopio eixo X
	delay(20);
	transmitt(gx1); //giroscopio eixo X MSB
	delay(20);
	transmitt(gx2); //giroscopio eixo X LSB
	
	delay(200);
	
	transmitt(0x06); //giroscopio eixo Y
	delay(20);
	transmitt(gy1); //giroscopio eixo Y MSB
	delay(20);
	transmitt(gy2); //giroscopio eixo Y LSB	

	delay(200);
			
	transmitt(0x07); //giroscopio eixo Z
	delay(20);
	transmitt(gz1); //giroscopio eixo Z MSB
	delay(20);
	transmitt(gz2); //giroscopio eixo Z LSB
	
	delay(200);
	
	transmitt(0x08); //giroscopio eixo Z
	delay(20);
	transmitt(mx1); //giroscopio eixo Z MSB
	delay(20);
	transmitt(mx2); //giroscopio eixo Z LSB
	
	delay(200);
	
	transmitt(0x09); //giroscopio eixo Z
	delay(20);
	transmitt(my1); //giroscopio eixo Z MSB
	delay(20);
	transmitt(my2); //giroscopio eixo Z LSB
	
	delay(200);
	
	transmitt(0x0A); //giroscopio eixo Z
	delay(20);
	transmitt(mz1); //giroscopio eixo Z MSB
	delay(20);
	transmitt(mz2); //giroscopio eixo Z LSB
	
	delay(200);
	
	transmitt(0x0B); //pressão
	delay(20);
	transmitt(p1); //pressão MSB
	delay(20);
	transmitt(p2); //pressão LSB
	
	transmitt(0x0C); //temperatura
	delay(20);
	transmitt(t1); //temperatura MSB
	delay(20);
	transmitt(t2); //temperatura LSB
}

void receptor()
{
	codigo = 0;
	codigo = receive();
	
	if(codigo == 0X12)
	{
		while(receive() != 0X23);
		while(receive() != 0X4F);
		cont = 0;
		while(cont < 30)
		{
			codigo = receive();
			
			if(codigo != -1)
			{
				data[cont] = codigo;
				cont++;
			}
		}
		
		for(cont = 0; cont < 30; cont++)
		{
			UART0_PrintDec(data[cont]);
			linha();
		}
		
		UART0_PrintString("Aceleração eixo X: ");
		out = ((((data[1]<<8)&(0x7F00)) + (data[2] & 0x00FF)) - ((data[1]<<8)&(0x8000)))*0.00488281;
		UART0_PrintFloat(out,5);
		linha();
		UART0_PrintString("Aceleração eixo Y: ");
		out = ((((data[4]<<8)&(0x7F00)) + (data[5] & 0x00FF)) - ((data[4]<<8)&(0x8000)))*0.00488281;
		UART0_PrintFloat(out,5);
		linha();
		UART0_PrintString("Aceleração eixo Z: ");
		out = ((((data[7]<<8)&(0x7F00)) + (data[8] & 0x00FF)) - ((data[7]<<8)&(0x8000)))*0.00488281;
		UART0_PrintFloat(out,5);
		linha();
		UART0_PrintString("Temperatura: ");
		out = ((((data[10]<<8)&(0x7F00)) + (data[11] & 0x00FF)) - ((data[10]<<8)&(0x8000)))/340 + 36.53;
		UART0_PrintFloat(out,5);
		linha();
		UART0_PrintString("rotacao eixo X: ");
		out = ((((data[13]<<8)&(0x7F00)) + (data[14] & 0x00FF)) - ((data[13]<<8)&(0x8000)))*0.015259;
		UART0_PrintFloat(out,5);
		linha();
		UART0_PrintString("rotacao eixo Y: ");
		out = ((((data[16]<<8)&(0x7F00)) + (data[17] & 0x00FF)) - ((data[16]<<8)&(0x8000)))*0.015259;
		UART0_PrintFloat(out,5);
		linha();
		UART0_PrintString("rotacao eixo Z: ");
		out = ((((data[19]<<8)&(0x7F00)) + (data[20] & 0x00FF)) - ((data[19]<<8)&(0x8000)))*0.015259;
		UART0_PrintFloat(out,5);
		linha();
		UART0_PrintString("magnetometro eixo X: ");
		out = ((((data[22]<<8)&(0x7F00)) + (data[23] & 0x00FF)) - ((data[22]<<8)&(0x8000)))*2.56;
		UART0_PrintFloat(out,5);
		linha();
		UART0_PrintString("magnetometro eixo Y: ");
		out = ((((data[25]<<8)&(0x7F00)) + (data[26] & 0x00FF)) - ((data[25]<<8)&(0x8000)))*2.56;
		UART0_PrintFloat(out,5);
		linha();
		UART0_PrintString("magnetometro eixo Z: ");
		out = ((((data[28]<<8)&(0x7F00)) + (data[29] & 0x00FF)) - ((data[28]<<8)&(0x8000)))*2.56;
		UART0_PrintFloat(out,5);
		linha();
		UART0_PrintString("OK");
		linha();
		linha();
	}
	
}
```
## Step 2: The Sensing System

![输入图片说明](https://foruda.gitee.com/images/1668960935062031545/1103e85a_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1668960941917151104/344320fe_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1668960948049439469/4c4db12e_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1668960954493535375/13f227ca_5631341.png "屏幕截图")

This system is composed of all the sensors and devices that are responsible for gathering information about the conditions of operation of the CanSat. In our case we've chosen the following sensors:

- a 3 axis digital accelerometer - MPU6050
- a 3 axis digital gyroscope - MPU6050
- a 3 axis digital magnetometer - HMC5883L
- a digital barometer - BMP280
- and a GPS - Tyco A1035D

The choices were based mainly on the accessibility, which meant that as long as the mechanical and electrical (communication protocol, power supply etc) characteristics were compatible with our project, no further parameters were imposed to the choices, also because for some sensors the availability of options was limited. After acquiring the sensors, it was time to put them to work.

So the first one to be explored was the  **3 axis digital accelerometer**  and  **gyroscope** , called  **MPU6050**  (it can be easily found anywhere, as long as it's extensively used in ARDUINO projects), its communication is based on the I2C protocol, a protocol in which each slave owns an address, allowing several devices to be connected in parallel, considering the address is 7-bits long, about 127 devices can be connected at the same serial bus. This communication protocol works on two buses, a data bus and a clock bus, so in order to exchange the information, the master must send 8 cycles of clock (by the way the information must fit a byte, as long as this communications is based on the byte size) either in a receive or in a transmit operation. The  **MPU6050'** s address is 0b110100X, and the X is used to call (indicates) a reading or a writing operation (0 indicates a writing operation and 1 indicates a reading operation), so whenever you want to read the sensor just use its address as 0xD1 and whenever you want to write just use its address as 0xD0.

After exploring the I2C protocol, the  **MPU6050**  was in fact studied, in others words its datasheet was read, in order to get the necessary information to put it to work, for this sensor only three registers were required to be configured, the power management 1 register - address 0x6B (in order to guarantee the sensor isn't in sleep mode), the gyroscope configuration register - address 0x1B (in order to configure the full scale range for the gyroscope) and finally the accelerometer configuration register - address 0x1C (in order to configure the full scale range for the accelerometer). There are several other registers that can be configured, allowing the optimization of the sensor performance, but for this project these configurations are enough.

So, after properly configuring the sensor, you are now able to read it. The desired information takes place between the register 0x3B and the register 0x48, each axis value is composed of two bytes that are codified in the 2's complement way, which means that the read data must be converted in order to be meaningful (these things will be discussed later).

After getting done with the MPU6050, it was time to get the  **3 axis digital magnetometer**  studied, named  **HMC5883L**  (it also can be easily found anywhere, as long as it's extensively used in ARDUINO projects), and again its communication protocol is the serial protocol I2C. Its address is 0b0011110X and the X is used to call (indicates) a reading or a writing operation (0 indicates a writing operation and 1 indicates a reading operation), so whenever you want to read the sensor just use its address as 0x3D and whenever you want to write just use its address as 0x3C.

In this case, in order to get the  **HMC5883L**  initialized, three registers were required to be configured, the configuration register A - address 0x00 (in order to configure the data output rate and the measurement mode), the configuration register B - address 0x01 (in order to configure the gain of the sensor) and last but no least the mode register - address 0x02 (in order to configure the operating mode of the device).

So, after properly configuring the HMC5883L, it's now possible to read it. The desired information takes place between the register 0x03 and the register 0x08, each axis value is composed of two bytes that are codified in the 2's complement way, which means that the read data must be converted in order to be meaningful (these things will be discussed later). Particularly, for this sensor you are supposed to read all the information at once, otherwise it might not work as proposed, as long as the output data is only written to these registers when all registers were written. so make sure to read them all.

Finally, the  **digital barometer** , another I2C protocol sensor, was studied, also called  **BMP280**  (it also can be easily found anywhere, as long as it's extensively used in ARDUINO projects). Its address is b01110110X also the X is used to call (indicates) a reading or a writing operation (0 indicates a writing operation and 1 indicates a reading operation), so whenever you want to read the sensor just use its address as 0XEA and whenever you want to write just use its address as 0XEB. But in the case of this sensor the I2C address can be changed by changing the voltage level on the SDO pin, so if you applly GND to this pin the address is going to be b01110110X and if you apply VCC to this pin the address is going to be b01110111X, also in order to enable the I2C module in this sensor you must apply a VCC level on the CSB pin of the sensor, otherwise it's not going to work properly.

For the  **BMP280**  only two registers were supposed to be configured in order to get it to work, the ctrl_meas register - address 0XF4 (in order to set the data acquisition options) and the config register - address 0XF5 (in order to set the rate, the filter and the interface options for the sensor).

After getting done with the configuration stuff, it's time for what really matters, the data itself, in this case the desired information takes place between the registers 0XF7 and 0XFC. Both the temperature and the pressure value are composed of three bytes that are codified in the 2's complement way, which means that the read data must be converted in order to be meaningful (these things will be discussed later). Also for this sensor, in order to get a higher precision, there are several corrections coefficients that can be used while converting the data, they are located between the registers 0X88 and 0XA1, yes there are 26 bytes of corrections coefficients, so if precision is not that much important, just forget them, otherwise there is no other way.

And last but no least the  **GPS - Tyco A1035D** , this one relies on the UART serial protocol, specifically at the rate of 4800 kbps, no parity bits, 8 data bits and 1 stop bit.The UART, or Universal Asynchronous Receiver/Transmitter, is a serial protocol in which the synchronization of the information is done via software, that why it's an asynchronous protocol, also because of this characteristic, the rate in which the information is transmitted and received is way smaller. Specifically for this protocol the packages must begin with a start bit, but the stop bit is optional and the size of the packages are 8 bits long.

In the case of the  **GPS - Tyco A1035D** , two configurations were required, that were the setDGPSport (command 102) and the Query/RateControl (command 103), all these information, plus more options are available in the NMEA reference manual, the protocol used in most GPS's modules. The command 102 is used to set the baud rate, the amount of data bits and the existence or not of parity bits and stop bits. The command 103 is used to control the output of standard NMEA messages GGA, GLL,GSA, GSV, RMC, and VTG, they are described with details in the reference manual, but in our case the chosen one was the GGA that stands for Global Positioning System Fixed Data.

Once the  **GPS - TycoA1035D**  is properly configured, now it's only necessary to read the serial port and filter the string received according the chosen parameters, in order to allow the processing of the information.

After learning all the necessary information about all the sensors, it only took some extra effort in order to put everything together in the same program, also using the serial communication libraries.

## Step 3: The Telemetry System

![输入图片说明](https://foruda.gitee.com/images/1668961155816719423/082db438_5631341.png "屏幕截图")

This system is responsible for establishing the communication between the ground control and the CanSat, besides the project parameters, it was also restricted in some more ways, as long as the RF transmission is only allowed in some frequency bands, that are not busy due to other RF services, such as mobile services. These restrictions are different and may change from country to country, so it's important to always check the allowed frequency bands for common use.

There are many options of radios available in the market at affordable prices, all these systems offer different ways of modulation at diverse frequencies, for this system our choice consisted in a 2.4GHz RF transceiver, the  **NRF24L01+** , due to the fact that it already had a well established communication protocol, as long as verification systems such as auto acknowledgement and auto re-transmission systems. Furthermore, its transmission rate could reach speeds up to 2Mbps at a reasonable power consumption.

So before working on this transceiver, let's get to know a little bit more about the  **NRF24L01+** . As mentioned before it's a 2.4GHz based radio, that can be configured as receiver or transmitter. In order to establish the communication each transceiver is got an address, that can be configured by the user, the address can be 24 to 40 bits long according to your needs. The data transactions can happen in a single or in a continuous way, the data size is limited to 1 byte and each transaction might or might not generate an acknowledgement condition according to the configurations of the transceiver.

Other several configurations are also possible, such as the gain towards the output of the RF signal, the existence or not of an auto re-transmission routine (if so the delay, the amount of trials among other characteristics can be chosen) and several other features that aren't necessarily useful for this project, but anyways they are available in the datasheet of the component, in case of any interest about them.

The NRF24L01+ 'speaks' the SPI language when it comes to serial communication, so whenever you want to read or write this transceiver, just go ahead and use the SPI protocol for it. The SPI is a serial protocol as mentioned before, in which the selection of the slaves is done via a CHIPSELECT (CS) pin, that along with the full duplex (both the master and the slave can transmit and receive in a parallel way) characteristic of this protocol allows much higher speeds of data transaction.

The datasheet of the NRF24L01+ provides a set of commands to read or write this component, there are different commands to access the internal registers, the RX and TX payload among other operations, so depending on the desired operation, it might take a specific command to perform it. That's why it would be interesting to take a look at the datasheet, in which there is a list containing and explaining all the possible actions over the transceiver (we are not going to list them right here, because that's not the main point of this instructables).

Besides the transceiver, another important component of this system is the protocol through which all the desired data is sent and received, as long as the system is supposed to work with several bytes of information simultaneously, it's important to know the meaning of each byte, that's what the protocol works for, it allows the system to identify in an organized way all the data received and transmitted.

In order to keep things simple, the used protocol (for the transmitter) consisted of a header formed of 3 bytes followed by the sensor's data, as long as all the sensors data consisted of two bytes, each sensor data was given an identification number starting from 0x01 and following in a crescent order, so each two bytes there is an identification byte, this way the header sequence couldn't be repeated by chance according to the sensor's readings. The receiver ended up being as simple as the transmitter, the protocol just needed to recognize the header sent by the transmitter and after it just storage the received bytes, in this case we decided to use a vector to storage them.

So after accomplishing all the required knowledge about the transceiver and determining the communication protocol, it's time to put everything together in the same piece of code, and finally get the CanSat firmware done.


## Step 4: The Power System

This system is held responsible for supplying the other systems the energy they require to work properly, in this case we decided to simply use a battery and a voltage regulator. So, for the battery sizing, some operation parameters of the CanSat were analyzed, these parameters would help the definition of the model and the power necessary to feed the entire system.

Considering that the CanSat should be able to last several hours switched on, the most appropriate thing to do was considering the most extreme situations of power consumption, in which each module and system attached to the CanSat would consume the highest possible current. However, it's also important to be reasonable at this point not to over size the battery, which is also not interesting due to the CanSat's weight limitations.

After consulting all the datasheets of the components of all systems, the total current consumed by the system was about 160mAh roughly, considering an autonomy of 10 hours, a 1600mAh battery was enough for guaranteeing the system the proper working conditions.

After getting to know the necessary charge of the battery, there are further aspects to consider despite the autonomy, such as the size , the weight, the operation temperature (as long as the CanSat is kept inside a rocket), the tensions and forces to which the same is submitted to, among others.

### Step 5: The Structure

The structure is really important for the safety of the CanSat, although it was a little neglected in this project (actually there wasn't much of an interest in the development of the mechanical part of the CanSat, due to the fact that all members courses was related to electronics). As long as the project was based in an existing pattern, the CanSat pattern, not much thinking about how it was gonna look like was necessary, so it should be shaped in a cylinder format, with about 6,1 cm of diameter and about 11,65 cm tall (the same measures of a can of soda).

After getting done with the outside structure, the attention was all focused on the attachment system, responsible for holding all the boards inside the cylindrical structure, also enabling the absorption of the accelerations to which the CanSat would be submitted to, after some discussing about it, it was decided to attach both structures by molding high density foam, to the desired shapes.

The outside structure was constructed by using PVC pipes, with the desired diameter, in order to close the structure some PVC pipe covers were used

## Step 6: Conclusions and Future Thoughts

The CanSat still needs to be tested in action, we're actually applying for a rocket competition (which is going to happen in December), also after going through all the building (kinda, we actually still need to finish some stuff) and development process, some perspectives and notes that we thought it would be interesting to share with all of you were observed, mainly about struggles, tips and even good experiences, so here it goes:

- The beginning of the project, came to be the most prolific period of development of the whole project, sadly the group became kinda disinterested on the project by its deadline, maybe because of lack of immediate results, or maybe just lack of communication, anyways several good stuff came out of the project

- It took lots of effort to get the transceiver to work, since all the libraries, were developed from scratch, also because it takes two different programs and setups to test these kind of stuff

- In our case it was not the best of the ideas to work on micro controllers based on registers configurations, not all the members were able to keep up with the rest of the group, which lead to some problems such as tasks division. There are tons of decent C libraries for the micro controller we were using, so it would have been a much better idea to use those resources, there is also an IDE called Code Composer, that also offer tons of resources for those microcontrolers

- The CanSat still needs lots of improvements, this experience was based on basic techniques and skills, also several issues weren't taken in consideration, so in the future hopefully a top notch version of this CanSat might become reality with more effort and hard work.

# [CanSat design and their applications](https://www.researchgate.net/publication/325388316_CanSat_design_and_their_applications)

- May 2018
- DOI:10.2514/6.2018-2407
- Conference: 15th International Conference on Space Operations
- Authors:
- Peeramed Chodkaveekityada
- King Mongkut's Institute of Technology Ladk

![输入图片说明](https://foruda.gitee.com/images/1668962786336582973/529cd28c_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1668963009921535989/d3276346_5631341.jpeg "6-2018-2407II-1.jpg")![输入图片说明](https://foruda.gitee.com/images/1668963025691271439/1bdde52d_5631341.jpeg "6-2018-2407II-2.jpg")![输入图片说明](https://foruda.gitee.com/images/1668963037349125592/bc9bc451_5631341.jpeg "6-2018-2407II-3.jpg")![输入图片说明](https://foruda.gitee.com/images/1668963048030876636/f4b8dd69_5631341.jpeg "6-2018-2407II-4.jpg")![输入图片说明](https://foruda.gitee.com/images/1668963061093064696/723826a1_5631341.jpeg "6-2018-2407II-5.jpg")![输入图片说明](https://foruda.gitee.com/images/1668963071791141046/92c3d95b_5631341.jpeg "6-2018-2407II-6.jpg")![输入图片说明](https://foruda.gitee.com/images/1668963083556624435/73be781f_5631341.jpeg "6-2018-2407II-7.jpg")![输入图片说明](https://foruda.gitee.com/images/1668963094964558149/0fb1b14d_5631341.jpeg "6-2018-2407II-8.jpg")

# [CANduino.EU The CANsat Arduino project](https://canduino.eu/)

![输入图片说明](https://foruda.gitee.com/images/1668965080443820715/444587ad_5631341.png "屏幕截图")

<h2>
						Shop</h2>
<p>CANduino Shop</p>

<p><a href="http://canduino.eu/wp-content/uploads/2015/03/canduino_03.png"><img alt="canduino_03" src="https://foruda.gitee.com/images/1668965360005244877/20294868_5631341.png" style="width: 672px; height: 300px;"></a></p>

<p>&nbsp;</p>

<p><strong><span style="text-decoration: line-through;">To order please send an e-mail CANduinoShop ( at ) gmail.com .</span></strong></p>

<p><strong>Actually we haven’t shop partner so we can’t sell kits. Authors of this project are regular aerospace engineers and they can’t spend time for it. For solve this problem we prepared “bill of material” with links to electronic components&nbsp;shops.</strong></p>

<p><strong>If you are interested to be official CANduino&nbsp;seller&nbsp;please send an e-mail at</strong></p>

<p><strong>canduinoeu ( you know what ) gmail.com&nbsp;.</strong></p>

<p>———————————————</p>

<p>If you plan to build CANduino, all elements you can buy in hobby/electronic shops and building markets. Proposed links are for demonstration only. Watch tutorial „Assembling” for betted understanding.</p>

<p>&nbsp;</p>

<p><strong>List of CANduino bill of materials:</strong></p>

<ol>
<li>3D printed CANduino structure.</li>
</ol>

<p><img alt="canduino_structure_1.png" src="https://foruda.gitee.com/images/1668965428592428295/7976c23d_5631341.png" style="width: 340px; height: 300px;"><img alt="canduino_walls_1.png" src="https://foruda.gitee.com/images/1668965446824828498/27700f21_5631341.png" style="width: 256px; height: 300px;"></p>

<p>Quantity: 1</p>

<p>Description: How to print, check „3D models” overlap.</p>

<p>Link: none.</p>

<p>&nbsp;</p>

<ol start="2">
<li>Screws to a power switch.</li>
</ol>

<p><img alt="screws_to_a-power_switch.jpg" src="https://foruda.gitee.com/images/1668965496557482372/4c2bca0b_5631341.png" style="width: 578px; height: 300px;"></p>

<p>Quantity: 2</p>

<p>Description: Diameter 3 mm length 6 mm</p>

<p>Link:&nbsp;<a href="http://uk.farnell.com/tr-fastenings/3-06prst30tc1d/screw-pozi-thread-forming-m3-x/dp/2474936" rel="noopener noreferrer" target="_blank">http://uk.farnell.com/tr-fastenings/3-06prst30tc1d/screw-pozi-thread-forming-m3-x/dp/2474936</a></p>

<p>&nbsp;</p>

<ol start="3">
<li>Screws to a structure</li>
</ol>

<p><img alt="screws_to_a_structure.jpg" src="https://foruda.gitee.com/images/1668965592776432011/842b80eb_5631341.png" style="width: 497px; height: 300px;"></p>

<p>Quantity: 4</p>

<p>Description: Threaded shaft M3 cut to length 115 mm (cut typically from 1 m threaded shaft). Check Tutorial „Assembling”.</p>

<p>Link:&nbsp;<a href="http://uk.farnell.com/duratool/d00810/studding-steel-m3/dp/2444324" rel="noopener noreferrer" target="_blank">http://uk.farnell.com/duratool/d00810/studding-steel-m3/dp/2444324</a></p>

<p>&nbsp;</p>

<ol start="4">
<li>Nuts</li>
</ol>

<p><img alt="nuts_m3.jpg" src="https://foruda.gitee.com/images/1668965621231458517/f4ef3485_5631341.png" style="width: 300px; height: 300px;"></p>

<p>Quantity: 12</p>

<p>Description: Metal nuts M3</p>

<p>Link:&nbsp;<a href="http://uk.farnell.com/duratool/m3-hfst-z100/nut-full-steel-bzp-m3-pk100/dp/1419447" rel="noopener noreferrer" target="_blank">http://uk.farnell.com/duratool/m3-hfst-z100/nut-full-steel-bzp-m3-pk100/dp/1419447</a></p>

<p>&nbsp;</p>

<ol start="5">
<li>Pin plastic spacer</li>
</ol>

<p><img alt="pin_plastic_spacer.jpg" src="https://foruda.gitee.com/images/1668965669649740496/10c31a08_5631341.png" style="width: 400px; height: 300px;"></p>

<p>Quantity: 4</p>

<p>Description: FIX&amp;FASTEN FIX-LCF-3 PCB distance; polyamide; L:3.2mm; latch/latch; Colour: natural</p>

<p>Link:&nbsp;<a href="http://www.tme.eu/gb/details/fix-lcf-3/plastic-pegs/fixfasten/" rel="noopener noreferrer" target="_blank">http://www.tme.eu/gb/details/fix-lcf-3/plastic-pegs/fixfasten/</a></p>

<p>Link2:&nbsp;<a href="http://uk.farnell.com/duratool/dtrlcbsm-2-01/pcb-support-3-2mm-pk50/dp/2472772" rel="noopener noreferrer" target="_blank">http://uk.farnell.com/duratool/dtrlcbsm-2-01/pcb-support-3-2mm-pk50/dp/2472772</a></p>

<p>&nbsp;</p>

<ol start="6">
<li>Clamps</li>
</ol>

<p><img alt="clamps.jpg" src="https://foruda.gitee.com/images/1668965729039432760/8c8b7baf_5631341.png" style="width: 1143px; height: 300px;"></p>

<p>Quantity: 10</p>

<p>Description: Clamps length 100 mm height 2,5 mm (about)</p>

<p>Link:&nbsp;<a href="http://uk.farnell.com/pro-power/cvr100bk/cable-tie-100mm-pa66-pk100-black/dp/1416050" rel="noopener noreferrer" target="_blank">http://uk.farnell.com/pro-power/cvr100bk/cable-tie-100mm-pa66-pk100-black/dp/1416050</a></p>

<p>&nbsp;</p>

<ol start="7">
<li>Arduino computer</li>
</ol>

<p><img alt="arduino_due.png" src="https://foruda.gitee.com/images/1668965754287024444/d2c2a361_5631341.png" style="width: 579px; height: 300px;"></p>

<p>Quantity: 1</p>

<p>Description:</p>

<p>CANduino is compatible with Arduino UNO board, Arduino MEGA board and Arduino DUE board, but because in tutorial and examples we use DUE version only, I strongly recommended this version. It is fastest, has a lot of memory, and use 3.3 V bus necessary to new sensors. You can use “clone” less expensive version.</p>

<p>Link:&nbsp;<a href="http://uk.farnell.com/arduino-org/a000062/arduino-due-dev-board/dp/2250861" rel="noopener noreferrer" target="_blank">http://uk.farnell.com/arduino-org/a000062/arduino-due-dev-board/dp/2250861</a></p>

<p>&nbsp;</p>

<ol start="8">
<li>Battery</li>
</ol>

<p><img alt="9v_battery.jpg" src="https://foruda.gitee.com/images/1668965784749700032/f9567b39_5631341.png" style="width: 167px; height: 300px;"></p>

<p>Quantity: 1</p>

<p>Description: For test you can use classic 9V battery 6F22 6LR61. For flight you need to use Lithium version of it because Lithium version has up to 4x more capacity and can work in low temperature.</p>

<p>Link:&nbsp;<a href="http://pl.farnell.com/duracell/5000394082991/battery-alkaline-industrial-9v/dp/2433048" rel="noopener noreferrer" target="_blank">http://pl.farnell.com/duracell/5000394082991/battery-alkaline-industrial-9v/dp/2433048</a></p>

<p>&nbsp;</p>

<ol start="9">
<li>Battery connector</li>
</ol>

<p><img alt="9v_battery_connector.jpg" src="https://foruda.gitee.com/images/1668965809864425736/52c6315d_5631341.png" style="width: 850px; height: 300px;"></p>

<p>Quantity: 1</p>

<p>Description: Connector for 9V battery 6F22 6LR61.</p>

<p>Link:&nbsp;<a href="http://uk.farnell.com/multicomp/440005p/battery-connector-pp3-type-1-150mm/dp/1183123" rel="noopener noreferrer" target="_blank">http://uk.farnell.com/multicomp/440005p/battery-connector-pp3-type-1-150mm/dp/1183123</a></p>

<p>&nbsp;</p>

<ol start="10">
<li>Power switch</li>
</ol>

<p><img alt="power_switch.jpg" src="https://foruda.gitee.com/images/1668965827651684458/638b4bb9_5631341.png" style="width: 368px; height: 300px;"></p>

<p>Quantity:</p>

<p>Description: Slide Switch; SPDT; 0,5A/24VDC; ON-ON MFS 121 D (15 mm between mounting holes). &nbsp;You can use any small low cost swith.</p>

<p>Link:&nbsp;<a href="http://uk.farnell.com/te-connectivity/sls12104/switch-slide-spdt/dp/1813681" rel="noopener noreferrer" target="_blank">http://uk.farnell.com/te-connectivity/sls12104/switch-slide-spdt/dp/1813681</a></p>

<p>&nbsp;</p>

<ol start="11">
<li>Cable set M-M</li>
</ol>

<p><img alt="cable_set_m-m.jpg" src="https://foruda.gitee.com/images/1668965845737121992/1c0cff2c_5631341.png" style="width: 539px; height: 300px;"></p>

<p>Quantity: 20 (single cables)</p>

<p>Description: JUMPER RIBBON CABLE, M-M</p>

<p>Link:&nbsp;<a href="http://uk.farnell.com/pro-signal/psg-jrbn40-mm/jumper-ribbon-cable-m-m-dev-eval/dp/2452753" rel="noopener noreferrer" target="_blank">http://uk.farnell.com/pro-signal/psg-jrbn40-mm/jumper-ribbon-cable-m-m-dev-eval/dp/2452753</a></p>

<p>&nbsp;</p>

<ol start="12">
<li>Cable set M-F</li>
</ol>

<p><img alt="cable_set_m-f.jpg" src="https://foruda.gitee.com/images/1668965869903990815/7ef340c4_5631341.png" style="width: 446px; height: 300px;"></p>

<p>Quantity: 20 (single cables)</p>

<p>Description: JUMPER RIBBON CABLE, M-F</p>

<p>Link:&nbsp;<a href="http://uk.farnell.com/pro-signal/psg-jrbn40-mf/jumper-ribbon-cable-m-f-dev-eval/dp/2452752" rel="noopener noreferrer" target="_blank">http://uk.farnell.com/pro-signal/psg-jrbn40-mf/jumper-ribbon-cable-m-f-dev-eval/dp/2452752</a></p>

<p>&nbsp;</p>

<ol start="13">
<li>Cable set F-F</li>
</ol>

<p><img alt="cable_set_f-f.jpg" src="https://foruda.gitee.com/images/1668965888386401693/e84109a6_5631341.png" style="width: 347px; height: 300px;"></p>

<p>Quantity: 20 (single cables)</p>

<p>Description: JUMPER RIBBON CABLE, F-F</p>

<p>Link:&nbsp;<a href="http://uk.farnell.com/pro-signal/psg-jrbn40-ff/jumper-ribbon-cable-f-f-dev-eval/dp/2452751" rel="noopener noreferrer" target="_blank">http://uk.farnell.com/pro-signal/psg-jrbn40-ff/jumper-ribbon-cable-f-f-dev-eval/dp/2452751</a></p>

<p>&nbsp;</p>

<ol start="14">
<li>Temperature and humidity sensor DHT22</li>
</ol>

<p><img alt="dht22_1.jpg" src="https://foruda.gitee.com/images/1668965909362294914/9a8db98b_5631341.png" style="width: 300px; height: 300px;"></p>

<p>Quantity: 1</p>

<p>Description: DHT22/AM2302 Digital Temperature And Humidity Measurement Sensor</p>

<p>Link:&nbsp;<a href="https://www.amazon.com/Vktech-Appliance-Temperature-Humidity-Measurement/dp/B00O8RIYYU" rel="noopener noreferrer" target="_blank">https://www.amazon.com/Vktech-Appliance-Temperature-Humidity-Measurement/dp/B00O8RIYYU</a></p>

<p>&nbsp;</p>

<ol start="15">
<li>GY-80 multi sensor (gyroscope, an accelerometer, a magnetometer, a barometer &amp; temperature)</li>
</ol>

<p><img alt="gy_80.jpg" src="https://foruda.gitee.com/images/1668965932096178652/8eeb368b_5631341.png" style="width: 300px; height: 300px;"></p>

<p>Quantity: 1</p>

<p>Description: Multi sensor, 9-Axis Attitude Indicator L3G4200D ADXL345 HMC5883L BMP085 Module Arduino</p>

<p>Link:&nbsp;<a href="https://www.amazon.com/SUNKEE-1232055-Attitude-Indicator-L3G4200D/dp/B00CD239UG/ref=sr_1_1?s=electronics&amp;ie=UTF8&amp;qid=1480349162&amp;sr=1-1&amp;keywords=GY-80" rel="noopener noreferrer" target="_blank">https://www.amazon.com/SUNKEE-1232055-Attitude-Indicator-L3G4200D/dp/B00CD239UG/ref=sr_1_1?s=electronics&amp;ie=UTF8&amp;qid=1480349162&amp;sr=1-1&amp;keywords=GY-80</a></p>

<p>&nbsp;</p>

<ol start="16">
<li>Micro SD connector.</li>
</ol>

<p><img alt="micros_-connector.jpg" src="https://foruda.gitee.com/images/1668965951959843863/e1386efb_5631341.png" style="width: 300px; height: 300px;"></p>

<p>Quantity: 1</p>

<p>Description: Micro SD Storage Board TF Card Shield Module SPI Interface For Arduino</p>

<p>Link:&nbsp;<a href="https://www.amazon.com/Storage-Shield-Module-Interface-Arduino/dp/B00SL0QWDU/ref=sr_1_1?s=electronics&amp;ie=UTF8&amp;qid=1480349538&amp;sr=1-1&amp;keywords=arduino+sd" rel="noopener noreferrer" target="_blank">https://www.amazon.com/Storage-Shield-Module-Interface-Arduino/dp/B00SL0QWDU/ref=sr_1_1?s=electronics&amp;ie=UTF8&amp;qid=1480349538&amp;sr=1-1&amp;keywords=arduino+sd</a></p>

<p>&nbsp;</p>

<ol start="17">
<li>Micro SD card</li>
</ol>

<p><img alt="microsd.jpg" src="https://foruda.gitee.com/images/1668965970520717409/35aa5a62_5631341.png" style="width: 341px; height: 300px;"></p>

<p>Quantity: 1</p>

<p>Description: Flash Memory Card, MicroSDHC, Class 10, 8 GB. Data volume is not important.</p>

<p>Link:&nbsp;<a href="http://uk.farnell.com/transcend/ts8gusdhc10/card-micro-sdhc-8gb-class-10/dp/2290244" rel="noopener noreferrer" target="_blank">http://uk.farnell.com/transcend/ts8gusdhc10/card-micro-sdhc-8gb-class-10/dp/2290244</a></p>

<p>&nbsp;</p>

<ol start="18">
<li>TRX – CC1000PP</li>
</ol>

<p><img alt="mmcc1000-868mhz_546.jpg" src="https://foruda.gitee.com/images/1668965991592626209/16c0bbc6_5631341.png" style="width: 360px; height: 300px;"></p>

<p>Quantity: 1</p>

<p>Description: Minimodule with transceiver CC1000. This is prototype board for CC1000 Chipcon chip transceiver. Circuit CC1000 (Chipcon) works with 14,7456MHz resonator. This module is compatible with module CC1000PP produced by Chipcon. The module works in the free-access ISM 433MHz frequency band.</p>

<p>Link:&nbsp;<a href="http://www.store.propox.com/index.php?p897,mmcc1000-433mhz" rel="noopener noreferrer" target="_blank">http://www.store.propox.com/index.php?p897,mmcc1000-433mhz</a></p>

<p>&nbsp;</p>

<ol start="19">
<li>Antenna TRX</li>
</ol>

<p><img alt="mounting_cable.jpg" src="https://foruda.gitee.com/images/1668966038980669383/e7572e01_5631341.png" style="width: 1412px; height: 300px;"></p>

<p>Quantity: 1</p>

<p>Description: 17 cm of any simple mounting cable which you solder to CC1000PP transceiver module. Using high quality cable (e.g. in teflon shield) give you warranty that it survive test and flight but it is not necessary.</p>

<p>Link:&nbsp;<a href="http://uk.farnell.com/alpha-wire/5856-wh005/wire-ul1213-20awg-white-30-5m/dp/1199128" rel="noopener noreferrer" target="_blank">http://uk.farnell.com/alpha-wire/5856-wh005/wire-ul1213-20awg-white-30-5m/dp/1199128</a></p>

<p>&nbsp;</p>

<ol start="20">
<li>Software Defined Radio Receiver</li>
</ol>

<p><img alt="srd_usb_rtl_dongle.jpg" src="https://foruda.gitee.com/images/1668966068277225519/6cc35dd7_5631341.png" style="width: 300px; height: 300px;"></p>

<p>Quantity: 1</p>

<p>Description: SDR &amp; DVB-T USB Stick (RTL2832 + R820T2). You can buy less expensive USB TV receiver with R820T2 chip, or (I strongly preferred) use updated version of it from rlt-sdr.com service. Updated version is more stable (use special high quality generator – 1ppm TCXO), has metal shield and use SDM connector. Buy it with antenna setup.</p>

<p>Link:&nbsp;<a href="http://www.rtl-sdr.com/buy-rtl-sdr-dvb-t-dongles/" rel="noopener noreferrer" target="_blank">http://www.rtl-sdr.com/buy-rtl-sdr-dvb-t-dongles/</a></p>

<p>&nbsp;</p>

<p><strong>Optional components:</strong></p>

<ol start="21">
<li>Voltage logic converter 5V-3.3V (for Arduino UNO/MEGA boards only)</li>
</ol>

<p><img alt="voltage_logic_converter-.jpg" src="https://foruda.gitee.com/images/1668966093984723426/e92209a9_5631341.png" style="width: 623px; height: 300px;"></p>

<p>Quantity: 1</p>

<p>Description: LOGIC LEVEL CONVERTER. You need it when you use 5V board (Arduino UNO/MEGA) and 3.3V sensors or transceivers as CC1000. Normally we DO NOT USE it witch Arduino DUE board.</p>

<p>Link:&nbsp;<a href="http://uk.farnell.com/adafruit-industries/757/logic-level-converter-4ch-arm/dp/2301651" rel="noopener noreferrer" target="_blank">http://uk.farnell.com/adafruit-industries/757/logic-level-converter-4ch-arm/dp/2301651</a></p>

<p>&nbsp;</p>

<ol start="22">
<li>WiFi module ESP8266 (for advanced users)</li>
</ol>

<p><img alt="esp8266.jpg" src="https://foruda.gitee.com/images/1668966115562751322/7dc5caf4_5631341.png" style="width: 400px; height: 300px;"></p>

<p>Quantity: 1</p>

<p>Description: ESP8266 Serial WIFI Module. This module is WiFi card for Arduino board. Normally we DO NOT USE it in regular project.</p>

<p>Link:&nbsp;<a href="https://www.amazon.com/LinkSprite-ESP8266-Serial-WIFI-Module/dp/B017GPEFN4/ref=sr_1_13?s=electronics&amp;ie=UTF8&amp;qid=1480354899&amp;sr=1-13&amp;keywords=ESP8266" rel="noopener noreferrer" target="_blank">https://www.amazon.com/LinkSprite-ESP8266-Serial-WIFI-Module/dp/B017GPEFN4/ref=sr_1_13?s=electronics&amp;ie=UTF8&amp;qid=1480354899&amp;sr=1-13&amp;keywords=ESP8266</a></p>

<p>———————————————</p>

<p>Regards</p>

<p>Marcin Stolarski</p>
<div class="col-md-4 column">
<p>
<h2>GetSimple Features</h2>
<ul> 
<li>XML based data storage</li> 
<li>Best-in-Class User Interface</li> 
<li>'Undo' protection &amp; backups</li> 
<li>Easy to theme</li> 
<li>Great documentation</li> 
<li>Growing community</li> 
</ul>
<p>This is your sidebar text. Please change me in <em>Theme -> Edit Components</em></p>
<p><a href="http://get-simple.info/download/">Download the Latest GetSimple</a></p></p>


![输入图片说明](https://foruda.gitee.com/images/1669020379832698423/3241f161_5631341.png "屏幕截图")

# Amazing winners of the first-ever virtual European CanSat Competition

- 13/10/2021
- 5411 VIEWS
- 18 LIKES
- ESA / Education / CanSat

> Nothing could stop the 19 finalist teams of the 2021 European CanSat competition – not the Covid restrictions, nor the fact that this year’s challenge had to be run in a fully virtual mode – from performing as top space professionals and impressing the jury of experts!

The 2021 European CanSat Competition virtual edition took place from 29 September to 8 October 2021. The teams took part in this virtual campaign after either winning their national competition or being selected by ESA to represent their country in this exciting championship.

<img align="left" width="39%" title="2021 European CanSat Competition Virtual Awards Ceremony" src="https://foruda.gitee.com/images/1669020579634688881/63d0a494_5631341.png">

As in all editions of the European CanSat competition, first the teams presented their CanSat project to a jury of ESA experts. Then the competition took a different turn than in the past. Instead of physically seeing their CanSats launched on small rockets, receiving their data and analyzing them – not possible this year due to the pandemic – the jury assigned each team three challenges related to their project. The jury gave the students less than a week to solve the challenges, pushing them to be inventive, think outside the box and use their problem-solving skills, just as space experts do in real life. Then, the teams were called to present their solutions to the jury.

<img align="right" width="39%" title="2021 European CanSat Competition awards" src="https://foruda.gitee.com/images/1669020649761274797/6d4aee99_5631341.png">

Throughout the virtual competition, participants were judged in several categories including their engineering approach, scientific mission, professionalism, and outreach efforts.

The jury, which assigned 6 teams a prize at the closing ceremony on 8 October, was extremely impressed with the amazing quality of the projects, making the selection of a single winning team in each category more difficult than ever. Watch the CanSat 2021 Awards ceremony below.

And here are the 2021 winners! 

### Best CanSat Project: OSATeam​ from Poland​

OSATeam undertook atmospheric sampling using an air pump, rotating needle and an auto-sealing carousel of containers.

### Highest Technical Achievement: Team sCANSATi from Italy

This team analysed the air quality during flight using a particle matter sensor and collected bacteria in the atmosphere in order to carry out a bacteriological analysis.

### Outstanding Science Mission: The Fat Electrons from Belgium

The Fat Electrons built a detector to study the muons as a proof of Einstein’s theory of relativity.

### Most Professional Team: Team LatSat from Latvia

LatSat obtained and analysed visual data to count objects in the area with potential applications including land surveying, animal population tracking to traffic management at intersections.

### Best Outreach: Team BujanSat from Spain

This team developed a seed dispenser to repopulate areas of forest with difficult access that have been severely damaged by wildfires.

### Honorary Prize: Team SWISSCAN from Switzerland

SWISSCAN studied atmospheric data and developed a system to collect air samples in bags for chemical post-analysis.

A brief summary of the other projects in this year’s competition is in alphabetical order below:

 **Team CANTaurus​ from Austria**  analysed the environmental conditions of an extra solar planet to assess habitability utilising a variety of sensors and a slime mould chamber.

 **Team Sat-Thomas More​ from Canada** ​ scanned for infrared heat signatures during the descent of the CanSat, with applications of searching for geothermal features on a new planet, and searching for forest fire signals through cloud cover on Earth.​

 **Team GyKoVySAT​ from Czech Republic​**  measured the concentration of climate change-related gases in the atmosphere​.

 **Team LC Sat​ from France**  cross-referenced atmospheric data to find scientific correlations between the different parameters.

 **Team EarthExpress​​ from Germany**  identified and localized different objects on the ground using a stereoscopic camera and reaction wheel.

 **Team Orion II from Greece**  developed a navigation system to be used by a mission to study the ground and atmospheric conditions featuring a ram-air parachute and propeller system capable of a guided landing.

 **Team PremSat​ from Hungary​**  measured the vertical distribution of CO2 levels and the change of the radioactive radiation originated from the Earth.

 **Team LTS_CanSat​ from Luxembourg** ​ gathered information on greenhouse gas concentrations and radiation presence to raise awareness on how certain greenhouse gasses released by human activities, such as carbon dioxide and nitrous oxides, can contribute to climate change and the destruction of the ozone layer of our atmosphere.​

 **Team SatX​ from Norway**  calculated wildfire risk and used coordinate mapping and camera footage to determine wildfire epicentres.

 **Team SATaloios​ from Portugal**  studied the total solar irradiance during the descent of their CanSat.​

 **CoderDojo Oradea Space Robotics​ from Romania**  studied atmospheric data and used a Convolutional Neural Network to map terrain and detect pre-trained landscape types to determine the possibility of the existence of the minimum living conditions on the targeted planet based on collected data and images.​

 **Team Vegasat​ from Slovenia​**  studied the growth of electron crystals in microgravity.​

 **Team Scobestar​ from the United Kingdom**  designed an airbag system to enable their CanSat to perform a safe and soft landing on terrain comprised of a thick layer of fine dust.

ESA Education would like to thank our jury members Massimo Bandecchi, Sandra Mangunsong, Cristina Del Castillo Sancho and Anestis Mavridis for their expert knowledge and support of this year’s competition. We would also like to thank all the National Organisers and those who support the CanSat competition behind the scenes.

Once again, a huge congratulations to all the teams that participated in this year’s competition. Your persistence and ability to deliver exceptional projects under difficult circumstances is truly commendable.

![输入图片说明](https://foruda.gitee.com/images/1669021213272281675/6ea8ad12_5631341.png "2021_European_CanSat_Competition_Awards_Ceremony_pillars.png")

- 2021 European CanSat Competition Awards Ceremony

[Access the video](https://www.esa.int/ESA_Multimedia/Videos/2021/10/2021_European_CanSat_Competition_Awards_Ceremony)

---

【笔记】[EXPLORE 6 RELATED PAGES](https://gitee.com/RV4Kids/CanSatKitLibrary/issues/I623LZ)

1. [CanSat - Beginners Guide : 6 Steps - Instructables](https://www.instructables.com/CanSat-Beginners-Guide/)

![输入图片说明](https://foruda.gitee.com/images/1668958736034331025/89ad22db_5631341.png "屏幕截图")

www.instructables.com

2. [CanSat project - code for sensors - Arduino Forum](https://forum.arduino.cc/t/cansat-project-code-for-sensors/296448)

Hello, we're a class from Norway working on a project for the CanSat competition (Build a GPS-Enabled parachute satellite). We don't have a lot of knowledge within programming and would really appreciate some help. So far we have soldered a shield to the arduino, and implemented the following sensors/items: Communication radio (APC220) with antenna Pressure sensor (MPX4115) 
Temperature sensor ...

forum.arduino.cc

3. [PDF] [CanSat design and their applications - ResearchGate](https://www.researchgate.net/publication/325388316_CanSat_design_and_their_applications)

   www.researchgate.net

   6. [CanSat Design and Their Applications - Aerospace Research Central](https://arc.aiaa.org/doi/pdf/10.2514/6.2018-2407)

   1 CanSat Design and Their Applications Atiwat Piatong1, Aonjira Sirisestanun1, Supachai Chiangchin1, Suwijak Apisakulroj1, Witsanu Suwat1, and Peeramed Chodkaveekityada2 Faculty of Engineering, King Mongkut’s Institute of Technology Ladkrabang, Bangkok, 10520, Thailand CanSat is a simulation model used in actual satellite working in the space

   [arc.aiaa.org](https://arc.aiaa.org/doi/pdf/10.2514/6.2018-2407)

4. [A Guide to the Primary Mission - European Space Agency](https://www.esa.int/Education/CanSat/Getting_Started_with_CanSat_A_Guide_to_the_Primary_Mission_Teach_with_Space_T08)

![输入图片说明](https://foruda.gitee.com/images/1668958801297943357/d8982dab_5631341.png "屏幕截图")

www.esa.int

5. [GROUND STATION DESIGN PROCEDURES FOR CANSAT - Academia.edu](https://www.academia.edu/4659667/GROUND_STATION_DESIGN_PROCEDURES_FOR_CANSAT)

![输入图片说明](https://foruda.gitee.com/images/1668958821117465195/d061e7dc_5631341.png "屏幕截图")

www.academia.edu

- Profile image of Rodriguez ArthursRodriguez Arthurs

  > CANSAT's are getting more popular each day in aerospace engineering curriculums because they enable the students to have hands-on experience on virtual satellite launch operations. This paper presents design and implementation of a PC based ground station for CANSAT's. So far, most of the emphasis has been built on the hardware and software inside the CANSAT. However, during the competitions it has been observed that even though the CANSAT works perfectly, the ground station frequently fails therefore leading to mission failure. In this study a ground station is developed from scratch using a high level language (C#) and the procedure is defined briefly. The station is platform-free therefore it may operate with any CANSAT having different brand microcontrollers. The users may track several parameters and send control commands simultaneously. The ground-station is one of the most important aspects of CANSAT trials however, for the beginners it may be challenging to provide all these...

- GitHub - AldaCL/CanSat: Ground Station System, …
  https://github.com/AldaCL/CanSat
  > Cansat code and implementation . Open and compile S2.ino file with Arduino IDE, then upload it to Arduino NANO that will be placed inside satellite. Check wiring to sensors …

  https://gitee.com/RV4Kids/CanSat

- GitHub - SigmaAcehole/CanSat: Arduino program and …
  https://github.com/SigmaAcehole/CanSat
  > CanSat My Teensy code for CanSat using Arduino IDE. My PCB schematics and design using AutoDesk Eagle. This code fulfils the problem statement of CanSat 2020 organized …

  https://gitee.com/RV4Kids/SigmaCanSat

- CANduino.EU | About

  https://canduino.eu
  > CANduino is based on the requirements of EUROPEAN CANSAT COMPETITION. The size of the can is 115 mm in height and 66 mm in diameter with additional space of 45mm in height above to fit additional …