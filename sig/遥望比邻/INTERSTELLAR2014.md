《星际穿越》INTERSTELLAR (2014)

- [百万人打出9.3分，近六年最硬核的科幻电影，导演脑洞太大了！](https://baike.baidu.com/item/%E6%98%9F%E9%99%85%E7%A9%BF%E8%B6%8A/7588527)
- [不谈科普 来聊聊《星际穿越》的实景拍摄](https://www.bohaishibei.com/post/5694/)
- [INTERSTELLAR (2014): Infographics...](https://film-book.com/interstellar-2014-infographics-spielbergs-script-differences-neil-degrasse-tyson-comments-nolans-science-defense/) & [Posters](https://gitee.com/RV4Kids/RVWeekly/issues/I5Q6KE#note_12924376_link)

《[RISC-V 中国创世记](https://gitee.com/flame-ai/siger/blob/master/RISC-V/RISC-V%E4%B8%AD%E5%9B%BD%E5%88%9B%E4%B8%96%E8%AE%B0.md)》 转自：[雷峰网](https://mp.weixin.qq.com/s?__biz=MTM2ODM0ODYyMQ==&mid=2651559224&idx=1&sn=f8dfdee0954cf636f29a49fefc57c245) ， [SIGer 笔记](https://gitee.com/RV4Kids/RVWeekly/issues/I5Q6KE)

  - 《[计算机体系架构：量化研究方法](https://gitee.com/flame-ai/siger/blob/master/%E7%AC%AC4%E6%9C%9F%20Better%20RISC-V%20For%20Better%20Life.md)》 [封面/照片](https://images.gitee.com/uploads/images/2021/0324/194443_80de4424_5631341.jpeg) 
  - 《[计算机体系结构的新黄金时代](https://gitee.com/flame-ai/siger/blob/master/RISC-V/%E8%AE%A1%E7%AE%97%E6%9C%BA%E4%BD%93%E7%B3%BB%E7%BB%93%E6%9E%84%E7%9A%84%E6%96%B0%E9%BB%84%E9%87%91%E6%97%B6%E4%BB%A3.md)》 [A New Golden Age for computer Architecture](https://cacm.acm.org/magazines/2019/2/234352-a-new-golden-age-for-computer-architecture/fulltext) 

<p><img width="706px" src="https://foruda.gitee.com/images/1662552778221384225/1f010a98_5631341.jpeg" title="269Interstellar.jpg"></p>

> “高能预警，前方有虫洞。” 这是我最喜欢的时刻了，就是完成海报后的注解。好好的一篇影评，被我有模有样地插入了私货，此时此刻，我有一种穿越的感觉，或是五维空间的未来感，或是博古通今的隐喻，这个时点，我输入的每一个文字，敲击键盘的每个嘀嗒，都可能成为穿越时空安放的 “虫洞” 啦。既期待未来的某个时点，它被联通，“也不期待” 是此刻的心情，一切随缘吧。:pray: 凡事自有安排。

说来有趣，SIGer 最得意的一张海报，就是《计算机体系结构的新黄金时代》啦，也是从那个时候起，有了 SIGer 的画风（话风），科普的文字更像是述说故事。刚刚完成 RVSC 2022 的专题，就迫不及待地 “分享”，而这张 RISCV 创世纪的照片，成为了连接我和 雷锋网 的纽带，我也如愿地联系到了作者，转载了《RISCV 中国创世纪》。每当重温一篇文字，总会有新的收获，《星际穿越》就是今天的收获，让我再次拉片，听影评，对这部硬核的科幻片也有了更多体会，如今 SIGer 也有了 “遥望比邻” “MARs火星” “天文社” 啦，而片中的暗线 更高维的 LOVE，成了全片的脚注。不管为什么选择 冰岛荒原的剧照做 “虫洞”，这份涟漪已经产生了，现在。

> 本期专题的主要内容，依然是影片本身，至于虫洞能否联通某个时空的问题，我们就拭目以待吧。

# [百万人打出9.3分，近六年最硬核的科幻电影，导演脑洞太大了！](https://baike.baidu.com/item/%E6%98%9F%E9%99%85%E7%A9%BF%E8%B6%8A/7588527)

> 《星际穿越》是2014年美英联合制作的科幻电影。该片由克里斯托弗·诺兰执导，马修·麦康纳、安妮·海瑟薇领衔主演。该片在物理学家基普·索恩的黑洞理论之上进行改编，主要讲述了一组宇航员通过穿越虫洞来为人类寻找新家园的冒险故事。该片于2014年11月5日在美国公映，11月7日在英国公映，11月12日在中国大陆公映。并于2020年8月2日在中国大陆重映。

1.

在未来的某一天，地球上的大多数植物都得了枯萎病，水稻、小麦、秋葵和向日葵等农作物逐渐在地球上灭绝，唯一能活下来的农作物只有玉米。人们只能每天吃点玉米饼子，喝点苞米茶和粥填饱肚子，闲着没事的时候想嗑个瓜子，吃个花生毛豆都是奢望。如果玉米也得了枯萎病，人类就会因为饥饿而彻底灭绝。然而，不光食物来源越来越少，气候也变得极差。沙尘暴是三天一小刮，五天一大刮，人类就在这种严苛的生态条件下苟延残喘。库珀是一名退役飞行员，他曾经也是一名机械工程师，现在是一个种苞米的农场主。他有两个孩子，分别是儿子汤姆和女儿墨菲。这天，库珀应学校的要求去参加家长会。在开车赶往学校的途中，一架至少已经飞行了十年的无人机飞过他们的头顶，库珀兴奋极了。因为这架无人机的太阳能电池足以给整个农场供电，他顾不上汽车压坏，装的直接冲进了玉米地。接着他的儿子坐在驾驶位，自己腾出手来操作电脑，试图黑掉无人机的飞行控制系统。一盘追逐之后，库珀成功把无人机变成了遥控玩具，并且把他的电池给拆了下来。三人玩的太放火了，所以家长会就迟到了。库珀的儿子汤姆学习很差，这一年大学的招生数量也很少，所以学校建议让汤姆回家种地。一向对科学十分感兴趣，并且成绩非常好的女人，墨菲前阵子把旧版课本拿给同学看，跟同学们说阿波罗登月计划是真实发生的。

2.

新版课本为了让同学们不再对外太空探索产生兴趣，就说登月计划是为了拖垮前，苏联而编造的谎言。莫非和同班同学因为这个问题产生了分歧，并且动手打了起来？学校的意思是让库珀管教一下莫菲，可库珀听完前因后果，直接给莫菲申请了求学，因为他不想让莫菲继续在学校学习一些错误的知识。库珀回到车上，因为农庄主通过无线电说，村里曾经改装过的自动收割机发生了无法解决的故障，这些收割机都不受控制的从田里跑到库珀家的门口。这个现象太过诡异，库珀也无法解释。库珀推门走进家中，突然楼上咣当一声，有什么东西掉在了地上，原来发出声音的是书架上的书。其实这种事儿已经不是第一次发生了。墨菲说屋里有幽灵，是幽灵，想要通过这些书来传递某种信息。库珀让莫菲不要乱想，还说幽灵并不存在。紧接着，库克就下楼把收割机的定位系统全部给修好了。第二天，库克的家人去看棒球比赛，球赛打到一半，铺天盖地的沙尘暴骤然袭来，远远看去，像一床30年没洗的被子，想要强行压在人们的身上，让人觉得十分恐惧。村里的人有车的开车，没车的跑步，都迅速赶回了家，钻进了爸爸妈妈的怀抱。库克跟随莫非回到卧室关窗户的时候，再一次见到了相当诡异的房面，房顶缝隙掉下来的人，竟然规则的排列成了某种图形，酷。

3.

布尔惊了，赶紧让墨菲抱着枕头去别的屋睡，虽然这些图案不可能是什么，神仙古怪弄出来的，但是很明显，这些图案确实是在向他们传递着某种信息。这天夜里，库珀无法入眠，他研究了一个通宵，并且开始相信墨菲的话，这些灰尘居然是利用重力排列出来的二进制信息，长的代表一短的代表名，换算后得出来的结果是一个距离并不太远的坐标。由于好奇心的驱使，库珀决定马上到坐标位置去一探究竟。墨菲也想跟着去，但库珀他倒是会发生危险，所以就拒绝了。好奇心这种东西，不光大人会有，小孩也会有。墨菲趁库珀不注意的时候钻上了汽车，藏在了副驾驶堆放的被子下面。库珀发现墨菲之后，虽然嘴上批评了他，但实际上对于女儿的突然出现，他还是感觉挺贪心的。两人一直行驶到深夜，总算到达目的地，面前突然出现了一个非常不起眼的铁门，库珀刚想用老虎钳把铁门夹断里边就走出来一个机器人保安，将妇女二人给带走了。其实这个地方以前是北美防空联合司令部，现在是美国国家航空航天局的秘密基地，也就是传说中的NASA。老百姓连饭都快吃不上了，所以根本眼睛不支持政府在花钱搞什么军备和航天探索了。NASA现在干活都是偷摸干的。机器保安的名字叫塔斯，他想从库珀嘴里审批出是怎么获得基地坐标的。库珀说自己必须先确认闺女是安全，然后才会开口。这时候，一个叫布兰德的女博士把库珀带到了一个会议室，见到了若非会议室里坐。

4.

这都是NASA的管理层，其中的带头大哥就是布兰德博士的爸爸，我们就叫他老博士。老博士在库珀参观了一圈，并对库珀说，在近几年的时间里，库韦并在不停的消灭地球上生产粮食的植物。别看现在咱们还能种植玉米，但很快玉米也一定会被库韦病杀死。不光如此，地球在空气中有近80%是淡季，约有21%是氧气库，韦病菌也是会呼吸氧气的，一旦病菌覆盖到大面积种植的玉米上，人类还没等饿死就已经因为缺氧窒息死掉了。莫非就极有可能是这个地球上的最后一代人，也是死的最惨最痛苦的一代人？其实老博士说这句话的目的是为了通过贩卖焦虑，迫使库珀加入探索宇宙的队伍，他希望库珀可以驾驶永和号飞船寻找新的，适合人类居住的星球。库珀已经是现存人类中为数不多的飞行员了。尽管他以前开的是飞机，但他也曾接触过宇航员的训练。再给换个角度来说，之所以只有库珀得到了激励坐标，很明显就是因为他们选择了库珀。库珀听完沉默片刻，决定参与到老博士的计划中。项目组成员罗比利告知库珀，引离异常现象其实已经发生过很多次了。除此之外，还有一个更让人吃惊的现象，那就是在48年前，有人在土星附近放置了一个可以通往其他星系的虫洞。十年前，拉萨已经带了包括曼恩博士在内的12名宇航员进行探索了，其中有三个进入相同星系的宇航员分别夺路在不同的星球。

5.

并且发出了宜居星球的信号，如果这次库珀团队进入重奏后，在这三个星球中真的筛选出了宜居星球，那么，就有a计划和B计划可以让人类迁徙过去。a计划是个大过程，其实整个实验室就是个空间站，老博士正在研究一套隐蔽公式，只要这套公式解开了，他就可以用实验室带着尽可能多的人类移动到宜居地带。B计划就比较简单道理，库珀他们这次出发会带着5000多枚受精卵，等到达宜居星球之后，再利用战略技术把这些受精卵全部孵化成人类，最多30年就能培养出百人规模的殖民地。但如果执行B计划，地球上的生命则全部都要放弃。老博士说，现在a计划的公式已经有了阶段性的突破，只要在攻克一个难点就可以实现了。等库珀归来之日就是地球人移民之时，莫非得知库珀尔离开地球之后？回到卧室自己生闷气，库珀就走进屋给墨菲做思想工作。他说在自己出发之前一定要得到墨菲的原谅。墨菲说，如果我不原谅你的话，你是不是就可以留下了？听了这句话之后，顿时我就感觉以后要孩子必须得有个闺女。接着，库珀说了一句很有意思的话，我们以后都只是孩子们的回忆，一旦你为人父母，你就成了孩子未来的幽灵。墨菲说，你曾经说过幽灵并不存在。墨菲又说，我现在没法做你的幽灵，为了确保咱们都能活下去，那些超自然现象都是你发现的，然后由我破译出来的。是他们让你带我去拿萨。墨菲拿出笔记本，说自己已经破译了幽灵留下的。

6.

真正信息只有一个词，这也就是留下。库珀认为这个词是因为墨菲太想让自己留下而编造出来的。他掏出一块手表对墨菲说，当我接近黑洞时，时间会流逝的很慢，并且发生偏差。等我回来之后，我们对比一下，可能那个时候咱们俩已经是同岁了，你一定要记得，到时候你还得叫爸，不能叫哥。墨菲听出来，库珀自己也不知道自己什么时候回来，甚至不知道能不能回来。他便愤怒的扔掉了手表，捂上被子不停的哭泣。库珀间再也劝不动了，留下了一句我永远爱你。然后便带着遗憾准备离开。这时书架上掉落了一本书，库珀回头看了一眼，并且愣了一下，但还是走入卧室。临行前，库珀和岳父照顾好孩子，托儿子照看好自己的家，然后便开着车义无反顾的向前驶去。他翻开副驾驶堆放的被子，却没有看到墨菲的身影。此时的墨菲刚刚跑出房门，甚至都没来得及和父亲说一声再见。就这样，库珀和三个同事以及塔斯KISS2个机器人共同坐着漫游者号飞上了太空。随着二级火箭的分离，他们再也无法听到地球上的嘈杂声了，取而代之的是死一般的寂静。在库珀的操纵下，飞船开始飞向空间站，在这简单介绍一下团队成员四名人类，分别是库珀，布兰德，罗米蒂和多耶尔。罗比利是一个聪明绝顶的黑人，多耶尔的特征那应该就是一脸的大胡话。咱们得着重介绍一下其中一类智能机器人塔斯，他不光会协助人类完成工作事宜，还会讲一些笑话逗人开心，甚至还能在。

7.

的时候自行斟酌，然后进行回答。漫游者号和永恒号空间站的距离越来越近，库珀和多伊尔配合起来尝试与空间站进行对接，对接，是一件非常严谨的工作。首先，飞船的飞行速度一定要稳，其次还要保证接口对的必须要准，他必须得扣严实了，同时保证以上三个条件才能打开舱门，不然就会有非常恐怖的后果发生。最终，几人凭借着自己过硬的业务能力和心理素质顺利完成了对接。库珀开始让空间站进行自转，空间站随着旋转慢慢开始有了重力，在这个距离遥望地球虽然很美，但你也象征着这些宇航员们将彻底离开家园，去往未知星系进行探索。在空间站，人们可以拍摄短视频与打法进行对话。第一条信息是老博士发来的，他告诉库珀团队，两年后他们就到达普京。并送上了一首诗歌，不要温和的走进那个柳荫，为了节省资源，大家必须到休眠舱里休眠。在第一次休眠之前，布兰德向库珀介绍了一下三个发出移居星球信号的宇航员。在聊天的过程中，库珀敏锐的发现，在讲到其中埃德蒙斯的时候，布兰德的眼神中透露出了一些不一样的东西，那是一种光芒，就好像曾经学校里的校花提到久书放出的光芒是一样，是爱心型。对话结束后，布兰德进入休眠舱，库珀偷偷询问机器人卡兹布兰德与埃德蒙斯的关系，尽管带有自行斟酌功能的机器人，卡兹对这个问题避而不答，但此时库珀心里却已经有了答案。

8.

很快，两年时间过去了，永恒号也到达了土星附近。和土星比起来，空间站渺小的像一粒细沙，距离虫洞已经越来越近了。此时，罗美丽解释了一下虫洞的原理，虫洞并不是洞，而是一个球。简单来说，它就像是一张纸上有两个洞口，在纸张中心进行折叠，让两个洞口重合，这样就缩短了距离，谁也猜不到它的另一头是在哪个星系。但毋庸置疑的是，虫洞肯定是比我们维度更高的，他们放在那里。库珀团队进入了虫洞，里边的景象特别震撼，却难以用现有的思维理解，因为它是超三维空间。没过多久，飞船内发生了时空疾变。突然，布兰德博士看着窗外，愣了许久，他仿佛在窗外发现了什么东西。当库珀问他在干什么的时候，布兰德回答道，我刚与他们进行了第一次握手。在穿越虫洞之后，大家就必须选择先去哪个星球了。米勒和曼安所在的星球情况良好，特别是米勒星球上有水资源以及有机物，而埃德蒙斯所在的星球三年前就失去了联络。米勒的星球距离比较近，但是这个星球在黑洞世界的边缘，由于黑洞巨大的引力影响，黑洞上的时间过得非常缓慢，一个小时就相当于地球上的七年。库珀听完之后立刻拒绝登陆，假设在这个星球上耽误几个小时，很可能人类就已经灭绝了，这实在是太冒险了。现在时间也是一种资源，而且是稀缺资源，不过如果先去距离较远的星球，再回头，米勒的星球燃料肯定也不够用了。库珀沉思片刻，想出了一个折中的办法，那就是先围绕黑洞，边缘刚好不会发。

9.

当距离米勒星球最近点的时候再转头登陆，这样做虽然会浪费燃料，但是会节省很多时间。这次任务除罗比利和塔斯之外，其他的人和机器人都要前往。时间宝贵。计划制定完毕后，库珀决定马上出发。黑洞远远看去非常壮观，且没有什么危险，但黑洞的引力强大的不可思议，即便是光线照射进去都会被黑洞扭曲，更别说降落的上面观察它的极点了。漫游者号很快接近目标星球，库珀为了节省燃料以及时间做出了一个大的引力，那就是要利用空气动力学滑翔，在接近星球表面时在反向喷射降落。经过一波惊心动魄的操作之后，总算平稳着陆。这波操作给布兰德吓的小心脏都被停止跳动了，这是一颗，一眼望去全能量是水的星球，远处有一排高耸入云的山峰，布兰德和波伊尔下船之后。向着200米开外的信号发射器走去，结果走到地方才知道，原来米热的飞船早已碎裂成了一块一块的残骸。布兰德为了去找飞船的记录仪，并向着山峰的方向走去。库珀定睛一看，远处的根本就不是什么山峰，而是一片惊天之号，他赶紧命令大家迅速返航，可布兰德为了达到记录仪中的数据违反了命令，而且还被残骸卡住了大腿。还好佩斯及时变身，跑过去将布兰德给救了回来，可惜布兰德耽误了时间，导致多伊尔没有走上船，被巨浪给带走。

10.

飞船虽然安全度过巨大的冲击，但是发动机进水无法起飞，他们需要一个小时才能将水排干净。库克大陆和布兰德吵了起来。不过很快两个人就冷静下来了，不然？从查看情况分析，米勒的飞船大概率是在着陆之后被巨浪打翻的。从地球的时间来看，巨浪来袭是在几十年前发生的，但由于重力导致时间延缓，实际上，米勒可能几个小时前在着陆才发生的，但由于重力导致时间延缓，实际上米勒可能几个小时前在着陆，这时顾客问到有没有让时间倒流的方法。

11.

布兰德解释道，时间是相对的，可以通过改变重力来让它变得很快或者很慢，但就是做不到让它倒退。唯一能像时间一样跨越维度的只有引力。那他们有没有可能从未来给我规划，不然他说有可能他们是五维生物，时间相对。

# [不谈科普 来聊聊《星际穿越》的实景拍摄](https://www.bohaishibei.com/post/5694/)

小编: 梁萧 发布: 2014 年 11 月 26 日

![输入图片说明](https://foruda.gitee.com/images/1662546122514473760/8584829d_5631341.png "屏幕截图")

### “守旧导演” 没 3D 爱胶片

没有满屏的电脑特效以及演员绿幕前 “尴尬” 的无实物表演，《星际穿越》显然与现在太空题材的科幻大片带给影迷的印象不同。而之所以会这么做的原因，与诺兰的 “守旧” 不无关系。在诺兰看来，他的电影不适合 3D，因为 “昏暗的 3D 图像会让观众有疏远感”。

![输入图片说明](https://foruda.gitee.com/images/1662546126697756881/4ac54eaa_5631341.png "屏幕截图")

诺兰的 “守旧” 不仅表现在他对 3D 的排斥，也表现在他对胶片电影的坚持。诺兰是胶片放映的狂热支持者，他认为胶片的色彩感较之数字更好。而《星际穿越》也是用 35mm 胶片和 70mm 胶片结合 IMAX 技术拍摄，影片将在 77 个市场发行胶片拷贝，包括 41 场胶片 IMAX、10 场 70 毫米胶片以及 189 场 35 毫米胶片格式。

![输入图片说明](https://foruda.gitee.com/images/1662546130292974636/010009b0_5631341.png "屏幕截图")

为了实现这场太空幻梦，不爱特效爱实景、不爱数字爱胶片、不爱 3D 爱 IMAX 的诺兰不得不带领团队攻克一系列技术难关。在寒冷地带找不到玉米地？那就花半年时间种一片；特效做不好沙尘暴？那就人为制造一场；演员站在绿幕前表演找不着感觉？那就给他们设计出浩瀚星空和太空舱；想用 IMAX 摄影机拍摄最多画面？那就让摄影师把它扛在肩膀上……

### 用半年时间种 500 亩玉米地

![输入图片说明](https://foruda.gitee.com/images/1662546138862903839/f321e74d_5631341.png "屏幕截图")

片中库珀一家的取景地位于加拿大，艾伯塔省根据诺兰的指示，要表现出地球上最不适合种玉米的地方都已经种起了玉米 (玉米喜高温，而艾伯塔省气候寒冷，冬天最低温为零下 50 度左右)。对于诺兰来说，找一片现成的玉米地然后用电脑拼上雪山背景这种事他是不屑于做的，因此团队费尽功夫终于在加拿大找到了可以实现的环境。

### 大型鼓风机制造 “飞沙走石” 场景

![输入图片说明](https://foruda.gitee.com/images/1662546146658346262/252b9b5a_5631341.png "屏幕截图")

电影里的沙尘暴铺天盖地，诺兰知道用电脑 CGI 技术不可能达到让他满意的沙砾和浸没感，因此他向特效协调员斯科特・费希尔求助。 费希尔的回答是 C-90，一种无毒，可生物降解的，用碾碎的纸板做成的材料，足够安全以用作某些加工食品的填充料，和重量足够轻，可实现诺兰要的悬停效果。

### “外星球” 皆在冰岛取景

![输入图片说明](https://foruda.gitee.com/images/1662546169938174408/72258649_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1662546170943121660/f89e490b_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1662546175561683204/68922355_5631341.png "屏幕截图")

诺兰上次访问冰岛是十年前，拍摄《蝙蝠侠：侠影之谜》的一些镜头，在实地探勘风景之前，他甚至已经知道他为主人公们找到了在《星际穿越》里用来探险的丰富地貌。“我们希望那超凡多俗的环境就像我们现在生存的地方一样真实得摸得到。” 他说。“所以，当宇航员在另一个星球上跨出第一步时让观众能与之同在，我们知道我们必须要到实景去拍，冰岛的景观真是独特而极端。”

### 安妮・海瑟薇在冰岛拍摄时患上低温症

![输入图片说明](https://foruda.gitee.com/images/1662546185989041306/be76f068_5631341.png "屏幕截图")

多元化的冰岛地形给故事中的两个目标星球提供了理想的拍摄景地，不远距离的浅但看上去无边无际的布鲁纳仙度泻湖可以作为电影中在水星球的降落区。被火山喷射过的冰川，形成了一个冰面上超现实主义的灰色大理石效果。而身穿宇航服泡在水里的海瑟薇拍摄时候差点感觉不到自己的脚趾头，甚至产生了低温症状。但当时可没人顾得上她，因为几乎所有工作人员、汽车、大物件们都泡在汪洋大海里。

### 制造能动的机器人和飞船

![输入图片说明](https://foruda.gitee.com/images/1662546199463463835/994ad147_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1662546203968356470/d71e1623_5631341.png "屏幕截图")

如果这些还不能让你体验到诺兰的固执，那么在加州建了一艘重达 10000 磅的飞船是不是有点夸张了？诺兰在拍摄前给设计师们看了 1983 年的《太空英雄》，研究他们如何用面盔之类的东西创造真实感，结果他们就在索尼的摄影棚里、那个诺兰原先打造蝙蝠洞的地方，搞出了一个完全密封的太空舱，这样马修和安妮在船上就可以透过窗外看到外面，让飞船不只是现场，更是模拟器。

![输入图片说明](https://foruda.gitee.com/images/1662546213018506609/bd0c16b4_5631341.png "屏幕截图")

而这个宇宙飞船 “漫游者号”(the Ranger)，是的，他们真的造了出来，混合了《007：海底城》里的水陆两用跑车、《星球大战》里的叛军雪地飞艇和蝙蝠车的设计，诺兰以几乎与电影中等大的飞船模型完成了最终拍摄。几艘飞船，每个都体重超过万磅，被拆卸，打包装在在集装箱里，由 747 货机运往在冰岛，然后装上卡车，运到拍摄地，在巨型帐篷里重组。

![](https://foruda.gitee.com/images/1662546209607417712/a4f726e2_5631341.png "屏幕截图")

电影里油嘴滑舌的机器人 TARS 萌到许多人，诺兰对它的最初设想就是要跟所有科幻片里的都不一样。当然了，他的要求依然是不要用特效。设计团队用仅有的八周时间，使得 TARS 终于可以借助液压系统触发附件控制器的方式动了起来，简而言之就像打电动游戏一样用按钮控制机器的各关节的动作，并且连屏幕也是真实的。TARS 重达 90 公斤，由舞台演员比尔 - 欧文操纵。

### 把沉重的 IMAX 摄影机扛到肩膀上

![输入图片说明](https://foruda.gitee.com/images/1662546247280576699/901f535f_5631341.png "屏幕截图")

作为 IMAX 最狂热的信徒，诺兰越来越竭尽可能地在电影中使用 IMAX 画面。对于这部《星际穿越》，诺兰更是重视对浩瀚宇宙的表现，并且还提出想实现纪录片式的手持镜头，以便营造真实感和亲密感。于是摄影机霍伊特 - 范 - 霍特玛便开始与诺兰一起研究，最终霍特玛决定将巨重无比的 IMAX 摄影机扛在肩膀上。人总归要比机器智能无数倍。

### 吉普 - 索恩物理学大师坐镇

![输入图片说明](https://foruda.gitee.com/images/1662546247781269722/b013f0e0_5631341.png "屏幕截图")

《星际穿越》原本是史蒂文 - 斯皮尔伯格的项目，由克里斯托弗 - 诺兰的弟弟乔纳森 - 诺兰担任编剧。但斯皮尔伯格没有时间执行，诺兰听闻后果断将之接手过来，然后兄弟俩一起改写剧本。诺兰坚持科幻片应该有理可依，因此请来了真正的物理大师吉普 - 索恩担任制片人之一，参与整个筹备过程，花时间进行测试，基普是当今世界上研究广义相对论下的天体物理学领域的领导者之一，他的代表作之一便是《黑洞与时间弯曲》。他的渊博学识成为了《星际穿越》的理论支柱。

![输入图片说明](https://foruda.gitee.com/images/1662546255337287941/9500ab9f_5631341.png "屏幕截图")

这个世界是属于偏执狂的，在当下电影追求炫酷的电脑特技的时代，诺兰用他的 “小众” 拍摄方式，送上了如此震撼惊艳的《星际迷航》，对了，忘了说，《盗梦空间》里的失重场景，这个偏执狂导演，也是实景拍摄的。via

> 本文来自网络，不代表博海拾贝立场，转载请注明出处：https://www.bohaishibei.com/post/5694/

# [INTERSTELLAR (2014): Infographics, Spielberg’s Script Differences, deGrasse Tyson Comments, Nolan’s Science Defense](https://film-book.com/interstellar-2014-infographics-spielbergs-script-differences-neil-degrasse-tyson-comments-nolans-science-defense/)

Photo of Rollo Tomasi Rollo Tomasi Follow on TwitterNovember 11, 20144 4 minutes read

![输入图片说明](https://foruda.gitee.com/images/1662548179778167714/0f7e2bc2_5631341.png "屏幕截图")

Interstellar Infographic, Script Differences, Neil DeGrasse Tyson Comments, and Science Defense. Christopher Nolan‘s Interstellar (2014) has arrived and with it many questions, observations, conjecture, and differing points of views on what was in the film, its assumptions, and what was left out of the film. Everyone for astrophysicist Neil deGrasse Tyson to director Christopher Nolan has spoken up about various Interstellar subjects. Below we have gathered the most interesting of these for you to read and view. Warning, major spoilers below.

The Interstellar Timeline Infographic:

![输入图片说明](https://foruda.gitee.com/images/1662548420652185467/f0e7c148_5631341.jpeg "interstellar-timeline-infographic-01-1240x1827.jpg")Interstellar Timeline Infographic

Interstellar Timeline Infographic

The Interstellar Science Infographic:

![输入图片说明](https://foruda.gitee.com/images/1662548532852927649/d0a8880f_5631341.jpeg "interstellar-science-infographic-01-575×3559.jpg")

Interstellar Science Infographic

The Space of Interstellar Infographic:

![输入图片说明](https://foruda.gitee.com/images/1662548654164990756/d6db3603_5631341.jpeg "space-of-interstellar-infographic-01-575x3049.jpg")

Space of Interstellar Infographic

The Interstellar Timeline Infographic:

![输入图片说明](https://foruda.gitee.com/images/1662548782944505630/7063a1d5_5631341.jpeg "interstellar-timeline-infographic-01-2328x1610.jpg")
 
Interstellar Timeline Infographic

Christopher Nolan on the validity of the science in Interstellar:

> My films are always held to a weirdly high standard for those issues that isn’t applied to everybody else’s films—which I’m fine with. People are always accusing my films of having plot holes, and I’m very aware of the plot holes in my films and very aware of when people spot them, but they generally don’t.

> Those issues are all buttoned-up, and Kip has a book on the science of the film about what’s real, and what’s speculation—because much of it is, of course, speculation. There have been a bunch of knee-jerk tweets by people who’ve only seen the film once, but to really take on the science of the film, you’re going to need to sit down with the film for a bit and probably also read Kip’s book. I know where we cheated in the way you have to cheat in movies, and I’ve made Kip aware of those things.

The differences between Jonathan Nolan‘s script for Steven Spielberg‘s Interstellar and Christopher Nolan’s Interstellar:

1. Cooper And His Son Murph Find a Fallen Space Probe

2. A Fallen Space Probe Brings Cooper To NASA, Not a Morse-Code Gravity Communicating Bookshelf Ghost

3. The Lazarus Missions Never Happened; This Is the First Manned Mission Through the Wormhole

4. Sphere-Shaped Distortions Visit the Crew While Traveling Through The Tiny Wormhole

5. The Mission Only Travels To One Planet: The Ice Planet

6. You Won’t Believe What They Find On The Ice Planet: The Chinese?!

7. A Gravity Machine Is Discovered

8. Evil Matt Damon Never Shows Up, But Instead We Get Aliens and Bad Robots

9. Everyone On Earth Dies But The Story Doesn’t Cut Away To Show Whats Happening On Our Planet

10. Cooper and Brand Fall In Love, Have Sex In Zero Gravity

11. A Second Wormhole Is Discovered and The Distortion “Creatures” Return

12. The Ship Finds a Space Station Built Outside of Space and Time

13. A Time Traveling Wormhole is Discovered

14. Cooper Arrives Back on Earth, But Humanity Is Completely Gone From the Planet

15. The Ending on Cooper Space Station Is the Same, But Different

The original script can be found here. “The reason Christopher Nolan shares the screenwriting credit on the final film with Jonathan Nolan is because he reworked the original script with substantial changes.” From Christopher Nolan: “I had the advantage of coming onto the project late and being able to look at what these guys [Jonah Nolan and Kip Thorne] had done. A lot of my contribution was ripping things out, because they put in more of these incredible mind blowing ideas that, I felt, I could absorb as an audience member. So I spent my time and my work on the script choosing the more emotive and tactile of these ideas to grab ahold of.”

Here are Neil deGrasse Tyson’s thoughts on the science of Interstellar:

* Experience Einstein’s Relativity of Time as no other feature film has shown.

* Experience Einstein’s Curvature of Space as no other feature film has shown.

* Relativity. Gravity. Quantum. Electrodynamics. Evolution. Each of these theories is true, whether or not you believe in them.

* And in the real universe, strong gravitational fields measurably slow passage of time relative to others. GPS satellites, located farther from Earth’s center than we are, keep faster time than do our clocks on Earth’s surface. GPS Satellites are pre-corrected for General Relativity, allowing them to beam us the accurate time for Earth’s surface.

* You enter a 3-Dimensional portal in space. Yes, you can fall in from any direction. Yes, it’s a Worm Hole.

* The producers knew exactly how, why, & when you’d achieve zero-G in space.

* You observe great Tidal Waves from great Tidal Forces, of magnitude that orbiting a Black Hole might create

* All leading characters, including McConaughey, Hathaway, Chastain, & Caine play a scientist or engineer. Of the leading characters (all of whom are scientists or engineers) half are women. Just an FYI.

* They reprise the matched-rotation docking maneuver from “2001: A Space Odyssey,” but they spin 100x faster.

* On another planet, around another star, in another part of the galaxy, two guys get into a fist fight.

* There’s a robot named KIPP. One of the Executive Producers, a physicist, is named Kip. I’m just saying.

* If you didn’t understand the physics, try Kip Thorne’s highly readable Bbook “The Science of Interstellar”. If you didn’t understand the plot, there is no published book to help you.

* They explore a planet near a Black Hole. Personally, I’d stay as far the hell away from BlackHoles as I can.

* Neil deGrasse Tyson Interstellar Comments: Is It More Scientifically Accurate Than Gravity?

Watch the video of deGrasse talking about the scientific accuracy of some of the scenes in Interstellar and leave your thoughts on it below in the comments section. For more Interstellar information, photos, and videos, visit our Interstellar Page, our Movie News Google+ Page, our Movie News Facebook Page, subscribe to us by Email, “follow” us on Twitter, Tumblr, Google+, or “like” us on Facebook.

Source: Slashfilm, Moptwo, Space, Dailybeast, Dogancangundogdu

### Posters

[![输入图片说明](https://foruda.gitee.com/images/1662549049723712656/6071082e_5631341.png "32b6ff34_5631341.png")](https://gitee.com/flame-ai/siger/blob/master/RISC-V/RISC-V%E4%B8%AD%E5%9B%BD%E5%88%9B%E4%B8%96%E8%AE%B0.md)

![输入图片说明](https://foruda.gitee.com/images/1662549203543799947/7b4e693f_5631341.jpeg "interstellar14053.jpg")

![输入图片说明](https://foruda.gitee.com/images/1662549258412641263/58cbda74_5631341.jpeg "max1426513755-potlaccd-cover.jpg")

![输入图片说明](https://foruda.gitee.com/images/1662549339037786348/2aad683e_5631341.png "d808aqt-3329940b-dc8c-4ecb-8e81-c080044efcd0.png")

![输入图片说明](https://foruda.gitee.com/images/1662549495209179264/225ce0d7_5631341.jpeg "interstellar14025.jpg")

![输入图片说明](https://foruda.gitee.com/images/1662550024955971413/cb9ebfeb_5631341.jpeg "1034341.jpg")

![输入图片说明](https://foruda.gitee.com/images/1662550042688311469/d224bdc6_5631341.jpeg "1034342.jpg")

![输入图片说明](https://foruda.gitee.com/images/1662550478860185074/ac395685_5631341.jpeg "max1427363917-frontback-cover.jpg")

![输入图片说明](https://foruda.gitee.com/images/1662550650455913756/dcfe3d8b_5631341.jpeg "interstellar14001.jpg")

【笔记】[RISC-V 中国创世记](https://gitee.com/RV4Kids/RVWeekly/issues/I5Q6KE)

- “你是我的幽灵，我一直知道”——《星际穿越》影评  
  https://www.bilibili.com/read/cv5597087/

- 【混剪/踩点/诺兰/治愈向】星际穿越欧美科幻超燃混剪  
  https://www.bilibili.com/video/BV1p64y1T7i4