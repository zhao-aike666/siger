![输入图片说明](https://foruda.gitee.com/images/1663759942596007138/fc8a7205_5631341.jpeg "科创少年在行动 SIGer 专题献礼科普周")

- [中国科协等18部门关于举办2022年全国科普日活动的通知](https://www.cast.org.cn/art/2022/8/4/art_51_194011.html)

[![输入图片说明](https://foruda.gitee.com/images/1663532320145389487/5d2d4686_5631341.png "屏幕截图")](https://www.kepuri.cn/)

期刊统计：
- 12期 .md 成刊
- 2期 双封面 刊内标签
- 4期 issues 立项

成刊统计：
- 6期 科创主题
- 6期 航天主题及其他

[<img title="280." width="9.99%" src="https://foruda.gitee.com/images/1663240664267282157/5eeaf082_5631341.jpeg">](../xiaoyuan/科创少年的Yes教育.md) [<img title="282." width="9.99%" src="https://foruda.gitee.com/images/1663297078128733541/1409c310_5631341.jpeg">](../xiaoyuan/给孩子们的梦想插上科技的翅膀.md) [<img title="284." width="9.99%" src="https://foruda.gitee.com/images/1663413714428164537/90c49b2c_5631341.jpeg">](../xiaoyuan/科创造就青铜少年.md) [<img title="285." width="9.99%" src="https://foruda.gitee.com/images/1663438105179268015/bd174bfa_5631341.jpeg">](../xiaoyuan/科创竞赛之最.md) [<img title="287." width="9.99%" src="https://foruda.gitee.com/images/1663504190023199732/00cc58bd_5631341.jpeg">](../../STEM/科幻画.md) [<img title="292." width="9.99%" src="https://foruda.gitee.com/images/1663610472008076169/008b8703_5631341.jpeg">](../../少年/玩出来的90后发明家牛培行.md)

航天及其他主题统计：
- 3期 航天科创
- 1期 [印度（小卫星）](../../MARs/印度小卫星.md)
- 1期 [灵境技术](../灵境/)（[5G+AI+元宇宙](../灵境/高通的三驾马车5G+AI+XR.md)）
- 1期 [航天创新大赛](../xiaoyuan/八一少年行-航天创新大赛.md)

[<img title="283." width="9.99%" src="https://foruda.gitee.com/images/1663411157224485149/038b6e85_5631341.jpeg">](【GET2020】把宇宙拉到孩子们面前.md) [<img title="289." width="9.99%" src="https://foruda.gitee.com/images/1663538055569716679/abdfb26f_5631341.jpeg">](瓢虫一号闪烁密码庆祝中国天文学会百年诞辰.md) [<img title="295." width="9.99%" src="https://foruda.gitee.com/images/1663695534271830377/056c522a_5631341.jpeg">](星际航行埃依斯星天地.md) [<img title="286." width="9.99%" src="https://foruda.gitee.com/images/1663493866655899699/62baa422_5631341.jpeg">](../../MARs/印度小卫星.md) [<img title="288." width="9.99%" src="https://foruda.gitee.com/images/1663510972768788333/134d8c40_5631341.jpeg">](../灵境/高通的三驾马车5G+AI+XR.md) [<img title="294." width="9.99%" src="https://foruda.gitee.com/images/1663663353119933027/86ee74a8_5631341.jpeg">](../xiaoyuan/八一少年行-航天创新大赛.md)

> 航天科创是[科创大赛](../xiaoyuan/八一少年行-航天创新大赛.md)的新兵，首届，但一脉相承，同根同种，都是科普人的使命，[鼓励青少年：爱科学，学科学，用科学](../xiaoyuan/科创竞赛之最.md)。

> 这六期主题是科创大赛的延伸，也才有了科普周的主题活动，在科普周结束的最后一天，用载人航天三十而立结语，太完美啦。SIGer 的两个频道[灵境技术](../灵境/)和[遥望比邻](../遥望比邻/)，就是[钱老对星辰大海的完美期待](README.md)，交给我们和我们的未来更多的孩子们。

> 而航天科创的起点和SIGer 钱馆的结缘，都是从[星际航行](星际航行埃依斯星天地.md)开始的。“ **从[星际航行](星际航行埃依斯星天地.md)到载人航天三十而立，最好的科普周礼物。** ” 就是本期的标题啦。

合并 双封面 刊内标签 的2期 和新立项的 4期：
- [500期《天文爱好者》](瓢虫一号闪烁密码庆祝中国天文学会百年诞辰.md#拍摄夜空中闪烁的密码喜迎天文爱好者500期)为 SIGer 300期的选题，向科普老兵学习
- 从《[开源观止](../../RISC-V/SIGer%20少年芯，从北向南的开源文化之旅.md#开源观止第四期)》到思想读本，是 “全民开源素养计划” 的正式开启
- [SIGer 少年芯](../../RISC-V/SIGer%20少年芯，从北向南的开源文化之旅.md)，从北向南，将全栈进行到底，穿越到 [8bits 计算机时代](https://foruda.gitee.com/images/1663576143826973143/f2e352be_5631341.jpeg)，[运用开源的力量](https://gitee.com/shiliupi/Zeal-8-bit-OS/issues/I5RTQ1)
- [艺术高于生活的 开源北向之极](https://foruda.gitee.com/images/1663602645591652493/0d2c22bc_5631341.jpeg)，也同样是[少年之芯培养的起点](https://gitee.com/vicky1980/siger/issues/I5RWX7)

[<img title="296." width="9.99%" src="https://foruda.gitee.com/images/1663700967136678816/95fee563_5631341.jpeg">](瓢虫一号闪烁密码庆祝中国天文学会百年诞辰.md#拍摄夜空中闪烁的密码喜迎天文爱好者500期) [<img title="290." width="9.99%" src="https://foruda.gitee.com/images/1663576143826973143/f2e352be_5631341.jpeg">](https://gitee.com/shiliupi/Zeal-8-bit-OS/issues/I5RTQ1) [<img title="291." width="9.99%" src="https://foruda.gitee.com/images/1663602645591652493/0d2c22bc_5631341.jpeg">](https://gitee.com/vicky1980/siger/issues/I5RWX7) [<img title="281." width="9.99%" src="https://foruda.gitee.com/images/1663241206183918576/3be539aa_5631341.jpeg">](../../RISC-V/SIGer%20少年芯，从北向南的开源文化之旅.md#开源观止第四期) [<img title="293." width="9.99%" src="https://foruda.gitee.com/images/1663638776464580616/2d0863f2_5631341.jpeg">](https://gitee.com/vicky1980/siger/issues/I5RVD2) [<img title="297." width="9.99%" src="https://foruda.gitee.com/images/1663726282393288175/fc91d3c2_5631341.jpeg">](https://gitee.com/vicky1980/siger/issues/I5S6SJ)

为学习笔记加脚注！#299 期，科创献礼的终极 “全民开源素养计划” 的总实施！ :pray: SIGer 终于可以从 Gitee 脚手架上开始起飞啦！

[![输入图片说明](https://foruda.gitee.com/images/1663532320145389487/5d2d4686_5631341.png "屏幕截图")](免费打卡零壹航天基地看真火箭.md)

> 全国科普日，怎么能少了航天人的身影。

  - [全国科普日丨免费打卡零壹航天基地，看真火箭怎么造](http://mp.weixin.qq.com/s?__biz=MzA4NDI0NjcyMg==&mid=2247487281&idx=1&sn=2ce26a4d02accf33797609d9d3aa5463)
  - [“全民国防教育宣传月”丨走进零壹航天科普基地，点亮少年航天梦](http://www.liangjiang.gov.cn/Content/2022-09/19/content_10416980.htm)

# [921，就爱你，中国载人航天三十而立](https://view.inews.qq.com/wxn/WXN20220921000179060)

中央广播电视总台 2022-09-21 09:37:03

<p align="center"><a href="https://www.bilibili.com/video/BV1Dt4y1A7A3" target="_blank"><img src="https://foruda.gitee.com/images/1663756515487725016/bddeead7_5631341.png"></a></p>

<p align="center">（视频制作：总台空天逐梦融媒体创意工作室）B站：<a href="https://www.bilibili.com/video/BV1Dt4y1A7A3" target="_blank">BV1Dt4y1A7A3</a></p>

<p align="center">三十年栉风沐雨<br>
<br>
三十年砥砺奋进<br>
<br>
三十年初心不改<br>
<br>
三十年书写忠诚<br>
<br>
从1992年9月21日<br>
<br>
载人航天工程立项实施以来<br>
<br>
一代代航天人接力奋斗<br>
<br>
用心血、时间和智慧<br>
<br>
铸造了飞天梦想<br>
<br>
“而立之年”的中国载人航天<br>
<br>
将继续<br>
<br>
在科学的大道上攻坚克难<br>
<br>
在时代的浪潮里扬帆起航<br>
<br>
<img src="https://foruda.gitee.com/images/1663754711443962496/9c505fd8_5631341.png"><br>
<br>
一梦一诺一征程<br>
<br>
且吟且行且乘风<br>
<br>
让我们穿越三十载<br>
<br>
见证伟大工程变迁</p>

### BGM《[如愿](https://www.bilibili.com/video/BV1i34y157Le)》B站：BV1i34y157Le

> [2022七夕晚会](https://tv.cctv.com/2022/08/04/VIDENPLIpVdhmFm9N1uxn2Yd220804.shtml) 歌曲《如愿》 演唱：周深

如果写一首诗

我更想写一首给你们的诗

一首敢闯敢为的诗

一首高歌猛进的诗

一首朴素的诗

一首从容的诗

一首希望的诗

自由而坚定的诗

含蓄而广阔的诗

圆梦九天

如愿以偿的诗

> [如愿（电影《我和我的父辈》主题推广曲）](https://music.163.com/#/mv?id=14360887)

- 作曲：钱雷
- 作词：唐恬
- 原唱：王菲

你是 遥遥的路

山野大雾里的灯

我是孩童啊 走在你的眼眸

你是 明月清风

我是你照拂的梦

见与不见都一生 与你相拥

而我将 爱你所爱的人间

愿你所愿的笑颜

你的手我蹒跚在牵

请带我去明天

如果说 你曾苦过我的甜

我愿活成你的愿

愿不枉啊 愿勇往啊

这盛世每一天

你是 岁月长河

星火燃起的天空

我是仰望者 就把你唱成歌

你是 我之所来

也是我心之所归

世间所有路都将 与你相逢

而我将 爱你所爱的人间

愿你所愿的笑颜

你的手我蹒跚在牵

请带我去明天

如果说 你曾苦过我的甜

我愿活成你的愿

愿不枉啊 愿勇往啊

这盛世每一天

山河无恙 烟火寻常

可是你如愿的眺望

孩子们啊 安睡梦乡

像你深爱的那样

而我将 梦你所梦的团圆

愿你所愿的永远

走你所走的长路

这样的爱你啊

我也将 见你未见的世界

写你未写的诗篇

天边的月 心中的念

你永在我身边

与你相约 一生清澈

如你年轻的脸

# [北京主场，科普向未来，2022年全国科普日活动来啦！](https://kepu.gmw.cn/2022-09/14/content_36024075.htm)

来源：[北京科协](https://www.bast.net.cn/) 2022-09-14 12:16

![输入图片说明](https://foruda.gitee.com/images/1663751650823498447/51c65472_5631341.png "屏幕截图")

-  **自立自强** ：科学家精神主题展
-  **光年深处** ：探索宇宙深空奥秘
-  **走进天宫** ：体验空间站精彩生活
-  **进校进馆** ：科普资源助力双减
-  **京津冀青少年** ：科技创新交流
-  **科技馆之城** ：走进千馆扫码打卡

> 主办单位：北京市全民科学素质纲要实施工作办公室

　　科普向未来！9月15日至30日，2022 年北京市全国科普日暨第十二届北京科学嘉年华活动火热来袭。作为全国科普日北京主场的标志性活动，北京科学嘉年华自 2011 年以来已经成功举办十一届。本届北京科学嘉年华突出弘扬科学精神，激发创新活力；聚焦重点领域，服务高质量发展；深化文明实践，培育时代新风；立足群众所需，赋能基层治理。动员和组织各级科协组织，以及首都地区科技场馆、学校、科研院所、企事业单位等开展主题科普活动，带动全市群众性科普活动广泛开展。为首都公众搭建了学习、体验、感受科学的舞台，让科普日活动变得更加接地气、更具吸引力，使公众在互动体验中快乐地享受科学，在北京宜人的秋天里度过一个盛大的科学节日。

　　本届嘉年华主要由全国科普日北京主场活动、首都科普联合行动、北京云端科学嘉年华三大板块组成。在北京科学中心的主场活动中，观众不仅可以参观科学家精神主题展系列活动、航天主题教育活动，“走进天宫”体验空间站精彩生活，还有机会参与各式各样新奇有趣的科学实践、科技创新活动等。

　　此外，包括全国科普日北京主场活动、首都科普联合行动在内的诸多优质科普内容也同步登陆北京云端科学嘉年华。届时，公众足不出户便可“云”享24小时不落幕的北京科学嘉年华，畅游科技馆之城。（北京市科协融媒体中心）

- [北京市全民科学素质纲要实施工作办公室召开工作会](https://mp.weixin.qq.com/s?__biz=MzU1NzcwOTY5NA==&mid=2247547258&idx=1&sn=3fa83b8c88ba7e2417025eab1b9956f5)

  ![输入图片说明](https://foruda.gitee.com/images/1663752518907205131/175c6585_5631341.png "屏幕截图")

  -  **微信号** ：[北京科协](https://www.bast.net.cn) BAST-2018
  -  **功能介绍** ：根植首都地区的创新文化价值观察和科学叙事。弘扬科学精神，传播科学方法，服务国际科技创新中心建设。

  ![输入图片说明](https://foruda.gitee.com/images/1663752630101175835/88ae7972_5631341.gif "640 (28).gif")

  - 《[北京市全民科学素质行动规划纲要（2021—2035年）](http://www.beijing.gov.cn/zhengce/zhengcefagui/202203/t20220313_2629431.html)》（简称《北京科学素质纲要》）
  - [北京市科学技术协会 新媒体传播体系 政务新媒体矩阵](https://foruda.gitee.com/images/1663752852739517476/dd076f61_5631341.png)
  - [官方微信公众号 & 官方网站](https://foruda.gitee.com/images/1663752826887779719/b320cf87_5631341.png)：https://www.bast.net.cn

---

- 提交于 2022-09-21（23）commits

  - update sig/xiaoyuan/八一少年行-航天创新大赛.md. MARK #赛项解读
    - 提交于 9 分钟 https://gitee.com/flame-ai/siger/commit/4107cd3d695506f4933fd38a1f26065b42d5fb6b
  - update sig/xiaoyuan/八一少年行-航天创新大赛.md. 耿赛猛老师分享：火星家园赛项解读
    - 提交于 12 分钟 https://gitee.com/flame-ai/siger/commit/64e8d86ec2fe002dfbd6ffbab573ab8f67cb5f02
  - update 2022 海报集.md. sig/遥望比邻/载人航天30而立SIGer科普周庆贺.md
    - 提交于 1 小时 https://gitee.com/flame-ai/siger/commit/0c8fc6ee75db03a5069f595abe6ed999ffc21ffd
  - update sig/遥望比邻/载人航天30而立SIGer科普周庆贺.md. 科创少年在行动 SIGer 专题献礼科普周
    - 提交于 1 小时 https://gitee.com/flame-ai/siger/commit/19745591718fa10a9d0896ae5131707e97993f59
  - update sig/遥望比邻/载人航天30而立SIGer科普周庆贺.md. 20220616 如愿 B站：BV1i34y157Le
    - 提交于 2 小时 https://gitee.com/flame-ai/siger/commit/bd8446f02fd5bbbaa008a629b5defb9994dd794c
  - update sig/遥望比邻/载人航天30而立SIGer科普周庆贺.md. BGM《如愿》周琛
    - 提交于 2 小时 https://gitee.com/flame-ai/siger/commit/35b677a40567b8a08b78876930b56090a04fec5b
  - update sig/遥望比邻/载人航天30而立SIGer科普周庆贺.md. 更新视频封面！
    - 提交于 2 小时 https://gitee.com/flame-ai/siger/commit/bcc7332644cfbf603150f3a39ee55faf6338d77e
  - update sig/遥望比邻/载人航天30而立SIGer科普周庆贺.md. center center.
    - 提交于 3 小时 https://gitee.com/flame-ai/siger/commit/6fc61fbfca3f1d3cd3cf9453971681652d8a8b9c
  - update sig/遥望比邻/载人航天30而立SIGer科普周庆贺.md. B站：BV1Dt4y1A7A3
    - 提交于 3 小时 https://gitee.com/flame-ai/siger/commit/e560a7913e77ddff56a7b9d308c4cfafe3029aa3
  - update sig/遥望比邻/载人航天30而立SIGer科普周庆贺.md. 921，就爱你，中国载人航天三十而立
    - 提交于 3 小时 https://gitee.com/flame-ai/siger/commit/b059562f594f5fe3e7f5a7a3500d5dcc5dbc9711
  - update sig/遥望比邻/载人航天30而立SIGer科普周庆贺.md. 北京主场 & 北京科协
    - 提交于 3 小时 https://gitee.com/flame-ai/siger/commit/894925affc1899300be805329c69dc71f95da3e1
  - update sig/遥望比邻/载人航天30而立SIGer科普周庆贺.md. 北京市全民科学素质行动规划纲要
    - 提交于 3 小时 https://gitee.com/flame-ai/siger/commit/32a35ebcb6f23eb9c5973daaa883fd9472b3984f
  - update sig/遥望比邻/载人航天30而立SIGer科普周庆贺.md. 全国科普日北京主场活动来啦！北京市全民科学素质纲要实施工作办公室，威武！:+1:
    - 提交于 3 小时 https://gitee.com/flame-ai/siger/commit/bb6c1388aaf621950c285c27d1182883943d636e
  - update sig/遥望比邻/载人航天30而立SIGer科普周庆贺.md. 全国科普日，怎么能少了航天人的身影。走进零壹航天科普基地，点亮少年航天梦
    - 提交于 4 小时 https://gitee.com/flame-ai/siger/commit/fa3a2bd5467a3f28fd7882d30761f828baf593a8
  - update sig/遥望比邻/免费打卡零壹航天基地看真火箭.md. 走进零壹航天科普基地，点亮少年航天梦
    - 提交于 4 小时 https://gitee.com/flame-ai/siger/commit/3e01854d3296e2d6ae781224f516daf9753f0605
  - update sig/遥望比邻/载人航天30而立SIGer科普周庆贺.md. 为学习笔记加脚注！ “全民开源素养计划” 的总实施！#299 期见！
    - 提交于 4 小时 https://gitee.com/flame-ai/siger/commit/a4812254a6b6406bd2096970a01ba9ce0f25d512
  - update sig/遥望比邻/载人航天30而立SIGer科普周庆贺.md. 合并 双封面 刊内标签 的2期 和新立项的 4期，预报 SIG…
    - 提交于 4 小时 https://gitee.com/flame-ai/siger/commit/b188790c35f3491b83528d3c7a3f5a52cddd76ac
  - update sig/遥望比邻/载人航天30而立SIGer科普周庆贺.md. 航天及其他主题统计：从星际航行到载人航天三十而立，最好的科普周礼物。
    - 提交于 4 小时 https://gitee.com/flame-ai/siger/commit/575f1b5521e5b1e7992193f0789b8fa610291ca5
  - update sig/遥望比邻/载人航天30而立SIGer科普周庆贺.md. 12期成刊统计：6期 科创主题；6期 航天主题及其他
    - 提交于 5 小时 https://gitee.com/flame-ai/siger/commit/c95f88cdb7127d227d4d4deaf823d27089d89095
  - update sig/遥望比邻/载人航天30而立SIGer科普周庆贺.md. 12. 2. 4. 分开期刊
    - 提交于 5 小时 https://gitee.com/flame-ai/siger/commit/2418202f08c50f956d2b3146967f8115e3f85568
  - update sig/遥望比邻/载人航天30而立SIGer科普周庆贺.md. 期刊统计：12期 .md 成刊；2期 双封面 刊内标签；4期 issues 立项
    - 提交于 5 小时 https://gitee.com/flame-ai/siger/commit/c93f4bdb3d7a8a4c7ac7773c52e179c47bd25382
  - update sig/遥望比邻/载人航天30而立SIGer科普周庆贺.md. 280. ~ 297. 18s SIGer issues ../../
    - 提交于 6 小时 https://gitee.com/flame-ai/siger/commit/fe7e07ba81788965fa08cc44758695a87f42e471
  - add sig/遥望比邻/载人航天30而立SIGer科普周庆贺.md.
    - 提交于 6 小时 https://gitee.com/flame-ai/siger/commit/e6525cd5e3b51069644adde0800b72c5bcfabdad