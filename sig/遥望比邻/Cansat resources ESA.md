[Cansat resources](https://www.esa.int/Education/CanSat/Cansat_resources)

[ESA](https://www.esa.int/) / [Education](https://www.esa.int/Education) / [CanSat](https://www.esa.int/Education/CanSat)

ESA Education, in collaboration with ESERO, have produced these CanSat-related classroom resources in order to support students and teachers’ participation in the European CanSat Competition.

- [Meet Arduino!](https://www.esa.int/Education/CanSat/Meet_Arduino!_Introduction_to_Arduino_computing_using_C_Teach_with_Space_T04.1)
Introduction to Arduino computing using C++ | Teach with Space T04.1

More details

- [Getting Started with CanSat](https://www.esa.int/Education/CanSat/Getting_Started_with_CanSat_A_Guide_to_the_Primary_Mission_Teach_with_Space_T08)
A Guide to the Primary Mission | Teach with Space T08

More details

- [Design your parachute](https://www.esa.int/Education/CanSat/Design_your_parachute_A_Guide_to_Landing_Your_CanSat_Safely_Teach_with_Space_T10)
A Guide to Landing Your CanSat Safely | Teach with Space T10

More details

- [Comms with Radio](https://www.esa.int/Education/CanSat/Communicating_with_Radio_Ground_Control_to_Major_CanSat_Teach_with_Space_T11)
Ground Control to Major CanSat | Teach with Space T11

<p><img width="706px" src="https://foruda.gitee.com/images/1669018199926646038/4bed6f5d_5631341.jpeg" title="457ESAcansat.jpg"></p>

# [Meet Arduino! – Introduction to Arduino computing using C++ | Teach with Space T04.1](https://www.esa.int/Education/CanSat/Meet_Arduino!_Introduction_to_Arduino_computing_using_C_Teach_with_Space_T04.1)

- 5120 VIEWS
- 31 LIKES
- ESA / Education / CanSat
- Subjects: Programming, Electronics

Students will explore technology used in space through the Arduino tool. They will build circuits to blink an LED and to measure temperature, pressure and altitude. The basics of programming in C++ will be introduced using the Arduino IDE (Integrated Development Environment) software. This set of activities could be the starting point for the future participation in the European CanSat Competition.

- Format: Teacher Guide and Student Worksheets
- Age range: 14 – 20 years old
- Keywords: Arduino, Sensor, Code

Download: [Meet Arduino!](https://esamultimedia.esa.int/docs/edu/T04.1_Meet_Arduino_C.pdf)

This classroom resource is part of a set of resources developed by ESA’s Education Office in collaboration with ESEROs to support the European CanSat Competition.

<p><img width="19%" src="https://foruda.gitee.com/images/1669023021619233550/1986e4bc_5631341.jpeg" title="T04_1_Meet_Arduino_C-1.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669023039434948213/eb3dee7d_5631341.jpeg" title="T04_1_Meet_Arduino_C-2.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669023053627887551/23c9f49b_5631341.jpeg" title="T04_1_Meet_Arduino_C-3.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669023068793404791/762e40e1_5631341.jpeg" title="T04_1_Meet_Arduino_C-4.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669023083606165732/1e75dc7b_5631341.jpeg" title="T04_1_Meet_Arduino_C-5.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669023098791405172/65d47bc2_5631341.jpeg" title="T04_1_Meet_Arduino_C-6.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669023112789379171/a990da22_5631341.jpeg" title="T04_1_Meet_Arduino_C-7.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669023127932214505/463d7c81_5631341.jpeg" title="T04_1_Meet_Arduino_C-8.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669023143902883531/64d8aed0_5631341.jpeg" title="T04_1_Meet_Arduino_C-9.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669023159925303107/94a93e85_5631341.jpeg" title="T04_1_Meet_Arduino_C-10.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669023175230278726/3ca41162_5631341.jpeg" title="T04_1_Meet_Arduino_C-11.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669023191304879761/a7829268_5631341.jpeg" title="T04_1_Meet_Arduino_C-12.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669023208104816155/5693d6e9_5631341.jpeg" title="T04_1_Meet_Arduino_C-13.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669023225767167184/df36c17c_5631341.jpeg" title="T04_1_Meet_Arduino_C-14.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669023238291587517/845ac94c_5631341.jpeg" title="T04_1_Meet_Arduino_C-15.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669023255030187795/82b5ab7c_5631341.jpeg" title="T04_1_Meet_Arduino_C-16.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669023278571483605/3c11013b_5631341.jpeg" title="T04_1_Meet_Arduino_C-17.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669023295534855625/4333a02f_5631341.jpeg" title="T04_1_Meet_Arduino_C-18.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669023312265089224/6e8c5164_5631341.jpeg" title="T04_1_Meet_Arduino_C-19.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669023327813023474/ce3b459e_5631341.jpeg" title="T04_1_Meet_Arduino_C-20.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669023342253401142/8fd1e9fa_5631341.jpeg" title="T04_1_Meet_Arduino_C-21.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669023359465218543/2eb6bc1f_5631341.jpeg" title="T04_1_Meet_Arduino_C-22.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669023377655603965/6dfc634f_5631341.jpeg" title="T04_1_Meet_Arduino_C-23.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669023394833678251/6e1a1075_5631341.jpeg" title="T04_1_Meet_Arduino_C-24.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669023414604656071/268e0581_5631341.jpeg" title="T04_1_Meet_Arduino_C-25.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669023432819067581/3b2d3746_5631341.jpeg" title="T04_1_Meet_Arduino_C-26.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669023461702247752/d70f98ec_5631341.jpeg" title="T04_1_Meet_Arduino_C-27.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669023483169963415/7599a793_5631341.jpeg" title="T04_1_Meet_Arduino_C-28.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669023501519223579/6047e8d4_5631341.jpeg" title="T04_1_Meet_Arduino_C-29.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669023526416876774/ee21367d_5631341.jpeg" title="T04_1_Meet_Arduino_C-30.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669023542921818725/15da9ef7_5631341.jpeg" title="T04_1_Meet_Arduino_C-31.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669023559739155020/08e065ce_5631341.jpeg" title="T04_1_Meet_Arduino_C-32.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669023574780791809/21b86839_5631341.jpeg" title="T04_1_Meet_Arduino_C-33.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669023591610436206/9db3354c_5631341.jpeg" title="T04_1_Meet_Arduino_C-34.jpg"></p>

# [Getting Started with CanSat – A Guide to the Primary Mission | Teach with Space T08](https://www.esa.int/Education/CanSat/Getting_Started_with_CanSat_A_Guide_to_the_Primary_Mission_Teach_with_Space_T08)

- 6011 VIEWS
- 21 LIKES
- ESA / Education / CanSat
- Subjects: Physics, Electronics, Programming, Mathematics |

This module outlines the main features of the Primary Mission for CanSat. In the Primary Mission, teams must measure the temperature and pressure and send the information to their ground station. Students will learn about the differences between the sensors they can use and about the challenges associated with completing the Primary Mission. This module is designed in line with a range of resources to support the entire CanSat mission.

- Format: Teacher Guide and Student Worksheets
- Age range: 14 - 20 years old
- Keywords: Sensors, Resistor, Radio, Communication, Protocols, Soldering, CanSat

Download: [Getting Started with CanSat](https://esamultimedia.esa.int/docs/edu/T08_Getting_Started_with_CanSat.pdf)

This classroom resource is part of a set of resources developed by ESA’s Education Office in collaboration with ESEROs to support the European CanSat Competition.

![输入图片说明](https://foruda.gitee.com/images/1668962295837843107/0418db2c_5631341.jpeg "T08_Getting_Started_with_CanSat-1.jpg")

<p><img width="19%" src="https://foruda.gitee.com/images/1668962306085978941/8e3de5a8_5631341.jpeg"> <img width="19%" src="https://foruda.gitee.com/images/1668962316531843089/98fb7eaa_5631341.jpeg"> <img width="19%" src="https://foruda.gitee.com/images/1668962328068694412/f44fd79d_5631341.jpeg"> <img width="19%" src="https://foruda.gitee.com/images/1668962340724187953/789bd52d_5631341.jpeg"> <img width="19%" src="https://foruda.gitee.com/images/1668962353571707502/01ca3e6a_5631341.jpeg"> <img width="19%" src="https://foruda.gitee.com/images/1668962371250650241/cddb5a8b_5631341.jpeg"> <img width="19%" src="https://foruda.gitee.com/images/1668962383541849009/d809088d_5631341.jpeg"> <img width="19%" src="https://foruda.gitee.com/images/1668962394397428930/5d93acde_5631341.jpeg"> <img width="19%" src="https://foruda.gitee.com/images/1668962405517308230/498cd494_5631341.jpeg"> <img width="19%" src="https://foruda.gitee.com/images/1668962416087491789/630912cf_5631341.jpeg"> <img width="19%" src="https://foruda.gitee.com/images/1668962429999666784/70b8612e_5631341.jpeg"> <img width="19%" src="https://foruda.gitee.com/images/1668962443793532810/efe8f4ab_5631341.jpeg"> <img width="19%" src="https://foruda.gitee.com/images/1668962455906457741/a68025ac_5631341.jpeg"> <img width="19%" src="https://foruda.gitee.com/images/1668962467956137742/5b101178_5631341.jpeg"> <img width="19%" src="https://foruda.gitee.com/images/1668962481214236228/46d5b804_5631341.jpeg"> <img width="19%" src="https://foruda.gitee.com/images/1668962493050192716/ccd51d06_5631341.jpeg"> <img width="19%" src="https://foruda.gitee.com/images/1668962506114069303/8c4a3b08_5631341.jpeg"> <img width="19%" src="https://foruda.gitee.com/images/1668962520238170839/c0a549f0_5631341.jpeg "T08_Getting_Started_with_CanSat-19.jpg")![输入图片说明](https://foruda.gitee.com/images/1668962557566939379/072d2cf9_5631341.jpeg"> <img width="19%" src="https://foruda.gitee.com/images/1669019285135274731/3ebe915b_5631341.jpeg">  <img width="19%" src="https://foruda.gitee.com/images/1668962570206282306/1e7aa3e8_5631341.jpeg"> <img width="19%" src="https://foruda.gitee.com/images/1668962582391493374/c898b1e2_5631341.jpeg"> <img width="19%" src="https://foruda.gitee.com/images/1668962594314367686/44fd2c03_5631341.jpeg"> <img width="19%" src="https://foruda.gitee.com/images/1668962609157741905/ce2c6459_5631341.jpeg"> <img width="19%" src="https://foruda.gitee.com/images/1668962621022315769/d79a83e1_5631341.jpeg"> <img width="19%" src="https://foruda.gitee.com/images/1668962632976027393/c576248f_5631341.jpeg"></p>

# [Design your parachute – A Guide to Landing Your CanSat Safely | Teach with Space T10](https://www.esa.int/Education/CanSat/Design_your_parachute_A_Guide_to_Landing_Your_CanSat_Safely_Teach_with_Space_T10)

- 12195 VIEWS
- 69 LIKES
- ESA / Education / CanSat
- Subjects: Physics, Mathematics, Design |

This resource gives students a brief overview of the different options available when building their CanSat parachute. Students will learn about the underlying physics of parachutes and their design and how to control the speed of their CanSat.

- Format: Teacher Guide and Student Worksheets
- Age range: 14 – 20 years old
- Keywords: Parachute, Drag, Air Resistance, Gravity, Weight, CanSat

Download: [Design your parachute](https://esamultimedia.esa.int/docs/edu/T10_Parachute_Design.pdf)

This classroom resource is part of a set of resources developed by ESA’s Education Office in collaboration with ESEROs to support the European CanSat Competition.

<p><img width="49%" src="https://foruda.gitee.com/images/1669033420170253492/6f3249de_5631341.jpeg" title="T10_Parachute_Design-1.jpg"> <img width="49%" src="https://foruda.gitee.com/images/1669033984722686338/5483dd0b_5631341.jpeg" title="T10_Parachute_Design-2.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669033443798997584/af196b49_5631341.jpeg" title="T10_Parachute_Design-3.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669033454837959105/ec791828_5631341.jpeg" title="T10_Parachute_Design-4.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669033469192876152/3187b1a5_5631341.jpeg" title="T10_Parachute_Design-5.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669033483275112216/dbf0ffaf_5631341.jpeg" title="T10_Parachute_Design-6.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669033495004521171/df112477_5631341.jpeg" title="T10_Parachute_Design-7.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669033506097058163/a9a56da4_5631341.jpeg" title="T10_Parachute_Design-8.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669033520248052174/f27d93c5_5631341.jpeg" title="T10_Parachute_Design-9.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669033532131565471/850c992b_5631341.jpeg" title="T10_Parachute_Design-10.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669033565278176507/2f45d733_5631341.jpeg" title="T10_Parachute_Design-11.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669033578486105114/2237a4ab_5631341.jpeg" title="T10_Parachute_Design-12.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669033590997678095/52dd4a3c_5631341.jpeg" title="T10_Parachute_Design-13.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669033604401040980/b830a267_5631341.jpeg" title="T10_Parachute_Design-14.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669033616442690880/5b8abbb9_5631341.jpeg" title="T10_Parachute_Design-15.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669033630477476173/9f2e0c9d_5631341.jpeg" title="T10_Parachute_Design-16.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669033645151516087/4048084f_5631341.jpeg" title="T10_Parachute_Design-17.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669033659834528725/b7dc57ca_5631341.jpeg" title="T10_Parachute_Design-18.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669033673397798559/8ed59fe6_5631341.jpeg" title="T10_Parachute_Design-19.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669033685428334967/c1de8c78_5631341.jpeg" title="T10_Parachute_Design-20.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669033707571577881/c158b277_5631341.jpeg" title="T10_Parachute_Design-21.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669033724128871914/b0cccec2_5631341.jpeg" title="T10_Parachute_Design-22.jpg"></p>

# [Communicating with Radio – Ground Control to Major CanSat | Teach with Space T11](https://www.esa.int/Education/CanSat/Communicating_with_Radio_Ground_Control_to_Major_CanSat_Teach_with_Space_T11)

- 5323 VIEWS
- 17 LIKES
- ESA / Education / CanSat
- Subjects: Physics, Electronics

To understand how everyday devices like mobile phones, routers and satellites work, we need to understand what radio waves are and how we can transmit information with them. Radio communication is one of the key elements in our CanSat. All the data needed for our scientific experiment will be sent from the CanSat to our ground station via radio waves once the CanSat is launched.

- Format: Teacher Guide and Student Worksheets
- Age Range: 14 – 20 years old
- Keywords: Radio, Communications, Wavelength, Frequency, Spectrum, CanSat

Download: [Communicating with Radio](https://esamultimedia.esa.int/docs/edu/T11_Radio_Communication.pdf)

This classroom resource is part of a set of resources developed by ESA’s Education Office in collaboration with ESEROs to support the European CanSat Competition.

<p><img width="19%" src="https://foruda.gitee.com/images/1669034229739812334/5db6d0b3_5631341.jpeg" title="T11_Radio_Communication-1.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669034271981900436/d009af59_5631341.jpeg" title="T11_Radio_Communication-2.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669034292895645757/3e31858e_5631341.jpeg" title="T11_Radio_Communication-3.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669034309395596406/40d573e6_5631341.jpeg" title="T11_Radio_Communication-4.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669034324895387928/87951810_5631341.jpeg" title="T11_Radio_Communication-5.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669034340254361239/5510827e_5631341.jpeg" title="T11_Radio_Communication-6.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669034356828155662/ff079c33_5631341.jpeg" title="T11_Radio_Communication-7.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669034372259056229/a6473e6a_5631341.jpeg" title="T11_Radio_Communication-8.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669034389473162402/6bff3df8_5631341.jpeg" title="T11_Radio_Communication-9.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669034408233907109/876a2e2c_5631341.jpeg" title="T11_Radio_Communication-10.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669034423687706299/2ca3ec99_5631341.jpeg" title="T11_Radio_Communication-11.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669034443692472586/989974d7_5631341.jpeg" title="T11_Radio_Communication-12.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669034460777267344/bed603fa_5631341.jpeg" title="T11_Radio_Communication-13.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669034483739043509/1b4305db_5631341.jpeg" title="T11_Radio_Communication-14.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669034498948347858/37d40f3c_5631341.jpeg" title="T11_Radio_Communication-15.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669034513692042296/c3dca6eb_5631341.jpeg" title="T11_Radio_Communication-16.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669034528809060289/3eb3e992_5631341.jpeg" title="T11_Radio_Communication-17.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669034558492221186/a3966dfe_5631341.jpeg" title="T11_Radio_Communication-18.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669034576563960244/7e6d0cbb_5631341.jpeg" title="T11_Radio_Communication-19.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669034595552594730/d802353d_5631341.jpeg" title="T11_Radio_Communication-20.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669034615047253422/c0829bce_5631341.jpeg" title="T11_Radio_Communication-21.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669034632989590615/d8d060d4_5631341.jpeg" title="T11_Radio_Communication-22.jpg"> <img width="19%" src="https://foruda.gitee.com/images/1669034646525179568/8f0e8430_5631341.jpeg" title="T11_Radio_Communication-23.jpg"></p>