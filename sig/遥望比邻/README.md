# 业余卫星通信 & CANSAT

[<img title="444." width="19%" src="https://foruda.gitee.com/images/1668436775016796528/a9ae0fac_5631341.jpeg">](https://mp.weixin.qq.com/s?__biz=Mzg2MjYyMTg1Mw==&mid=2247485703&idx=1&sn=4d4763f4f41a015de191bf9aec485d0c) [<img title="453." width="19%" src="https://foruda.gitee.com/images/1668868573964922685/7979923c_5631341.jpeg">](../103/SRRC.md) [<img title="5." width="19%" src="https://images.gitee.com/uploads/images/2022/0114/032757_950fc5ac_5631341.jpeg">](../../%E7%AC%AC5%E6%9C%9F%20CANSAT%20%E6%88%91%E7%9A%84%E8%88%AA%E5%A4%A9%E6%A2%A6.md) [<img title="457." width="19%" src="https://foruda.gitee.com/images/1669018199926646038/4bed6f5d_5631341.jpeg">](Cansat%20resources%20ESA.md) [<img title="458." width="19%" src="https://foruda.gitee.com/images/1669051267363749104/d6452d14_5631341.jpeg">](CANSAT%20arduino.md)

- [数字卫星通联！使用哈工大自制的小卫星完成无线电通话！](https://www.bilibili.com/video/av11860049)
- [清华招生宣传片《追光少年》荣获央视“你好，新时代”融媒体作品大赛最高奖](https://www.sohu.com/a/447140361_397252)

# 月球环境保护问题研究 —— 法国航空航天研究院 AAE 的专题研讨

the AAE International Conference "Space Exploration"
(https://academieairespace.com/space-exploration/)

![输入图片说明](https://foruda.gitee.com/images/1667268261252441904/b85b53f7_5631341.png "屏幕截图")

汇报人：果琳丽（中国区召集人）
AAE Lunar Environment workshop 
2022.10

<p><img width="13.59%" src="https://foruda.gitee.com/images/1667273353446787921/aa2e7a7b_5631341.png"> <img width="13.59%" src="https://foruda.gitee.com/images/1667277261561243177/5bf7bec0_5631341.png"> <img width="13.59%" src="https://foruda.gitee.com/images/1667277364479890098/1dc2636c_5631341.png"> <img width="13.59%" src="https://foruda.gitee.com/images/1667278764043798858/8134f036_5631341.png"> <img width="13.59%" src="https://foruda.gitee.com/images/1667277873103091431/12be3f14_5631341.png"> <img width="11.5%" src="https://foruda.gitee.com/images/1667277983175552472/018778ef_5631341.png"> <img width="13.59%" src="https://foruda.gitee.com/images/1667278059554683994/40acb4d4_5631341.png"></p>

1. Philosophy and human motivations: [太空探索的哲学和人类动机](https://academieairespace.com/space-exploration/2022/06/22/philosophy-and-human-motivations-for-space-exploration/)
2. Water: Both a scientific and engineering issue. [水：既是一个科学问题，也是一个工程问题。](https://academieairespace.com/space-exploration/2022/06/21/water-both-a-scientific-and-engineering-issue/)
3. Going Faster: [跑得更快](https://academieairespace.com/space-exploration/2022/06/21/going-faster/)
4. Flying machines: [飞行器](https://academieairespace.com/space-exploration/2022/06/21/flying-machines/)
5. Lunar environment: [月球环境](https://academieairespace.com/space-exploration/2022/07/29/lunar-environment/) 
6. Humans and robots: [人类和机器人](https://academieairespace.com/space-exploration/2022/06/21/humans-and-robots/)
7. Dream missions: [梦想任务](https://academieairespace.com/space-exploration/2022/06/21/dream-missions/)

<p><img width="13.59%" src="https://foruda.gitee.com/images/1667265834340578430/9bd7fb74_5631341.png"> <img width="13.59%" src="https://foruda.gitee.com/images/1667265840886992342/595b95f0_5631341.png"> <img width="13.59%" src="https://foruda.gitee.com/images/1667265846590791162/9cca8174_5631341.png"> <img width="13.59%" src="https://foruda.gitee.com/images/1667265851691691704/d4fea3f0_5631341.png"> <img width="13.59%" src="https://foruda.gitee.com/images/1667265857039789499/3420440c_5631341.png"> <img width="13.59%" src="https://foruda.gitee.com/images/1667265860690782463/f8a6cd67_5631341.png"> <img width="13.59%" src="https://foruda.gitee.com/images/1667265865592628359/5a609109_5631341.png"></p>

- 【公开招募】[月球环境保护问题公开招募学术研究员](https://mp.weixin.qq.com/s?__biz=MzI2NzA3ODAyMg==&mid=2651005820&idx=1&sn=157f3185d098501cf66c5f105898c22f) 星际航行 2022-10-24 14:59 发表于北京

  由法国航空航天研究院（Air and space Academy, AAE）组织发起的月球环境保护研讨组，正在全球范围内公开招募学术研究员，参与有关月球环境保护问题的研讨。正式研讨会计划在2023年5月11日的法国AAE发起的“空间探索”论坛上举行，本次会议计划采取线上+线下的研讨方式。考虑到月球环境保护问题的多学科交叉特点，中国区的活动计划采用公开线上的方式进行，广泛征求各专业各学科人员的意见，凝聚中国智慧。具体内容如下：

  - 法国AAE发起的 “空间探索” 论坛简介

    - 专题5主题：月球环境保护

      中国区活动总目标：

      1. 考虑到月球环境保护问题的多学科交叉特点，拟广泛征求各专业各学科意见，凝聚中国智慧；
      2. 推荐若干中国专业技术人员专题报告参加AAE会议；
      3. 筹备中国区学术委员会。

  - 问题1： 如何尽量减少月球居住系统中废物的产生？

    - 地月空间站会产生哪些废物？
    - 月球基地会产生哪些废物？
    - 无人及有人基地的废物管理系统是什么样的？
    - 有什么样的新系统、新技术？
    - 其它

  - 问题2：月球附近轨道的碎片管理问题？

    - 月球附近可用的轨道有那些？
    - 月球轨道上是否会发生碰撞行为？
    - 如何进行交通管理和避免碰撞？
    - 月球轨道上失控的空间碎片是否会撞击月球表面基地？
    - 其他

  - 问题3：月球采矿和运输活动产生的环境问题？

    - 月球上采矿会带来那些环境污染及破坏问题？
    - 月表运输过程中会产生什么样的环境问题？
    - 月尘如何消散？是否会对月球景观和大气造成影响？
    - 地月空间运输中的末级火箭、无法重复使用的下降级、坠毁的上升级会产生什么样的环境问题？
    - 未来地月空间运输的物流量如何？是否会对地球环境产生影响？
    - 其它

  - 问题4： 月球农场及生物技术产生的环境问题？

    - 月球农场会是什么样的？生物孢子会如何传播？
    - 月球农场中的动植物技术是否会对月球的表面环境形成破坏？
    - 水、气等物质循环是否会对基础设施形成破坏？
    - 其它

  - 问题5：需要什么样的新规则新秩序？

    - 可能产生的有害干扰是什么？
    - 如何进行全球月球活动的有效监管和控制，确保安全操作？
    - 其它

[<img width="49%" src="https://foruda.gitee.com/images/1667281909762640903/3a6a59f4_5631341.jpeg" title="397月球环境保护.jpg">](【AAE专题】月球环境保护问题研究.md)
[<img width="49%" src="https://foruda.gitee.com/images/1667440887310973275/3e146a8d_5631341.jpeg" title="406月球村.jpg">](【AAE专题】太空探索的哲学和人类动机.md)

- [月球环境保护问题公开招募学术研究员](https://mp.weixin.qq.com/s?__biz=MzI2NzA3ODAyMg==&mid=2651005820&idx=1&sn=157f3185d098501cf66c5f105898c22f)
- [太空探索 国际会议 2023年5月-意大利都灵](https://academieairespace.com/space-exploration/)
- [太空探索的哲学和人类动机](https://academieairespace.com/space-exploration/2022/06/22/philosophy-and-human-motivations-for-space-exploration/)
- [月球遗址保护需采取“极端克制”的态度||国际月球村39](https://mp.weixin.qq.com/s?__biz=MzI2NzA3ODAyMg==&mid=2651005827&idx=1&sn=5f067957180e9da98099311a5acccc9d) 原创 果然伶俐 星际航行 2022-10-29 16:27 发表于北京

# [星际航行概论](星际航行概论.md)

[<img title="125." width="15.69%" src="https://images.gitee.com/uploads/images/2022/0529/175618_d258cbd7_5631341.jpeg">](星际航行概论.md) [<img title="367." width="15.69%" src="https://foruda.gitee.com/images/1666573241550740924/f4c27b17_5631341.jpeg">](1059.md) [<img title="392." width="15.69%" src="https://foruda.gitee.com/images/1667200390676940608/9ea4bf07_5631341.jpeg">](梦天就位！择机发射！.md) <a href="缅怀钱学森｜天才并非天生！.md" target="_blank"><img title="394." width="15.69%" src="https://foruda.gitee.com/images/1667234505025006377/af5a2a67_5631341.jpeg"></a> [<img title="407." width="15.69%" src="407梦天T字转位.gif">](梦天转位，中国T字空间站建成.md) [<img title="408." width="15.69%" src="https://foruda.gitee.com/images/1667646427980399796/3e4c52b4_5631341.jpeg">](缅怀钱学森，天宫即将建成！.md)

# [Open1059](open1059.md)

<p><a href="open1059.md" target="_blank"><img width="30%" src="https://foruda.gitee.com/images/1667198772246092057/9db54c69_5631341.jpeg" title="379RECT3X3副本01.jpg"> <img width="30%" src="https://foruda.gitee.com/images/1667198789129212461/d4ac7b99_5631341.jpeg" title="379RECT3X3副本02.jpg"> <img width="30%" src="https://foruda.gitee.com/images/1667198807513787576/603d36f3_5631341.jpeg" title="379RECT3X3副本03.jpg"> <img width="30%" src="https://foruda.gitee.com/images/1667198841827675150/89e0ed43_5631341.jpeg" title="379RECT3X3副本04.jpg"> <img width="30%" src="https://foruda.gitee.com/images/1667198856151945690/9716b12e_5631341.jpeg" title="379RECT3X3副本05.jpg"> <img width="30%" src="https://foruda.gitee.com/images/1667198870264531619/429f7a6e_5631341.jpeg" title="379RECT3X3副本06.jpg"> <img width="30%" src="https://foruda.gitee.com/images/1667198885974128544/79174210_5631341.jpeg" title="379RECT3X3副本07.jpg"> <img width="30%" src="https://foruda.gitee.com/images/1667198901157636142/0265a5b0_5631341.jpeg" title="379RECT3X3副本08.jpg"> <img width="30%" src="https://foruda.gitee.com/images/1667198914165890319/6947b874_5631341.jpeg" title="379RECT3X3副本09.jpg"></a></p>

# [遥望比邻 Dreams to Reality](https://mp.weixin.qq.com/s?__biz=MjM5NjUwMDYyMQ==&mid=2650528277&idx=1&sn=ee34fb34fffe902c8070060e27b31786)

- 这是个人的一小步，人类的一大步。
- That's one small step for a man, one giant leap for mankind.

[<img width="706px" src="https://images.gitee.com/uploads/images/2022/0614/185416_bcf3ee50_5631341.jpeg" title="遥望比邻DREAMTOREALITY副本.jpg">](遥望比邻%20Dreams%20to%20Reality.md)

> 星际航行的愿望在多少年前已经有人表示过，只不过那些都是空想，在以前我们没有实现星际航行的科学技术知识。  
> 但是现在不同了，由于火箭技术和高速飞行在近十几年来突飞猛进的发展，我们已经具有航行于星际空间的条件。  
> 在今天星际航行已经不是空想了，我们一定会在不久的将来，跳出地球引力的约束，像哥伦布一样地去发现星际空间的 “新大陆”。
> —— 钱学森

- **展览时间：** 2020年12月31日——2021年05月
- **展览地点：** 上海交通大学钱学森图书馆B1专题展厅

![输入图片说明](https://images.gitee.com/uploads/images/2022/0614/224742_be2033c2_5631341.png "屏幕截图.png")
