（注：译文来自网页自动翻译！校对工作由 @[starship](https://gitee.com/starshipedu) 完成，特此感谢！） 

- [Why fungi adapt so well to life in space](https://scienceline.org/2018/03/fungi-love-to-grow-in-outer-space/#)
- [为什么真菌能对太空生活适应得如此之好](#为什么真菌能对太空生活适应得如此之好)

<p><img width="706px" src="https://foruda.gitee.com/images/1676898006177260860/ac915cf4_5631341.gif" title="555真菌的太空生活.gif"></p>

SPACE, PHYSICS, AND MATH

## [Why fungi adapt so well to life in space](https://scienceline.org/2018/03/fungi-love-to-grow-in-outer-space/#)

>  _In many ways, these little microbes are better prepared for space travel than we are_ 

MATTHEW PHELAN • MARCH 7, 2018

![输入图片说明](https://foruda.gitee.com/images/1676825120666555060/9e5cd19f_5631341.png "屏幕截图")

> Crew members routinely take air, water and surface samples on the International Space Station to monitor changes in the microbiome for their personal health and safety. [Image credit: NASA | Public Domain]

Astronaut David Wolf made a disturbing discovery in 1997 onboard the Russian space station, Mir: behind a little-used service panel inside the Kvant-2 module, Wolf found “large condensate globs, bowling-ball size or beach-ball size.” These floating orbs of stagnant water, as he described them [in one NASA history](https://history.nasa.gov/SP-4225/nasa6/nasa6.htm), were “gooey, slimy, ice-cold,” occluded by dozens of species of bacteria and fungi. Worse still, the blobs — tenuously suspended in zero-gravity — weren’t benignly staying put inside this electrical box; they were “starting to track down the structure and into the wiring.”

Fungi nearly wrecked Mir on multiple occasions. For example, fungus found on one of Mir’s [Soyuz transports](https://www.nasa.gov/audience/forstudents/k-4/stories/nasa-knows/what-is-the-soyuz-spacecraft-k-4), the variety of spacecraft used to ferry personnel to and from the orbital platform, was once caught eating away at the hardened quartz glass of the vessel’s viewports. It etched webs of cracks and corroded the rubber seals connecting the windows to their titanium frames. Mold (a variety of fungus) became so widespread that cosmonauts and their international colleagues would return to Mir to discover that the whole place smelled like a basement full of rotten apples, as noted in the January 2016 issue of the Russian edition of  _Popular Mechanics_ .

Fungi plagued Mir because, in general, the microbes that thrive inside space shuttles and space stations like to feast on decayed human flesh — or, to put it more politely, have served as ecological mechanisms for the decomposition of dead skin cells, which astronauts can’t help but shed over long journeys. Most fungi fall into this category, as do Actinobacteria, long confused for fungi because they both grow wispy branching structures called mycelia. (NASA’s Jet Propulsion Laboratory found an abundance of Actinobacteria on the International Space Station in 2015.)

![输入图片说明](https://foruda.gitee.com/images/1676825252934516535/dd8c5b25_5631341.gif "NASA-Fungi-001-for-SL.gif")

> Astronaut Peggy Whitson (above and below),  working to identify microbes inside the International Space Station last year. [Animated GIF Credit: [NASA](https://www.nasa.gov/feature/genes-in-space-3-successfully-identifies-unknown-microbes-in-space) | Public Domain]

![输入图片说明](https://foruda.gitee.com/images/1676825297904723652/de68f7cf_5631341.gif "NASA-Fungi-000-for-SL.gif")

So, there’s plenty of food to help fungi grow. But another reason they thrive in space is that their standard reproduction method doesn’t quite meet the same level of resistance that it does back on Earth.

“Fungi reproduce by spores and it shoots them out with a certain amount of force,” says Rocco Mancinelli, an astrobiologist working with NASA’s Ames Research Center. “So, if there is no friction due to gravity to haul it down, it’s going to just float; and they’re going to go everywhere, and they do. That’s why you get these fungal formations all over the place in things like the space station and Mir.”

NASA scientists have periodically turned to yeasts (also a subgroup of fungi) to better understand how certain pathogenic microbes can become more resistant to drugs in outer space, as they did with [the launch of the PharmaSat nanosatellite in 2009](http://www.nbcnews.com/id/30588385/ns/technology_and_science-science/t/nasa-testing-antifungal-drugs-space/). (Brewer’s yeast, in particular, has been a staple of commercial pharmaceutical research back here on Earth for decades.)

And yeasts, it turns out, have a unique survival mechanism for one of the biggest hazards in space: radiation. The microbes have an abundance of radiation-shielding antioxidants containing positively-charged manganese.

Cosmic radiation splits and super-charges water molecules creating a variety of highly reactive particles, including a toxic form of oxygen known as superoxide. Antioxidants have proven remarkably effective in mitigating the cellular damage produced by these reactive particles — and certain manganese-containing antioxidants have been the strongest at tackling build-ups of superoxide.

“Virtually all yeast, including baker’s yeast, are remarkably radiation resistant,” says molecular biologist Mike Daly. This past October, Daly and his team [published research](http://www.pnas.org/content/early/2017/10/18/1713608114.full.pdf) in the  _Proceedings of the National Academy of Sciences_  showing a consistent link between the presence of manganese-complex antioxidants and radiation resistance.

Daly says the explanation for this relationship stems from the counter-intuitive reason for why radiation damages living cells. “Gamma rays themselves, when they come out — like in cartoons and  _Star Trek_  — they just go through walls, they go through cells,” he says. “But, they’re breaking up water as they’re traveling [and] because every living cell is 98 percent water, suddenly you have all these water molecules being burst apart, and giving out the hydroxyl radicals, and the superoxides, and hydrogen peroxide, and all these electrons. So, suddenly, all your proteins are in a soup of free oxygen radicals.”

While many of these radicals just react back with one another — reforming water — superoxides do not. Unchecked, (by manganese-complex antioxidants, let’s say) their build-up will inevitably damage key proteins deployed by cells to repair DNA.

To review: A lot of fungi are hearty in space.

The challenge for scientists is to make this fact as much a blessing as a curse — a challenge some researchers plan to meet by synthesizing useful materials during space travel from the microbial cultures likely to grow well under those interplanetary conditions anyway. “You could use synthetic biology for producing things, offworld, that you wouldn’t have to use them for producing here,” Mancinelli says. This past summer, in fact, [engineers from Clemson University](https://www.washingtonpost.com/news/speaking-of-science/wp/2017/08/23/the-survival-of-a-mars-mission-could-depend-on-astronaut-urine/) unveiled their progress with a yeast capable of recycling astronaut urine and carbon dioxide into much-needed omega-3 fatty acids.

Another “classic example,” according to Mancinelli, is pharmaceuticals: “If you go to Mars, you can’t take a pharmacy with you.”

But you can take fungi. More than you could anticipate. Probably, more than you wanted.


### 6 Comments

- **Terry Newberg**  MARCH 11, 2018 AT 1:42 AM

  > If this is the case, do they really think they’d make all the way to Mars? Ooops there goes the electrical system; oops there goes the viewport; oops, there goes the astronauts . . .

- **adonias**  MARCH 12, 2018 AT 4:56 PM

  > The Actinobacteria were the first bacteria shown to have multiple chaperonins (Rinke de Wit et al.

- **Jonathan McDonald**  FEBRUARY 12, 2019 AT 10:37 AM

  > Yeah, absolutely. Fungi also produce our best antibiotics. We could bring a fungus like an amarilla from Washington called the candy cap, which makes delicious sweet mushrooms out of straw and is highly dominant with one strain living to be thousands of years old, or formitosis officialionalis which can treat viruses like HSV, or even orphocordyceps sinensis which acts like ricin but for cancer cells exclusively. The fact that fungi can grow well in zero gravity is excellent. I’d love to see how the mushrooms orient though.

- **Victor Zboralski**   MARCH 26, 2019 AT 12:47 PM

  > Interesting read. I wonder if a protective shell could be grown in space inside a double hull made of fungi redistant material.

- **Ralph E. Moon, Ph.D.**  MAY 3, 2019 AT 11:06 PM

  > How will the astronauts cope with fungal growth in their clothing and underwear during long voyages. Sweat would contain sufficient nutrients to support and maintain mold growth. Research conducted on cleaning mold from fabrics showed that it was difficult to remove all viable mold spores.

- **Victor Zboralski**  SEPTEMBER 14, 2019 AT 3:32 PM

  > Guess clothing will need to be purged periodically. Stuff discarded clothing into a double hull as compost for the protective shell, or jettison.

---

直击科学 · 你和科学之间最短的距离 · 纽约大学的一个科学、健康和环境报告项目
> TAG: 空间、物理和数学

# 为什么真菌能对太空生活适应得如此之好

> 在很多方面，这些小微生物比我们更适合太空旅行

马修•费兰 2018年3月7日

![输入图片说明](https://foruda.gitee.com/images/1676825120666555060/9e5cd19f_5631341.png "在这里输入图片标题")

> 宇航员们经常在国际空间站采集空气、水和表面样本，以监测微生物群的变化，以保障他们的个人健康和安全。(图片:美国国家航空航天局|公共领域]

1997年，宇航员大卫·沃尔夫在俄罗斯和平号空间站上有了一个令人不安的发现:在kvat -2舱内一个很少使用的维修面板后面，沃尔夫发现了“保龄球大小或海滩球大小的大凝析球”。这些漂浮的死水球，正如他在NASA的一篇历史(文献)中所描述的那样，“黏糊糊的，冰冷的”，被几十种细菌和真菌堵塞着。更糟糕的是，这些悬浮在零重力状态下的水滴并没有乖乖地呆在这个电器盒子里;他们“开始追踪结构和线路。”

真菌在很多情况下几乎摧毁了和平号。例如，在和平号的一艘联盟号运输飞船上发现的真菌曾被发现侵蚀了飞船视口的硬化石英玻璃。联盟号运输飞船是一种用于运送人员往返轨道平台的航天器。它腐蚀了裂缝，腐蚀了连接窗户和钛框架的橡胶密封件。霉菌(一种真菌)变得如此普遍，以至于宇航员和他们的国际同事返回和平号时，发现整个地方闻起来就像一个满是烂苹果的地下室 —— 2016年1月的俄罗斯版《大众力学》就是这样报道的。

真菌困扰着和平号，因为一般来说，在航天飞机和空间站内繁殖的微生物喜欢吃腐烂的人肉——或者，更委婉地说，它们是分解死皮细胞的生态机制，宇航员在长途旅行中忍不住要摆脱死皮细胞。大多数真菌都属于这一类，放线菌也是如此，长期以来，放线菌都被误认为是真菌，因为它们都长出被称为菌丝的纤细分支结构。(2015年，美国宇航局的喷气推进实验室在国际空间站发现了大量的放线菌。)

![输入图片说明](https://foruda.gitee.com/images/1676825252934516535/dd8c5b25_5631341.gif "在这里输入图片标题")

> 宇航员佩吉·惠特森(上图和下图)去年在国际空间站内鉴定微生物。[图片:美国国家航空航天局|公共领域]

![输入图片说明](https://foruda.gitee.com/images/1676825297904723652/de68f7cf_5631341.gif "在这里输入图片标题")

所以，有足够的食物来帮助真菌生长。但它们在太空中茁壮成长的另一个原因是，它们在太空中没有遇到与地球上同水平的阻力——它们(在太空中也采用了与地球上一样的)标准的繁殖方法。

在美国宇航局艾姆斯研究中心工作的天体生物学家罗科·曼奇内利说:“真菌通过孢子繁殖，并以一定的力量将孢子射出。”“所以，如果没有归因于重力的摩擦力把它拉下来，它就会漂浮起来;他们会去任何地方，他们确实做到了。这就是为什么在空间站和和平号空间站等地方到处都有真菌形成。”

美国宇航局的科学家们会定期(把注意力)转向酵母(也是真菌的一个亚群)，以更好地了解某些致病微生物如何在外太空对药物产生更强的抗药性，他们在2009年发射PharmaSat纳米卫星时做了相关的研究。(特别是啤酒酵母，几十年来一直是地球上商业药物研究的主要原料。)

事实证明，酵母有一种独特的生存机制，可以应对太空中最大的危险之一:辐射。这种微生物含有丰富的辐射屏蔽抗氧化剂，其中含有带正电的锰。

宇宙辐射会分裂水分子并给水分子充电，产生各种高活性粒子，其中包括一种被称为超氧化物的有毒形式的氧。抗氧化剂已被证明在减轻这些活性粒子产生的细胞损伤方面非常有效——某些含锰的抗氧化剂在处理超氧化物积聚方面最为有效。

“几乎所有的酵母，包括面包酵母，都有很强的抗辐射能力，”分子生物学家迈克·戴利说。去年10月，戴利和他的团队在《美国国家科学院院刊》上发表了一项研究，表明锰复合抗氧化剂的存在与抗辐射能力之间存在一致的联系。

戴利说，这种关系的解释源于辐射损害活细胞的反直觉原因。他说:“伽马射线本身，当它们出来的时候——就像动画片和《星际迷航》里那样——它们只是穿过墙壁，穿过细胞。”“但是，它们在移动过程中会分解水，因为每个活细胞98%都是水，突然所有这些水分子都破裂了，释放出羟基自由基、超氧化物、过氧化氢和所有这些电子。所以，突然之间，你所有的蛋白质都在自由氧自由基的汤里。”

虽然许多自由基只是相互反应——重组水——但超氧化物不会。如果不加以控制(比如说，通过锰复合抗氧化剂)，它们的积累将不可避免地破坏细胞用来修复DNA的关键蛋白质。

回顾一下:很多真菌在太空中很健壮。

科学家们面临的挑战是如何使这一事实成为一种祝福和诅咒——一些研究人员计划，通过在太空旅行中从可能在星际条件下生长良好的微生物培养物中，合成有用的材料来应对这一挑战。曼奇内利说:“你可以利用合成生物学在地球以外生产东西，而不必在地球上生产。”事实上，去年夏天，克莱姆森大学(Clemson University)的工程师们公布了他们的研究进展，他们研制出了一种酵母，可以将宇航员的尿液和二氧化碳循环利用，生成人们急需的ω -3脂肪酸。

曼奇内利说，另一个“经典的例子”是药品:“如果你去火星，你不能带着药房。”

但是你可以拿真菌做例子。比你预料的要多。可能比你想要的要多。

TAGs : 细菌 药物 真菌 国际空间站 米尔 美国国家航空航天局 辐射 空间 酵母

### 6个评论

- 特里·纽伯格 2018年3月11日凌晨1点42分
  > 如果是这样的话，他们真的认为他们能一路到达火星吗?噢，电力系统坏了;哦，这是视口;哎呀，宇航员走了……

- adonias 2018年3月12日下午4点56分
  > 放线菌是第一个被证明具有多种伴侣蛋白的细菌(Rinke de Wit et al。

- 乔纳森·麦克唐纳 2019年2月12日上午10:37
  > 是的,当然。真菌还能产生最好的抗生素。我们可以从华盛顿带来一种叫做“糖果帽”的真菌，它可以用稻草制成美味的甜蘑菇，而且有一种菌株可以存活数千年，或者可以治疗HSV等病毒的官形虫病，甚至可以像蓖广林毒素一样作用的中国形虫草，但只对癌细胞有效。事实上，真菌在零重力下也能很好地生长。不过我很想看看蘑菇是如何定位的。

- 维克多Zboralski 2019年3月26日下午12:47
  > 有趣的阅读。我想知道一个保护壳是否可以在太空中生长在一个由真菌制成的双层外壳中。

- 拉尔夫·e·穆恩博士 2019年5月3日晚上11:06
  > 在长途航行中，宇航员将如何应对衣服和内衣上的真菌生长?汗液含有足够的营养物质来支持和维持霉菌的生长。对清洗织物上的霉菌进行的研究表明，很难去除所有存活的霉菌孢子。

- 维克多Zboralski 2019年9月14日下午3:32
  > 我猜衣服需要定期清洗。将丢弃的衣物放入双层船体中作为保护壳的堆肥，或丢弃。