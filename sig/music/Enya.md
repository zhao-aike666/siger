<p><img width="706px" src="https://foruda.gitee.com/images/1670691525600107298/429bad66_5631341.jpeg" title="494ENYA.jpg"></p>

【笔记】[兴趣-爱尔兰风格音乐分享：恩雅（Enya）](https://gitee.com/chen-yi-111/siger/issues/I652DM)

> SIGer 终于有了自己的 DJ 他就是我们音乐频道的  @[陈一111](https://gitee.com/chen-yi-111) 上面是他的码更，封面素材全部来自他的搜集整理，12张 1400x1400 的唱片封面，而楼顶的依偎在树旁的眼神打动了我，我将她张贴在油画布上，任选了4张唱片照，在绿色树影的衬托下，最适合听她如梦如幻的乐曲，这个时候，歌词已经不那么重要啦，所有的情绪都在音符间流淌。  @yuandj  2022-12-11

# 爱尔兰风格音乐：恩雅（Enya）

## 前言
音乐一直是我生活中的一部分。我很喜欢听音乐，涉猎的音乐风格非常多，包括电影配乐、游戏配乐、民乐、电子、摇滚等等。小学时有三年学习古筝的经历。虽然是个理科生，但是不影响我对音乐的喜爱。每天我都会花上一些时间听自己喜欢的音乐。
恩雅的歌其实是跟着父母听到的。我们都很喜欢她的歌曲，风格非常独特。因此，想要在这里分享一些恩雅的代表作，希望有更多人喜欢。
## 简介
恩雅·帕特里夏·布伦南(Eithne Pádraigín Ní Bhraonáin;生于1961年5月17日)是一位爱尔兰歌手、词曲作者和音乐家，以现代凯尔特音乐而闻名。她是爱尔兰历史上最畅销的独唱歌手，也是爱尔兰整体销量第二高的歌手，仅次于U2。恩雅出生在一个音乐家庭，在多尼戈尔郡的Gweedore爱尔兰语区长大。1980年，她加入了家中的凯尔特民谣乐队Clannad，担任键盘和和声，开始了她的音乐生涯。1982年，她和他们的经纪人兼制作人尼基·瑞安一起离开了乐队，与瑞安的妻子罗姆·瑞安(Roma Ryan)担任词作者，单飞。
恩雅常用多音轨人声和键盘，音乐风格非常独特，包含了凯尔特、古典、教堂、新时代、世界、流行和爱尔兰民间音乐的元素。希望更够多多分享她的事迹与歌曲，以及其他爱尔兰风格音乐，让更多人了解并喜欢上现代凯尔特音乐。
<div align="center">
   <img src="https://foruda.gitee.com/images/1670394509968304837/3f762a3e_11991685.jpeg" width="500" height="350">
</div>

# 恩雅历年主要专辑与主打歌曲
## 1985–1989: The Celts 和 Watermark
● The Celts
1985年，BBC六集电视纪录片《凯尔特人》(The Celts)征集配曲。恩雅受制片人邀请，写了一首凯尔特风格的歌曲《凯尔特人的进行曲》，并提交给了该项目。一开始的安排是每一集都有不同的作曲家，但导演大卫·理查森(David Richardson)非常喜欢她的歌曲，所以他请恩雅为整个系列配乐。于是有了第一张较为出名的专辑‘The Celts’。
<div align="center">
   <img src="https://foruda.gitee.com/images/1670481537947958925/b5f88483_11991685.jpeg" width="500" height="500">
</div>

[Enya 1991 The Celts](https://www.bilibili.com/video/BV1cx411D7Hc/?spm_id_from=333.337.search-card.all.click&vd_source=c2389be1955e4d0296cecddcbaa96c7a)

由于是恩雅（Enya）的早期作品，而且是原声配乐，所以其中的音乐风格个性并不突出，整体效果非常舒缓、平和。这些音乐讲述了古代凯尔特民族的历史与神话，略带神秘却又令人敬畏。

● Watermark
在这些创作的基础上，恩雅开始逐渐形成自己的风格。专辑《水印》‘Watermark’于1988年9月发行，并获得了意想不到的成功。1989年1月在英国发行后，在英国Billboard 200排行榜上排名第5位，在美国排名第25位。它的主打歌‘Orinoco Flow’成为当时国际十大热门歌曲，并在英国蝉联三周冠军。
<div align="center">
   <img src="https://foruda.gitee.com/images/1670482128895067192/d13393b0_11991685.jpeg" width="706" height="705">
</div>

[Enya-Watermark](https://www.bilibili.com/video/BV1cx411D7kw/?share_source=copy_web&vd_source=56a20057a732814d724e92793a4914c6)

[Enya-Orinoco Flow-官方宣传MV-2020年数位修复4K画质版](https://www.bilibili.com/video/BV15Z4y137pp/?share_source=copy_web&vd_source=56a20057a732814d724e92793a4914c6)


● Orinoco Flow歌词大意：
<br>

扬帆远航扬帆远航
<br>
Sail away, sail away, sail away
<br>
扬帆远航扬帆远航
<br>
Sail away, sail away, sail away
<br>

扬帆远航扬帆远航
<br>
Sail away, sail away, sail away
<br>

扬帆远航扬帆远航
<br>
Sail away, sail away, sail away
<br>

从比绍到帕劳，在阿瓦隆的树荫下
<br>
From Bissau to Palau, in the shade of Avalon
<br>

从斐济到泰里和乌木群岛
<br>
From Fiji to Tyree and the Isles of Ebony
<br>

从秘鲁到宿雾，聆听巴比伦的力量
<br>
From Peru to Cebu, hear the power of Babylon
<br>

从巴厘岛到卡利，远在珊瑚海之下
<br>
From Bali to Cali, far beneath the Coral Sea
<br>

开起来，开起来，开起来，再见，哦，哦
<br>
Turn it up, turn it up, turn it up, up, adieu, ooh, ooh
<br>

开起来，开起来，开起来，再见，哦，哦
<br>
Turn it up, turn it up, turn it up, up, adieu, ooh, ooh
<br>

开起来，开起来，开起来，再见，哦
<br>
Turn it up, turn it up, turn it up, up, adieu, oh
<br>

扬帆远航扬帆远航
<br>
Sail away, sail away, sail away
<br>

扬帆远航扬帆远航
<br>
Sail away, sail away, sail away
<br>

扬帆远航扬帆远航
<br>
Sail away, sail away, sail away
<br>

扬帆远航扬帆远航
<br>
Sail away, sail away, sail away
<br>

从北到南，埃布代到喀土穆
<br>
From the North to the South, Ebudæ unto Khartoum
<br>

从云海深处到月亮岛
<br>
From the deep Sea of Clouds to the Island of the Moon
<br>

带我乘风破浪去我从未到过的地方
<br>
Carry me on the waves to the lands I've never been
<br>

带我乘风破浪去我从未见过的土地
<br>
Carry me on the waves to the lands I've never seen
<br>

我们可以航行，我们可以与奥里诺科河一起航行
<br>
We can sail, we can sail with the Orinoco Flow
<br>

我们可以航行，我们可以航行
<br>
We can sail, we can sail
<br>

（远航，远航，远航）
<br>
(Sail away, sail away, sail away)
<br>

我们可以掌舵，我们可以靠近
<br>
We can steer, we can near
<br>

Rob Dickins 掌舵
<br>
With Rob Dickins at the wheel
<br>

我们可以叹息，告别罗斯和他的伙伴
<br>
We can sigh, say goodbye Ross and his dependency
<br>

我们可以航行，我们可以航行
<br>
We can sail, we can sail
<br>

（远航，远航，远航）
<br>
(Sail away, sail away, sail away)
<br>

我们可以到达，我们可以上岸
<br>
We can reach, we can beach
<br>

在的黎波里的海岸
<br>
On the shores of Tripoli
<br>

我们可以航行，我们可以航行
<br>
We can sail, we can sail
<br>

（远航，远航，远航）
<br>
(Sail away, sail away, sail away)
<br>

从巴厘岛到卡利，远在珊瑚海之下
<br>
From Bali to Cali, far beneath the Coral Sea
<br>

我们可以航行，我们可以航行
<br>
We can sail, we can sail
<br>

（远航，远航，远航）
<br>
(Sail away, sail away, sail away)
<br>

从比绍到帕劳，在阿瓦隆的树荫下
<br>
From Bissau to Palau, in the shade of Avalon
<br>

我们可以航行，我们可以航行
<br>
We can sail, we can sail
<br>

（远航，远航，远航）
<br>
(Sail away, sail away, sail away)
<br>

我们可以到达，我们可以上岸
<br>
We can reach, we can beach
<br>

远在黄海之外
<br>
Far beyond the Yellow Sea
<br>

我们可以航行，我们可以航行
<br>
We can sail, we can sail
<br>

（远航，远航，远航）
<br>
(Sail away, sail away, sail away)
<br>

从秘鲁到宿雾，聆听巴比伦的力量
<br>
From Peru to Cebu, hear the power of Babylon
<br>

我们可以航行，我们可以航行
<br>
We can sail, we can sail
<br>

（远航，远航，远航）
<br>
(Sail away, sail away, sail away)
<br>

我们可以航行，我们可以航行
<br>
We can sail, we can sail
<br>

扬帆远航扬帆远航
<br>
Sail away, sail away, sail away
<br>

扬帆远航扬帆远航
<br>
Sail away, sail away, sail away
<br>

扬帆远航扬帆远航
<br>
Sail away, sail away, sail away
<br>

扬帆远航扬帆远航
<br>
Sail away, sail away, sail away
<br>

扬帆远航扬帆远航
<br>
Sail away, sail away, sail away

词曲作者：Eithne Ni Bhraonain / Nicky Ryan / Roma Ryan
‘Orinoco Flow’歌词 © BMG Rights Management, Sony/ATV Music Publishing LLC

在《水印》‘Watermark’专辑里，恩雅初步展现了她极具个人风格的音乐特点。有层次的声乐，键盘导向的音乐，以及带有凯尔特、古典、教堂和民间音乐元素的打击乐。新纪元风格和凯尔特风格相得益彰。

## 1989–1997: Shepherd Moons 和 The Memory of Trees
●  Shepherd Moons
《水印》取得的巨大成功给了恩雅继续创作的动力。她从自己的生活和家人们，尤其是祖父祖母那里获得新的创作灵感，并于1991年11月发行《牧羊人之月》‘Shepherd Moons’。它在商业上的成功超过了《水印》，在英国连续一周蝉联冠军，在美国排名第17位。其主打单曲《加勒比之蓝》‘Caribbean Blue’在英国排行榜上排名第13位。
<div align="center">
   <img src="https://foruda.gitee.com/images/1670484585432521970/349f3b8f_11991685.jpeg" width="500" height="500">
</div>

[Enya-1991-Shepherd Moons-牧羊人之月.专辑](https://www.bilibili.com/video/BV1cx411D7JD/?share_source=copy_web&vd_source=56a20057a732814d724e92793a4914c6)

[Enya-Caribbean Blue-官方宣传MV(2020年数位修复4K画质版)](https://www.bilibili.com/video/BV1ii4y1t7ib/?share_source=copy_web&vd_source=56a20057a732814d724e92793a4914c6)

● ‘Caribbean Blue’歌词大意：
<br>
Eurus
<br>
Eurus
<br>

Afer Ventus
<br>
Afer Ventus
<br>

所以这个世界转了又转
<br>
So the world goes round and round
<br>

用你所知道的一切
<br>
With all you ever knew
<br>

他们说天空高高在上
<br>
They say the sky high above
<br>

是加勒比海蓝色的吗？
<br>
Is Caribbean blue?
<br>

如果每个人都竭尽所能
<br>
If every man says all he can
<br>

如果每个人都是真实的
<br>
If every man is true
<br>

我相信头顶的天空
<br>
Do I believe the sky above
<br>

是加勒比海蓝色的吗？
<br>
Is Caribbean blue?
<br>

Boreas
<br>
Boreas
<br>

Zephyrus
<br>
Zephyrus
<br>

如果你所说的一切都变成了金子
<br>
If all you told was turned to gold
<br>

如果你梦想的一切都是新的
<br>
If all you dreamed was new
<br>

想象天空高高在上
<br>
Imagine sky high above
<br>

是加勒比海蓝
<br>
In Caribbean blue
<br>

Eurus
<br>
Eurus
<br>

Afer Ventus
<br>
Afer Ventus
<br>

Boreas
<br>
Boreas
<br>

Zephyrus
<br>
Zephyrus
<br>

Africus
<br>
Africus
<br>

词曲作者：Roma Ryan / Eithne Ni Bhraonain / Nicky Ryan
Caribbean Blue 歌词 © Emi Music Publishing Ltd

《牧羊人之月》与《水印》风格非常相似，但更有一种淡淡的忧愁感。在这张专辑中，新世纪风格已经较为成熟。

●  The Memory of Trees
在《牧羊人之月》后，恩雅进入了一个创作的高峰期。专辑《树的记忆》‘The Memory of Trees’于1995年11月发行。这张专辑在英国销量达到了第五，在美国销量达到了第九，销量超过了300万册。其主打单曲‘Anywhere Is’在英国排名第七。
<div align="center">
   <img src="https://foruda.gitee.com/images/1670487295108109848/9013291a_11991685.jpeg" width="500" height="500">
</div>

[Enya-The Memory Of Trees](https://www.bilibili.com/video/BV1bs411s7Q1/?share_source=copy_web&vd_source=56a20057a732814d724e92793a4914c6)

[Enya-Anywhere Is-官方宣传MV(2020年数位修复4K画质版)](https://www.bilibili.com/video/BV1Y5411W7ab/?share_source=copy_web&vd_source=56a20057a732814d724e92793a4914c6)

● Anywhere Is 歌词大意：
<br>

我走在瞬间的迷宫中
<br>
I walk the maze of moments
<br>

但无论我转向哪里
<br>
But everywhere I turn to
<br>

开始新的开始
<br>
Begins a new beginning
<br>

却始终找不到结局
<br>
But never finds a finish
<br>

我走向地平线
<br>
I walk to the horizon
<br>

在那里我找到了另一个
<br>
And there I find another
<br>

一切都显得那么意外
<br>
It all seems so surprising
<br>

然后我发现我知道
<br>
And then I find that I know
<br>

你去那里，你永远消失了
<br>
You go there, you're gone forever
<br>

我去那里，我会迷路
<br>
I go there, I'll lose my way
<br>

如果我们留在这里，我们就不会在一起
<br>
If we stay here we're not together
<br>

任何地方都是
<br>
Anywhere is
<br>

海面上的月亮
<br>
The moon upon the ocean
<br>

在运动中被扫过
<br>
Is swept around in motion
<br>

却不知
<br>
But without ever knowing
<br>

流动的原因
<br>
The reason for its flowing
<br>

在海洋上运动
<br>
In motion on the ocean
<br>

月亮还在继续移动
<br>
The moon still keeps on moving
<br>

海浪还在继续
<br>
The waves still keep on waving
<br>

而我仍然继续前进
<br>
And I still keep on going
<br>

你去那里，你永远消失了
<br>
You go there, you're gone forever
<br>

我去那里，我会迷路
<br>
I go there, I'll lose my way
<br>

如果我们留在这里，我们就不会在一起
<br>
If we stay here we're not together
<br>

任何地方都是
<br>
Anywhere is
<br>

我想知道星星是否在感叹
<br>
I wonder if the stars sign
<br>

属于我的生活
<br>
The life that is to be mine
<br>

他们会让他们的光芒闪耀吗
<br>
And would they let their light shine
<br>

足够我追寻
<br>
Enough for me to follow
<br>

我仰望苍穹
<br>
I look up to the heavens
<br>

但夜色已浓
<br>
But night has clouded over
<br>

没有星座的火花
<br>
No spark of constellation
<br>

没有船帆座，没有猎户座
<br>
No Vela, no Orion
<br>

温暖沙滩上的贝壳
<br>
The shells upon the warm sands
<br>

从他们自己的土地上拿走了
<br>
Have taken from their own lands
<br>

他们故事的回声
<br>
The echo of their story
<br>

但我听到的都是低沉的声音
<br>
But all I hear are low sounds
<br>

当枕边词在编织
<br>
As pillow words are weaving
<br>

柳浪离去
<br>
And willow waves are leaving
<br>

但我应该相信吗
<br>
But should I be believing
<br>

我只是在做梦
<br>
That I am only dreaming
<br>

你去那里，你永远消失了
<br>
You go there, you're gone forever
<br>

我去那里，我会迷路
<br>
I go there, I'll lose my way
<br>

如果我们留在这里，我们就不会在一起
<br>
If we stay here we're not together
<br>

任何地方都是
<br>
Anywhere is
<br>

离开所有时间的线程
<br>
To leave the thread of all time
<br>

让它做一条暗线
<br>
And let it make a dark line
<br>

希望我还能找到
<br>
In hopes that I can still find
<br>

回到当下的路
<br>
The way back to the moment
<br>

我轮流转向
<br>
I took the turn and turned to
<br>

开始新的开始
<br>
Begin a new beginning
<br>

仍在寻找答案
<br>
Still looking for the answer
<br>

我找不到结局
<br>
I cannot find the finish
<br>

要么这样要么那样
<br>
It's either this or that way
<br>

这是一种方式或另一种方式
<br>
It's one way or the other
<br>

应该是一个方向
<br>
It should be one direction
<br>

这可能是反射
<br>
It could be on reflection
<br>

我刚转过的弯
<br>
The turn I have just taken
<br>

我正在做的转弯
<br>
The turn that I was making
<br>

我可能才刚刚开始
<br>
I might be just beginning
<br>

我可能快接近终点。
<br>
I might be near the end
<br>
词曲作者：Roma Ryan / Eithne Ni Bhraonain / Nicky Ryan
‘Anywhere Is’歌词 © Emi Music Publishing Ltd

《树的记忆》展现的主题更偏向探索谜样人生，体现的更多的是一种对于天地万物的思考和心底的感触。是恩雅最具穿透力的作品。

## 1996-1997:On My Way Home，Paint the Sky with Stars与Only If...
● On My Way Home
1996年，恩雅发行了圣诞节主题单曲 ‘On My Way Home’。这首歌曲沉稳安静，也带有家的温馨感。
<div align="center">
   <img src="https://foruda.gitee.com/images/1670514298602158796/886373dd_11991685.jpeg" width="500" height="500">
</div>

[Enya-On My Way Home-官方宣传MV](https://www.bilibili.com/video/BV1gx411K7HB/?share_source=copy_web&vd_source=56a20057a732814d724e92793a4914c6)

● On My Way Home歌词大意：
<br>

我被赋予
<br>
I have been given
<br>

来自天堂的一瞬间
<br>
One moment from heaven
<br>

当我走路时
<br>
As I am walking
<br>

被夜色包围
<br>
Surrounded by night
<br>

星星在我头顶
<br>
Stars high above me
<br>

在月光下许个愿
<br>
Make a wish under moonlight
<br>

回家的路上
<br>
On my way home
<br>

我记得的
<br>
I remember
<br>

只有美好的日子
<br>
Only good days
<br>

我在回家的路上
<br>
I'm on my way home
<br>

我能记住
<br>
I can remember
<br>

每一个新的一天
<br>
Every new day
<br>

我默默地移动
<br>
I move in silence
<br>

每迈出一步
<br>
With each step taken
<br>

雪在我身边飘落
<br>
Snow falling round me
<br>

像飞行中的天使
<br>
Like angels in flight
<br>

远在天边
<br>
Far in the distance
<br>

月光下是我的心愿吗
<br>
Is my wish under moonlight
<br>

回家的路上
<br>
On my way home
<br>

我记得
<br>
I remember
<br>

只有美好的日子
<br>
Only good days
<br>

回家的路上
<br>
On my way home
<br>

我记得
<br>
I remember
<br>

所有最好的日子
<br>
All the best days
<br>

我在回家的路上
<br>
I'm on my way home
<br>

我能记住
<br>
I can remember
<br>

每一个新的一天
<br>
Every new day
<br>

回家的路上
<br>
On my way home
<br>

我记得
<br>
I remember
<br>

只有美好的日子
<br>
Only good days
<br>

回家的路上
<br>
On my way home
<br>

我记得
<br>
I remember
<br>

只有美好的日子
<br>
Only good days
<br>

词曲作者：Roma Ryan / Eithne Ni Bhraonain / Nicky Ryan
 ‘On My Way Home’歌词 © Emi Music Publishing Ltd

● Paint the Sky with Stars
1997年11月，恩雅发行了她的第一张合辑，收录了一些明显的、热门的曲子,起名为《用星星画天空:恩雅精选》‘Paint the Sky with Stars:The Best of Enya’，并添加了两首新曲目《用星星画天空》‘Paint the Sky with Stars’和‘Only If…’。这张专辑在全球范围内获得了巨大的商业成功，在英国排名第4，在美国排名第30，销量超过400万张。
<div align="center">
   <img src="https://foruda.gitee.com/images/1670510196740207325/991ce3ae_11991685.jpeg" width="500" height="500">
</div>
‘Only If…’于1997年以单曲形式发行。恩雅形容这张专辑“就像一本音乐日记……每一个旋律都有一个小故事，我从一开始就经历了整个故事……你会回想起那天你在想什么。”


[Enya Only If...官方宣传MV](https://www.bilibili.com/video/BV1gx411K7bL/?share_source=copy_web&vd_source=56a20057a732814d724e92793a4914c6)

● Only If...歌词大意：
<br>
有阴影的时候，你就跟着太阳走。
<br>
When there's a shadow, you follow the sun.
<br>

当有爱时，你就去寻找那个命中注定之人。
<br>
When there is love, then you look for the one.
<br>

对于承诺，有天空。
<br>
And for the promises, there is the sky.
<br>

对于能飞的人就是天堂。
<br>
And for the heavens are those who can fly.
<br>

如果你真的想要，你可以听我说
<br>
If you really want to, you can hear me say
<br>

只有你想，你才会找到方法。
<br>
Only if you want to will you find a way.
<br>

如果你真的想，你可以抓住这一天。
<br>
If you really want to you can seize the day.
<br>

只有你想，你才会飞走。
<br>
Only if you want to will you fly away.
<br>

达达达达，达达达达达。
<br>
Da da da da, da da da da da.
<br>

达达达达，达达达达达，
<br>
Da da da da, da da da da da,
<br>

哒哒哒哒。
<br>
Da da da da.
<br>

当有一段旅程时，你会跟随一颗星星。
<br>
When there's a journey, you follow a star.
<br>

当有大海时，你远航。
<br>
When there's an ocean, you sail from afar.
<br>

对于破碎的心，有天空。
<br>
And for the broken heart, there is the sky.
<br>

对于明天是那些能飞的人。
<br>
And for tomorrow are those who can fly.
<br>

如果你真的想要，你可以听我说
<br>
If you really want to, you can hear me say
<br>

只有你想，你才会找到方法。
<br>
Only if you want to will you find a way.
<br>

如果你真的想，你可以抓住这一天。
<br>
If you really want to you can seize the day.
<br>

只有你想，你才会飞走。
<br>
Only if you want to will you fly away.
<br>

达达达达，达达达达达。
<br>
Da da da da, da da da da da.
<br>

达达达达，达达达达达，
<br>
Da da da da, da da da da da,
<br>

哒哒哒哒。
<br>
Da da da da.
<br>

Ooh go doe bay mwa.
<br>
Ooh go doe bay mwa.
<br>

Ooh go doe bay mwa.
<br>
Ooh go doe bay mwa.
<br>

如果你真的想要，你可以听我说
<br>
If you really want to, you can hear me say
<br>

只有你想，你才会找到方法。
<br>
Only if you want to will you find a way.
<br>

如果你真的想，你可以抓住这一天。
<br>
If you really want to you can seize the day.
<br>

只有你想，你才会飞走。
<br>
Only if you want to will you fly away.
<br>

啊!
<br>
Ah!
<br>

Je voudrai voler comme un oiseau d'aile
<br>
Je voudrai voler comme un oiseau d'aile
<br>

啊!
<br>
Ah!
<br>

Je voudrai voler comme un oiseau d'aile,
<br>
Je voudrai voler comme un oiseau d'aile,
<br>

D'aile
<br>
D'aile
<br>

Ooh go doe bay mwa.
<br>
Ooh go doe bay mwa.
<br>

Ooh go doe bay mwa.
<br>
Ooh go doe bay mwa.
<br>

如果你真的想要，你可以听我说
<br>
If you really want to, you can hear me say
<br>

只有你想，你才会找到方法。
<br>
Only if you want to will you find a way.
<br>

如果你真的想，你可以抓住这一天。
<br>
If you really want to you can seize the day.
<br>

只有你想，你才会飞走。
<br>
Only if you want to will you fly away.
<br>

如果你真的想，你可以抓住这一天。
<br>
If you really want to you can seize the day.
<br>

只有你想，你才会飞走。
<br>
Only if you want to will you fly away.
<br>

词曲作者：Eithne Ni Bhraonain / Nicky Ryan / Roma Ryan
‘Only If...’歌词 © Sony/ATV Music Publishing LLC

可以说，1996-1997年间，恩雅的作品都带有着振奋人心的积极性，对温馨的家的依恋，对生活的美好向往是这部分专辑的主题。

## 1998-2001: A Day Without Rain 和 Only Time
●  A Day Without Rain
1998年中，恩雅开始创作她的新专辑《不下雨的一天》‘A Day Without Rain’。在这部专辑里，恩雅第一次尝试在她的歌曲里加入了弦乐的元素。整部专辑主要体现着梦境，意境和心情的豁然开朗的主题。该专辑的主打歌曲‘Only Time’也是恩雅最出名的歌曲之一。
<div align="center">
   <img src="https://foruda.gitee.com/images/1670559641222867836/11f35da7_11991685.jpeg" width="500" height="500">
</div>

[Enya - A Day Without Rain](https://www.bilibili.com/video/BV18s411s7Vw/?share_source=copy_web&vd_source=56a20057a732814d724e92793a4914c6)

[Only Time-官方宣传MV(2020年数位修复4K画质版)](https://www.bilibili.com/video/BV1pK411L7w7/?share_source=copy_web&vd_source=56a20057a732814d724e92793a4914c6)
 
● Only Time歌词大意：
<br>
谁能说出路在何方？
<br>
Who can say where the road goes?
<br>

白日流向何方？
<br>
Where the day flows?
<br>

只有时间
<br>
Only time
<br>

谁能说你的爱是否增长
<br>
And who can say if your love grows
<br>

如你所愿？
<br>
As your heart chose?
<br>

只有时间
<br>
Only time
<br>

谁能说出你的心为何叹息
<br>
Who can say why your heart sighs
<br>

随着你的爱飞翔？
<br>
As your love flies?
<br>

只有时间
<br>
Only time
<br>

谁能说出你的心为何哭泣
<br>
And who can say why your heart cries
<br>

当你的爱撒谎？
<br>
When your love lies?
<br>

只有时间
<br>
Only time
<br>

谁能说路何时相遇？
<br>
Who can say when the roads meet?
<br>

那种爱可能在你心里？
<br>
That love might be in your heart?
<br>

谁能说白昼何时入睡
<br>
And who can say when the day sleeps
<br>

如果夜留住你所有的心？
<br>
If the night keeps all your heart?
<br>

夜留住你所有的心
<br>
Night keeps all your heart
<br>

谁能说你的爱是否增长
<br>
Who can say if your love grows
<br>

如你所愿？
<br>
As your heart chose?
<br>

只有时间
<br>
Only time
<br>

谁又能说出路通向何方？
<br>
And who can say where the road goes?
<br>

白日流向何方？
<br>
Where the day flows?
<br>

只有时间
<br>
Only time
<br>

谁知道？
<br>
Who knows?
<br>

只有时间
<br>
Only time
<br>

谁知道？
<br>
Who knows?
<br>

只有时间
<br>
Only time
<br>

词曲作者：Eith Ni-Bhraonain / Nicky Ryan / Roma Shane Ryan
‘Only Time’歌词 © Sony/ATV Music Publishing LLC

## 2001-2003：指环王电影配乐和Amarantine
● 2001年《指环王：护戒使者》电影配乐
2001年，恩雅应导演彼得·杰克逊的要求，同意为《指环王:护戒使者》(2001)的原声带创作并表演两首歌曲。在飞往新西兰观看电影拍摄并观看了电影的粗剪后，恩雅回到爱尔兰，创作了阿拉贡和亚文的主题曲‘Aníron’，歌词使用了托尔金虚构的精灵语辛达林语；以及‘May It Be’，歌词使用英语和托尔金的另一种语言昆雅语。然后为电影配乐的作曲家霍华德·肖尔(Howard Shore)根据恩雅录制的声音和主题进行编曲，创造出“无缝的声音”。
<div align="center">
   <img src="https://foruda.gitee.com/images/1670561780012216680/4ec9c1ea_11991685.jpeg" width="500" height="500">
</div>
2002年，恩雅发行单曲‘May It Be’，并获得奥斯卡最佳原创歌曲奖提名。2002年3月，她在第74届奥斯卡颁奖典礼上与管弦乐队现场表演了这首歌，后来她称那一刻是她职业生涯的亮点。

[Enya-May It Be-官方宣传MV](https://www.bilibili.com/video/BV1Gx411K7jD/?share_source=copy_web&vd_source=56a20057a732814d724e92793a4914c6)

[恩雅Enya-May It Be真唱版-美国奥斯卡金像奖颁奖典礼](https://www.bilibili.com/video/BV1ss411k732/?share_source=copy_web&vd_source=56a20057a732814d724e92793a4914c6)

● May It Be歌词大意：
<br>
愿它是一颗晚星
<br>
May it be an evening star
<br>

照耀着你
<br>
Shines down upon you
<br>

愿它在黑暗降临时
<br>
May it be when darkness falls
<br>

你的心会是真的
<br>
Your heart will be true
<br>

你走在孤独的路上
<br>
You walk a lonely road
<br>

哦，你离家有多远
<br>
Oh, how far you are from home
<br>

莫尼乌图列
<br>
Mornie utulie
<br>

相信你会找到你的路
<br>
Believe and you will find your way
<br>

莫尼阿兰蒂
<br>
Mornie alantie
<br>

一个承诺现在就在你心中
<br>
A promise lives within you now
<br>

愿影子的呼唤会飞走
<br>
May it be the shadow's call will fly away
<br>

愿它成为你点亮一天的旅程
<br>
May it be your journey on to light the day
<br>

当夜幕降临
<br>
When the night is overcome
<br>

你可能会起来寻找太阳
<br>
You may rise to find the sun
<br>

莫尼乌图列
<br>
Mornie utulie
<br>

相信你会找到你的路
<br>
Believe and you will find your way
<br>

莫尼阿兰蒂
<br>
Mornie alantie
<br>

一个承诺现在就在你心中
<br>
A promise lives within you now
<br>

一个承诺现在就在你心中
<br>
A promise lives within you now
<br>

词曲作者：Howard Leslie Shore / Nicky Ryan / Roma Shane Ryan / Ethne Patricia Brennan
‘May It Be’歌词 © New Line Tunes

● Amarantine
2003年9月，恩雅回到工作室开始制作专辑‘Amarantine’，意思是“永恒”。这个词常被过去的诗人用来描述永不凋谢的花朵，也寓意着作品的主题：永恒的爱，新古典浪漫主义和飘渺写意的意境。这张专辑标志着恩雅第一次用洛仙语（Loxian）演唱，洛仙语是恩雅在创作‘Water Shows the Hidden Heart’时创造的一种虚构语言，来源于恩雅在创作歌曲时会跟着哼唱的一些声音。这首歌很成功，于是恩雅以同样的方式演唱了另外两首歌‘Less Than a Pearl’ 和 ‘The River Sings’。这张专辑在全球大获成功，在Billboard 200排行榜上排名第6，在英国排名第8。主打歌‘Amarantine’于2005年12月发行。
<div align="center">
   <img src="https://foruda.gitee.com/images/1670568982550767888/7f128b43_11991685.jpeg" width="500" height="500">
</div>

[Enya-Amarantine-官方宣传MV](https://www.bilibili.com/video/BV1Gx411K77X/?share_source=copy_web&vd_source=56a20057a732814d724e92793a4914c6)

● Amarantine歌词大意：
<br>
你知道什么时候你放弃了你的爱
<br>
You know when you gave your love away
<br>

它打开你的心，一切都如此崭新
<br>
It opens your heart, everything is new
<br>

你知道时间总会找到办法
<br>
And you know time will always find a way
<br>
让你的心相信这是真的
<br>
To let your heart believe it's true
<br>

你知道爱就是你所说的一切
<br>
You know love is everything you say
<br>

耳语，一句话，你给的承诺
<br>
A whisper, a word, promises you give
<br>

你在一天的心跳中感受到它
<br>
You feel it in the heartbeat of the day
<br>

你知道这就是爱的方式
<br>
You know this is the way love is
<br>

Amarantine
<br>
Amarantine
<br>

Amarantine
<br>
Amarantine
<br>

Amarantine
<br>
Amarantine
<br>

爱永远是爱
<br>
Love is always love
<br>

你知道爱有时会让你哭泣
<br>
You know love may sometimes make you cry
<br>

所以放开眼泪，它们会流走
<br>
So let the tears go, they will flow away
<br>

因为你知道爱总是会让你飞翔
<br>
For you know love will always let you fly
<br>

一颗心能飞多远
<br>
How far a heart can fly away
<br>

Amarantine
<br>
Amarantine
<br>

Amarantine
<br>
Amarantine
<br>

Amarantine
<br>
Amarantine
<br>

爱永远是爱
<br>
Love is always love
<br>

你知道什么时候爱在你眼中闪耀
<br>
You know when love's shining in your eyes
<br>

可能是天上掉下来的星星
<br>
It may be the stars fallen from above
<br>

你知道当你站起来时爱就在你身边
<br>
And you know love is with you when you rise
<br>

因为白天和黑夜都属于爱
<br>
For night and day belong to love
<br>

词曲作者：Eithne Ni Bhraonain / Nicky Ryan / Roma Ryan
‘Amarantine’歌词 © Sony/ATV Music Publishing LLC, Takwene LLC

## 2007-2009:And Winter Came...和The Very Best of Enya
● And Winter Came...
2007年，恩雅继续创作，为她的第七张录音室专辑《冬天来了》‘And Winter Came...’创作冬季和圣诞节主题的音乐。起初，她打算在2007年底发行一张季节性歌曲和赞美诗专辑，但最终决定制作一张以冬季为主题的专辑。
<div align="center">
   <img src="https://foruda.gitee.com/images/1670585850177692913/6c285a0c_11991685.jpeg" width="500" height="500">
</div>
其中，单曲‘My！My！Time Flies!’是一首致敬已故爱尔兰吉他手吉米·福克纳(Jimmy Faulkner)的歌曲，邀请了帕特·法雷尔(Pat Farrell)来进行吉他独奏，这是恩雅少有的使用吉他的作品。2008年11月发行后，该专辑在英国排名第六，在美国排名第八，2011年全球销量近350万册。

[Enya - And Winter Came](https://www.bilibili.com/video/BV18s411s7wk/?share_source=copy_web&vd_source=56a20057a732814d724e92793a4914c6)

[恩雅Enya-My! My! Time Flies!现场-英国BBC-The Alan Titchmarsch show节目+采访](https://www.bilibili.com/video/BV18s411k7F4/?share_source=copy_web&vd_source=56a20057a732814d724e92793a4914c6)

● My! My! Time Flies!歌词大意：
<br>
哇！哇！光阴似箭，一步之遥
<br>
My, my, time flies, one step
<br>
我们到了月球上
<br>
And we're on the moon
<br>
下一步进入星空
<br>
Next step into the stars
<br>
哇！哇！光阴似箭
<br>
My, my, time flies
<br>
也许我们很快就能到那
<br>
Maybe we could be there soon
<br>
一张去火星的单程票
<br>
A one way ticket to Mars
<br>
哇！哇！光阴似箭
<br>
My, my, time flies
<br>
一个人在树下
<br>
A man underneath a tree
<br>
一个苹果掉在了他的头上
<br>
An apple falls on his head
<br>
哇！哇！光阴似箭
<br>
My my time flies
<br>
一个人写了一部交响曲，那是 1812 年
<br>
A man wrote a symphony, it's 1812
<br>
哇！哇！光阴似箭
<br>
My, my, time flies
<br>
四个人穿过艾比路
<br>
Four guys across Abbey Road
<br>
一个忘了穿鞋
<br>
One forgot to wear shoes
<br>
哇！哇！光阴似箭
<br>
My, my, time flies
<br>
说唱狂想曲
<br>
A rap on a rhapsody
<br>
一个仍在新闻中的国王
<br>
A king who's still in the news
<br>
国王为你唱蓝调
<br>
A king to sing you the blues
<br>
哇！哇！光阴似箭
<br>
My, my, time flies
<br>
一个冬天雪橇上的人
<br>
A man in a winter sleigh
<br>
白白白如雪
<br>
White white white as the snow
<br>
哇！哇！光阴似箭
<br>
My, my, time flies
<br>
新的一天即将到来
<br>
A new day is on its way
<br>
所以就让昨天过去吧
<br>
So let's let yesterday go
<br>
我们可以再次走出去吗
<br>
Could be we step out again
<br>
可能是明天，但那时
<br>
Could be tomorrow but then
<br>
可能是2010年
<br>
Could be 2010
<br>
词曲作者：Eithne Ni Bhraonain / Nicky Ryan / Roma Ryan
My! My! Time Flies!歌词 © Sony/ATV Music Publishing LLC

● The Very Best of Enya
在《冬天来了》之后，恩雅暂停了作曲和录制音乐，休息了一段时间，包括去澳大利亚探亲，以及翻修她在法国南部的新家。她的第二张合辑‘The Very Best of Enya’于2009年11月发行，收录了1987年至2008年的歌曲，包括之前未发行的“Aníron”版本和迄今为止她大部分音乐视频的DVD。
<div align="center">
   <img src="https://foruda.gitee.com/images/1670586435230843629/3469873e_11991685.jpeg" width="500" height="500">
</div>

[恩雅Enya - The very best of Enya](https://www.bilibili.com/video/BV1RK4y187Y5/?share_source=copy_web&vd_source=56a20057a732814d724e92793a4914c6)


## 2012-2015：Dark Sky Island
2012年，恩雅回到录音室录制她的第八张专辑《黑暗天空岛》‘Dark Sky Island’。该专辑的名字指的是萨克岛，第一个被指定为黑暗天空保护区的岛屿。这张专辑收录的歌曲都以岛屿为主题。2015年11月20日发行后，‘Dark Sky Island’在英国排名第四，这是恩雅自‘Shepherd Moons’排名第一以来在英国排名最高的录音室专辑，在美国排名第八。
<div align="center">
   <img src="https://foruda.gitee.com/images/1670587033797035005/5b66fca0_11991685.jpeg" width="500" height="500">
</div>

[恩雅 Enya - Dark Sky Island (Deluxe)黑暗的天空岛 (豪华版) flac](https://www.bilibili.com/video/BV1VB4y167az/?share_source=copy_web&vd_source=56a20057a732814d724e92793a4914c6)

## 2015-现在
在2015年后，恩雅不再像过去那样高频率地创作，而更多地把时间放在表演和参加活动中。恩雅在英国、欧洲、美国和日本进行了巡回演出之旅。2016年12月，恩雅出现在爱尔兰电视节目《来自科克的圣诞颂歌》中并演唱了几首歌，这是她七年来首次在爱尔兰电视节目中亮相。
2020年，恩雅在她的油管频道中透露，工作室正在翻新，等施工完成后，她可能会创作新的作品。

# 恩雅的音乐风格和外界评价
恩雅的音乐风格独树一帜，许多人都认为她是新世纪音乐风格的代表，但她本人总是说“这只是恩雅的风格”。恩雅的嗓音是女中音。她说自己的音乐基础是“经典音乐”、教堂音乐和“爱尔兰摇摆舞曲”。她的音乐风格包含了凯尔特、古典、教堂、新时代、世界、流行和爱尔兰民间音乐的元素。恩雅在她的歌曲中除了演奏打击乐、吉他、乌伊利安管、短号和低音提琴外，还表演了所有的人声和大部分乐器。
作为一位音乐家，恩雅的音乐获得了非常广泛的认可。恩雅的专辑在美国已售出2650万张，在全球约有8000万张，使她成为有史以来最畅销的音乐艺术家之一。《无雨的一天》(2000)仍然是新时代最畅销的专辑，全球销量约为1600万张。恩雅获得了无数奖项，包括七项世界音乐奖，四项格莱美最佳新时代专辑奖，以及一项Ivor Novello奖。她曾凭借为《指环王:护戒使者》(2001)创作的《May It Be》获得奥斯卡金像奖和金球奖提名。

# 个人感想
第一次听到Enya的歌曲来源于小时候父母买来的DVD。她的声音非常独特，一听就能记得她；她的歌曲也常常有着欢快悠扬的旋律，制作的MV也都极具想象力和油画般的场景。对于她的主要记忆并不是完全来自于指环王，更多地是她那些主打歌曲。非常朗朗上口，也希望更多的人能够喜欢她的音乐。



