转自：[这个颁奖典礼上，来了几位大山里的“小诗人”](https://new.qq.com/rain/a/20221123A05MQ700) （[笔记](https://gitee.com/yuandj/siger/issues/I66L12#note_15510765_link)）

![](https://foruda.gitee.com/images/1672535769844269981/e9e8ae40_5631341.png)

- 《月光与晚霞》

  > 月光爱上了美丽的晚霞

  > 可每当他兴奋地

  > 来见心上人时

  > 晚霞早就不见了踪影