- [Dragon SpX-14 Cargo Overview](https://spaceflight101.com/dragon-spx14/cargo-overview/)
- [龙SpX-14货物概述](#龙SpX-14货物概述)
- [国际空间站到处是霉菌？宇航员健康受到威胁，中国也要重视](https://new.qq.com/rain/a/20211119A07LWM00)

<p><img width="706px" src="https://foruda.gitee.com/images/1676918221726776069/2a27fda7_5631341.jpeg" title="557龙飞船载重微生物追踪2副本.jpg"></p>

![输入图片说明](https://foruda.gitee.com/images/1676901860002680182/61b65bb2_5631341.png "屏幕截图")

<li id="menu-item-78" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-78"><a href="http://spaceflight101.com">Home</a></li>
<li id="menu-item-216" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-216"><a href="https://spaceflight101.com/members">Members</a></li>
<li id="menu-item-79" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-79"><a href="http://spaceflight101.com/news/">News</a></li>
<li id="menu-item-80" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-80"><a href="http://spaceflight101.com/calendar/">Calendar</a></li>
<li id="menu-item-81" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-81"><a href="http://spaceflight101.com/spacerockets/">Rockets</a></li>
<li id="menu-item-104" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-104"><a href="http://spaceflight101.com/iss/">ISS</a>
<ul class="sub-menu">
	<li id="menu-item-116" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-116"><a href="http://spaceflight101.com/iss/news/">ISS Updates</a></li>
	<li id="menu-item-117" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-117"><a href="http://spaceflight101.com/iss/iss-calendar/">ISS Calendar</a></li>
	<li id="menu-item-118" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-118"><a href="http://spaceflight101.com/iss/iss-mission-archive/">Mission Archive</a></li>
</ul>
<span class="sub-toggle"> <i class="fa fa-angle-right"></i> </span></li>
<li id="menu-item-82" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-82"><a href="http://spaceflight101.com/nasa/">NASA</a>
<ul class="sub-menu">
	<li id="menu-item-100" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-100"><a href="http://spaceflight101.com/nasa-human-spaceflight/">NASA Human Spaceflight</a></li>
	<li id="menu-item-101" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-101"><a href="http://spaceflight101.com/nasa-interplanetary-missions/">Interplanetary Missions</a></li>
	<li id="menu-item-102" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-102"><a href="http://spaceflight101.com/nasa-earth-sciences-missions/">Earth Sciences Missions</a></li>
	<li id="menu-item-103" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-103"><a href="http://spaceflight101.com/nasa-science-missions/">Science Missions</a></li>
</ul>
<span class="sub-toggle"> <i class="fa fa-angle-right"></i> </span></li>
<li id="menu-item-83" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-83"><a href="http://spaceflight101.com/esa/">ESA</a></li>
<li id="menu-item-84" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-84"><a href="http://spaceflight101.com/commercial/">Commercial</a>
<ul class="sub-menu">
	<li id="menu-item-105" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-105"><a href="http://spaceflight101.com/spacex/">SpaceX</a></li>
	<li id="menu-item-106" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-106"><a href="http://spaceflight101.com/united-launch-alliance/">United Launch Alliance</a></li>
	<li id="menu-item-107" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-107"><a href="http://spaceflight101.com/arianespace/">Arianespace</a></li>
	<li id="menu-item-108" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-108"><a href="http://spaceflight101.com/orbital-atk/">Orbital ATK</a></li>
	<li id="menu-item-109" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-109"><a href="http://spaceflight101.com/ils/">ILS</a></li>
	<li id="menu-item-213" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-213"><a href="http://spaceflight101.com/category/sierra-nevada">Sierra Nevada</a></li>
	<li id="menu-item-214" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-214"><a href="http://spaceflight101.com/category/blue-origin/">Blue Origin</a></li>
	<li id="menu-item-110" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-110"><a href="http://spaceflight101.com/sea-launch/">Sea Launch</a></li>
</ul>
<span class="sub-toggle"> <i class="fa fa-angle-right"></i> </span></li>
<li id="menu-item-87" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-87"><a href="http://spaceflight101.com/other/">Other</a>
<ul class="sub-menu">
	<li id="menu-item-85" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-85"><a href="http://spaceflight101.com/russia/">Russia</a></li>
	<li id="menu-item-86" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-86"><a href="http://spaceflight101.com/china/">China</a></li>
	<li id="menu-item-88" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-88"><a href="http://spaceflight101.com/japan/">Japan</a></li>
	<li id="menu-item-89" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-89"><a href="http://spaceflight101.com/india/">India</a></li>
	<li id="menu-item-215" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-215"><a href="http://spaceflight101.com/category/north-korea">North Korea</a></li>
	<li id="menu-item-111" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-111"><a href="http://spaceflight101.com/spacecraft/satellites-by-origin/">Satellite Catalog</a></li>
	<li id="menu-item-161" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-161"><a href="http://spaceflight101.com/countdown-timelines/">Countdown Timelines</a></li>
</ul>
<span class="sub-toggle"> <i class="fa fa-angle-right"></i> </span></li>
<li id="menu-item-90" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-90"><a href="http://spaceflight101.com/re-entry/">Re-Entry</a></li>
<li id="menu-item-93" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-93"><a href="http://spaceflight101.com/archive/">Archive</a>
<ul class="sub-menu">
	<li id="menu-item-91" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-91"><a href="http://spaceflight101.com/search/">Search</a></li>
</ul>
<span class="sub-toggle"> <i class="fa fa-angle-right"></i> </span></li>

# [Dragon SpX-14 Cargo Overview](https://spaceflight101.com/dragon-spx14/cargo-overview/)

<img align="right" width="299px" src="https://foruda.gitee.com/images/1676903412521637355/4a6f0c08_5631341.jpeg" title="Dragon C110 during its first mission – Photo: NASA">

Dragon SpX-14 is the fourteenth operational mission of SpaceX’s Dragon spacecraft to the International Space Station under NASA’s Commercial Resupply Services contract. It is the first of at least three Dragon cargo missions planned in 2018.

Loaded with more than two and a half metric tons of cargo, Dragon will be delivering the typical mix of utilization hardware, maintenance gear and crew supplies to the Space Station to keep up its capability of serving as a world-class laboratory in Low Earth Orbit. The 14th regular Dragon flight will carry three external payloads to the Space Station: the MISSE Flight Facility as a new state-of-the-art exposure facility for materials science outside the Space Station, the ASIM instrument to study interactions where Earth’s atmosphere meets space, and a spare pump assembly for the Station’s photovoltaic power-generation system.

The Dragon SpX-14 mission is part of the CRS-1 contact extension awarded by NASA to bridge a gap to the second round of Commercial Resupply Services contracts that cover the Space Station’s cargo requirements for the first half of the 2020s. Under the CRS-1 extension, SpaceX will keep flying Dragon 1 spacecraft through CRS-20 while Orbital ATK received an order of three additional missions.

<img align="left" width="299px" src="https://foruda.gitee.com/images/1676903574367215997/36d6bdf5_5631341.jpeg" title="Dragon SpX-12 Launch – Photo: SpaceX">

Dragon SpX-14 is the third SpaceX cargo mission to fly a re-used spacecraft and the second to employ a “flight-proven” Falcon 9 first stage as part of the company’s re-use business model that has taken major steps toward becoming routine over the past year. The SpX-14 mission is re-using the Dragon C110 spacecraft that spent 33 days in orbit in April/May 2016 supporting the [Dragon SpX-8](http://spaceflight101.com/dragon-spx8/) mission, delivering 3,136 Kilograms of cargo to the Space Station including the Bigelow Expandable Activity Module. The Falcon 9 launching this mission will employ Booster #1039 from the SpX-12 mission of August 2017.

NASA completed extensive reviews of data on the condition of previously flown Falcon 9 first stages and life-leader experimentation as well as SpaceX’s successful re-use missions in 2017 to conclude that the use of flight-proven first stages comes with no to minimal additional risk to the success of the overall CRS mission. Engineering reviews cleared Dragon missions to fly on first stages with no more than one prior Low Earth Orbit mission, excluding first stages that have gone through more rigorous re-entry environments when flying higher energy missions like GTO deliveries. Dragon SpX-13 in late 2017 was the first NASA CRS mission to fly on a previously used Falcon 9.

<img align="left" width="299px" src="https://foruda.gitee.com/images/1676903669137410882/13229053_5631341.png" title="Image: NASA">

All in all, Dragon SpX-14 is delivering 2,647 Kilograms of cargo to the International Space Station, primarily focused on utilization hardware and dozens of science experiments – some of which are to be completed while Dragon is attached to ISS in order to ride back to Earth on the spacecraft. Also aboard the Dragon is the largest satellite to be deployed from the Space Station to date.

Dragon takes a unique spot on the Space Station’s cargo vehicle roster, given its ability of returning meaningful quantities of cargo to the ground – allowing for the return to performed experiments for laboratory analysis and the return of failed hardware for inspections and/or refurbishment. To that end, Dragon SpX-14 will be tasked with ferrying nearly two metric tons of cargo back to Earth, primarily consisting of science hardware and experiment samples riding back to the ground in laboratory freezers and double cold bags.

- Total Cargo: 2,647 Kilograms
  - Pressurized Cargo (with packaging): 1,721 Kilograms
    - Science Investigations: 1,070 Kilograms
      - Satellites: [Remove Debris](http://spaceflight101.com/dragon-spx14/removedebris/), [Overview 1A](http://spaceflight101.com/dragon-spx14/overview-1a/) (?)
    - Vehicle Hardware: 148 Kilograms
    - Crew Supplies: 344 Kilograms
    - Computer Resources: 49 Kilograms
    - Spacewalk Equipment: 99 Kilograms
    - Russian Cargo: 11 Kilograms
  - Unpressurized Cargo: 926 Kilograms
    - [MISSE-FF – Materials on ISS Experiment – Flight Facility](http://spaceflight101.com/dragon-spx14/misse-ff/)
    - [ASIM – Atmosphere-Space Interactions Monitor](http://spaceflight101.com/dragon-spx14/asim/)
    - [PFCS – Pump Flow Control Subassembly](http://spaceflight101.com/dragon-spx14/pfcs/)

## RemoveDebris Satellite

<img align="right" width="299px" src="https://foruda.gitee.com/images/1676903850516053353/1ef69437_5631341.jpeg" title="Photo: SSTL">

RemoveDebris is a small satellite mission by Surrey NanoSatellite Technology Ltd. under a European Union Framework 7 research project to develop and fly a low-cost demonstrator for the key aspects of Active Debris Removal missions on a quest to address the growing space debris problem.

The 100-Kilogram, cube-shaped satellite – the largest deployed from the International Space Station to date (2018) – will demonstrate active debris removal techniques by releasing, tracking and capturing two small CubeSats called DebrisSATs, in the process demonstrating different rendezvous, capture and deorbiting techniques to evaluate their feasibility for operational debris removal missions of the future.

- [RemoveDebris Satellite Overview](http://spaceflight101.com/dragon-spx14/removedebris/)

## MISSE Flight Facility

<img align="right" width="299px" src="https://foruda.gitee.com/images/1676907713208833291/3b0809f8_5631341.png" title="Image: AlphaSpace">

The Materials on ISS Experiment – Flight Facility (MISSE-FF) builds on the success of the original MISSE Passive Experiment Container concept which consisted of smaller and larger sample plates containing a variety of surface materials for exposure to the space environment outside the International Space Station for varying durations to inform satellite designers on how different materials degrade over time.

The MISSE-FF project created a platform capable of holding 14 exchangeable sample modules for powered & heated payloads as well as passive experiments.

- [Detailed MISSE-FF Overview](http://spaceflight101.com/dragon-spx14/misse-ff/)

## ASIM – Atmosphere-Space Interactions Monitor

<img align="right" width="299px" src="https://foruda.gitee.com/images/1676907816125064778/184bbdc2_5631341.jpeg" title="Photo: NASA">

ASIM, the Atmosphere-Space Interactions Monitor, is an ESA science instrument taking up residence outside the Columbus Module of the International Space Station to study Transient Luminous Events (TLEs) in Earth’s upper atmosphere like Blue Jets, Red Sprites and Elves via a suite of cameras and photometers sensitive in a broad wavelength range to reveal previously unknown details of the electrical and chemical processes ongoing where the atmosphere and space interact.

- [ASIM Instrument Overview](http://spaceflight101.com/dragon-spx14/asim/)

## PFCS Spare

<img align="right" width="299px" src="https://foruda.gitee.com/images/1676907889510196707/baaf2f54_5631341.png" title="Image: NASA">

The third Trunk Payload riding on the Dragon SpX-14 mission is not a science/utilization instrument like its two companions but a potentially critical spare part for the Space Station’s Thermal Control System. The Pump Flow Control Subassembly, PFCS for short, is a critical component of the ISS Photovoltaic Thermal Control System (PVTCS) in that it routes ammonia coolant to transport heat from the various electrical assemblies located within the Integrated Equipment Assembly to a Photovoltaic Radiator where it is dissipated into space.

- [PFCS Overview](http://spaceflight101.com/dragon-spx14/pfcs/)

## NASA Sample Cartridge Assembly

<img align="left" width="299px" src="https://foruda.gitee.com/images/1676908047721957572/62b86540_5631341.png" title="Liquid phase sintered tungsten alloy – Image: San Diego State University">

The MSL SCA-GEDS-German experiment will be run in the Materials Science Laboratory aboard ESA’s Columbus module to look into the underlying mechanisms of sintering processes and their ability of creating hardened materials. The Physical Sciences experiment was developed at San Diego State University under Principal Investigator Randall German.

Sintering is the process of compacting and forming a solid mass of material by heat or pressure, typically with the goal of creating a hardened piece of material. Typically, sintering occurs without melting the material to the point of liquefecation, though liquid-phase sintering has been a method used for the fabrication of net-shaped composite materials.

Although sintering has been used for centuries and is a critical element in a large number of industrial branches today, the underlying scientific principles are only poorly understood. The goal of this experiment is to determine the mechanisms driving the density, size, shape, and properties for liquid phase sintered bodies in Earth-gravity and microgravity conditions with special focus on the causes of distortion in the material that appears to be alleviated by the presence of a gravitational force.

<img align="right" width="299px" src="https://foruda.gitee.com/images/1676908091786510344/ab447f08_5631341.png" title="Material Science Lab – Image: ESA">

During low-temperature sintering, powders can gain strength through interparticle bonding – typically through solid-state surface diffusion, followed by further strengthening at high temperatures driven by densification of the powder material. However, a secondary process occurring at high temperatures causes a softening of the material due to a distortion phenomenon. While conducive for the formation of net-shaped composites it is counterproductive if only densification is desired. Working out the interplay between the various processes may allow for a sintering method to be developed that can accomplish densification without distortion.

Previous studies have shown surprising results as gravity may be playing a beneficial role in reducing distortion – in contrast to a very large number of physical processes where gravity has been identified as a disturbing force. Microgravity liquid-phase sintering experiments performed to date have shown a lesser degree of densification and higher distortion, raising questions on the underlying mechanisms to fully understand the role of gravity to work out routes for minimized distortion.

It has been found that when a liquid phase forms, densification can be accelerated via solid transport within the liquid, capillary forces and liquid lubrication. This, however, only works to a certain degree as long as there are solid bonds or open pores in the sintering body. If the degree of liquefecation outweighs the solid material, substantial weakness is introduced.

Scientists have likened this process to building a sand castle which works poorly when one only has dry sand with no strength and no ability to hold shape or when sand is saturated with water. It works best with an intermediate mix where the liquid pulls the sand grains into contact and gives the greatest strength.

<img align="right" width="299px" src="https://foruda.gitee.com/images/1676908134361368739/9b9f6842_5631341.png" title="Image: ESA">

The MSL SCA-GEDS-German experiment observes phase changes and product formation within solid mixtures undergoing spontaneous reaction in the absence of gravity to find out what causes lower performance, an inability to eliminate pores, and higher distortion. Cartridges with different sample materials will be delivered to ISS by Dragon SpX-14 and fully processed within the Materials Science Laboratory (MSL) Low Gradient Furnace (LGF) followed by return to Earth for sample analysis with the added context of temperature profiles and other sensor data from the furnace.

Knowledge gained by this experiment will be beneficial for sintering processes on Earth as information on the underlying mechanisms of distortion will lead to better protocols to increase densification. For the spacefarers of the future, knowledge on sintering in zero- or reduced gravity environments is of importance as extraterrestrial repair and construction based on freeform fabrication from powders will be a key element of settlements on the Moon, Mars and beyond.

## Nano Racks Microscopes Facility

<img align="left" width="299px" src="https://foruda.gitee.com/images/1676908185766186749/84cd75a5_5631341.png" title="Photo: NanoRacks">

The NanoRacks Microscope Facility hosts three Commercial Off The Shelf (COTS) microscopes for use on the International Space Station to study in-situ samples in a simplified architecture using plug-and-play USB interfaces to allow the microscopes to be used from any laptop. “The NanoRacks Microscopes are ideally suited for examining specimen slides of yeasts and molds, cultures, plant and animal parts, fibers, bacteria, etc”

The first of the three Microscopes offers objectives for 5x, 10x and 20x magnification, Microscope-2 has the same video head but adds a lighting system and offers 20x to 40x, and 200x digital power magnification as well as a 20x eyepiece for viewing with 4x to 8x, and 50x power. Microscope-3 is a handheld microscope with a 5-megapixel imager, adjustable polarization to set the proper light level and reduce glare, eight LEDs for sample illumination, and 10x to 240x zoom magnification.

## Multi-Use Variable-G Platform

<img align="left" width="299px" src="https://foruda.gitee.com/images/1676908266372023700/3ba84fdf_5631341.jpeg" title="Image: Techshot">

The Multi-Use Variable-G Platform, MVG, is a product of Techshot to expand the Space Station’s scientific repertoire by adding a new centrifuge facility capable of producing anything between microgravity and 2 G of artificial gravity. This will open up possibilities for a large number of studies, including commercial exploitation of the novel environment found on the International Space Station. MVG is suitable for a number of sample types, including fruit flies, flatworms, plants, fish, cells, protein crystals and many others.

MVP is a commercially developed, manufactured, owned and operated platform, offering a pair of 39-centimeter carousels that can produce up to 2G of artificial gravity with six experiment modules on each carousel. The facility is designed to allow for easy exchange of sample modules, permitting a large number of experiments to be completed with little crew time requirements. Real-time video and still imagery, including microscopy, can be provided for each sample module per the specific needs of every experiment and the facility provides additional environmental control with a temperature range of 14 to 40 °C and a humidity between 50 ad 80% while data logging is provided for Oxygen and Carbon Dioxide.

## Veggie PONDS

<img align="right" width="299px" src="https://foruda.gitee.com/images/1676908320851079826/ee76b0b6_5631341.png" title="Veggie Facility – Photo: NASA">

Veggie PONDS uses the existing Veggie plant-growth hardware present on the International Space Station as well as knowledge gained through the initial experiment runs to develop a passive nutrient delivery system that could build the basis for the reliable plant production facilities on future long-duration space missions. PONDS, the Passive Orbital Nutrient Delivery System, addresses some of the deficiencies found with the standard plant pillow system employed by the initial Veggie runs and will also expand the facility’s envelope by supporting larger leafy vegetables and fruit crops like tomatoes which will be grown as part of the Veg-05 experiment.

The PONDS system is designed to mitigate microgravity effects on water distribution, increase oxygen exchange and provide sufficient room for root zone growth. The initial PONDS experiment has the goal of validating whether improved water and nutrient delivery can produce a more uniform plant growth and increase crop yields.

<img align="left" width="299px" src="https://foruda.gitee.com/images/1676908360932907183/18640f48_5631341.png" title="Lettuce Plants growing inside Veggie Facility – Photo: NASA">

The experiment will use ‘Outredgeous’ red romaine lettuce and mizuna mustard plants, the former has been used for studies Veg-01 through Veg-03 and will allow for comparisons between plant pillows and PONDS. Another aspect will be studying the microbial load of plants grown during the initial Veggie runs and those provided with PONDS technology.

The PONDS architecture was designed at the Kennedy Space Center and involves contributions by TechShot, and commercial partner Tupperware. PONDS retains the arcillite, a calcined clay type, as growth medium which has been selected through comparative studies in the earlier Veggie runs and PONDS continues as single-use hardware, only employed for one growth run to manage microbial contamination. Like previous runs, PONDS will use the cut-and-come again harvest technique with four planned harvests or more if plant growth allows.

The crew will be cleared to consume any lettuce that is not needed for analysis on the ground.

<img align="right" width="299px" src="https://foruda.gitee.com/images/1676908408232176048/bab5660d_5631341.png" title="Mixed Crops inside Veggie – Photo: NASA">

Veg-01 ran in 2014 and provided valuable data in the form of returned water samples and root pads, imagery acquired in orbit of the growth process, and plant samples that were brought back to Earth. This data helped investigators assess the two different growth media with respect to water and root distribution within the different sized particles to chose media for future Veggie missions.

Although the overall Veg-01 experiment was a success, a number of deficiencies with regard to the plant pillows and the water delivery systems were identified leading to modifications made to the pillows and watering procedures that will be tested by Veg-03, also introducing a different crop with different water requirements. In 2015, the Space Station crew got their first taste of home-grown lettuce harvested from the Veggie Plant Growth Unit. Another 2015 study provided the crew with a touch of color when the first flowers grown on ISS were harvested by Astronaut Scott Kelly.

<img align="right" width="299px" src="https://foruda.gitee.com/images/1676908450816485751/12ffea79_5631341.jpeg" title="Space-Grown Zinnia Flowers in Veggie Facility – Photo: NASA">

Veg-03 A-C tested different crop harvest techniques, showing that cut-and-come again repetitive harvest could be used to double the amount harvested with the same set of starting materials; Veg-03 B and C tested a new crop, Tokyo bekana. The Veg-03 D-F experiments looked into mixed growth of different leafy greens and different harvest schedules.

The Veggie experiment facility provides lighting and nutrient supply and is capable of supporting a variety of plant species that can be cultivated for educational outreach, fresh food and even recreation for crewmembers on long-duration missions. Thermal control is provided from ISS in-cabin systems and the carbon dioxide source is the ambient air aboard ISS.

Plants grown in the Veg-03 facility will be observed to determine how plants sense gravity and how they respond to microgravity. Serving as a pathfinder, the plants grown as part of VEGGIE will be harvested and studied before being cleared for consumption by crew members in orbit. The VEGGIE facility is the largest volume available aboard ISS for plant growth, which will allow the study of larger plants that could not be grown in previous experiments.

<img align="right" width="299px" src="https://foruda.gitee.com/images/1676908502127754656/18579c75_5631341.png" title="Photo: ORBITEC">

Veggie uses a plant growth chamber using planting pillows and an LED bank to provide lighting. Ground testing of the pillow planting concept led to the selection of growth media and fertilizers, plant species, materials, and protocols. The facility weighs 7.2 Kilograms and measures 53 by 40 centimeters and permits a maximum growth height of 45 centimeters. The root mat has a growing area of 0.16m² with a 2-liter fluid reservoir.

The system draws 115W of peak power and its LED banks can support adjustable wavelengths, light levels and day and night cycles to match the biological needs of the plants. A transparent teflon cover allows viewing of the plants. The plants will be photographed regularly to assess plant growth rates and health. Tissue samples will provide information on possible growth anomalies when being compared to ground controls. Environmental data will be provided by a data logger that measures temperature, humidity and pCO2.

The first studies performed with VEGGIE will also provide microbial samples of the plants and pillows to assess the level of microbial contamination and implement corrective measures if needed. For most species, microbial contamination levels will be well within limits and pose no threat to the crew. Other species that naturally have higher levels of microorganisms may need a sanitation method which must be developed and tested as part of the experiment. Growing plants in space provides crewmembers with fresh foods to supplement their diets, as well as a positive effect on morale and well-being.

## NanoRacks Module 74 – Wound Healing

<img align="right" width="299px" src="https://foruda.gitee.com/images/1676908644025672841/d242d437_5631341.png" title="Plate Reader 2 – Photo: NASA">

The Wound Healing Experiment will evaluate a patch containing an antimicrobial hydrogel that promotes wound healing while also acting as a scaffold for tissue regeneration. The study’s formal name is “Investigation of the Effects of Microgravity on Controlled Release of Antibiotics and Curing Mechanism of a Novel Wound Dressing (Hydrogel Formation and Drug Release in Microgravity Conditions).”

Performing the experiment in microgravity is necessary to properly assess the hydrogel behavior in an environment of reduced fluid motion due to the absence of gravity-induced processes. This will allow for a more precise analysis of the release of the antibiotic agent from the cross-linked or mixed hydrogel patches. The improvement of drug-releasing dermal dressings and patches is of great desire for the prevention of sepsis; however, there currently is no wound dressing that can release antibiotics for long enough to successfully prevent sepsis – a major issue faced by combat-inured soldiers.

The goal of NanoRacks Module 74 is providing the data needed for evaluating a series of hydrogel formulations and study how drugs release from the gels in the absence of gravity to further the development of new devices for humans in high-stress environments. Understanding the mechanisms of drug release in microgravity can also be beneficial for the development of patches, implants or other devices to help keep astronauts physically robust while on extended journeys to distant targets.

The NanoRacks Module 74 flight equipment employs NanoRacks Reactor Microplates serving as reaction vessels and providing the crosslink for the hydrogels. Drug release from the gels is measured using the NanoRacks Plate Reader-2. Experiments will be run with unloaded hydrogels to study their mechanical and structural properties while a second set of experiments involves crosslinking hydrogels loaded with antibiotics and soaking them in enzyme solution to simulate cellular degradation and antibiotic release from the gels. The release profile – collected by means of spectrometry – will be measured over a period of two weeks.

## Marrow

<img align="right" width="299px" src="https://foruda.gitee.com/images/1676908670814150859/35b4e2b9_5631341.jpeg" title="Blood Sampling aboard ISS – Photo: NASA">

The MARROW study, known by its full name Bone Marrow Adipose Reaction: Red Or White?, looks at the effect of microgravity on the human bone marrow. It is known that prolonged exposure to microgravity, just like long bed rest on Earth, has a negative effect on bone marrow and the production of blood cells. The extent of this effect and possible recovery are of interest in space research and for application on Earth.

Blood-producing cells of the bone marrow share the confined space within the bone with fat cells which are known to grow at the expense of blood-producing cells during prolonged bed rest. Accumulation of fat cells may directly impact blood cell production and the bone marrow may also provide an explanation for abnormalities in red and white blood cells observed in zero gravity.

<img align="right" width="299px" src="https://foruda.gitee.com/images/1676908738834986647/8005d2d5_5631341.jpeg" title="Photo: NASA">

The MARROW study aims to measure fat changes in the bone marrow before and after space flight. Also, the investigation looks at specific changes to red and white blood cells over the course of a mission.

MARROW requires crew members to undergo pre- and post-flight MRT measurements to assess their bone marrow fat while changes in red and white blood cell count and functions are tracked throughout the flight by means of regular blood sampling, chromatographic analysis of breath samples for red blood cell function and changes in gene expression for white blood cells.

Data collected by MARROW is hoped to shed light on the mechanisms for spaceflight anemia (red blood cell function), susceptibility to infection (white blood cell function), thrombosis (platelets) in space, and similar conditions on Earth in people with decreased mobility of confined to bed for extended periods.

## ExoLab and Arabidopsis thaliana

<img align="left" width="299px" src="https://foruda.gitee.com/images/1676908825699243140/aab28c93_5631341.jpeg" title="Photo: magnitude.io">

The “Life Cycle of Arabidopsis thaliana in Microgravity” experiment studies the morphology and physiology of thale cress using modular growth chambers for microgravity studies. Plants are grown from germinated seeds under automated light, temperature and nutrient conditions while cameras document the growth at every stage to determine plant viability and the effectiveness of the cultivation modules and nutrient treatment.

The experiment is flown on an ExoLab experiment platform designed by magntitude.io as a new type of educational tool to bring together classrooms and the International Space Station for an investigation of the effects of microgravity on living things, accompanied by lesson material for grades 6 through 8. “Working with school districts across the United States along with the Center for the Advancement of Science in Space, Magnitude.io seeks to provide an extraordinary exobiology experience mapped to accepted local science standards while dramatically reducing the cost to access experiments in space,” the company said on its project website.

The initiative includes a space-based experiment and ground-based experiments schools can perform in identical ExoLab facilities procured from magnitude.io. Students can look for relationships between the environmental conditions found on Earth and in the space environment that may impact how the plants develop and grow.

<img align="left" width="299px" src="https://foruda.gitee.com/images/1676908884628650362/dfaa6dfe_5631341.jpeg" title="Photo: magnitude.io">

The ExoLab facility is hosted by the TangoLab facility and is designed based on the CubeSat specification, measuring 10 x 10 x 22 centimeters in size (2 CubeSat Units) and hosting a series of sensors and the plant growth chamber itself. Sensor equipment on ExoLab includes luminosity, temperature, carbon dioxide and humidity sensors as well as a camera to document plant growth. The system transmits its data through the Station’s WiFi network and data will be accessible online after downlink from orbit.

Arabidopsis thaliana has been chosen as a model organism for the experiment since it is a very well understood species and has been used in numerous previous space experiments. It is particularly well suited for a space-based experiment due to its small size and fast growth rate, allowing multiple plants to be studied.

## Metabolic Tracking

The goal of the Metabolic Tracking experiment is the examination of effects of microgravity on the metabolic impact of five different therapeutic compounds and studying the feasibility of autobioluminescent human tissue culture for continuous metabolic activity tracking without destroying the sample culture. The full name of the experiment is Comparative Real-time Metabolic Activity Tracking for Improved Therapeutic Assessment Screening Panels.

Drug studies are frequently found on the Space Station’s science schedule, leveraging the unique environment offered by the Space Station. Some studies take advantage of the fact that the space environment induces effects of aging and bone loss at a much quicker rate while others use the microgravity environment to study drug delivery mechanisms in the absence of masking phenomena induced by gravity-related processes. This enabled a streamlined drug development process and studies into protein crystal structures helps optimize drugs for targeting specific proteins to reduce side effects.

The Metabolic Tracking study evaluates a synthetic luciferase gene that can serve as a real-time tracer of metabolic activity through the emission of light (using processes similar to fireflies or luminous jellyfish). For the experiment, modified human embryonic kidney cells will be monitored to sense light emitted to track the metabolic impact of five different compounds and to illustrate the overall usefulness of the monitoring technique. A major advantage of the luciferase tracking method is that it allows for continuous insight into metabolic rates without invasively affecting the sample cell culture.

The experiment employs autobioluminescent HEK293 cells and the NanoRacks Plate Reader to monitor the autobioluminescent output of the different samples (with five different agents & different concentrations) for a period of 24 hours. A second experiment with identical samples will run in the SABL incubator at 37°C and elevated carbon dioxide concentration to achieve a maximal alteration to metabolic activity.

## Microbial Tracking 2

<img align="left" width="299px" src="https://foruda.gitee.com/images/1676908972622832869/78f97551_5631341.jpeg" title="Photo: NASA">

The Microbial Tracking 2 study will acquire crew samples pre-flight, in-flight and post-flight as well as air and surface samples from various locations of the International Space Station to observe which bacteria, fungi and viruses are present and what impact they may have on the crew’s well being. The overall goal of the research is to catalog and characterize potential disease-causing microorganisms aboard ISS and their impact on crew health.

This collection will last for at least two ISS increments to allow scientists to look at the types of microbes that can survive in the space environment and study their changes over time. This data is valuable for the assessment of risks to crew health and it also allows a close look at the way microbes adept in space as compared to mechanisms known from Earth-based studies.

Experiment data from MT-2 will go down to the cellular level to identify reactions that may be occurring in space but not on Earth which can provide helpful information for the development of antibiotics and antimicrobial agents. The experiment also serves as validation for omics technology in the screening for microbes and identification of those that may be present.

<img align="left" width="299px" src="https://foruda.gitee.com/images/1676909019827915151/2be3abf0_5631341.jpeg" title="Photo: CSA">

MT-2 aims to better understand risks to crew health in a closed environment for infection and illness, assess risk to fouling of clean air supplies and contamination of fluids and food, compare the microbial communities on ISS and on Earth in normal and extreme environments, identifying specific microbes that flourish in the space environment and conduct a study of the adaptation of microbes to the space environment.

Three ISS crew members will participate in the study, collecting baseline data at launch minus six and three months (four sampling operations over one week), in-flight data at L+ 1-2, 2-4 months and return minus ten days and post-flight data at R+1 day, 1 month and 6 months. Samples taken include saliva and oral swabs, skin swabs from forehead, armpit, retroauricular crease, antecubital fossa, naval region and nasal cavity.

The experiment uses air and surface samples collected from the various ISS modules, stored at room temperature and returned to Earth within two weeks of acquisition. Sample methods of MT-2 include adhesive tapes, swabs, contact slides, wipes, gelatin air filters and used gloves. Eight specific locations for surface sampling and six air sampling points have been selected – the zero-G stowage rack (ZSR) surface and dining table inside Unity (Node 1); crew quarters (CQ3) interior port wall inside Harmony (Node 2); overhead hatch area of cupola, rack next to waste and hygiene compartment (WHC), and foot platform of the advance resistive exercise device (ARED) inside Tranquility (Node 3); the ZSR surface inside Leonardo PMM; and the rack front near portable water dispenser (PWD) inside Destiny (Laboratory).

## Additional CASIS National Laboratory Payloads

https://www.youtube.com/embed/fUI8KmPtFeg

## comments

- ABI-Barley Germination

  > This investigation from Budweiser is continuation research that seeks to evaluate the germination of barley seeds and growth of barley seedlings grown in a microgravity environment. Morphological and genetic effects of the microgravity environment on samples will be assessed in orbit and upon return. A better understanding of barley’s response to microgravity could help investigators adapt the grain for use in long-duration spaceflight.

- Genes in Space – 5

  > The Genes in Space student research competition, founded by Boeing and miniPCR and supported by CASIS, invites students to propose pioneering DNA amplification experiments that utilize the unique environment of the ISS. Two experiments were selected as winners from the 2017 competition, both flying on this mission. Elizabeth Reizis, age 14, from Stuyvesant High School in New York will examine the effects of microgravity on the differentiation of immune system cells. Sophia Chen, age 14, from Lakeside School in Washington aims to measure cancer-inducing genomic instability in astronauts. Spaceflight causes many changes to the human body, including a weakened immune system and alterations in DNA. Findings from these experiments could lead to a better understanding of how to safeguard astronaut health in space.

- Higher Orbits Go For Launch!

  > This educational project with students from Morehead (KY), which is part of the Higher Orbits Go For Launch! program, will examine the effectiveness of lactobacillus probiotics in the space environment. The yeast Candida albicans is responsible for a variety of systemic and superficial infections in people with weakened immune systems. This study aims to develop a more complete understanding of the process by which lactobacillus is able to limit Candida growth.

- Effect of BAM-FX Nutrient Solution on Plant Growth in Microgravity

  > This payload from the Quest institute includes four independent experiments. The first will examine the effect of microgravity on plant growth properties using the Bio-Available Mineral Formula-X nutrient solution. The second experiment will observe seed germination and tolerance after exposure to an electric field in microgravity. The third and fourth experiments will assess the behavior of heat and humidity in microgravity based on different heating and cooling methods.

- National Design Challenge-Boy Scouts of America

  > The National Design Challenge is a series of student engagement challenges in which young explorers compete for the opportunity to send experiments to the ISS National Lab. This winning experiment from the Chicagoland Boys Scouts and Explorers is focused on the use of an infrared spectrometer to investigate beta-amyloid peptide, a key contributor towards the proliferation of Alzheimer’s Disease.

- Neutron Crystallographic Studies of Human Acetylcholinesterase for the Design of Accelerated Reactivators – 2

  > Oak Ridge National Lab, in partnership with the University of Tennessee-Battelle, is sending a second experiment to the ISS National Lab aimed at producing high-quality crystals of the medically important enzyme acetylcholinesterase. Crystals grown in microgravity are often larger and more ordered than those grown on the ground. This investigation seeks to utilize the microgravity environment on the ISS to produce crystals large enough for neutron diffraction analysis. Such analysis could provide essential insights into the enzyme’s structure, how it functions in the human body, and how it might be bound by nerve agents to no longer function. Findings from this investigation could lead to the development of novel therapeutics that may decrease morbidity and mortality rates from exposure to chemical agents that affect acetylcholinesterase, such as overexposure to pesticides (both in humans and livestock) or exposure from potential chemical warfare attacks.

- Princeton Institute for the Science and Technology Materials

  > This payload, from the Princeton Institute for the Science and Technology of Materials, seeks to measure the time dilation effect (the idea that time moves more slowly for a moving clock than for one standing still) predicted by Einstein’s theories. To do this, the investigation will examine the performance of a clock designed with nanosecond precision. A secondary payload will examine the mutation rate of isolated plasmid DNA over an extended period of time on the ISS. The DNA sample will be sequenced before and after going to space, and the number of mutations in the flight sample will be compared with the number of mutations in a ground control.

- Space Tango Fan Module

  > This investigation seeks to demonstrate the ability of Space Tango’s Fan Module to direct air coming into the facility toward specific internal elements to keep them cooler. Successful demonstration of the Space Tango Fan Module would increase the number and types of investigations able to be carried out onboard the space station.
 
- Wisconsin Crystal Growing Contest-Wisconsin Space Crystal Mission

  > The winning students from the 2017 Wisconsin Crystal Growing Competition will grow crystals onboard the ISS National Lab to test their optimized conditions for Earth-based crystallization against microgravity-based crystallization. The students adapted their Earth-based crystallization methods to prepare a flight project and will compare the crystals grown in microgravity with crystals grown on the ground.

<h3 class="widget-title"><span>Dragon SpX-14</span></h3>

<ul id="menu-spx14" class="menu"><li id="menu-item-337" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-has-children menu-item-337"><a href="http://spaceflight101.com/dragon-spx14/">Mission Updates</a>
<ul class="sub-menu">
	<li id="menu-item-384" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-384"><a href="https://spaceflight101.com/dragon-spx14/live-dragon-mission-coverage/">Live Mission Coverage</a></li>
</ul>
</li>
<li id="menu-item-338" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-305 current_page_item menu-item-has-children menu-item-338"><a href="https://spaceflight101.com/dragon-spx14/cargo-overview/" aria-current="page">Dragon SpX-14 Cargo Overview</a>
<ul class="sub-menu">
	<li id="menu-item-342" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-342"><a href="https://spaceflight101.com/dragon-spx14/removedebris/">RemoveDebris</a></li>
	<li id="menu-item-343" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-343"><a href="https://spaceflight101.com/dragon-spx14/asim/">ASIM</a></li>
	<li id="menu-item-344" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-344"><a href="https://spaceflight101.com/dragon-spx14/misse-ff/">MISSE-FF</a></li>
</ul>
</li>
<li id="menu-item-339" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-339"><a href="http://spaceflight101.com/spacerockets/falcon-9-ft/">Falcon 9 Launch Vehicle</a></li>
<li id="menu-item-340" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-340"><a href="http://spaceflight101.com/falcon-9-ft-countdown-timeline/">Countdown Timeline</a></li>
<li id="menu-item-385" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-385"><a href="https://spaceflight101.com/dragon-spx14/flight-profile/">Flight Profile</a></li>
<li id="menu-item-341" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-341"><a href="http://spaceflight101.com/spacecraft/dragon/">Dragon Spacecraft Overview</a></li>
</ul>

### Recent Updates

| imgs | title | date |
|---|---|---|
| ![输入图片说明](https://foruda.gitee.com/images/1676913763931200286/5be949b2_5631341.png "ISS Receives new External Research Facilities & Spare Pump as Part of Dragon Cargo Transfers") | [Commercial Space Dragon Expedition 55 Falcon 9 Human Spaceflight ISS NASA News Newsfeed SpaceX](https://spaceflight101.com/dragon-spx14/iss-spx-14-cargo-operations/) | April 14, 2018 |
| ![输入图片说明](https://foruda.gitee.com/images/1676913744378539387/d1fbe36e_5631341.png "Dragon CRS-14 Arrives at ISS after Textbook Rendezvous for Critical Science Delivery") | [Commercial Space Dragon Expedition 55 Falcon 9 Featured Human Spaceflight ISS NASA News Newsfeed SpaceX](https://spaceflight101.com/dragon-spx14/dragon-crs-14-arrives-at-iss-after-textbook-rendezvous-for-critical-science-delivery/) | April 4, 2018 |
| ![输入图片说明](https://foruda.gitee.com/images/1676913708137683790/2423d908_5631341.png "Photos: Recycled Falcon 9 – Dragon Combination Blasts Off from Florida") | [Commercial Space Dragon Expedition 55 Falcon 9 Human Spaceflight ISS NASA Newsfeed Photos SpaceX](https://spaceflight101.com/dragon-spx14/dragon-crs-14-falcon-9-launch-photos/) | April 3, 2018 | 
| ![输入图片说明](https://foruda.gitee.com/images/1676913678331170053/fc7303e5_5631341.png "Science-Laden Dragon Lifted to Orbit by 5th Expendable Falcon 9 in a Row") | [Commercial Space Dragon Expedition 55 Falcon 9 Featured Human Spaceflight ISS Launch NASA News Newsfeed SpaceX](https://spaceflight101.com/dragon-spx14/falcon-9-launches-dragon-crs-14-mission/) | April 2, 2018 |
| ![输入图片说明](https://foruda.gitee.com/images/1676913665037257613/d0065bda_5631341.png "Videos: 14th Operational Dragon Mission Launches on Falcon 9") | [Commercial Space Dragon Expedition 55 Falcon 9 Human Spaceflight ISS NASA News SpaceX Videos](https://spaceflight101.com/dragon-spx14/videos-14th-operational-dragon-mission-launches-on-falcon-9/) | April 2, 2018 |

<p><img width="369px" src="https://foruda.gitee.com/images/1676913915319897949/35190dae_5631341.jpeg" title="CRS14-Patch.jpg"></p>

<p><img width="369px" src="https://foruda.gitee.com/images/1676913926328232747/86fa03ce_5631341.jpeg" title="CfsHKQYWIAQzwH0.jpg-orig1.jpg"></p>

<p><img width="369px" src="https://foruda.gitee.com/images/1676913937473656780/18aaea77_5631341.jpeg" title="s101logo-2013-conv.jpg"></p>

# 龙SpX-14货物概述

> 龙C110第一次执行任务时——图片:美国宇航局

根据美国宇航局的商业再补给服务合同，“龙”SpX-14是SpaceX“龙”飞船到国际空间站的第14次运行任务。这是2018年计划的至少三次龙货运任务中的第一次。

“龙”号运载了超过2.5吨的货物，将向空间站运送典型的使用硬件、维护设备和船员用品，以保持其作为近地轨道世界一流实验室的能力。第14次“龙”号定期飞行将携带三个外部有效载荷到空间站:MISSE飞行设施，作为空间站外最先进的材料科学暴露设施，ASIM仪器，用于研究地球大气与空间的相互作用，以及空间站光伏发电系统的备用泵组件。

“龙”SpX-14任务是NASA授予的CRS-1联系延期的一部分，以弥补第二轮商业再补给服务合同的差距，该合同涵盖了21世纪20年代前半段空间站的货物需求。根据CRS-1的延期，SpaceX将继续在CRS-20中飞行龙1号飞船，而Orbital ATK则收到了另外三个任务的订单。

> 龙SpX-12发射-图片:SpaceX

“龙”SpX-14是SpaceX第三次发射再利用航天器的货运任务，也是第二次使用“经过飞行验证”的猎鹰9号第一级运载火箭，这是该公司再利用商业模式的一部分，该模式在过去一年中朝着常规化迈进了重要一步。SpX-14任务是重新使用龙C110飞船，该飞船于2016年4月/ 5月在轨道上运行了33天SpX-8任务它向空间站运送了3136公斤货物，其中包括毕格罗可膨胀活动模块。执行此次任务的猎鹰9号将使用2017年8月SpX-12任务中的1039号助推器。

NASA完成了对之前飞行的猎鹰9号第一级和生命领袖实验以及SpaceX在2017年成功重复使用任务的数据的广泛审查，得出的结论是，使用经过飞行验证的第一级对整个CRS任务的成功没有任何额外风险。工程审查允许龙任务的第一级飞行不超过一次低地球轨道任务，不包括在执行GTO交付等更高能量任务时经历更严格的再入环境的第一级。2017年底的Dragon SpX-13是NASA首次使用猎鹰9号执行CRS任务。


> 图片:美国国家航空航天局

总而言之，“龙”SpX-14将向国际空间站运送2647公斤货物，主要集中在使用硬件和数十个科学实验上，其中一些实验将在“龙”与国际空间站连接时完成，以便乘坐宇宙飞船返回地球。“龙”号上还装载着迄今为止从空间站发射的最大卫星。

“龙”号在空间站货运飞船名册上占有独特的地位，因为它有能力将相当数量的货物返回地面——允许返回进行实验室分析的实验，以及返回故障的硬件进行检查和/或翻新。为此，“龙”号SpX-14的任务是将近2公吨的货物运回地球，主要包括科学硬件和实验样本，它们被装在实验室冰柜和双冷袋中运回地面。

- 总货物:2,647公斤
  - 加压货物(带包装):1721公斤年代
    - 科学调查:1070公斤
      - 卫星:清除碎片，概述1A (?)
    - 车辆硬件:148公斤
    - 船员补给:344公斤
    - 计算机资源:49公斤
    - 太空行走装备:99公斤
    - 俄罗斯货物:11公斤
  - 无压货物:926公斤
    - ISS实验上的材料-飞行设施
    - 大气-空间相互作用监测器
    - PFCS -泵流量控制组件

## RemoveDebris卫星

> 照片:萨里

“清除碎片”是萨里纳米卫星技术有限公司在欧盟框架7研究项目下的一项小型卫星任务，旨在为主动碎片清除任务的关键方面开发和飞行低成本演示机，以解决日益增长的空间碎片问题。

这颗100公斤重的立方体卫星是迄今为止(2018年)从国际空间站部署的最大的卫星，它将通过释放、跟踪和捕获两个名为“碎片卫星”的小型立方体卫星来演示主动碎片清除技术，在此过程中演示不同的交会、捕获和脱轨技术，以评估其在未来操作碎片清除任务中的可行性。

- 移除碎片卫星概述

## MISSE飞行设施

> 图片:AlphaSpace

ISS实验材料-飞行设施(MISSE- ff)建立在最初MISSE被动实验容器概念的成功基础上，该概念由更小和更大的样品板组成，其中包含各种表面材料，用于暴露在国际空间站外的空间环境中，并在不同的时间内告知卫星设计师不同材料如何随着时间的推移而降解。

MISSE-FF项目创建了一个平台，能够容纳14个可交换的样本模块，用于供电和加热有效载荷以及被动实验。

- MISSE-FF详细概述

## 大气-空间相互作用监测器

> 图片:美国国家航空航天局

ASIM，大气-空间相互作用监测器，是欧空局的科学仪器，在国际空间站的哥伦布模块外居住，通过一套宽波长范围内敏感的相机和光度计，研究地球上层大气中的瞬态发光事件(TLEs)，如蓝色射流，红色精灵和精灵，以揭示以前未知的大气和空间相互作用的电气和化学过程的细节。

- ASIM仪器概述

## 全氟化物备用

> 图片:美国国家航空航天局

搭载在“龙”SpX-14任务上的第三个“干线有效载荷”不像它的两个同伴那样是一个科学/利用仪器，而是空间站热控制系统的潜在关键备件。泵流量控制组件，简称PFCS，是国际空间站光伏热控制系统(PVTCS)的关键组件，它将氨冷却剂从集成设备组件内的各种电气组件传输热量到光伏散热器，在那里散热到太空中。

- 全氟化物概述

## NASA样品筒组件

> 液相烧结钨合金-图片来源:圣地亚哥州立大学

MSL sca - geds - germany实验将在ESA哥伦布模块上的材料科学实验室进行，以研究烧结过程的潜在机制及其制造硬化材料的能力。这项物理科学实验是由圣地亚哥州立大学的首席研究员兰德尔·German进行的。

烧结是通过加热或压力压实并形成固体材料的过程，通常目的是制造硬化材料。虽然液相烧结是一种用于制造网状复合材料的方法，但通常情况下，烧结不会将材料熔化到液化的程度。

尽管烧结已经使用了几个世纪，并且是当今许多工业分支的关键元素，但人们对其基本的科学原理知之甚少。本实验的目标是确定在地球重力和微重力条件下驱动液相烧结体的密度、大小、形状和性能的机制，特别关注材料中扭曲的原因，这种扭曲似乎是由重力的存在所缓解的。

> 材料科学实验室-图片:欧洲航天局

在低温烧结过程中，粉末可以通过颗粒间结合获得强度——通常是通过固态表面扩散，然后在高温下由粉末材料致密化驱动进一步增强。然而，由于变形现象，在高温下发生的二次过程会导致材料软化。虽然有利于网状复合材料的形成，但如果只要求致密化则适得其反。研究出各种过程之间的相互作用，可以开发出一种可以在不变形的情况下实现致密化的烧结方法。

先前的研究显示了令人惊讶的结果，因为重力可能在减少扭曲方面发挥了有益的作用——与此相反，在许多物理过程中，重力被确定为一种干扰力。迄今为止进行的微重力液相烧结实验表明，微重力液相烧结的致密程度较低，变形较高，这就提出了充分了解重力作用的潜在机制，以制定最小化变形的途径。

研究发现，当液相形成时，通过液体内的固体迁移、毛细力和液体润滑可以加速致密化。然而，只有在烧结体中存在牢固的粘结或开放的孔隙时，这种方法才能在一定程度上起作用。如果液化的程度超过固体材料，就会产生实质性的弱点。

科学家们把这个过程比作建造一个沙堡，当一个人只有干燥的沙子，没有力量，没有能力保持形状，或者沙子被水饱和时，这个过程就会很糟糕。它与中间混合物的效果最好，在中间混合物中，液体将沙粒相互接触，并提供最大的强度。


> 图片:ESA

MSL的SCA-GEDS-German实验观察了在失重情况下发生自发反应的固体混合物中的相变和产物形成，以找出导致性能较低、无法消除孔隙和较高失真的原因。含有不同样品材料的弹药盒将由Dragon SpX-14交付给ISS，并在材料科学实验室(MSL)低梯度炉(LGF)内进行全面处理，然后返回地球进行样品分析，并添加温度分布和来自炉的其他传感器数据。

从这个实验中获得的知识将有利于地球上的烧结过程，因为关于扭曲的潜在机制的信息将导致更好的协议，以增加致密化。对于未来的宇航员来说，在零重力或减重力环境下烧结的知识至关重要，因为基于粉末自由形状制造的地外修复和建造将是月球、火星和其他地方定居的关键因素。

## 纳米架显微镜设备

> 照片:NanoRacks

NanoRacks显微镜设备拥有三台商用现成(COTS)显微镜，用于国际空间站，在简化的架构中使用即插即用的USB接口研究原位样品，使显微镜可以从任何笔记本电脑上使用。NanoRacks显微镜非常适合用于检查酵母和霉菌、培养物、植物和动物部位、纤维、细菌等的标本玻片。

三款显微镜中的第一款提供了5倍、10倍和20倍的放大镜，显微镜-2具有相同的视频头，但增加了照明系统，并提供20倍至40倍和200倍的数字功率放大，以及一个20倍的目镜，用于4倍至8倍和50倍的功率观看。显微镜-3是一个手持显微镜500万像素成像仪，可调偏振设置适当的光水平和减少眩光，8个led样品照明，和10倍至240倍放大倍率。

## 多用途可变g平台

> 图片:Techshot

多用途可变重力平台(MVG)是Techshot公司的产品，通过增加一个新的能够产生微重力和2g人工重力之间任何东西的离心机设施，扩大了空间站的科学储备。这将为大量研究提供可能性，包括对国际空间站上发现的新环境进行商业开发。MVG适用于许多样本类型，包括果蝇、扁形虫、植物、鱼、细胞、蛋白质晶体和许多其他。

MVP是一个商业开发、制造、拥有和运营的平台，提供一对39厘米长的旋转木马，每个旋转木马上有六个实验模块，可产生高达2G的人工重力。该设施的设计允许轻松交换样品模块，允许以很少的工作人员时间要求完成大量实验。实时视频和静态图像，包括显微镜，可以根据每个实验的具体需求为每个样品模块提供，该设施提供额外的环境控制，温度范围为14至40°C，湿度在50%至80%之间，同时提供氧气和二氧化碳的数据记录。

## 素食的池塘

> 蔬菜设施-图片:美国宇航局

Veggie PONDS利用国际空间站上现有的蔬菜植物生长硬件，以及通过初始实验运行获得的知识，开发了一种被动营养输送系统，可以为未来长期太空任务中可靠的植物生产设施奠定基础。pond，即被动轨道营养输送系统，解决了最初Veggie运行中使用的标准植物枕系统的一些不足之处，还将通过支持更大的叶蔬菜和水果作物(如作为Veggie -05实验的一部分种植的西红柿)来扩大设施的范围。

池塘系统旨在减轻微重力对水分分配的影响，增加氧气交换，并为根区生长提供足够的空间。最初的池塘实验的目标是验证改善水和养分输送是否能使植物生长更加均匀并提高作物产量。

> 生菜植物生长在蔬菜设施-图片:美国宇航局

该实验将使用“Outredgeous”红长叶莴苣和水芥菜，前者已用于研究veg01至veg03，并将用于植物枕和池塘之间的比较。另一个方面是研究在最初的蔬菜种植过程中种植的植物和那些提供了池塘技术的植物的微生物负荷。

pond架构在肯尼迪航天中心设计，由TechShot和商业合作伙伴特百惠(Tupperware)贡献。pond保留了煅烧粘土类型的偏角岩作为生长介质，这是通过早期Veggie运行的比较研究选择的，pond继续作为一次性硬件，仅用于一次生长运行以管理微生物污染。与之前的操作一样，如果植物生长允许，pond将使用“切割-再来”收割技术，计划收割4次或更多。

宇航员将被允许食用任何不需要在地面上进行分析的生菜。


> 混合作物在蔬菜-图片:美国宇航局

“蔬菜01号”于2014年运行，并以返回的水样和根垫、在轨道上获得的生长过程图像以及带回地球的植物样本的形式提供了有价值的数据。这些数据帮助研究人员评估了两种不同的生长介质在不同大小颗粒中的水和根分布情况，以便为未来的蔬菜任务选择介质。

尽管蔬菜-01实验总体上是成功的，但关于植物枕和水输送系统的一些缺陷被发现，导致对枕和浇水程序进行了修改，这将由蔬菜-03进行测试，同时引入了具有不同水需求的不同作物。2015年，空间站的工作人员第一次品尝到了从蔬菜植物生长单元收获的自制生菜。2015年的另一项研究为宇航员斯科特·凯利(Scott Kelly)收获国际空间站上种植的第一批花朵时，给宇航员们带来了一丝色彩。


> 太空种植的百日菊在蔬菜设施-图片:美国宇航局

Veg-03 A-C测试了不同的作物收获技术，表明重复收割可以使同一套起始材料的收获量翻一番;Veg-03 B和C测试了一种新作物，Tokyo bekana。蔬菜-03 D-F实验研究了不同绿叶蔬菜的混合生长和不同的收获时间表。

“蔬菜”实验设施提供照明和营养供应，并能够支持各种植物物种，这些植物可以用于教育推广、新鲜食品，甚至为长期执行任务的宇航员提供娱乐。热控制由国际空间站舱内系统提供，二氧化碳来源是国际空间站上的环境空气。

在Veg-03设施中生长的植物将被观察，以确定植物如何感知重力以及它们如何对微重力做出反应。作为探路者，作为VEGGIE计划的一部分种植的植物将被收割和研究，然后供宇航员在轨道上食用。VEGGIE设施是国际空间站上可供植物生长的最大体积，这将允许研究在以前的实验中无法生长的更大的植物。


> 照片:ORBITEC
Veggie使用了一个植物生长室，使用种植枕和LED库来提供照明。枕头种植概念的地面测试导致了生长介质和肥料、植物种类、材料和方案的选择。该设施重7.2公斤，尺寸为53 × 40厘米，最大生长高度为45厘米。根垫的生长面积为0.16平方米，有一个2升的流体库。

该系统的峰值功率为115W，其LED组可以支持可调节的波长、光照水平和昼夜周期，以满足植物的生物需求。透明的聚四氟乙烯盖可以看到植物。这些植物将定期被拍照，以评估植物的生长速度和健康状况。当与地面对照进行比较时，组织样本将提供关于可能的生长异常的信息。环境数据将由测量温度、湿度和二氧化碳分压的数据记录器提供。

与VEGGIE合作进行的首次研究还将提供植物和枕头的微生物样本，以评估微生物污染水平，并在必要时实施纠正措施。对于大多数物种来说，微生物污染水平将在一定范围内，不会对船员构成威胁。其他天然具有较高微生物水平的物种可能需要一种卫生方法，必须作为实验的一部分进行开发和测试。在太空中种植植物为宇航员提供了新鲜的食物来补充他们的饮食，同时对士气和健康也有积极的影响。

## NanoRacks模块74 -伤口愈合

> 平板阅读器2 -图片:美国宇航局

伤口愈合实验将评估含有抗菌水凝胶的贴片，该贴片可以促进伤口愈合，同时也可以作为组织再生的支架。这项研究的正式名称是“微重力对抗生素控制释放的影响和新型伤口敷料的愈合机制(微重力条件下水凝胶的形成和药物释放)的研究”。

在微重力环境下进行实验对于正确评估水凝胶在由于没有重力诱导过程而减少流体运动的环境中的行为是必要的。这将允许更精确地分析抗生素剂从交联或混合水凝胶贴片的释放。药物释放真皮敷料和贴片的改进是预防脓毒症的迫切需要;然而，目前还没有一种伤口敷料可以长时间释放抗生素来成功预防败血症——这是久经战斗的士兵面临的一个主要问题。

NanoRacks Module 74的目标是提供评估一系列水凝胶配方所需的数据，并研究药物在失重情况下如何从凝胶中释放，以进一步开发用于高压力环境中的人类的新设备。了解微重力环境下药物释放的机制也有助于开发贴片、植入物或其他设备，以帮助宇航员在前往遥远目标的长途旅行中保持身体健康。

NanoRacks Module 74飞行设备采用NanoRacks反应器微板作为反应容器，并为水凝胶提供交联。使用NanoRacks Plate Reader-2测量凝胶中的药物释放。实验将在未加载水凝胶的情况下进行，以研究其机械和结构特性，而第二组实验则涉及将加载抗生素的水凝胶交联，并将其浸泡在酶溶液中，以模拟细胞降解和抗生素从凝胶中释放。通过光谱法收集的释放剖面将在两周内测量。

## 骨髓

> 国际空间站上的血液取样-图片:NASA

> 图片:美国国家航空航天局

骨髓研究，全名为骨髓脂肪反应:红色还是白色?，研究微重力对人类骨髓的影响。众所周知，长时间暴露在微重力下，就像在地球上长时间卧床休息一样，会对骨髓和血细胞的产生产生负面影响。这种影响的程度和可能的恢复是空间研究和在地球上应用的兴趣所在。

骨髓的造血细胞与脂肪细胞共享骨骼内的有限空间，而脂肪细胞在长时间卧床休息时以造血细胞为代价生长。脂肪细胞的积累可能直接影响血细胞的产生，骨髓也可能解释在零重力下观察到的红细胞和白细胞异常。

骨髓研究旨在测量太空飞行前后骨髓脂肪的变化。此外，这项调查还着眼于在执行任务过程中红细胞和白细胞的具体变化。

MARROW要求机组人员在飞行前和飞行后进行MRT测量，以评估他们的骨髓脂肪，同时通过定期抽血、对呼吸样本进行红细胞功能色谱分析和白细胞基因表达变化，在整个飞行过程中跟踪红细胞和白细胞计数和功能的变化。

骨髓收集的数据有望阐明太空飞行贫血(红细胞功能)、感染易感性(白细胞功能)、太空血栓形成(血小板)的机制，以及地球上活动能力下降或长时间卧床的人的类似情况。

 

## ExoLab和拟南芥

> 照片:magnitude.io

“拟南芥微重力生命周期”实验采用模块化生长室进行微重力研究，研究了芥蓝的形态和生理。植物在自动光照、温度和营养条件下从发芽的种子生长而来，同时摄像机记录下每个阶段的生长情况，以确定植物的生存能力以及培养模块和营养处理的有效性。

该实验是在ExoLab按尺寸设计的实验平台上进行的。io是一种新型的教育工具，它将教室和国际空间站结合在一起，调查微重力对生物的影响，并配有6至8年级的课程材料。“我们与美国各地的学区以及太空科学促进中心合作。IO力求提供符合公认的当地科学标准的非凡的外空生物学体验，同时大幅降低太空实验的成本，”该公司在其项目网站上表示。

该计划包括一个天基实验和一个地面实验，学校可以在从magnitude.io采购的相同ExoLab设施中进行。学生们可以寻找地球上的环境条件和太空环境之间的关系，这些环境条件可能会影响植物的发育和生长。

> 照片:magnitude.io

ExoLab设施由TangoLab设施托管，基于CubeSat规范设计，尺寸为10 x 10 x 22厘米(2个CubeSat单元)，并托管一系列传感器和植物生长室本身。ExoLab上的传感器设备包括光度、温度、二氧化碳和湿度传感器，以及用于记录植物生长的摄像机。该系统通过空间站的WiFi网络传输数据，从轨道下行后可以在线访问数据。

拟南芥被选为实验的模式生物，因为它是一种非常了解的物种，并已在以前的许多空间实验中使用。由于其体积小，生长速度快，可以对多种植物进行研究，因此特别适合进行太空实验。

## 代谢跟踪

代谢跟踪实验的目标是检查微重力对五种不同治疗化合物代谢影响的影响，并研究自体生物发光人体组织培养在不破坏样品培养的情况下进行连续代谢活动跟踪的可行性。该实验的全称是用于改进治疗评估筛选小组的比较实时代谢活动跟踪。

利用空间站提供的独特环境，药物研究经常出现在空间站的科学时间表上。一些研究利用了太空环境以更快的速度诱导衰老和骨质流失的影响，而另一些研究则利用微重力环境在没有重力相关过程引起的掩蔽现象的情况下研究药物传递机制。这使得简化了药物开发过程，对蛋白质晶体结构的研究有助于优化针对特定蛋白质的药物，以减少副作用。

代谢追踪研究评估了一种合成荧光素酶基因，该基因可以通过发光(使用类似于萤火虫或发光水母的过程)作为代谢活动的实时示踪剂。在实验中，改良的人类胚胎肾细胞将被监测，以感知发出的光，跟踪五种不同化合物的代谢影响，并说明监测技术的总体有用性。荧光素酶跟踪方法的一个主要优点是，它允许在不侵入性地影响样品细胞培养的情况下连续地洞察代谢率。

本实验使用自体发光的HEK293细胞和NanoRacks平板阅读器来监测不同样品(使用五种不同的药剂和不同的浓度)的自体发光输出，持续24小时。使用相同样本的第二个实验将在37°C的SABL培养箱中进行，并提高二氧化碳浓度，以实现代谢活性的最大改变。

## 微生物追踪2

> 图片:美国国家航空航天局

“微生物跟踪2”研究将从国际空间站不同地点采集飞行前、飞行中和飞行后的机组人员样本，以及空气和表面样本，以观察存在哪些细菌、真菌和病毒，以及它们可能对机组人员的健康产生什么影响。这项研究的总体目标是对国际空间站上潜在的致病微生物及其对宇航员健康的影响进行分类和描述。

这次收集将持续至少两次国际空间站增量，让科学家们可以观察可以在太空环境中生存的微生物类型，并研究它们随时间的变化。这些数据对于评估宇航员健康风险很有价值，而且还可以近距离观察微生物在太空中适应的方式，与地球研究中已知的机制进行比较。

MT-2的实验数据将深入到细胞水平，以确定可能发生在太空而不是地球上的反应，这可以为抗生素和抗菌剂的开发提供有用的信息。该实验还验证了组学技术在筛选微生物和识别可能存在的微生物方面的作用。


> 照片:CSA

MT-2旨在更好地了解在封闭环境中感染和疾病对宇航员健康的风险，评估清洁空气供应污染和液体和食物污染的风险，比较正常和极端环境下国际空间站和地球上的微生物群落，确定在空间环境中大量生长的特定微生物，并对微生物对空间环境的适应进行研究。

三名国际空间站工作人员将参与研究，收集发射后6个月和3个月的基线数据(一周内四次采样操作)，L+ 1- 2,2 -4个月和返回后10天的飞行数据以及R+1天、1个月和6个月的飞行后数据。样本包括唾液和口腔拭子，前额、腋下、耳后皱痕、肘前窝、海军区和鼻腔的皮肤拭子。

该实验使用了从国际空间站各个模块收集的空气和表面样本，这些样本在室温下储存，并在采集后两周内返回地球。MT-2的取样方法包括胶带、棉签、接触载玻片、湿巾、明胶空气过滤器和用过的手套。表面采样的8个特定位置和6个空气采样点已被选择-零重力积载架(ZSR)表面和Unity(节点1)内部的餐桌;和谐号(节点2)内的船员宿舍(CQ3)内部港口墙;冲天炉的架空舱口区、废物和卫生舱(WHC)旁边的机架、“宁静”(节点3)内部的先进阻力运动装置(ARED)的脚平台;ZSR表面内莱昂纳多PMM;以及命运号(实验室)内便携式饮水机(PWD)附近的机架前端。

 

## 额外的CASIS国家实验室载荷

 

## 评论

- 来自百威的这项调查是一项旨在评估在微重力环境中生长的大麦种子萌发和大麦幼苗生长的持续研究。微重力环境对样本的形态和遗传影响将在轨道上和返回时进行评估。更好地了解大麦对微重力的反应可以帮助研究人员将这种谷物用于长时间的太空飞行。

- “太空基因”学生研究竞赛由波音公司和miniPCR公司发起，并得到CASIS的支持，邀请学生提出利用国际空间站独特环境的开拓性DNA扩增实验。2017年比赛中有两个实验被选为获胜者，都执行了这项任务。Elizabeth Reizis, 14岁，来自纽约史岱文森高中，她将研究微重力对免疫系统细胞分化的影响。Sophia Chen, 14岁，来自华盛顿湖畔学校，旨在测量宇航员致癌的基因组不稳定性。航天飞行会对人体造成许多变化，包括免疫系统减弱和DNA改变。这些实验的发现可能会让人们更好地理解如何保障宇航员在太空中的健康。

- 更高轨道发射!来自Morehead (KY)的学生参与的这个教育项目是“高轨道发射”的一部分!计划，将检查乳酸菌益生菌在太空环境中的有效性。在免疫系统较弱的人群中，酵母白色念珠菌是导致各种系统性和浅表感染的原因。本研究旨在更全面地了解乳酸菌能够限制念珠菌生长的过程。

- BAM-FX营养液对微重力环境下植物生长的影响Quest研究所的有效载荷包括四个独立的实验。第一项研究将使用生物可利用矿物配方x营养液来研究微重力对植物生长特性的影响。第二个实验将观察种子在微重力条件下暴露于电场后的萌发和耐受性。第三和第四项实验将基于不同的加热和冷却方法来评估微重力环境下的热和湿的行为。

- 美国国家设计挑战赛是一系列学生参与的挑战，年轻的探险家们通过竞争获得向国际空间站国家实验室发送实验的机会。这个获奖的实验来自芝加哥童子军和探险者，重点是使用红外光谱仪来研究β -淀粉样肽，这是阿尔茨海默病扩散的关键因素。

-  用于加速再激活剂设计的人类乙酰胆碱酯酶的中子晶体学研究- 2橡树岭国家实验室与田纳西大学巴特尔分校合作，正在向国际空间站国家实验室发送第二次实验，旨在生产具有医学重要性的酶乙酰胆碱酯酶的高质量晶体。在微重力条件下生长的晶体通常比在地面上生长的晶体更大、更有序。这项研究旨在利用国际空间站上的微重力环境来生产足够大的晶体，以进行中子衍射分析。这样的分析可以对酶的结构、它在人体内的作用以及它如何被神经毒剂束缚而不再起作用提供重要的见解。这项调查的结果可能导致开发新的治疗方法，从而降低因暴露于影响乙酰胆碱酯酶的化学制剂(如过度暴露于农药(包括人类和牲畜)或暴露于潜在的化学战攻击)而导致的发病率和死亡率。

- 该有效载荷来自普林斯顿材料科学技术研究所，旨在测量爱因斯坦理论预测的时间膨胀效应(即移动的时钟比静止的时钟时间移动得更慢)。为了做到这一点，调查将检查以纳秒精度设计的时钟的性能。第二个有效载荷将在国际空间站上检测分离质粒DNA的突变率。DNA样本将在进入太空之前和之后进行测序，飞行样本中的突变数量将与地面控制中的突变数量进行比较。

- “太空探戈”风扇模块这项调查旨在证明“太空探戈”风扇模块能够引导进入设施的空气流向特定的内部元素，以保持它们的凉爽。太空探戈风扇模块的成功演示将增加能够在空间站上进行的调查的数量和类型。

- 2017年威斯康星晶体生长竞赛的获奖学生将在国际空间站国家实验室种植晶体，以测试他们在地球上结晶和微重力结晶的优化条件。学生们利用他们在地球上的结晶方法来准备一个飞行项目，并将在微重力下生长的晶体与在地面上生长的晶体进行比较

# [国际空间站到处是霉菌？宇航员健康受到威胁，中国也要重视](https://new.qq.com/rain/a/20211119A07LWM00)

2021/11/19 16:48 老粥科普

空间站是人类最高科技的结晶，它高高在上，以每秒7.6公里的速度绕地球飞行，在里边工作的是全人类最优秀的宇航员和工程师，他们操作着几千万甚至上亿美元一台的科学设备，从事全世界最顶尖的科学研究。如果现在告诉你，国际空间站里边充斥着各种霉菌，一些地方霉迹很难擦掉，空气里有一种怪怪的霉味，宇航员健康受到威胁，是不是难以置信？

![输入图片说明](https://foruda.gitee.com/images/1676901017573021571/30c094a0_5631341.png "屏幕截图")

> 国际空间站里到处都有霉菌

2018年11月，法国杂志发表了一篇惊悚文章《国际空间站的昆虫偷渡者》，里边这样说：“这是基于美国宇航局的一项研究，该研究表明，在国际空间站上发现了臭虫，就像那些困扰某些法国医院病人生命的臭虫一样......它们在空间站的厕所被发现，引起科学家担心。因为这些臭虫是大肠杆菌的携带者。”

![输入图片说明](https://foruda.gitee.com/images/1676901027650088599/ee56e072_5631341.png "屏幕截图")

> “臭虫”上了法国杂志封面

国际空间站竟然有臭虫？！它们是怎么被带到太空的？要知道即便是在今天普通城市家庭，要想找到臭虫也很不容易了呀！

后来人们发现，谣言只在法国媒体流传，并且他们也不是故意，只是把英文单词“bugs”误译成法语“punaises de lit”（臭虫）。而英语“bug”尽管有虫子、臭虫的意思，却被美国宇航局专指在国际空间站发现的“霉菌”。所以这不能怪法国记者，他们哪知道这一层呀！

![输入图片说明](https://foruda.gitee.com/images/1676901037531707912/f564d909_5631341.png "屏幕截图")

> 一场误会，空间站里并没有臭虫

但国际空间站长霉却是真事儿，2017年宇航员们通过对空间站进行全面调查发现，这里不仅到处都是霉菌和各种细菌，有些地方甚至达到了令人发指的程度。

水是空间站上最宝贵的资源，一般来讲宇航员的内衣只换不洗，因为洗衣服浪费水。但为了防止骨质流失和肌肉萎缩，宇航员在太空里每天都需要锻炼大约2小时，锻炼就意味着大量出汗，每天扔一套运动服显然不合适，于是运动服会用再生水清洗、晾干之后再穿。

就在宇航员们晾衣服的区域，壁板上滋生了大量霉菌。类似情况在空间站洗手间、运动区域以及生活区也有发现，虽然每个星期都搞清洁，却无法清除掉那些令人恶心的菌斑。

![输入图片说明](https://foruda.gitee.com/images/1676901057023413700/98365711_5631341.png "屏幕截图")

> 宇航员晾衣服的地方滋生霉菌

一篇发表在《微生物学杂志》上的研究文章指出，国际空间站空气过滤器和真空吸尘袋里都含有细菌病原体，这些病原体在地球上大多安全，但也可能导致感染、炎症和皮肤刺激。

霉菌是怎么进入空间站的？事实上，只要有人的地方就有真菌和细菌，哪怕你洗得再干净，身体表面和体内也不可避免地携带若干种细菌，这些细菌会在潮湿环境中繁殖，空间站狭小密闭的空间简直就是细菌们的天堂！

![输入图片说明](https://foruda.gitee.com/images/1676901087940921124/3d0f9a11_5631341.gif "NASA-Fungi-000-for-SL.gif")

> 宇航员取样培养细菌

宇航员曾经形容前苏联“和平”号空间站的气味像一个“满是烂苹果的地下室”，因为冷凝器故障，1997年，美国宇航员大卫·沃尔夫打开“和平”号一个很少使用的控制面板，发现电箱里漂浮着一个“排球大小、黏稠、冰冷的凝结物球”，这个肮脏的水球不知存在了多久，里边有几十种细菌和真菌。

不只是空间站，包括美国重复使用的航天飞机、俄罗斯联盟号飞船返回舱里也都发现过大量真菌。这些小东西生命力顽强，它们不仅吃宇航员们掉落的皮屑和食物残渣，腐蚀电线、密封橡胶，还将飞船观察窗的石英玻璃蚀刻出一条条纹路，只要有水它们就能充满活力。

![输入图片说明](https://foruda.gitee.com/images/1676901106090382330/108ca91a_5631341.png "屏幕截图")

> 航天飞机对接“和平”号空间站

### 空间站里的水从哪里来？

实际上除了失重与高辐射环境之外，空间站里的一切与地面并没有多大差别，它内部维持着一个大气压强，为了让宇航员们感觉舒服，空气湿度与地面也差不多。

宇航员平静状态下每小时呼出大约17毫升水，在运动时每小时呼出的空气中将有60~70毫升水。与此同时，人的皮肤也会蒸发汗液，这些水都将汇聚到狭小的太空舱里。

在微重力状态下，空间站里的空气不会对流，需要依靠抽风系统强制流动，否则人会憋死。抽风系统会过滤掉空气中的灰尘、杂质和人掉下来的皮屑，同时通过冷凝装置让空气中的水汽凝结、回收再利用。细菌就在冷凝器附近大量繁殖，越来越多。

![输入图片说明](https://foruda.gitee.com/images/1676901134571485709/3fdeb32e_5631341.png "屏幕截图")

> 空间站冷凝器检出大量细菌

国际空间站空气与水循环系统里有最先进的除菌装置，无奈水的吸附性极强，只要温度合适，小水滴就能在舱壁和角落里聚集，成为真菌繁殖的生物膜。

许多真菌能耐受太空高辐射环境，科学家研究了空间站里最常见的两种真菌，发现它们的孢子能在高达1000戈瑞（Gray）剂量的X射线照射下存活下来，遭受500戈瑞重离子轰击、每平方米3000焦耳的紫外线照射都没问题。而超过0.5戈瑞能让人得辐射病、5戈瑞就足以把人给杀了。

![输入图片说明](https://foruda.gitee.com/images/1676901148178257656/cea52832_5631341.png "屏幕截图")

> 一些细菌生命力极强

空间站里大部分细菌都是良性的，能与人类和平共处，但这不代表所有真菌不会因为长时间宇宙辐射而发生变异，如果它们中的一部分偶然变成超级致病细菌，将对宇航员的健康构成严重威胁！

天宫空间站正处于在轨验证阶段，未来将在太空服役十年甚至更长时间，相信中国科学家和航天员也会重视我们自己空间站里真菌繁殖的问题，切实保护好航天员的身体健康和安全。

---

【[笔记](https://gitee.com/zhangyiyuan_will/siger/issues/I6FWTD#note_16352520_link)】

- [Dragon SpX-14 Cargo Overview](https://spaceflight101.com/dragon-spx14/cargo-overview/)

  ![输入图片说明](https://foruda.gitee.com/images/1676892725561079190/fddd8357_5631341.png "屏幕截图")

- [国际空间站到处是霉菌？宇航员健康受到威胁，中国也要重视](https://new.qq.com/rain/a/20211119A07LWM00)

  ![输入图片说明](https://foruda.gitee.com/images/1676892738029050123/7c28b261_5631341.png "屏幕截图")