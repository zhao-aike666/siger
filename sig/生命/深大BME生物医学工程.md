- [中国科研信息化蓝皮书 2018](https://www.sciping.com/33238.html)（信息技术在医学超声工程领域的应用）
- [功能超声诊疗实验室 Functional Ultrasound Theranostics LAB](http://group.acagrid.com/1458)
- [深圳大学生物医学工程学院](https://space.bilibili.com/644323919/video) Biomedical Engineering [SZUBME](https://bme.szu.edu.cn) [本科招生](https://bme.szu.edu.cn/g/college_admissions/undergraduates/index.html) 【[宣传片](https://mp.weixin.qq.com/s?t=pages/video_detail_new&scene=1&vid=wxv_1441231737589399555&__biz=MzUyOTYzOTc4MQ==&mid=2247487161&idx=1&sn=54cde6a5b0ec2b18d01ccb675dd5bbf3)】
- [生物医学工程学院科研团队近期取得系列新进展](https://mp.weixin.qq.com/s?__biz=MzUyOTYzOTc4MQ==&mid=2247491406&idx=4&sn=f570df468504ad0158e1a412705f1215)
- [“从粤海到丽湖，与深大生物医学工程学科共成长”](https://mp.weixin.qq.com/s?__biz=MzUyOTYzOTc4MQ==&mid=2247488370&idx=3&sn=6c61729b08108d9ecb9dc3065ffa99b6)

<p><img width="706px" src="https://foruda.gitee.com/images/1673493549812653168/28af8e7c_5631341.jpeg" title="505BME副本.jpg"></p>

> 没有健康哪来的快乐！这是 2022 给每一个人的记忆，SIGer 2023 新年的第一个选题就是 DIY 血氧仪，这是家有老人的焦虑。回望志愿初心 “健康快乐” 一直都未改变，这也是选取蓝天为期刊背景，它的清澈，映衬着云朵的笑脸，满屏溢出的是希望和对未来的畅想，健康是快乐之本，希望就是快乐之源啦。对于年轻人，成长成才，规划蓝图，是永恒的主题。我喜欢 BME 宣传片的这个收尾，令人无限畅想。 :+1: 

SIGer 2023 期刊封面会有一个新规，需要替换两张海报，作为当期的出处，作为学习线索的接续展现给同学们。本期的两个缘起分别是 立创EDA，以及《卓越工程师培养系列》，一则深大老师出圈的新闻，促成了本次对 深圳大学 生物医学工程（以下简称 BME）的深度访问，从在线探访开始。下面会附上学习笔记（依学习顺序呈现），之后是 BME 专访。

- 陈思平 , 陈昕 , 沈圆圆 ,等 .中国科研信息化蓝皮书（信息技术在医学超声工程领域的应用）[M]. 北京: 电子工业出版社, 2018
- 陈思平 , 陈昕 , 沈圆圆 ,等 .China's e-Science Blue Book 2018 (Application of Information Technology in Medical Ultrasound Engineering)[M]. 北京: Springer Nature, 2019

### 中国科研信息化蓝皮书（信息技术在医学超声工程领域的应用）

> China's e-Science Blue Book 2018 (Application of Information Technology in Medical Ultrasound Engineering)

[《中国科研信息化蓝皮书》2018全球发布](https://www.sciping.com/33238.html)（2019年12月）
  
2019年12月7日 18:08:06 2,425 views 6401字 阅读 21分20秒

![输入图片说明](https://foruda.gitee.com/images/1673370234495311821/d3ddcdee_5631341.png "屏幕截图")

> 电子工业出版社总编辑、副社长刘九如主持发布仪式

![输入图片说明](https://foruda.gitee.com/images/1673370267000245339/222b814f_5631341.png "屏幕截图")

> 施普林格·自然出版集团副总裁Dieter Merkle通过视频充分肯定《蓝皮书》英文版的出版价值

![输入图片说明](https://foruda.gitee.com/images/1673370279426407350/36e66a62_5631341.png "屏幕截图")

> 联合编纂单位代表出席新书发布仪式

 **第一章 科技前沿研究中的应用** 

1. 《互联网发展态势与展望》清华大学计算机系主任吴建平
2. 《“天眼”等天文学重大科技基础设施的信息化需求与建设现状》中国科学院国家天文台信息与计算中心主任崔辰州
3. 《空间科学卫星计划的科研信息化应用》中国科学院国家空间科学中心副主任邹自明

   4个科学卫星：

   - 空间科学先导专项（一期）成功发射的暗物质粒子探测卫星（“悟空”）
   - “实践十号”返回式科学实验卫星（SJ-10）
   - 量子科学实验卫星（“墨子”，如下图所示）
   - 硬X射线调制望远镜卫星（“慧眼”）

4. 《南极巡天望远镜AST3在引力波探测中的应用》中国科学院紫金山天文台南极天文研究中心主任王力帆

 **第二章 国家重大项目中的科研信息化进展** 

1. 《“蛟龙”号载人潜水器声学系统的现状及展望》中国科学院声学研究所海洋声学技术中心主任朱敏
2. 《北斗卫星时间系统的建设与应用》中国科学院国家授时中心时频基准实验室主任、首席研究员董绍武
3. 《国家科技管理信息系统建设与应用》科学技术部信息中心常务副主任胡少华
4. 《信息化助力国家自然科学基金，实现精准管理与开放共享》国家自然科学基金委委员会信息中心副主任李东
5. 《“国家哲学社会科学文献中心”建设与展望》中国社科院图书馆馆长王岚

 **第三章 交叉学科的科研信息化应用成果** 

1. 《大数据支撑的临床肿瘤研究》中山大学肿瘤防治中心院长、中国抗癌协会肿瘤靶向治疗专业委员会首任主任徐瑞华

2. 《信息技术在医学超声工程领域的应用》深圳大学教授、医学超声关键技术国家地方联合工程实验室主任陈思平

   医学超声成像已经成为现代临床医学应用最广泛诊断手段之一。每一次超声回波隐含信息的发现都促进了医学超声成像技术突破，其发展史就是一部不断发现隐含信息的历史，医学超声从最初一维信息的A超扩展到二维图像信息的B超以及结合血流运动信息的彩超，正在向早期诊断、精准医疗方向发展。通过超声、电磁与弹性及生化等多元信息协同互补，同时获得了人体组织在解剖结构、力学特性和电学特性上的重要的相关信息，这些多元信息发现对疾病的早期诊断具有重要价值，并已取得较为显著的医疗效果（如下图所示）。

   ![输入图片说明](https://foruda.gitee.com/images/1673370765326551155/b4942d48_5631341.png "屏幕截图")

   > 双模成像实验平台

3. 《农业经济空间信息服务关键技术与应用平台（中国农业经济电子地图）》中国农业科学院农业信息研究所智能农业技术研究室副主任刘升平

4. 《面向先进制造业的标识服务技术发展与展望》中国科学院计算机网络信息中心物联网信息技术与应用实验室常务副主任田野

# [功能超声诊疗实验室 Functional Ultrasound Theranostics LAB](http://group.acagrid.com/1458)

<img align="right" width="199px" src="https://foruda.gitee.com/images/1673371089868659306/d05cbd26_5631341.png">

- [团队介绍](http://group.acagrid.com/1458/team)
- [研究方向](http://group.acagrid.com/1458/research_field)
- [研究进展](http://group.acagrid.com/1458/publications)
- [人才招募](http://group.acagrid.com/1458/recruit)
- [联系我们](http://group.acagrid.com/1458/contact_us)
- [新闻动态](http://group.acagrid.com/1458/news)
- [团队生活](http://group.acagrid.com/1458/team_moments)
- [通知公示](http://group.acagrid.com/1458/announcements)
- [学术交流](http://group.acagrid.com/1458/conference)

 **课题组介绍** 

> 功能超声诊疗实验室隶属于深圳大学生物医学工程学院及医学超声关键技术国家地方联合工程实验室，课题组主要围绕超声多模态功能成像、超声信号处理与智能诊断、超声介导微泡治疗等方面开展科学研究工作。课题组近年来已承担国家自然科学基金项目、青年基金项目、国家科技部科技支撑计划、省市级科技项目、博士后科学基金等项目多项，依托项目发表SCI论文100余篇，获得省市科技进步奖2项，制定国家行业标准2项。 诚挚欢迎国内外科研工作者前来交流、学习，并热烈欢迎国内外同学加入本实验室。

 **组员风采** 16

<img align="right" width="199px" src="https://foruda.gitee.com/images/1673371363155339110/cf7a41f2_5631341.png">

课题组长

- [陈昕](http://scholar.acagrid.com/56247) 教授   博士生导师  @[深圳大学](https://bme.szu.edu.cn/20161/0301/43.html)

教师

- [沈圆圆](http://scholar.acagrid.com/49886) 副教授 硕士生导师  @[深圳大学](https://bme.szu.edu.cn/20161/0301/41.html)
- [林浩铭](http://scholar.acagrid.com/50114) 助理教授 硕士生导师 @[深圳大学](https://bme.szu.edu.cn/20181/1203/74.html)
- [陈冕](http://scholar.acagrid.com/48860) 助理教授  @[深圳大学](https://bme.szu.edu.cn/20201/0917/88.html)

研究员

- [刘忠](http://scholar.acagrid.com/137773) 副研究员 @[深圳大学](https://bme.szu.edu.cn/20221/1024/97.html)

在读博士

- [黄斌](http://scholar.acagrid.com/144066)
- [皮兆柯](http://scholar.acagrid.com/135196)
- [孙通](http://scholar.acagrid.com/143742)

已毕业

- [郝鹏慧](http://scholar.acagrid.com/138210)
- [刘强](http://scholar.acagrid.com/138211)
- [谢辰熙](http://scholar.acagrid.com/138212)
- [曾鑫](http://scholar.acagrid.com/138213)
- [齐亭亭](http://scholar.acagrid.com/138215)
- [王梦珂](http://scholar.acagrid.com/138216)
- [陈依玲](http://scholar.acagrid.com/138221)
- [华羚辰](http://scholar.acagrid.com/118576)

 **代表成果 Representative Achievements** 

- Optimal linear combination of ARFI, transient elastography, and APRI for the assessment of fibrosis in chronic hepatitis B

  > Chen Xin;Liu Yingxia;Dong Changfeng;Yang Guilin;Jing Liu;Simin Yao;Hanying Li;Jing Yuan;Shaxi Li;Xiaohua Le;Yimin Lin;Wen Zeng;Lin Haoming;Zhang Xinyu

  2015-03-01

- Automatic Muscle Fiber Orientation Tracking in Ultrasound Images Using a New Adaptive Fading Bayesian Kalman Smoother

  > Zhong Liu;Shing Chow Chan;Shuai Zhang;et al

  2019-03-23

- Sonodynamic Therapy on Intracranial Glioblastoma Xenografts Using Sinoporphyrin Sodium Delivered by Ultrasound with Microbubbles

  > Xin Chen;Zhaoke Pi;Huang, Yongpeng;Yuanyuan Shen;Zeng, Xiaojun;Yaxin Hu;Chen, Tie;Li, Chenyang;Yu, Hao;Siping Chen

  2019-02-01

## 研究方向 Research Field

### （一）功能超声成像

传统超声成像主要是对组织形态或者血流成像，临床需求推动超声成像技术的发展，各种功能性的超声成像方法不断出现。本实验室重点研究两种功能超声成像方法：

(1) 超声弹性成像技术现已广泛应用在临床肝脏、乳腺、前列腺等部位的疾病检测。但是该技术还处于不断发展过程中，各种新方法、新应用不断涌现。本项目团队在国家自然科学基金重点项目（61031003）的支持下，对弹性成像关键技术进行深入研究，从中挖掘更多信息。

![输入图片说明](https://foruda.gitee.com/images/1673372216531686281/87a851e9_5631341.jpeg "3229399a7c008e325bd7f1439cb98dac.jpeg")

(2) 本项目团队在国家自然科学基金重大仪器项目（61427806）、自然科学基金面上项目（81871429）的支持，对超声-电磁多模态成像方法进行研究。这是同时结合组织的声学特性、力学特性和电磁特性的新型多模态功能性成像方法和设备，具有优势互补、无创、定量、低成本等特点。

![输入图片说明](https://foruda.gitee.com/images/1673372314134032736/ba485eed_5631341.jpeg "a3b8e57dafa91a19a510d34ae598f04f.jpg")

### （二）超声联合微泡治疗

超声造影剂微泡是一类尺度在微米级的包膜微气泡。当超声波的频率与微泡固有频率一致时则发生共振现象，从而提高超声图像的灵敏度和对比度。而在治疗领域，微泡在超声场中产生空化效应，引发产生一系列生物效应，从而可以进行药物或基因的递送以及脑部刺激调控等。

当血液循环中存在微泡时，超声可以极低的能量开放血脑屏障，将药物输送入脑，具有无创、可逆、安全的独特优势。而微泡本身也是药物和基因的优良载体，因此在脑肿瘤治疗领域得到了关注。除此之外，研究者们发现超声联合微泡可以清楚阿尔茨海默症小鼠脑内的淀粉样蛋白的沉积，显著改善动物的记忆能力。与药物结合，该技术还可以改善帕金森症动物的症状。

![输入图片说明](https://foruda.gitee.com/images/1673372381991004289/d708cc28_5631341.jpeg "e949cf8db3250088afce81a7afcdc1f4.jpg")

### （三）超声信号处理与智能诊断

临床超声诊断的主要方法是由超声医师通过阅读超声图像进行人分析。这种方法不仅费时费力，而且易受医师个人经验等主观因素影响。近年来，人工智能技术被广泛应用于超声图像处理与分析，可有效弥补人工诊断方法的不足。目前大部分人工智能技术的发展均基于超声图像，但超声图像所含组织信息有限，而且不同超声设备产生的超声图像数据风格迥异，因此已发展的人工智能技术普适性较差。相比超声图像，超声原始射频信号包含丰富的组织信息，同时不受同超声设备成像风格影响。课题组立足于超声原始射频信号，采用信号处理与人工智能技术发展新型超声智能诊断系统。

1. **融合超声射频数据的智能诊断系统** 

   ![输入图片说明](https://foruda.gitee.com/images/1673372423328483061/91bd9491_5631341.png "屏幕截图")

2. **基于定量超声的智能诊断系统** 

   ![输入图片说明](https://foruda.gitee.com/images/1673372443792779665/023bfb33_5631341.png "屏幕截图")

# [深大生物医学工程学院](https://space.bilibili.com/644323919/video) B站

![输入图片说明](https://foruda.gitee.com/images/1673411622795235987/51a4c34e_5631341.png "屏幕截图")

| img | title | views | 弹幕 | date |
|---|---|---|---|---|
| <img width="199px" src="https://foruda.gitee.com/images/1673410925187351554/a1d4f17a_5631341.png"> | 1. [深圳大学首届生物医学工程学科与产业融合发展高端论坛](//www.bilibili.com/video/BV1sr4y12799/) | 172 | 0 | 2021-10-15 |
| <img width="200px" src="https://foruda.gitee.com/images/1673410941341159916/78fc230a_5631341.png"> | 2. [「 这样的我们，你会记得吗？」 深圳大学生物医学工程学院2021届毕业生如是说](//www.bilibili.com/video/BV1fq4y1Z7Jh/) | 860 | 0 | 2021-9-13 |
| <img width="201px" src="https://foruda.gitee.com/images/1673410959319000208/075cc48f_5631341.png"> | 3. [欢迎报考深圳大学生物医学工程学院，来深大吃荔枝噢~](//www.bilibili.com/video/BV19v411H7Vs/) | 467 | 0 | 2021-6-29 |
| <img width="202px" src="https://foruda.gitee.com/images/1673410975518726526/4f0dbe9d_5631341.png"> | 4. [深圳大学生物医学工程学院招生宣讲](//www.bilibili.com/video/BV1hK4y1g7KZ/) | 731 | 0 | 2021-6-28 |
| <img width="203px" src="https://foruda.gitee.com/images/1673410997059971738/dbed3364_5631341.png"> | 5. [我们毕业啦之答辩季](//www.bilibili.com/video/BV1zf4y1t7ws/) | 156 | 0 | 2021-6-28 |
| <img width="204px" src="https://foruda.gitee.com/images/1673411010122834281/0b608b6f_5631341.png"> | 6. [一起来看看今年的毕业礼物~~](//www.bilibili.com/video/BV1Kg41137Wv/) | 96 | 0 | 2021-6-24 |
| <img width="205px" src="https://foruda.gitee.com/images/1673411030954293906/a3112223_5631341.png"> | 7. [深圳大学生物医学工程专业宣讲](//www.bilibili.com/video/BV13q4y1s73E/) | 315 | 0 | 2021-6-23 |
| <img width="206px" src="https://foruda.gitee.com/images/1673411042606604920/a069d7b3_5631341.png"> | 8. [我的那些校园时光](//www.bilibili.com/video/BV1i64y1r7sW/) | 51 | 0 | 2021-6-18 |
| <img width="207px" src="https://foruda.gitee.com/images/1673411056565027564/f66bd0b4_5631341.png"> | 9. [我们毕业啦](//www.bilibili.com/video/BV1ew411f74s/) | 62 | 0 | 2021-6-18 |
| <img width="208px" src="https://foruda.gitee.com/images/1673411071558846772/5e0845bd_5631341.png"> | 10. [深圳大学生物医学工程学院招生宣传片](//www.bilibili.com/video/BV17z4y1Q7ks/) | 1794 | 3 | 2020-7-23 |

1. 2021年10月10日，深圳大学发起并主办的“首届生物医学工程学科与产业融合发展高端论坛”在深圳博林圣海伦酒店顺利召开。全国专家学者及行业大咖近60人出席会议并发表精彩讲话。现场参会人数达200人之多。线上直播最高时接近7000人收看。

   <p><img title="1" height="99px" src="https://foruda.gitee.com/images/1673433983354568105/26a7955e_5631341.png"> <img title="2" height="99px" src="https://foruda.gitee.com/images/1673433974400689949/94fd1d0e_5631341.png"> <img title="3" height="99px" src="https://foruda.gitee.com/images/1673433969344569147/0a30b9cf_5631341.png"> <img title="4" height="99px" src="https://foruda.gitee.com/images/1673433966096157974/0bd08545_5631341.png"> <img title="5" height="99px" src="https://foruda.gitee.com/images/1673433961166613734/7880ced1_5631341.png"> <img title="6" height="99px" src="https://foruda.gitee.com/images/1673433958143868012/b0ed2e04_5631341.png"> <img title="7" height="99px" src="https://foruda.gitee.com/images/1673433945537058891/75cea0f8_5631341.png"> <img title="8" height="99px" src="https://foruda.gitee.com/images/1673433932828596176/787c3311_5631341.png"> <img title="9" height="99px" src="https://foruda.gitee.com/images/1673433926898493442/999ab00c_5631341.png"> <img title="10" height="99px" src="https://foruda.gitee.com/images/1673433922180590854/e5aa6753_5631341.png"> <img title="11" height="99px" src="https://foruda.gitee.com/images/1673433917181542291/a96eeb35_5631341.png"> <img title="12" height="99px" src="https://foruda.gitee.com/images/1673433914365953732/7e43460f_5631341.png"> <img title="13" height="99px" src="https://foruda.gitee.com/images/1673433900476590395/b1778cd5_5631341.png"> <img title="14" height="99px" src="https://foruda.gitee.com/images/1673433893686610873/b31fe6e9_5631341.png"> <img title="15" height="99px" src="https://foruda.gitee.com/images/1673433887891748884/53dbc979_5631341.png"> <img title="16" height="99px" src="https://foruda.gitee.com/images/1673433885829685954/827f0db2_5631341.png"> <img title="17" height="99px" src="https://foruda.gitee.com/images/1673433880726621125/51e12008_5631341.png"> <img title="18" height="99px" src="https://foruda.gitee.com/images/1673433876661643266/cc8b42ef_5631341.png"> <img title="19" height="99px" src="https://foruda.gitee.com/images/1673433861426826241/e06851b3_5631341.png"> <img title="20" height="99px" src="https://foruda.gitee.com/images/1673433850542970724/acf38dfc_5631341.png"> <img title="21" height="99px" src="https://foruda.gitee.com/images/1673433846054334695/f5534dc9_5631341.png"> <img title="22" height="99px" src="https://foruda.gitee.com/images/1673433834288815065/d0bac6e0_5631341.png"> <img title="23" height="99px" src="https://foruda.gitee.com/images/1673433823532928520/56363419_5631341.png"> <img title="24" height="99px" src="https://foruda.gitee.com/images/1673433810358341698/a2facd94_5631341.png"> <img title="25" height="99px" src="https://foruda.gitee.com/images/1673433799752419237/b4e15b3f_5631341.png"> <img title="26" height="99px" src="https://foruda.gitee.com/images/1673433785284685047/249818ea_5631341.png"> <img title="27" height="99px" src="https://foruda.gitee.com/images/1673441915809584599/dfdfbc27_5631341.png"> <img title="28" height="99px" src="https://foruda.gitee.com/images/1673433759088622821/c964cb8c_5631341.png"> <img title="29" height="99px" src="https://foruda.gitee.com/images/1673433746116069443/0c1f2783_5631341.png"> <img title="30" height="99px" src="https://foruda.gitee.com/images/1673433732336596384/e4dfc48a_5631341.png"> <img title="31" height="99px" src="https://foruda.gitee.com/images/1673433713652325543/192fc790_5631341.png"> <img title="32" height="99px" src="https://foruda.gitee.com/images/1673433701217056495/59c24a8a_5631341.png"> <img title="33" height="99px" src="https://foruda.gitee.com/images/1673433693616845417/1be246ed_5631341.png"> <img title="34" height="99px" src="https://foruda.gitee.com/images/1673433682598642934/37b9d82e_5631341.png"> <img title="35" height="99px" src="https://foruda.gitee.com/images/1673433677069364367/7d828536_5631341.png"> <img title="36" height="99px" src="https://foruda.gitee.com/images/1673433666102576787/7ea2f1ab_5631341.png"> <img title="37" height="99px" src="https://foruda.gitee.com/images/1673433655063223925/642e732c_5631341.png"> <img title="38" height="99px" src="https://foruda.gitee.com/images/1673433612332352894/45b74580_5631341.png"> <img title="39" height="99px" src="https://foruda.gitee.com/images/1673433638045683941/3e7e0fa8_5631341.png"></p>

   - 首届生物医学工程学科与产业融合发展 高端论坛 （主办单位：深圳大学）2021年10月9日-10日，中国 · 深圳
   - 开幕式致辞：
     - 生物医学工程学院名誉院长 陈思平教授
     - 深圳大学副校长 汪永成教授 致辞
     - 深圳大学医学部主任 朱卫国教授 致辞
   - 创新不足：
     - 医工交叉融合前沿引领性研究不足
     - 高端医疗器械材料，部件依赖进口
     - 仅一个国家重点实验室（生物电子学）
     - 国产自主高端医疗器械 < 5%
   - BME 工程教育认证与相应工作重点 （万遂人，东南大学 CSBME）
   - 人体器官芯片 （顾忠泽 生物电子学国家重点实验室）
     - 东南大学生物科学与医学工程学院
     - 东南大学苏州医疗器械研究院
   - 分子医学影像与（任秋实，北京大学信息与工程学部副主任，长江学者特聘教授，）
     - [任秋实](https://baike.baidu.com/item/任秋实/10148500)，男，教授，博士生导师。现任北京大学信息学部副主任，北京大学未来技术学院生物医学工程系教授，教育部长江学者特聘教授，国家杰出青年基金获得者，美国生物与医学工程学会 …
   - 三、微肿瘤芯片的初步结果（二、类器官研究进展和挑战）
   - 刘谦、曾柱、建华、[张会生](https://bme.szu.edu.cn/20161/0324/53.html)
   - 奋进中的浙江大学生物医学工程学科（张宏，生物医学工程与仪器科学学院）
   - 方舱医院（2019/12/27 中西医结合医院，报疑似病例，2020/2/8 中南医院接管雷神山医院）
   - 全栈式 智能医学超声研究与转化（[倪东](https://bme.szu.edu.cn/20161/0326/55.html)，超声图像计算（MUSIC）实验室，深圳大学医学部生物医学工程学院）
   - 慢性病闭环管理与控制（数字医学技术的探索与实践）段会龙（浙江大学生物医学工程与仪器科学学院）
   - 张建保，蒋兴宇，冯前进，徐桂芝
   - 健康中国2030 “背景下的新需求” —— 大健康将覆盖：预防、诊疗、康养全周期，现有的医疗资源将承受更大的压力
     - 全民对健康的渴求与我国目前老龄化日益严重，医疗资源稀缺且分布不均衡，医疗服务体系效率低下
   - 体外诊断的前景辽阔
     - 定义：IVD（In Vitro Diagnosis），是指在人体之外，通过对人体样本（血液，等）进行检测而获取临床诊断信息，进而判断疾病或机体功能的产品化服务。
   - 题目：《国家重点研发计划项目申报及产学融合研讨会》主持人：[陈思平](https://bme.szu.edu.cn/20161/0301/38.html)
     - 301医院（王卫东）
     - 清华大学（李秀清）
     - 中国科学院苏州生物医学工程技术研究所（武晓东）
     - 南昌大学（王共先）
     - 中日友好医院（马国林）
     - 中国医疗器械行业协会临床试验分会（孙京昇）
   - 题目：《人才培养与产业发展》主持人 航空航天大学 李
     - 桂林电子科技大学（陈真诚）
     - 大连医科大学（张维升）
     - 中国科学技术大学（邱本胜）
     - 华南理工大学（王均）
     - 空军军医大学（付峰）
     - 华中科技大学（邓勇）
     - 深圳市精密仪器股份有限公司（谢锡城）
     - 首都医科大学（张）
   - 模式一：产学研亲自上阵
     - 《我，做VC，到大学抢教授》VC争抢教授并非偶然，背后折射了时代的缩影——硬科技创业正迎来黄金时代
     - 知名教授创业案例（不完全统计）

2. 不忧愁的脸是我们的少年，不仓惶的眼等岁月改变，当离别的钟声敲响，情不自禁回忆那来时的路，有傻笑，有发呆，有实验成功的喜悦，有难忘的“烧烤大道”，有排球场上的激情拼杀……恍然发现，相册中记载的便是一段青春。

   既已毕业，或就业，或读研，或直博生，或出国深造，或去更远的地方，而我们还是我们。

   从战而降的青春时光，我们仍在为目标而奔跑。

   这样的我们，你会记得吗？

   - 这条路就是堪比火焰山得烧烤大道，这是我们每天去上课得必经之路，真的要烤熟了。
   - 这是我四年住过的地方，但是二楼有一个神圣之地，从早到晚都有一群考研战士，摩拳擦掌，青春时光从战而将。

   <p><img title="1" height="99px" src="https://foruda.gitee.com/images/1673445116376840468/eea7f1c0_5631341.png"> <img title="2" height="99px" src="https://foruda.gitee.com/images/1673445108028983144/aaefeb81_5631341.png"> <img title="3" height="99px" src="https://foruda.gitee.com/images/1673445101657020624/753394ed_5631341.png"> <img title="4" height="99px" src="https://foruda.gitee.com/images/1673445093093302212/e29c1661_5631341.png"></p>

   - 毕业有什么感受？毕业后有什么打算？直博，工作，深造...

3. 世间珍果更无加，深大荔枝你来尝?

   <p><img height="99px" src="https://foruda.gitee.com/images/1673445398277947354/0e167107_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1673445411852526077/60a83b64_5631341.png"></p>

4. 错过直播的同学敬请留意~ 有些专业不了解的时候，觉得一般；深入了解后，真香！——此言指的就是深圳大学生物医学工程

   <p><img title="1" height="99px" src="https://foruda.gitee.com/images/1673449179174250304/09902768_5631341.png"> <img title="2" height="99px" src="https://foruda.gitee.com/images/1673448877962688164/524be3ab_5631341.png"> <img title="3" height="99px" src="https://foruda.gitee.com/images/1673448865766084582/c323d1a9_5631341.png"> <img title="4" height="99px" src="https://foruda.gitee.com/images/1673448852797344755/916b79b2_5631341.png"> <img title="5" height="99px" src="https://foruda.gitee.com/images/1673448839888520241/82dde43f_5631341.png"> <img title="6" height="99px" src="https://foruda.gitee.com/images/1673448825895022367/4a314458_5631341.png"> <img title="7" height="99px" src="https://foruda.gitee.com/images/1673448810028202910/e538c732_5631341.png"> <img title="8" height="99px" src="https://foruda.gitee.com/images/1673448795396221612/b1a83972_5631341.png"> <img title="9" height="99px" src="https://foruda.gitee.com/images/1673448776568360772/8020d52e_5631341.png"> <img title="10" height="99px" src="https://foruda.gitee.com/images/1673448764001994371/f31b3d7c_5631341.png"> <img title="11" height="99px" src="https://foruda.gitee.com/images/1673448747945225423/06d46621_5631341.png"> <img title="12" height="99px" src="https://foruda.gitee.com/images/1673448730710410958/aed58ea8_5631341.png"> <img title="13" height="99px" src="https://foruda.gitee.com/images/1673448718386049142/1376a67a_5631341.png"> <img title="14" height="99px" src="https://foruda.gitee.com/images/1673448700949301086/44e90879_5631341.png"> <img title="15" height="99px" src="https://foruda.gitee.com/images/1673448681492261033/93d09ccd_5631341.png"> <img title="16" height="99px" src="https://foruda.gitee.com/images/1673448668528029320/a2d5d443_5631341.png"> <img title="17" height="99px" src="https://foruda.gitee.com/images/1673448652040601437/0ab373fd_5631341.png"> <img title="18" height="99px" src="https://foruda.gitee.com/images/1673448632111721046/cefd6d7a_5631341.png"> <img title="19" height="99px" src="https://foruda.gitee.com/images/1673448603342780979/56907b76_5631341.png"> <img title="20" height="99px" src="https://foruda.gitee.com/images/1673448590747646272/18103227_5631341.png"> <img title="21" height="99px" src="https://foruda.gitee.com/images/1673448580706301856/200eac06_5631341.png"> <img title="22" height="99px" src="https://foruda.gitee.com/images/1673448570393154765/ecbc8a73_5631341.png"> <img title="23" height="99px" src="https://foruda.gitee.com/images/1673448559654481268/16ac94f8_5631341.png"> <img title="24" height="99px" src="https://foruda.gitee.com/images/1673448546917017623/91f291ba_5631341.png"> <img title="27" height="99px" src="https://foruda.gitee.com/images/1673448534291267181/af753872_5631341.png"> <img title="28" height="99px" src="https://foruda.gitee.com/images/1673448518523538924/cbaf2e93_5631341.png"> <img title="29" height="99px" src="https://foruda.gitee.com/images/1673448507310234718/e4d5a5b7_5631341.png"> <img title="30" height="99px" src="https://foruda.gitee.com/images/1673448494972864268/c0cbb519_5631341.png"> <img title="31" height="99px" src="https://foruda.gitee.com/images/1673448483122324509/1a4ecf08_5631341.png"> <img title="32" height="99px" src="https://foruda.gitee.com/images/1673448467221870442/cc0509d2_5631341.png"> <img title="33" height="99px" src="https://foruda.gitee.com/images/1673448444980295025/fe98f8f2_5631341.png"> <img title="34" height="99px" src="https://foruda.gitee.com/images/1673448435200854158/7e0017c8_5631341.png"> <img title="35" height="99px" src="https://foruda.gitee.com/images/1673448421655731019/5494d44a_5631341.png"> <img title="36" height="99px" src="https://foruda.gitee.com/images/1673448410562105869/8ef9cf11_5631341.png"> <img title="37" height="99px" src="https://foruda.gitee.com/images/1673448397389253581/4097822b_5631341.png"> <img title="38" height="99px" src="https://foruda.gitee.com/images/1673448381886541052/114185b4_5631341.png"></p>

   2021本科招生宣讲 （深圳大学医学部）生物医学工程学院

   - 陈思平（教授）
     - 深圳大学生物医学工程学科创始人
     - 深圳大学原副校长
     - 医学超声关键技术国家地方联合工程实验室主任
   - 学科方向
     - 医学人工智能
     - 医疗电子工程
     - 医疗检测技术
     - 纳米精准医学
   - BME 是工程学与生命、医学紧密相融的新兴交叉学科，它致力于以工程技术解决生命、医学及健康领域的关键问题，为医疗器械产业培养人才。
     - 深圳大学生物医学工程学院发展历程：
       - 2005：成立生物医学工程系
       - 2007：获批生物医学工程以及学科硕士点
       - 2008：第一届本科生招生
       - 2012：获批广东省优势重点学科
       - 2015：升级为生物医学工程学院
       - 2016：教育部第4轮学科评估全国第15，广东省第2（共178所）
       - 2018：获批生物医学工程一级学科博士点
       - 2019：获批生物医学工程博士后流动站
       - 2020：获批教育部新工科专业（首批国家级一流本科专业建设点）
   - BME 专业优势：1. 朝阳产业 （2.地域优势，3.师资雄厚，4.定位高端，5.特色办学
     - 美国大健康产业权重：GDP 19%
     - 中国医疗器械业主要行业收入预测，2019 5992亿（增幅9%）
     - 2030 市场规模预测，16万亿
     - 2010-2030 年中国大健康产业市场规模变化以及预测（2020占比：8%）
   - BME 专业优势：2.地域优势
     - 广东省 8358.1 亿元 市值 46%（深圳 6719.58，37%）
     - mindray 迈瑞 5469.64
     - 华大基因 BGI 494.34
     - Snibe Diagnositic 新产业生物 494.13
     - EDAN 理邦仪器 96.68
     - SonoScape 开立 118.28
     - 尚荣医疗 Glory Medical 46.51
     -  产业优势 是 地域优势 的核心
       - 深圳的大湾区是国内最大的医疗器械产业带
       - 深圳医疗器械产业对人才有大量需求
       - 创业气氛良好，学院多位老师带领学生创业，成绩斐然
       - 深圳大学生物医学工程专业大力开展产学研协同育人
       - 本科、硕士毕业生绝大多数选择留在深圳工作
   - 高端医疗器械方向
     - 医学超声成像（主要涉及声学、电子学、生物医学、材料科学、传感技术、计算机及数字技术、图像处理等科学技术领域，是理、工、医学学科相互渗透的技术领域。本学院的超声研究团队主要是以陈思平教授为核心，目前超声团队有教授4人，副教授3人，讲师4人。学院已经建立国内领先的超声研究平台，包括全国高校唯一的医用超声换能器研发中心。）
       1. 超声成像理论
       2. 超声设备开发（换能器等）
       3. 超声智能诊断
       4. 超声治疗研究
     - 应用：
       - 诊疗一体化（药物递送、影像检测）
       - 超声换能器（高灵敏度、高密度）看得见
       - 功能成像（多模态，多参数）看得清
       - 智能诊断（多场景，多特征）看得懂
     - 超声换能器
       - 经消化道超声探头
       - 高分辨率4D探头
       - 自旋转IVUS探头
       - 环视超声内窥镜
     - 超声成像前沿技术
       - 超声微血管成像
         - 大鼠肾脏 BMode 图像
         - 肾脏内部微泡信号
         - 肾脏微血管图像
       - 磁致振动超声弹性成像
         - B Mode 图像
         - 磁致剪切波传播
         - 弹性分布图像
     - 超声治疗新技术
       - 超声联合微泡治疗示意图
       - 超声联合微泡能够引起更多的紫杉醇脂质体进入脑胶质瘤内
       - 超声联合微泡治疗后，能够激活小胶质细胞（红色），进而吞噬脑内的淀粉样蛋白沉积（绿色），有可能是AD潜在的治疗机制之一。
       - 实时观察超声微泡开放血脑屏障的过程。
   - 医学检测技术（国内唯一一个对体外诊断进行系统教学、研究和产业化的机构。）
     1. 体外诊断关键材料（试剂）（高灵敏核酸检测）
     2. 体外诊断关键技术（微流控）（高灵敏蛋白检测，超声辅助生物标志物电化学检测）
     3. 可穿戴生物传感仪器（可穿戴电子设备的运动传感器）
     4. 高端体外诊断系统（AI） （床旁医学检测仪器）
     - 高通量凝血检测平台
     - 液滴数字PCR
     - 电路板试剂操控
     - 超声辅助样本混合
   - 人工智能医疗方向（产科超声智能系统：智能化产科超声影像分析是通过创新的人工智能研究，优化产科超声的图像采集，提高临床医生的检查效率，改善胎儿诊断的精度。通过减少怀孕阶段的误诊，漏诊和过度医疗，以人工智能为每一个宝宝的成长保驾护航。学院是国内拥有研究和 **产业化** 领先优势的智能化产科超声影响团队，在全世界范围内享有盛名。）
     1. 机械手智能导航与超声扫查
     2. 视频超声标准切面的智能检测
     3. 胎儿生长参数和姿态的智能测量
     - 产科超声影响筛查：
       - 机械臂智能导航与扫查
       - 胎儿运动姿态分析
       - 3D渲染可视化
       - 胎儿体积分析
       - 测量
     - 基于三维医学影像的人体多器官智能化分割
     - 机械臂智能导航与产科超声的自主扫查
     - 三维超声智能化胎儿发育分析
     - 三维超声智能化胎儿姿态和参数分析
   - 人工智能医疗方向，脑机接口（脑机接口技术指通过信号采集的方式，将人脑与外部设备间建立直接连接通路，实现脑与外部设备间的单项或双向信号交换，并最终促进人与人之间的沟通，创造巨大价值。通过发展脑机接口技术，探究与神经疾病相关的神经标记物，推进 **脑疾病的准确诊断** 技术，发展神经修复治疗手段。）脑（成像/脑波）   - 信号采集    - 脑机接口（    - 预处理    - 特征提取    - 机器学习    - ）   - 应用接口    - 应用（疲劳检测，游戏、康复、外骨骼、机械臂）
     1. 神经信息工程研究
     2. 多模态脑数据处理
     3. 精准神经调控技术
     4. 神经疾病标记识别
     5. 脑疾病诊断新技术
     - 应用
       - 脑电控制的智能打字
       - 基于脑神经活动的机械手控制技术
       - 基于脑网络分析方法的神经疾病标记物研究
       - 精准实时神经调控闭环系统
   - 纳米精准诊疗方向（分子影响与纳米医学）肿瘤多模态精准诊疗（Multimodal Synergistic Cancer Therapy：Chemotherapy PDT，RT，GT，PTT，HIFU，MHT Immunotherapy）（生物医学工程学院2017年成立分子影像系。主要从事分子影像学和纳米医学等相关领域的科学研究，致力于分子影像探针和纳米药物的临床转化应用。目前拥有教学和科研人员共28名，包括特聘教授3名，教授1名，副教授1名，助理教授2名。）
     1. 分子影像与智能诊疗试剂
     2. 纳米气体治疗
     3. 无创光声检测
     4. 纳米材料的 3D 生物打印
     5. 基因治疗
     - 肿瘤饥饿治疗
     - 肿瘤化学动力治疗
     - 肿瘤免疫治疗
     - 纳米气体治疗
     - 3D生物打印
     - 肿瘤基因治疗
   - 科研力量
     - 近三年，经费合计 10,684余万，科研设备总值 9100 万。
       - Maglumi 2000 全自动化学发光免疫分析仪
       - Maglumi 8000 全自动血清工作站
       - 多参数监护产品
       - 呼吸气体检测仪
       - 掌上式彩超
     - ADVANCED MATERIALS 
     - Therneostics
     - BIOMEDICAL ENGINEERING
   - 教学平台
     - 广东生物医学信息检测与超声成像，重点实验室
     - 国家地方联合 工程实验室
     - 深圳市 体外诊断仪器，公共技术服务平台
     - 广东省 医疗电子仪器转化 工程技术研究中心
     - 深圳大学，广东省高校实验教学示范中心
   - 特色办学：人才培养在左，产学研结合在右
     - 本研一体化培养卓越班
       - 毕业推免率大于 30%
       - 国际交流
       - 直博计划
       - 名企带薪实习计划
       - 导师制
       - 提前进入研究生课程
     - 人工智能医疗校企协同实验班
       - 校企合作协同育人
       - 课程学习与项目学习融合
       - 专注人工智能医疗方向
       - 培养复合型 ”新工科“ 人才
       - 导师制
       - 与业界接轨
     - 校企协同育人（华为、腾讯、MINDRAY迈瑞，UNITED IMAGING 联影）
       - 全面、彻底的导师制，个性化培养
       - 为每位同学创造个性发展良好空间（兴趣出发，不内卷）
       - 产学研深度合作，培养复合型人才
       - 优越的教学科研资源
       - 卓越班与创新班，特色教学
       - 国际名校交流，开阔眼界
   - 特色办学，人才篇
     - 校企协同育人
       - 企业实习实践
       - 产学研结合
     - 动手实践
       - 推广聚徒教学
       - 本科生进实验室
       - 理论与实践结合
       - 实现个性发展
       - 尽量避免内卷
     - 竞赛竞争力
       - 参加各类竞赛
       - 锻炼实践经验
       - 高手同场竞技
       - 虽985何足惧
   - 特色办学
     - 交流形式
       - 暑假项目（全部年级可参加）
       - 学期交流（全部年级可参加）
       - 2+2学分连接项目（大二可参加）
       - 3+1学分连接项目（大三可参加）
       - 世界名校本科生来学院交流学习：
     - 部分合作学校：剑桥大学、哥伦比亚大学、埃默里大学、香港大学等
   - 职业发展（深圳是我国最重要的医疗器械产业基地，对生物医学工程专业人才需求量巨大。本科毕业生去向主要包括工作、读研（含海外深造）。就业率（计入读研）超过95%，位居深圳大学前列。毕业后选择工作的同学一般有多个选择，不乏腾讯等著名企业。师生创业火爆，已经造就多位亿万富翁，千万富翁。）学术精英、行业骨干、企业高管
     - 读研（60%，包括海外深造 10%）绝大部分国内读研的学生选择留在深大，其他就读中科院，华南理工，中山，浙大，复旦等国内名校。海外深造包括 斯坦福大学，哥伦比亚大学，帝国理工学院等名校。
     - 就业（40%）去向主要为知名企业，包括腾讯，联影，迈瑞，理邦等；或相关单位的公务员。
   - 创业实例：LWPOCT 活水 快易准 （体外诊断张会生教授带领学生于 2016年11月创立，打造场景化快速诊断的整体解决方案，在”快，易，准“三个方面，很好满足临床诊断需求，抗击新冠大放异彩。估值 20-30亿。）
     - 研发总监：王东元（2013级学生）
     - 研发副总：陈嘉玲（2016级学生）
     - 质量总监：武通园（2013级学生）
   - 社团互动：新生风采大赛，舞诞，校运会彩跑，Bancs 音乐社，义工联，篮球社
   - 丽湖校区：深大盒子 网红通知书 ...
   - 张学记 教授：
     - 深圳大学副校长
     - 生物医学工程学科带头人
     - 美国医学与生物工程院院士
     - 俄罗斯工程院外籍院士
   - 优势总结：
     - 专业优势：医疗器械、大健康：朝阳行业
     - 地域优势（产业优势）：深圳为核心的大湾区是全国医疗器械产业主要聚集地
     - 平台优势：国家级一流本科，顶配师资力量，多个顶级教学科研平台
     - 特色培养：产学研协同育人，个性培养避免内卷，大力开展创新创业
   - 我们距离顶级大学的顶级专业，只差一个条件：优秀的你

   ![输入图片说明](https://foruda.gitee.com/images/1673456100809544356/849714db_5631341.png "屏幕截图")
  
5. 每一刻都是小永远 每一刻都是整装待发，毕业季答辩会

   <p><a target="_blank" href="https://med.szu.edu.cn/Item/1906.aspx"><img height="99px" src="https://foruda.gitee.com/images/1673445646453971890/0ea3d87c_5631341.png"></a> <a target="_blank" href="https://bme.szu.edu.cn/20221/1024/97.html"><img height="99px" src="https://foruda.gitee.com/images/1673445640496310302/e34be110_5631341.png"></a> <a href="https://med.szu.edu.cn/Item/7606.aspx" target="_blank"><img height="99px" src="https://foruda.gitee.com/images/1673445634804346344/44e99f49_5631341.png"></a> <img height="99px" src="https://foruda.gitee.com/images/1673445624117421980/56354a06_5631341.png"></p>

6. 一起来看看今年的毕业礼物~~

   <p><img height="99px" src="https://foruda.gitee.com/images/1673446356847004321/da623144_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1673446337644176261/38be4826_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1673446312756334270/71001ec4_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1673446241744336720/ffeda1e3_5631341.png"></p>

7. 最近有同学咨询关于深圳大学生物医学工程专业的一些问题，请大家关注深大直播平台，6月24日 晚8：00，来听听学院杰出的青年教师[黄炳升](https://bme.szu.edu.cn/20161/0301/47.html)老师聊一聊深圳大学的生物医学工程。

   生物医学工程专业是深大的最强专业之一，教育部评估在全国高校并列15，入围国家首批一流本科专业建设点。

   [![黄炳升](https://foruda.gitee.com/images/1673411781251120792/6d36b07f_5631341.png "黄炳升副教授")](https://bme.szu.edu.cn/20161/0301/47.html)

   - 生物医学工程是一门什么样的学科？
   - 深圳大学生物医学工程专业有什么优势？
   - 专业的市场价值如何？就业前景如何？
   - 敬请关注 6月24日 晚 8:00 深圳大学生物医学工程学院 招生宣讲

8. 致我那些停留在深圳大学生物医学工程学院的美好时光

   大学那些将逝及将至的时光 所有的经历多彩到我只想多做停留

   嗯，考研或是个不错的选择!

   <p><img height="99px" src="https://foruda.gitee.com/images/1673446671516457951/5ba1a60d_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1673446662891142052/9b4e5e3d_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1673446650167538426/69ab52ce_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1673446633430054023/03a455e4_5631341.png"></p>

9. 满满回忆~

   毕业快乐！  
   愿你鲜衣怒马，繁华阅尽，归来仍是少年！  
   生工是你一辈子的家~

   <p><img height="99px" src="https://foruda.gitee.com/images/1673447316653749369/eb228a4b_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1673447296723565568/2770657c_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1673447274601556550/01fd7d2f_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1673447254544822368/223f7f9d_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1673447229143928440/fd470e26_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1673447213260787229/0b16bffc_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1673447197242524018/077269d3_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1673447182307062193/08211a8b_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1673447458248587872/5560ac0e_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1673447160231950308/10b23fdc_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1673447148982531339/a0991800_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1673447138399538427/eca0af37_5631341.png"></p>

10. 生物医学工程专业是怎样一个学科？在深圳大学生物医学工程学院就读是一个怎样的体验? 跟我来，带你绕操场跑两圈，且看我一一道来哈~~~~

    <img height="99px" src="https://foruda.gitee.com/images/1673458111765311390/108732ba_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1673458147412280371/257a95a8_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1673458106601993059/34b53951_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1673458104593152659/6c0a064e_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1673458070322857690/ffd47104_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1673458063754998716/ba0d94fe_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1673458055419371780/e2d0a452_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1673458048059132328/1ed498fa_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1673458040588640838/679b8770_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1673458034629876945/8eb55884_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1673458026775508079/1c1d4b12_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1673458024504889191/2c9e08d3_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1673458030394792683/74ab95a5_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1673458024818460339/6e4c19fe_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1673458009026998969/4aea7d5a_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1673457983881606334/6ed1f656_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1673457971414610231/b2d573b8_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1673457963472127089/dd65d3f2_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1673457956222268396/eed82ef7_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1673457948236338495/38611a47_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1673457940839589410/3cf5f795_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1673457944035492135/45e93f2c_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1673457922200582190/863ebca8_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1673457913488409763/89778ddb_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1673457907574479663/9ab56d25_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1673457897308314960/217d8ed6_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1673457889316816207/56500a41_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1673457880671398432/75f1bfec_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1673457869193941531/0e6c4b31_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1673457862146301222/802970ec_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1673457853655273136/e31c305b_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1673457844648029497/602c5449_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1673457838483533288/d3556e52_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1673457824053442718/f394a9de_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1673457811937292038/a3e61d64_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1673457805312706461/7ce44e33_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1673457790093158686/01a91b77_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1673457716357230831/a1e65463_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1673457717309744000/31e5f975_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1673457716044723558/a754ca5d_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1673457750069167553/f5b20c59_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1673458115957784699/6af27b00_5631341.png"></p>

    > 这是我每天要做的生物医学检测工作，好，以后我也要报考生物医学工程专业，欢迎欢迎，等你来，这里攒着我一生最美好的大学时光，阳光洒满绿荫，友情将本青春探索陪伴方向。深圳，中国最年轻的一线城市深圳大学，建校30余年，可以培养出众多传奇校友，这是一所年轻却实力雄厚的新一线大学，也轻而易举。

    > 在这里，生生学子被赋予一切，教师们不仅在科研上不断突破，孜孜不倦，在培养学生上更是因材施教，用心良苦。不仅成就了我的事业，更让我无限接近生命的本身。我们探索和发现将帮助无数的人从疾病和痛苦中获得健康和牺牲。2020年新冠肺炎四面全球深大深宫学院为病毒抗体检测打造了分析仪。

    > 为新冠肺炎C。一项项关乎人类健康的科研，一双双善于发现的眼睛，一步步构建起生物医学里面的技术高。

    > 我相信只有创新才是驱动未来世界发展的真正力量。我希望从这里出发，可以通向无限可能深圳大学，利于大湾区从就业到创业，这里科学怎么提供特色教育模式，培养复合型人才。这里众多著名企业云集，周边一流产学研平台将为全球输送国际尖端人才。这里，实践和资源将不断透过我认知的边界，平台和交流，将开启我向往未来的。

    > 知识与交流，理论与实践，拼搏与何在？从这里，我怀揣梦想，担起责任，创造我的事业。加入复印机公司，就走一个方向，就是你的未来。在最美的时光里，相约最好的伙伴，畅谈最美的梦想，这里将会书写我美好的青春回忆。在这里，蓝图由我勾画，梦想任我追。

    - 深圳大学总医院
    - 姐姐 李云朵
      - 深圳迈瑞生物医疗电子股份有限公司 临床工程师
      - 2015级深大生物医学工程学院毕业生
    - 弟弟 李云锦
      - 深圳高级中学 高三学生
    - 倪东 教授
      - 香港中文大学 工学博士
      - 研究方向：智能医学超声
    - 陆敏华 教授
      - 香港理工大学 工学博士
      - 研究方向：颅脑及血流超声成像
    - 高毅 教授、青年特聘专家
      - 美国佐治亚理工大学 工学博士
      - 美国哈佛大学医学院 博士后
      - 研究领域：医学人工智能
    - 陈昕 教授
      - 中国科学技术大学 工学博士
      - 研究领域：医学超声技术
    - 汪天富 教授
      - 深圳大学生物医学工程创系主任
      - 教育部高等学校生物医学工程类专业教学指导委员会委员
      - 广东省生物医学信息检测与超声成像重点实验室主任
    - 许改霞 教授
      - 浙江大学 工学博士
      - 美国纽约大学布法罗分校 博士后
      - 新加坡南洋理工大学 访问学者
      - 研究领域：生物成像与传感
    - 生物医学工程学院 广东省生物医学工程实验教学中心
    - 李自达 助理教授
      - 美国密歇根大学 博士
      - 研究领域：微纳生物工程
    - 科研平台
      - 国家工程实验室
      - 深圳市体外诊断仪器 公共技术服务平台
      - 广东省医疗电子仪器转化 工程技术研究中心
      - 广东省 高等学校 实验教学 示范中心
      - 深圳大学 - 美国赛灵思公司 医疗电子 EDA 联合实验室
      - 广东省 生物医学信息 检测与超声成像 重点实验室
      - 深圳大学 - 赛普控斯半导体（CYPRESS）PSoC 联合实验室与培训中心
    - 生物医学工程学院 医学超声成像技术实验室
      - 研究项目：超快速颅脑血流成像
    - 生物医学工程学院 微纳生物工程实验室
      - 研究项目：微流控快速诊断芯片、单细胞基因测序
    - 生物医学工程学院 医学超声成像技术实验室
      - 研究项目：超快速颅脑血流成像
    - 生物医学工程学院 医学信息与神经动态实验室
      - 研究项目：脑机交互系统
    - 生物医学工程学院 神经信息实验室
      - 研究项目：神经信息处理机制及神经信息解码
    - 生物医学工程学院 深圳体外诊断仪器公共技术服务平台
      - 研究项目：全自动免疫分析仪
    - 生物医学工程学院 视觉与系统智能计算实验室
      - 研究项目：医学影响的人工智能分析
    - 吴光耀 教授、主任医师
      - 深圳大学总医院放射科主任
      - 生物医学工程学科博士生导师
    - 叶继伦 教授
      - 西安交通大学 工学博士
      - 研究方向：生命信息监护技术及应用
    - 但果 教授
      - 大连理工大学 工学博士
      - 研究方向：康复工程、医疗电子
    - 生物医学工程学院 广东省生物医学信息检测与超声成像重点实验室
      - 研究项目：人体生命信息检测
    - 陈思平 教授
      - 深圳大学生物医学工程学科创始人
      - 深圳大学原副校长
      - 医学超声关键技术国家地方联合工程实验室主任
    - 本专业 2019 年 首批入选国家一流专业建设点 广东省内本专业仅两所高校入选
    - 生物医学工程学院 广东省生物医学信息检测与超声成像重点实验室
      - 研究项目：太赫兹成像
    - 生物医学工程学院 学科方向
      - 医学人工智能
      - 医疗电子工程
      - 医疗检测技术
      - 纳米精准医学
    - 生物医学工程学院 MUSIC（智能医学超声）实验室
      - 研究项目：机器人超声自主扫查系统
    - 教学科研实验室 8845 平方米
      - 1个国家级平台
      - 3个省级平台
      - 4个市级平台
      - 2个院士工作站：田禾、顾建军
      - 1个诺贝尔奖科学家实验室：马歇尔生物医学工程实验室
    - 生物医学工程学院 医学信息与神经动态实验室
      - 研究项目：虚拟现实、眼动追踪系统
    - 生物医学工程学院 柔性智能感知实验室
      - 研究项目：柔性智能感知器件及系统
    - 生物医学工程学院 视觉与系统智能计算实验室
      - 研究项目：全幅病理图像的人工智能分析
    - 本专业建立了三十多家省级 校级企业实习实践基地 （深圳湾创业广场）
      - 飞利浦、迈瑞、科曼、联影、迈科龙、商汤、西门子、普门
    - 生物医学工程学院 深圳体外诊断仪器公共技术服务平台
      - 研究项目：全自动发光免疫分析仪
    - 胡雨阳 2020 届毕业生
      - 获得荷兰 Erasmus 大学博士全额奖学金 即将前往攻读 博士学位
    - 职业发展：
      - 读研、出国、就业
      - 科学引领者、技术发明者、企业创立者
    - 张旭 讲师
      - 哈尔滨工业大学 工学博士
      - 研究方向：生命信息无创检测技术
    - 尹力 生物医学工程  2010 级毕业生
      - 现任 深圳市新产业医学工程股份有限公司 研发总监
    - 刘旭东 助理教授
      - 香港中文大学 工学博士
      - 研究方向：太赫兹光谱及成像
    - 孙怡雯 教授
      - 香港中文大学 博士
      - 英国伦敦国王学院 访问学者
      - 研究方向：太赫兹光谱与成像
    - 张学记 教授
      - 深圳大学副校长
      - 生物医学工程学科带头人
      - 美国医学与生物工程院院士
      - 俄罗斯工程院外籍院士
    - 梦想 任我追随
    - 公众号 招生办 咨询号 http://bme.szu.edu.cn 深圳大学 医学部 生物医学工程学院

---

（网站首页）

- 深圳大学 医学部 https://med.szu.edu.cn/Default.aspx

  院系设置

  - 基础医学院
  - 临床医学院
  - 生物医学工程学院
  - 药学院
  - 护理学院
  - 口腔医学院
  - 公共卫生学院
  - 中医学院（筹）
  - 全科医学系
  - 医学人文中心
  - 转化医学研究院（含生化中心）
  - 深圳大学卡尔森国际肿瘤中心

  医疗机构

  - 深圳大学第一附属医院
  - 深圳大学总医院
  - 深圳大学附属华南医院

- 生物医学工程学院 https://bme.szu.edu.cn/

  师资队伍

  - 教授
  - 副教授
  - 助理教授
  - 行政/教辅人员
  - 专职研究员

  学院招生

  - [本科招生](https://bme.szu.edu.cn/g/college_admissions/undergraduates/index.html)
  - [硕士招生](https://bme.szu.edu.cn/g/college_admissions/master/index.html)
  - [博士招生](https://bme.szu.edu.cn/g/college_admissions/doctor/index.html)
  - 联合培养

# 生物医学工程 本科招生

[首 页](https://bme.szu.edu.cn/index.html) > [学院招生](https://bme.szu.edu.cn/g/college_admissions/master/index.html) > [本科招生](https://bme.szu.edu.cn/g/college_admissions/undergraduates/index.html)

### 生物医学工程

生物医学工程专业培养掌握生物医学工程的基本理论和专业知识，获得工程师良好训练，基础知识扎实、专业知识宽厚、实践能力突出，能胜任生物医学工程领域的科研、教学、设计、开发、管理和咨询等方面工作，具有创新能力、组织协调能力、团队精神、国际视野和终身学习能力的高级专门人才。学生毕业后经过5年左右的实际工作锻炼，能成长为生产岗位的技术管理者或科研设计岗位的技术骨干。

### 创新班 · 人工智能医疗校企协同实验班 【生物医学工程类专业代码：234】
—— 创新班30人，大一结束后另行选拔

- 校企合作协同育人
- 专注人工智能医疗方向
- 导师制
- 课程学习与项目学习融合
- 培养复合型“新工科”人才
- 与业界接轨

### 卓越班 · 本研一体化培养卓越班 【生物医学工程（卓越班）专业代码：235】
—— 卓越班30人，从高考考生中招收

- 毕业推免大于30%
- 直博计划
- 导师制
- 国际交流
- 名企带薪实习计划
- 提前进入研究生课程

### 培养方案

- [2022级生物医学工程（创新班）主修培养方案](https://bme.szu.edu.cn/pic/files/2022chuangxinban.pdf) （请点击下载）
- [2022级生物医学工程（卓越班）主修培养方案](https://bme.szu.edu.cn/pic/files/2022zhuoyueban.pdf) （请点击下载）

### 招生计划

深圳大学2022年普通本科广东省普通类招生计划

 **广东省（含分组、选考科目）** 

![输入图片说明](https://foruda.gitee.com/images/1673459471859318726/e8bd9bc3_5631341.png "屏幕截图")

 **外省（3+1+2高考改革省份）** 

![输入图片说明](https://foruda.gitee.com/images/1673459478098138700/fd84b3ce_5631341.png "屏幕截图")

 **外省（非高考改革省份）** 

![输入图片说明](https://foruda.gitee.com/images/1673459483546799757/c26dab3e_5631341.png "屏幕截图")

 **历年分数（广东省）** 

![输入图片说明](https://foruda.gitee.com/images/1673459488838688342/c69a1bcf_5631341.png "屏幕截图")
 

专业选修课

- 体外诊断仪器原理与实践 Clinical Laboratory Instrumentation Designs and Implementations
- 硬件描述语言及数字系统设计 Hardware Description Language and Digital System Design
- 分子诊断技术 Molecular Diagnostic Technologies

卓越班选修

- 纳米生物医学 Nano-Bio-Medicine
- 工程制图 Engineering Graphics

---

（公号转载）

- [在深圳大学就读生物医学工程专业是一种什么样的体验？](https://mp.weixin.qq.com/s?t=pages/video_detail_new&scene=1&vid=wxv_1441231737589399555&__biz=MzUyOTYzOTc4MQ==&mid=2247487161&idx=1&sn=54cde6a5b0ec2b18d01ccb675dd5bbf3) 【[BME宣传片：丽湖校区](https://www.bilibili.com/video/BV17z4y1Q7ks/)】
- [听在读青年人谈个人成长 | 黄斌 ：“从粤海到丽湖，与深大生物医学工程学科共成长”](http://mp.weixin.qq.com/s?__biz=MzUyOTYzOTc4MQ==&mid=2247488370&idx=3&sn=6c61729b08108d9ecb9dc3065ffa99b6)
- [生物医学工程学院科研团队近期取得系列新进展](http://mp.weixin.qq.com/s?__biz=MzUyOTYzOTc4MQ==&mid=2247491406&idx=4&sn=f570df468504ad0158e1a412705f1215)
- [我院多位学者入选全球前2%顶尖科学家榜单（2022版）](http://mp.weixin.qq.com/s?__biz=MzUyOTYzOTc4MQ==&mid=2247491406&idx=2&sn=f59afe10997938f228a51a9bc7f9dab6)

# [生物医学工程学院科研团队近期取得系列新进展](https://mp.weixin.qq.com/s?__biz=MzUyOTYzOTc4MQ==&mid=2247491406&idx=4&sn=f570df468504ad0158e1a412705f1215)

深圳大学生物医学工程学院 2022-10-29 07:46 发表于广东

![输入图片说明](https://foruda.gitee.com/images/1673485234037361000/e9ebad6c_5631341.png "屏幕截图")

- 微信号：深圳大学生物医学工程学院 SZUBME2006
- 功能介绍：平等、友爱、团结、奋进。无私奉献、服务师生！

![输入图片说明](https://foruda.gitee.com/images/1673485289136459751/78149ecf_5631341.gif "640 (3).gif")
图片

倪东教授团队在 Medical Image Analysis 上发表重要文章

### 学术论文

![输入图片说明](https://foruda.gitee.com/images/1673485321804711847/9263a8eb_5631341.png "屏幕截图")

近日，深圳大学医学部生物医学工程学院倪东教授团队在国际医学图像分析领域的顶级期刊Medical Image Analysis上发表了题为“ **Extracting Key Frames of Breast Ultrasound Video Using Deep Reinforcement Learning** ”的学术论文，介绍团队在乳腺超声视频关键帧提取的突破性研究成果。 **团队成员研究生英启龙和黄若冰助理教授为共同第一作者，倪东教授为通讯作者，深圳大学为第一署名单位。** 

在本研究中， _针对乳腺超声视频的冗余性及复杂性_ ， **团队设计了基于深度强化学习自动提取视频关键帧的框架** 。通过对病灶在乳腺超声视频中出现的位置、大小以及属性的分析，设计新颖的奖励机制以及处理数据不均衡的损失函数，应对乳腺超声视频的复杂情况，动态输出包含丰富诊断信息的关键帧集。通过关键帧集的自动生成，有效辅助医生诊断，提高了医生对乳腺结节良恶性的分类精度。

 **文章链接：**  https://www.sciencedirect.com/science/article/pii/S1361841522001372

黄鹏教授团队在Angewandte Chemie 发表封面文章

### 研究论文

![输入图片说明](https://foruda.gitee.com/images/1673485406227394072/c1dd1a8b_5631341.png "屏幕截图")

近日，深圳大学医学部生物医学工程学院黄鹏特聘教授团队在国际顶级学术期刊Angewandte Chemie上发表了题为“ **Antineoplastic Enzyme as Drug Carrier with Activatable Catalytic Activity for Efficient Combined Therapy** ”的热点研究论文（Hot Paper），并被杂志社选为封面论文。该论文所有工作均在深圳大学完成， **团队张一帆助理教授和江珊珊博士研究生为共同第一作者，黄鹏教授为通讯作者，深圳大学为唯一署名单位和唯一通讯作者单位** 。

在该研究中，团队将三种小分子（索拉菲尼，tPy-Cy和紫杉醇）与葡萄糖氧化酶通过疏水作用自组装，构建了三种酸响应激活的基于天然酶的纳米药物。天然酶直接作为纳米载体与小分子自组装具有独特的优势：首先，小分子进入了酶的疏水口袋，同时封闭了葡萄糖氧化酶的催化位点，使其即使进入血液或正常组织中，也不能产生足够强的催化效果，显著地降低了其毒性。然后，当该纳米药物到达肿瘤组织后，肿瘤组织的酸性微环境可以改变酶的二级结构，使纳米颗粒解组装后恢复活性，从而行使酶催化治疗。这种酸响应激活的催化治疗使小鼠的血糖始终保持在正常范围。 **实验结果表明** ， _在小鼠的皮下三阴性乳腺癌肿瘤模型上，激活后的纳米药物均具有显著的肿瘤抑制效果。_ 

 **文章链接：**  https://onlinelibrary.wiley.com/doi/10.1002/anie.202208583

END

- 文字 | 黄若冰  张一帆
- 初审 | 李双双
- 复审 | 许改霞
- 终审 | 陆敏华

### 往期推荐

- [2022年秋季招聘宣讲会超燃来袭](https://mp.weixin.qq.com/s?__biz=MzUyOTYzOTc4MQ==&mid=2247491040&idx=1&sn=184295c740b5c5c4498086a1ac7f94ef)
- [欢迎报考深圳大学生物医学工程学院 2023级硕士研究生](https://mp.weixin.qq.com/s?__biz=MzUyOTYzOTc4MQ==&mid=2247490866&idx=1&sn=8cce0c001ed41b363c023837cc9e876a)
- [深圳大学生医学部生物医学工程学院 2022级新生见面会成功举行](https://mp.weixin.qq.com/s?__biz=MzUyOTYzOTc4MQ==&mid=2247490866&idx=2&sn=787035b5ac614a8d54fadda5bbf1f852)
- [医学部生物医学工程学院召开2022 新学期工作会议](https://mp.weixin.qq.com/s?__biz=MzUyOTYzOTc4MQ==&mid=2247490866&idx=3&sn=4a46797391aa8b9a49a772a232f01bce)

深圳大学 生物医学工程学院 为梦想加油！

<p><img width="199px" src="https://foruda.gitee.com/images/1673485533574329348/a8866cfd_5631341.png"></p>

长按二维码 关注我们

# [听在读青年人谈个人成长 | 黄斌 ：“从粤海到丽湖，与深大生物医学工程学科共成长”](https://mp.weixin.qq.com/s?__biz=MzUyOTYzOTc4MQ==&mid=2247488370&idx=3&sn=6c61729b08108d9ecb9dc3065ffa99b6)

深圳大学生物医学工程学院 2021-12-02 16:40 发表于广东

![输入图片说明](https://foruda.gitee.com/images/1673485234037361000/e9ebad6c_5631341.png "在这里输入图片标题")

- 微信号：深圳大学生物医学工程学院 SZUBME2006
- 功能介绍：平等、友爱、团结、奋进。无私奉献、服务师生！

![输入图片说明](https://foruda.gitee.com/images/1673485289136459751/78149ecf_5631341.gif "在这里输入图片标题")

有这样一位“土生土长”的深大人  
在美丽的荔园里  
从本科到硕士到博士  
多年的求学生涯中  
他与深大生物医学工程学科共同成长

一起来听听  
他的成长故事吧！

### 人物简介 黄斌

![输入图片说明](https://foruda.gitee.com/images/1673485836586756111/86d0345c_5631341.png "屏幕截图")

本硕博就读于  
深圳大学生物医学工程专业  
曾获全国大学生生物医学工程  
创新设计竞赛全国一等奖  
连续三年获得研究生学业奖学金  
参与发表SCI论文4篇  
中文核心论文2篇  
申请专利3项，授权1项

### TA的故事

![输入图片说明](https://foruda.gitee.com/images/1673485876721175059/8524cc6c_5631341.png "屏幕截图")

怀着对深圳大学的热爱，黄斌来到了深大医学部生物医学工程专业。他大二就进入黄炳升老师的实验室学习，多年累积的科研实践经验和对深大的热爱更加坚定了他考研留在深圳大学的决心。硕士研究生期间，黄斌在导师的指导下进一步学习专业知识，进行更深入的科学研究。终于，黄斌凭借对医学图像处理领域的热爱，经过多年专注的学习和实践，在导师的指导和团队的协作下，在其研究领域的顶级期刊IEEE transactions on medical imaging以第一作者的身份发表文章。同时，他有幸获得平安科技公司的实习机会，从学校到职场，从实验室到公司，黄斌感受到不同环境下的差异，也从了解、学习同事们的思想观念和办事能力中收获累累硕果。这些经历坚定了他继续攻读博士的信念，在陈昕和黄炳升导师的支持和鼓励下，他继续在深圳大学生物医学工程专业攻读博士学位。

他认为从本科到硕士到博士，是学业不断精进的过程。本科时期，导师主要帮助学生在实践中学习，从技术上入门；硕士时期，学生要学会自主研究，了解如何发起课题，实施课题，分配任务，最终完成课题，同时需要对自己的专业领域有更深刻的认识和了解，掌握本领域必备的专业技能；博士时期，学生需要对专业领域深入研究、了解，发现领域内仍未解决的关键问题，分析问题，并解决问题。这同时也是自我不断成长的过程。本科期间，黄斌遇到对自己产生深远影响的导师；硕士期间，黄斌收获了爱情，收获了去大公司实习的机会，也更加明确自己的目标——继续攻读博士学位；如今，黄斌正在香港大学进行交换学习，体验不同学校的教学方式、教学制度，学习港大导师的科研思路，开阔了眼界，增长了见识。

![输入图片说明](https://foruda.gitee.com/images/1673485901670136894/cfe813cd_5631341.png "屏幕截图")

> 黄斌作为BME线上学术交流会嘉宾

黄斌的导师建议他说：不需要你多聪明，也不需要你非得头悬梁锥刺股。你需要的是首先专注这个方向，然后找到兴趣所在，每天都做喜欢做的事情，再加上团队合作，考虑社会、行业需求，那么你就离成功就不远了。

![输入图片说明](https://foruda.gitee.com/images/1673485934174864608/9e669543_5631341.png "屏幕截图")

> 黄斌参与国际会议的志愿者活动

黄斌于2013年入学深大，至今已有8年之久，对深圳大学有比较深入的认识。无论在日常生活还是教学实验中，他都能深切地体会到深圳大学优美的校园环境，雄厚的师资力量，完善的硬件设施，日益完善的各项制度，这都为学生进行实验研究提供更多的机会、平台和空间。在科研中，他也切身感受到了：如果你有做课题的想法，可以与老师商量，老师会帮你分析并提出建议，这种“科研自由”的理念也正是深圳大学一年又一年飞速发展的原因之一。

黄斌建议，如果师弟师妹们有志于科学研究，可以尽早了解不同导师的科研方向，找机会进入感兴趣的导师实验室学习，这样既可以帮助我们将理论与实践相结合，更加深刻地了解学习专业知识的用处，增加学习兴趣，还可以帮助我们找准方向，坚定信念。

提到最想感谢的人，黄斌有太多想说的，“感谢带了我7年的导师黄炳升老师，是他带领我走进了科研的海洋，是他让我了解了科研的乐趣，让我坚定地走上了学术的道路，他严厉的要求和谨慎的做事风格，使我受益匪浅。我永远也不会忘记满屏红色的修订印记，那是老师的心血，从中可以发现并改进自己的不足。感谢我的博士导师陈昕老师，他带我走进了新的科研领域，他不遗余力地帮助学生，支持并鼓励每位学生发挥出自己的长处，带领团队在软件、硬件、算法三方面齐头并进，是值得学习的榜样。”“感谢深大，让我遇到了我挚爱的妻子蔡盎，是她的支持和鼓励，才让我能继续坚持走在学术的道路上”。

最后，他祝愿各位深大人前途似锦，祝愿深大桃李满天下。

### 编后语

学长在科研路上奋勇前行  
身体力行深大校训  
他用青春扎根深大  
沉淀出最美的华章

我们像他一样，  
都在追求  
愿做个“安静而纯粹”的人，  
安静地成长，  
安静地提升自己，  
纯粹地学点知识，  
纯粹地浸润在实验室，  
纯粹地为理想而奋斗，  
纯粹地为让自己更好去努力。

今年是你在深大的第几年？  
你与我们生工又有什么故事呢？  
请在评论区分享你的故事吧！

—END—

### 往期推荐

- 陈思平教授受聘为国家药品监督管理局超声手术 设备质量评价重点实验室第一届学术委员会主任
- “以医工结合为抓手破解临床问题”学术讲座圆满举行 
- 学术论坛第十六期 | 太赫兹技术 在生物医学方向的应用
- 生物医学工程学院工会——淘金山 绿道、求水山公园健步活动

深圳大学 生物医学工程学院 为梦想加油！

<p><img width="199px" src="https://foruda.gitee.com/images/1673485533574329348/a8866cfd_5631341.png"></p>

长按二维码 关注我们

---

【笔记】[生物医学工程](https://gitee.com/cylina/siger/issues/I69UJW)

陈昕，深圳大学生物医学工程学院，院长

SIGER 期刊邀请到 深圳大学 生物医学工程学院 接受采访 @cylina 负责拟订一份采访提纲，不限主题，任何与生命科学主题相关的问题均可，也不限于学院，可以扩展到深圳大学，医学院，当前疫情。也可以 [SMARTtimes](https://gitee.com/flame-ai/siger/blob/master/sig/生命/SMARTtimes01.md) 视角拟订一些问题。事先的案头工作，发到自己的 issues 立项。成品MD 可分多主题直投生命频道。

不需要全文转载案头文章，连接，摘要即可。以产出采访提纲为目标。获得嘉宾对青少年科普的支持。

- 陈昕 >> 教授
  https://bme.szu.edu.cn/20161/0301/43.html
  > 姓名：陈昕. 职位：教授，博士生导师. 邮箱：chenxin@szu.edu.cn. 2003年获得中国科学技术大学博士学位，2003-2008在香港理工大学工作，2008年加入深圳大学，现任深圳大学生物医学工 …

  进一步探索
  - 霍普金斯大学陈昕团队发现胚胎干细胞不对称组蛋白 ... sohu.com
  - [陈昕 - 深圳大学 - 生物医学工程学院](https://www.x-mol.com/university/faculty/21339) x-mol.com （登录后才能查看导师的联系方式，硕导）[转自](https://bme.szu.edu.cn/20161/0301/43.html)
  - 陈昕 - 深圳大学艺术学部 art.szu.edu.cn
  - 北京科技大学化学与生物工程学院-陈昕 huasheng.ustb.edu.cn
  - [深圳大学医学部 生物医学工程学院](http://bme.szu.edu.cn/) bme.szu.edu.cn


- 生物医学工程学院-深圳大学医学部
  https://med.szu.edu.cn/Item/1906.aspx
  > 陈昕（Xin Chen）博士，教授，博导，生物医学工程学院副院长。 2003年获中国科学技术大学生物医学工程博士学位，2003-2008年在香港理工大学担任博士后、助理研究员等工作，2008年 …

- 生物医学工程学院-深圳大学医学部
  https://med.szu.edu.cn/Category_431/Index.aspx

  <img width="199px" align="right" src="https://foruda.gitee.com/images/1673350484958292690/9482a994_5631341.png">

  > 简介： 陈昕（XinChen）博士，教授，博导，生物医学工程学院副院长。2003年获中国科学技术大学生物医学工程博士学位，2003-2008年在香港理工大学担任博士后、助理研究员等工 …

  - 学院：生物医学工程学院
  - 专业：医学超声
  - 职称：教授
  - 简介：陈昕（XinChen）博士，教授，博导，生物医学工程学院副院长。2003年获中国科学技术大学生物医学工程博士学位，2003-2008年在香港理工大学担任博士后、助理研究员等工作，2008年加入深圳大学。先后获深圳大学首届荔园优青、深圳市高层次领军人才、2020年深…

  [教授(正高名录)](https://med.szu.edu.cn/Category_18/Index.aspx)  **生物医学工程学院** ：

  | ![输入图片说明](https://foruda.gitee.com/images/1673353648232092156/5dde0504_5631341.png "屏幕截图") | ![输入图片说明](https://foruda.gitee.com/images/1673353475698902927/c3cc2feb_5631341.png "屏幕截图") | ![输入图片说明](https://foruda.gitee.com/images/1673353510949935520/5ed24f22_5631341.png "屏幕截图") | ![输入图片说明](https://foruda.gitee.com/images/1673353486256349322/16d0a1e5_5631341.png "屏幕截图") |
  |---|---|---|---|
  | [张学记](https://med.szu.edu.cn/Item/5430.aspx) | [陈思平](https://med.szu.edu.cn/Item/397.aspx) | [汪天富](https://med.szu.edu.cn/Item/400.aspx) | [常春起](https://med.szu.edu.cn/Item/399.aspx) |
  | 教授，博士生导师 | 教授，博士生导师 | 教授，博士生导师 | 特聘教授，博士生导师 |
  | zhangxueji@szu.edu.cn | chensiping@szu.edu.cn | tfwang@szu.edu.cn | cqchang@szu.edu.cn |
  | ![输入图片说明](https://foruda.gitee.com/images/1673353461918780493/8a16a001_5631341.png "屏幕截图") | ![输入图片说明](https://foruda.gitee.com/images/1673353451104640170/6cd542d2_5631341.png "屏幕截图") | ![输入图片说明](https://foruda.gitee.com/images/1673353440016488171/56512c38_5631341.png "屏幕截图") | ![输入图片说明](https://foruda.gitee.com/images/1673353430236500583/b28d8bef_5631341.png "屏幕截图") |
  | [黄鹏](https://med.szu.edu.cn/Item/403.aspx) | [高毅](https://med.szu.edu.cn/Item/1533.aspx) | [林静](https://med.szu.edu.cn/Item/1734.aspx) | [董海峰](https://med.szu.edu.cn/Item/5431.aspx) |
  | 特聘教授，博士生导师 | 特聘教授，博士生导师 | 特聘教授，博士生导师 | 教授，博士生导师 |
  | peng.huang@szu.edu.cn | gaoyi@szu.edu.cn | jingl@szu.edu.cn | hfdong@szu.edu.cn |
  | ![输入图片说明](https://foruda.gitee.com/images/1673353406521212896/9da72151_5631341.png "屏幕截图") | ![输入图片说明](https://foruda.gitee.com/images/1673353394312149095/569b224f_5631341.png "屏幕截图") | ![输入图片说明](https://foruda.gitee.com/images/1673353386361391040/4c20b305_5631341.png "屏幕截图") | ![输入图片说明](https://foruda.gitee.com/images/1673353372838767109/fd4cda74_5631341.png "屏幕截图") |
  | [张会生](https://med.szu.edu.cn/Item/472.aspx) |  **[陈昕](https://med.szu.edu.cn/Item/1906.aspx)**  | [但果](https://med.szu.edu.cn/Item/401.aspx) | [苏磊](https://med.szu.edu.cn/Item/5890.aspx) |
  | 教授，硕士生导师 | 教授，博士生导师 | 教授，博士生导师 | 教授，博士生导师 | 
  | isaac_zhs@126.com<br>zhanghs@szu.edu.cn | chenxin@szu.edu.cn | danguo@szu.edu.cn | sulei@szu.edu.cn |
  | ![输入图片说明](https://foruda.gitee.com/images/1673353362327610634/c767a37e_5631341.png "屏幕截图") | ![输入图片说明](https://foruda.gitee.com/images/1673353328329698185/03acbf92_5631341.png "屏幕截图") | ![输入图片说明](https://foruda.gitee.com/images/1673353317714181517/c100f19d_5631341.png "屏幕截图") | ![输入图片说明](https://foruda.gitee.com/images/1673353308406342158/cb499c64_5631341.png "屏幕截图") |
  | [孙怡雯](https://med.szu.edu.cn/Item/3691.aspx) | [雷柏英](https://med.szu.edu.cn/Item/7606.aspx) | [陆敏华](https://med.szu.edu.cn/Item/2392.aspx) | [倪东](https://med.szu.edu.cn/Item/1907.aspx) |
  | 教授，博士生导师<br>院长助理 | 特聘教授，博士生导师 | 教授，博士生导师 | 教授，博士生导师 | 
  | ywsun@szu.edu.cn | leiby@szu.edu.cn | luminhua@szu.edu.cn | nidong@szu.edu.cn |
  | ![输入图片说明](https://foruda.gitee.com/images/1673353282890492020/e8f74dc2_5631341.png "屏幕截图") | ![输入图片说明](https://foruda.gitee.com/images/1673353267129482146/cdfbf514_5631341.png "屏幕截图") | ![输入图片说明](https://foruda.gitee.com/images/1673353233850870177/3279f463_5631341.png "屏幕截图") | |
  | [彭珏](https://med.szu.edu.cn/Item/1735.aspx) | [钱建庭](https://med.szu.edu.cn/Item/474.aspx) | [叶继伦](https://med.szu.edu.cn/Item/402.aspx) |
  | 教授，博士生导师<br>，校科研处副处长 | 教授，硕士生导师 | 教授，硕士生导师 | 
  | erica@szu.edu.cn | c.t.chin@szu.edu.cn | Yejilun@126.com
  | | ![输入图片说明](https://foruda.gitee.com/images/1673353135782743882/f98e53cb_5631341.png "屏幕截图") | ![输入图片说明](https://foruda.gitee.com/images/1673353136965454146/4a3cfaf0_5631341.png "屏幕截图") | |
  | | [许改霞](https://bme.szu.edu.cn/20181/0920/71.html) | [杨华艳](https://bme.szu.edu.cn/20211/1119/90.html) | |
  | | 教授，博士生导师 | 特聘教授，博士生导师 | |
  | | xugaixia@szu.edu.cn | yanghuayan@szu.edu.cn | |

- [生物医学工程（Biomedical Engineering，简称BME）](https://baike.baidu.com/item/生物医学工程/660948) （一级学科）

是结合物理、化学、数学和计算机与工程学原理，从事生物学、医学、行为学或卫生学的研究；提出基本概念，产生从分子水平到器官水平的知识，开发创新的生物学制品、材料、加工方法、植入物、器械和信息学方法，用于疾病预防、诊断和治疗，病人康复，改善卫生状况等目的

学科：学科物理、化学、数学和计算机

生物醫學工程（Biomedical engineering）是一门年轻的交叉学科，与生物工程密切相关；其主要特点是运用工程学和应用科学的知识和技术解决生物学和医学领域的科学问题，充分研究生命系统及其行为，以及开发相关的生物医学系统和设备，最终帮助患者得到更好的照料以及提高健康个体的生活质量。

参考: [zh.wikipedia.org/zh-hans/生物医学工程](https://zh.wikipedia.org/zh-hans/生物医学工程)

 **生物医学工程** 

[中国普通高等学校本科专业](https://baike.baidu.com/item/生物医学工程/24611522)

| 中文名 | 生物医学工程 | 外文名 | Biomedical Engineering | 
|---|---|---|---|
| 专业代码 | 082601 | 专业层次 | 本科 | 
| 学科门类 | 工学 | 专业类别 | 生物医学工程类 | 
| 修业年限 | 四年或五年 | 授予学位 | 工学或理学学士 |

- [生物医学工程：科学防范疾病，提升健康水平](http://baike.baidu.com/l/Nk0rm4np) 秒懂百科

- [陈昕，深圳大学生物医学工程学院，副院长](https://bme.szu.edu.cn/20161/0301/43.html) （[转载](https://gitee.com/cylina/siger/issues/I69UJW#note_15712446_link)）

  - [个人主页](http://scholar.acagrid.com/56247) （[转载](https://gitee.com/cylina/siger/issues/I69UJW#note_15712640_link)）

    1. 陈昕 , 谢勤岚 , 董磊 ,等 .医用DSP开发实用教程―基于TMS320F28335[M]. 北京: 电子工业出版社, 2020
    3. 陈昕 .现代医学电子仪器原理与设计实验[M]. 北京: 电子工业出版社, 2020
    4. 陈昕 .STM32F1开发标准教程[M]. 北京: 电子工业出版社, 2020
    5. 陈昕 .电路设计与制作实用教程（Allegro版）[M]. 北京: 电子工业出版社, 2020
    6. 陈昕 .电路设计与制作实用教程（PADS版）[M]. 北京: 电子工业出版社, 2019
    8. 陈昕 .电路设计与制作实用教程（Altium Designer版）[M]. 北京: 电子工业出版社, 2019
    9. 陈思平 , 陈昕 , 沈圆圆 ,等 .[中国科研信息化蓝皮书](https://gitee.com/cylina/siger/issues/I69UJW#note_15712894_link)（信息技术在医学超声工程领域的应用）[M]. 北京: 电子工业出版社, 2018
    7. 陈思平 , 陈昕 , 沈圆圆 ,等 .China's e-Science Blue Book 2018 (Application of Information Technology in Medical Ultrasound Engineering)[M]. 北京: Springer Nature, 2019
  
  - 联系我们 Contact US

    深圳市南山区深圳大学丽湖校区A2楼

    - 陈昕：chenxin@szu.edu.cn
    - 沈圆圆：shenyy@szu.edu.cn
    - 林浩铭：linhaomail@163.com

    <p><img height="199px" src="https://foruda.gitee.com/images/1673372652902951359/c6e6a303_5631341.png"></p>

  - [功能超声诊疗实验室 Functional Ultrasound Theranostics LAB](http://group.acagrid.com/1458) （[转载](https://gitee.com/cylina/siger/issues/I69UJW#note_15712956_link)）

    <p><img width="199px" src="https://foruda.gitee.com/images/1673371089868659306/d05cbd26_5631341.png"></p>

  - 8. 詹凯 , 覃正笛 , 陈昕 ,等 .[一种FPGA输出引脚复用电路、方法及设备](https://www.xjishu.com/zhuanli/55/CN104750481.html)[P]. 中国专利 :2018-04-17 （[转载](https://gitee.com/cylina/siger/issues/I69UJW#note_15713791_link)）

    【IPC分类】G06F9-44  
    【公开号】CN104750481  
    【申请号】CN201510105117  
    【发明人】詹凯, 覃正笛, [陈昕](https://bme.szu.edu.cn/20161/0301/43.html), [彭珏](https://bme.szu.edu.cn/20161/0301/36.html)    
    【申请人】深圳大学  
    【公开日】2015年7月1日  
    【申请日】2015年3月10日

    - 天眼查：深圳大学 [一种FPGA输出引脚复用电路、方法及设备](https://zhuanli.tianyancha.com/0906792ecde34ab2ba3b9986462a85ca)

      - [硕士生导师](https://bme.szu.edu.cn/g/jiaoxue/44/68/index.html)：医学超声方向：陈思平、彭珏、陈昕、 **覃正笛** 、钱建庭、刁现芬、沈圆圆、陆敏华、张新宇、孙怡雯、胡亚欣

      - [詹凯 - 教师 - 深圳大学医学部](https://cn.linkedin.com/in/凯-詹-899931b9) | LinkedIn

- [Verasonics 开放式多通道超声研究平台](http://pdlab.nuaa.edu.cn/2019/0411/c8445a154261/page.htm) （[转载](https://gitee.com/cylina/siger/issues/I69UJW#note_15713197_link)）

  - [Verasonics – The Leader in Research Ultrasound](https://verasonics.com/about-verasonics/) （[转载](https://gitee.com/cylina/siger/issues/I69UJW#note_15713234_link)）

    - 典型用户 > 中国医学科学院生物医学工程研究所 > 高频版多通道研究超声平台

      https://www.instrument.com.cn/typical/ins_86508.html

      中国医学科学院生物医学工程研究所  天津市 高校

      > 中国医学科学院生物医学工程研究所拥有电磁兼容性抗扰度测试系统、多功能材料表面测试分析系统、低频超声研究平台、时间相关小信号前置测量系统、激光扫描共聚焦显微镜、表面等离子共振折射仪、等温滴定微量量热仪（ITC）、多通道神经元信号与峰电位分类采集处理分析系统、数控加工中心、高频经颅磁刺激系统等科研仪器。

  - [Verasonics系统用户主要研究方向及部分参考文献](https://blog.csdn.net/mm19950104/article/details/80702271) （[转载](https://gitee.com/cylina/siger/issues/I69UJW#note_15713499_link)）

  - [多通道可编程超声研究平台](https://ss.bjmu.edu.cn/Html/Mobile/Articles/3237.html) 北京大学口腔医学院 https://ss.bjmu.edu.cn/ [实验室设备介绍](https://gitee.com/cylina/siger/issues/I69UJW#note_15713530_link) 2020-07-24

    - [分子医学研究所“高频多通道可编程超声成像平台”通过可行性论证【18-128】](https://www.lab.pku.edu.cn/gzdtt/315792.htm) （[摘要](https://gitee.com/cylina/siger/issues/I69UJW#note_15713660_link)）
  
  - [多通道可编程超声数据采集处理系统](https://eshare.shanghaitech.edu.cn/equipment/21945) [上海科技大学 首页 仪器列表](https://gitee.com/cylina/siger/issues/I69UJW#note_15713564_link)

  - [256通道超声研究平台（JX180069）单一来源采购公示](https://oldwww.snnu.edu.cn/info/1119/22040.htm) [陕西师范大学物资设备采购招标管理办公室](https://gitee.com/cylina/siger/issues/I69UJW#note_15713688_link)

- [开放式超声研究平台](http://www.worldwide-china.com/productDetails.php?serial=60) [上海埃飞科技](https://gitee.com/cylina/siger/issues/I69UJW#note_15713600_link) Worldwide Technology(S. H)

  ![输入图片说明](https://foruda.gitee.com/images/1673376848535535674/b8930a4b_5631341.png "屏幕截图")

  - [产品展示](http://www.worldwide-china.com/product.php?sortserial=114&secondserial=115) （[摘要](https://gitee.com/cylina/siger/issues/I69UJW#note_15713624_link)）

  - [PLC工程师 [ 上海 ]](http://www.worldwide-china.com/recruit.php) （[摘要](https://gitee.com/cylina/siger/issues/I69UJW#note_15713641_link)）

- [深圳大学医学部2018级生物医学工程3班团活动](https://www.bilibili.com/video/BV17U4y1t7rh/) - [CommittedWu](https://space.bilibili.com/383311925) 

  > 为响应活力在基层 “主题团日” 竞赛活动 2021.4.27
    - 2018级生物医学工程3班 开展了学史知识竞赛 
    - 同学们积极参与，踊跃举手。
    - 支书李洁玉上台讲解题目 （25. 1937.7.7 是什么事变，从而全面抗战由此开始）
    - 班长吴灿仪上台讲解题目 （27. ）
    - 李嘉强同学（30. 1942.5 ，）
    - 刘文山（31. 1965 年，我国科学家在世界上首次人工合成结晶 ？ 岛素，集中表现了我国科学技术达到的新水平）
    - 蔡清宜（3. ）
    - 何敏洁
    - （5. ）
    - 龚悦（6. ）
    - 张静宜（9. ）
    - 陈子扬 （13.）
    - 李凯升 （32.）
    - 吴煜轩 （33. 第一任上海市市长是谁？）
    - 吴申奥 （34. ）
    - 张紫莹 （2. ）

    <p><img height="99px" src="https://foruda.gitee.com/images/1673404487375068256/8ce3432d_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1673404504060908738/0fb4563f_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1673404621724966753/b322fa51_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1673404751608486130/6dd7c3c5_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1673404815156316535/7a14a011_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1673404894748660941/76cfbddf_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1673405015011514164/53c17649_5631341.png"></p>

    - 班主任 王毅 （1. 我国设立的经济特区：C 深圳，珠海，汕头，厦门，海南 ）
    - 合影
    - 李嘉强同学参加疫苗接种义工活动
    - 陈世霖同学积极参加实验室科研项目
    - 张伟楠同学参与龙华区一方城图书馆义工
    - 张伟楠同学参与禾正医院疫苗接种义工
    - 吴灿仪同学观看《榜样5》

  - [2023深圳大学生物医学工程视频考研经验导学课第一名](https://www.bilibili.com/video/BV1DY4y137zi/)

    <img height="99px" src="https://foruda.gitee.com/images/1673458562620463837/6e5db9df_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1673458548785981612/60759fa5_5631341.png"></p>

    正路还是 深圳大学生物医学工程 （专硕和学硕），工科比学医更过硬。  

  [他校视频](https://gitee.com/cylina/siger/issues/I69UJW#note_15719136_link)：

  - [广东医科大学 生物医学工程学院 生物医学工程、智能医学工程专业解读](https://www.bilibili.com/video/BV1t5411g7Rv) [广东医科大学招生办](https://space.bilibili.com/593001962)
  - [【学院零距离】南方医科大学-生物医学工程学院2021招生宣讲](https://www.bilibili.com/video/BV1tX4y1A7mN) （[摘要](https://gitee.com/cylina/siger/issues/I69UJW#note_15720550_link)）
  - [生物医学工程师和人造器官](https://open.163.com/newview/movie/free?pid=M6GJ2B1NR&mid=M6GJ45PEK) 网易公开课：耶鲁大学。 （[摘要](https://gitee.com/cylina/siger/issues/I69UJW#note_15721202_link)）
  - [这个“冷门”专业，毕业后年薪百万：生物医学工程](https://www.bilibili.com/video/BV1vZ4y1d713) （[摘要](https://gitee.com/cylina/siger/issues/I69UJW#note_15721297_link)）

- [深大生物医学工程学院](https://space.bilibili.com/644323919/video) （[笔记摘要](https://gitee.com/cylina/siger/issues/I69UJW#note_15721778_link)）

  - 深圳大学 医学部 https://med.szu.edu.cn/Default.aspx

  - [生物医学工程 本科招生](https://gitee.com/cylina/siger/issues/I69UJW#note_15740942_link) 转载

  - （[公号转载](https://gitee.com/cylina/siger/issues/I69UJW#note_15743992_link)）

    - [在深圳大学就读生物医学工程专业是一种什么样的体验？](https://mp.weixin.qq.com/s?t=pages/video_detail_new&scene=1&vid=wxv_1441231737589399555&__biz=MzUyOTYzOTc4MQ==&mid=2247487161&idx=1&sn=54cde6a5b0ec2b18d01ccb675dd5bbf3) 【[BME宣传片：丽湖校区](https://www.bilibili.com/video/BV17z4y1Q7ks/)】
    - [听在读青年人谈个人成长 | 黄斌 ：“从粤海到丽湖，与深大生物医学工程学科共成长”](http://mp.weixin.qq.com/s?__biz=MzUyOTYzOTc4MQ==&mid=2247488370&idx=3&sn=6c61729b08108d9ecb9dc3065ffa99b6) （[转载](https://gitee.com/cylina/siger/issues/I69UJW#note_15744481_link)）
    - [生物医学工程学院科研团队近期取得系列新进展](http://mp.weixin.qq.com/s?__biz=MzUyOTYzOTc4MQ==&mid=2247491406&idx=4&sn=f570df468504ad0158e1a412705f1215) （[转载](https://gitee.com/cylina/siger/issues/I69UJW#note_15744243_link)）
    - [我院多位学者入选全球前2%顶尖科学家榜单（2022版）](http://mp.weixin.qq.com/s?__biz=MzUyOTYzOTc4MQ==&mid=2247491406&idx=2&sn=f59afe10997938f228a51a9bc7f9dab6)

- [本期专题封面](https://gitee.com/cylina/siger/issues/I69UJW#note_15746734_link)

  - [访谈提纲](https://gitee.com/cylina/siger/issues/I69UJW#note_15751540_link)