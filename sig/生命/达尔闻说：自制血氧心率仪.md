- [【拆解】爆红血氧仪解密，100多块钱值得花么](https://www.bilibili.com/video/av968199443) [达尔闻](https://space.bilibili.com/430777205)
- [爆红血氧仪拆解揭秘（含原厂方案分享）](https://mp.weixin.qq.com/s?__biz=Mzg5MDIwNjIwMA==&mid=2247488557&idx=1&sn=34385639cc5719435e16872fee269d00)
- [毕业设计| 自制简易血氧心率仪STM32+MAX30100](https://www.bilibili.com/read/cv6155810) - 哔哩 …

<p><img width="706px" src="https://foruda.gitee.com/images/1675938471188656042/d5ae6b0e_5631341.jpeg" title="529达尔闻说副本.jpg"></p>

> 当我看到这篇2020年初的血氧仪推介的时候，对达尔闻说的老师产生了由衷的敬意，这可是第五版防治规范中强调的啊，在那个被保护的岁月，一线抗议的指南距离我们是如此遥远，直到疫情结束前，顶着40度的高烧，我们才意识到健康来之不易。春节前调研血氧仪DIY的选型就是 MAX30102，这现成的毕设参考已经早早成型啦。联系到了妮姐，获得了转载许可，第一时间成刊，与同学们分享。（下面是从视频开始，一路考古到公号，再到 MAX30102，其中邱海波老师的一期央视访谈会单独出一期。满满的医者仁心 :pray: ）

邱海波 （《生命线》 [20200118 邱海波——生命的守门人](https://tv.cctv.com/2020/01/18/VIDEZvcSVmlXhYOCp52v6Svx200118.shtml)）
- 东南大学附属中大医院副院长
- 国家重症医学质量控制中心主任

![输入图片说明](https://foruda.gitee.com/images/1675908133003260696/919bf3cc_5631341.jpeg "达尔闻副本.jpg")

> 第五版的治疗规范里头，我们也特别强调了怎么去早期发现，这特别强调了需氧饱和度的，因为需氧饱和度的监测，有利于我们发现早期变动的表现。

  - 国卫办疾控函〔2020〕156号  **附件** ：[新型冠状病毒肺炎防控方案（第五版）](http://www.gov.cn/zhengce/zhengceku/2020-02/22/5482010/files/310fd7316a89431d977cc8f2dbd2b3e0.pdf) 2020年2月21日

    相关链接：

    1. [解读《新型冠状病毒肺炎防控方案（第五版）》](http://www.nhc.gov.cn/jkj/s3578/202002/dc7f3a7326e249c0bad0155960094b0b.shtml)
    2. [一图读懂：新型冠状病毒肺炎防控方案（第五版）](http://www.nhc.gov.cn/jkj/s3578/202002/dc2f3b1200144ae1a50ffa4e0df62da8.shtml) | [打印版](http://www.nhc.gov.cn/jkj/s3578/202002/dc2f3b1200144ae1a50ffa4e0df62da8/files/b2ab76e9322647bf98d4fee1a4476a7b.rar) （主要说明 四和五两版的差别！）

- [【拆解】爆红血氧仪解密，100多块钱值得花么](https://www.bilibili.com/video/av968199443) [达尔闻](https://space.bilibili.com/430777205)

  1.7万 | 10 | 2020-05-15 17:39:15

  > 2分钟拆解解密爆红血氧仪，这款测量仪使用透射式PPG传感器。

  部分芯片清单：
  - 新唐32位ARM Cortex-M0单片机mini58TDE
  - ET5223M基于亚微米硅栅极CMOS工艺的模拟开关
  - 5050封装贴片LED
  - 硅光电池/光敏二极管

# [爆红血氧仪拆解揭秘（含原厂方案分享）](https://mp.weixin.qq.com/s?__biz=Mzg5MDIwNjIwMA==&mid=2247488557&idx=1&sn=34385639cc5719435e16872fee269d00)

原创 妮mo 达尔闻说 2020-05-20 20:00

![输入图片说明](https://foruda.gitee.com/images/1675869785542687884/2b576bec_5631341.png "屏幕截图")

- 微信号：达尔闻说 Darwinlearns
- 功能介绍：只讲技术不撩汉的小姐姐妮mo与你相遇达尔闻。这里提供技能培训视频，覆盖各细分领域热门话题，如嵌入式，FPGA，人工智能等。针对不同人群量身定制学习内容，如常用知识点，拆解评测，电赛/智能车/考研等！

> 不想错过我的推送，记得右上角-查看公众号-设为星标，摘下星星送给我

疫情期间，为了保命，你买过一些“高科技”产品吗？我就有，比如下面这个100多块钱买的血氧仪——

![输入图片说明](https://foruda.gitee.com/images/1675870554638235952/ee784ec1_5631341.png "屏幕截图")

> 不是为了拆解而买，是为了保命

血氧饱和度是新冠早期病情诊断指标之一。有人自己DIY血氧仪（点击查看DIY方案），而大部分人如我一样购买指夹式血氧仪。

![输入图片说明](https://foruda.gitee.com/images/1675869910045291742/35f8c12c_5631341.png "屏幕截图")

指夹式血氧仪一般由微处理器、存储器(EPROM与RAM)、两个控制LED的数模转换器、对光电二极管接收的信号进行滤波与放大的器件、将接收信号数字化以提供给微处理器的模数转换器等器件组成，LED与光电二极管放置在与患者指尖或耳垂接触的小型探针中。

![输入图片说明](https://foruda.gitee.com/images/1675869919549254892/f6e8a79d_5631341.png "屏幕截图")

我们这个血氧仪微处理器使用的是新唐32位Arm Cortex-M0单片机mini58TDE，透射式PPG传感器。这类血氧仪方案市场上有很多，比如我们熟知的半导体大厂每家都有，这也侧面说明厂商对健康监控市场的重视。下面给大家简要分享他们的血氧仪方案，如果想了解更多，可以至他们的官网搜索：

### 1 瑞萨电子

瑞萨电子血氧仪解决方案搭载的高集成度传感器OB1203，业界最小的光学生物传感器模块，具有用于反射性光容积描记的全集成生物传感器，通过适当的算法可以确定心率、血氧饱和度（SpO2）、呼吸频率和心率变异性。OB1203将光源和驱动器集成在一个单一的光学优化包中。主单片机RA2A1可以通过IIC通信直接获取PPG数据。

![输入图片说明](https://foruda.gitee.com/images/1675869933879744376/2331642b_5631341.png "屏幕截图")

### 2 德州仪器

德州仪器脉动式血氧计方案的AC/DC适配器子系统将交流电源线转换为隔离的稳压直流输出；传感器前端子系统驱动红色/IR led，并将光电二极管的输入转换为缓冲的、获得的电压信号，以便系统处理器对其进行处理。同时还包含全套 BLE 连接设计，可轻松连接到已启用 BLE 的智能手机、平板电脑等设备。

![输入图片说明](https://foruda.gitee.com/images/1675869946505758567/f8748663_5631341.png "屏幕截图")

### 3 恩智浦

恩智浦提供超低功耗MCU，支持LCD显示器，适用于便携式脉搏血氧仪，可跟踪病人血液中的氧含量。

![输入图片说明](https://foruda.gitee.com/images/1675869959131544351/4a6af5f8_5631341.png "屏幕截图")

这些器件提供了一种简单的非侵入式方法，用于监测血液中氧饱和血红蛋白百分比。LCD显示屏上会显示血氧饱和度及心率。

### 4 亚德诺

ADI公司为脉搏血氧仪设计提供种类齐全的高性能线性、混合信号、MEMS和数字信号处理技术。

![输入图片说明](https://foruda.gitee.com/images/1675869971684335584/8db918d5_5631341.png "屏幕截图")

脉搏血氧仪包括发射路径、接收路径、显示和背光、数据接口以及音频报警。发射路径包括红光LED、红外光LED和用于驱动LED的 DAC。接收路径包括光电二极管传感器、信号调理、模数转换器和处理器。 
从上面的血氧仪框图可以看出，除了微控制器和传感器之后，外围电路都有和模电相关的电路，比如运放电路。不止是血氧仪，很多项目都会设计到模拟电路，也是很多人学习的难点，给大家推荐一门学习课程——西安交通大学杨建国老师的《新概念模拟电路》全5册视频课程：

[![输入图片说明](https://foruda.gitee.com/images/1675870361450860019/15f6bfc4_5631341.png "屏幕截图")](https://www.bilibili.com/video/BV1hC4y1H7e8)

> 课程福利：价值500元+限量未发行纸质版《新概念模拟电路》

END

### 达尔闻实验室系列：

- [实验室日常|在这里刚诞生了挑战杯国一](http://mp.weixin.qq.com/s?__biz=Mzg5MDIwNjIwMA==&mid=2247486544&idx=1&sn=6b40ee7b5a67a0f005b8c4368cf103f1)
- [这个实验室，有你梦寐以求的全部样子](http://mp.weixin.qq.com/s?__biz=Mzg5MDIwNjIwMA==&mid=2247483769&idx=1&sn=777ce63640f9b35dd25aa918e8cbd12f)！
- [上帝视角，暴力拆评近万元的大疆御2专业版无人机](http://mp.weixin.qq.com/s?__biz=Mzg5MDIwNjIwMA==&mid=2247487423&idx=1&sn=ef408c07b5767c48d30b545085c294cb)
- [6款重磅级MCU板卡吐血推荐！](http://mp.weixin.qq.com/s?__biz=Mzg5MDIwNjIwMA==&mid=2247484107&idx=1&sn=843185e17b501e5af36ffe7018cfaa00)
- [暴拆奔驰大灯，揭秘黑科技](http://mp.weixin.qq.com/s?__biz=Mzg5MDIwNjIwMA==&mid=2247483691&idx=1&sn=aa85c33bf614eeb45196463413be73ea)
- [开箱首发！树莓派4到手实测](http://mp.weixin.qq.com/s?__biz=Mzg5MDIwNjIwMA==&mid=2247483817&idx=1&sn=b87d958cb12460d2abb3ac28d6449708)
- [“电子垃圾”真的是垃圾？](http://mp.weixin.qq.com/s?__biz=Mzg5MDIwNjIwMA==&mid=2247483988&idx=1&sn=793805d143304f10c745e3420b9caf2a)
- [火爆B站！iPhone SE改装成“台式机”背后的创意解析](http://mp.weixin.qq.com/s?__biz=Mzg5MDIwNjIwMA==&mid=2247488496&idx=1&sn=3793ae9f112cee0c06170b3882308205)

### 推荐阅读：

[项目分享](https://mp.weixin.qq.com/mp/appmsgalbum?action=getalbum&album_id=1319058823776223232&__biz=Mzg5MDIwNjIwMA==#wechat_redirect) | [电赛系列](https://mp.weixin.qq.com/mp/appmsgalbum?action=getalbum&album_id=1319405847352623105&__biz=Mzg5MDIwNjIwMA==#wechat_redirect) | [人工智能](https://mp.weixin.qq.com/mp/appmsgalbum?action=getalbum&album_id=1319438607601549313&__biz=Mzg5MDIwNjIwMA==#wechat_redirect) | [考研](https://mp.weixin.qq.com/mp/appmsgalbum?action=getalbum&album_id=1319415507438714881&__biz=Mzg5MDIwNjIwMA==#wechat_redirect) | [必考知识点](https://mp.weixin.qq.com/mp/appmsgalbum?action=getalbum&album_id=1319391068873474048&__biz=Mzg5MDIwNjIwMA==#wechat_redirect) | [毕业设计](https://mp.weixin.qq.com/mp/appmsgalbum?action=getalbum&album_id=1319424187802501122&__biz=Mzg5MDIwNjIwMA==#wechat_redirect) | [开关电源](http://mp.weixin.qq.com/s?__biz=Mzg5MDIwNjIwMA==&mid=2247488140&idx=1&sn=35ac6cb85395f4b927bd9d675bae25f5&chksm=cfe1701bf896f90dc91a4301fa812ca167370501a7d04c92cb65067da6a1f74f3c0778bcbeb4&scene=21#wechat_redirect) | [求职](https://mp.weixin.qq.com/mp/appmsgalbum?action=getalbum&album_id=1319397476696244225&__biz=Mzg5MDIwNjIwMA==#wechat_redirect)

我们是妮mo，达尔闻创始人，只讲技术不撩汉的小姐姐。达尔闻在线教育平台旨在服务电子行业专业人士，提供技能培训视频，覆盖各细分领域热门话题，比如嵌入式，FPGA，人工智能等。并针对不同人群量身定制分层级学习内容，例如常用知识点，拆解评测，电赛/智能车/考研等，欢迎关注。

- 官网：www.darwinlearns.com
- B站：达尔闻
- QQ群：群1：786258064（已满）群2：1057755357

![输入图片说明](https://foruda.gitee.com/images/1675870069620936747/dc525e41_5631341.png "屏幕截图")

阅读原文

---

- [毕业设计| 自制简易血氧心率仪STM32+MAX30100](https://www.bilibili.com/read/cv6155810) - 哔哩 …
  https://www.bilibili.com/read/cv6155810

  > WebMay 21, 2020 · 测量传感器使用的是MAX30100，能够读取心率、血氧的传感器，通信方式是通过IIC进行通信。. 两个发光二极管，一个光检测器， …

  Estimated Reading Time: 3 mins

  - [MAX30102心率血氧显示例程-STM32F103C8T6-C语言-裸机](https://bbs.csdn.net/topics/392366795) … | bbs.csdn.net

    - 黑黑333333 2018-04-26 09:03:46

      > MAX30102心率血氧显示例程，keil-MDK，C语言，裸机代码，包含计算心率血氧的算法。移植自美信官方例程。最近需要用stm32做心率血氧测试，找了下要么是只有芯片驱动没有算法，要么是美信的官方例程，而官方例程是用C++写的，还跑了个叫mbed的操作系统......非常不爽，弄了一下午，移植成功，特地分享上来，觉得好的给个好评！ 接线方式：PB9-SDA,PB8-SCL,PB7-INT,PA9/PA10是串口TX/RX，波特率设置为115200 注意：网上的PA2/PA3引脚为串口传输的都是美信官方例程！用C++写的，还带个mbed的操作系统。移植不易，如果对你有帮助，记得给个好评！相关下载链接：//download.csdn.net/download/a435262767/10374403?utm_source=bbsseo

  - [基于STM32的MAX30100心率计设计 - 代码天地](https://www.codetd.com/article/10433389) | codetd.com
  - [用MAX30102制作一个血氧及心率测量仪 - 知乎](https://zhuanlan.zhihu.com/p/496413803) | zhuanlan.zhihu.com
  - [基于单片机MAX30100血氧、心率检测系统设计-整套资料](https://blog.csdn.net/DIY_lOVER/article/details/112143370) ... | blog.csdn.net

    - DIY_lOVER 于 2021-01-04 16:58:56 发布 4436  收藏 61 文章标签： 单片机

      > 基于美信MAX30100/MAX30102传感器设计的血氧和心率检测，以单片机STM32F103CBT6为主控核心，其中涉及电源模块、心率血氧模块、蓝牙模块。电源模块是提供所有系统的供电，心率血氧是整个系统功能的实现，MCU读取心率血氧信号，分析数据，蓝牙模块是实现无线的一种方式，连接外部和单片机。

      > 本设计可以通过手机蓝牙连接。测量者可将手指放于被测处，测得的数据可在单片机上直观显示或者是电脑显示。在数据显示的问题上，本设计采用了二种方法，一种是手机蓝牙连接，利用手机蓝牙连接硬件电路的蓝牙模块，通过蓝牙传输数据送往手机显示，第二种是单片机液晶屏显示，被测者可在液晶屏上直接获取自己的血氧脉搏的数据。 最终，被测者可获得自己的心率和血氧值。

        【资源下载】下载地址如下（935）：https://docs.qq.com/doc/DTlRSd01BZXNpRUxl

        <img height="199px" src="https://foruda.gitee.com/images/1675910825416362440/f9f2975a_5631341.png"> <img height="199px" src="https://foruda.gitee.com/images/1675910835013868663/75ebc2f4_5631341.png">

  - [STM32学习值传感器篇——max30102心率血氧传感器](https://www.codetd.com/article/10648772) - 代码 ... | codetd.com | 2020-04-10 23:15:45 

    > 这个传感器让我了解了很多，以前使用的单片机，基本没有遇见过堆栈溢出的问题，这个传感器让我遇到了，在此记录调试心得。

    首先把所有驱动文件都贴出来了

    - 心率血氧算法代码：algorithm.h · algorithm.c
    - max30102驱动代码：max30102.h · max30102.c
    - iic驱动代码：max_iic.h · max_iic.c
    - main.c 

    //上边给出的算法代码是原始的，修改地方已标注

    > 这个传感器整体来说不算挺难，不过我在这个上边耗费了不小的时间，主要有一个很大的坑就是它的算法程序定义了大概有5个 uint32_t   aun_ir_buffer[500] 这样类似的数组，我用的stm32l151c8t6的传感器，局部数组太大导致一直程序一烧进去直接卡死在hardfault_handler（）这个中断里边  程序根本跑不动，因为mcu已经确定不能换了，只能通过其他方法改善这个问题，刚开始使用串口打出来aun_ir_buffer[500]这些数组里边的 数发现他们都大于65536，好像也不能将uint32_t改为uint16_t,于是改了其他感觉可以改的uint32_t的变量，程序此刻是可以跑起来了，但是程序总是运行到一半的时候就又卡死在hardfault_handler（），头疼了好几天，今天灵机一动，给它们两个主要占地的（（aun_ir_buffer[500]，aun_red_buffer[500]））同时减80000再给他赋值，等到用这个数据的时候再给他加80000，这样数据就可以用uint16_t定义了，也不会出现溢出的情况 ，根据此思路测试了一下数据出来了可是测出来的心率值比实际的大了100左右（用小米手环和max30102同时测手指数据），目前不知道是什么问题，就给了一个最直接的办法，把解算出来的心率只都减100，心率基本和小米手环测出来的只有微小差异。（感觉 **有点投机** ，以后有时间再好好研究一下）

# [毕业设计| 自制简易血氧心率仪STM32+MAX30100](https://www.bilibili.com/read/cv6155810)
2020-05-22 10:34 · 2776 阅读 · 73喜欢 · 8评论

<img height="32px" src="https://foruda.gitee.com/images/1675908485464600275/6281f285_5631341.png"> 达尔闻  粉丝：16.0万 文章：71 <img height="32px" src="https://foruda.gitee.com/images/1675908479688594258/d0366362_5631341.png"> 知名偶像 所有自制视频总播放数>=100万 稀有勋章  

![输入图片说明](https://foruda.gitee.com/images/1675908906731998881/12f3506d_5631341.png "屏幕截图")

> 为了方便同学们查找相关的毕业设计方案，达尔闻助力毕业生新增毕业设计方案专区，并征集毕业设计项目，愿意分享的小伙伴可联系妮姐（微信：459888529）。

> “达尔闻说”微信每周四更新毕设方案，可以关注“达尔闻说”微信，找方案不迷路~~同时，欢迎加入达尔闻毕业设计交流qq群：1081905597 

还记得前不久，有公司开源了呼吸机的源文件，火爆朋友圈。虽然有了源文件，DIY呼吸机还没成功案例。呼吸机就别做了，来看看这个一样可以监测健康的血氧心率仪——

感谢达尔闻粉丝 **张东（B站ID：张咚咚171）** 分享他自己的制作，我们借花献佛把这个设计分享给DIY爱好者以及毕业生们。

### 血氧心率仪功能

传感器监测到数据之后，在屏幕上以曲线加数字的方式显示出检测者的血氧和心率数据。同时LED指示灯和蜂鸣器根据心率数据闪烁和发出声音。如果未监测到数据，蜂鸣器常响警报。

血氧心率仪的STM32代码及配置可以复制链接至浏览器打开：   **https://darwinlearns.com/productinfo/153091.html** 

### 硬件组成

主控芯片使用的是大家用的最多的MCU——STM32F103C8T6，ARM Cortex-M 内核32位，程序存储器容量是64KB，内部还集成了模数转换器A/D 10*12b。

测量传感器使用的是MAX30100，能够读取心率、血氧的传感器，通信方式是通过IIC进行通信。两个发光二极管，一个光检测器，优化光学和低噪声的仿真信号处理，以检测脉搏血氧饱和度和心脏速率信号。只需要将手指头紧贴在传感器上，就能估计脉搏血氧饱和度(SpO2)及脉搏(相当于心跳)。但是现在MAX30100已经停产了，可以使用MAX30102替代。

![输入图片说明](https://foruda.gitee.com/images/1675909949145341506/991461aa_5631341.png "屏幕截图")

显示心跳和血氧波形的屏幕是1.44 TFT屏，控制芯片是ST7735。屏幕通信接口方式采用SPI，而且只用到3个IO口：CLK、SDI（DIN）、RS（D/C），其中寄存器/数据选择信号RS（D/C）为0时，SPI数据总线发送的是指令，为1时数据总线发送的是像素数据。1.44 TFT屏其他接口是这样配置：片选CS信号直接精简接到地上，保持常低；1.44 TFT屏 LED背光控制信号接高电平3.3V，背光常亮；RST复位信号可以接STM32的复位，利用系统上电复位。

![输入图片说明](https://foruda.gitee.com/images/1675909957186322045/7afd3d6d_5631341.png "屏幕截图")

其他硬件有，当数据异常时，蜂鸣器发出警报声。有数据时，LED指示灯随着脉搏跳动。系统直接使用microusb供电即可。

![输入图片说明](https://foruda.gitee.com/images/1675909964501377685/a4bc4a5e_5631341.png "屏幕截图")

这个系统虽说功能做的挺好，但是在硬件上有个问题，按的用力或者角度有问题，数据就不准。如果想要改善的话，就需要在传感器上盖一块玻璃。

### 软件配置 软件是基于STM32标准库开发：

1. 1.44 FTF屏幕上显示的心率数据是返回AD经过FFT出来的，而波形是AD数据通过DCfilter做比例和偏置出来。

2. 通过配置STM32定时器PWM控制蜂鸣器和LED随着心脏跳动而工作一次。

3. 每次缓冲区读满就会进行FFT算心率，屏幕右上角的FFT就会闪烁一下。

![输入图片说明](https://foruda.gitee.com/images/1675910000608984007/4f61b0a7_5631341.png "屏幕截图")

血氧心率仪的STM32代码及配置可以复制链接至浏览器打开：  
 **https://darwinlearns.com/productinfo/153091.html** 

> 血氧心率仪的STM32代码及配置：[点击下载](https://darwinlearns.com/filedownload/205754)

### 毕设系列：

- 基于STM32实现的温度/心率/步数设计
- 智能自动寻光循迹灭火小车
- 低成本打造STM32 IoT便携式功率计
- STM32+OV7670设计车牌识别系统
- 自制手机app与arduino实现智能监测控制系统
- STM32F103+NB模组+MQTT实现物联网采集系统
- 7天完成手势控制ESP32 WIFI电子相册 
- 自制STM32万用表，吊打500元正规牌

注意：达尔闻提供的完整方案仅供大家参考学习，不鼓励直接套用，要知道你的答辩老师也可能是达尔闻粉丝噢。

![输入图片说明](https://foruda.gitee.com/images/1675910068347423128/8f836380_5631341.png "屏幕截图")

本文为我原创本文禁止转载或摘编  
· DIY · 毕业设计 · STM32 · 血氧仪

---

# [基于STM32的MAX30100心率计设计](https://www.codetd.com/article/10433389)

企业开发 2020-04-03 23:47:18 阅读次数: 0

MAX30100和MAX30102是常用的测量心率的模块，MAX30100是能够读取心率、血氧的传感器，通信方式是通过IIC进行通信。其工作原理是通过红外led灯照射，能够得到心率的ADC值，通过算法处理相应的AD值得心率。

![输入图片说明](https://foruda.gitee.com/images/1675911833861111965/bea3e1b5_5631341.png "屏幕截图")

STM32使用IIC协议访问MAX30100时序如下图，此次使用STM32两个IO口模拟IIC协议，具体IO口见后续程序。

![输入图片说明](https://foruda.gitee.com/images/1675911843791045939/d98d6a9b_5631341.png "屏幕截图")

MAX30100内部功能框图如下图，由图可见，RED和IR照射然后使用ADC进行数据采集，采集后的AD值经过数字滤波器进入数据寄存器，然后使用IIC进行数据采集，在转换AD值时，也可以采集温度值（用于校正）。

![输入图片说明](https://foruda.gitee.com/images/1675911851896905591/62efa83e_5631341.png "屏幕截图")

MAX30100内部全部寄存器如下图，具体每位功能见数据手册（已上传）

![输入图片说明](https://foruda.gitee.com/images/1675911861041230684/55b5d7e3_5631341.png "屏幕截图")

MAX30100FIFO最大可存64B的数据，其中存储数据如下图，第一字节存储IR值高八位，第二字节存储IR值的低八位，第三字节存储RED的高八位，第四字节存储RED的第八位，第五字节存储IR的高八位，依次类推。其中，数据始终左对齐。

![输入图片说明](https://foruda.gitee.com/images/1675911867950413091/64368dae_5631341.png "屏幕截图")

此次设计采用SPO2模式，即需要采集IR和RED值。以下是MAX30100在SPO2的时序图。第一步，IIC开始，设置模式。第二步，时隔29ms温度数据准备好，置相应的中断位。第三步。。。。（每一步具体描述见下图）

![输入图片说明](https://foruda.gitee.com/images/1675911879514279542/b2668a59_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1675911902816226566/20b20bbb_5631341.png "屏幕截图")

使用IIC访问MAX30100时，MAX30100器件地址为0XAE，写操作R/W=0，读操作R/W=1.具体格式如下图。

![输入图片说明](https://foruda.gitee.com/images/1675911912537293335/c7cf9233_5631341.png "屏幕截图")

STM32通过IIC访问MAX30100时，向MAX30100某个寄存器写数据过程如下图。第一步IIC开始，第二步写从机地址+（R/W=0），第三步等待应答，第四步写寄存器地址，第五步等待应答，第六步写代写数据，第七步等待应答，第八步IIC停止。

![输入图片说明](https://foruda.gitee.com/images/1675911923746794754/9f0271ad_5631341.png "屏幕截图")

STM32通过IIC访问MAX30100时，从MAX30100某个寄存器读数据过程如下图。第一步IIC开始，第二步写从机地址+（R/W=0），第三步等待应答，第四步写寄存器地址，第五步等待应答，第六步重新开始，第七步写从机地址+（R/W=1图中有误），第八步等待应答，第九步从寄存器读数据，第十步发送非应答，第十一步IIC停止。

![输入图片说明](https://foruda.gitee.com/images/1675911935090907161/8fe5544b_5631341.png "屏幕截图")

STM32通过IIC访问MAX30100时，从MAX30100的寄存器连续读多个数据过程如下图。

![输入图片说明](https://foruda.gitee.com/images/1675911943309865372/445012a7_5631341.png "屏幕截图")

MAX30100引脚连接：  
SCL连接PB6, SDA连接PB7，VIN连接3.3V，共地，其它引脚可以不用连接。

![输入图片说明](https://foruda.gitee.com/images/1675911959663964084/f7771eaa_5631341.png "屏幕截图")

代码如下：  

- MAX30100头文件：

```
#ifndef __MAX30100_H
#define __MAX30100_H

#include "stm32f10x.h"

typedef struct MAX30100_DEV
{
	double temperature;     //保存MAX30100内部温度
	u16 ir;					//保存MAX30100的IR值
	u16 red; 				//保存MAX30100的RED值
	u8 dat[4];				//暂时保存MAX30100的IR和RED值
	u8 flag;				//接收数据成功标志
}_MAX30100;

#define MAX30100_PORT  GPIOB
#define MAX30100_INT_PIN  GPIO_Pin_4
#define MAX30100_RD_PIN   GPIO_Pin_2
#define MAX30100_IRD_PIN  GPIO_Pin_3

#define MAX30100_INT PBin(4)
#define MAX30100_RD PBout(2)
#define MAX30100_IRD PBout(3)

//MAX30100 Slave Addr
#define Write_ADDR 0XAE
#define Read_ADDR  0XAF
#define RD 0X01
#define WR 0X00
//MAX30100内部寄存器定义
//INTERRUPT
#define Interrupt_Status	0x00//R
#define Interrupt_Enable	0x01//R/W
//FIFO   ALL R/W
#define FIFO_Write_Pointer		0x02
#define FIFO_OverFlow_Counter	0x03
#define FIFO_Read_Pointer		0x04
#define FIFO_Data_Register  	0x05
//CONFIGURATION  ALL R/W
#define Mode_Configuration		0x06
#define SPO2_Configuration		0x07
//RESERVED   0x08 0x0A-0x15 0x8D
#define LED_Configuration		0x09
//TEMPERATURE  ALL R/W
#define Temp_Integer			0x16
#define Temp_Fraction			0x17
//PART ID
#define Revision_ID		0xFE //R
#define Part_ID		    0xFF //R/W  //芯片ID值为0x11

#define INTERRUPT_REG_A_FULL  			(0X01<<7)
#define INTERRUPT_REG_TEMP_RDY  		(0X01<<6)
#define INTERRUPT_REG_HR_RDY  			(0X01<<5)
#define INTERRUPT_REG_SPO2_RDY  		(0X01<<4)
#define INTERRUPT_REG_PWR_RDY  			(0X01<<0)


#define SAMPLES_PER_SECOND 			100	//检测频率

void MAX30100_Init(void);
u8 Read_Chip_ID(void);
double Read_Chip_Temp(void);
void MAX30100_Mode_Configure(void);
void MAX30100_Read_FIFO(void);
void MAX30100_INT_Init(void);
void MAX30100_Read_FIFO(void);
static void MAX30100_Write_One_Byte(u8 address,u8 saddress,u8 w_data);
static void MAX30100_Read_NumData(u8 address,u8 regaddress,u16 ir,u16 red);
#endif

```

- IIC头文件：


```
#ifndef __myiic_H
#define __myiic_H
#include "sys.h"

//IO方向设置
#define SDA_IN()  {GPIOB->CRL&=0X0FFFFFFF;GPIOB->CRL|=(u32)8<<28;}
#define SDA_OUT() {GPIOB->CRL&=0X0FFFFFFF;GPIOB->CRL|=(u32)3<<28;}

#define IIC_PORT 	GPIOB
#define IIC_SCL_PIN GPIO_Pin_6
#define IIC_SDA_PIN GPIO_Pin_7
//IO操作函数	 
#define IIC_SCL    PBout(6) //SCL
#define IIC_SDA    PBout(7) //SDA	 
#define READ_SDA   PBin(7)  //输入SDA 

//IIC所有操作函数
void IIC_Init(void);                				//初始化IIC的IO口				 
void IIC_Start(void);								//发送IIC开始信号
void IIC_Stop(void);	  							//发送IIC停止信号
void IIC_Send_Byte(u8 txd);							//IIC发送一个字节
u8 IIC_Read_Byte(unsigned char ack);				//IIC读取一个字节
u8 IIC_Wait_Ack(void); 								//IIC等待ACK信号
void IIC_Ack(void);									//IIC发送ACK信号
void IIC_NAck(void);								//IIC不发送ACK信号
void IIC_Write_One_Byte(u8 daddr,u8 addr,u8 data);
u8 IIC_Read_One_Byte(u8 daddr,u8 addr);
void IIC_Write_One_Byte_NData(u8 daddr,u8 regaddr);

#endif

```

- MAX30100 .C文件部分函数：


```

/*
*	函数名称：读MAX30100的ID号   
*	参数：	 无  
*	返回值：	 ID值
*/
u8 Read_Chip_ID(void)
{
	u8 dat;//IIC读出                 
	dat=IIC_Read_One_Byte(Write_ADDR,Part_ID);            // 读出芯片ID
	delay_ms(100); 
	return dat;
}
/*
*	函数名称：读MAX30100内部FIFO数据   
*	参数：	 数据保存指针  
*	返回值：	 无
*/
void MAX30100_Read_FIFO(void)
{
	uint16_t temp_num=0;
	temp_num = IIC_Read_One_Byte(Write_ADDR,Interrupt_Status);
	if (INTERRUPT_REG_A_FULL&temp_num)	
	{		MAX30100_Read_NumData(Write_ADDR,FIFO_Data_Register,_max30100.ir,_max30100.red);
	}
}
```

- IICC文件部分：


```
/*
*	函数名称：初始化IIC
*	参数：	  无
*	返回值：	  无
*/
void IIC_Init(void)
{					     
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB,ENABLE);	//使能GPIOB时钟
	   
	GPIO_InitStructure.GPIO_Pin =IIC_SCL_PIN|IIC_SDA_PIN;
	GPIO_InitStructure.GPIO_Mode =GPIO_Mode_Out_PP ;   //推挽输出
	GPIO_InitStructure.GPIO_Speed =GPIO_Speed_50MHz;
	GPIO_Init(IIC_PORT, &GPIO_InitStructure);
	GPIO_SetBits(IIC_PORT,IIC_SCL_PIN|IIC_SDA_PIN); 	//PB6,PB7 输出高
}
```


将采集的数据通过拟合的公式计算出心率，每分钟跳动次数，正常成年人一般是60-100，心情激动的时候偏高一点。采集的数据通过TFT显示屏显示并通过串口打印出来。如下图：

器件ID：0x11(由芯片手册可得)，内部温度22.14度

![输入图片说明](https://foruda.gitee.com/images/1675912182614976164/ede05511_5631341.png "屏幕截图")

本人心率80几（正常）采集的时候有点小激动

![输入图片说明](https://foruda.gitee.com/images/1675912191149245243/82a46a52_5631341.png "屏幕截图")

下一步，通过相应的算法，将数据拟合成波形在显示屏实时显示。

---

![输入图片说明](https://foruda.gitee.com/images/1675912232031779817/86ee9e0c_5631341.png "屏幕截图")

# [用MAX30102制作一个血氧及心率测量仪](https://zhuanlan.zhihu.com/p/496413803)

硬之城Allchips 
> 硬之城提供元器件选型、BOM配齐、PCBA量产等一站式服务。

项目采用MAX30102血氧及心率监视模块、Arduino UNO板、OLED显示器和蜂鸣器，搭建了一个简单的心率（BPM）测量仪。

![输入图片说明](https://foruda.gitee.com/images/1675912340661287411/cae25f7b_5631341.png "屏幕截图")

这里，BPM为每分钟的心跳数，正常人的数字为65-75之间，体育运动员的数字要低些；血氧饱和度(SaO2)对于正常然来说大概为95%。

### 项目物料

本项目所需材料包括：

- Max30102（×1）：这是项目的功能器件，是一款用于可穿戴设备的高灵敏脉冲式血样及心率传感器，具有不同版本，但是只要型号正确即可。

- Arduino UNO（×1）

- OLED 128x32（×1）：低功耗显示器。

- Buzzer （×1）：蜂鸣器

- 面包板（×1）：方便组件安装和连接的线路板。

- 连接线

![输入图片说明](https://foruda.gitee.com/images/1675912378012881298/1f01f01f_5631341.png "屏幕截图")

本项目所用代码来自Sparkfun_MAX3010x示例库，OLED和Buzzer代码根据 "HeartRate" 示例改进而来，这需要用户将手指放在传感器上。

![输入图片说明](https://foruda.gitee.com/images/1675912381199849632/b4940b4b_5631341.png "屏幕截图")

注意，如果将手指放在传感器上，就要保持安静，直到蜂鸣器的“哔哔”声与你的心率节拍同步，或者与OLED动画同步，这时可读出正确的BPM心率读数。

本项目采用４次BPM读数的平均值，因此比较准确。

### 让OLED显示bmp图像

OLED显示的是小的“心形”栅格图（bmp），一旦传感器检测到一次心跳，就立即切换为大点的“心形”栅格图并保持一会儿，这样屏幕就像心跳一样，一闪一闪的，并伴有蜂鸣器的“哔哔”声。

![输入图片说明](https://foruda.gitee.com/images/1675912398912456377/3e5e708f_5631341.png "屏幕截图")

选择希望看到的图走向的格式，如 .png\ .bmp\ .dib等。切记，本项目屏幕尺寸为128x32px，图像尺寸要小一些，为32x32px和24x21px。

![输入图片说明](https://foruda.gitee.com/images/1675912406376611809/37ca7e06_5631341.png "屏幕截图")

下载LCD助手并打开。

![输入图片说明](https://foruda.gitee.com/images/1675912416463893092/4dc69c2a_5631341.png "屏幕截图")

可看到如下"数字"

![输入图片说明](https://foruda.gitee.com/images/1675912423036584648/5c1cde01_5631341.png "屏幕截图")

这就是所谓的代码：

`display.drawBitmap(5, 5, logo2_bmp, 24, 21, WHITE);`

其含义为：

`display.drawBitmap(Starting x pos, Starting y pos, Bitmap name, Width, Height, Color);`

这段代码描述了两件事——“当检测到手指时做什么”，以及“捡到心跳时做什么”。

以下是更新后的 Arduino 代码：

- MAX_BPM_OLED_Buzzer.ino

  > Modified from the SparkFun MAX3010x library

```
/* This code works with MAX30102 + 128x32 OLED i2c + Buzzer and Arduino UNO

* It's displays the Average BPM on the screen, with an animation and a buzzer sound

* everytime a heart pulse is detected

* It's a modified version of the HeartRate library example

* Refer to http://www.surtrtech.com for more details or SurtrTech YouTube channel

*/

#include <Adafruit_GFX.h> //OLED libraries

#include <Adafruit_SSD1306.h>

#include <Wire.h>

#include "MAX30105.h" //MAX3010x library

#include "heartRate.h" //Heart rate calculating algorithm

MAX30105 particleSensor;

const byte RATE_SIZE = 4; //Increase this for more averaging. 4 is good.

byte rates[RATE_SIZE]; //Array of heart rates

byte rateSpot = 0;

long lastBeat = 0; //Time at which the last beat occurred

float beatsPerMinute;

int beatAvg;

#define SCREEN_WIDTH 128 // OLED display width, in pixels

#define SCREEN_HEIGHT 32 // OLED display height, in pixels

#define OLED_RESET -1 // Reset pin # (or -1 if sharing Arduino reset pin)

Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET); //Declaring the display name (display)

static const unsigned char PROGMEM logo2_bmp[] =

{ 0x03, 0xC0, 0xF0, 0x06, 0x71, 0x8C, 0x0C, 0x1B, 0x06, 0x18, 0x0E, 0x02, 0x10, 0x0C, 0x03, 0x10, //Logo2 and Logo3 are two bmp pictures that display on the OLED if called

0x04, 0x01, 0x10, 0x04, 0x01, 0x10, 0x40, 0x01, 0x10, 0x40, 0x01, 0x10, 0xC0, 0x03, 0x08, 0x88,

0x02, 0x08, 0xB8, 0x04, 0xFF, 0x37, 0x08, 0x01, 0x30, 0x18, 0x01, 0x90, 0x30, 0x00, 0xC0, 0x60,

0x00, 0x60, 0xC0, 0x00, 0x31, 0x80, 0x00, 0x1B, 0x00, 0x00, 0x0E, 0x00, 0x00, 0x04, 0x00, };

static const unsigned char PROGMEM logo3_bmp[] =

{ 0x01, 0xF0, 0x0F, 0x80, 0x06, 0x1C, 0x38, 0x60, 0x18, 0x06, 0x60, 0x18, 0x10, 0x01, 0x80, 0x08,

0x20, 0x01, 0x80, 0x04, 0x40, 0x00, 0x00, 0x02, 0x40, 0x00, 0x00, 0x02, 0xC0, 0x00, 0x08, 0x03,

0x80, 0x00, 0x08, 0x01, 0x80, 0x00, 0x18, 0x01, 0x80, 0x00, 0x1C, 0x01, 0x80, 0x00, 0x14, 0x00,

0x80, 0x00, 0x14, 0x00, 0x80, 0x00, 0x14, 0x00, 0x40, 0x10, 0x12, 0x00, 0x40, 0x10, 0x12, 0x00,

0x7E, 0x1F, 0x23, 0xFE, 0x03, 0x31, 0xA0, 0x04, 0x01, 0xA0, 0xA0, 0x0C, 0x00, 0xA0, 0xA0, 0x08,

0x00, 0x60, 0xE0, 0x10, 0x00, 0x20, 0x60, 0x20, 0x06, 0x00, 0x40, 0x60, 0x03, 0x00, 0x40, 0xC0,

0x01, 0x80, 0x01, 0x80, 0x00, 0xC0, 0x03, 0x00, 0x00, 0x60, 0x06, 0x00, 0x00, 0x30, 0x0C, 0x00,

0x00, 0x08, 0x10, 0x00, 0x00, 0x06, 0x60, 0x00, 0x00, 0x03, 0xC0, 0x00, 0x00, 0x01, 0x80, 0x00 };

void setup() {

display.begin(SSD1306_SWITCHCAPVCC, 0x3C); //Start the OLED display

display.display();

delay(3000);

// Initialize sensor

particleSensor.begin(Wire, I2C_SPEED_FAST); //Use default I2C port, 400kHz speed

particleSensor.setup(); //Configure sensor with default settings

particleSensor.setPulseAmplitudeRed(0x0A); //Turn Red LED to low to indicate sensor is running

}

void loop() {

long irValue = particleSensor.getIR(); //Reading the IR value it will permit us to know if there's a finger on the sensor or not

//Also detecting a heartbeat

if(irValue > 7000){ //If a finger is detected

display.clearDisplay(); //Clear the display

display.drawBitmap(5, 5, logo2_bmp, 24, 21, WHITE); //Draw the first bmp picture (little heart)

display.setTextSize(2); //Near it display the average BPM you can display the BPM if you want

display.setTextColor(WHITE);

display.setCursor(50,0);

display.println("BPM");

display.setCursor(50,18);

display.println(beatAvg);

display.display();

if (checkForBeat(irValue) == true) //If a heart beat is detected

{

display.clearDisplay(); //Clear the display

display.drawBitmap(0, 0, logo3_bmp, 32, 32, WHITE); //Draw the second picture (bigger heart)

display.setTextSize(2); //And still displays the average BPM

display.setTextColor(WHITE);

display.setCursor(50,0);

display.println("BPM");

display.setCursor(50,18);

display.println(beatAvg);

display.display();

tone(3,1000); //And tone the buzzer for a 100ms you can reduce it it will be better

delay(100);

noTone(3); //Deactivate the buzzer to have the effect of a "bip"

//We sensed a beat!

long delta = millis() - lastBeat; //Measure duration between two beats

lastBeat = millis();

beatsPerMinute = 60 / (delta / 1000.0); //Calculating the BPM

if (beatsPerMinute < 255 && beatsPerMinute > 20) //To calculate the average we strore some values (4) then do some math to calculate the average

{

rates[rateSpot++] = (byte)beatsPerMinute; //Store this reading in the array

rateSpot %= RATE_SIZE; //Wrap variable

//Take average of readings

beatAvg = 0;

for (byte x = 0 ; x < RATE_SIZE ; x++)

beatAvg += rates[x];

beatAvg /= RATE_SIZE;

}

}

}

if (irValue < 7000){ //If no finger is detected it inform the user and put the average BPM to 0 or it will be stored for the next measure

beatAvg=0;

display.clearDisplay();

display.setTextSize(1);

display.setTextColor(WHITE);

display.setCursor(30,5);

display.println("Please Place ");

display.setCursor(30,15);

display.println("your finger ");

display.display();

noTone(3);

}

}
```

发布于 2022-04-11 10:37

· DIY · 心率计 · 血氧饱和度 ·


- 特青年  01-11

  > 你倒是把血氧调好噻[捂脸]

- Sirius 2022-12-25

  > 你这里只有心率，没有血氧呀[衰]

---

![输入图片说明](https://foruda.gitee.com/images/1675935885097385392/d3ad7293_5631341.jpeg "7470824.jpg")

关于达尔闻

> 达尔闻是兼备前沿电子技术资讯与视频在线教育的学习平台。业务包含“在线教育，行业活动，企业服务”，我们致力于打造海量优质电子类视频课程，涵盖人工智能，物联网，嵌入式，硬件设计等领域，定期组织技术讲座，设计竞赛等形式多样的行业活动，旨在为电子工程师打造一条直通大师的进化之路。

![输入图片说明](https://foruda.gitee.com/images/1675935913485922120/2da247fb_5631341.jpeg "7470683.jpg")

联系达尔闻

> 用户账号、课程观看、课程购买、商务合作均可联系： Email:service@darwinlearns.com  

  | 达尔闻官方微信 | 达尔闻官方QQ群 | 达尔闻B站 |
  |---|---|---|
  | <img width="266px" src="https://foruda.gitee.com/images/1675935785016398398/e3432f77_5631341.png"> | <img width="263px" src="https://foruda.gitee.com/images/1675935792553456835/75ded8a7_5631341.png"> | <img width="239px" src="https://foruda.gitee.com/images/1675935799593439638/30efc246_5631341.png"> |

名师精英 <img align="right" width="69px" src="https://foruda.gitee.com/images/1675935994824399956/8480c867_5631341.png">

- 妮mo：[达尔闻创始人](https://course.darwinlearns.com/p/t_pc/course_pc_detail/video/v_5e0993ab7f513_nshTgqOs)

  > 我们是妮mo，在这里会持续给大家提供电子工程师/学生 最常用的，最需要的知识点。还有暴力拆、重度评，好玩又营养的拆解评测系列

![输入图片说明](https://foruda.gitee.com/images/1675919632254838424/a1d8c67b_5631341.jpeg "达尔闻妮姐和MOMO.jpg")