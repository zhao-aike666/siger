- [交叉学科一瞥 | Journal of Biological Engineering 编辑精选合集](https://news.sciencenet.cn/htmlpaper/2019/3/201932610201866849708.shtm)
- [Emerging leaders in biological engineering](https://www.biomedcentral.com/collections/ELBIOLENG?utm_source=other&utm_medium=other&utm_content=null&utm_campaign=BSCN_2_WX_elbioleng_scinet)
- [The BMC Series journals](https://www.biomedcentral.com/p/the-bmc-series-journals)

<p><img width="706px" src="https://foruda.gitee.com/images/1677428916176923434/23cbc699_5631341.jpeg" title="566BMC.jpg"></p>

> 在过去的十年中，生物工程促进了多学科主题的融合，并在整合生物学原理和工程方法论以应对人类健康和地球健康的巨大挑战方面获得了巨大的成就。作为BMC 生物工程领域中首屈一指的期刊， Journal of Biological Engineering （JBE）近期推出了新的主题合集：“Emerging leaders in biological engineering”，旨在传播这一领域最新的研究进展和为之付出努力的行业先驱。

这就是我要的，没有比这个序言更准确地描述 SIGerBME 的目标和使命了 “拥抱BME，拥抱交叉学科！”

# [交叉学科一瞥 | Journal of Biological Engineering 编辑精选合集](https://news.sciencenet.cn/htmlpaper/2019/3/201932610201866849708.shtm)

原文链接：https://www.biomedcentral.com/collections/ELBIOLENG?utm_source=other&utm_medium=other&utm_content=null&utm_campaign=BSCN_2_WX_elbioleng_scinet

微信链接：https://mp.weixin.qq.com/s/L7bhVlZgaUUwmPj-yutL8w

在过去的十年中，生物工程促进了多学科主题的融合，并在整合生物学原理和工程方法论以应对人类健康和地球健康的巨大挑战方面获得了巨大的成就。作为BMC 生物工程领域中首屈一指的期刊， **_Journal of Biological Engineering_** （JBE）近期推出了新的主题合集：“Emerging leaders in biological engineering”，旨在传播这一领域最新的研究进展和为之付出努力的行业先驱。

![输入图片说明](https://foruda.gitee.com/images/1677386119469732073/1094b9e9_5631341.png "屏幕截图")

来自University of Arkansas 的Raj R. Rao在一篇社论中提到了这一合集的亮点。她谈到，大体上JBE推广的主题领域涵盖：合成生物学与细胞设计、生物分子、细胞和组织工程、生物生产与代谢工程、生物传感器、生态环境工程、生物工程教育等方面。本合集包含的近两年发表的多篇综述文章，均为经过编辑委员会成员推荐的高质量论文。同时，JBE对参与同行评审过程的多个评审员进行了查验。在这个合集的首发文章中，我们重点挑选了旨在呈现生物工程广度和影响力的文章。文章涉及个性化医学、人类疾病发展工程系统、跨物种的联系、细胞-材料相互作用、生物标志物、药物传递和成像生物系统等等。正如Jeong Yeol Yoon博士在2017年发表的一篇社论中强调的那样，生物工程不仅具有包容性，而且显示出其独特的潜力，可以将不同的工程学科连接起来，甚至将其他应用科学学科连接起来，从而融合成一个全新的研究领域。因此JBE新推出的这个两年一届的合集为科学界更多地了解该领域内重大挑战以及行业的前沿动态提供了一个良好的平台，帮助众多思想、概念和观点在这里进一步汇聚、融合！

快来一睹为快吧！

### Protein-based vehicles for biomimetic RNAi delivery

用于仿生RNAi传递的蛋白质载体

Alex Eli Pottash et.al.

Doi:10.1186/s13036-018-0130-7

[点击此处，阅读论文](https://jbioleng.biomedcentral.com/articles/10.1186/s13036-018-0130-7?utm_source=wechat&utm_medium=social&utm_content=organic&utm_campaign=BSCN_2_xw_jbioleng)。

NA干扰（RNAi）技术的成功依赖于有效的传递方法。为此，研究人员开发了多种策略，包括对RNA的化学修饰、病毒和非病毒转染方法，以及与诸如聚合物和脂质纳米粒、工程蛋白质和天然蛋白质、细胞外囊泡（EVs）等运载工具的结合。其中，作为受仿生学启发的EVs和以蛋白质为基础的载体脱颖而出。这篇综述涵盖了用于RNAi传递的工程蛋白载体，以及目前已知的自然存在的细胞外RNA载体，以揭示这些载体的设计思路从而为以后的载体构建带来启示。

### Current trends in biomarker discovery and analysis tools for traumatic brain injury

创伤性脑损伤生物标志物的发现及其分析工具的发展趋势

Briana I. Martinez and Sarah E. Stabenfeldt

Doi:10.1186/s13036-019-0145-8

[点击此处，阅读论文](https://jbioleng.biomedcentral.com/articles/10.1186/s13036-019-0145-8?utm_source=wechat&utm_medium=social&utm_content=organic&utm_campaign=BSCN_2_xw_jbioleng)。

美国每年有170万人受到创伤性脑损伤（TBI）的影响，导致终生认知和行为功能缺陷。神经损伤的复杂病理生理学特点是研发特异性诊断工具的主要障碍。鉴于其他疾病（如癌症）的生物标志物对疾病的发生和发展提供了关键的思路，有助于帮助科研人员开发强有力的临床干预工具。因此，研究人员也开始攻克脑外科领域的生物标志物，并取得了实质性的进展，且有望转化到临床以应用于脑外伤患者的诊断和护理。这篇综述关注在神经损伤生物标志物发现上的进展，包括从基于统计学的方法到成像和机器学习的方法，以及现有技术的发展情况。

### Inhibition of bacterial toxin recognition of membrane components as an anti-virulence strategy

一种新的抗毒力策略：抑制细菌毒素的膜识别组分

Eric Krueger and Angela C. Brown

Doi:10.1186/s13036-018-0138-z

[点击此处，阅读论文](https://jbioleng.biomedcentral.com/articles/10.1186/s13036-018-0138-z?utm_source=wechat&utm_medium=social&utm_content=organic&utm_campaign=BSCN_2_xw_jbioleng)。

随着当前抗生素耐药性的提高，人们迫切需要新的治疗细菌性疾病的方法。为此，许多研究小组开始设计和研究替代疗法。由于许多致病细菌释放的蛋白毒素可以导致或加重疾病，因此，抑制细菌毒素的活性是一种有前途的抗毒力策略。在这篇综述中，Eric Krueger等人描述了几种抑制细菌毒素与宿主细胞膜成分相互作用的方法。作者回顾了前人在抑制与蛋白质受体结合、与必需膜脂成分结合、抑制复合物的组装以及抑制孔形成的研究中所做的努力。并提到虽然这些分子没有在临床试验中得到证实，但在体外和体内的研究结果表明它们作为传统抗生素的新替代品和/或补充物均有十分明朗的应用前景。

### Direct cell reprogramming for tissue engineering and regenerative medicine

组织工程和再生医学的细胞直接重编程

Alexander Grath and Guohao Dai

Doi:10.1186/s13036-019-0144-9

[点击此处，阅读论文](https://jbioleng.biomedcentral.com/articles/10.1186/s13036-019-0144-9?utm_source=wechat&utm_medium=social&utm_content=organic&utm_campaign=BSCN_2_xw_jbioleng)。

细胞直接重编程，也称为转分化，是指在无需通过诱导细胞多能性的前提下便能将体内丰富的细胞重新编程为所需的细胞表型，这些表型能够恢复受损区域的组织功能。因此，细胞直接重编程是细胞组织工程和再生医学领域的一个有前途的研究方向。在这篇综述中，来自美国东北大学的Alexander Grath等人为我们总结了各种细胞重编程技术的最新进展，它们在转化各种体细胞中的应用，在组织再生中的应用，以及向临床应用进行过渡所面临的挑战，和潜在的解决方案。

### Hydrophobins: multifunctional biosurfactants for interface engineering

疏水蛋白：界面工程领域多功能的生物表面活性剂

Bryan W. Berger and Nathanael D. Sallada

Doi:10.1186/s13036-018-0136-1

[点击此处，阅读论文](https://jbioleng.biomedcentral.com/articles/10.1186/s13036-018-0136-1?utm_source=wechat&utm_medium=social&utm_content=organic&utm_campaign=BSCN_2_xw_jbioleng)。

疏水蛋白是一类表面活性很强的蛋白质，具有广泛的界面工程应用前景。由于所识别的独特疏水蛋白序列数量庞大且不断在增加，使用蛋白质工程和其他方法为特定应用设计相应的蛋白变体十分必要。在这篇综述中，作者介绍了疏水蛋白技术及其生产策略的最新应用和进展。疏水蛋白的应用包括但不限于疏水药物的增溶和传递、蛋白质标签纯化、蛋白质和细胞固定、抗菌涂层、生物传感器、生物矿化模板和乳化剂等等。Bryan W. Berger指出，尽管它们在广泛的应用领域有着巨大的应用前景，但开发新的生产策略是提高低重组产量以使其在更广泛的领域得以应用的关键。因此，将这些疏水蛋白得以推广到商业应用中，进一步优化表达系统和产量仍然是科学家们所面临的挑战。

### Engineered In Vitro Models of Tumor Dormancy and Reactivation

设计体外肿瘤休眠和再激活模型

Shantanu Pradhan et.al.

Doi: 10.1186/s13036-018-0120-9

[点击此处，阅读论文](https://jbioleng.biomedcentral.com/articles/10.1186/s13036-018-0120-9?utm_source=wechat&utm_medium=social&utm_content=organic&utm_campaign=BSCN_2_xw_jbioleng)。

控制癌症死亡的主要障碍是肿瘤的转移和复发。原发部位残留的肿瘤细胞，或继发部位的弥散性肿瘤细胞可长期处于休眠状态，数年至数十年后再活化为增殖生长状态。在本文中，作者就弥散性癌细胞死亡、单细胞休眠、肿瘤团休眠和转移性生长的微环境信号和他们的生物学机制以及诱发再活化的因素进行了总结。简要介绍了休眠肿瘤的分子靶点和治疗方法，并重点突出了以工程、体外、生物材料为基础的模拟肿瘤休眠和再激活的方法。

阅读全文请访问：

https://www.biomedcentral.com/collections/ELBIOLENG?utm_source=other&utm_medium=other&utm_content=null&utm_campaign=BSCN_2_WX_elbioleng_scinet

（来源：科学网）

> 特别声明：本文转载仅仅是出于传播信息的需要，并不意味着代表本网站观点或证实其内容的真实性；如其他媒体、网站或个人从本网站转载使用，须保留本网站注明的“来源”，并自负版权等法律责任；作者如果不希望被转载或者联系转载稿费等事宜，请与我们接洽。

# [The BMC Series journals](https://www.biomedcentral.com/p/the-bmc-series-journals)

https://gitee.com/siger99/Wifi-Oximiter-using-MAX30102-and-ESP32/issues/I6HOC1#note_16477779_link

# [About BMC](https://www.biomedcentral.com/about)
        
<p>BMC has an evolving portfolio of some 300 peer-reviewed journals, sharing discoveries from research communities in science, technology, engineering and medicine. In 1999 we made high quality research open to everyone who needed to access it – and in making the open access model sustainable, we changed the world of academic publishing.</p><p>We are committed to continual innovation in research publishing to better support the needs of our communities, ensuring the integrity of the research we publish and championing the benefits of open research for all.</p><p>Our leading research journals include selective titles such as <a href="https://bmcbiol.biomedcentral.com/" class="is-external"><em>BMC Biology</em></a>, <a href="https://bmcmedicine.biomedcentral.com/" class="is-external"><em>BMC Medicine</em></a>,&nbsp;<a href="https://genomebiology.biomedcentral.com/" target="_self" class="is-external"><em>Genome Biology</em></a>,&nbsp;<a href="https://genomemedicine.biomedcentral.com/" target="_self" class="is-external"><em>Genome Medicine</em></a>&nbsp;and <a href="https://bmcglobalpublichealth.biomedcentral.com/" target="_blank" class="is-external"><em>BMC Global and Public Health</em></a>, academic journals such as <a href="https://jhoonline.biomedcentral.com/" class="is-external"><em>Journal of Hematology &amp; Oncology</em></a>, <a href="https://malariajournal.biomedcentral.com/" target="_self" class="is-external"><em>Malaria Journal</em></a> and <a href="https://microbiomejournal.biomedcentral.com/" target="_self" class="is-external"><em>Microbiome</em></a>, and the <a href="https://www.biomedcentral.com/p/the-bmc-series-journals" class="is-external"><em>BMC</em> series</a>, 53 inclusive journals focused on the needs of individual research communities. We also partner with leading institutions and societies to publish journals on their behalf.</p><p>BMC is part of <a href="http://www.springernature.com/gb/" target="_blank" class="is-external">Springer Nature</a>, giving us greater opportunities to help authors everywhere make more connections with research communities across the world.</p>

![输入图片说明](https://foruda.gitee.com/images/1677388492942659141/1a35bfe2_5631341.png "屏幕截图")

  
<ul>                            
<li class="c-header__item u-hide-at-lt-lg">
<a class="c-header__link" href="https://www.biomedcentral.com/journals">
                                        Explore journals
</a>
</li>
                            
<li class="c-header__item u-hide-at-lt-lg">
<a class="c-header__link" href="https://www.biomedcentral.com/getpublished">
                                        Get published
</a>
</li>
                            
<li class="c-header__item u-hide-at-lt-lg">
<a class="c-header__link" href="https://www.biomedcentral.com/about">
                                        About BMC
</a>
</li>
                            
                        
<li class="c-header__item">
<a href="https://www.biomedcentral.com/login" class="c-header__link">Login</a>
</li>
</ul>


# [BMC, research in progress](https://www.biomedcentral.com/)

<p>A pioneer of open access publishing, BMC has an evolving portfolio of high quality peer-reviewed journals including broad interest titles such as BMC Biology and BMC Medicine, specialist journals such as Malaria Journal and Microbiome, and the&nbsp;<a href="http://www.biomedcentral.com/p/the-bmc-series-journals" target="_blank" class="is-external">BMC Series</a>.<br><br>Expanding beyond biomedicine into the physical sciences, mathematics and engineering disciplines, BMC now offers a wider portfolio of subject fields on a single open access platform.</p><p>At BMC, research is always in progress. We are committed to continual innovation to better support the needs of our communities, ensuring the integrity of the research we publish, and championing the benefits of open research. BMC is part of Springer Nature.</p>

<img align="right" width="39%" src="https://foruda.gitee.com/images/1677388996902757101/97e0d24f_5631341.png">

<h2>BMC 20th anniversary</h2>
    
<p>This year BMC is celebrating its 20<sup>th</sup> year anniversary. We are excited about everything we have achieved in that time, especially about BMC’s leadership role in the global growth of open access. We invite you to join us, as we look at BMC’s achievements and future endeavours through interviews, videos and other resources we have created to commemorate our journey.</p>
            
<a href="/p/bmc-20th-anniversary" class="u-button u-button--primary" data-test="teaser-button-link"> BMC 20th Year Anniversary Hub</a>

## Supporting the research community at this time

| <img width="1000px" src="https://foruda.gitee.com/images/1677389172883084669/87b7dfb3_5631341.png"> | <img width="1000px" src="https://foruda.gitee.com/images/1677389180800958752/51bdf8c7_5631341.png"> |
|---|---|
| [SARS-CoV-2 and COVID-19](https://www.springernature.com/gp/researchers/campaigns/coronavirus?utm_source=bmc&utm_medium=referral&utm_content=null&utm_campaign=Homepage_Coronovirus_Teaser) | [Coronavirus research highlights](https://www.biomedcentral.com/collections/coronavirus) |
| Springer Nature is committed to supporting the global response to emerging outbreaks by enabling fast and direct access to the latest available research, data, and resources for the research community | This collection draws together research from across BMC journals, including broad scope titles such as  _BMC Public Health_ ,  _Virology Journal_  and  _Respiratory Research_ , and our more specialist journals such as  _Globalization and Health_  and  _Conflict and Health_ .  |
| [![输入图片说明](https://foruda.gitee.com/images/1677389399659006709/c301d6d0_5631341.png "屏幕截图")](https://www.springernature.com/gp/researchers/campaigns/black-lives-matter?utm_source=bmc&utm_medium=referral&utm_content=RMarketing&utm_campaign=CMTL_1_RS_CMTL_BlackLivesMatter_BMC_HP_Teaser) | <h2>Black Lives Matter</h2><p>A collection of books, journal articles and magazine content that amplifies Black voices and the issues raised by the Black Lives Matter movement.</p><a href="https://www.springernature.com/gp/researchers/campaigns/black-lives-matter?utm_source=bmc&amp;utm_medium=referral&amp;utm_content=RMarketing&amp;utm_campaign=CMTL_1_RS_CMTL_BlackLivesMatter_BMC_HP_Teaser" class="u-button u-button--primary" data-test="teaser-button-link"> Find out more </a> |

## Editors' picks

| ![输入图片说明](https://foruda.gitee.com/images/1677389558471010163/783d8085_5631341.png "屏幕截图") | ![输入图片说明](https://foruda.gitee.com/images/1677389564152289924/e1ad6903_5631341.png "屏幕截图") |
|---|---|
| [Vegans, vegetarians and pescetarians may be at higher risk of bone fractures than meat eaters](https://bmcmedicine.biomedcentral.com/articles/10.1186/s12916-020-01815-3) | [Poor work-life balance may have negative health effects](https://bmcpublichealth.biomedcentral.com/articles/10.1186/s12889-020-09139-w) |
| <p>Compared with people who ate meat, vegans with lower calcium and protein intakes on average, had a 43% higher risk of fractures anywhere in the body (total fractures), as well as higher risks of site-specific fractures of the hips, legs and vertebrae.</p> <i>BMC Medicine 2020</i> | <p>Working adults across Europe with poor work-life balance are more likely to report poor general health.</p> <i>BMC Public Health 2020</i> |
| ![输入图片说明](https://foruda.gitee.com/images/1677389847134934017/42e8bf26_5631341.png "屏幕截图") | ![输入图片说明](https://foruda.gitee.com/images/1677389852300901127/13baccc6_5631341.png "屏幕截图") |
| [BMC Research Notes Launches Data Notes](https://bmcresnotes.biomedcentral.com/about/introducing-data-notes?utm_source=website&utm_medium=Referral&utm_campaign=Data_Note_BMCHomepage_to_Landing_Page) |  [Open Access Funding Support Service](https://goo.gl/TLwXfJ) |
| Data is becoming increasingly more important. Researchers tell us they want to share their data to progress research, to receive more credit and visibility for their work and to comply with funder policies.  To help our authors do this, we have launched the data note. |  Did you know that there almost 250 open access article processing charge (APC) funds available to researchers worldwide? At BMC, we offer a free advice service to help our authors to discover and apply for funding. On our support pages you can find our OA checklist along with a list of research funders and institutions that make APC funding available. |
| <h2>Retrospectively registered trials</h2> | <h2>Through the looking glass</h2> |
| ![输入图片说明](https://foruda.gitee.com/images/1677389867449831830/879d8483_5631341.png "屏幕截图") | ![输入图片说明](https://foruda.gitee.com/images/1677389874392294001/ac379c28_5631341.png "屏幕截图") |
| [The Editors’ dilemma](https://blogs.biomedcentral.com/bmcblog/2016/04/15/retrospectively-registered-trials-editors-dilemma/) | [Citizen science](https://blogs.biomedcentral.com/bmcseriesblog/2016/07/22/citizen-science-looking-glass/) |
| Prospective clinical trial registration aims to address publication and reporting bias. Unfortunately, not all clinical trials are registered before they start. Here we discuss the dilemma faced by editors when receiving submissions reporting a clinical trial that was not registered prospectively, and a new policy for increasing transparency when a trial was registered after participant recruitment has begun. | In this blog, Poppy Lakeman Fraser, Senior Programme Coordinator for the Open Air Laboratories (OPAL) initiative, explains what citizen science actually is and how researchers rely on this form of data collection. |

## From our blogs
    
<ul class="c-list-group c-list-group--bordered c-list-group c-list-group--md">
                
<li class="c-list-group__item">
<article class="c-media c-media--sm">
                            
<div class="c-media__body">
<h3 class="c-media__title u-text-md">
                                    
<a href="https://blogs.biomedcentral.com/on-medicine/2023/02/24/enceph-ig-trial-intravenous-immunoglobulin-ivig-in-autoimmune-encephalitis-in-adults-a-randomised-double-blind-placebo-controlled-trial-isrctn/">Intravenous immunoglobulin in autoimmune encephalitis – a blog for Rare Disease Day 2023</a>
                                    
</h3>
                                
<p class="u-text-sm">24 February 2023</p>
                                
</div>
</article>
</li>
                
<li class="c-list-group__item">
<article class="c-media c-media--sm">
                            
<div class="c-media__body">
<h3 class="c-media__title u-text-md">
                                    
<a href="https://blogs.biomedcentral.com/on-medicine/2023/02/20/pilot-clinical-trial-to-test-the-function-of-a-diagnostic-sensor-in-predicting-impending-urinary-catheter-blockage-in-long-term-catheterized-patients-isrctn/">Pilot clinical trial to test the function of a diagnostic sensor in predicting impending urinary catheter blockage in long-term catheterized patients</a>
                                    
</h3>
                                
<p class="u-text-sm">20 February 2023</p>
                                
</div>
</article>
</li>
                
<li class="c-list-group__item">
<article class="c-media c-media--sm">
                            
<div class="c-media__body">
<h3 class="c-media__title u-text-md">
                                    
<a href="https://blogs.biomedcentral.com/bugbitten/2023/02/17/parasites-of-passerines-parasitic-fauna-of-wild-passerine-birds-of-britain-and-ireland/">Parasites of Passerines: parasitic fauna of wild passerine birds of Britain and Ireland</a>
                                    
</h3>
                                
<p class="u-text-sm">17 February 2023</p>
                                
</div>
</article>
</li>
                
</ul>

# Emerging leaders in biological engineering
                    
![输入图片说明](https://foruda.gitee.com/images/1677390430638787140/cf003c1c_5631341.png "屏幕截图")

<p>Over the past decade, biological engineering has promoted the convergence of themes from multiple disciplines and has gained significant promise in integrating the principles of biology and engineering methodologies to address grand challenges in&nbsp;human health and the&nbsp;health of the planet. This thematic series in<a href="https://jbioleng.biomedcentral.com/" target="_blank" class="is-external"><em>Journal of Biological Engineering</em></a> highlights exciting new areas and emerging leaders who are addressing many of these challenges.</p><p><strong>This collection was first launched in 2018 and is re-opening for new submissions&nbsp;in 2021.</strong></p><p>Edited by Raj R Rao</p><p>Click<a href="https://jbioleng.biomedcentral.com/articles/10.1186/s13036-019-0146-7" target="_self" class="is-external">here</a>&nbsp;for an Editorial highlighting the thematic series.</p><p>Further information about the journal, including details of the aims and scope, can be found&nbsp;<a href="https://urldefense.proofpoint.com/v2/url?u=https-3A__nam11.safelinks.protection.outlook.com_-3Furl-3Dhttps-253A-252F-252Fjbioleng.biomedcentral.com-252Fabout-26data-3D04-257C01-257Crajrao-2540uark.edu-257Ca226a46e62e548fd3c3c08d972c7efab-257C79c742c4e61c4fa5be89a3cb566a80d1-257C0-257C0-257C637667025314101444-257CUnknown-257CTWFpbGZsb3d8eyJWIjoiMC4wLjAwMDAiLCJQIjoiV2luMzIiLCJBTiI6Ik1haWwiLCJXVCI6Mn0-253D-257C1000-26sdata-3DYNgZOdzna-252BbbjYs-252BwWdX2g0MM4Me7VeyTgRjeTWF19g-253D-26reserved-3D0&amp;d=DwMGaQ&amp;c=vh6FgFnduejNhPPD0fl_yRaSfZy8CWbWnIf4XJhSqx8&amp;r=bQJt3gAqKqkjTFqFDIY8ysTHl2JqvlW8dfbxOwHdkVNSWmE9kdaMlhlM52JZ1MXk&amp;m=vZIb7O2RarZgqQXKnEVK09zFNwzpQkU8X76r0BUDjgc&amp;s=4F48cBxjNohA9EvPckDEAgV6T0HlgetYU7147_OvH7k&amp;e=" class="is-external">here</a>.</p><p><strong>How to submit:</strong></p><p>Submissions of original&nbsp;<a href="https://urldefense.proofpoint.com/v2/url?u=https-3A__nam11.safelinks.protection.outlook.com_-3Furl-3Dhttps-253A-252F-252Fjbioleng.biomedcentral.com-252Fsubmission-2Dguidelines-252Fpreparing-2Dyour-2Dmanuscript-252Fresearch-26data-3D04-257C01-257Crajrao-2540uark.edu-257Ca226a46e62e548fd3c3c08d972c7efab-257C79c742c4e61c4fa5be89a3cb566a80d1-257C0-257C0-257C637667025314101444-257CUnknown-257CTWFpbGZsb3d8eyJWIjoiMC4wLjAwMDAiLCJQIjoiV2luMzIiLCJBTiI6Ik1haWwiLCJXVCI6Mn0-253D-257C1000-26sdata-3DyHThA99eCLNzLvXSv0poLCxc-252BdajOa9wQKKumw5N7bg-253D-26reserved-3D0&amp;d=DwMGaQ&amp;c=vh6FgFnduejNhPPD0fl_yRaSfZy8CWbWnIf4XJhSqx8&amp;r=bQJt3gAqKqkjTFqFDIY8ysTHl2JqvlW8dfbxOwHdkVNSWmE9kdaMlhlM52JZ1MXk&amp;m=vZIb7O2RarZgqQXKnEVK09zFNwzpQkU8X76r0BUDjgc&amp;s=pg9kXHuErnDanfuFG3ToHvX--fC7KLKyKIznqDFxw1o&amp;e=" class="is-external">Research Article</a> and&nbsp;<a href="https://urldefense.proofpoint.com/v2/url?u=https-3A__nam11.safelinks.protection.outlook.com_-3Furl-3Dhttps-253A-252F-252Fjbioleng.biomedcentral.com-252Fsubmission-2Dguidelines-252Fpreparing-2Dyour-2Dmanuscript-252Freviews-26data-3D04-257C01-257Crajrao-2540uark.edu-257Ca226a46e62e548fd3c3c08d972c7efab-257C79c742c4e61c4fa5be89a3cb566a80d1-257C0-257C0-257C637667025314111435-257CUnknown-257CTWFpbGZsb3d8eyJWIjoiMC4wLjAwMDAiLCJQIjoiV2luMzIiLCJBTiI6Ik1haWwiLCJXVCI6Mn0-253D-257C1000-26sdata-3DfG-252F6m-252FqPExxW8V56jfPSsJSLCSiNlTMkpYoBCzVvEtw-253D-26reserved-3D0&amp;d=DwMGaQ&amp;c=vh6FgFnduejNhPPD0fl_yRaSfZy8CWbWnIf4XJhSqx8&amp;r=bQJt3gAqKqkjTFqFDIY8ysTHl2JqvlW8dfbxOwHdkVNSWmE9kdaMlhlM52JZ1MXk&amp;m=vZIb7O2RarZgqQXKnEVK09zFNwzpQkU8X76r0BUDjgc&amp;s=NQb07k3OrwY4REwS59CNRhU5oC7AnnkZz8_-VgUodvY&amp;e=" class="is-external">Review</a>&nbsp;papers are invited, subject to editorial approval. Before submitting, please send a pre-submission enquiry to the collection editor Raj Rao (<a href="mailto:rajrao@uark.edu" class="is-external">rajrao@uark.edu</a>) including an abstract and short personal biography.&nbsp;</p><p><strong>Submission deadline: 31st March 2022</strong></p><p>Articles once submitted will be subject to editorial triage and peer-review as standard for the journal. Collection content will be published continuously as part of the main journal and highlighted on the collection homepage as soon as it is ready.</p><p><br></p>
        
<h3 class="c-listing__title" itemprop="name">
                    
<a itemprop="url" data-test="title-link" href="//jbioleng.biomedcentral.com/articles/10.1186/s13036-022-00299-4">Pathophysiology of mesangial expansion in diabetic nephropathy: mesangial structure, glomerular biomechanics, and biochemical signaling and regulation</a>
                    
</h3>
                
                    
<p>
                        Diabetic nephropathy, a kidney complication arising from diabetes, is the leading cause of death in diabetic patients. Unabated, the growing epidemic of diabetes is increasing instances of diabetic nephropathy...
</p>
                    
                

<div class="c-listing__authors u-mb-0" data-test="ResultList-authors">
<span class="u-visually-hidden">Authors:</span>
<span class="c-listing__authors-list">Haryana Y. Thomas and Ashlee N. Ford Versypt</span>
</div>

<div data-test="teaser-citation">
<span class="u-visually-hidden">Citation:</span>
<em data-test="journal-title">Journal of Biological Engineering</em>
                    2022
<span>16</span>:19
</div>
<div class="c-listing__metadata">
<span data-test="result-list"><span class="u-visually-hidden">Content type:</span>Review</span>
<span data-test="published-on">Published on:<span itemprop="datePublished">2 August 2022</span></span>
</div>

                
                


                
<p>                    
<ul class="c-listing__view-options" data-test="article-list-search-results">
<li>
<a href="//jbioleng.biomedcentral.com/articles/10.1186/s13036-022-00299-4" data-track="click" data-track-category="Collection item" data-track-action="Click Fulltext" data-test="fulltext-link" data-track-label="10.1186/s13036-022-00299-4" itemprop="citation">
<svg class="c-listing__view-options-icon" width="12" height="12" aria-hidden="true" focusable="false">
<use xlink:href="#icon-chevron-right"></use>
</svg>
<span>
<span class="u-visually-hidden">View</span>
                                        Full Text
</span>
</a>
</li>
<li>
<a data-track="click" data-track-category="Collection item" data-track-action="Download PDF" data-track-label="10.1186/s13036-022-00299-4" data-test="pdf-link" href="//jbioleng.biomedcentral.com/counter/pdf/10.1186/s13036-022-00299-4.pdf">
<svg class="c-listing__view-options-icon" width="12" height="12" aria-hidden="true" focusable="false">
<use xlink:href="#icon-chevron-right"></use>
</svg>
<span>
<span class="u-visually-hidden">View</span>
                                        PDF
</span>
</a>
</li>
</ul>
</p>

<h3 class="c-listing__title" itemprop="name">
                    
<a itemprop="url" data-test="title-link" href="//jbioleng.biomedcentral.com/articles/10.1186/s13036-022-00298-5">Dendrimer-based drug delivery systems: history, challenges, and latest developments</a>
                    
</h3>
                
                    
<p>
                        Since the first dendrimer was reported in 1978 by Fritz Vögtle, dendrimer research has grown exponentially, from synthesis to application in the past four decades. The distinct structure characteristics of den...
</p>
                    
                

<div class="c-listing__authors u-mb-0" data-test="ResultList-authors">
<span class="u-visually-hidden">Authors:</span>
<span class="c-listing__authors-list">Juan Wang, Boxuan Li, Li Qiu, Xin Qiao and Hu Yang</span>
</div>

<div data-test="teaser-citation">
<span class="u-visually-hidden">Citation:</span>
<em data-test="journal-title">Journal of Biological Engineering</em>
                    2022
<span>16</span>:18
</div>
<div class="c-listing__metadata">
<span data-test="result-list"><span class="u-visually-hidden">Content type:</span>Review</span>
<span data-test="published-on">Published on:<span itemprop="datePublished">25 July 2022</span></span>
</div>

                
                


                
<p>
<ul class="c-listing__view-options" data-test="article-list-search-results">
<li>
<a href="//jbioleng.biomedcentral.com/articles/10.1186/s13036-022-00298-5" data-track="click" data-track-category="Collection item" data-track-action="Click Fulltext" data-test="fulltext-link" data-track-label="10.1186/s13036-022-00298-5" itemprop="citation">
<svg class="c-listing__view-options-icon" width="12" height="12" aria-hidden="true" focusable="false">
<use xlink:href="#icon-chevron-right"></use>
</svg>
<span>
<span class="u-visually-hidden">View</span>
                                        Full Text
</span>
</a>
</li>
<li>
<a data-track="click" data-track-category="Collection item" data-track-action="Download PDF" data-track-label="10.1186/s13036-022-00298-5" data-test="pdf-link" href="//jbioleng.biomedcentral.com/counter/pdf/10.1186/s13036-022-00298-5.pdf">
<svg class="c-listing__view-options-icon" width="12" height="12" aria-hidden="true" focusable="false">
<use xlink:href="#icon-chevron-right"></use>
</svg>
<span>
<span class="u-visually-hidden">View</span>
                                        PDF
</span>
</a>
</li>
</ul>
                    
</p>


<li>

<h3>
                    
<a href="//jbioleng.biomedcentral.com/articles/10.1186/s13036-022-00292-x">Extracellular matrix dynamics: tracking in biological systems and their implications</a>
                    
</h3>
                
                    
<p>
                        The extracellular matrix (ECM) constitutes the main acellular microenvironment of cells in almost all tissues and organs. The ECM not only provides mechanical support, but also mediates numerous biochemical in...
</p>
                    
                

<div>
<span>Authors:</span>
<span>Michael Hu, Zihan Ling and Xi Ren</span>
</div>

<div>
<span>Citation:</span>
<em>Journal of Biological Engineering</em>
                    2022
<span>16</span>:13
</div>
<div>
<span><span>Content type:</span>Review</span>
<span>Published on:<span>30 May 2022</span></span>
</div>

                
                


                
<p>
<ul>
<li>
<a href="//jbioleng.biomedcentral.com/articles/10.1186/s13036-022-00292-x" data-track-label="10.1186/s13036-022-00292-x">
<svg width="12" height="12" aria-hidden="true" focusable="false">
<use xlink:href="#icon-chevron-right"></use>
</svg>
<span>
<span>View</span>
                                        Full Text
</span>
</a>
</li>
<li>
<a data-track-label="10.1186/s13036-022-00292-x" href="//jbioleng.biomedcentral.com/counter/pdf/10.1186/s13036-022-00292-x.pdf">
<svg width="12" height="12" aria-hidden="true" focusable="false">
<use xlink:href="#icon-chevron-right"></use>
</svg>
<span>
<span>View</span>
                                        PDF
</span>
</a>
</li>
</ul>
                    
</p>

                

</li>
    
<li>

<h3>
                    
<a href="//jbioleng.biomedcentral.com/articles/10.1186/s13036-022-00291-y">Tissue engineering in age-related macular degeneration: a mini-review</a>
                    
</h3>
                
                    
<p>
                        Age-related macular degeneration (AMD) is a progressive, degenerative disease of the macula, leading to severe visual loss in the elderly population. There are two types of AMD: non-exudative (‘dry’) AMD and e...
</p>
                    
                

<div>
<span>Authors:</span>
<span>Andres Wu, Renhao Lu and Esak Lee</span>
</div>

<div>
<span>Citation:</span>
<em>Journal of Biological Engineering</em>
                    2022
<span>16</span>:11
</div>
<div>
<span><span>Content type:</span>Review</span>
<span>Published on:<span>16 May 2022</span></span>
</div>

                
                


<p>
              
                    
<ul>
<li>
<a href="//jbioleng.biomedcentral.com/articles/10.1186/s13036-022-00291-y" data-track-label="10.1186/s13036-022-00291-y">
<svg width="12" height="12" aria-hidden="true" focusable="false">
<use xlink:href="#icon-chevron-right"></use>
</svg>
<span>
<span>View</span>
                                        Full Text
</span>
</a>
</li>
<li>
<a data-track-label="10.1186/s13036-022-00291-y" href="//jbioleng.biomedcentral.com/counter/pdf/10.1186/s13036-022-00291-y.pdf">
<svg width="12" height="12" aria-hidden="true" focusable="false">
<use xlink:href="#icon-chevron-right"></use>
</svg>
<span>
<span>View</span>
                                        PDF
</span>
</a>
</li>
</ul>
                    
</p>
             

                

</li>
    
<li>

<h3>
                    
<a href="//jbioleng.biomedcentral.com/articles/10.1186/s13036-022-00289-6">Tissue engineering of the gastrointestinal tract: the historic path to translation</a>
                    
</h3>
                
                    
<p>
                        The gastrointestinal (GI) tract is imperative for multiple functions including digestion, nutrient absorption, and timely waste disposal. The central feature of the gut is peristalsis, intestinal motility, whi...
</p>
                    
                

<div>
<span>Authors:</span>
<span>Claudia A. Collier, Christian Mendiondo and Shreya Raghavan</span>
</div>

<div>
<span>Citation:</span>
<em>Journal of Biological Engineering</em>
                    2022
<span>16</span>:9
</div>
<div>
<span><span>Content type:</span>Review</span>
<span>Published on:<span>4 April 2022</span></span>
</div>

                
                


<p>
           
                    
<ul>
<li>
<a href="//jbioleng.biomedcentral.com/articles/10.1186/s13036-022-00289-6" data-track-label="10.1186/s13036-022-00289-6">
<svg width="12" height="12" aria-hidden="true" focusable="false">
<use xlink:href="#icon-chevron-right"></use>
</svg>
<span>
<span>View</span>
                                        Full Text
</span>
</a>
</li>
<li>
<a data-track-label="10.1186/s13036-022-00289-6" href="//jbioleng.biomedcentral.com/counter/pdf/10.1186/s13036-022-00289-6.pdf">
<svg width="12" height="12" aria-hidden="true" focusable="false">
<use xlink:href="#icon-chevron-right"></use>
</svg>
<span>
<span>View</span>
                                        PDF
</span>
</a>
</li>
</ul>
                    
</p>
                

                

</li>
    
<li>

<h3>
                    
<a href="//jbioleng.biomedcentral.com/articles/10.1186/s13036-022-00287-8">Review: biological engineering for nature-based climate solutions</a>
                    
</h3>
                
                    
<p>
                        Nature-based Climate Solutions are landscape stewardship techniques to reduce greenhouse gas emissions and increase soil or biomass carbon sequestration. These mitigation approaches to climate change present a...
</p>
                    
                

<div>
<span>Authors:</span>
<span>Benjamin R. K. Runkle</span>
</div>

<div>
<span>Citation:</span>
<em>Journal of Biological Engineering</em>
                    2022
<span>16</span>:7
</div>
<div>
<span><span>Content type:</span>Review</span>
<span>Published on:<span>29 March 2022</span></span>
</div>

                
                

<p>

                
                    
<ul>
<li>
<a href="//jbioleng.biomedcentral.com/articles/10.1186/s13036-022-00287-8" data-track-label="10.1186/s13036-022-00287-8">
<svg width="12" height="12" aria-hidden="true" focusable="false">
<use xlink:href="#icon-chevron-right"></use>
</svg>
<span>
<span>View</span>
                                        Full Text
</span>
</a>
</li>
<li>
<a data-track-label="10.1186/s13036-022-00287-8" href="//jbioleng.biomedcentral.com/counter/pdf/10.1186/s13036-022-00287-8.pdf">
<svg width="12" height="12" aria-hidden="true" focusable="false">
<use xlink:href="#icon-chevron-right"></use>
</svg>
<span>
<span>View</span>
                                        PDF
</span>
</a>
</li>
</ul>
                    
                
</p>

                

</li>
    
<li>

<h3>
                    
<a href="//jbioleng.biomedcentral.com/articles/10.1186/s13036-019-0194-z">Neuromodulation of metabolic functions: from pharmaceuticals to bioelectronics to biocircuits</a>
                    
</h3>
                
                    
<p>
                        Neuromodulation of central and peripheral neural circuitry brings together neurobiologists and neural engineers to develop advanced neural interfaces to decode and recapitulate the information encoded in the n...
</p>
                    
                

<div>
<span>Authors:</span>
<span>Benjamin J. Seicol, Sebastian Bejarano, Nicholas Behnke and Liang Guo</span>
</div>

<div>
<span>Citation:</span>
<em>Journal of Biological Engineering</em>
                    2019
<span>13</span>:67
</div>
<div>
<span><span>Content type:</span>Review</span>
<span>Published on:<span>1 August 2019</span></span>
</div>

                
                
<p>


                
                    
<ul>
<li>
<a href="//jbioleng.biomedcentral.com/articles/10.1186/s13036-019-0194-z" data-track-label="10.1186/s13036-019-0194-z">
<svg width="12" height="12" aria-hidden="true" focusable="false">
<use xlink:href="#icon-chevron-right"></use>
</svg>
<span>
<span>View</span>
                                        Full Text
</span>
</a>
</li>
<li>
<a data-track-label="10.1186/s13036-019-0194-z" href="//jbioleng.biomedcentral.com/counter/pdf/10.1186/s13036-019-0194-z.pdf">
<svg width="12" height="12" aria-hidden="true" focusable="false">
<use xlink:href="#icon-chevron-right"></use>
</svg>
<span>
<span>View</span>
                                        PDF
</span>
</a>
</li>
</ul>
                    
                
</p>

                

</li>
    
<li>

<h3>
                    
<a href="//jbioleng.biomedcentral.com/articles/10.1186/s13036-019-0192-1">Performance evaluation of a hybrid sequencing batch reactor under saline and hyper saline conditions</a>
                    
</h3>
                
                    
<p>
                        Significant rise in concentration of saline wastewater entering the treatment plants has been resulting in many problems in the biological treatment processes. On the other hand, the specific conditions of phy...
</p>
                    
                

<div>
<span>Authors:</span>
<span>Mostafa Tizghadam Ghazani and Alireza Taghdisian</span>
</div>

<div>
<span>Citation:</span>
<em>Journal of Biological Engineering</em>
                    2019
<span>13</span>:64
</div>
<div>
<span><span>Content type:</span>Research</span>
<span>Published on:<span>29 July 2019</span></span>
</div>

                
                

<p>

                
                    
<ul>
<li>
<a href="//jbioleng.biomedcentral.com/articles/10.1186/s13036-019-0192-1" data-track-label="10.1186/s13036-019-0192-1">
<svg width="12" height="12" aria-hidden="true" focusable="false">
<use xlink:href="#icon-chevron-right"></use>
</svg>
<span>
<span>View</span>
                                        Full Text
</span>
</a>
</li>
<li>
<a data-track-label="10.1186/s13036-019-0192-1" href="//jbioleng.biomedcentral.com/counter/pdf/10.1186/s13036-019-0192-1.pdf">
<svg width="12" height="12" aria-hidden="true" focusable="false">
<use xlink:href="#icon-chevron-right"></use>
</svg>
<span>
<span>View</span>
                                        PDF
</span>
</a>
</li>
</ul>
                    
</p>
                

                

</li>
    
<li>

<h3>
                    
<a href="//jbioleng.biomedcentral.com/articles/10.1186/s13036-019-0190-3">The engineering principles of combining a transcriptional incoherent feedforward loop with negative feedback</a>
                    
</h3>
                
                    
<p>
                        Regulation of gene expression is of paramount importance in all living systems. In the past two decades, it has been discovered that certain motifs, such as the feedforward motif, are overrepresented in gene r...
</p>
                    
                

<div>
<span>Authors:</span>
<span>Gregory T. Reeves</span>
</div>

<div>
<span>Citation:</span>
<em>Journal of Biological Engineering</em>
                    2019
<span>13</span>:62
</div>
<div>
<span><span>Content type:</span>Research</span>
<span>Published on:<span>10 July 2019</span></span>
</div>

                
                

<p>

                
                    
<ul>
<li>
<a href="//jbioleng.biomedcentral.com/articles/10.1186/s13036-019-0190-3" data-track-label="10.1186/s13036-019-0190-3">
<svg width="12" height="12" aria-hidden="true" focusable="false">
<use xlink:href="#icon-chevron-right"></use>
</svg>
<span>
<span>View</span>
                                        Full Text
</span>
</a>
</li>
<li>
<a data-track-label="10.1186/s13036-019-0190-3" href="//jbioleng.biomedcentral.com/counter/pdf/10.1186/s13036-019-0190-3.pdf">
<svg width="12" height="12" aria-hidden="true" focusable="false">
<use xlink:href="#icon-chevron-right"></use>
</svg>
<span>
<span>View</span>
                                        PDF
</span>
</a>
</li>
</ul>
                    
</p>
                

                

</li>
    
<li>

<h3>
                    
<a href="//jbioleng.biomedcentral.com/articles/10.1186/s13036-019-0185-0">Cardiac tissue engineering: state-of-the-art methods and outlook</a>
                    
</h3>
                
                    
<p>
                        The purpose of this review is to assess the state-of-the-art fabrication methods, advances in genome editing, and the use of machine learning to shape the prospective growth in cardiac tissue engineering. Thos...
</p>
                    
                

<div>
<span>Authors:</span>
<span>Anh H. Nguyen, Paul Marsh, Lauren Schmiess-Heine, Peter J. Burke, Abraham Lee, Juhyun Lee and Hung Cao</span>
</div>

<div>
<span>Citation:</span>
<em>Journal of Biological Engineering</em>
                    2019
<span>13</span>:57
</div>
<div>
<span><span>Content type:</span>Review</span>
<span>Published on:<span>28 June 2019</span></span>
</div>

                
                


<p>
                
                    
<ul>
<li>
<a href="//jbioleng.biomedcentral.com/articles/10.1186/s13036-019-0185-0" data-track-label="10.1186/s13036-019-0185-0">
<svg width="12" height="12" aria-hidden="true" focusable="false">
<use xlink:href="#icon-chevron-right"></use>
</svg>
<span>
<span>View</span>
                                        Full Text
</span>
</a>
</li>
<li>
<a data-track-label="10.1186/s13036-019-0185-0" href="//jbioleng.biomedcentral.com/counter/pdf/10.1186/s13036-019-0185-0.pdf">
<svg width="12" height="12" aria-hidden="true" focusable="false">
<use xlink:href="#icon-chevron-right"></use>
</svg>
<span>
<span>View</span>
                                        PDF
</span>
</a>
</li>
</ul>
                    
                
</p>

                

</li>
    
<li>

<h3>
                    
<a href="//jbioleng.biomedcentral.com/articles/10.1186/s13036-019-0173-4">Quantum dot therapeutics: a new class of radical therapies</a>
                    
</h3>
                
                    
<p>
                        Traditional therapeutics and vaccines represent the bedrock of modern medicine, where isolated biochemical molecules or designed proteins have led to success in treating and preventing diseases. However, sever...
</p>
                    
                

<div>
<span>Authors:</span>
<span>Max Levy, Partha P. Chowdhury and Prashant Nagpal</span>
</div>

<div>
<span>Citation:</span>
<em>Journal of Biological Engineering</em>
                    2019
<span>13</span>:48
</div>
<div>
<span><span>Content type:</span>Review</span>
<span>Published on:<span>29 May 2019</span></span>
</div>

                
                


<p>
               
                    
<ul>
<li>
<a href="//jbioleng.biomedcentral.com/articles/10.1186/s13036-019-0173-4" data-track-label="10.1186/s13036-019-0173-4">
<svg width="12" height="12" aria-hidden="true" focusable="false">
<use xlink:href="#icon-chevron-right"></use>
</svg>
<span>
<span>View</span>
                                        Full Text
</span>
</a>
</li>
<li>
<a data-track-label="10.1186/s13036-019-0173-4" href="//jbioleng.biomedcentral.com/counter/pdf/10.1186/s13036-019-0173-4.pdf">
<svg width="12" height="12" aria-hidden="true" focusable="false">
<use xlink:href="#icon-chevron-right"></use>
</svg>
<span>
<span>View</span>
                                        PDF
</span>
</a>
</li>
</ul>
                    
                
</p>

                

</li>
    
<li>

<h3>
                    
<a href="//jbioleng.biomedcentral.com/articles/10.1186/s13036-019-0166-3">Non-canonical amino acid labeling in proteomics and biotechnology</a>
                    
</h3>
                
                    
<p>
                        Metabolic labeling of proteins with non-canonical amino acids (ncAAs) provides unique bioorthogonal chemical groups during de novo synthesis by taking advantage of both endogenous and heterologous protein synt...
</p>
                    
                

<div>
<span>Authors:</span>
<span>Aya M. Saleh, Kristen M. Wilding, Sarah Calve, Bradley C. Bundy and Tamara L. Kinzer-Ursem</span>
</div>

<div>
<span>Citation:</span>
<em>Journal of Biological Engineering</em>
                    2019
<span>13</span>:43
</div>
<div>
<span><span>Content type:</span>Review</span>
<span>Published on:<span>22 May 2019</span></span>
</div>

                
                

<p>

                
                    
<ul>
<li>
<a href="//jbioleng.biomedcentral.com/articles/10.1186/s13036-019-0166-3" data-track-label="10.1186/s13036-019-0166-3">
<svg width="12" height="12" aria-hidden="true" focusable="false">
<use xlink:href="#icon-chevron-right"></use>
</svg>
<span>
<span>View</span>
                                        Full Text
</span>
</a>
</li>
<li>
<a data-track-label="10.1186/s13036-019-0166-3" href="//jbioleng.biomedcentral.com/counter/pdf/10.1186/s13036-019-0166-3.pdf">
<svg width="12" height="12" aria-hidden="true" focusable="false">
<use xlink:href="#icon-chevron-right"></use>
</svg>
<span>
<span>View</span>
                                        PDF
</span>
</a>
</li>
</ul>
                    
                
</p>

                

</li>
    
<li>

<h3>
                    
<a href="//jbioleng.biomedcentral.com/articles/10.1186/s13036-019-0170-7">Genetic circuits to engineer tissues with alternative functions</a>
                    
</h3>
                
                    
<p>
                        Persistent and complex problems arising with respect to human physiology and pathology have led to intense investigation into therapies and tools that permit more targeted outcomes and biomimetic responses to ...
</p>
                    
                

<div>
<span>Authors:</span>
<span>C. P. Healy and T. L. Deans</span>
</div>

<div>
<span>Citation:</span>
<em>Journal of Biological Engineering</em>
                    2019
<span>13</span>:39
</div>
<div>
<span><span>Content type:</span>Review</span>
<span>Published on:<span>3 May 2019</span></span>
</div>

                
                


<p>
               
                    
<ul>
<li>
<a href="//jbioleng.biomedcentral.com/articles/10.1186/s13036-019-0170-7" data-track-label="10.1186/s13036-019-0170-7">
<svg width="12" height="12" aria-hidden="true" focusable="false">
<use xlink:href="#icon-chevron-right"></use>
</svg>
<span>
<span>View</span>
                                        Full Text
</span>
</a>
</li>
<li>
<a data-track-label="10.1186/s13036-019-0170-7" href="//jbioleng.biomedcentral.com/counter/pdf/10.1186/s13036-019-0170-7.pdf">
<svg width="12" height="12" aria-hidden="true" focusable="false">
<use xlink:href="#icon-chevron-right"></use>
</svg>
<span>
<span>View</span>
                                        PDF
</span>
</a>
</li>
</ul>
                    
</p>
                

                

</li>
    
<li>

<h3>
                    
<a href="//jbioleng.biomedcentral.com/articles/10.1186/s13036-019-0162-7">Electrical energy storage with engineered biological systems</a>
                    
</h3>
                
                    
<p>
                        The availability of renewable energy technologies is increasing dramatically across the globe thanks to their growing maturity. However, large scale electrical energy storage and retrieval will almost certainl...
</p>
                    
                

<div>
<span>Authors:</span>
<span>Farshid Salimijazi, Erika Parra and Buz Barstow</span>
</div>

<div>
<span>Citation:</span>
<em>Journal of Biological Engineering</em>
                    2019
<span>13</span>:38
</div>
<div>
<span><span>Content type:</span>Review</span>
<span>Published on:<span>3 May 2019</span></span>
</div>

                
                


<p>
               
                    
<ul>
<li>
<a href="//jbioleng.biomedcentral.com/articles/10.1186/s13036-019-0162-7" data-track-label="10.1186/s13036-019-0162-7">
<svg width="12" height="12" aria-hidden="true" focusable="false">
<use xlink:href="#icon-chevron-right"></use>
</svg>
<span>
<span>View</span>
                                        Full Text
</span>
</a>
</li>
<li>
<a data-track-label="10.1186/s13036-019-0162-7" href="//jbioleng.biomedcentral.com/counter/pdf/10.1186/s13036-019-0162-7.pdf">
<svg width="12" height="12" aria-hidden="true" focusable="false">
<use xlink:href="#icon-chevron-right"></use>
</svg>
<span>
<span>View</span>
                                        PDF
</span>
</a>
</li>
</ul>
                    
                
</p>

                

</li>
    
<li>

<h3>
                    
<a href="//jbioleng.biomedcentral.com/articles/10.1186/s13036-019-0161-8">Tools to reverse-engineer multicellular systems: case studies using the fruit fly</a>
                    
</h3>
                
                    
<p>
                        Reverse-engineering how complex multicellular systems develop and function is a grand challenge for systems bioengineers. This challenge has motivated the creation of a suite of bioengineering tools to develop...
</p>
                    
                

<div>
<span>Authors:</span>
<span>Qinfeng Wu, Nilay Kumar, Vijay Velagala and Jeremiah J. Zartman</span>
</div>

<div>
<span>Citation:</span>
<em>Journal of Biological Engineering</em>
                    2019
<span>13</span>:33
</div>
<div>
<span><span>Content type:</span>Review</span>
<span>Published on:<span>23 April 2019</span></span>
</div>

                
                

<p>

                
                    
<ul>
<li>
<a href="//jbioleng.biomedcentral.com/articles/10.1186/s13036-019-0161-8" data-track-label="10.1186/s13036-019-0161-8">
<svg width="12" height="12" aria-hidden="true" focusable="false">
<use xlink:href="#icon-chevron-right"></use>
</svg>
<span>
<span>View</span>
                                        Full Text
</span>
</a>
</li>
<li>
<a data-track-label="10.1186/s13036-019-0161-8" href="//jbioleng.biomedcentral.com/counter/pdf/10.1186/s13036-019-0161-8.pdf">
<svg width="12" height="12" aria-hidden="true" focusable="false">
<use xlink:href="#icon-chevron-right"></use>
</svg>
<span>
<span>View</span>
                                        PDF
</span>
</a>
</li>
</ul>
                    
</p>
        

                

</li>
    
<li>

<h3>
                    
<a href="//jbioleng.biomedcentral.com/articles/10.1186/s13036-019-0155-6">Three-dimensional microengineered models of human cardiac diseases</a>
                    
</h3>
                
                    
<p>
                        In vitro three-dimensional (3D) microengineered tissue models have been the recent focus of pathophysiological studies, particularly in the field of cardiovascular research. These models, as classified by 3D b...
</p>
                    
                

<div>
<span>Authors:</span>
<span>Jaimeson Veldhuizen, Raymond Q. Migrino and Mehdi Nikkhah</span>
</div>

<div>
<span>Citation:</span>
<em>Journal of Biological Engineering</em>
                    2019
<span>13</span>:29
</div>
<div>
<span><span>Content type:</span>Review</span>
<span>Published on:<span>3 April 2019</span></span>
</div>

                
                

<p>

                
                    
<ul>
<li>
<a href="//jbioleng.biomedcentral.com/articles/10.1186/s13036-019-0155-6" data-track-label="10.1186/s13036-019-0155-6">
<svg width="12" height="12" aria-hidden="true" focusable="false">
<use xlink:href="#icon-chevron-right"></use>
</svg>
<span>
<span>View</span>
                                        Full Text
</span>
</a>
</li>
<li>
<a data-track-label="10.1186/s13036-019-0155-6" href="//jbioleng.biomedcentral.com/counter/pdf/10.1186/s13036-019-0155-6.pdf">
<svg width="12" height="12" aria-hidden="true" focusable="false">
<use xlink:href="#icon-chevron-right"></use>
</svg>
<span>
<span>View</span>
                                        PDF
</span>
</a>
</li>
</ul>
                    
</p>
               

                

</li>
    
<li>

<h3>
                    
<a href="//jbioleng.biomedcentral.com/articles/10.1186/s13036-019-0158-3">Microvascular bioengineering: a focus on pericytes</a>
                    
</h3>
                
                    
<p>
                        Capillaries within the microcirculation are essential for oxygen delivery and nutrient/waste exchange, among other critical functions. Microvascular bioengineering approaches have sought to recapitulate many k...
</p>
                    
                

<div>
<span>Authors:</span>
<span>Huaning Zhao and John C. Chappell</span>
</div>

<div>
<span>Citation:</span>
<em>Journal of Biological Engineering</em>
                    2019
<span>13</span>:26
</div>
<div>
<span><span>Content type:</span>Review</span>
<span>Published on:<span>29 March 2019</span></span>
</div>

                
                

<p>

                
                    
<ul>
<li>
<a href="//jbioleng.biomedcentral.com/articles/10.1186/s13036-019-0158-3" data-track-label="10.1186/s13036-019-0158-3">
<svg width="12" height="12" aria-hidden="true" focusable="false">
<use xlink:href="#icon-chevron-right"></use>
</svg>
<span>
<span>View</span>
                                        Full Text
</span>
</a>
</li>
<li>
<a data-track-label="10.1186/s13036-019-0158-3" href="//jbioleng.biomedcentral.com/counter/pdf/10.1186/s13036-019-0158-3.pdf">
<svg width="12" height="12" aria-hidden="true" focusable="false">
<use xlink:href="#icon-chevron-right"></use>
</svg>
<span>
<span>View</span>
                                        PDF
</span>
</a>
</li>
</ul>
                    
</p>
             

                

</li>
    
<li>

<h3>
                    
<a href="//jbioleng.biomedcentral.com/articles/10.1186/s13036-018-0130-7">Protein-based vehicles for biomimetic RNAi delivery</a>
                    
</h3>
                
                    
<p>
                        Broad translational success of RNA interference (RNAi) technology depends on the development of effective delivery approaches. To that end, researchers have developed a variety of strategies, including chemica...
</p>
                    
                

<div>
<span>Authors:</span>
<span>Alex Eli Pottash, Christopher Kuffner, Madeleine Noonan-Shueh and Steven M. Jay</span>
</div>

<div>
<span>Citation:</span>
<em>Journal of Biological Engineering</em>
                    2019
<span>13</span>:19
</div>
<div>
<span><span>Content type:</span>Review</span>
<span>Published on:<span>26 February 2019</span></span>
</div>

                
                


<p>
               
                    
<ul>
<li>
<a href="//jbioleng.biomedcentral.com/articles/10.1186/s13036-018-0130-7" data-track-label="10.1186/s13036-018-0130-7">
<svg width="12" height="12" aria-hidden="true" focusable="false">
<use xlink:href="#icon-chevron-right"></use>
</svg>
<span>
<span>View</span>
                                        Full Text
</span>
</a>
</li>
<li>
<a data-track-label="10.1186/s13036-018-0130-7" href="//jbioleng.biomedcentral.com/counter/pdf/10.1186/s13036-018-0130-7.pdf">
<svg width="12" height="12" aria-hidden="true" focusable="false">
<use xlink:href="#icon-chevron-right"></use>
</svg>
<span>
<span>View</span>
                                        PDF
</span>
</a>
</li>
</ul>
                    
                
</p>

                

</li>
    
<li>

<h3>
                    
<a href="//jbioleng.biomedcentral.com/articles/10.1186/s13036-019-0146-7">Thematic series on emerging leaders in biological engineering: convergence and new directions</a>
                    
</h3>
                
                    
<p>
                        Over the past two decades, the biological engineering research community has increased its efforts to promote ‘Convergence’. Many research endeavors have thus involved the synergy of multiple perspectives and ...
</p>
                    
                

<div>
<span>Authors:</span>
<span>Raj R. Rao</span>
</div>

<div>
<span>Citation:</span>
<em>Journal of Biological Engineering</em>
                    2019
<span>13</span>:17
</div>
<div>
<span><span>Content type:</span>Editorial</span>
<span>Published on:<span>21 February 2019</span></span>
</div>

                
                


<p>
                
                    
<ul>
<li>
<a href="//jbioleng.biomedcentral.com/articles/10.1186/s13036-019-0146-7" data-track-label="10.1186/s13036-019-0146-7">
<svg width="12" height="12" aria-hidden="true" focusable="false">
<use xlink:href="#icon-chevron-right"></use>
</svg>
<span>
<span>View</span>
                                        Full Text
</span>
</a>
</li>
<li>
<a data-track-label="10.1186/s13036-019-0146-7" href="//jbioleng.biomedcentral.com/counter/pdf/10.1186/s13036-019-0146-7.pdf">
<svg width="12" height="12" aria-hidden="true" focusable="false">
<use xlink:href="#icon-chevron-right"></use>
</svg>
<span>
<span>View</span>
                                        PDF
</span>
</a>
</li>
</ul>
                    
                
</p>

                

</li>
    
<li>

<h3>
                    
<a href="//jbioleng.biomedcentral.com/articles/10.1186/s13036-019-0145-8">Current trends in biomarker discovery and analysis tools for traumatic brain injury</a>
                    
</h3>
                
                    
<p>
                        Traumatic brain injury (TBI) affects 1.7 million people in the United States each year, causing lifelong functional deficits in cognition and behavior. The complex pathophysiology of neural injury is a primary...
</p>
                    
                

<div>
<span>Authors:</span>
<span>Briana I. Martinez and Sarah E. Stabenfeldt</span>
</div>

<div>
<span>Citation:</span>
<em>Journal of Biological Engineering</em>
                    2019
<span>13</span>:16
</div>
<div>
<span><span>Content type:</span>Review</span>
<span>Published on:<span>19 February 2019</span></span>
</div>

                
                


<p>
                
                    
<ul>
<li>
<a href="//jbioleng.biomedcentral.com/articles/10.1186/s13036-019-0145-8" data-track-label="10.1186/s13036-019-0145-8">
<svg width="12" height="12" aria-hidden="true" focusable="false">
<use xlink:href="#icon-chevron-right"></use>
</svg>
<span>
<span>View</span>
                                        Full Text
</span>
</a>
</li>
<li>
<a data-track-label="10.1186/s13036-019-0145-8" href="//jbioleng.biomedcentral.com/counter/pdf/10.1186/s13036-019-0145-8.pdf">
<svg width="12" height="12" aria-hidden="true" focusable="false">
<use xlink:href="#icon-chevron-right"></use>
</svg>
<span>
<span>View</span>
                                        PDF
</span>
</a>
</li>
</ul>
                    
</p>
               

                

</li>
    
<li>

<h3>
                    
<a href="//jbioleng.biomedcentral.com/articles/10.1186/s13036-018-0138-z">Inhibition of bacterial toxin recognition of membrane components as an anti-virulence strategy</a>
                    
</h3>
                
                    
<p>
                        Over recent years, the development of new antibiotics has not kept pace with the rate at which bacteria develop resistance to these drugs. For this reason, many research groups have begun to design and study a...
</p>
                    
                

<div>
<span>Authors:</span>
<span>Eric Krueger and Angela C. Brown</span>
</div>

<div>
<span>Citation:</span>
<em>Journal of Biological Engineering</em>
                    2019
<span>13</span>:4
</div>
<div>
<span><span>Content type:</span>Review</span>
<span>Published on:<span>19 February 2019</span></span>
</div>

                
                


<p>
                
                    
<ul>
<li>
<a href="//jbioleng.biomedcentral.com/articles/10.1186/s13036-018-0138-z" data-track-label="10.1186/s13036-018-0138-z">
<svg width="12" height="12" aria-hidden="true" focusable="false">
<use xlink:href="#icon-chevron-right"></use>
</svg>
<span>
<span>View</span>
                                        Full Text
</span>
</a>
</li>
<li>
<a data-track-label="10.1186/s13036-018-0138-z" href="//jbioleng.biomedcentral.com/counter/pdf/10.1186/s13036-018-0138-z.pdf">
<svg width="12" height="12" aria-hidden="true" focusable="false">
<use xlink:href="#icon-chevron-right"></use>
</svg>
<span>
<span>View</span>
                                        PDF
</span>
</a>
</li>
</ul>
                    
                
</p>

                

</li>
    
<li>

<h3>
                    
<a href="//jbioleng.biomedcentral.com/articles/10.1186/s13036-019-0144-9">Direct cell reprogramming for tissue engineering and regenerative medicine</a>
                    
</h3>
                
                    
<p>
                        Direct cell reprogramming, also called transdifferentiation, allows for the reprogramming of one somatic cell type directly into another, without the need to transition through an induced pluripotent state. Th...
</p>
                    
                

<div>
<span>Authors:</span>
<span>Alexander Grath and Guohao Dai</span>
</div>

<div>
<span>Citation:</span>
<em>Journal of Biological Engineering</em>
                    2019
<span>13</span>:14
</div>
<div>
<span><span>Content type:</span>Review</span>
<span>Published on:<span>13 February 2019</span></span>
</div>

                
                

<p>

                
                    
<ul>
<li>
<a href="//jbioleng.biomedcentral.com/articles/10.1186/s13036-019-0144-9" data-track-label="10.1186/s13036-019-0144-9">
<svg width="12" height="12" aria-hidden="true" focusable="false">
<use xlink:href="#icon-chevron-right"></use>
</svg>
<span>
<span>View</span>
                                        Full Text
</span>
</a>
</li>
<li>
<a data-track-label="10.1186/s13036-019-0144-9" href="//jbioleng.biomedcentral.com/counter/pdf/10.1186/s13036-019-0144-9.pdf">
<svg width="12" height="12" aria-hidden="true" focusable="false">
<use xlink:href="#icon-chevron-right"></use>
</svg>
<span>
<span>View</span>
                                        PDF
</span>
</a>
</li>
</ul>
                    
</p>
                

                

</li>
    
<li>

<h3>
                    
<a href="//jbioleng.biomedcentral.com/articles/10.1186/s13036-018-0136-1">Hydrophobins: multifunctional biosurfactants for interface engineering</a>
                    
</h3>
                
                    
<p>
                        Hydrophobins are highly surface-active proteins that have versatile potential as agents for interface engineering. Due to the large and growing number of unique hydrophobin sequences identified, there is growi...
</p>
                    
                

<div>
<span>Authors:</span>
<span>Bryan W. Berger and Nathanael D. Sallada</span>
</div>

<div>
<span>Citation:</span>
<em>Journal of Biological Engineering</em>
                    2019
<span>13</span>:10
</div>
<div>
<span><span>Content type:</span>Review</span>
<span>Published on:<span>23 January 2019</span></span>
</div>

                
                


<p>
                
                    
<ul>
<li>
<a href="//jbioleng.biomedcentral.com/articles/10.1186/s13036-018-0136-1" data-track-label="10.1186/s13036-018-0136-1">
<svg width="12" height="12" aria-hidden="true" focusable="false">
<use xlink:href="#icon-chevron-right"></use>
</svg>
<span>
<span>View</span>
                                        Full Text
</span>
</a>
</li>
<li>
<a data-track-label="10.1186/s13036-018-0136-1" href="//jbioleng.biomedcentral.com/counter/pdf/10.1186/s13036-018-0136-1.pdf">
<svg width="12" height="12" aria-hidden="true" focusable="false">
<use xlink:href="#icon-chevron-right"></use>
</svg>
<span>
<span>View</span>
                                        PDF
</span>
</a>
</li>
</ul>
                    
                
</p>

                

</li>
    
<li>

<h3>
                    
<a href="//jbioleng.biomedcentral.com/articles/10.1186/s13036-019-0140-0">Nucleic acid delivery to mesenchymal stem cells: a review of nonviral methods and applications</a>
                    
</h3>
                
                    
<p>
                        Mesenchymal stem cells (MSCs) are multipotent stem cells that can be isolated and expanded from many tissues, and are being investigated for use in cell therapies. Though MSC therapies have demonstrated some s...
</p>
                    
                

<div>
<span>Authors:</span>
<span>Andrew Hamann, Albert Nguyen and Angela K. Pannier</span>
</div>

<div>
<span>Citation:</span>
<em>Journal of Biological Engineering</em>
                    2019
<span>13</span>:7
</div>
<div>
<span><span>Content type:</span>Review</span>
<span>Published on:<span>18 January 2019</span></span>
</div>

                
                


<p>
               
                    
<ul>
<li>
<a href="//jbioleng.biomedcentral.com/articles/10.1186/s13036-019-0140-0" data-track-label="10.1186/s13036-019-0140-0">
<svg width="12" height="12" aria-hidden="true" focusable="false">
<use xlink:href="#icon-chevron-right"></use>
</svg>
<span>
<span>View</span>
                                        Full Text
</span>
</a>
</li>
<li>
<a data-track-label="10.1186/s13036-019-0140-0" href="//jbioleng.biomedcentral.com/counter/pdf/10.1186/s13036-019-0140-0.pdf">
<svg width="12" height="12" aria-hidden="true" focusable="false">
<use xlink:href="#icon-chevron-right"></use>
</svg>
<span>
<span>View</span>
                                        PDF
</span>
</a>
</li>
</ul>
                    
                
</p>

                

</li>
    
<li>

<h3>
                    
<a href="//jbioleng.biomedcentral.com/articles/10.1186/s13036-018-0131-6">Coatings on mammalian cells: interfacing cells with their environment</a>
                    
</h3>
                
                    
<p>
                        The research community is intent on harnessing increasingly complex biological building blocks. At present, cells represent a highly functional component for integration into higher order systems. In this revi...
</p>
                    
                

<div>
<span>Authors:</span>
<span>Kara A. Davis, Pei-Jung Wu, Calvin F. Cahall, Cong Li, Anuhya Gottipati and Brad J. Berron</span>
</div>

<div>
<span>Citation:</span>
<em>Journal of Biological Engineering</em>
                    2019
<span>13</span>:5
</div>
<div>
<span><span>Content type:</span>Review</span>
<span>Published on:<span>17 January 2019</span></span>
</div>

                
                

<p>

                
                    
<ul>
<li>
<a href="//jbioleng.biomedcentral.com/articles/10.1186/s13036-018-0131-6" data-track-label="10.1186/s13036-018-0131-6">
<svg width="12" height="12" aria-hidden="true" focusable="false">
<use xlink:href="#icon-chevron-right"></use>
</svg>
<span>
<span>View</span>
                                        Full Text
</span>
</a>
</li>
<li>
<a data-track-label="10.1186/s13036-018-0131-6" href="//jbioleng.biomedcentral.com/counter/pdf/10.1186/s13036-018-0131-6.pdf">
<svg width="12" height="12" aria-hidden="true" focusable="false">
<use xlink:href="#icon-chevron-right"></use>
</svg>
<span>
<span>View</span>
                                        PDF
</span>
</a>
</li>
</ul>
                    
                
</p>

                

</li>
    
<li>

<h3>
                    
<a href="//jbioleng.biomedcentral.com/articles/10.1186/s13036-018-0137-0">Bioengineered models to study tumor dormancy</a>
                    
</h3>
                
                    
<p>
                        The onset of cancer metastasis is the defining event in cancer progression when the disease is considered lethal. The ability of metastatic cancer cells to stay dormant for extended time periods and reawaken a...
</p>
                    
                

<div>
<span>Authors:</span>
<span>Shreyas S. Rao, Raghu Vamsi Kondapaneni and Akshay A. Narkhede</span>
</div>

<div>
<span>Citation:</span>
<em>Journal of Biological Engineering</em>
                    2019
<span>13</span>:3
</div>
<div>
<span><span>Content type:</span>Review</span>
<span>Published on:<span>10 January 2019</span></span>
</div>

                
                

<p>

                
                    
<ul>
<li>
<a href="//jbioleng.biomedcentral.com/articles/10.1186/s13036-018-0137-0" data-track-label="10.1186/s13036-018-0137-0">
<svg width="12" height="12" aria-hidden="true" focusable="false">
<use xlink:href="#icon-chevron-right"></use>
</svg>
<span>
<span>View</span>
                                        Full Text
</span>
</a>
</li>
<li>
<a data-track-label="10.1186/s13036-018-0137-0" href="//jbioleng.biomedcentral.com/counter/pdf/10.1186/s13036-018-0137-0.pdf">
<svg width="12" height="12" aria-hidden="true" focusable="false">
<use xlink:href="#icon-chevron-right"></use>
</svg>
<span>
<span>View</span>
                                        PDF
</span>
</a>
</li>
</ul>
                    
</p>
              

                

</li>
    
<li>

<h3>
                    
<a href="//jbioleng.biomedcentral.com/articles/10.1186/s13036-018-0134-3">Follicle development as an orchestrated signaling network in a 3D organoid</a>
                    
</h3>
                
                    
<p>
                        The ovarian follicle is the structural and functional unit of the ovary, composed of the female gamete (the oocyte) and supportive somatic cells. Follicles are not only the source of a female’s germ cell suppl...
</p>
                    
                

<div>
<span>Authors:</span>
<span>Andrea S. K. Jones and Ariella Shikanov</span>
</div>

<div>
<span>Citation:</span>
<em>Journal of Biological Engineering</em>
                    2019
<span>13</span>:2
</div>
<div>
<span><span>Content type:</span>Review</span>
<span>Published on:<span>9 January 2019</span></span>
</div>

                
                


<p>
                
                    
<ul>
<li>
<a href="//jbioleng.biomedcentral.com/articles/10.1186/s13036-018-0134-3" data-track-label="10.1186/s13036-018-0134-3">
<svg width="12" height="12" aria-hidden="true" focusable="false">
<use xlink:href="#icon-chevron-right"></use>
</svg>
<span>
<span>View</span>
                                        Full Text
</span>
</a>
</li>
<li>
<a data-track-label="10.1186/s13036-018-0134-3" href="//jbioleng.biomedcentral.com/counter/pdf/10.1186/s13036-018-0134-3.pdf">
<svg width="12" height="12" aria-hidden="true" focusable="false">
<use xlink:href="#icon-chevron-right"></use>
</svg>
<span>
<span>View</span>
                                        PDF
</span>
</a>
</li>
</ul>
                    
                
</p>

                

</li>
    
<li>

<h3>
                    
<a href="//jbioleng.biomedcentral.com/articles/10.1186/s13036-018-0125-4">The effects of low-dose radiation on articular cartilage: a review</a>
                    
</h3>
                
                    
<p>
                        Articular cartilage is a specialized connective tissue, predominately composed of water, collagen, and proteoglycans, that provides a smooth, lubricated surface for articulation in joints. It has long been con...
</p>
                    
                

<div>
<span>Authors:</span>
<span>Hannah Cash and Delphine Dean</span>
</div>

<div>
<span>Citation:</span>
<em>Journal of Biological Engineering</em>
                    2019
<span>13</span>:1
</div>
<div>
<span><span>Content type:</span>Review</span>
<span>Published on:<span>7 January 2019</span></span>
</div>

                
                


<p>
                
                    
<ul>
<li>
<a href="//jbioleng.biomedcentral.com/articles/10.1186/s13036-018-0125-4" data-track-label="10.1186/s13036-018-0125-4">
<svg width="12" height="12" aria-hidden="true" focusable="false">
<use xlink:href="#icon-chevron-right"></use>
</svg>
<span>
<span>View</span>
                                        Full Text
</span>
</a>
</li>
<li>
<a data-track-label="10.1186/s13036-018-0125-4" href="//jbioleng.biomedcentral.com/counter/pdf/10.1186/s13036-018-0125-4.pdf">
<svg width="12" height="12" aria-hidden="true" focusable="false">
<use xlink:href="#icon-chevron-right"></use>
</svg>
<span>
<span>View</span>
                                        PDF
</span>
</a>
</li>
</ul>
                    
</p>
             

                

</li>
    
<li>

<h3>
                    
<a href="//jbioleng.biomedcentral.com/articles/10.1186/s13036-018-0120-9">Engineered<em>In Vitro</em> Models of Tumor Dormancy and Reactivation</a>
                    
</h3>
                
                    
<p>
                        Metastatic recurrence is a major hurdle to overcome for successful control of cancer-associated death. Residual tumor cells in the primary site, or disseminated tumor cells in secondary sites, can lie in a dor...
</p>
                    
                

<div>
<span>Authors:</span>
<span>Shantanu Pradhan, John L. Sperduto, Cindy J. Farino and John H. Slater</span>
</div>

<div>
<span>Citation:</span>
<em>Journal of Biological Engineering</em>
                    2018
<span>12</span>:37
</div>
<div>
<span><span>Content type:</span>Review</span>
<span>Published on:<span>27 December 2018</span></span>
</div>

                
                


<p>
               
                    
<ul>
<li>
<a href="//jbioleng.biomedcentral.com/articles/10.1186/s13036-018-0120-9" data-track-label="10.1186/s13036-018-0120-9">
<svg width="12" height="12" aria-hidden="true" focusable="false">
<use xlink:href="#icon-chevron-right"></use>
</svg>
<span>
<span>View</span>
                                        Full Text
</span>
</a>
</li>
<li>
<a data-track-label="10.1186/s13036-018-0120-9" href="//jbioleng.biomedcentral.com/counter/pdf/10.1186/s13036-018-0120-9.pdf">
<svg width="12" height="12" aria-hidden="true" focusable="false">
<use xlink:href="#icon-chevron-right"></use>
</svg>
<span>
<span>View</span>
                                        PDF
</span>
</a>
</li>
</ul>
                    
                
</p>

                

</li>
    
<li>

<h3>
                    
<a href="//jbioleng.biomedcentral.com/articles/10.1186/s13036-018-0122-7">Lymphatic Tissue Engineering and Regeneration</a>
                    
</h3>
                
                    
<p>
                        The lymphatic system is a major circulatory system within the body, responsible for the transport of interstitial fluid, waste products, immune cells, and proteins. Compared to other physiological systems, the...
</p>
                    
                

<div>
<span>Authors:</span>
<span>Laura Alderfer, Alicia Wei and Donny Hanjaya-Putra</span>
</div>

<div>
<span>Citation:</span>
<em>Journal of Biological Engineering</em>
                    2018
<span>12</span>:32
</div>
<div>
<span><span>Content type:</span>Review</span>
<span>Published on:<span>17 December 2018</span></span>
</div>

                
                

<p>

                
                    
<ul>
<li>
<a href="//jbioleng.biomedcentral.com/articles/10.1186/s13036-018-0122-7" data-track-label="10.1186/s13036-018-0122-7">
<svg width="12" height="12" aria-hidden="true" focusable="false">
<use xlink:href="#icon-chevron-right"></use>
</svg>
<span>
<span>View</span>
                                        Full Text
</span>
</a>
</li>
<li>
<a data-track-label="10.1186/s13036-018-0122-7" href="//jbioleng.biomedcentral.com/counter/pdf/10.1186/s13036-018-0122-7.pdf">
<svg width="12" height="12" aria-hidden="true" focusable="false">
<use xlink:href="#icon-chevron-right"></use>
</svg>
<span>
<span>View</span>
                                        PDF
</span>
</a>
</li>
</ul>
                    
                
</p>

                

</li>
    
<li>

<h3>
                    
<a href="//jbioleng.biomedcentral.com/articles/10.1186/s13036-018-0126-3">An electrically-controlled programmable microfluidic concentration waveform generator</a>
                    
</h3>
                
                    
<p>
                        Biological systems have complicated environmental conditions that vary both spatially and temporally. It becomes necessary to impose time-varying soluble factor concentrations to study such systems, including ...
</p>
                    
                

<div>
<span>Authors:</span>
<span>Joshua Garrison, Zidong Li, Barath Palanisamy, Ling Wang and Erkin Seker</span>
</div>

<div>
<span>Citation:</span>
<em>Journal of Biological Engineering</em>
                    2018
<span>12</span>:31
</div>
<div>
<span><span>Content type:</span>Research</span>
<span>Published on:<span>14 December 2018</span></span>
</div>

                
                

<p>

                
                    
<ul>
<li>
<a href="//jbioleng.biomedcentral.com/articles/10.1186/s13036-018-0126-3" data-track-label="10.1186/s13036-018-0126-3">
<svg width="12" height="12" aria-hidden="true" focusable="false">
<use xlink:href="#icon-chevron-right"></use>
</svg>
<span>
<span>View</span>
                                        Full Text
</span>
</a>
</li>
<li>
<a data-track-label="10.1186/s13036-018-0126-3" href="//jbioleng.biomedcentral.com/counter/pdf/10.1186/s13036-018-0126-3.pdf">
<svg width="12" height="12" aria-hidden="true" focusable="false">
<use xlink:href="#icon-chevron-right"></use>
</svg>
<span>
<span>View</span>
                                        PDF
</span>
</a>
</li>
</ul>
                    
                
</p>

                

</li>
    
<li>

<h3>
                    
<a href="//jbioleng.biomedcentral.com/articles/10.1186/s13036-018-0124-5">Imaging retinal melanin: a review of current technologies</a>
                    
</h3>
                
                    
<p>
                        The retinal pigment epithelium (RPE) is essential to the health of the retina and the proper functioning of the photoreceptors. The RPE is rich in melanosomes, which contain the pigment melanin. Changes in RPE...
</p>
                    
                

<div>
<span>Authors:</span>
<span>Maryse Lapierre-Landry, Joseph Carroll and Melissa C. Skala</span>
</div>

<div>
<span>Citation:</span>
<em>Journal of Biological Engineering</em>
                    2018
<span>12</span>:29
</div>
<div>
<span><span>Content type:</span>Review</span>
<span>Published on:<span>4 December 2018</span></span>
</div>

                
                

<p>

                
                    
<ul>
<li>
<a href="//jbioleng.biomedcentral.com/articles/10.1186/s13036-018-0124-5" data-track-label="10.1186/s13036-018-0124-5">
<svg width="12" height="12" aria-hidden="true" focusable="false">
<use xlink:href="#icon-chevron-right"></use>
</svg>
<span>
<span>View</span>
                                        Full Text
</span>
</a>
</li>
<li>
<a data-track-label="10.1186/s13036-018-0124-5" href="//jbioleng.biomedcentral.com/counter/pdf/10.1186/s13036-018-0124-5.pdf">
<svg width="12" height="12" aria-hidden="true" focusable="false">
<use xlink:href="#icon-chevron-right"></use>
</svg>
<span>
<span>View</span>
                                        PDF
</span>
</a>
</li>
</ul>
                    
                
</p>

                

</li>
    
<li>

<h3>
                    
<a href="//jbioleng.biomedcentral.com/articles/10.1186/s13036-018-0123-6">Cell surface engineering and application in cell delivery to heart diseases</a>
                    
</h3>
                
                    
<p>
                        Cell-based therapy has expanded its influence in cancer immunotherapy, regenerative medicine, and tissue engineering. Due to their secretory functions, differentiation capabilities, specific homing effects thr...
</p>
                    
                

<div>
<span>Authors:</span>
<span>Daniel Y. Lee, Byung-Hyun Cha, Minjin Jung, Angela S. Kim, David A. Bull and Young-Wook Won</span>
</div>

<div>
<span>Citation:</span>
<em>Journal of Biological Engineering</em>
                    2018
<span>12</span>:28
</div>
<div>
<span><span>Content type:</span>Review</span>
<span>Published on:<span>4 December 2018</span></span>
</div>

                
                

<p>

                
                    
<ul>
<li>
<a href="//jbioleng.biomedcentral.com/articles/10.1186/s13036-018-0123-6" data-track-label="10.1186/s13036-018-0123-6">
<svg width="12" height="12" aria-hidden="true" focusable="false">
<use xlink:href="#icon-chevron-right"></use>
</svg>
<span>
<span>View</span>
                                        Full Text
</span>
</a>
</li>
<li>
<a data-track-label="10.1186/s13036-018-0123-6" href="//jbioleng.biomedcentral.com/counter/pdf/10.1186/s13036-018-0123-6.pdf">
<svg width="12" height="12" aria-hidden="true" focusable="false">
<use xlink:href="#icon-chevron-right"></use>
</svg>
<span>
<span>View</span>
                                        PDF
</span>
</a>
</li>
</ul>
                    
</p>
                

                

</li>
    
<li>

<h3>
                    
<a href="//jbioleng.biomedcentral.com/articles/10.1186/s13036-018-0116-5">Bioconversion of cheese whey permeate into fungal oil by<em>Mucor circinelloides</em></a>
                    
</h3>
                
                    
<p>
                        Oleaginous fungi are efficient tools to convert agricultural waste streams into valuable components. The filamentous fungus<em>Mucor circinelloides</em> was cultivated in whey permeate, a byproduct from cheese production...
</p>
                    
                

<div>
<span>Authors:</span>
<span>Lauryn G. Chan, Joshua L. Cohen, Gulustan Ozturk, Marie Hennebelle, Ameer Y. Taha and Juliana Maria L. N. de Moura Bell</span>
</div>

<div>
<span>Citation:</span>
<em>Journal of Biological Engineering</em>
                    2018
<span>12</span>:25
</div>
<div>
<span><span>Content type:</span>Research</span>
<span>Published on:<span>14 November 2018</span></span>
</div>

                
                

<p>

                
                    
<ul>
<li>
<a href="//jbioleng.biomedcentral.com/articles/10.1186/s13036-018-0116-5" data-track-label="10.1186/s13036-018-0116-5">
<svg width="12" height="12" aria-hidden="true" focusable="false">
<use xlink:href="#icon-chevron-right"></use>
</svg>
<span>
<span>View</span>
                                        Full Text
</span>
</a>
</li>
<li>
<a data-track-label="10.1186/s13036-018-0116-5" href="//jbioleng.biomedcentral.com/counter/pdf/10.1186/s13036-018-0116-5.pdf">
<svg width="12" height="12" aria-hidden="true" focusable="false">
<use xlink:href="#icon-chevron-right"></use>
</svg>
<span>
<span>View</span>
                                        PDF
</span>
</a>
</li>
</ul>
                    
</p>
              

                

</li>


---

[Infographic: In Vitro Diagnostics (IVD) in the U.S. and Latin America](https://kaloramainformation.com/infographic-in-vitro-diagnostics-ivd-in-the-u-s-and-latin-america/)
kaloramainformation.com|1448 × 2048 png|Image may be subject to copyright.

![输入图片说明](https://foruda.gitee.com/images/1677426830188687361/3601020d_5631341.png "IVD-USA-and-Latin-America-1448x2048.png")

[Synthetic Biology Expands and Grows](https://www.genengnews.com/magazine/synthetic-biology-expands-and-grows/)
GENGEN|3000 × 3000 jpeg|Image may be subject to copyright.

![输入图片说明](https://foruda.gitee.com/images/1677427823357241106/8de9c4a6_5631341.jpeg "Getty_587805514_iaremenko_DoubleHelix1024261165.jpg")

https://qeprize.org/news/speeding-dna-manufacturing-biological-engineering

![输入图片说明](https://foruda.gitee.com/images/1677429534234783784/a9f4b477_5631341.jpeg "edit_7d37a092c54e610ffd7611a609801300.jpg")