https://babbledabbledo.com/

- [STEAM PLAY & LEARN](https://gitee.com/gzk-0913/siger/issues/I6D1K1#note_16073251_link)
- [SHOP](https://gitee.com/gzk-0913/siger/issues/I6D1K1#note_16076307_link)： https://babble-dabble-do.myshopify.com/
- [Holiday Flexagon](https://www.teacherspayteachers.com/Product/Holiday-Flexagon-4267971)  | [Teachers Pay Teachers](https://gitee.com/gzk-0913/siger/issues/I6D1K1#note_16072126_link)
- [MAKE MY DAY: VISIONS](https://gitee.com/gzk-0913/siger/issues/I6D1K1#note_16077859_link)

<p><img width="706px" src="https://foruda.gitee.com/images/1675712224916530576/48b00c5a_5631341.jpeg" title="526babbledabbledo.jpg"></p>

> 这是一个典型的学习帖，调研专题，这要感谢 Copy 2 China 的码更人，尽管下面的小站很亮眼，经过调研，确实我们也可以做的不错，不只限于 Copy。当然亮眼的原因我也剧透了，奇妙的三角，希望有机会给同学们分享这个小故事。先上笔记，这是老师调研的顺序，然后是根据之后的分类（封面前面的大纲）稍微调整下（主要是提前了SHOP和TFT呼应下），100% 转载到正文，也省了同学们跟着老师的笔记再迷了路。（提纲的链接请在电脑端打开，手机端定位会混乱。）

封面是两个主要线索，STEAM 著作封面，以及示范游戏的合体，希望同学们能够举一反三，涂鸦自己的图形。

> 笔记的顺序首先是搜索，以风筝为线索找到了国内的帖子，通过遗漏的一张原图，找到了站点，逐次展开后了解了脉络，然后是 TFT 课程寄售，原站各个栏目，最后是原站的产出 SHOP，本次调研的主要收获，顺道看了下作者的恩爱秀小公司。

【[笔记](https://gitee.com/gzk-0913/siger/issues/I6D1K1#note_16069270_link)】https://babbledabbledo.com/

- [bing Babble Dabble Do](https://gitee.com/gzk-0913/siger/issues/I6D1K1#note_16070188_link) https://babbledabbledo.com

  - [hello, wonderful](https://gitee.com/gzk-0913/siger/issues/I6D1K1#note_16070794_link) [10 FUN KITE MAKING IDEAS FOR KIDS](https://www.hellowonderful.co/post/10-fun-ways-to-make-a-kite/)

  - [Holiday Flexagon](https://www.teacherspayteachers.com/Product/Holiday-Flexagon-4267971)  | [Teachers Pay Teachers](https://gitee.com/gzk-0913/siger/issues/I6D1K1#note_16072126_link)

    - [Dear Ana: re: Holiday Flexagon](https://gitee.com/gzk-0913/siger/issues/I6D1K1#note_16072640_link)

    - [MY PRODUCTS By Babble Dabble Do](https://gitee.com/gzk-0913/siger/issues/I6D1K1#note_16072140_link)

    - [other works of Holiday Flexagon](https://gitee.com/gzk-0913/siger/issues/I6D1K1#note_16075429_link)

      [![输入图片说明](https://foruda.gitee.com/images/1675685824636423822/28fa0f75_5631341.png "屏幕截图")](https://babbledabbledo.com/make-a-holiday-flextangle/)  [![输入图片说明](https://foruda.gitee.com/images/1675685834383905355/588777ff_5631341.png "屏幕截图")](https://babbledabbledo.com/how-to-make-a-vote-flexagon/) [![输入图片说明](https://foruda.gitee.com/images/1675685842616899094/eab6cc24_5631341.png "屏幕截图")](https://babbledabbledo.com/creative-election-day-activities-for-kids/)

    - [why babbledabbledo triangles background](https://gitee.com/gzk-0913/siger/issues/I6D1K1#note_16074544_link) 

- [Privacy/Disclosure Policy](https://gitee.com/gzk-0913/siger/issues/I6D1K1#note_16075534_link) [隐私/信息披露政策](https://gitee.com/gzk-0913/siger/issues/I6D1K1#note_16075626_link)

- [Terms & Conditions, Shipping, & Refunds](https://gitee.com/gzk-0913/siger/issues/I6D1K1#note_16075775_link) [条款和条件，运输和退款](https://gitee.com/gzk-0913/siger/issues/I6D1K1#note_16075891_link)

- [MAKE MY DAY: VISIONS](https://gitee.com/gzk-0913/siger/issues/I6D1K1#note_16077859_link)

- [STEAM Play & Learn: 20 fun step-by-step preschool projects about science, technology](https://gitee.com/gzk-0913/siger/issues/I6D1K1#note_16072879_link), …

  - [12本英美孩子必备的STEAM教育书籍，你家孩子读过吗？-幼儿编程入门](https://gitee.com/gzk-0913/siger/issues/I6D1K1#note_16072910_link)

  - [Book Review: STEAM PLAY & LEARN](https://gitee.com/gzk-0913/siger/issues/I6D1K1#note_16073124_link)

  - [STEAM PLAY & LEARN](https://gitee.com/gzk-0913/siger/issues/I6D1K1#note_16073251_link)

  - [cover of siger](https://gitee.com/gzk-0913/siger/issues/I6D1K1#note_16078734_link)

- [SHOP](https://gitee.com/gzk-0913/siger/issues/I6D1K1#note_16076307_link)： https://babble-dabble-do.myshopify.com/

# https://babbledabbledo.com/ ![输入图片说明](https://foruda.gitee.com/images/1675673342167276125/91d71f40_5631341.png "屏幕截图") 

![输入图片说明](https://foruda.gitee.com/images/1675673586291982579/c8c682d7_5631341.png "屏幕截图")

<img src="https://foruda.gitee.com/images/1675673305936738469/a9ec5e23_5631341.png" title="https://cdn.babbledabbledo.com/wp-content/uploads/2015/01/GRAPH-PAPER-BACK.jpg" align="right">

- SHOP
- CLASSES
- Art for Kids
  - 12 Easy Art Ideas: Start Here
  - 20+ Preschool Art Projects
  - 75 of the Best Arts and Crafts for Kids to Enjoy Creating
  - 80 Easy Creative Projects for Kids
  - Art for Kids: All Projects
 
- Science for Kids
  - 80 of the Best Physics Projects for Clever Kids
  - 30+ Science Fair Projects
  - 50 Chemistry Projects That Will Amaze Kids!
  - 20 Science Projects for Preschoolers
  - 20 Science Experiments for Kids
  - 30 Simple Kitchen Science Experiments for Curious Kids
  - Science for Kids: All Projects
 
- Engineering for Kids
  - 25 STEAM Projects for Kids
  - 100 of the Best Recycled Crafts for Kids
  - STEAM Toy Guide: Smart Toys for Little Designers
  - Engineering for Kids: All Projects

- Design for Kids
  - 60+ Amazing Paper Crafts For Kids and Adults
  - 40+ DIY Toys
  - Design for Kids: All Projects

[![输入图片说明](https://foruda.gitee.com/images/1675674222646995711/a344038c_5631341.png "屏幕截图")](https://babbledabbledo.com/glue-batik/)

[![输入图片说明](https://foruda.gitee.com/images/1675674267135532933/c5b523d6_5631341.png "屏幕截图")](https://babbledabbledo.com/get-started-steam-projects-home/)

[![输入图片说明](https://foruda.gitee.com/images/1675674299996682392/edb614fd_5631341.png "屏幕截图")](https://babbledabbledo.com/5-easy-ways-find-time-family-creativity/)

[![输入图片说明](https://foruda.gitee.com/images/1675674337975883639/34667b88_5631341.png "屏幕截图")](https://babbledabbledo.com/design-for-kids-recycled-handmade-journals/)

[![输入图片说明](https://foruda.gitee.com/images/1675674364815194301/70f65014_5631341.png "屏幕截图")](https://babbledabbledo.com/diy-toys-frame-lacers/)

[![输入图片说明](https://foruda.gitee.com/images/1675674392258128298/53b707c5_5631341.png "屏幕截图")](https://babbledabbledo.com/simple-science-experiment-diy-bouncy-balls/)

---

![输入图片说明](https://foruda.gitee.com/images/1675673874715269236/7a8b8e00_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1675673769529172617/c32d8311_5631341.png "屏幕截图")

©2022 Babble Dabble Do. All rights reserved. You may not take images or content from this site without written permission.

### Make A Holiday Flextangle!

November 16, 2022 by Ana Dziengel 2 Comments

![输入图片说明](https://foruda.gitee.com/images/1675673993889695630/f16974c1_5631341.png "屏幕截图")

> For today’s project I took the most popular project on my blog Flextangles and turned it into a holiday craft!

- Babble Dabble Do
  https://babbledabbledo.com

  > 2023年1月31日 · Babble Dabble Do. SHOP; CLASSES; Art for Kids. 12 Easy Art Ideas: Start Here; 20+ Preschool Art Projects; 75 of the Best Arts and Crafts for Kids to Enjoy …

    - Privacy/Disclosure Policy
      Privacy/Disclosure Policy - Babble Dabble Do

    - CLASSES
      Current class offerings in our Afterschool STEAM program Classes are only open …

    - Art for Kids
      Art for Kids - Babble Dabble Do

    - Science for Kids
      Engaging (and magical) science projects for children ages 3-12.

    - Engineering for Kids
      Engineering for Kids - Babble Dabble Do

    - Design for Kids
      Creative project ideas for kids combining art, engineering, and science.

- babbledabble- 必应在线翻译 cn.bing.com/translator
  > 喋喋不休

- Language for Life - Babbel.com
  https://www.babbel.com

  > Babbel is the new way to learn a foreign language. The comprehensive learning system combines effective education methods with state-of-the-art technology. Interactive online …

- Including results for babble dabble.
  Do you want results only for babbledabble?

  ![输入图片说明](https://foruda.gitee.com/images/1675674861201236799/3644ed09_5631341.png "屏幕截图") ![输入图片说明](https://foruda.gitee.com/images/1675674873419850989/f2cc5a2e_5631341.png "屏幕截图") ![输入图片说明](https://foruda.gitee.com/images/1675674898608346172/1fbd820a_5631341.png "屏幕截图") ![输入图片说明](https://foruda.gitee.com/images/1675674942932968010/2a74e69f_5631341.png "屏幕截图") ![输入图片说明](https://foruda.gitee.com/images/1675674954110471471/dac546fd_5631341.png "屏幕截图") ![输入图片说明](https://foruda.gitee.com/images/1675674974875473971/30c36bfc_5631341.png "屏幕截图") ![输入图片说明](https://foruda.gitee.com/images/1675675005687057587/2f5aacc7_5631341.png "屏幕截图") ![输入图片说明](https://foruda.gitee.com/images/1675676466950831490/c94fd067_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1675675452758032756/efb8d4ed_5631341.png "屏幕截图")

# [10 FUN KITE MAKING IDEAS FOR KIDS](https://www.hellowonderful.co/post/10-fun-ways-to-make-a-kite/)

By: AuthorAgnes

Posted onLast updated: March 2, 2021

CategoriesCreate, Latest

Originally posted on 12/06/2015

In the summer months, playing outside becomes a fun daily adventure. A kite makes for a fantastic way to play outdoors and also makes an entertaining summer craft. 

We’ve rounded up 10 creative kite design ideas for you to make your own DIY kite. Below are 10 different ways to make this nostalgic summer toy – from cool tetrahedral designs to colorful confetti kites! Then there are also simple folded paper kites, or ones kids can fill in with their artistic doodles. 

Many of these can actually fly high in the sky which makes them a toy worth making. And while you are playing outdoors check out my guide on [summer activities](https://www.hellowonderful.co/post/8-AWESOME-WATER-SUMMER-ACTIVITIES) to keep you inspired while outside.

### Creative Kite Making Ideas

Do you know how to make a kite? If not, don’t worry, we’ve got you covered. With the creative kite design ideas below, you’ll want to try them all! Most of the kites will keep you occupied all day as you [play outdoors](https://www.hellowonderful.co/post/10-OUTRAGEOUSLY-FUN-WAYS-TO-PLAY-OUTDOORS), others are great for the creative kids to practice their design skills.

![输入图片说明](https://foruda.gitee.com/images/1675675138911944372/9c8dc702_5631341.png "屏幕截图")

### Simple Paper Kite (via [Inner Child Fun](http://innerchildfun.com/2015/04/how-to-make-a-simple-kite.html))

Paper kite making is fun and simple. This paper kite can easily be made by a kid in less than 5 minutes. And the bonus is you’ll probably already have all the materials needed at home.

![输入图片说明](https://foruda.gitee.com/images/1675675201074452348/706198b6_5631341.png "屏幕截图")

### Tetrahedral Kite (via [Instructables](http://www.instructables.com/id/10-Cell-Treahedral-Kite/))

Make this impressive 10-cell tetrahedral creative kite that’s an eye-catcher in the sky. You’ll need simple materials like straw and wrapping paper.

![输入图片说明](https://foruda.gitee.com/images/1675675228456861885/8db533d0_5631341.png "屏幕截图")

### Confetti Kite (via [Parent Savvy](http://parentsavvy.com/articles-and-blog/309/))

This vibrant confetti kite is one of the most innovative kite-making ideas. Kids can get hands-on in making this decorative kite which looks so pretty flying high up in the sky.

![输入图片说明](https://foruda.gitee.com/images/1675675241256265833/457c3d30_5631341.png "屏幕截图")

- KID-MADE DECORATIVE DRIFTWOOD KITE   By: AuthorAgnes
  Posted onLast updated: February 22, 2018
  CategoriesCreate, Latest

What kid doesn’t love to collect sticks? We are always trying to think of ways to display their treasures!

![输入图片说明](https://foruda.gitee.com/images/1675675705476512671/5961c386_5631341.png "屏幕截图")

Materials:

- 2 or more pieces of large paper (we used watercolour paper, but anything durable could work, such as poster board)
- Driftwood or sticks
- Glue gun (to be used by an adult) or craft glue
- Paint (we used watercolour and a bit of craft grade acrylic)
- Painting tools (brush, old gift card)
- Cotton rope
- Scissors
- Beads or wooden balls

 **Painted Beads**  (Optional: If you want to paint them to coordinate with your kite)

![输入图片说明](https://foruda.gitee.com/images/1675675828011428850/12921bae_5631341.png "屏幕截图")

- Wooden beads
- Acrylic paint
- Floral foam
- BBQ skewers
- Pony beads
- Paint brush or brushes

We also find these items helpful:

<p><img height="99px" src="https://foruda.gitee.com/images/1675675865270250746/003d3863_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1675675947843529966/76a12d17_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1675675954303463498/98546d6e_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1675675962894697130/c851587c_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1675675966922258426/afbd2481_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1675675973614503705/f0c3e491_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1675675979809936720/7eb5390d_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1675675986266815763/2c30f00e_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1675675992906022575/fd76062a_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1675676000548009942/97a1f2d2_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1675676006252544221/a2363445_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1675676012804418262/007c5066_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1675676018792409842/b3f4fd9b_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1675676025925617595/c68b55ff_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1675676033954253606/4a7dbbb0_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1675676039630984280/963ed761_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1675676046581833056/3a9db40d_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1675676054336755713/01cfb9d6_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1675676059266964945/b5b14ac5_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1675676067096873517/5fc5d9c0_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1675676081610159493/f1d4ae3e_5631341.png"><img height="99px" src="https://foruda.gitee.com/images/1675676088104417204/a7226bdb_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1675676094365372487/20385c48_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1675676100777355608/57b64191_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1675676106633424216/8a8f246f_5631341.png"><img height="99px" src="https://foruda.gitee.com/images/1675676117003589079/c246cff0_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1675676124299935984/4092fc74_5631341.png"><img height="99px" src="https://foruda.gitee.com/images/1675676130437696662/21202d40_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1675676136091706843/dafb0617_5631341.png"><img height="99px" src="https://foruda.gitee.com/images/1675676142273543254/5f3bb918_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1675676163930199216/9c086b37_5631341.png"></p>

### Decorative Driftwood Kite (via [hello, Wonderful](https://www.hellowonderful.co/post/KID-MADE-DECORATIVE-DRIFTWOOD-KITE))

Go on a nature walk and turn your collected finds into a rustic driftwood kite drawn in with the kids’ artwork. This is one of the most creative kites to make and allows your child to express their own creativity. Not only that, but they will also learn how to reuse discarded materials – perfect for a thriving eco-warrior.

![输入图片说明](https://foruda.gitee.com/images/1675675267361900442/befb2603_5631341.png "屏幕截图")

> Tyvek Sled Kite Instructions BABBLE DABBLE DO
  July 2, 2013 by Ana Dziengel Leave a Comment

  [![输入图片说明](https://foruda.gitee.com/images/1675675581520738708/dfad6fea_5631341.png "Tyvek-Sled-Kite-Instructions-BABBLE-DABBLE-DO")](https://cdn.babbledabbledo.com/wp-content/uploads/2013/07/Tyvek-Sled-Kite-Instructions-BABBLE-DABBLE-DO-e1372775243480.jpg)

### Tyvek Sled Kite (via [Babble Dabble Do](https://babbledabbledo.com/how-to-make-a-kite/tyvek-sled-kite-instructions-babble-dabble-do/))

This bright and colorful Tyvek Sled kite with bright streamers would bring a smile to any kid excited to fly it.

![输入图片说明](https://foruda.gitee.com/images/1675675287034495155/74a8e815_5631341.png "屏幕截图")

### Finger Kites (via [That Artist Woman](http://www.thatartistwoman.org/2010/09/finger-kites.html))

Make a cute finger kite, they are a great way to show off your kids’ art and fun for little hands to hold. These kites are smaller than the usual kite, so they only fly about a meter high but the kids still love them!

![输入图片说明](https://foruda.gitee.com/images/1675675305844330709/5e4a57c7_5631341.png "屏幕截图")

### Butterfly Kites (via [Creative Jewish Mom](http://www.creativejewishmom.com/2012/06/butterfly-kite-kids-craft-with-straws.html))

Use colorful tissue paper as your kite decoration material. You’ll be able to create different shapes like these adorable butterfly kites.

![输入图片说明](https://foruda.gitee.com/images/1675675329031342616/e7b641d9_5631341.png "屏幕截图")

### Simple Paper Kite (via [Made by Joel](http://madebyjoel.com/2010/11/paper-kite.html))

Quite possibly the easiest kite for kids to make and the best part is that it actually flies!

![输入图片说明](https://foruda.gitee.com/images/1675675349945072674/676db51b_5631341.png "屏幕截图")

### Stained Glass Kites (via [Make and Takes](http://www.makeandtakes.com/colorful-stained-glass-kites))

These lovely decorative kites make sparkly window decorations and are a fun art project for the kids. These stained glass kites are perfect for bringing out the artistic side of your child, and for decorating windows. But these kites don’t actually fly so this is a perfect creative kite-making activity for winter.

![输入图片说明](https://foruda.gitee.com/images/1675675371367902652/c004344e_5631341.png "屏幕截图")

### Colorful Kites (via [Red Ted Art](http://www.redtedart.com/2011/05/27/how-to-make-a-beautiful-kite/))

Can you believe these gorgeous kites were made by kids? There are endless possibilities here to make your own colorful kite in the simple tutorial above.

### Final Thoughts on DIY Kite Ideas 

Kite-making for kids is the perfect activity. It is simple and easy and keeps your little one entertained for hours. 

What are your favorite [summer DIY crafts](https://www.hellowonderful.co/post/diy-mini-summer-doodle-books-with-free-printables/)? Whether you are playing indoors or outdoors there are plenty of fun ideas to keep you entertained.

- [Holiday Flexagon by Babble Dabble Do | Teachers Pay Teachers](https://www.teacherspayteachers.com/Product/Holiday-Flexagon-4267971)
  TeachersPayTeachersTeachersPayTeachers|270 × 350 jpeg|Image may be subject to copyright.

  https://www.teacherspayteachers.com/Product/Holiday-Flexagon-4267971

# [Holiday Flexagon](https://www.teacherspayteachers.com/Product/Holiday-Flexagon-4267971)
Rated 5 out of 5, based on 4 reviews | 4 Ratings | 314 Downloads

![输入图片说明](https://foruda.gitee.com/images/1675677051545343522/2e8237b1_5631341.png "屏幕截图")  ![输入图片说明](https://foruda.gitee.com/images/1675677058410939497/02645dde_5631341.png "屏幕截图")  ![输入图片说明](https://foruda.gitee.com/images/1675677063611423204/e64cf274_5631341.png "屏幕截图")
 
- GRADE LEVELS: 4th - 8th
- SUBJECTS: Visual Arts, Holidays/Seasonal
- RESOURCE TYPE: Activities, Printables
- FORMATS INCLUDED: PDF

 **FREE ** 

- Share this resource
- Report this resource to TPT

Babble Dabble Do <img align="right" src="https://foruda.gitee.com/images/1675676715331962356/be4c917f_5631341.png">
> 56 Followers

- Description
- Reviews 4
- Q&A
- More from Babble Dabble Do

### Description
Print, cut, and fold this mesmerizing paper toy!

- Total Pages
- Answer Key N/A
- Teaching Duration 40 minutes

Report this resource to TPT
> Reported resources will be reviewed by our team. [Report this resource](https://teacherspayteachers.wufoo.com/forms/zmclrb00whessu/def/field19=www.teacherspayteachers.com/Product/Holiday-Flexagon-4267971) to let us know if this resource violates [TPT’s content guidelines](https://www.teacherspayteachers.com/Help/Seller-Guidelines-27/TpT-Content-Guidelines).

### Reviews

Rated 5 out of 5, based on 4 reviews | 5.0
> Based on 4 reviews

- Laurie K. | June 27, 2022 | Rated 5 out of 5 |  **Extremely satisfied** 

  > I loved creating my own designs! Lots of fun! Thank you for creating this resource.

- Sarah R. | January 5, 2022 | Rated 5 out of 5 |  **Extremely satisfied** 

  > This was so fun, but my 5th graders were probably a little too young to do this.

    - Students used with : 5th grade
      > Students were engaged Strongly disagreeStrongly agree

- Elizabeth J. | October 26, 2021 | Rated 5 out of 5 |  **Extremely satisfied** 

  > Fun activity after a day with lots of county testing

    - Students used with : 9th grade
      > Students were engaged  Strongly disagreeStrongly agree

- Mia Bowles | Mia Bowles (TPT Seller) | September 2, 2019 | 5.0 

  > Fun and easy activity to do during the holidays! 
  
  This review was left prior to the new ratings system. See rating breakdown.

居然之前注册过这个网站，找回密码后，下载了这个FREE版本！

### MY PRODUCTS

sort by:
Best Sellers
Best Sellers
view:

- [Paper Toys Play Pack- Printables](https://www.teacherspayteachers.com/Product/Paper-Toys-Play-Pack-Printables-4271408)

  [![输入图片说明](https://foruda.gitee.com/images/1675678596061072665/f027e304_5631341.png "屏幕截图")](https://www.teacherspayteachers.com/Product/Paper-Toys-Play-Pack-Printables-4271408)

  By Babble Dabble Do

  > Our Paper Toys Play Pack contains 7 printable DIY toys + 10 printable templates including:Paper TopsFlextanglesPaper HelicoptersOp-Art PinwheelsPaper HousesDecotropesInfinity TilesRecommended Ages: The projects in this play pack are appropriate

  - Subjects: Other (Math), Engineering, Arts
  - Grades: 2nd, 3rd, 4th, 5th, 6th, 7th
  - Types: Activities, Printables

  $7.50 | 6 | Digital Download PDF (6.53 MB) |  ADD TO CART | WISH LIST

- [Balance Heart STEAM Project](https://www.teacherspayteachers.com/Product/Balance-Heart-STEAM-Project-4366388)

  [![输入图片说明](https://foruda.gitee.com/images/1675678673608255349/da20f64f_5631341.png "屏幕截图")](https://www.teacherspayteachers.com/Product/Balance-Heart-STEAM-Project-4366388)

  By Babble Dabble Do

  > Kids will be introduced to a number of STEAM concepts in this delightful engineering toy made from easy to find and recycled materials!This lesson plan teaches kids about physics, engineering, art, and math, specifically about balance, weight,

  - Subjects: Physics, Valentine's Day, Engineering
  - Grades: Homeschool, 2nd, 3rd, 4th, 5th
  - Types: Activities, Printables
  - CCSS: 4.G.A.3

  $3.00 | 5 | Digital Download PDF (1.74 MB) | ADD TO CART | WISH LIST

- Earth Balancers STEAM Activity Lesson Plan

  ![输入图片说明](https://foruda.gitee.com/images/1675678739446420260/456a658b_5631341.png "屏幕截图")

  By Babble Dabble Do

  > Kids can celebrate Earth Day and be introduced to STEAM concepts in this balance toy made from easy to find and recycled materials! This lesson plan is for grades 2-5 and teaches kids about physics, astronomy, environmental science, engineering, and

  - Subjects: Earth Sciences, Physics, Engineering
  - Grades: 2nd, 3rd, 4th, 5th
  - Types: Lesson, Activities
  - CCSS: 4.G.A.3

  $3.00 | 3 | Digital Download PDF (5.38 MB) | ADD TO CART | WISH LIST

- Science Fair Mini Guide for Kids

  [![输入图片说明](https://foruda.gitee.com/images/1675678783667979916/dce58f5b_5631341.png "屏幕截图")](https://www.teacherspayteachers.com/Product/Science-Fair-Mini-Guide-for-Kids-4447417)

  By Babble Dabble Do

  > Not sure where to begin with your science fair project? Our mini guide will walk kids through all the steps for completing a project fit for the science fair! The mini guide includes 2 sections: A science fair overview and a student planning

  - Subjects: General Science, Science
  - Grades: 2nd, 3rd, 4th, 5th
  - Types: Printables

  $4.00 | 1 | Digital Download PDF (1.28 MB) | ADD TO CART | WISH LIST

- Printable Valentine's Day Activity Pack

  ![输入图片说明](https://foruda.gitee.com/images/1675678833027959062/1566371b_5631341.png "屏幕截图")

  By Babble Dabble Do

  > Our Printable Valentine's Day Activity Pack contains (4) printable Valentine's Day activities and (2) sets of printable Valentine's Day Cards.3D Paper HeartsBalance HeartsScientific ValentinesI Love You BooksHidden Message BookHeart Tangram

  - Subjects: Valentine's Day, Arts
  - Grades: 1st, 2nd, 3rd, 4th, 5th, 6th
  - Types: Activities, Printables

  $6.00 | 1 | Digital Download PDF (5.10 MB) | ADD TO CART | WISH LIST

- Printable Heart Tangrams

  [![输入图片说明](https://foruda.gitee.com/images/1675678872980875807/968d48b3_5631341.png "屏幕截图")](https://www.teacherspayteachers.com/Product/Printable-Heart-Tangrams-4372015)

  By Babble Dabble Do

  > A printable puzzle for kids to solve!A tangram is a classic Chinese puzzle made up of seven shapes (called tans) that can be rearranged to form different geometric pictures. This printable puzzle is fun math activity for Valentine's Day!

  - Subjects: Geometry, Valentine's Day
  - Grades: Homeschool, 1st, 2nd, 3rd, 4th, 5th
  - Types: Activities, Printables

  FREE | 3 | Digital Download PDF (0.81 MB) | WISH LIST

- Holiday Card Pack

  [![输入图片说明](https://foruda.gitee.com/images/1675678937928510694/d0b1cc3b_5631341.png "屏幕截图")](https://www.teacherspayteachers.com/Product/Holiday-Card-Pack-4271458)

  By Babble Dabble Do

  > Our Holiday Card Pack contains the following:-8 different Optical Illusion Greeting Cards-4 sets of Banana Cards-Holiday Flexagon-BONUS: Bell Tota Printable ActivityRequirements: All projects in this Card Pack require the use of a computer and

  - Subjects: Holidays/Seasonal, Christmas/ Chanukah/ Kwanzaa
  - Grades: 1st, 2nd, 3rd, 4th, 5th, 6th
  - Types: Activities, Printables

  $6.00 | Digital Download PDF (5.64 MB) | ADD TO CART | WISH LIST

- 10 Science Activity Cards: CLASSICS

  [![输入图片说明](https://foruda.gitee.com/images/1675678983575393707/b93a7335_5631341.png "屏幕截图")](https://www.teacherspayteachers.com/Product/10-Science-Activity-Cards-CLASSICS-4271448)

  By Babble Dabble Do

  > Our classic science activity cards include the following activities:Milk PaintingChromatographyPinecone Bird FeederOobleckShiny PenniesIce SculpturesSeed StartsLayered LiquidsToothpick StructuresRubber EggsRecommended Ages: The science projects in

  - Subjects: General Science, Science
  - Grades: PreK, Kindergarten, 1st, 2nd, 3rd, 4th
  - Types: Projects, Activities

  $5.00 | Digital Download PDF (6.46 MB) | ADD TO CART | WISH LIST

- Holiday Flexagon

  [![输入图片说明](https://foruda.gitee.com/images/1675679021879605454/99b11ca8_5631341.png "屏幕截图")](https://www.teacherspayteachers.com/Product/Holiday-Flexagon-4267971)

  By Babble Dabble Do

  > Print, cut, and fold this mesmerizing paper toy!

  - Subjects: Holidays/Seasonal, Visual Arts
  - Grades: 8th, 4th, 5th, 6th, 7th
  - Types: Activities, Printables

  FREE | 4 | Digital Download PDF (0.67 MB) | WISH LIST

Dear Ana: 

I'm science teachere in beijing china, and running a e-magazine also for volunteer action in community, my students do the DIY works for small kids play with them with funs. One little kite words give me your website, and fantastic site babbledabbledo make me exciting, [it's background triangle array graph was same as my worksheet background, I always paint scratch on it, also make a big art works using it.](https://foruda.gitee.com/images/1675683651807709040/b455f5b0_5631341.png) If we can community with email, i'll send my works and share the store in china with you. 

nice to meet you, after chinese new year - spring holiday, send the best wishes to you . good luck. 

I mark the Email me when the seller responds, hope your reply.

- DEJUN Y. re: Holiday Flexagon

  Nice sharing gift. I'm searching the topic by babbledabbledo and found this website again, already regist before, find back the password, and download it, print it, play it with my kids.

  thanks a lot.

  two kids, 5th grade , 9th grade
  February 6, 2023 (Edit)

# SHOP： https://babble-dabble-do.myshopify.com/

- Home

| ![输入图片说明](https://foruda.gitee.com/images/1675688085698617932/a9be2284_5631341.png "屏幕截图") | ![输入图片说明](https://foruda.gitee.com/images/1675688098772485427/51c65ea8_5631341.png "屏幕截图") | ![输入图片说明](https://foruda.gitee.com/images/1675688113182791071/2b7e7435_5631341.png "屏幕截图") |
|---|---|---|
| 7. [Paper Village Templates](https://babble-dabble-do.myshopify.com/collections/frontpage/products/paper-village-templates)  **$5.00**  | 17. [The Bot Book](https://babble-dabble-do.myshopify.com/collections/frontpage/products/the-bot-book)  **$19.99**  | 6. [Paper Toys Play Pack](https://babble-dabble-do.myshopify.com/collections/frontpage/products/paper-toys-play-pack)  **$7.50**  |
| ![输入图片说明](https://foruda.gitee.com/images/1675688282767689175/21a57f84_5631341.png "屏幕截图") | ![输入图片说明](https://foruda.gitee.com/images/1675688289136342271/0926cd0f_5631341.png "屏幕截图") | ![输入图片说明](https://foruda.gitee.com/images/1675688294710003990/77417bb2_5631341.png "屏幕截图") |
| 1. [10 Science Activity Cards: CLASSICS](https://babble-dabble-do.myshopify.com/collections/frontpage/products/10-science-activity-cards-classics)  **$5.00**  | 5. [Earth Balancers STEAM Activity Lesson Plan](https://babble-dabble-do.myshopify.com/collections/frontpage/products/earth-balancers-steam-activity-lesson-plan)  **$3.00**  | 9. [Printable Holiday Card Pack](https://babble-dabble-do.myshopify.com/collections/frontpage/products/printable-holiday-card-pack)  **$6.00**  |

- [Catalog](https://babble-dabble-do.myshopify.com/collections/all)

| ![输入图片说明](https://foruda.gitee.com/images/1675688636001803752/a25f332d_5631341.png "屏幕截图") | ![输入图片说明](https://foruda.gitee.com/images/1675688644042425239/58f01f86_5631341.png "屏幕截图") | ![输入图片说明](https://foruda.gitee.com/images/1675688650887480031/27c7ecf3_5631341.png "屏幕截图") | ![输入图片说明](https://foruda.gitee.com/images/1675688657628648525/ed4c2715_5631341.png "屏幕截图") |
|---|---|---|---|
| 2. [Balance Hearts STEAM Activity Lesson Plan](https://babble-dabble-do.myshopify.com/collections/all/products/balance-hearts-steam-activity-lesson-plan)  **$3.00**  | 3. [BUNDLE: Paper Toys, Holiday Cards, & Bonus Printables](https://babble-dabble-do.myshopify.com/collections/all/products/bundle-paper-toys-holiday-cards-bonus-printables)  ~$13.50~  **$6.75 Sale**  | 4. [DIY Camp STEAM](https://babble-dabble-do.myshopify.com/collections/all/products/camp-steam)  **$23.99**  | 8. [Printable Heart Tangrams](https://babble-dabble-do.myshopify.com/collections/all/products/printable-heart-tangrams)  **$2.00**  |
| | ![输入图片说明](https://foruda.gitee.com/images/1675689012214343480/d86b58ce_5631341.png "屏幕截图") | ![输入图片说明](https://foruda.gitee.com/images/1675689018881399873/2c96b160_5631341.png "屏幕截图") | ![输入图片说明](https://foruda.gitee.com/images/1675689028564464799/ce17c8b7_5631341.png "屏幕截图") |
| | 10. [Printable Valentine's Day Activity Pack](https://babble-dabble-do.myshopify.com/collections/all/products/printable-valentines-day-activity-pack)  **$6.00**  | 11. [Problem Solver: Unisex Jersey Short Sleeve Tee](https://babble-dabble-do.myshopify.com/collections/all/products/problem-solver-unisex-jersey-short-sleeve-tee)  **$15.35**  | 12. [Science Fair Mini Guide for Kids](https://babble-dabble-do.myshopify.com/collections/all/products/science-fair-mini-guide)  **$4.00**  |
| ![输入图片说明](https://foruda.gitee.com/images/1675689170131360994/c69ae2fa_5631341.png "屏幕截图") | ![输入图片说明](https://foruda.gitee.com/images/1675689176545973826/e5077102_5631341.png "屏幕截图") | ![输入图片说明](https://foruda.gitee.com/images/1675689183202833565/bebd9cea_5631341.png "屏幕截图") | ![输入图片说明](https://foruda.gitee.com/images/1675689193766016415/51eaa970_5631341.png "屏幕截图") |
| 13. [STEAM Kids eBook](https://babble-dabble-do.myshopify.com/collections/all/products/steam-kids)  **$14.99**  | 14. [STEAM Kids in the Kitchen](https://babble-dabble-do.myshopify.com/collections/all/products/steam-kids-in-the-kitchen-ebook)   **$14.99**  | 15. [STEAM Kids Printable Placemats](https://babble-dabble-do.myshopify.com/collections/all/products/steam-kids-printable-placemats)  **$4.99**  | 16. [STEAM Play & Learn](https://babble-dabble-do.myshopify.com/collections/all/products/steam-play-learn)  **$14.95**  |

# [Privacy/Disclosure Policy](https://babbledabbledo.com/privacydisclosure-policy/)

<p><strong>Babble Dabble Do&nbsp;operates under&nbsp;Make My Day: Visions LLC.</strong> For LLC information please contact Ana Dziengel at ana@babbledabbledo.com or info@makemydayvisions.com</p><hr><h2>Babble Dabble Do (“Website”) is governed by the following Privacy Policy.</h2><p>We respect your privacy and are committed to protecting it. The purpose of this Privacy Policy is to inform you what information we may collect and how it may be used. This statement only applies to this Website.</p><p><strong><u>WHAT INFORMATION DO WE COLLECT AND HOW IS IT USED?</u></strong></p><ul>
<li><strong>Information You Voluntarily Submit to the Website: </strong>We may collect personal information from you such as your name or email address. For example, you may voluntarily submit information to the Website by leaving a comment, subscribing to a newsletter, or submitting a contact form. In addition, we may ask you to create a user profile, which would allow you to create a username and password. We will store the username, but your password will not be visible in our records.</li></ul><ul>
<li><strong>Information We Collect from Others: </strong>We may receive information about you from other sources. For example, if you use a third-party software through the site, they may transfer information to us for fulfillment.</li></ul><ul>
<li><strong>Automatically-Collected Information: </strong>We automatically collect certain information about you and the device with which you access the Website. For example, when you use the Website, we will log your IP address, operating system type, browser type, referring website, pages you viewed, and the dates/times when you accessed the Website. We may also collect information about actions you take when using the Website, such as links clicked.</li></ul><ul>
<li><strong>Cookies:</strong>We may log information using cookies, which are small data files stored on your browser by the Website. We may use both session cookies, which expire when you close your browser, and persistent cookies, which stay on your browser until deleted, to provide you with a more personalized experience on the Website.</li></ul><p><strong><u>HOW YOUR INFORMATION MAY BE USED</u></strong></p><p>We may use the information collected in the following ways:</p><ul>
<li>To operate and maintain the Website;</li>
<li>To create your account, identify you as a user of the Website, and customize the Website for your account;</li>
<li>To send you promotional information, such as newsletters. Each email promotion will provide information on how to opt-out of future mailings;</li>
<li>To send you administrative communications, such as administrative emails, confirmation emails, technical notices, updates on policies, or security alerts;</li>
<li>To respond to your comments or inquiries;</li>
<li>To provide you with user support;</li>
<li>To track and measure advertising on the Website;</li>
<li>To process payment for purchases you make through the Website; or,</li>
<li>To protect, investigate, and deter against unauthorized or illegal activity.</li></ul><p><strong><u>THIRD-PARTY USE OF PERSONAL INFORMATION</u></strong></p><p>We may share your information with third parties when you explicitly authorize us to share your information.</p><p>Additionally, the Website may use third-party service providers service various aspects of the Website. Each third-party service provider’s use of your personal information is dictated by their respective privacy policies. The Website currently uses the following third-party service providers:</p><ul>
<li>Google Analytics – this service tracks Website usage and provides information such as referring websites and user actions on the Website. Google Analytics may capture your IP address, but no other personal information is captured by Google Analytics.</li>
<li>Payment Processing – this service is used to serve our ecommerce platform. At no time is your banking information passed to the Website. We receive only information used for order fulfillment.</li>
<li>Mailchimp – this service is used for delivery of email updates and newsletters. We store your name and email address for purposes of delivering such communications.</li></ul><p>At this time, your personal information is not shared with any other third-party applications. This list may be amended from time to time in the Website’s sole discretion.</p><p>Except when required by law, we will not sell, distribute, or reveal your email addresses or other personal information without your consent; however, we may disclose or transfer personal information collected through the Website to third parties who acquire all or a portion of our business, which may be the result of a merger, consolidation, or purchase of all or a portion of our assets, or in connection with any bankruptcy or reorganization proceeding brought by or against us.</p><p><strong><u>ANONYMOUS DATA</u></strong></p><p>From time to time, we may use anonymous data, which does not identify you alone, or when combined with data from other parties. This type of anonymous data may be provided to other parties for marketing, advertising, or other uses. Examples of this anonymous data may include analytics or information collected from cookies.</p><p><strong><u>PUBLICLY VISIBLE INFORMATION</u></strong></p><p><u></u>If you create a user profile on the Website or leave a comment, certain information may be publicly visible.</p><p><strong><u>ADVERTISING</u></strong></p><p>This Site is affiliated with CMI Marketing, Inc., d/b/a CafeMedia (“CafeMedia”) for the purposes of placing advertising on the Site, and CafeMedia will collect and use certain data for advertising purposes. To learn more about CafeMedia’s data usage, click here: <a href="https://www.cafemedia.com/publisher-advertising-privacy-policy" target="_blank" rel="noopener noreferrer">www.cafemedia.com/publisher-advertising-privacy-policy</a></p><p><strong>Cookies</strong></p><p>This Website uses cookies to store visitors’ preferences,&nbsp;record user-specific information on what pages users access or visit,&nbsp;ensure that visitors are not repeatedly sent the same banner ads,&nbsp;customize Website content based on visitors’ browser type or other&nbsp;information that the visitor sends. Cookies may also be used by third-party services, such as Google Analytics, as described herein.</p><p>A cookie is a file containing an identifier (a string of letters and numbers) that is sent by a web server to a web browser and is stored by the browser. The identifier is then sent back to the server each time the browser requests a page from the server. Cookies may be either “persistent” cookies or “session” cookies: a persistent cookie will be stored by a web browser and will remain valid until its set expiry date, unless deleted by the user before the expiry date; a session cookie, on the other hand, will expire at the end of the user session, when the web browser is closed.&nbsp; Cookies do not typically contain any information that personally identifies a user, but personal information that we store about you may be linked to the information stored in and obtained from cookies.</p><p>By using this Site, you consent to the use of such cookies and the sharing of data captured by such cookies with AdThrive, Google, and our other third party partners. You can view, delete or add interest categories associated with your browser by visiting: <a href="https://adssettings.google.com/" target="_blank" rel="noopener noreferrer">https://adssettings.google.com</a>. You can also opt out of the network cookie using those settings or using the Network Advertising Initiative’s multi-cookie opt-out mechanism at: <a href="http://optout.networkadvertising.org/" target="_blank" rel="noopener noreferrer">http://optout.networkadvertising.org</a>. However, these opt-out mechanisms themselves use cookies, and if you clear the cookies from your browser your opt-out will not be maintained.</p><p><em>Cookies on different browsers:</em></p><p>Users may, at any time, prevent the setting of cookies, by the Website, by using a corresponding setting of your internet browser and may thus permanently deny the setting of cookies. Furthermore, already set cookies may be deleted at any time via an Internet browser or other software programs. This is possible in all popular Internet browsers. However, if users deactivate the setting of cookies in your Internet browser, not all functions of our Website may be entirely usable.</p><p><strong>Retargeting Ads<br>
</strong>From time to time, the Website may engage in remarketing efforts with third-party companies, such as Google, Facebook, or Instagram, in order to market the Website. These companies use cookies to serve ads based on someone’s past visits to the Website.</p><p><strong>Sponsored Content Tracking Pixels<br>
</strong>This Website may engage in sponsored campaigns with various influencer networks, brands, and agencies. All sponsored content is duly disclosed in accordance with the FTC’s requirements. From time to time, these sponsored campaigns utilize tracking pixels (aka web beacons), which may contain cookies to collect data regarding usage and audience. This information is collected by the sponsoring company to track the results of the campaign. No personally identifiable information collected by the Website is used in conjunction with these tracking pixels.</p><p><strong>Affiliate Program Participation<br>
</strong>The Website may engage in affiliate marketing, which is done by embedding tracking links into the Website. If you click on a link for an affiliate partnership, a cookie will be placed on your browser to track any sales for purposes of commissions.</p><p>Babble Dabble Do is a participant in the Amazon Services LLC Associates Program, an affiliate advertising program designed to provide a means for sites to earn advertising fees by advertising and links to Amazon.com.&nbsp; As part of this Amazon Associates program, the Website will post customized links, provided by Amazon, to track the referrals to their website. This program utilizes cookies to track visits for the purposes of assigning commission on these sales.</p><p><strong>Newsletters</strong><br>
On the Website, you may subscribe to our newsletter, which may be used for advertising purposes. All newsletters sent may contain tracking pixels. The pixel is embedded in emails and allows an analysis of the success of online marketing campaigns. Because of these tracking pixels, we may see if and when you open an email and which links within the email you click. Also, this allows the Website to adapt the content of future newsletters to the interests of the user. This behavior will not be passed on to third parties.</p><p><strong><u>RIGHTS RELATED TO YOUR PERSONAL INFORMATION</u></strong></p><p><strong>Opt-out</strong>– You may opt-out of future email communications by following the unsubscribe links in our emails. You may also notify us at ana@babbledabbledo.com to be removed from our mailing list.</p><p><strong>Access</strong>– You may access the personal information we have about you by submitting a request to ana@babbledabbledo.com</p><p><strong>Amend</strong>– You may contact us at ana@babbledabbledo.com to amend or update your personal information.</p><p><strong>Forget</strong>– In certain situations, you may request that we erase or forget your personal data. To do so, please submit a request to ana@babbledabbledo.com.</p><p>Please note that we may need to retain certain information for recordkeeping purposes or to complete transactions, or when required by law.</p><p><strong><u>SENSITIVE PERSONAL INFORMATION</u></strong></p><p>At no time should you submit sensitive personal information to the Website. This includes your social security number, information regarding race or ethnic origin, political opinions, religious beliefs, health information, criminal background, or trade union memberships. If you elect to submit such information to us, it will be subject to this Privacy Policy.</p><p><strong><u>CHILDREN’S INFORMATION</u></strong></p><p>The Website does not knowingly collect any personally identifiable information from children under the age of 16. If a parent or guardian believes that the Website has personally identifiable information of a child under the age of 16 in its database, please contact us immediately ana@babbledabbledo.com and we will use our best efforts to promptly remove such information from our records.</p><p><strong><u>CONTACT INFORMATION</u></strong></p><p>At any time, please contact us at ana@babbledabbledo.com for questions related to this Privacy Policy.</p><p><span style="text-decoration: underline;"><strong>CALIFORNIA RESIDENTS</strong></span></p><p>California law gives residents the right to opt out of the “sale” of their personal information to third parties, including for advertising purposes. Under California law, the sharing of your information with a service provider for advertising purposes could be considered a “sale.” To opt out of the sharing of your information for advertising information, click the opt out link provided at the end of the page. You also have the right to request from us the categories of personal information that we have shared and the categories of third parties to whom the information was provided. To make such a request, please contact us at <a href="mailto:info@adthrive.com" target="_self" rel="noopener noreferrer">info@adthrive.com</a>. To be clear we do not share your name, contact information, or any other sensitive information with third parties, and the categories of third parties that we share information with for advertising purposes are supply side platforms, programmatic advertising exchanges, and demand side platforms.</p><p>Last updated: 1/14/20</p>

<h1 class="entry-title">隐私/信息披露政策</h1>

<p>Babble Dabble Do隶属于Make My Day: Visions LLC.有关LLC信息，请通过ana@babbledabbledo.com或info@makemydayvisions.com联系Ana Dziengel</p>
<hr>
<h2>Babble Dabble Do(“网站”)受以下隐私政策管辖。</h2>
<p>我们尊重您的隐私，并致力于保护它。本隐私政策的目的是告知您我们可能收集哪些信息以及如何使用这些信息。本声明仅适用于本网站。</p>
<p><strong><u>我们收集哪些信息，如何使用这些信息?</u></strong></p>
<ul> 
<li>您自愿提交给网站的信息:我们可能会收集您的个人信息，如您的姓名或电子邮件地址。例如，您可以通过留言、订阅时事通讯或提交联系表格的方式自愿向本网站提交信息。此外，我们可能会要求您创建用户配置文件，这将允许您创建用户名和密码。我们将存储用户名，但您的密码将不会在我们的记录中可见。</li>
</ul>
<ul> 
<li>我们从他人处收集的信息:我们可能会从其他来源收到有关您的信息。例如，如果您通过本网站使用第三方软件，他们可能会向我们传输信息以供履行。</li>
</ul>
<ul> 
<li>自动收集的信息:我们自动收集有关您和您访问本网站的设备的某些信息。例如，当您使用本网站时，我们将记录您的IP地址、操作系统类型、浏览器类型、引用网站、您浏览的页面以及您访问本网站的日期/时间。我们还可能收集有关您在使用本网站时所采取的操作的信息，例如点击链接。</li>
</ul>
<ul> 
<li>Cookies:我们可能会使用Cookies记录信息，Cookies是本网站存储在您浏览器上的小数据文件。我们可能同时使用会话cookie(在您关闭浏览器时过期)和持久性cookie(在您删除之前一直保留在浏览器上)，为您提供更个性化的网站体验。</li>
</ul>
<p><strong><u>你的信息将如何被使用</u></strong></p>
<p>我们可能会以以下方式使用收集的信息:</p>
<ul> 
<li>运营和维护本网站;</li> 
<li>创建您的帐户，确认您是本网站的用户，并为您的帐户自定义网站;</li> 
<li>向您发送促销信息，如时事通讯。每个电子邮件促销活动将提供有关如何选择退出未来邮件的信息;</li> 
<li>向您发送行政通信，例如行政电子邮件、确认电子邮件、技术通知、策略更新或安全警报;</li> 
<li>回复您的意见或询问;</li> 
<li>为您提供用户支持;</li> 
<li>跟踪和衡量网站上的广告;</li> 
<li>处理您通过本网站购买商品的付款;或者,</li> 
<li>保护、调查和阻止未经授权的或非法的活动</li>
</ul>
<p><strong><u>第三方使用个人信息</u></strong></p>
<p>当您明确授权我们共享您的信息时，我们可能会与第三方共享您的信息。</p>
<p>此外，本网站可能使用第三方服务提供商为本网站的各个方面提供服务。每个第三方服务提供商使用您的个人信息取决于他们各自的隐私政策。本网站目前使用以下第三方服务提供商:</p>
<ul> 
<li>谷歌分析-该服务跟踪网站使用情况，并提供诸如引用网站和用户在网站上的操作等信息。谷歌Analytics可能会捕获您的IP地址，但谷歌Analytics不会捕获其他个人信息。</li> 
<li>支付处理-这项服务用于服务于我们的电子商务平台。在任何时候，您的银行信息都不会传递到本网站。我们只接收用于订单履行的信息。</li> 
<li>Mailchimp -这项服务用于发送电子邮件更新和时事通讯。我们储存您的姓名和电子邮件地址，以供传送此类通讯之用。</li>
</ul>
<p>目前，您的个人信息不会与任何其他第三方应用程序共享。本网站可随时自行决定对本名单进行修订。</p>
<p>除法律规定外，未经您的同意，我们不会出售、分发或透露您的电子邮件地址或其他个人信息;但是，我们可以向收购我们全部或部分业务的第三方披露或转让通过网站收集的个人信息，这些信息可能是由于合并、整合或购买我们全部或部分资产，或与我们提起的或针对我们的任何破产或重组程序有关。</p>
<p><strong><u>匿名数据</u></strong></p>
<p>我们可能会不时使用匿名数据，这些数据不会单独识别您的身份，也不会与其他方的数据结合使用。这种类型的匿名数据可能会提供给其他方用于营销、广告或其他用途。这种匿名数据的例子可能包括分析或从cookie收集的信息。</p>
<p><strong><u>公开可见的信息</u></strong></p>
<p>如果您在网站上创建用户简介或留下评论，某些信息可能会被公开可见。</p>
<p><strong><u>广告</u></strong></p>
<p>本网站隶属于CMI Marketing, Inc.， d/b/a CafeMedia(“CafeMedia”)，用于在网站上投放广告，CafeMedia将收集和使用某些数据用于广告目的。要了解更多关于CafeMedia的数据使用情况，请点击这里:www.cafemedia.com/publisher-advertising-privacy-policy</p>
<p><strong>饼干</strong></p>
<p>本网站使用cookie存储访问者的偏好，记录用户访问或访问的页面的特定用户信息，确保访问者不会重复发送相同的横幅广告，根据访问者的浏览器类型或访问者发送的其他信息定制网站内容。如本文所述，第三方服务(如谷歌Analytics)也可能使用cookie。</p>
<p>cookie是一个包含标识符(一串字母和数字)的文件，由web服务器发送给web浏览器，并由浏览器存储。然后，每当浏览器从服务器请求一个页面时，标识符就被发送回服务器。Cookies可以是“持久”Cookies，也可以是“会话”Cookies:持久Cookies将由web浏览器存储，并在其设置的到期日之前保持有效，除非用户在到期日之前删除;另一方面，会话cookie将在用户会话结束时过期，此时web浏览器被关闭。Cookies通常不包含任何识别用户个人身份的信息，但我们存储的关于您的个人信息可能与Cookies中存储的信息相关联。</p>
<p>通过使用本网站，您同意使用此类cookie，并与AdThrive、谷歌和我们的其他第三方合作伙伴共享此类cookie捕获的数据。您可以通过访问https://adssettings.google.com查看、删除或添加与您的浏览器相关的兴趣类别。您也可以选择退出网络cookie，使用这些设置或使用网络广告倡议的多cookie退出机制:http://optout.networkadvertising.org。然而，这些退出机制本身使用cookie，如果您从浏览器中清除了cookie，您的退出将不会得到维护。</p>
<p><em>不同浏览器的Cookies:</em></p>
<p>用户可以在任何时候通过使用您的互联网浏览器的相应设置来阻止本网站对cookie的设置，并可能因此永久拒绝对cookie的设置。此外，已经设置的cookie可能随时通过互联网浏览器或其他软件程序被删除。这在所有流行的Internet浏览器中都是可能的。但是，如果用户在您的互联网浏览器中停用cookies设置，本网站的所有功能可能无法完全使用。</p>
<p>本网站可能会不时与第三方公司(如谷歌、Facebook或Instagram)进行再营销，以推广本网站。这些公司使用cookie根据用户过去访问网站的情况来投放广告。</p>
<p>赞助内容跟踪像素本网站可能与各种有影响力的网络、品牌和机构进行赞助活动。所有赞助内容都按照联邦贸易委员会的要求正式披露。这些赞助活动不时利用跟踪像素(又名网络信标)，其中可能包含cookie，以收集有关使用情况和受众的数据。赞助公司收集这些信息来跟踪活动的结果。本网站收集的个人身份信息不会与这些跟踪像素一起使用。</p>
<p>本网站可通过在网站中嵌入跟踪链接来进行联属营销。如果你点击了一个附属伙伴关系的链接，一个cookie将被放置在你的浏览器上，以跟踪任何销售为目的的佣金。</p>
<p>Babble Dabble Do是亚马逊服务有限责任公司联营计划的参与者，这是一个联营广告计划，旨在为网站提供一种手段，通过广告和亚马逊的链接来赚取广告费。作为亚马逊合作伙伴计划的一部分，网站将发布由亚马逊提供的定制链接，以跟踪他们网站的推荐。这个程序利用cookie跟踪访问的目的分配佣金这些销售。</p>
<p>在网站上，您可以订阅我们的通讯，这些通讯可能被用于广告目的。所有发送的通讯都可能包含跟踪像素。像素嵌入在电子邮件中，可以分析在线营销活动的成功与否。由于这些跟踪像素，我们可以看到你是否以及何时打开电子邮件，以及你点击了电子邮件中的哪些链接。此外，这还允许本网站根据用户的兴趣调整未来通讯的内容。此行为不会传递给第三方。</p>
<p><strong><u>与您的个人信息相关的权利</u></strong></p>
<p>选择退出-您可以选择退出未来的电子邮件通信通过点击我们电子邮件中的退订链接。您也可以通过ana@babbledabbledo.com通知我们将您从我们的邮件列表中删除。</p>
<p>访问-您可以通过向ana@babbledabbledo.com提交请求来访问我们拥有的有关您的个人信息</p>
<p>修改-您可以通过ana@babbledabbledo.com与我们联系以修改或更新您的个人信息。</p>
<p>忘记-在某些情况下，您可以要求我们删除或忘记您的个人资料。为此，请向ana@babbledabbledo.com提交请求。</p>
<p>请注意，我们可能需要保留某些信息用于记录保存目的或完成交易，或在法律要求时。</p>
<p><strong><u>敏感个人资料</u></strong></p>
<p>在任何时候，您都不应向本网站提交敏感的个人信息。这包括你的社会安全号码，有关种族或民族出身的信息，政治观点，宗教信仰，健康信息，犯罪背景，或工会会员资格。如果您选择向我们提交此类信息，这些信息将受本隐私政策的约束。</p>
<p><strong><u>孩子们的信息</u></strong></p>
<p>本网站不会故意收集16岁以下儿童的任何个人身份信息。如果家长或监护人认为本网站的数据库中有16岁以下儿童的个人身份信息，请立即与我们联系ana@babbledabbledo.com，我们将尽最大努力迅速从我们的记录中删除该等信息。</p>
<p><strong><u>联系信息</u></strong></p>
<p>如有与本隐私政策有关的问题，请随时通过ana@babbledabbledo.com与我们联系。</p>
<p><span style="text-decoration: underline;"><strong>加州居民</strong></span></p>
<p>加州法律赋予居民选择不向第三方“出售”个人信息的权利，包括用于广告目的。根据加州法律，出于广告目的与服务提供商共享您的信息可被视为“销售”。若要选择不分享您的信息用于广告信息，请单击页面末尾提供的“退出”链接。您也有权要求我们提供我们已共享的个人信息的类别，以及向其提供信息的第三方的类别。如有此类要求，请通过info@adthrive.com与我们联系。需要明确的是，我们不会与第三方共享您的姓名、联系信息或任何其他敏感信息，我们为广告目的而与之共享信息的第三方类别包括供应方平台、程序性广告交流平台和需求方平台。</p>
<p>最近更新:20年1月14日</p> 

# [Terms &amp; Conditions, Shipping, &amp; Refunds](https://babbledabbledo.com/terms-conditions-shipping-refunds/)

<p>Welcome to Babble Dabble Do. If you continue to browse and use this website you are agreeing to comply with and be bound by the following terms and conditions of use, which together with our privacy policy govern Babble Dabble Do’s relationship with you in relation to this website.</p><h2><strong>TERMS &amp; CONDITIONS</strong></h2><p>The term ‘Babble Dabble Do or ‘us’ or ‘we’ refers to the owner of the website whose registered office is 12345 Ventura Blvd, STE H, Studio City, CA 91604.&nbsp;The term ‘you’ refers to the user or viewer of our website.</p><div id="cls-video-container-b0Mf1uv1" class="adthrive" style="min-height: 418.5px;"><div class="adthrive-player-container adthrive-collapse-player" id="adthrive-collapse-container" style="height: 439px;"><div class="adthrive-player-position adthrive-player-without-wrapper-text" id="adthrive-collapse-position" style=""><h3 class="adthrive-player-title" id="adthrive-collapse-title">MY LATEST VIDEOS</h3><p>The use of this website is subject to the following terms of use:</p><ul>
<li>The content of the pages of this website is for your general information and use only. It is subject to change without notice.</li>
<li>Neither we nor any third parties provide any warranty or guarantee as to the accuracy, timeliness, performance, completeness or suitability of the information and materials found or offered on this website for any particular purpose. You acknowledge that such information and materials may contain inaccuracies or errors and we expressly exclude liability for any such inaccuracies or errors to the fullest extent permitted by law.</li>
<li>Your use of any information or materials on this website is entirely at your own risk, for which we shall not be liable. It shall be your own responsibility to ensure that any products, services or information available through this website meet your specific requirements.</li>
<li>This website contains material which is owned by or licensed to us. This material includes, but is not limited to, the design, layout, look, appearance and graphics. Reproduction is prohibited other than in accordance with the copyright notice, which forms part of these terms and conditions.</li>
<li>All trademarks reproduced in this website, which are not the property of, or licensed to the operator, are acknowledged on the website.</li>
<li>Unauthorized use of this website may give rise to a claim for damages and/or be a criminal offence.</li>
<li>From time to time this website may also include links to other websites. These links are provided for your convenience to provide further information. They do not signify that we endorse the website(s). We have no responsibility for the content of the linked website(s).</li>
<li>You may not create a link to this website from another website or document without&nbsp;Babble Dabble Do’s prior written consent.</li></ul><h2><strong>SHIPPING</strong></h2><p>All products available on our site are digital products and will be delivered to you via EMAIL.&nbsp;No physical products are sold and shipped from this site.</p><h2><strong>REFUNDS</strong></h2><p>Digital products may not be returned. However if you have purchased the product in error or under the misunderstanding it was a physical product, a refund may&nbsp;be issued. Please contact Ana at <strong>anadziengel@gmail.com</strong> with your refund request.</p><p>Periodically, I may put products on sale or update my products. I am not liable for offering refunded amounts to individuals who have purchased the product for the full amount before or after a sale time is over or an update has been done. Thank you for your understanding.</p>

<h1 class="entry-title">条款和条件，运输和退款</h1>

<div class="entry-content">
<p>欢迎来到Babble Dabble Do。如果您继续浏览和使用本网站，您即同意遵守并受以下使用条款和条件的约束，这些条款和条件以及我们的隐私政策管辖Babble Dabble Do与您就本网站的关系。</p>
<h2><strong>条款和条件</strong></h2>
<p>术语“Babble Dabble Do”或“我们”或“我们”是指网站的所有者，其注册办事处是12345 Ventura Blvd, STE H, Studio City, CA 91604。“您”一词是指我们网站的用户或观众。</p>
<p>使用本网站须遵守以下使用条款:</p>
<ul> 
<li>本网站网页内容仅供一般参考及使用。如有更改，恕不另行通知。</li> 
<li>我们或任何第三方均不对本网站为任何特定目的而发现或提供的信息和材料的准确性、及时性、性能、完整性或适用性提供任何保证或保证。您承认这些信息和材料可能包含不准确或错误，我们明确排除在法律允许的最大范围内对任何此类不准确或错误承担责任。</li> 
<li>您使用本网站的任何信息或材料，风险完全由您自行承担，我们对此不承担任何责任。您应自行负责确保通过本网站提供的任何产品、服务或信息符合您的特定要求。</li> 
<li>本网站包含本公司拥有或授权本公司使用的资料。此材料包括但不限于设计、布局、外观、外观和图形。除根据版权声明以外，禁止复制，版权声明是这些条款和条件的一部分。</li> 
<li>本网站承认本网站所转载的所有商标，如并非经营者的财产，亦非获授权予经营者。</li> 
<li>未经授权使用本网站可能导致索赔和/或刑事犯罪。</li> 
<li>本网站可能不时包含其他网站的链接。这些链接是为了方便您提供进一步的信息。他们并不意味着我们认可网站(s)。我们对链接网站的内容不承担任何责任。</li> 
<li>未经Babble Dabble Do事先书面同意，您不得从其他网站或文件创建到本网站的链接。</li>
</ul>
<h2><strong>航运</strong></h2>
<p>我们网站上的所有产品都是数字产品，将通过电子邮件发送给您。本网站不销售和运输实物产品。</p>
<h2><strong>退款</strong></h2>
<p>数码产品将不予退还。但是，如果您错误地购买了该产品或误解它是实物产品，则可能会退款。请通过anadziengel@gmail.com与Ana联系并要求退款。</p>
<p>我可能会定期推出产品或更新我的产品。我不负责向在销售时间结束或更新完成之前或之后全额购买产品的个人提供退款金额。谢谢您的理解。</p> 

# MAKE MY DAY: VISIONS
> EYE HEART STORIES

http://www.makemydayvisions.com/

HOME  | [OUR WORK](http://www.makemydayvisions.com/videos) | [ABOUT](http://www.makemydayvisions.com/about) | [BEHIND THE FRAMES](http://www.makemydayvisions.com/behind-the-scenes) | [CONTACT](http://www.makemydayvisions.com/contact)

- POPCORN TIME.

  
  - [Black Letter Gothic Calligraphy w/Janet Takahashi - Sakura of America](https://www.youtube.com/embed/SeJ4W6fIzAQ)
  - [3 Days in Indiana](https://player.vimeo.com/video/154526451)
  - [West Texas - iPhone with Anamorphic Lens (preview)](https://player.vimeo.com/video/362393073)
  - [The Finisher](https://player.vimeo.com/video/111827138)
  - [Karla & Oren, Bacara Resort & Spa](https://player.vimeo.com/video/118744735)
  - [Baseball Ready](https://player.vimeo.com/video/96742548)
  - [Uncial Calligraphy - Sakura of America](https://www.youtube.com/embed/dt0yFnN4IiI)

    <p><img width="29.369%" src="https://foruda.gitee.com/images/1675697197874111880/5471f632_5631341.png"> <img width="29.369%" src="https://foruda.gitee.com/images/1675697231820961481/9981c31d_5631341.png"> <img width="29.369%" src="https://foruda.gitee.com/images/1675697255624538063/cf477ed0_5631341.png"></p>

  - [Here Comes The Sun - iPhone with Anamorphic Lens (journalistic)](https://player.vimeo.com/video/312654725)
  - [Jimmy & Me](https://player.vimeo.com/video/266902669)
  - [Beach 2018 - iPhone with Anamorphic Lens](https://player.vimeo.com/video/308960200)
  - [Chasidy & Jonathan, Calamigos Ranch](https://player.vimeo.com/video/83727783)
  - [Runny Nose](https://player.vimeo.com/video/60338515)
  - [Good Company](https://player.vimeo.com/video/103192397)
  - [Snow Path - iPhone with Anamorphic Lens](https://player.vimeo.com/video/316918639)
  - [Katie & Peter, Descanso Gardens](https://player.vimeo.com/video/81465136)
  - [Bastard the film teaser](https://player.vimeo.com/video/14024718)
  - [Bastard the film teaser 2.0](https://player.vimeo.com/video/29763280)
  - [Diane & Kevin, Casa Del Mar](https://player.vimeo.com/video/86397464)
  - [Here](https://player.vimeo.com/video/108429992)
  - [LA to Cholame, CA on the 61st Anniversary - Filmed on iPhone](https://www.youtube.com/embed/1TAKJ-LxH8I)
  - [The Special # 2 - Short Film](https://player.vimeo.com/video/11005593)

- About: LET'S DO THIS.

  > WE STRIVE TO BE COLLABORATIVE COMMUNICATORS OF THE WORLD AROUND US. IN OUR WORK WE WANT TO EXPLORE IDEAS WITH YOU, CONVEY YOUR SPIRIT, AND DISCOVER YOUR VOICE. WE CREATE VIDEO FILMS TO SHARE SOCIALLY THE INS & OUTS OF EVERYDAY HUMAN LIFE THROUGH VISUAL MEDIA STORYTELLING.

  ![输入图片说明](https://foruda.gitee.com/images/1675694718799039416/be3d2e4f_5631341.png "屏幕截图")

  - JR DZIENGEL (CAMERA, WRITER & DIRECTOR) IS THE DIGITAL VIDEO GUY. HE REALLY LIKES THANKSGIVING, GEEKING OUT OVER CAMERA GEAR AND JAMES DEAN (BY THE WAY, HAVING CO-WRITTEN THE (JAMES DEAN) SCREENPLAY, "BASTARD" THE FILM).  
             
  - ANA DZIENGEL (MANAGER, 2ND CAMERA & DIRECTOR) RUNS THE SHOW. SHE IS ALSO THE TALENT WHO CREATED HER SUCCESSFUL BLOG BABBLEDABBLEDO.COM

- DOING OUR BEST TO DO OUR BEST.

  <p><img width="39.69%" src="https://foruda.gitee.com/images/1675694071782828367/2659b7dc_5631341.png"> <img width="39.69%" src="https://foruda.gitee.com/images/1675694082410358504/101579eb_5631341.png"> <img width="39.69%" src="https://foruda.gitee.com/images/1675694089052748133/279847ab_5631341.png"> <img width="39.69%" src="https://foruda.gitee.com/images/1675694095333250984/89c88860_5631341.png"> <img width="39.69%" src="https://foruda.gitee.com/images/1675694115279008192/3e04eb4c_5631341.jpeg" title="Fair7.jpg JAMES DEAN FESTIVAL & MEMORIAL 2015 On the Winslow Farm. Fairmount, IN"> <img width="39.69%" src="https://foruda.gitee.com/images/1675694212661475689/7489a450_5631341.jpeg" title="Fair3web.jpg JAMES DEAN FESTIVAL & MEMORIAL 2015 Filming the Memorial Ceremony. Marion, IN"> <img width="39.69%" src="https://foruda.gitee.com/images/1675694224639929524/f41f27fe_5631341.jpeg" title="IMG_8790-1.jpg JAMES DEAN FESTIVAL & MEMORIAL 2015 Working Hard. Fairmount, IN"> <img width="39.69%" src="https://foruda.gitee.com/images/1675694284156989506/a3b832d0_5631341.jpeg" title="Fair11.jpg JAMES DEAN FESTIVAL & MEMORIAL 2015 Setting up the Canon 5D Mark III camera for some cool work. Fairmount, IN"> <img width="39.69%" src="https://foruda.gitee.com/images/1675694298878479854/6c04ecac_5631341.jpeg" title="Fair2web.jpg JAMES DEAN FESTIVAL & MEMORIAL 2015 Part of the crowd. Fairmount, IN"> <img width="39.69%" src="https://foruda.gitee.com/images/1675694312769679017/70a52b1c_5631341.jpeg" title="HS5JR.jpg PREPPING FOR A GIG WITH MY RIG Hangin' with my buddy cam the Canon C300"> <img width="39.69%" src="https://foruda.gitee.com/images/1675694323421773247/93b553de_5631341.jpeg" title="BSyU-3yCEAEHdOz.jpg MAGICAL WEDDING SHOOT On top of World...well, er...LA."> <img width="39.69%" src="https://foruda.gitee.com/images/1675694331530441816/10cd0943_5631341.jpeg" title="Bvr3I14CIAAdhPC.jpg LAYING THE GROUNDWORK Missing my little 'ole Canon C100 cam"></p>

- Contact 

  WE LOOK FORWARD TO CONNECTING WITH YOU.

  - MMDV
  - (818) 571-9908
  - Los Angeles, California
  - info@makemydayvisions.com

  HAVE A NICE DAY.

# [STEAM PLAY & LEARN](https://www.quartoknows.com/books/9781633225268/STEAM-Play-Learn.html)

Subtitle20 fun step-by-step preschool projects about science, technology, engineering, art, and math!

Ana Dziengel

Price$15.95 / £11.99

With the simple instructions and large, full-color photos in STEAM Play & Learn, preschoolers will love tackling the 20 fun, easy-to-follow step-by-step projects as they learn about science, technology, engineering, arts, and math.
 
The acronym STEAM stands for Science, Technology, Engineering, Art, and Math. At the preschool level, this doesn’t mean trying to teach your kids robotics and engineering, but rather presenting them with open-ended projects that allow them to problem-solve, make mistakes, get creative, and most of all, have fun.
 
While most preschool-age children are not yet reading, STEAM Play & Learn is meant to be child-driven. The images, illustrations, and projects are meant to appeal to kids of their age and abilities. Let them be the guide as you explore the projects in this book. Topics include symmetry and how light bounces to create reflections with mirror mandalas, diffusion and capillary action with tie dye towels, structural framing and bracing with marshmallow structures, and electrical currents with salty circuits. Other projects include:

- Color Mixing Lab
- Frozen Goop
- Sound Tubes
- Citrus Volcanoes
- Paper Bag Blocks
- Egg Carton Geoboards
- Pool Noodle Marble Run
- Potions Lab
- Balance Scale
- Spin Art Tops
- Marble Mazes
- Tinkering Set
- Pattern Projectors
- Newton’s Cradle
- Art Machine
- Lime Light

The projects in this book fall into three difficulty levels: Easy projects require parents and teachers to do nothing more than set out the materials and offer a challenge to children. A few of the easy projects will need parents to assemble components in advance of presenting them to kids. Medium projects will require some adult assistance while the project is underway. Children should be able to complete some of the tasks while stopping periodically for an adult to assist in a step. Difficult projects are meant to be adult-led. This is an opportunity for parents to encourage their child to offer problem-solving ideas along the way, giving children a chance to help and make suggestions.

The cross-subject approach to learning in STEAM Play & Learn will prepare young children for the subjects they will soon learn in elementary school and beyond. These 20 projects will provide hours of fun education for both kids and parents!

| items | comments |
|---|---|
| Category: | Go To Subject AreaArts, Crafts & Hobbies, Go To Subject AreaKids & Teens
| Subject area: | Go To CategoryArt for Children, Go To CategoryNonfiction
| Format: | FormatTrade Paperback 80 Pages
| ISBN: | ISBN9781633225268
| Size: | Size8.50 in x 11.00 in / 215.90 mm x 279.40 mm
| Published: | Published DateJune 5th, 2018
| Imprint: | Go To ImprintWalter Foster Jr

### Author

 **Ana Dziengel** 

Ana Dziengel is an architect, award-winning furniture designer, and professional blogger. She began blogging at Babble Dabble Do in 2012 and found her dream career as an expert crafter, amateur scientist, and impromptu art teacher to her three young children. Ana wants her blog to help other families connect through creativity and feel comfortable making and tinkering. She has been a regular contributor to PBS Parents Crafts for Kids and Fisher Price, and has been featured in FamilyFun magazine. 

### Reader Reviews

---

![输入图片说明](https://foruda.gitee.com/images/1675679403280443476/c424d6e0_5631341.png "屏幕截图")

STEAM Play & Learn: 20 fun step-by-step preschool projects about science, technology, …
- Play by Ana Dziengel

STEAM Play & Learn: 20 fun step-by-step preschool projects about science, technology, engineering, art, and math!
- Amazon	4.6/5 

> With the simple instructions and large, full-color photos in STEAM Play & Learn, preschoolers will love tackling the 20 fun, easy-to-follow step-by-step projects as they learn about science, technology, engineering, arts, and math. The acronym ST…

- Author: Ana Dziengel
- First published: Jun 05, 2018
- Genre: Children's literature

- [12本英美孩子必备的STEAM教育书籍，你家孩子读过吗？-幼儿编程入门](http://www.code6.cn/post/6165.html)
  code6.cn|1080 × 1417 jpeg|Image may be subject to copyright.

  ![输入图片说明](https://foruda.gitee.com/images/1675679764973095879/f3bde706_5631341.png "屏幕截图")

  STEAM Play & Learn
  - Ana Dziengel(作者)
  - 适合：4-8岁

  ![输入图片说明](https://foruda.gitee.com/images/1675679489406902371/02f7cc9f_5631341.jpeg "code6-1535546031.jpeg")

  学前儿童在学习STEAM（科学、技术、工程、艺术和数学）时，会喜欢处理这20个有趣的项目、可以按照每个项目的步骤完成，简单易上手。

  包括对称性和如何产生镜面反射，结构框架和支撑棉花糖结构，电流咸电路。每一个项目都有彩色图片说明，过程中孩子和父母都能享受到乐趣。

  ![输入图片说明](https://foruda.gitee.com/images/1675679810583557148/9b014472_5631341.png "屏幕截图")

  这种跨学科的学习方法，可以帮助孩子开发大脑，提升思维和智力，为即将进入小学学习的学生做好准备。

- [Book Review: STEAM PLAY & LEARN](http://make-it-your-own.com/book-review-steam-play-learn/)
  make-it-your-own.com|2000 × 2000 jpeg|Image may be subject to copyright.

  ![输入图片说明](https://foruda.gitee.com/images/1675680363275062193/19a07c0e_5631341.jpeg "R-C.jpg")

- [STEAM Play & Learn by Ana Dziengel | Quarto At A Glance | The Quarto Group](https://www.quartoknows.com/books/9781633225268/STEAM-Play-Learn.html)
  quartoknows.com|5100 × 3300 jpeg|Image may be subject to copyright.

  ![输入图片说明](https://foruda.gitee.com/images/1675680376550187306/bf8470fd_5631341.jpeg "O (2).jpg")

  ![输入图片说明](https://foruda.gitee.com/images/1675680389318126689/3f866883_5631341.jpeg "O (1).jpg")

  ![输入图片说明](https://foruda.gitee.com/images/1675680401244988470/0f6cd6af_5631341.jpeg "O.jpg")

- [STEAM Play & Learn - Babble Dabble Do](https://babbledabbledo.com/steam-play-and-learn/)
  Babble Dabble DoBabble Dabble Do|1280 × 828 jpeg|Image may be subject to copyright.

  ![输入图片说明](https://foruda.gitee.com/images/1675680435272518185/c4085306_5631341.jpeg "STEAM-PL-interior.jpg")

---

【笔记】[悟空风筝](https://gitee.com/gzk-0913/siger/issues/I6D1K1)