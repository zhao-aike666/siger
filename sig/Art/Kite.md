- [悟空风筝](https://gitee.com/gzk-0913/siger/issues/I6D1K1)
- [10 FUN KITE MAKING IDEAS FOR KIDS](https://www.hellowonderful.co/post/10-fun-ways-to-make-a-kite/) 
- Tyvek Sled Kite (via [Babble Dabble Do](https://babbledabbledo.com/how-to-make-a-kite/tyvek-sled-kite-instructions-babble-dabble-do/))

<p><img width="706px" src="https://foruda.gitee.com/images/1675711268227200012/5f913cac_5631341.jpeg" title="525风筝副本.jpg"></p>

> 风筝飞得在远，线都在父母的手中，小树长的再高，一样在他们心里，这是《[风筝和小树](../我们/风筝和小树.md)》的诗意。 @[Penny](https://gitee.com/penny1125) 交稿的一刻我又再一次重温了这首诗，这就是我们的诗意生活的感觉。“春天的风筝” 主题活动，开始啦，因为更详细的文案还是要得到更多同学的支持，这里只 MARK 一下，记录下此刻的心情。这期手作专题的完成也是那么自然，流畅，不做任何雕饰，就是全程记录它的学习过程。

封面选取了原作的攻略实拍照片，它记录了一个时刻，或老师或父母正在带着一个或者一众小盆友再试飞。如果严格时间线，它的原作至少要到80年代，尽管我不能找到那本我记忆中的杂志。附上封面标题的作品，我心中的悟空风筝，是我轻快地复现的版本，抛砖引玉。开篇的语音转文字稿，是 SIGer 伴礼手工组的培训小会，我沿用了传统的 “一言堂” 模式，自顾自地分享了我的故事，期待这枚火种能够分享出去，这一刻，我知道，随着一枚枚小风筝，火种会诞生在更多孩子的心中。（校对版，有劳手工组的两位同学啦 :pray: ）

> 培训材料里的 《迎春》是另一位同学的选题，用来介绍 SIGer 编委工作 职责和流程的，也贡献了即将开启的活动 “春天的风筝”。剧透在笔记结尾 A4纸风筝，就是即将推出的 春天的风筝 的核心内容啦。

# [悟空风筝](https://gitee.com/gzk-0913/siger/issues/I6D1K1)

@袁德俊 创建于  2023-02-06 11:13

### 该问题是怎么引起的？

![输入图片说明](https://foruda.gitee.com/images/1675652981264471953/392335db_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1675652993956811057/6e873558_5631341.png "屏幕截图")

### @yuandj 我语音分享下这个小故事

1.

那个大家可以看一下这个小风筝，这个风筝，实际上是我大概小学的时候，因为那个时候实际上没有互联网，就是还是很传统的叫的杂志，就平面媒体，因为学校的每年的就是这个，应该是寒，那个寒假的时候会定全年的杂志，所以，小时候，实际上就是没有什么其他的，这个这个类似获得信获得课外知识的渠道，就是通过。这种杂志就是各种杂志，所以，就是因为男生，就是我，我实际上那个时候就也就算是这种，这个爱爱动手，虽然就那种类似于科科普杂志的性质的，具体到什么杂志我可能忘了，但是，每每月一期的这个杂志，我都特别喜欢看其中的这种小制作环节，因为它可以动手，有有成果。

2.

这印象那么深刻，因为那是我第一次照着这个杂志，DIY了一个风筝之后，又能够飞上天，因为这个风筝本身的这个尺寸，主要是用这个一张普通的那种，A4纸的大小来制作的，所以，它不会很大，它也飞不了太高，然后线呢，就是用那种特别普通的那种，就是过去可能妈妈们还会用用用这个针线去修，修修补补什么的。有那种棉线，现在就一卷儿棉线，然后，大概能飞到4334层楼高的样子，你像小学生，如果能够放到三四层楼高，其实已经很开心了，然后，我本身小学就是美术这课代表，然后就美术小组的这个成员，所以也会画一些装饰，所以就在那上边儿画了一个，就是因为我本身又又有图样，就这儿画了一个这个孙悟空的这个。

3.

头俩不像现在你，你们看这这，这两张图是我动议要，要要给大家讲这故事，先先先用这个软件绘制的，因为有印象非常深刻，就是它的那个结构非常简单，而且比例关系非常清晰，所以我印象特别深，然后，就是按照这个思路就复现了，所以我想，我我我会把我今天复现这个风筝的DIY过程，凭着记忆做一下，然后回头再再再使。再实验一次，那么这样就是一个非常完整的这个DIY的一个教程，那么也就是说我这上一次这个培训的时候，给大家讲的一个很核心的点，目的是什么？就是我们转载也好，还是说自己原创也好，一定是要有一个可以分享的这个过程能够让这个其他同学看到之后，就像我刚才分享的这个，我看到这个科普杂志一样。

4.

他的分享当中能够这个带来一一一一定的这种这种体验，不管是成就感也好，还是感动也好，所以这就是一个传标准的一个传播方式，不光是志愿精神，还是这刻薄小知识，还是什么，所以这就是，载体非常重要，也就是我们的，真正的行动的这个这个形式是什么，所以这就是这个故事，就是非常清晰的，就是表达了老师，从小爱爱爱动手，然后成为。这个科技迷，包括现在在从事这个信息产业的这个这个科技，都是跟这个小时候的爱好，那分不开的，所以这就是科普的价值，因为可能男生女生和女生的这种爱好不一样，就是所以这是老师的一个分享，所以作为一个抛砖引玉。

### @yuandj 下面我针对 @深夜的奇迹 的建议逐一说明。

「袁德俊: 「深夜的奇迹：
老师，我很支持学姐的看法，宣传确实非常重要。可以用微信等软件和除了志愿北京以外的网站也多做一下宣传，这样会有更多人了解我们的期刊。我觉得宣传本身也要有创新，有吸引力，这样才有更多人来帮我们宣传（可以是转发一类的）」
- - - - - - - - - - - - - - -
先说宣传和杂志」

5.

那既然是宣传，那就叫就叫做这个渠道，或者说这个介质，那么杂志是一个介质，但它还有一个传播渠道，就是我们要把这杂志送到别人手里，那比如现在互联网非常轻，方便的就是直接一键转发，对？然后，过去，可能就是要要要要要要要要要这种订阅，然后或者说那种传统的这种方式，那现在我们这个那杂志社也有这个印刷版本，因为这个印刷版本和这个手机版本，还有那个视频。不太一样，那不太一样，那可能与时俱进，然后有这个形式创新，但它的这个这个核心还是内容还是内容。所以说第一个建议，就是我们不光是在志愿北京招募，志愿者来产出内容，我们还要把这个内容发布出去，所以那么发布出去的时候，我们不能说，这个这个就是，还和我们这个在志愿北京，招募的志愿者的那个信息是一样的，所以说才要有成果。

6.

之前已经说明了，所以，包括今天这个培训内容和这个之前培训内容都是讲我们伴手礼组，重重要的，这个这个责任是什么？就是汇集整个CDCC个杂志社的这个成果，我们得有成果才能够，比如说干就是，触动到这个，就是这个接受者的这个这个心理才能够，比如说不管是带来快乐也好，还是带来感动也好，一定要有成果，所以这就是伴手礼组的一个核心使命，所以内容是关键，内容是关键。

7.

说明一下这个转发，那首先咱们是一个志愿服务小组，那么，实际上志愿北京是有很很明确的这个规定的，那种互联网转发本身它是不能做作为志愿服务的，但作为我们志愿者本身这种分享的这种动力，这是没问题的，这是作为我们，我们这个产出之后的一个延伸，那之所以这个是，我要提前说明一下，所以现在以前这个有那种的，所以说。转发朋友圈儿，然后什么之类的那种计时的方式，这是明确禁止的，这不是不是不是禁止这个转发到这个行动，就是转发本身是不计时的，但是你产生内容这是没问题的，这是我们的志愿服务的核心，对？把自己学到的科普知识也好，把自己了解到的一些这优秀的志愿服务活动，这个行程内容，然后再传不出去，这行程内容的过程是计时。

—————————
「袁德俊: 「深夜的奇迹：
而且作为一个志愿活动，我觉得可以在杂志内容本身上做一些文章，出一些又吸引人，又和分享和爱相关的题目，或者把现有的频道和我们的志愿精神结合，来实现志愿火种的传递。然后同时在科普方面更出彩，这样可以提高我们的作品质量，本身也有一定的宣传作用。」
- - - - - - - - - - - - - - -
再说下内容，志愿服务的结合。包括科普的关系。」

8.

我们这个所有新同学都会收到一个简单的介绍，那其中有我们这个C格编辑部这两年的一个成果，就是C格九九点儿，GT点儿io，那是一个首页，那个首页是这个所有期刊的一个汇集，同时，在在顶部有这个，我们目前积累的43个频道，基本上有有有更多了，就是一个，集中展示，那我们这个伴手礼组，没有单独的频道。没有单独的频道，那么我们规划到比如说手工，对，我们现在就就就算作了这个艺术频道，类似然后，第二个，就是说这个志愿服务，我们也没有单独的频道，那我们现现在基本上，都是放到这个生活频道里，因为生活频道是个大类，志愿服务就是我们的学习生活，然后，我们现在有一个专属的频道叫。

9.

公益频道是什么？就是介绍优秀的公益项目，就是公益项目，是是更的大范围的一个一个一个一个门类，那么每个公益公益项目，都其中都会有志愿者和志愿服务的环节，所以是这么一个关系，那那我们传播的内容当中又有分享爱和我们这个火种的一个，那这这种独特的这个这个故事和内核儿，所以它只是一个就志愿服务中的。一个部分，那我们这个，是，在这个分享志愿服务精神的这个内核驱动下，以这个科普来作为这个形式，来来来来通过这个分享科普知识，然后，能够让我们更多的这个同学感受到这个志愿精神的这种价值。

10.

是，比如说这刚刚刚才的这个小风筝的制作，然后这个促成老师，爱动手，爱科技的这个这个这个这个这个爱好，然后，还有我我这个上次分享的这个，去演灾区的这个，这个这个栀子花的这个故事，这这都是我们的这个感动，那么这一个一个的感动，我希望，每个同学能够通过。自身的，行动也好，或者自身的一个经验也好，然后再再次的去传递出去，那等于说，现身说法的方式，才是高效的。那么，比如转发老师的故事和老师的这个感动的不是不是不可以，但是这个，这并不是老师希望的，所以我，我为什么会花那么大的力气，来给你们两位同学来做培训，就是这个意义。

11.

这样两个同学能够，有自己的产出，然后再拿自己的产出，不管是内容也好，还是这个这体会也好，然后再去传播出去，然后再通过这个接接收者的这种反馈，形成一个这个再思考，再再升华，那么这个过程就是老师希望的，所以老师会花力气去，哪怕感去去感动一个同学，那都是有有意义的，所以这是我们这个飞哥，或者说我们。火种的真正的内核和和这个，这这这个这个形式，所以就是，我们的志愿团团队的一个特点，所以这就是今天我们就我老师又又借题发挥的，我说了很多，然后希望同学们能够有所体会，然后转转转转过头来结合自己的情况来制定具体的行动方式。

—————————
「袁德俊: 再次分享了科普火种的小故事。同学们应该了解了，这两次培训的目的啦，不求复刻笔记老师培训内容，但求激发的个人行动计划。」
—————————
「袁德俊: 比如，老师结合今天的回顾，就给自己立了一个项《悟空风筝》可以丰富手工组的内容。」
—————————
「袁德俊: 马上也到春天了。各组同学各有侧重，素材可以自行组织。」

![输入图片说明](https://foruda.gitee.com/images/1675653303304285611/6149af7d_5631341.png "屏幕截图")

—————————
「袁德俊: https://gitee.com/mr-bolin/siger/issues/I6CXE5 
《我们》诗社，开放测试，新学期和小同学做诗友。

——

昨天上新的以诗会友的活动，同样适用手工组。同样的一张纸，可以记录文字和故事，也可以分享手工小制作，一份成品加一个攻略，是标准的火种教程的形式。」
—————————
「袁德俊: 「袁德俊：[该消息类型暂不能展示]」
- - - - - - - - - - - - - - -
民族棋中国史项目的产出就是一面是攻略，一面是棋盘。在分享过程中就有很多故事产出，被记录在棋盘纸上啦。以棋会友，以诗会友都是一样的。就和陪伴孩子，陪伴老人，形式不同，陪伴是一样的。和孩子们一起玩就是核心内容啦。有很多志愿服务都是可以用得上的。」
—————————
「袁德俊: 老师计划，咱们手工组，可以独立发招募信息，更具体，更详细。相信会有更多同学响应我们，并一起行动。这个任务就在你们两个想好，做什么，就开始吧。加油@黄一兰 @深夜的奇迹 」
—————————

### 悟空风筝

![输入图片说明](https://foruda.gitee.com/images/1675653229039126040/8f9491d4_5631341.png "屏幕截图")

「袁德俊: 这张A4纸，可以作为一个成品，丰富下攻略，就可以用来传递啦。」
—————————

---

# [10 FUN KITE MAKING IDEAS FOR KIDS](https://www.hellowonderful.co/post/10-fun-ways-to-make-a-kite/)

By: AuthorAgnes

Posted onLast updated: March 2, 2021

CategoriesCreate, Latest

Originally posted on 12/06/2015

In the summer months, playing outside becomes a fun daily adventure. A kite makes for a fantastic way to play outdoors and also makes an entertaining summer craft. 

We’ve rounded up 10 creative kite design ideas for you to make your own DIY kite. Below are 10 different ways to make this nostalgic summer toy – from cool tetrahedral designs to colorful confetti kites! Then there are also simple folded paper kites, or ones kids can fill in with their artistic doodles. 

Many of these can actually fly high in the sky which makes them a toy worth making. And while you are playing outdoors check out my guide on [summer activities](https://www.hellowonderful.co/post/8-AWESOME-WATER-SUMMER-ACTIVITIES) to keep you inspired while outside.

### Creative Kite Making Ideas

Do you know how to make a kite? If not, don’t worry, we’ve got you covered. With the creative kite design ideas below, you’ll want to try them all! Most of the kites will keep you occupied all day as you [play outdoors](https://www.hellowonderful.co/post/10-OUTRAGEOUSLY-FUN-WAYS-TO-PLAY-OUTDOORS), others are great for the creative kids to practice their design skills.

![输入图片说明](https://foruda.gitee.com/images/1675675138911944372/9c8dc702_5631341.png "屏幕截图")

### Simple Paper Kite (via [Inner Child Fun](http://innerchildfun.com/2015/04/how-to-make-a-simple-kite.html))

Paper kite making is fun and simple. This paper kite can easily be made by a kid in less than 5 minutes. And the bonus is you’ll probably already have all the materials needed at home.

![输入图片说明](https://foruda.gitee.com/images/1675675201074452348/706198b6_5631341.png "屏幕截图")

### Tetrahedral Kite (via [Instructables](http://www.instructables.com/id/10-Cell-Treahedral-Kite/))

Make this impressive 10-cell tetrahedral creative kite that’s an eye-catcher in the sky. You’ll need simple materials like straw and wrapping paper.

![输入图片说明](https://foruda.gitee.com/images/1675675228456861885/8db533d0_5631341.png "屏幕截图")

### Confetti Kite (via [Parent Savvy](http://parentsavvy.com/articles-and-blog/309/))

This vibrant confetti kite is one of the most innovative kite-making ideas. Kids can get hands-on in making this decorative kite which looks so pretty flying high up in the sky.

![输入图片说明](https://foruda.gitee.com/images/1675675241256265833/457c3d30_5631341.png "屏幕截图")

- KID-MADE DECORATIVE DRIFTWOOD KITE   By: AuthorAgnes
  Posted onLast updated: February 22, 2018
  CategoriesCreate, Latest

What kid doesn’t love to collect sticks? We are always trying to think of ways to display their treasures!

![输入图片说明](https://foruda.gitee.com/images/1675675705476512671/5961c386_5631341.png "屏幕截图")

Materials:

- 2 or more pieces of large paper (we used watercolour paper, but anything durable could work, such as poster board)
- Driftwood or sticks
- Glue gun (to be used by an adult) or craft glue
- Paint (we used watercolour and a bit of craft grade acrylic)
- Painting tools (brush, old gift card)
- Cotton rope
- Scissors
- Beads or wooden balls

 **Painted Beads**  (Optional: If you want to paint them to coordinate with your kite)

![输入图片说明](https://foruda.gitee.com/images/1675675828011428850/12921bae_5631341.png "屏幕截图")

- Wooden beads
- Acrylic paint
- Floral foam
- BBQ skewers
- Pony beads
- Paint brush or brushes

We also find these items helpful:

<p><img height="99px" src="https://foruda.gitee.com/images/1675675865270250746/003d3863_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1675675947843529966/76a12d17_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1675675954303463498/98546d6e_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1675675962894697130/c851587c_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1675675966922258426/afbd2481_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1675675973614503705/f0c3e491_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1675675979809936720/7eb5390d_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1675675986266815763/2c30f00e_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1675675992906022575/fd76062a_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1675676000548009942/97a1f2d2_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1675676006252544221/a2363445_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1675676012804418262/007c5066_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1675676018792409842/b3f4fd9b_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1675676025925617595/c68b55ff_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1675676033954253606/4a7dbbb0_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1675676039630984280/963ed761_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1675676046581833056/3a9db40d_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1675676054336755713/01cfb9d6_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1675676059266964945/b5b14ac5_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1675676067096873517/5fc5d9c0_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1675676081610159493/f1d4ae3e_5631341.png"><img height="99px" src="https://foruda.gitee.com/images/1675676088104417204/a7226bdb_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1675676094365372487/20385c48_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1675676100777355608/57b64191_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1675676106633424216/8a8f246f_5631341.png"><img height="99px" src="https://foruda.gitee.com/images/1675676117003589079/c246cff0_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1675676124299935984/4092fc74_5631341.png"><img height="99px" src="https://foruda.gitee.com/images/1675676130437696662/21202d40_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1675676136091706843/dafb0617_5631341.png"><img height="99px" src="https://foruda.gitee.com/images/1675676142273543254/5f3bb918_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1675676163930199216/9c086b37_5631341.png"></p>

### Decorative Driftwood Kite (via [hello, Wonderful](https://www.hellowonderful.co/post/KID-MADE-DECORATIVE-DRIFTWOOD-KITE))

Go on a nature walk and turn your collected finds into a rustic driftwood kite drawn in with the kids’ artwork. This is one of the most creative kites to make and allows your child to express their own creativity. Not only that, but they will also learn how to reuse discarded materials – perfect for a thriving eco-warrior.

![输入图片说明](https://foruda.gitee.com/images/1675675267361900442/befb2603_5631341.png "屏幕截图")

> Tyvek Sled Kite Instructions BABBLE DABBLE DO
  July 2, 2013 by Ana Dziengel Leave a Comment

  [![输入图片说明](https://foruda.gitee.com/images/1675675581520738708/dfad6fea_5631341.png "Tyvek-Sled-Kite-Instructions-BABBLE-DABBLE-DO")](https://cdn.babbledabbledo.com/wp-content/uploads/2013/07/Tyvek-Sled-Kite-Instructions-BABBLE-DABBLE-DO-e1372775243480.jpg)

### Tyvek Sled Kite (via [Babble Dabble Do](https://babbledabbledo.com/how-to-make-a-kite/tyvek-sled-kite-instructions-babble-dabble-do/))

This bright and colorful Tyvek Sled kite with bright streamers would bring a smile to any kid excited to fly it.

![输入图片说明](https://foruda.gitee.com/images/1675675287034495155/74a8e815_5631341.png "屏幕截图")

### Finger Kites (via [That Artist Woman](http://www.thatartistwoman.org/2010/09/finger-kites.html))

Make a cute finger kite, they are a great way to show off your kids’ art and fun for little hands to hold. These kites are smaller than the usual kite, so they only fly about a meter high but the kids still love them!

![输入图片说明](https://foruda.gitee.com/images/1675675305844330709/5e4a57c7_5631341.png "屏幕截图")

### Butterfly Kites (via [Creative Jewish Mom](http://www.creativejewishmom.com/2012/06/butterfly-kite-kids-craft-with-straws.html))

Use colorful tissue paper as your kite decoration material. You’ll be able to create different shapes like these adorable butterfly kites.

![输入图片说明](https://foruda.gitee.com/images/1675675329031342616/e7b641d9_5631341.png "屏幕截图")

### Simple Paper Kite (via [Made by Joel](http://madebyjoel.com/2010/11/paper-kite.html))

Quite possibly the easiest kite for kids to make and the best part is that it actually flies!

![输入图片说明](https://foruda.gitee.com/images/1675675349945072674/676db51b_5631341.png "屏幕截图")

### Stained Glass Kites (via [Make and Takes](http://www.makeandtakes.com/colorful-stained-glass-kites))

These lovely decorative kites make sparkly window decorations and are a fun art project for the kids. These stained glass kites are perfect for bringing out the artistic side of your child, and for decorating windows. But these kites don’t actually fly so this is a perfect creative kite-making activity for winter.

![输入图片说明](https://foruda.gitee.com/images/1675675371367902652/c004344e_5631341.png "屏幕截图")

### Colorful Kites (via [Red Ted Art](http://www.redtedart.com/2011/05/27/how-to-make-a-beautiful-kite/))

Can you believe these gorgeous kites were made by kids? There are endless possibilities here to make your own colorful kite in the simple tutorial above.

### Final Thoughts on DIY Kite Ideas 

Kite-making for kids is the perfect activity. It is simple and easy and keeps your little one entertained for hours. 

What are your favorite [summer DIY crafts](https://www.hellowonderful.co/post/diy-mini-summer-doodle-books-with-free-printables/)? Whether you are playing indoors or outdoors there are plenty of fun ideas to keep you entertained.

---

【笔记】[悟空风筝](https://gitee.com/gzk-0913/siger/issues/I6D1K1)

- [幼儿手工作品：纸风筝的制作方法 - 环保手工 - 咿咿呀呀儿童手工网](http://www.yiyiyaya.cn/xiaofaming/huanbao/14049.html) （[转载](https://gitee.com/gzk-0913/siger/issues/I6D1K1#note_16069145_link)）

  做一只独一无二美丽的风筝，和孩子一起放风筝。只需纸，木头和绳子就可以做成一只在天空翱翔的风筝。对孩子来说，这是一个完美的工艺，是教他们设计和测量一种不错的方式。

  ![幼儿手工作品：纸风筝的制作方法](https://foruda.gitee.com/images/1675672697384793702/c0877fd6_5631341.png "幼儿手工作品：纸风筝的制作方法")

  材料请注意：我们的风筝用的是一块特卫强。特卫强做的风筝是完美的,因为它是很轻的，耐水，难以撕裂，你可以用它来做。如果你不想去麻烦采购特卫强，一些信件是用它做的，下次你收到的邮件，保存它，可以用来做风筝！你也可以尝试用纸或塑料购物袋做一个风筝。

  ![幼儿手工作品：纸风筝的制作方法](https://foruda.gitee.com/images/1675672822477748175/c90ea7b6_5631341.png "屏幕截图")

  材料/工具：

  - 特卫强纸
  - 细绳
  - 胶带
  - 吸管
  - 纸板
  - 铅笔
  - 剪刀
  - 直尺
  - 打孔机
  - 订书机
  - 皱纹纸条尾巴

  步骤方法：

  1. 按照下面模板上显示的比例，用铅笔和尺子在特卫强纸上画出你风筝上的图案，你可以按比例调整模板，以符合您使用的纸张大小。

     ![幼儿手工作品：纸风筝的制作方法](https://foruda.gitee.com/images/1675672882125380952/16a028c0_5631341.png "Sled Kite Template www.babbledabbledo.com")

     ![幼儿手工作品：纸风筝的制作方法](https://foruda.gitee.com/images/1675672952771208772/70b6df2b_5631341.png "屏幕截图")

     ![幼儿手工作品：纸风筝的制作方法](https://foruda.gitee.com/images/1675672697384793702/c0877fd6_5631341.png "幼儿手工作品：纸风筝的制作方法")

  2. 裁剪出风筝的形状
  3. 加固你风筝的胶带
  4. 用打孔机在模板上距离最远的两个角上各打个洞，以用来穿绳。
  5. 一个吸管的一端插入到另一个吸管的一端，使它们能够紧密结合在一起就可以了，你的组合吸管的长度需要匹配风筝的长度。重复这个过程，使你有两个秸秆“棒”，然后固定两杆的位置。
  6. 开始添加尾巴！裁剪皱纹纸，用订书订在风筝底部。
  7. 剪一条风筝两倍宽的缰绳。将缰绳穿过孔后打个结，另一边重复上面动作。
  8. 剪出你要的飞绳长度，这取决于你考虑风筝飞多高所需的长度。在硬纸板中间剪个缺口，将飞绳缠绕在纸板上，最后留下四十几厘米的一段自由绳子。
  9. 在缰绳子中间做一个环，现在把飞绳穿过环。

     ![幼儿手工作品：纸风筝的制作方法](https://foruda.gitee.com/images/1675673015851727826/1c5edd70_5631341.png "屏幕截图")

     ![幼儿手工作品：纸风筝的制作方法](https://foruda.gitee.com/images/1675673029311453774/2a340c43_5631341.png "屏幕截图")

  完成了！你还可以在风筝上面贴一些图案，来装饰你的风筝。

  ![输入图片说明](https://foruda.gitee.com/images/1675673095717182103/04727b4e_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1675675452758032756/efb8d4ed_5631341.png "屏幕截图")

- [10 FUN KITE MAKING IDEAS FOR KIDS](https://www.hellowonderful.co/post/10-fun-ways-to-make-a-kite/)

- [10种自制创意风筝做起来，春天当然不能错过它们，太好玩啦！](https://view.inews.qq.com/a/20220410A03SV700)

  [图片上传失败(image-ZOx81gbzlyDLLhyYB7fr)]

  10. 白纸风筝

  风筝的外形有很多种，只要你敢于尝试  
  就总是能做出很多新鲜的样式  
  甚至可以将自己脑海中设想的完全不存在的形象  
  也可以用风筝的方式来帮你展现出来  
  下面这款风筝若是喜欢，也可跟着一起做

  ![输入图片说明](https://foruda.gitee.com/images/1675698458058299374/0781064c_5631341.png "屏幕截图")

  准备好白纸、纸胶带、皱纹纸、剪刀、吸管等

  ![输入图片说明](https://foruda.gitee.com/images/1675698465177508642/317af716_5631341.png "屏幕截图")

  先在白纸上画出如图所示的六边形  
  并在每个角粘贴上胶带  
  将吸管粘贴在如图所示位置区域  
  再用好看的纸胶带进行固定  
  在做好的风筝上粘贴皱纹彩纸做的穗子  
  将丝线一端固定在风筝上  
  另一端缠绕在快递纸板上

  ![输入图片说明](https://foruda.gitee.com/images/1675698490533379486/5dfac82a_5631341.png "屏幕截图")

  ![输入图片说明](https://foruda.gitee.com/images/1675698539609105750/d5b0cf30_5631341.png "屏幕截图")

  拿着这样的风筝，叫上孩子的小伙伴们  
  周末去户外放风筝玩起来吧

  ![输入图片说明](https://foruda.gitee.com/images/1675698548742037534/d88dd905_5631341.png "屏幕截图")

  ![输入图片说明](https://foruda.gitee.com/images/1675698555979635540/9802a419_5631341.png "屏幕截图")

  ![输入图片说明](https://foruda.gitee.com/images/1675698591897294717/ba8ed84d_5631341.png "屏幕截图")

  风筝，放飞的不仅是快乐  
  更是想要追求更高更远的梦想  
  不管是自制的风筝还是买来的  
  在周末的闲暇时间，多陪陪孩子   
  这种陪伴也许会成为疗愈孩子终身的良药  
  若是喜欢今天的内容，欢迎分享给更多人

- [本期封面](https://gitee.com/gzk-0913/siger/issues/I6D1K1#note_16078728_link)

- [工笔绘画 孙悟空,高清图片,电脑桌面-壁纸族](https://www.bizhizu.cn/photoview/28733/8.html)
  bizhizu.cn|1820 × 1969 jpeg|Image may be subject to copyright.

  ![输入图片说明](https://foruda.gitee.com/images/1675693427820465447/f908bd43_5631341.jpeg "e5774d5f325c1bacad03cb3a840ce449.jpg")

- [黄金分割在logo设计中的应用|平面_UI_纯艺术|观点|搞设计的阿明_原创文章-站酷(ZCOOL)](https://www.zcool.com.cn/article/ZMTExOTg0MA==.html)
  zcool.com.cnzcool.com.cn|1145 × 1116 jpeg|图像可能受版权保护。

  ![输入图片说明](https://foruda.gitee.com/images/1675693457556619940/c7ae913c_5631341.png "屏幕截图")

- A4纸风筝

  - 【风筝】用塑料袋在家制作风筝
    https://www.bilibili.com/video/BV1ne4y187w3

  - 手工制作：春暖花开 带着孩子用A4纸做个风筝 一起去春游吧！
    https://v.qq.com/x/cover/mzc003cfvief268/r3330c18zrl.html

  - 自己如何自制简单的风筝
    https://jingyan.baidu.com/article/066074d6431b22c3c21cb0c8.html