- [苏州最硬核夏校，SNA火箭营开营在即！](https://mp.weixin.qq.com/s?__biz=MzA5NjQyMjQwNQ==&mid=2649917892&idx=1&sn=b158ccdd095fd03e3218beea4f6ff505)
- [苏州北美火箭营特别项目：CANSAT卫星创新工程实践活动，助力学子逐梦蓝天！](https://mp.weixin.qq.com/s?__biz=MzUxMDQyOTE1Mw==&mid=2247490628&idx=1&sn=2f9d786272aac203bebf8d06a284aec3)
- [感受数独魅力, 体验智力激荡 | SNA Sudoku Competition数独大赛](https://mp.weixin.qq.com/s?__biz=MzA5NjQyMjQwNQ==&mid=2649928428&idx=1&sn=01b4cb45ca0122887bf042dc98410af1)

<p><img width="706px" src="https://foruda.gitee.com/images/1666583311799782733/c520ec2d_5631341.jpeg" title="368SNA.jpg"></p>

CANSAT 是当前 SIGer 确定的目标，Sudoku 则是火焰棋实验室的起点之一，以及 SIGer 少年芯的新起航 “吉祥数独” 的载体，国际学校也是科创少年的主力军，这就是 本期校园专题的缘起。

# [苏州北美国际高级中学](https://baike.baidu.com/item/%E8%8B%8F%E5%B7%9E%E5%8C%97%E7%BE%8E%E5%9B%BD%E9%99%85%E9%AB%98%E7%BA%A7%E4%B8%AD%E5%AD%A6/9521942)

### 苏州市一所中美合作创办的国际高中

苏州北美国际高级中学创建于2013年5月，由美国北卡州立大学和江苏吴中集团合作创办，经江苏省教育厅批准[苏教发(2013)28号]的省内第一所、也是唯一一所中美合作创办的国际高中。创立苏州北美国际高级中学，旨在引进现代学校制度、先进的教育理念和管理体系，打造精品国际中学教育。 [1] 

| 中文名 | 苏州北美国际高级中学 | 校训 | VIEWING THE WORLD AS ONE |
|---|---|---|---|
| 外文名 | Suzhou North America High School | 所属地区 | 中国江苏 |
| 简称 | SNA | 类型 | 国际学校 |
| 创办时间 | 2013年5月 | 现任校长 | 王莹 |
| 办学性质 | 美国高中课程 AP课程 | 京领排名 | 2022年竞争力美国本科方向第71, <br>英国本科方向第50 [6] | 

### 相关视频

- 「视频」[苏州北美国际高级中学：定制最合适的教育，成就每个孩子的梦想](https://baike.baidu.com/video?secondId=59148807&lemmaId=9521942) 5890 播放 02:42

 **目录** 

1. 学校简介
2. 办学优势
3. 地理位置
4. 所获荣誉

### 学校简介

<p><img width="23.59%" src="https://foruda.gitee.com/images/1666575402474554297/d182c60b_5631341.png"> <img width="23.59%" src="https://foruda.gitee.com/images/1666575409592021861/7f91b92b_5631341.png"> <img width="23.59%" src="https://foruda.gitee.com/images/1666575416650030281/930a1db7_5631341.png"> <img width="23.59%" src="https://foruda.gitee.com/images/1666575432107830658/a5cac273_5631341.png"></p>

> 北美风光(4张)

苏州北美国际高级中学经江苏省教育厅批准、国家教育部备案，为省内唯一 一所完整实施美国高中课程体系的国际学校。

学校与北卡州立大学深度合作实行整体化的美式教育，是北美知名高校在中国的重要生源基地。面向中外籍学生，招收优秀的初中毕业生和高中在读生。

教育就是改变，成才就是方向。国际学校的教育，就是长翅膀的教育。由北卡州立大学专家量身定做的美式课程体系，与国外知名高校无缝对接的教学内容与学习方式，再加上一对一的升学指导，是助你成功起飞的翅膀。高品质的教师团队，精品化的小班教学，创意型的社团活动，个性化的教育辅导，是托举你高飞的翅膀。全身心的呵护，鼓舞着飞翔的信心与勇气，激发出振翅的意志与能力。携手北美，拥有世界。 [1] 

### 办学优势

1. 江苏省唯一 一所省教育厅批准、国家教育部备案的国际高中。
2. 完整实施美国高中课程体系的国际高中。
3. 拥有资深的、经验丰富的具有多元文化的美国教师。
4. 课程体系包含：美国原汁原味的高中基础课程，还有TOEFL、SAT/ACT、AP等国际学生必修课程；
5. 全英文授课，小班制、分层式教学。
6. 深度的国际文化交流：PBI、IC、IEP等。
7. 开设最丰富的学生俱乐部社团活动，多方位、多角度、深度多元化地培养学生综合素质与能力。
8. 严谨的学生管理，督导制培养模式。
9. 多元化的考评体系，实施发展性评估，关注学习过程，培养自学能力。
10. 中美结合的专业升学指导团队进行升学指导，让每一位学生获得最理想的大学深造机会。 [1] 

### 地理位置

苏州北美国际高级中学坐落于苏州市吴中区太湖新城，环境优美，周边交通便利，方便快捷。

太湖新城位于苏州南部，南临太湖水，背靠七子山脉，是苏州“一核四城”城市发展战略的重要组成部分，也是吴中走进“太湖时代”战略的发展方向。

地址：江苏省苏州市吴中区太湖新城天鹅荡路268号 [1] 

![输入图片说明](https://foruda.gitee.com/images/1666575585474389054/3478f71d_5631341.png "屏幕截图")

© 2022 Baidu - GS(2021)6026号 - 甲测资字11111342 - 京ICP证030173号 - Data © 百度智图
> 苏州北美国际高级中学（新校区）

### 所获荣誉

- 2018年10月，在京领新国际发布的中国国际学校竞争力排行榜中排第257。 [2-3]  
- 获京领2020中国国际学校竞争力排行榜美国本科方向第98/英国本科方向第79 [2] 
- 获京领2020中国国际化学校品牌价值百强榜全国第85 [4] 
- 获京领2020中国国际学校创新竞争力百强榜全国第55 [5] 
- 2022年3月31日，在京领新国际所发布的“2022中国国际学校竞争力排行榜”中，位于美国本科方向第71名，英国本科方向第50名 [6]  。

### 词条图册

<p><img width="29%" src="https://foruda.gitee.com/images/1666575675207950825/93232d9e_5631341.png"> <img width="29%" src="https://foruda.gitee.com/images/1666575727991756181/7ceb23d7_5631341.png"> <img width="29%" src="https://foruda.gitee.com/images/1666575671965400680/7af857fe_5631341.png"></p>

> 概述图册(3)

### 参考资料

1. 苏州北美国际高级中学   ．苏州北美国际高级中学网站．2013-07-03[引用日期2013-08-19]
2. 2020中国国际学校竞争力排行榜正式发布   ．人民政协网[引用日期2020-02-10](http://www.rmzxb.com.cn/c/2020-12-30/2750344.shtml)
3. [京领新国际发布“中国国际学校竞争力排行榜”](https://baike.baidu.com/reference/9521942/f892IhQQ0USXwtvGehJdC2W2IdMXax8J0j0ypozhaaikL_dI9wajdtWDODtGKg3TlU4lhjD28noae1iyZdk5eVgkjxkvfHAAwDAMgSNXhDB5FiRbPFWYZGhiWG-NB6LJWyuYIg)   ．国际在线．2018-11-01[引用日期2018-12-28]
4. 诺奖见证｜2020中国国际化学校品牌价值百强榜正式发布   ．腾讯[引用日期2021-02-18](https://new.qq.com/rain/a/20201227A0B17900)
5. 诺奖见证｜2020中国国际学校创新竞争力百强榜正式发布   ．百度[引用日期2021-02-18](https://baijiahao.baidu.com/s?id=1687282433668733119)
6. 北上广深名校汇聚，京领2022中国国际学校竞争力榜单发布   ．中国日报[引用日期2022-09-22](https://ex.chinadaily.com.cn/exchange/partners/82/rss/channel/cn/columns/sz8srm/stories/WS6250010ba3101c3ee7acf8a2.html)

# [苏州最硬核夏校，SNA火箭营开营在即！](https://mp.weixin.qq.com/s?__biz=MzA5NjQyMjQwNQ==&mid=2649917892&idx=1&sn=b158ccdd095fd03e3218beea4f6ff505)

苏州北美国际高级中学 2021-05-14 19:01

![输入图片说明](https://foruda.gitee.com/images/1666576148198930384/db8cded3_5631341.png "屏幕截图")

- 微信号：苏州北美国际高级中学 sna-highschool
- 功能介绍：苏州北美完整实施美国高中课程体系。六十名外教全英文授课；开设80大俱乐部，全面提升学生综合素质；中美升学团队，一对一升学指导；丰富的国际交流项目。苏州北美助您直升美国、加拿大等世界名校！

五一假期后  
许多家长和孩子们  
不由为即将到来的暑假而感到“头痛”  
是继续学习深造，不断提升自己  
还是外出玩耍，尽情享受假期生活  
那不妨来苏州北美SNA夏季火箭营  
趣味与知识并重  
 **全英文授课、最强师资**   
 **多元化定制课程、高端科创文化体验**   
 **7月苏州北美SNA火箭营**   
开营在即  
........

### 营地特色

- 定制多元化课程，适合全年龄段学习体验
-  **小班分层教学，全英文授课** 
- 苏州唯一 **卫星火箭项目教学实践地** 
- 数十类 **特色俱乐部体验** ，提升综合素质能力
- 苏州北美 **SNA各学科教研组组长联合执教** 
- 体育类 **国家一级教练员授课** 
-  **10天封闭式** 私校环境，食宿安心

![输入图片说明](https://foruda.gitee.com/images/1666576290806982731/338d3e23_5631341.png "屏幕截图")

### 营地设施

2021年SNA火箭营的主要阵地位于苏州北美国际高级中学，校园内设施完备，包括体育馆、足球场、篮球场、音乐厅、图书馆、录音室、舞蹈房、物理超导实验室、生物技术实验室等，我们致力于为学生提供充满氛围感的教学环境及完善便利的生活配置。

<p><img width="49%" src="https://foruda.gitee.com/images/1666576320994203491/67730136_5631341.png"> <img width="49%" src="https://foruda.gitee.com/images/1666576323467039789/a785e62d_5631341.png"> <img width="49%" src="https://foruda.gitee.com/images/1666576331058935215/9c75f332_5631341.png"> <img width="49%" src="https://foruda.gitee.com/images/1666576330590653466/4d68227b_5631341.png"> <img width="49%" src="https://foruda.gitee.com/images/1666576334652439063/c0c359d4_5631341.png"> <img width="49%" src="https://foruda.gitee.com/images/1666576341227136968/bd82cd1c_5631341.png"></p>

### 营地类型一：AP课程提优营（美国大学先修课）

- 招募对象：推荐8-11年级学生
- 在营时间:10天
- 参考课表：

| 时间/日期 | 7月6日-10日 | 7月11日-14日 | 7月15日 |
|---|---|---|---|
| 7:30-8:25 | 早  餐 | 早  餐 | 早  餐 |
| 8:30-10:00 | AP经济学 | 英语辩论/演讲 | CANSAT卫星发射 仪式 |
| 10:15-11:45 | AP微积分 | AP科学 |
| 11:45-13:30 | 午餐/午休 | 午餐/午休 |
| 1:45-4:45 | 俱乐部/竞赛 | CANSAT | 卫星火箭项目 |
| （三选一） | 特色 | 俱乐部 | 体验 |
| | 数学竞赛 | （AMC:8-10; | AMC:10-12+） |
| 5:00-6:30 | 晚餐 | 晚餐 | / |
| 6:30-8:30 | 晚自习/活动 | （电影、戏剧、<br>汉字听写大赛等北美特色活动） | / |

 **注意事项：** 

1. 数学竞赛项目与特色俱乐部项目只能择其一，若 **选择数学竞赛项目，则不可选择特色俱乐部** ；
2. 选择CANSAT 卫星火箭活动项目，夏令营 **前5天** 仍可选择特色俱乐部课程体验或数学竞赛项目；夏令营 **后5天** 开启CANSAT 卫星火箭活动项目；
3. 也 **可单项选择特色俱乐部课程体验** ，不参与数学竞赛项目、CANSAT 卫星火箭项目。

 **入营费用：** 

-  **7288元（不参与CANSAT 卫星火箭活动项目）** 
-  **9988元（包含CANSAT 卫星火箭活动项目）** 

备注：此费用含营期内所有活动课程费及午餐、晚餐、住宿、外出项目用车费等。

[CANSAT卫星火箭活动项目介绍--戳这里](https://mp.weixin.qq.com/s?__biz=MzUxMDQyOTE1Mw==&mid=2247490628&idx=1&sn=2f9d786272aac203bebf8d06a284aec3&scene=21#wechat_redirect)！

### 营地类型二 沉浸式趣味学术营

- 招募对象：推荐4-8年级学生
- 在营时间:10天
- 参考课表：

| 时间/日期 | 7月6日-10日 | 7月11日-14日 | 7月15日 |
|---|---|---|---|
| 7:30-8:25 | 早  餐 | 早  餐 | 早  餐 |
| 8:30-10:00 | 外教英语 口语教学 | 英语配音/演讲 | CANSAT卫星发射 仪式 |
| 10:15-11:45 | 趣味数学 | 科学实验/计算机 |
| 11:45-13:30 | 午餐/午休 | 午餐/午休 |
| 1:45-4:45 | 俱乐部/竞赛 | CANSAT | 卫星火箭项目 |
| （三选一） | 特色 | 俱乐部 | 体验 |
| | 数学竞赛 | （AMC:8-10; | AMC:10-12+） |
| 5:00-6:30 | 晚餐 | 晚餐 | / |
| 6:30-8:30 | 晚自习/活动 | （电影、戏剧、<br>汉字听写大赛等北美特色活动） | / |

 **注意事项：** 

1. 数学竞赛项目与特色俱乐部项目只能择其一，若 **选择数学竞赛项目，则不可选择特色俱乐部** ；
2. 选择CANSAT 卫星火箭活动项目，夏令营 **前5天** 仍可选择特色俱乐部课程体验或数学竞赛项目；夏令营 **后5天** 开启CANSAT 卫星火箭活动项目；
3. 也 **可单项选择特色俱乐部课程体验** ，不参与数学竞赛项目、CANSAT 卫星火箭项目。

 **入营费用：** 

-  **7288元（不参与CANSAT 卫星火箭活动项目）** 
-  **9988元（包含CANSAT 卫星火箭活动项目）** 

备注：此费用含营期内所有活动课程费及午餐、晚餐、住宿、外出项目用车费等。

[CANSAT卫星火箭活动项目介绍--戳这里](https://mp.weixin.qq.com/s?__biz=MzUxMDQyOTE1Mw==&mid=2247490628&idx=1&sn=2f9d786272aac203bebf8d06a284aec3&scene=21#wechat_redirect)！

### 师资介绍

本次SNA火箭营由苏州北美SNA各学科教研组组长联合执教，老师们经验丰富、专业知识过硬、教学技巧强、课程生动有益。在夏令营中，我们将通过多元化课程，助力孩子们学习提优，培养兴趣，度过趣味暑假生活！

![输入图片说明](https://foruda.gitee.com/images/1666577171901624546/5671dc7f_5631341.png "屏幕截图")

- 何其新 Angelo

  - 现任校长助理，分管初中教学
  - 中国人民大学经济学本硕学位
  - 美国匹兹堡大学经济学博士
  - 经济学教研组组长、精英班项目协调人、学生会学术部指导老师和校辩论队、全美经济学挑战赛领队等职务
  - 现主要教授AP经济学和AP Capstone课程，获得College board官方的AP培训课程证书

![输入图片说明](https://foruda.gitee.com/images/1666577207747800581/780cb52e_5631341.png "屏幕截图")

- 吴申扬 Peter

  - 本硕毕业于伦敦帝国理工大学数学专业
  - 获得美国数学竞赛（AMC&AIME)满分和奥林匹克竞赛金牌
  - 现任苏州北美SNA招办主任兼AP微积分、AP Capstone任课老师

![输入图片说明](https://foruda.gitee.com/images/1666577239873055544/fdf2fa9e_5631341.png "屏幕截图")

- Vincent Roy

  - 毕业于University Of New Brunswick
  - 获得经济学硕士学位
  - 现主要教授AP宏观经济学和AP 微观经济学课程

![输入图片说明](https://foruda.gitee.com/images/1666577267976006680/431bfc97_5631341.png "屏幕截图")

- 洒荣霄 Rongxiao

  - 本科毕业于南京大学化学系
  - 硕士毕业于美国明尼苏达大学
  - 现主要教授美高化学和AP化学
  - 教龄11年，熟悉考纲，擅长提分

![输入图片说明](https://foruda.gitee.com/images/1666577298773053718/8c8c5991_5631341.png "屏幕截图")

- 陈华华 Andy

  - 科学教研组组长
  - 6年物理教学经验
  - AP 物理全系列100%通过率

![输入图片说明](https://foruda.gitee.com/images/1666577325439146885/03738e40_5631341.png "屏幕截图")

- 吴明莉 Lily

  - 数学教研组组长
  - AP 微积分AB/BC通过率100%

### 特色俱乐部体验

SNA火箭营从学术科技、艺术设计、营养美食、体育竞技等领域，多维度发展学生个性，培养其核心素养。

![输入图片说明](https://foruda.gitee.com/images/1666577375003801367/ae789cf9_5631341.png "屏幕截图")

★ 柔 道 : 强身健体的同时，加强专注力、提升自信

![输入图片说明](https://foruda.gitee.com/images/1666577381005214046/dc456f7f_5631341.png "屏幕截图")

★ 油 画 : 增长知识、提高审美、培养感知及想象能力

![输入图片说明](https://foruda.gitee.com/images/1666577417053652444/fe703b51_5631341.png "屏幕截图")

> 图片源于网络

★ 攀  岩 : 平衡身体协调能力，锻炼学生意志力及进取心

![输入图片说明](https://foruda.gitee.com/images/1666577442229371821/8fb2da7e_5631341.png "屏幕截图")

★ 陶  艺 : 亲近自然，提高艺术素养、锻炼手眼协调能力

![输入图片说明](https://foruda.gitee.com/images/1666577463221090329/87659308_5631341.png "屏幕截图")

★ 手  球 : 强身健体，提高反应能力及团队配合能力

![输入图片说明](https://foruda.gitee.com/images/1666577476519485322/039d4d34_5631341.png "屏幕截图")

★ 马  术 : 锻炼平衡感、塑造优美体态、增强心理素质

![输入图片说明](https://foruda.gitee.com/images/1666577485551141678/dbe39657_5631341.png "屏幕截图")

★ 烹  饪 : 生活技巧学习，了解食材、培养独立性

![输入图片说明](https://foruda.gitee.com/images/1666577504669284195/e74ae488_5631341.png "屏幕截图")

> 图片源于网络

★ 击  剑 : 培养高雅气质，锻炼反应能力

![输入图片说明](https://foruda.gitee.com/images/1666577523251776869/0ef1de65_5631341.png "屏幕截图")

★ 帆  船 : 拓宽视野，增强责任感、培养耐心

![输入图片说明](https://foruda.gitee.com/images/1666577543377063751/90945c29_5631341.png "屏幕截图")

★ 高尔夫 : 情绪调节，身体柔韧性锻炼，礼仪培养

![输入图片说明](https://foruda.gitee.com/images/1666577551053217120/23f5fc84_5631341.png "屏幕截图")

★ 微电影制作 : 逻辑思维能力、创造能力及美学素养提升

注：该课程结业有作品展示。

![输入图片说明](https://foruda.gitee.com/images/1666577572387031994/14581e56_5631341.png "屏幕截图")

扫描上方二维码

即可报名今夏最硬核的火箭营

名额有限，期满截止

........

### SNA活动预告

点击下方链接直达

- [5.15主任面对面（中考方向特色专场）](https://mp.weixin.qq.com/s?__biz=MzUxMDQyOTE1Mw==&mid=2247490613&idx=1&sn=3466693ddd2a91853c0a7a196979be7d&scene=21#wechat_redirect)
- [5.15主任面对面（国际方向特色专场）](https://mp.weixin.qq.com/s?__biz=MzA5NjQyMjQwNQ==&mid=2649917519&idx=1&sn=37bb8f1bed2cea419217804a699ffd8d&scene=21#wechat_redirect)
- [苏州北美火箭营特别项目：CANSAT卫星创新工程实践活动，助力学子逐梦蓝天！](https://mp.weixin.qq.com/s?__biz=MzA5NjQyMjQwNQ==&mid=2649917835&idx=1&sn=120f43dbdb2231a50a510a701f9a1e06&scene=21#wechat_redirect)

-END-

# [苏州北美火箭营特别项目：CANSAT卫星创新工程实践活动，助力学子逐梦蓝天！](https://mp.weixin.qq.com/s?__biz=MzUxMDQyOTE1Mw==&mid=2247490628&idx=1&sn=2f9d786272aac203bebf8d06a284aec3)

苏州百美外国语学校 2021-05-12 17:24

![输入图片说明](https://foruda.gitee.com/images/1666578029092460705/af8ac157_5631341.png "屏幕截图")

- 微信号：苏州百美外国语学校 sna-fls
- 功能介绍：苏州北美外国语学校采用小班教学，在完成国家义务教育课程的基础上，增设课外服务：英语强化课程、社团俱乐部、主题实践活动等。通过“优质、开明、灵动”的教育管理，让孩子们在学校收获自信，快乐成长。旨在培养具有家国情怀、全球视野的未来精英。

### 航/天/梦 SUMMER TIME

为纪念中国航天事业发展的伟大成就， **唱响“发展航天事业，建设航天强国”的主旋律** ，国家决定，自 2016 年起，将每年 ** 4 月 24 日** 定为“ **中国航天日** ”。中国航天人以自己的勇气和智慧跨越生命的极限和自然的障碍，一步步将传说和梦想化为现实，跨越苍穹的探索给人类带来一次又一次成功出现的惊喜和激动，也带来造福人类的应用价值。飞天梦是强国梦的重要组成部分，随着中国航天事业的快速发展，中国人探索太空的脚步会迈得更大、更远！

![输入图片说明](https://foruda.gitee.com/images/1666578092394287783/6d97c66a_5631341.png "屏幕截图")

为激发学生对航天进行更多的了解与探索， **苏州北美SNA火箭营特别开设暑期CANSAT 卫星创新工程实践活动** ，CANSAT 卫星设计创新挑战为全球未来太空学者大会(GFSSM)中国区下设的子项目， **而GFSSM是由美国航天基金会、英国国家空间学院、中国航天基金会、ITCCC联合发起** 的面向全球青少年的航天科普教育活动，让孩子们通过 CANSAT 卫星设计、制作与发射学习等过程更多关于卫星的科学知识。

........

### CANSAT 卫星创新工程实践活动

CANSAT 卫星创新工程实践活动旨在模拟真实卫星任务的各个方面，包括设计、开发、测试、发射、 操作和数据分析。

![输入图片说明](https://foruda.gitee.com/images/1666578141076738098/4623e9dc_5631341.png "屏幕截图")

 **一 招募对象** 

7-11年级学生（对科技类、 航天航空和动手实操有热情与激情同学们， **不要求具备航天项目经验** ，在活动期间专家讲师会对活动内容进行讲解与辅导。）

 **二 项目时间** 

暂定7月11日-7月15日

 **三 课程介绍** 

学生可在一个易拉罐大小的空间内设计和制作一个卫星，这个卫星可以被 CANSAT 火箭发射到两公里的高空。卫星在降落的过程中会打开降落伞，滞空时间可以长达 8-10 分钟。学生可以利用这个平台设计一个他们想做的任务并制作出一个 CANSAT 卫星，通过 CANSAT 火箭发射来验证设计是否合理。

![输入图片说明](https://foruda.gitee.com/images/1666578189789691994/ba19584e_5631341.png "屏幕截图")

 **四 课程模块** 

课程分四大模块。

-  **第一个模块是认识 CANSAT 卫星** ，探测高空到地面的温度和气压变化，及进行创意载荷设计；
-  **第二个模块是传感器** 信息获取，组帧，板间通信，无线传输，地面站接收、存储和显示；
-  **第三个模块是创意载荷设计制作** ，利用所学知识编程实现创意载荷的使用场景设计和制作；
-  **第四个模块整星 AIT** ，这个模块学生需要完成将自己做的 CANSAT 卫星结构组装起来、程序集成和整星测试这些工作。保证卫星能够经历运输、发射和回收过程依然能够正常工作。
- 最后， **专业老师会对学生撰写的文档进行评审，综合评定出成绩** 。

<p><img width="49%" src="https://foruda.gitee.com/images/1666578259541643956/580ed461_5631341.png"> <img width="49%" src="https://foruda.gitee.com/images/1666578264527505814/3663196e_5631341.png"> <img width="49%" src="https://foruda.gitee.com/images/1666578267956434554/4af2cc34_5631341.png"> <img width="49%" src="https://foruda.gitee.com/images/1666578272293017193/6846e80c_5631341.png"></p>

 **五 课程安排** 

| | 时间 | 内容 |
|---|---|---|
| D1 | 下午 | 开营仪式、航天科普、 任务发布、电路设计与焊接 |
| | 晚上 | 必选任务设计、星务板功能设计与实现 |
| D2 | 下午 | 电源板、通信板、地面站功能设计与实现 |
| | 晚上 | 创意载荷设计与实现 |
| D3 | 下午 | 整星桌面联试 |
| | 晚上 | 整星桌面联试、 CANSAT 卫星 AIT-1 |
| D4 | 下午 | CANSAT 卫星 AIT-2 |
| | 晚上 | CANSAT 卫星发射活动与数据接收 |
| D5 | 上午 | CANSAT 卫星放飞及数据分析 |
| | 下午 | 结营仪式 |

 **六 课程成果** 

- 了解现实科学中典型的探究性学习方法
- 加强基本技术、物理和程序设计的课程知识
- 了解团队合作的重要性
- 提升学生沟通与表达的能力
- 个人背景提升、获得相关证书

苏州北美SNA  
一直以来走在科创型国际教育的前列  
并在学术科技、艺术设计、体育竞技等方面  
都取得卓越的教学成果  
而本次CANSAT 卫星创新工程实践活动  
不仅多维度、多元化的提升学生的核心素养  
更能助力孩子们  
逐梦蓝天 放飞梦想  
........


### SNA活动预告

点击下方链接直达

- [5.15主任面对面（中考方向特色专场）](https://mp.weixin.qq.com/s?__biz=MzUxMDQyOTE1Mw==&mid=2247490538&idx=1&sn=6b03ba1a3f9dcb7182fa44cf77bfc13e&scene=21#wechat_redirect)
- [5.15主任面对面（国际方向特色专场）](https://mp.weixin.qq.com/s?__biz=MzUxMDQyOTE1Mw==&mid=2247490538&idx=1&sn=6b03ba1a3f9dcb7182fa44cf77bfc13e&scene=21#wechat_redirect)

![输入图片说明](https://foruda.gitee.com/images/1666578537884747780/191cae08_5631341.png "屏幕截图")

-END-

# [感受数独魅力, 体验智力激荡 | SNA Sudoku Competition数独大赛](https://mp.weixin.qq.com/s?__biz=MzA5NjQyMjQwNQ==&mid=2649928428&idx=1&sn=01b4cb45ca0122887bf042dc98410af1)

苏州北美国际高级中学 2022-10-21 15:14 发表于江苏

![输入图片说明](https://foruda.gitee.com/images/1666579063988659490/57c55795_5631341.png "屏幕截图")

数独不孤独，你我来相逐；  
纵横九宫格，雄才天下合。

你听说过“数独”吗？你第一次听到“数独”的反应是什么？难，还是有挑战？

### 01 数独起源

最早在《易经》中出现过“洛书九宫图”，即为数独的前身。近代数独是在18世纪初，由瑞士数学家欧拉等研究的拉丁方阵而发明出来的。Sudoku一词来源于日语，意思是“单独的数字”。

![输入图片说明](https://foruda.gitee.com/images/1666579090275147137/c49296c5_5631341.png "屏幕截图")

### 02 数独元素

数独就是一种填数字游戏。常见的数独为九宫格的正方形状，每一格再细分为一个九宫格。在9个九宫格中填入数字1到9，且保证每个数字在每一行、每一列及每个九宫格中只出现一次。答题者需要通过逻辑推敲，找到空格中需要填写的数字。

![输入图片说明](https://foruda.gitee.com/images/1666579106421290120/c7078a46_5631341.png "屏幕截图")

### 03 数独赛事

世界数独锦标赛：由世界智力谜题联合会组织的国际性最高水准数独赛事。

中国数独锦标赛：选拔数独国家队，参加世界数独竞标赛。

![输入图片说明](https://foruda.gitee.com/images/1666579122881501825/7d0294ad_5631341.png "屏幕截图")

### 04 SNA Sudoku Competition

数独的魅力不仅仅在解出答案，更在于对数字的敏感度的提高，对数字的逻辑推敲。通过不同的方式，找到最终解，是所有数独人最快乐的瞬间。

数独不仅仅有传统的九宫格数独，还有变形数独，如对角线数独，迷你数独，锯齿数独，连体数独，杀手数独。甚至还有立体数独等等各种各样的变化形式。

![输入图片说明](https://foruda.gitee.com/images/1666579140863241116/ff5fcc70_5631341.png "屏幕截图")

> 练习题

希望北美的同学们可以通过这个小小的数独比赛，对数独产生更浓厚的兴趣，不断挑战自我，挑战有更高难度，更奇特形状的数独。

### 比赛时间

10月26日  16:05-17:00

### 比赛地点

1#C10

### 报名方式

识别下方二维码报名

![输入图片说明](https://foruda.gitee.com/images/1666579176761131307/c3f601f2_5631341.png "屏幕截图")

> 来源：高中教学部

![输入图片说明](https://foruda.gitee.com/images/1666579195671171779/6aacffa7_5631341.png "屏幕截图")

---

【[笔记](https://gitee.com/yuandj/siger/issues/I5X857#note_13860090_link)】

- [苏州北美国际高级中学2020春招校园开放日](http://www.chaojixue.com.cn/newshow.aspx?ID=1377&ClassID=3)
chaojixue.com.cn|1080 × 1309 jpeg|图像可能受版权保护。

  ![输入图片说明](https://foruda.gitee.com/images/1666582093874384690/f383e813_5631341.png "屏幕截图")

- [苏州北美国际高级中学](http://www.sna-edu.com/show/641)
sna-edu.com|1080 × 1920 jpeg|图像可能受版权保护。

  ![输入图片说明](https://foruda.gitee.com/images/1666582167668798203/c0228912_5631341.jpeg "R-C (1).jpg")