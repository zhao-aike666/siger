北美教育

- [申请美国大学的4年规划——第一阶段：信息准备](http://mp.weixin.qq.com/s?__biz=MzI0ODE1NjIzOA==&mid=2653400039&idx=1&sn=f7c874a713a4ef877ff588fb6a641bb9)
- [申请美国大学的4年规划——第二阶段：自我修炼](http://mp.weixin.qq.com/s?__biz=MzI0ODE1NjIzOA==&mid=2653400039&idx=2&sn=2b952564a82154a49e96624ab81500f0)
- [申请美国大学的4年规划——第三阶段：时间表、截止日及工作清单](http://mp.weixin.qq.com/s?__biz=MzI0ODE1NjIzOA==&mid=2653400039&idx=3&sn=1808a3f30ddbfbe22f23356fddbb58ab)
- [中学生科研专题 | 星渡研学](https://mp.weixin.qq.com/mp/appmsgalbum?action=getalbum&__biz=MzI0ODE1NjIzOA==&scene=1&album_id=1355624705532837888&count=3#wechat_redirect)

<p><img width="706px" src="https://foruda.gitee.com/images/1676403190521945172/56560dd0_5631341.jpeg" title="551北美留学副本.jpg"></p>

> “我现在主要在国内做教学及小课题研究，明年会重新开始带学生去美国，参加夏令营、科考、参加NASA竞赛；您可以宣传一下正在进行航天教育的学校以及喜欢太空的小朋友们的故事，我带的一位学生北美国名校UIUC录取了，他读的材料专业在全美排名前十。这个故事可以分享一下。当时做的课题就是用模拟火星土(从美国进口得)做砖，并测试其物理指标”，苏老师推荐说。

本篇的姊妹篇已经收录过的《[在月球上采矿？时机已日渐成熟！](NASACamps.md#在月球上采矿时机已日渐成熟)》是出自《[中学生科研专题](https://mp.weixin.qq.com/mp/appmsgalbum?action=getalbum&__biz=MzI0ODE1NjIzOA==&scene=1&album_id=1355624705532837888&count=3#wechat_redirect)》的。这都是苏老师的心血，他手持的宝典《The College Application Organizer》一定要推荐！:+1: :+1: :+1:

# [申请美国大学的4年规划——第一阶段：信息准备](https://mp.weixin.qq.com/s?__biz=MzI0ODE1NjIzOA==&mid=2653400039&idx=1&sn=f7c874a713a4ef877ff588fb6a641bb9)

原创 星渡教育编译 星渡研学 2020-06-04 21:10

![输入图片说明](https://foruda.gitee.com/images/1676379853438748556/55f51680_5631341.png "屏幕截图")

- 微信号：星渡研学 NASACamps
- 功能介绍：星渡太空科创学院项目(Starship Space Innovation Academy)官方订阅号，代表星渡（上海）教育科技有限公司发布项目最新信息，并为合作机构、家长、学生提供最新资讯

![输入图片说明](https://foruda.gitee.com/images/1676379878367308955/0ce1a5fe_5631341.png "屏幕截图")

Elizabeth Long 为美国高中生及家长写了一本升学指导手册：THE COLLEGE APPLICANT'S ORGANIZER. 笔者认为很实用、很系统，特别是其方法论值得中国的家长参考，以免被不专业的留学中介误导。计划分五期连载的方式，摘要介绍请美国大学的主要准备工作，并以此作为该书的导读。有计划购买此书的朋友，请关注最后一期，届时将公布该书的出版信息。

![输入图片说明](https://foruda.gitee.com/images/1676379901429235922/c92df0fa_5631341.png "屏幕截图")

### 第一阶段 信息准备

孩子9年级时，就要开始做准备工作了！

在这一阶段，要收集、研究大学招生信息，并缩小选择范围，定下5至8所计划申请的大学。

在本阶段，有7部分工作要做：

1. 准备工作
2. 录取信息资源
3. 找到心仪的大学
4. 研究大学
5. 实地访问大学
6. 校园考量
7. 决定要申请的大学名单

### 准备工作

首先要准备以下几份工作表单

1. 申请人的工作表(考量因素：环境、气候、规模、地理位置、离家远近、居住条件、社交、文化、政治、学术)
2. 父母的工作表(因素同上，见下图)
3. 大学基础信息汇总表（名称、网址、说明）
4. 每所大学的入学考试录取线汇总(SAT、ACT、AP、GPA等)
5. 计划实地考察的大学
6. 最终名单（分成三个档次(高档、中档、安全档)，分别列出2~3所大学，并注明每所大学的网址及是否是第一选择或第二选择，合计5~8所大学）

![输入图片说明](https://foruda.gitee.com/images/1676380074962789959/6075e58c_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1676380080517989691/8553df49_5631341.png "屏幕截图")

### 建议查阅以下图书收集有价值的信息

- Admission Matters: What Students and Parents Need to Know about Getting into College (Jossey-Bass)
- Barron's The All-In-One College Guide (Barron's)
- Cracking College Admissions (The Princeton Reviews)
- Don't Worry, You'll Get In: 100 Winning Tips for Stress-Free College Admissions (Marlowe&Company)
- The Truth About Getting In: A Top College Adviser Tells You Everything You Need to Know (Hyperion)

### 找到希望录取你的学校

把要申请的学校分为三档：

- 第一档：你在申请人中，竞争力处于最后30%
- 第二档：你在申请人中，竞争力处于中游的40%，有可能申请到奖学金
- 安全档：你在申请人中，竞争力处于前30%，可能就读优秀专业，可能拿到最好的奖学金

![输入图片说明](https://foruda.gitee.com/images/1676380128261410728/5724a549_5631341.png "屏幕截图")

### 申请准备攻略部分(略)

### 实地考察大学

- 到大学里要做什么事
- 拜访你感兴趣的院系
- 午餐时到学生中心晃一下
- 到招生办看一下
- 在校园里吃一顿饭
- 参加面试(针对11或12年级的学生)

### 校园考量因素

- 公共与隐私
- 大与小
- 校园政治
- 艺术气息
- 多元性与相似性
- 娱乐设施

### 校内的升学指导老师与校外的升学顾问

在中学里通常会有升学指导老师，为学生提供大学录取信息、择校建议、升学规划、协助申请等服务，这是很好的一个渠道。但是，由于该老师面对的是众多的在校学生，你不能指望从这位老师那里获得你需要的全部关照。通常这位老师不会有时间获得各所大学的一手信息，因为指望他们实地访问各所大学是不现实的。这项工作最终要靠申请人及家长自己完成。

如果你想从专家那里获得一手信息或一流的信息，而且不差钱，校外的独立升学顾问就是必然的选择。独立升学顾问对以下学生特别有价值：

- 想就读录取率很低的好大学，
- 动机不够强，想获得额外的帮助，以便就读高性价比的大学(录取率相对高一些)

### 关于常春藤大学

SAT满分但却被拒于常春藤大学的案例司空见惯。除非你提前被常春藤大学录取了，否则，你一定需要申请中档和安全档的大学。

常春藤大学里最难以琢磨的是这些大学(见下图)：

- 布朗大学
- 哥伦比亚大学
- 康奈尔大学
- 达特茅斯大学
- 哈佛大学
- 普林斯顿大学
- 耶鲁大学

还有一些大学虽不是常春藤，但却与常春藤大学一样好(见下图)：

![输入图片说明](https://foruda.gitee.com/images/1676380225415309112/c99544f2_5631341.png "屏幕截图")

### 怎样建立最终的申请名单

(略)

![输入图片说明](https://foruda.gitee.com/images/1676380239648995720/c42314f9_5631341.png "屏幕截图")

------------------------------------------------------------------------------

# [申请美国大学的4年规划——第二阶段：自我修炼](https://mp.weixin.qq.com/s?__biz=MzI0ODE1NjIzOA==&mid=2653400039&idx=2&sn=2b952564a82154a49e96624ab81500f0)

原创 星渡教育编译 星渡研学 2020-06-04 21:10

![输入图片说明](https://foruda.gitee.com/images/1676385050856015367/c0cefe90_5631341.png "屏幕截图")

- 微信号：星渡研学 NASACamps
- 功能介绍：星渡太空科创学院项目(Starship Space Innovation Academy)官方订阅号，代表星渡（上海）教育科技有限公司发布项目最新信息，并为合作机构、家长、学生提供最新资讯

Elizabeth Long 为美国高中生及家长写了一本升学指导手册：THE COLLEGE APPLICANT'S ORGANIZER. 笔者认为很实用、很系统，特别是其方法论值得中国的家长参考，以免被不专业的留学中介误导。计划分五期连载的方式，摘要介绍请美国大学的主要准备工作，并以此作为该书的导读。有计划购买此书的朋友，请关注最后一期，届时将公布该书的出版信息。

![输入图片说明](https://foruda.gitee.com/images/1676385085898630982/04eb4366_5631341.png "屏幕截图")

### 第二阶段 自我修炼——让自己成为理想的候选人

很多人想知道大学招生部门采用的秘密准则，想知道大学会接纳哪些人、拒绝哪些人。尽管无法断定大学们赋予每个申请要素的准确权重，但他们对每个要素重要性的排名顺序很明确，这些要素的重要性从高到低依次为： **平时分数级别(GPA)、标准化考试分数、课外活动、社区服务和暑假活动** ，这些都会被大学招生老师认真考虑和审核。(如果你是因为体育特长或其他奖学金的原因被录取，则上述顺序不适用)

![输入图片说明](https://foruda.gitee.com/images/1676385109441862938/dac8fabc_5631341.png "屏幕截图")

### 1 平均成绩(GPA)

大学录取官会考察你的课程难度及你在班里的排名，以决定你的GPA的权重。举个例子：与编篮子手工课的A相比，在微积分课得个A，给录取官的印象要更加深刻。如果你的GPA是3.8，但在班里排名在后50%，这样的成绩不能与GPA3.5并在班里排名前25%相提并论。

### 要成绩A还是要读AP课程？

当然，最理想的是读AP课程并得到A。在现实生活中，如果你能得到B，就可以读AP课程；如果你只能得到C，那还是读一般的课程，把大学水平的课程留到大学入学后再读吧。

### 2 标准化考试

那些平均成绩(GPA)好但标准化考试成绩差的学生，有时ACT成绩会高于SAT成绩，这是因为ACT考查学生对知识的掌握程度，而SAT考查抽象逻辑推理能力。

### SAT与ACT

几乎所有大学都会采用这两种标准化考试中的一种，来评估申请人。美国东部和西部的热门大学更流行采用SAT，美国中西部和南部的大学则喜欢采用ACT。很多大学声称，不会偏爱其中一种考试，但从申请人的心态来看，大学确是会看重更某一种考试。如果你确实担心这事，不妨问一下该大学的录取官；如果答复还是让你不满意，你可以两种考试都参加，当然这会很累。

两种考试的比较：

1. SAT有写作部分，ACT不要求(可选项)
2. ACT更加考验语法功底
3. ACT包含三角函数，SAT只包含算术、代数、几何。
4. ACT有科学部分，而SAT没有
5. SAT会用错误答案迷惑你，而ACT没有
6. 分数计算方法不同
7. 大学要看申请人全部的SAT分数，ACT考试则只看最高分。

### AP考试

就高中阶段读AP课程并通过考试，可以令申请人在高中阶段就获得大学学分。这不是申请大学必须提供的考试成绩，但最热门大学通常希望看到申请人的成绩单上有几门AP课程的考试成绩。

![输入图片说明](https://foruda.gitee.com/images/1676385185172598869/a893165c_5631341.png "屏幕截图")

### 网上备考资源

- www.barronstestprep.com
- http://sat.bostontestprep.com
- www.kaptest.com
- www.number2.com (免费！)
- http://store.collegeboard.com
- www.prepme.com
- www.review.com

### 3 课外活动

大学希望看到申请人参加一些高质量的课外活动，而不是一长串课外活动的清单。课外活动要以质取胜(深度参与几项活动，并承担领导责任)，而非单纯的堆积数量(参加了10项活动，但在活动中只是跑龙套)。活动的主题要选自己真正有兴趣的，而不要一味迎合大学的口味。

如果对自己到底喜欢什么毫无头绪，可以先在入学后先报名几样不同领域的活动体验一下，然后再决定要投身到哪个方向，以及继续参加哪些活动、放弃哪些活动。如果这样下来还是没有找到兴趣方向，可以考虑参加社区服务活动。

![输入图片说明](https://foruda.gitee.com/images/1676385220106958439/a2420fb5_5631341.png "屏幕截图")

### 社区服务

现在很多高中要求学生一定要参加社区服务。既然本来就要参加，为什么不找到一个自己认为成立的、参加这项活动的理由？而不是简单认为，这只是老大不情愿的花一个周六的时间跟着同学们捡拾垃圾。

一旦你选择了一个理由，并负责地长期坚持参与这项活动，为期一年(或至少半年)，在此期间你能带头参与工作并能发挥领导作用。这样的表现，在大学录取官眼中的权重，肯定高于那些轻松的活动(例如:带弱势族群小孩去动物园玩)。

关于活动报告的建议： _要站在大学录取官的角度看待如何写活动报告_ 。不要功利地认为参加社区服务就是为了申请大学。 _参加社区服务其实是为了让你走出自我、回报社区，以及在此过程中成长_ 。每次参加完社区活动，就把活动情况记录到你的“4年工作表单”中，待日后总结。不要每参加完一项活动，就在当周写一篇报告，并声称这是一项改变人生的活动。如果活动谈不上“责任”、“主动性”、“领导作用”，就认真的考虑另换一个合适的主题写份报告。

### 暑假活动

如果不差钱，可以参加夏令营、实习、社区服务、出国参加国际交换学生计划。

很多学生选择在暑假打工，攒大学学费或零花钱。这些学生会担心，没能像那些不必暑假打工的同学一样参加上述活动，日后可能会不利于申请大学。暑假打工是不参加上述活动的正当理由，同时也可以证明你能承担责任。找暑假工作时，要找那些可以让你承担责任、发挥主动性、锻炼领导技能的工作。

### 4 四年工作表单

在9至12年级这四年里，按时将学习成绩及课外活动情况填写进当年的工作表单，每个学年完成一张表单(见下图)。课外活动要记录参加的时长和付出的努力。在你填写大学入学申请表及个人简历时，这些工作表单就会派上用场。当你要写申请大学报告时，或简短回答问题时，就可以引用这些工作表单。

![输入图片说明](https://foruda.gitee.com/images/1676385266002466652/8c8eb801_5631341.png "屏幕截图")

### 5 个人简历

尽管在大学申请文件中无需个人简历，但对给你写推荐信的人来说，你的个人简历就是一个宝贵的工具，它可以简化推荐人的工作，因为你的成就、参加的活动、获得的奖励等亮点都一目了然。利用多出来的时间，推荐人(例如你的老师)就能给你写一份更好的推荐信。

简历的内容包括：姓名、电话号码、电邮、学术奖励与荣誉、课外活动、特别的兴趣、社区服务、暑假经历、工作经历等。

写作技巧(详略)可参考以下网站：

- www.resumeedge.com
- www.resumewriters.com
- www.resume-help.org

----------------------

# [申请美国大学的4年规划——第三阶段：时间表、截止日及工作清单](https://mp.weixin.qq.com/s?__biz=MzI0ODE1NjIzOA==&mid=2653400039&idx=3&sn=1808a3f30ddbfbe22f23356fddbb58ab)

原创 星渡教育编译 星渡研学 2020-06-04 21:10

![输入图片说明](https://foruda.gitee.com/images/1676386504899380091/f133c883_5631341.png "屏幕截图")

- 微信号：星渡研学 NASACamps
- 功能介绍：星渡太空科创学院项目(Starship Space Innovation Academy)官方订阅号，代表星渡（上海）教育科技有限公司发布项目最新信息，并为合作机构、家长、学生提供最新资讯

Elizabeth Long 为美国高中生及家长写了一本升学指导手册：THE COLLEGE APPLICANT'S ORGANIZER. 笔者认为很实用、很系统，特别是其方法论值得中国的家长参考，以免被不专业的留学中介误导。计划分五期连载的方式，摘要介绍请美国大学的主要准备工作，并以此作为该书的导读。有计划购买此书的朋友，请关注最后一期，届时将公布该书的出版信息。

![输入图片说明](https://foruda.gitee.com/images/1676386536097110413/810db52e_5631341.png "屏幕截图")

在这个阶段，要有序的坚持完成各项任务，不能忘记那些重要的截止日期。

![输入图片说明](https://foruda.gitee.com/images/1676386544223634935/1cdde732_5631341.png "屏幕截图")

### 9年级排期提示

进入高中了，随之而来的是更多的功课、更多的活动、更多的分心，以及更多的快乐。建议如下：

- 计划在前：开学后做一个综合计划表，并按重要性排序，核心课程列为最优先级
- 日常计划：制定一个每周工作日历、每天工作清单(工作按重要性排序)，每天在固定时间写作业
- 做功课时，不要看电视、听音乐、收发手机信息、打电话
- 选择你最清醒的时间学习
- 功课及任务完成后，每天花20分钟时间用于娱乐

![输入图片说明](https://foruda.gitee.com/images/1676386573302716082/f77ba509_5631341.png "屏幕截图")

### 9年级考试截止日期

考虑参加SAT II 科目考试数学和/或生物。注册截止日期如在3月末及4月，考试就在5月份。注册截止日期如在4月末及5月，考试就在6月份。

参加SAT科目考试的最佳时间是科目学习完成之后，趁大脑对科目信息的记忆新鲜，尽早参加考试。热门大学要求学生参加三门科目考试。

你所有的SAT考试成绩都将被提交给你要申请的大学。所以，如果生物(举例)不是你擅长的，你肯定你会考砸，那就不要参加这门考试。

![输入图片说明](https://foruda.gitee.com/images/1676386584809920052/e0bae371_5631341.png "屏幕截图")

### 10年级的考试截止日期

- 与学校的升学指导老师讨论何时注册PSAT或PLAN考试
- 10月份参加考试

### 11年级的考试截止日期

在手册的表单上注明PSAT、PLAN、SAT I、ACT、AP等考试的注册时间和考试时间。你应该在6月份参加考试，除非你上的是暑期学校(最快10月份考试)。

### 12年级排期建议

除了学业压力之外，你忽然发现参加课外活动的责任多了很多。你还要完成很多大学申请，要填写很多表格，回答很多问题......你现在是高年级学长了。




------------------------------------------------------------------------------

# [#中学生科研专题](https://mp.weixin.qq.com/mp/appmsgalbum?__biz=MzI0ODE1NjIzOA==&action=getalbum&album_id=1355624705532837888&subscene=159&subscene=189&scenenote=https%3A%2F%2Fmp.weixin.qq.com%2Fs%3F__biz%3DMzI0ODE1NjIzOA%3D%3D%26mid%3D2653400039%26idx%3D3%26sn%3D1808a3f30ddbfbe22f23356fddbb58ab)

| # | title | date | img |
|---|---|---|---|
| 1 | [NASA太空科学实验大赛设计案例](http://mp.weixin.qq.com/s?__biz=MzI0ODE1NjIzOA==&amp;mid=2653399768&amp;idx=8&amp;sn=ce329e101669b4ea243d601514637109) | 2020-2-12 | <img width="50px" src="https://foruda.gitee.com/images/1676391441121998217/5b7b4a97_5631341.png"> |
| 2 | [《火星救援》影片赏析之四：寻找电影中的科学错误(火星基地、种植土豆、沙尘暴...)](http://mp.weixin.qq.com/s?__biz=MzI0ODE1NjIzOA==&amp;mid=2653399780&amp;idx=1&amp;sn=7cc2285c43d39a0bf82e8c8ae1f461b8) | 2020-3-9 | <img width="50px" src="https://foruda.gitee.com/images/1676391423574002343/c62ab777_5631341.png"> |
| 3 | [《火星救援》影片赏析之三：太空通讯](http://mp.weixin.qq.com/s?__biz=MzI0ODE1NjIzOA==&amp;mid=2653399780&amp;idx=2&amp;sn=df6a16c716435d88ce9fec6c4bef666a) | 2020-3-9 | <img width="50px" src="https://foruda.gitee.com/images/1676391405176194293/0ed7a436_5631341.png"> |
| 4 | [《火星救援》影片赏析之二：太空生活](http://mp.weixin.qq.com/s?__biz=MzI0ODE1NjIzOA==&amp;mid=2653399780&amp;idx=3&amp;sn=cf060b001e40b99e26034957faf7f641) | 2020-3-9 | <img width="50px" src="https://foruda.gitee.com/images/1676391387357462218/fcf33303_5631341.png"> |
| 5 | [《火星救援》影片赏析之一：火星有水吗？如何在火星上制水？](http://mp.weixin.qq.com/s?__biz=MzI0ODE1NjIzOA==&amp;mid=2653399780&amp;idx=4&amp;sn=8a9869e6247a4b1e1d5085a3d8a95fdd) | 2020-3-9 | <img width="50px" src="https://foruda.gitee.com/images/1676391367785206762/6dd4508f_5631341.png"> |
| 6 | [纪念圆周率日特刊："派"在NASA的18个大用场！](http://mp.weixin.qq.com/s?__biz=MzI0ODE1NjIzOA==&amp;mid=2653399855&amp;idx=1&amp;sn=568b52e35c60999b789c3433a542d3d7) | 2020-3-13 | <img width="50px" src="https://foruda.gitee.com/images/1676391349699392424/cdb587f4_5631341.png"> |
| 7 | [火星生活是这样的吗？(英国篇)](http://mp.weixin.qq.com/s?__biz=MzI0ODE1NjIzOA==&amp;mid=2653399922&amp;idx=2&amp;sn=cc94a1ff6061a95afcc7869f71a11ee1) | 2020-4-9 | <img width="50px" src="https://foruda.gitee.com/images/1676391331732473503/7df8b08a_5631341.png"> |
| 8 | [在月球上采矿？时机已日渐成熟！](http://mp.weixin.qq.com/s?__biz=MzI0ODE1NjIzOA==&amp;mid=2653399967&amp;idx=1&amp;sn=891fb9c8ef6693abe523891fc8468821) | 2020-5-3 | <img width="50px" src="https://foruda.gitee.com/images/1676391313424306922/9a3dd8f8_5631341.png"> |
| 9 | [从月壤中提取冰，怎样做最省钱？佛罗里达中央大学正在研发低成本月球采矿技术](http://mp.weixin.qq.com/s?__biz=MzI0ODE1NjIzOA==&amp;mid=2653399987&amp;idx=1&amp;sn=12b74922ef50c497d576cde931076448) | 2020-5-8 | <img width="50px" src="https://foruda.gitee.com/images/1676391295692072792/faa37467_5631341.png"> |
| 10 | [火星上的咸水很多，要不要让火星车走近研究一下？](http://mp.weixin.qq.com/s?__biz=MzI0ODE1NjIzOA==&amp;mid=2653400008&amp;idx=2&amp;sn=9f98e7504433cd9da87fcbcf73c62321) | 2020-5-25 | <img width="50px" src="https://foruda.gitee.com/images/1676391278342562676/35fa9d45_5631341.png"> |
| 11 | [在月球上，宇航员的尿将是一种热门商品！](http://mp.weixin.qq.com/s?__biz=MzI0ODE1NjIzOA==&amp;mid=2653400008&amp;idx=1&amp;sn=a3cd1c681ff523e476bac4666c164ff2) | 2020-5-25 | <img width="50px" src="https://foruda.gitee.com/images/1676391258280837564/cdc49981_5631341.png"> |
| 12 | [申请美国大学的4年规划——第三阶段：时间表、截止日及工作清单](http://mp.weixin.qq.com/s?__biz=MzI0ODE1NjIzOA==&amp;mid=2653400039&amp;idx=3&amp;sn=1808a3f30ddbfbe22f23356fddbb58ab) | 2020-6-4 | <img width="50px" src="https://foruda.gitee.com/images/1676391239778183256/15cba618_5631341.png"> |
| 13 | [申请美国大学的4年规划——第二阶段：自我修炼](http://mp.weixin.qq.com/s?__biz=MzI0ODE1NjIzOA==&amp;mid=2653400039&amp;idx=2&amp;sn=2b952564a82154a49e96624ab81500f0) | 2020-6-4 | <img width="50px" src="https://foruda.gitee.com/images/1676391213204260489/3f32b858_5631341.png"> |
| 14 | [申请美国大学的4年规划——第一阶段：信息准备](http://mp.weixin.qq.com/s?__biz=MzI0ODE1NjIzOA==&amp;mid=2653400039&amp;idx=1&amp;sn=f7c874a713a4ef877ff588fb6a641bb9) | 2020-6-4 | <img width="50px" src="https://foruda.gitee.com/images/1676391165542951546/8587e522_5631341.png"> |
| 15 | [月球南极取样挑战赛(太空实验大赛系列之一)](http://mp.weixin.qq.com/s?__biz=MzI0ODE1NjIzOA==&amp;mid=2653400283&amp;idx=1&amp;sn=04c78dd14e1ab19ff7a9848b741bb18f) | 2020-10-3 | <img width="50px" src="https://foruda.gitee.com/images/1676391148343485265/1e84a1e6_5631341.png"> |
| 16 | [嫦娥七号及小行星探测科普试验设计征集活动(太空实验大赛系列之二)](http://mp.weixin.qq.com/s?__biz=MzI0ODE1NjIzOA==&amp;mid=2653400294&amp;idx=1&amp;sn=80903698cdc989584ddcf3b6a9879c81) | 2020-10-4 | <img width="50px" src="https://foruda.gitee.com/images/1676391128027595459/5cdd7c81_5631341.png"> |
| 17 | [为什么要去小行星取样？样品中可能含有揭示太阳系生命起源的重要信息！](http://mp.weixin.qq.com/s?__biz=MzI0ODE1NjIzOA==&amp;mid=2653400334&amp;idx=1&amp;sn=372969c68e226b6b9602c07fb9d4ff70) | 2020-10-19 | <img width="50px" src="https://foruda.gitee.com/images/1676391105814147924/fbafe1ae_5631341.png"> |
| 18 | [如果火星上发生了谋杀案，警察会怎样侦破? (连载二)](http://mp.weixin.qq.com/s?__biz=MzI0ODE1NjIzOA==&amp;mid=2653400411&amp;idx=2&amp;sn=9bd11b3f11fa0351b8847ffb9b88fa8c) | 2020-11-2 | <img width="50px" src="https://foruda.gitee.com/images/1676391084995729448/6b271972_5631341.png"> |
| 19 | [好奇号翻车了？“假新闻”的背后，也许是一个善意的微笑。](http://mp.weixin.qq.com/s?__biz=MzI0ODE1NjIzOA==&amp;mid=2653400455&amp;idx=1&amp;sn=b2211b43389dcecd2a34b41e1bbf86af) | 2020-11-17 | <img width="50px" src="https://foruda.gitee.com/images/1676391063600735671/c230cfe0_5631341.png"> |
| 20 | [嫦娥五号月球取样的意义何在？“外星挖土”的黄金时代或已到来！](http://mp.weixin.qq.com/s?__biz=MzI0ODE1NjIzOA==&amp;mid=2653400510&amp;idx=1&amp;sn=7711c36ad862ea338b31333184677034) | 2020-12-7 | <img width="50px" src="https://foruda.gitee.com/images/1676391033402005341/9e8b013e_5631341.png"> |
| 21 | [外星上的雨滴有多大？哈佛大学最新研究表明：居然和地球差不多！](http://mp.weixin.qq.com/s?__biz=MzI0ODE1NjIzOA==&amp;mid=2653401010&amp;idx=1&amp;sn=931254c8d8182479a4905a729f654365) | 2021-4-14 | <img width="50px" src="https://foruda.gitee.com/images/1676391013417332411/6e695590_5631341.png"> |
| 22 | [毅力号首次在火星上制造出了氧气！火星原位资源利用的一大突破！](http://mp.weixin.qq.com/s?__biz=MzI0ODE1NjIzOA==&amp;mid=2653401052&amp;idx=1&amp;sn=e3afff4c56042f1ab8bb7228f61988bc) | 2021-4-22 | <img width="50px" src="https://foruda.gitee.com/images/1676390993683951703/9f8ce8e3_5631341.png"> |
| 23 | [NASA太空实验要化身木卫一，并制造一次“迷你”极光！](http://mp.weixin.qq.com/s?__biz=MzI0ODE1NjIzOA==&amp;mid=2653401203&amp;idx=1&amp;sn=833c8f3f301815f303bd4e699f5d6964) | 2021-5-16 | <img width="50px" src="https://foruda.gitee.com/images/1676390971378525493/4cc9782a_5631341.png"> |
| 24 | [6月23日13:10，国际太空实验大赛的实验又要飞上太空了！](http://mp.weixin.qq.com/s?__biz=MzI0ODE1NjIzOA==&amp;mid=2653401321&amp;idx=1&amp;sn=c97c252a361993e73dd1b8b3a3b1d2bd) | 2022-6-22 | <img width="50px" src="https://foruda.gitee.com/images/1676390921080157128/f392e977_5631341.png"> |

---

【笔记】[欧阳自远院士揭秘：为何美国之后几十年来再无人登月](https://gitee.com/yuandj/siger/issues/I6EGTS#note_16218461_link)

[![输入图片说明](https://foruda.gitee.com/images/1676391718744403045/0b442d5b_5631341.png "屏幕截图")](https://www.amazon.sg/College-Applicants-Organizer-Everything-Application/dp/1569069859)

![输入图片说明](https://foruda.gitee.com/images/1676391747505866115/be3e7895_5631341.png "屏幕截图")

[College Application Checklist](https://secure-media.collegeboard.org/CollegePlanning/media/pdf/BigFuture-College-Application-Checklist.pdf)  
https://secure-media.collegeboard.org/College... <img align="right" src="https://foruda.gitee.com/images/1676392313077692418/b91b9831_5631341.png">

- File Size: 217KB
- Page Count: 4

> College Application Checklist Having a list of important tasks to complete for each college application will make the application process go smoothly and help you meet deadlines. Opting in to the College Board Opportunity … 

<p><img width="23.69%" src="https://foruda.gitee.com/images/1676392564757818739/3cba1c48_5631341.jpeg" title="BigFuture-College-Application-Checklist-1.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1676392575107399333/f17f9f85_5631341.jpeg" title="BigFuture-College-Application-Checklist-2.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1676392585799596531/699adfda_5631341.jpeg" title="BigFuture-College-Application-Checklist-3.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1676392595354673409/d58628d6_5631341.jpeg" title="BigFuture-College-Application-Checklist-4.jpg"></p>