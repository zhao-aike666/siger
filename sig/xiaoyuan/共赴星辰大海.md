- 清华大学计算机系 [自然语言处理 实验室](http://nlp.csai.tsinghua.edu.cn) [开源项目](http://nlp.csai.tsinghua.edu.cn/project/)

  - [THUNLP-AIPoet：诗歌自动生成模型及数据资源](http://nlp.csai.tsinghua.edu.cn/project/thunlp-aipoet%E8%AF%97%E6%AD%8C%E8%87%AA%E5%8A%A8%E7%94%9F%E6%88%90%E6%A8%A1%E5%9E%8B%E5%8F%8A%E6%95%B0%E6%8D%AE%E8%B5%84%E6%BA%90/)
  - [THUMT：神经机器翻译工具包](http://nlp.csai.tsinghua.edu.cn/project/thumt/)
  - [OpenKE：知识图谱表示学习工具包](http://nlp.csai.tsinghua.edu.cn/project/openke/)
  - [OpenNRE：可一键运行的开源关系抽取工具包](http://nlp.csai.tsinghua.edu.cn/project/opennre/)
  - [OpenAttack：文本对抗攻击工具包](http://nlp.csai.tsinghua.edu.cn/project/openattack/)
  - [OpenNE-Pytorch：开源网络嵌入工具包](http://nlp.csai.tsinghua.edu.cn/project/openne-pytorch/)
  - [OpenMatch：开放域信息检索开源工具包](http://nlp.csai.tsinghua.edu.cn/project/openmatch/)
  - [THULAC：一个高效的中文词法分析工具包](http://nlp.csai.tsinghua.edu.cn/project/thulac/)

- [丰子恺 “家塾课”：有兴趣才能做得好](http://www.chinawriter.com.cn/n1/2022/0321/c404064-32379458.html)

<p><img width="706px" src="https://images.gitee.com/uploads/images/2022/0605/195823_1194c769_5631341.jpeg" title="SIGerNLP副本name.jpg"></p>

观星&摄影：@tanrui2008

昨天神州十四号升空，第423次长2F火箭圆满发射，天宫主体即将建成，孩子们的空中课堂即将结束，天宫课堂即将开始，这振奋人心的消息，是人类奔向星辰大海的又一次坚实的脚步 :+1: 

> 我没能看直播，是因为伴随着封面的孩子的望远镜，重温了一遍《丰子恺的“家塾课”》，而它的直接原因就是本期封面的这九幅作品，尤其是主图《自制望远镜》，是丰子恺先生的长孙光学物理学家宋菲君儿时仰望星空的场景。沿着这条线索，围绕丰子恺先生兴趣爱好（吃蟹、养蚕和钓鱼），开始了一天的研学。收获颇丰，不只沿着 “有兴趣才能做得好！” 的 “丰子恺星” 的小行星命名，唤醒了观星小组的课题，“人间至味——丰子恺散文奖” 也特别符合我们爱写作的同学们。

说道这里，我要再介绍下 “我们” —— “ SIGer 兴趣小组 ” 青少年开源文化期刊，一个学生为主体的科普自媒体。学生既是作者，也是读者，以笔记的方式分享各自的学习心得，按照兴趣爱好，借助开源平台聚合其他同学的内容，最终形成一本科普百科。形式上为 开源项目 SIGer ，除内容主体外，还会有 数字化工具 [Tools](https://gitee.com/flame-ai/siger/tree/master/Tools)，以提高协作和聚合内容的效率，这是 SIGer 立项之初就制定的，科普的第一项内容就是开源本身。本期主题就是 数字化工具 NLP 开源项目推荐。

> 关键字提取一直都是自然语言处理的重点，完全依赖搜索引擎的今日，甚至有 “论文写的好，全凭关键字搜索选的好” 面对浩如烟海的开源项目，如何站长巨人的肩膀也同样重要。SIGer 编委的工作日常更是严重依赖于此。这就要引出 SIGer 校园版的需求了：

  - [sig](https://gitee.com/flame-ai/siger/tree/master/sig)/[xiaoyuan](https://gitee.com/flame-ai/siger/tree/master/sig/xiaoyuan) 是面向校园社群聚合兴趣地图的 SIGer 子频道，旨在突出每个校园的特色兴趣和强项兴趣
  - 兴趣地图，是以关键字为线索，整理个人多种兴趣和关注的学习方向的，个性展示方式，是2022年4月中以来，编辑部确定的最新的 SIGER 编委的重点任务。
  - 一篇文章有摘要，关键字，一个人也同样可以有简介和标签，一个组织同理
  - TAG 标签化，将是 SIGer 的重要学习工具，提高同类兴趣的知识聚合，并提高编辑的工作效率
  - 依照 SIGER 的社群属性，能够加速，社群依照兴趣标签聚合的速度，提高学习效率

以上关于 SIGer 校园版的论述，来自育英学校 “思与索实践展示空间” 的调研工作，一个全部由学生的研学成果组成的校园版公号，发现它也是来自该校的一次出圈儿的媒体报道 “收藏春天！”。而这次出圈儿行动，还是较早前我认识的一位该校老师的转发才了解到的。和大多数公号的定位一样 “宣传” 校园风采不同，SIGer 有自己的涉猎知识的方式，主动搜索。

> 在邀请育英学校加入 SIGer 校园分享行列后，一个课题摆在面前 《[寻找 爱分享·会思考·懂探索 的育英好少年](https://gitee.com/bryanwang1229/siger/issues/I56VML#note_10541715_link)》，前面关于 SIGer 校园版的论述，就是这个课题的精华所在，它只描述了任务目标，按照目标，借助 NLP 技术加速知识聚合，并专注兴趣培养方向，成为了 SIGer 的行动，才有了主动搜索。之后，清华 NLP 实验室的 刘知远 老师的知乎回答，让我找到了本期的主角。他自喻 NLPer 是不是和 SIGer 异曲同工呢 :+1: 然后 清华大学 NLP 实验室的全部项目都开源，专注的方向和 SIGer 当前的目标高度重合，“就是 SIGer 的菜！” 为什么所有项目的 图片 都是丰子恺的画？我要留待当面请教啦 :pray: 满满都是惊喜 ……

下面就是一个更具体的应用场景描述啦：

1. 给出一篇文章的 URL（或者黏贴进文本区），返回以下信息：
   - 9个关键字，每个附上相对的权重（%），从高到低排序
   - 摘要信息（不是文章的前多少个字，而是通过理解自主生成的）最多299个字

2. 实现文章的批量处理，可以事先通过工具下载系列文章（如从公号，网站）
   - 除了单个文章的返回信息
   - 还有所有文章的全部：
     - 关键字权重统计，
     - 关键字引用的文章数统计
   - 这批文章的整体摘要：可以扩展到599个字

3. 人工干预接口，
   - 对于以上单个文章和批量文章，需要保留人工编辑的功能，并保留版本信息
   - 将人工干预的成果，反馈给 NLP 引擎，实现作者辅助编辑功能

4. 作者信息提取，
   - 辅助完成，相同爱好作者的辅助推荐能力

5. 兴趣情报摘编能力
   - 依照兴趣关键字，对特定内容进行聚合推送，作为期刊的主体内容的比例逐步提升，降低编辑工作量。
   - 人人皆可编辑，摘编成果可继续作为素材用于 NLP 分析。
   - 实现快速迭代，加速知识聚合的速度。

6. 独立设置 SIGerNLP 工具包，与 Tools 目录剥离

> 太空出差三人组安全抵达工作岗位，接下来的半年将有各项任务等待他们完成。我们的 SIGerNLP 项目也已就位，成功的场景仿佛已在眼前，我要提前发表致谢，观星&摄影：@tanrui2008 是第一位 面向星辰大海的 SIGer 编委，SIGer 期刊的版式也是在去年火星车热点报道的三月，确立的。一年后，上周开始密集地恶补 火星，星辰大海犹如近在咫尺。我要感谢他 为我补上，柯伊伯带，太阳系八大行星，这些关键词，他仰望星空的样子和《自制望远镜》的画面简直就是完美复刻，这让我充分相信，我们的孩子们如他们的前辈一样，保有这对宇宙的好奇心，也有信心未来的某一天，他们将站在世界的中心，宇宙的中心，带领全人类走向更美好的未来。

  - 期待 SIGer 能进入星际航行快车道，为他找到更多小伙伴，共同找到一个小行星，以 “SIGer @[谭睿](https://gitee.com/tanrui2008)” 命名 :pray: 

    > [他](https://gitee.com/tanrui2008)  _喜欢雨里抓虫子、喜欢和朋友骑行露营、喜欢美食、喜欢星辰大海_ 。

# 清华大学计算机系[自然语言处理实验室](http://nlp.csai.tsinghua.edu.cn) [开源项目](http://nlp.csai.tsinghua.edu.cn/project/)

<img width="369px" src="https://images.gitee.com/uploads/images/2022/0606/155657_dd15e66a_5631341.png" title="THUNLP-AIPoet：诗歌自动生成模型及数据资源">

### <a href="http://nlp.csai.tsinghua.edu.cn/project/thunlp-aipoet%E8%AF%97%E6%AD%8C%E8%87%AA%E5%8A%A8%E7%94%9F%E6%88%90%E6%A8%A1%E5%9E%8B%E5%8F%8A%E6%95%B0%E6%8D%AE%E8%B5%84%E6%BA%90/">THUNLP-AIPoet：诗歌自动生成模型及数据资源</a>

“九歌”是清华大学自然语言处理与社会人文计算实验室（THUNLP）在负责人孙茂松教授带领下研发的中文诗歌自动生成系统。作为目前最好的中文诗歌生成系统之一，“九歌”曾于2017年登上央视一套大型科技类挑战节目《机智过人》第一季的舞台，与当代优秀青年诗人同台竞技比拼诗词创作。2017年上线至今，“九歌”已累计为用户创作超过1000万首诗词，并荣获全国计算语言学学术会议最佳系统展示奖(2017，2019...

<a class="learn-more button" href="http://nlp.csai.tsinghua.edu.cn/project/thunlp-aipoet%E8%AF%97%E6%AD%8C%E8%87%AA%E5%8A%A8%E7%94%9F%E6%88%90%E6%A8%A1%E5%9E%8B%E5%8F%8A%E6%95%B0%E6%8D%AE%E8%B5%84%E6%BA%90/">了解更多</a>


<img width="369px" src="https://images.gitee.com/uploads/images/2022/0606/155911_12c44092_5631341.png" title="THUMT：神经机器翻译工具包">

### <a href="http://nlp.csai.tsinghua.edu.cn/project/thumt/">THUMT：神经机器翻译工具包</a>

THUMT是清华大学自然语言处理与社会人文计算实验室开发的神经机器翻译工具包。目前THUMT有三种实现：THUMT-PyTorch、THUMT-TensorFlow以及THUMT-Theano。其中，THUMT-PyTorch实现了主流的Transformer[3]模型；THUMT-TensorFlow实现了Seq2Seq[1]、RNNSearch[2]和Transformer[3]模型；THUM... 

<a class="learn-more button" href="http://nlp.csai.tsinghua.edu.cn/project/thumt/">了解更多</a>

<img width="369px" src="https://images.gitee.com/uploads/images/2022/0606/160056_e373a993_5631341.png" title="OpenKE：知识图谱表示学习工具包">

### <a href="http://nlp.csai.tsinghua.edu.cn/project/openke/">OpenKE：知识图谱表示学习工具包</a>

OpenKE是THUNLP基于TensorFlow、PyTorch开发的用于将知识图谱嵌入到低维连续向量空间进行表示的开源框架。在OpenKE中，我们提供了快速且稳定的各类接口，也实现了诸多经典的知识表示学习模型。该框架易于扩展，基于框架设计新的知识表示模型也十分的方便。具体来说，OpenKE具有如下特点：接口设计简单，可以轻松在各种不同的训练环境下部署模型。底层的数据处理进行了优化，模型训练... 

<a class="learn-more button" href="http://nlp.csai.tsinghua.edu.cn/project/openke/">了解更多</a>

<img width="369px" src="https://images.gitee.com/uploads/images/2022/0606/160243_696ee57f_5631341.png" title="OpenNRE：可一键运行的开源关系抽取工具包">

### <a href="http://nlp.csai.tsinghua.edu.cn/project/opennre/">OpenNRE：可一键运行的开源关系抽取工具包</a>

关系抽取是自然语言处理当中的一项重要任务，致力于从文本中抽取出实体之间的关系。比如从句子“达芬奇绘制了蒙娜丽莎”中，我们可以抽取出（达芬奇，画家，蒙娜丽莎）这样一个关系三元组。关系抽取技术是自动构建知识图谱的重要一环。知识图谱是由真实世界中的实体和实体间复杂关系构成的结构化表示，是帮助机器理解人类知识的重要工具，在问答系统、搜索引擎、推荐系统中都有着重要的应用。一个知识图谱的简单例子 总体介绍 ... 

<a class="learn-more button" href="http://nlp.csai.tsinghua.edu.cn/project/opennre/">了解更多</a>

<img width="369px" src="https://images.gitee.com/uploads/images/2022/0606/160521_24e2699a_5631341.jpeg" title="OpenAttack：文本对抗攻击工具包">

### <a href="http://nlp.csai.tsinghua.edu.cn/project/openattack/">OpenAttack：文本对抗攻击工具包</a>

OpenAttack基于Python开发，可以用于文本对抗攻击的全过程，包括文本预处理、受害模型访问、对抗样本生成、对抗攻击评测以及对抗训练等。对抗攻击能够帮助暴露受害模型的弱点，有助于提高模型的鲁棒性和可解释性，具有重要的研究意义和应用价值。OpenAttack具有如下特点：高可用性。OpenAttack提供了一系列的易用的API，支持文本对抗攻击的各个流程。攻击类型全覆盖。OpenAtta... 

<a class="learn-more button" href="http://nlp.csai.tsinghua.edu.cn/project/openattack/">了解更多</a>

<img width="369px" src="https://images.gitee.com/uploads/images/2022/0606/160726_dbe7536c_5631341.png" title="OpenNE-Pytorch：开源网络嵌入工具包">

### <a href="http://nlp.csai.tsinghua.edu.cn/project/openne-pytorch/">OpenNE-Pytorch：开源网络嵌入工具包</a>

OpenNE-Pytorch是对网络嵌入开源工具包OpenNE的一次整体升级，本次升级将之前的工具包从TensorFlow版本全面迁移至PyTorch，而且从代码、使用、结构和效率等方面进行了全面优化，让工具包更加易于使用、定制、阅读和进一步开发，同时使运行速度和模型效果得到大幅提升。新的工具包被命名为OpenNE-Pytorch。本次升级后，OpenNE-Pytorch主要包含了三个新特性，包... 

<a class="learn-more button" href="http://nlp.csai.tsinghua.edu.cn/project/openne-pytorch/">了解更多</a>

<img width="369px" src="https://images.gitee.com/uploads/images/2022/0606/160925_b80f0b7c_5631341.png" title="OpenMatch：开放域信息检索开源工具包">

### <a href="http://nlp.csai.tsinghua.edu.cn/project/openmatch/">OpenMatch：开放域信息检索开源工具包</a>

开放域信息检索工具包OpenMatch是清华大学计算机系与微软研究院团队联合完成的成果，基于Python和PyTorch开发，它具有两大亮点：一是为用户提供了开放域下信息检索的完整解决方案，并通过模块化处理，方便用户定制自己的检索系统。二是支持领域知识的迁移学习，包括融合外部知识图谱信息的知识增强模型以及筛选大规模数据的数据增强模型。工具包地址：https://github.com/thunlp... 

<a class="learn-more button" href="http://nlp.csai.tsinghua.edu.cn/project/openmatch/">了解更多</a>

<img width="369px" src="https://images.gitee.com/uploads/images/2022/0606/160930_bc1eb675_5631341.png" title="THULAC：一个高效的中文词法分析工具包">

### <a href="http://nlp.csai.tsinghua.edu.cn/project/thulac/">THULAC：一个高效的中文词法分析工具包</a>

THULAC（THU Lexical Analyzer for Chinese）由清华大学自然语言处理与社会人文计算实验室研制推出的一套中文词法分析工具包，具有中文分词和词性标注功能。THULAC具有如下几个特点：能力强。利用我们集成的目前世界上规模最大的人工分词和词性标注中文语料库（约含5800万字）训练而成，模型标注能力强大。准确率高。该工具包在标准数据集Chinese Treebank（... 

<a class="learn-more button" href="http://nlp.csai.tsinghua.edu.cn/project/thulac/">了解更多</a>

# 星辰大海

[九歌](http://jiuge.thunlp.org/cangtou.html)（2022-6-6 16:13:39）

>  **星** 河熠熠耀奎躔  
>  **辰** 节光辉万象连  
>  **大** 乐由来无一事  
>  **海** 牛何处觅神仙

送给：喜欢雨里抓虫子、喜欢和朋友骑行露营、喜欢美食、喜欢星辰大海 的 @tanrui2008 

> 空阶虫语细  
> 落叶雨声繁  
> 欲访支公去  
> 年来学灌园

—— [九歌](http://jiuge.thunlp.org/) [绝句](http://jiuge.thunlp.org/jueju.html)

