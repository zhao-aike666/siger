- GPT-3 为下一代应用提供动力
- [GPT-3 powers the next generation of apps](https://openai.com/blog/gpt-3-apps)
- 利用GPT-3提供客户反馈分析服务的应用：Viable

<p><img width="706px" src="https://foruda.gitee.com/images/1678907052899108289/3eacb676_5631341.jpeg" title="598Viable.jpg"></p>

> 那些在使用 GPT-3 构建方面最成功的人是出色的沟通者，因为他们能够解锁提示设计的细微差别。  
> —— Abran Maldonado，Create Labs联合创始人

未来早就在我们面前了，而全新的生产力需要的是出色的沟通者（使用者）解锁它的能力（应用场景）

- 研究
  - 概述： https://openai.com/research/overview
  - 指数： https://openai.com/research
- 产品
  - 概述： https://openai.com/product
  - GPT-4： https://openai.com/product/gpt-4
  - 达尔·E 2： https://openai.com/product/dall-e-2
  - 客户案例： https://openai.com/customer-stories
  - 安全标准： https://openai.com/safety-standards
  - 定价： https://openai.com/pricing
- 安全： https://openai.com/safety-and-responsibility
- 公司：
  - 大约： https://openai.com/about
  - 职业： https://openai.com/careers
  - 博客： https://openai.com/blog
  - 宪章： https://openai.com/charter

重温一下 OPENAI 的导航，开发安全和负责任的人工智能，是独立的入口 :+1: :+1: :+1: 

# [GPT-3 为下一代应用提供动力](https://openai.com/blog/gpt-3-apps)

> 超过 300 个应用程序通过我们的 API 提供 GPT-3 驱动的搜索、对话、文本完成和其他高级 AI 功能。

自发射我们的第一个商业产品，OpenAI API，现在有 300 多个应用程序正在使用 GPT-3，全球数以万计的开发人员正在我们的平台上进行构建。我们目前平均每天生成 4 亿字，并继续扩大生产流量。

给定任何文本提示（如短语或句子），GPT-3 会以自然语言返回文本补全。开发人员可以通过仅显示几个示例或“提示”来“编程”GPT-3。我们将 API 设计为既简单易用，又足够灵活，使机器学习团队更高效。

### 应用和行业

迄今为止，已有 300 多个应用在各个类别和行业中使用 GPT-3，从生产力和教育到创造力和游戏。这些应用程序利用了一套 GPT-3 的各种功能（并帮助我们发现了新功能！其中一些包括：

![输入图片说明](https://foruda.gitee.com/images/1678907727958578651/8c67af15_5631341.png "屏幕截图")

 **Viable**  通过使用 GPT-3 以易于理解的摘要从客户反馈中提供有用的见解，从而帮助公司更好地了解他们的客户。

使用 GPT-3，Viable 从调查、帮助台票证、实时聊天日志、评论等中识别主题、情绪和情绪。然后，它会从此聚合反馈中提取见解，并在几秒钟内提供摘要。

例如，如果被问及“客户对结账体验感到沮丧的是什么？”，Viable 可能会提供以下见解：客户对结账流程感到沮丧，因为加载时间太长。他们还想要一种在结账时编辑地址并保存多种付款方式的方法。

- “GPT-3 能够从自然语言中识别主题并生成摘要，使 Viable 能够让各行各业公司的产品、客户体验和营销团队更好地了解客户的需求。”

  > — 丹尼尔·埃里克森，Viable 首席执行官

[访问可行](https://www.askviable.com/)

![输入图片说明](https://foruda.gitee.com/images/1678907797327182635/56513e41_5631341.png "屏幕截图")

> 露西在圣丹斯首映在维密欧上。

 **寓言工作室** 正在创造一种新的互动故事类型，并使用 GPT-3 来帮助推动他们的故事驱动的“虚拟生物”。

露西，尼尔·盖曼和戴夫·麦基恩的英雄城墙里的狼，被Fable改编成艾美奖获奖VR体验，由于GPT-3生成的对话，可以与人进行自然的对话。露西作为嘉宾出现在 2021 年圣丹斯电影节上，并展示了她自己的电影《德古拉》。

- “GPT-3 赋予我们赋予角色生命的能力。我们很高兴能将艺术家的视野、人工智能和情商结合起来，创造出强大的叙事，并相信有一天，每个人都会认识一个虚拟的存在。

  > —— 爱德华·萨奇，寓言工作室首席执行官

[参观寓言工作室](https://www.fable-studio.com/)

![输入图片说明](https://foruda.gitee.com/images/1678907853960312975/dd7c1fb7_5631341.png "屏幕截图")

 **Algolia**  在其 Algolia Answers 产品中使用 GPT-3 为其客户提供相关的、闪电般的语义搜索。

当OpenAI API推出时，Algolia与OpenAI合作，将GPT-3与其先进的搜索技术集成在一起，以创建新的Answers产品，以更好地理解客户的问题，并将他们连接到回答他们问题的内容的特定部分。Algolia Answers 可帮助发布商和客户支持帮助台使用自然语言进行查询，并显示重要的答案。在对 3 万篇新闻文章运行 GPT-2 测试后，Algolia 的准确率达到 1% 或更高，Algolia 准确回答复杂自然语言问题的频率是 BERT 的四倍。

我们已经看到了Algolia Answers在仅通过文本搜索难以回答的问题上的出色结果，“ABC澳大利亚产品经理Peter Buffington说。“它能够从我们的新闻档案中返回非常相关的常青内容，例如'为什么火山会喷发？'”

- “GPT-3 允许 Algolia 使用我们的 Algolia Answers 产品回答比以往任何时候都更复杂的查询，识别更深层次的上下文信息，以提高结果质量并在几秒钟内交付。”

  > — Dustin Coates，Algolia 产品和 GTM 经理

[参观阿尔戈利亚](https://www.algolia.com/)

### 平台改进

随着我们扩展访问权限，我们的团队正在不断改进平台 - 从实施内容过滤器到为开发人员提供新功能，包括我们最近推出的：

-  **答案端点** ：在使用 GPT-3 完成操作之前，搜索提供要添加到提示中的相关上下文的信息（文档、知识库等）。可用于构建客户支持机器人等应用程序，无需微调。
-  **分类终结点** ：无需微调即可利用标记的训练数据。通过搜索与输入查询最接近的示例并将其添加到提示符中，它通常与最先进的微调模型的性能相匹配，从而提供易于配置和适应的 autoML 解决方案。
-  **增强的搜索终结点** ：为答案和分类终结点提供主干，可缩放到大量文档，同时价格便宜且快速。
-  **安全** ：偏见和误用很重要，我们非常重视整个行业的问题。我们会审核所有申请，并仅批准那些以负责任的方式使用 GPT-3 的生产申请。我们要求开发人员在投入生产之前实施安全措施，例如速率限制、用户验证和测试或人机交互要求。我们还积极监测滥用的迹象以及”红队“应用程序，以查找可能的漏洞。此外，我们还开发并部署了一个内容过滤器，可将文本分类为安全、敏感或不安全。我们目前将其设置为谨慎行事，这会导致更高的误报率。
-  **[提示库](https://beta.openai.com/examples)** ：为用户可以直接在 Playground 中开始编程的数十个用例提供入门提示设计示例，例如电子表格生成器、语法校正器或机场代码提取器。

![输入图片说明](https://foruda.gitee.com/images/1678907968401103248/519b4c7b_5631341.png "屏幕截图")

> 提示用户可以直接开始编程的设计示例。

### 我们不断壮大的开发者社区

我们在全球拥有数以万计的开发人员社区，其中大多数遍布北美、欧洲、亚洲和澳大利亚。我们还发现，我们的许多开发人员往往是那些没有传统人工智能或软件工程背景的人。从我们的几位开发人员那里听到他们第一次使用 API 或编程的经验是使用 OpenAI 的界面，这令人鼓舞。

- “对于我自己和其他使命驱动的创新者来说，OpenAI 为我们提供了最终需要的工具，通过 GPT-3 在社区中做出变革性变革。通过自然语言处理，技术经验不再是障碍，我们可以真正专注于解决现实世界的问题。在我与许多首次开发人员的合作中，那些在使用 GPT-3 构建方面最成功的人是出色的沟通者，因为他们能够解锁提示设计的细微差别。

  > ——Abran Maldonado，Create Labs联合创始人

- “由于自然语言提示，与传统编码相比，使用 GPT-3 编程感觉像是一个更具创造性的过程。我相信人工智能将在未来集成到每个产品中，很高兴与来自世界各地的各种经验水平的开发人员合作，他们正在通过API创建创新应用程序。

  > — Natalie Pistunovich，Aerospike首席开发者倡导者，柏林女性技术制造商创始人

### 征集开发人员

我们认为 GPT-3 仍有许多新功能有待发现，我们希望您帮助我们发现它们！本着与我们以前的精神相似的精神研究请求和 Y 组合器的对初创公司的请求，我们希望看到我们当前和未来的开发人员突破 GPT-3 的极限，并在以下领域构建新的应用程序：

- 生产力工具
- 医疗保健和生物技术
- 气候科学与能源
- 教育技术和学习工具

我们很乐意支持黑客马拉松并为这些活动提供 API 访问，特别是如果它们包含上述领域的挑战（我们当然也对其他挑战领域持开放态度！请发送电子邮件community@openai.com提供有关事件的详细信息。我们很高兴看到我们的开发人员接下来会构建什么。

 _如果您有兴趣加入我们的应用AI团队，他们专注于将OpenAI的技术和产品推向世界，我们正在招聘!_ 

---

作者
- 开放人工智能 [查看所有文章](https://openai.com/blog?authors=openai)
- 阿什利·皮利皮辛 [查看所有文章](https://openai.com/blog?authors=ashley-pilipiszyn)
