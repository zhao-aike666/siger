- [系列交叉论坛预告 | 孙茂松教授：ChatGPT与大模型：启示及思考](http://nlp.csai.tsinghua.edu.cn/news/%E7%B3%BB%E5%88%97%E4%BA%A4%E5%8F%89%E8%AE%BA%E5%9D%9B%E9%A2%84%E5%91%8A-%E5%AD%99%E8%8C%82%E6%9D%BE%E6%95%99%E6%8E%88chatgpt%E4%B8%8E%E5%A4%A7%E6%A8%A1%E5%9E%8B%E5%90%AF%E7%A4%BA%E5%8F%8A%E6%80%9D%E8%80%83/)
- [新闻](https://mp.weixin.qq.com/s/2R38-2OpTcY_UR5w0_a4qw "清华大学人工智能国际治理研究院人工智能国际治理大讲堂第四讲成功举办") | [我组孙茂松教授在人工智能治理大讲堂上谈ChatGPT，线上观众超百万](http://nlp.csai.tsinghua.edu.cn/news/%E6%96%B0%E9%97%BB-%E6%88%91%E7%BB%84%E5%AD%99%E8%8C%82%E6%9D%BE%E6%95%99%E6%8E%88%E5%9C%A8%E4%BA%BA%E5%B7%A5%E6%99%BA%E8%83%BD%E6%B2%BB%E7%90%86%E5%A4%A7%E8%AE%B2%E5%A0%82%E4%B8%8A%E8%B0%88chatgpt%E7%BA%BF%E4%B8%8A%E8%A7%82%E4%BC%97%E8%B6%85%E7%99%BE%E4%B8%87/)
- [清华辅导员](https://mp.weixin.qq.com/s?__biz=MzA3NDQ5NzUzNA==&mid=2652542216&idx=1&sn=1d26796035f31054e0e090a0edba20da) | [刘冰奖候选人孙茂松：同伴同行投身学生科创，学科交叉培养拔尖人才](http://nlp.csai.tsinghua.edu.cn/news/%E8%BD%AC%E8%BD%BD-%E5%88%98%E5%86%B0%E5%A5%96%E5%80%99%E9%80%89%E4%BA%BA%E5%AD%99%E8%8C%82%E6%9D%BE%E5%90%8C%E4%BC%B4%E5%90%8C%E8%A1%8C%E6%8A%95%E8%BA%AB%E5%AD%A6%E7%94%9F%E7%A7%91%E5%88%9B%E5%AD%A6%E7%A7%91%E4%BA%A4%E5%8F%89%E5%9F%B9%E5%85%BB%E6%8B%94%E5%B0%96%E4%BA%BA%E6%89%8D/)
- [澎湃](https://m.thepaper.cn/newsDetail_forward_20011572) | [当诗歌邂逅算法](http://nlp.csai.tsinghua.edu.cn/news/%E5%BD%93%E8%AF%97%E6%AD%8C%E9%82%82%E9%80%85%E7%AE%97%E6%B3%95/)
- [TsinghuaNLP实验室2020年度大事记](http://nlp.csai.tsinghua.edu.cn/news/tsinghuanlp%E5%AE%9E%E9%AA%8C%E5%AE%A42020%E5%B9%B4%E5%BA%A6%E5%A4%A7%E4%BA%8B%E8%AE%B0/)

<p><img width="706px" src="https://foruda.gitee.com/images/1678937340616808420/6a9f5f06_5631341.jpeg" title="599THUNLP.jpg"></p>

> 你说“不想开学”，它答“何必读书”；[你说“谁在用琵琶弹奏一曲东风破”，它答“我来把玉笛吹开满园春意浓”](https://m.thepaper.cn/newsDetail_forward_20011572)……一来一回，趣意盎然，传统文化的魅力悄然彰显，让人不自觉沉浸其中。

这是重温 THUNLP 的最殊胜的时刻啦，刚刷屏完 ChatGPT ，GPT3.5 就成了 过去，GPT4 再次占据头条霸屏，各类焦虑担忧接踵而至，这大大的广告给做的，难道是被大数据了。无论如何，科技离不开人文，这样看来 THUNLP 已经站在时代前沿，早早布局了，这学科交叉的到位 “[自然语言处理与社会人文计算实验室](http://nlp.csai.tsinghua.edu.cn/)” 而我们的船长就是 

> “学术带头人，孙茂松，长聘教授，博士生导师，现任清华大学人工智能研究院常务副院长、清华大学计算机学位评定分委员会主席、教育部在线教育研究中心副主任、清华大学大规模在线开放教育研究中心主任。曾任清华大学计算机系主任。”

本期专题就是学习目录，因为内容太多，将会分多期分享学习笔记，先节选了 2021.1.1 以来的 THUNLP 的动态，也是因为今天晚上会有孙教授的直播，搬上小马扎准备着小本本提问。也是给自己补的作业《[用GPT大模型加速构建基于分享的兴趣引导型学习社区](https://gitee.com/yuandj/siger/issues/I6IVZU)》打个底儿。题头的5篇笔记，附在文末。 :pray:

> 本期专题的缘起就是今天晚上的直播《ChatGPT与大模型：启示及思考》，带着准备好的问题，满怀期待地（也没问上）

  1. 巨模型的基础，发展英语或者通用的应用，只能以来GPT-4 ...
  2. 我是九歌的粉丝，看了  谁在用琵琶弹奏一曲东风破，我来把玉笛吹开满园春意浓 ，来听老师讲座的。您却说，不是用的大模型。用的是海量的古诗。
  3. 我是做机器博弈AI的，现在的课题是非遗棋类的棋规的恢复研究，借您讲座的化学的应用，将棋规符号化，导入自然语言模型，来进行训练，现在我们建立了一个大的数据库。咱们 THUNLP 有哪些开源的工具可以使用？ 

  - 带着遗憾，分享给一位老师整个听下来的感受：

    > 孙教授说，大模型追赶不上啦，只能做小模型，和应用落地。同时关注大模型研究对人脑的智能涌现的现象的理解。从侧面印证了GPT-4的划时代意义，大大的 :+1: 充满整个会议室。今天文心一言也发布了，直接忽略了GPT的热词，只描述了中文语料。而孙教授对人才培养的举例，颇为发人深省，我们的老师和学生会如何去寻根究底地完成一篇学术文章的著述呢？如果能称得上您的谬赞的话，至少咱们现在的实践，正逢其时。孙教授原话：GPT-4 让我们不得不面对一些问题啦 [拥抱][拥抱][拥抱] （而背景是：前沿 :+1: 二字。）

    趁热打铁，整理了孙老师的讲稿，作为本期专题的置顶帖。

    <p><img width="26.69%" src="https://foruda.gitee.com/images/1678982398685853618/d5d8501c_5631341.jpeg" title="孙-08.jpg"> <img width="26.69%" src="https://foruda.gitee.com/images/1678982408052547707/8298824f_5631341.jpeg" title="孙-10.jpg"> <img width="14.69%" src="https://foruda.gitee.com/images/1678982390469918520/3411a0d6_5631341.jpeg" title="孙-06.jpg"> <img width="26.69%" src="https://foruda.gitee.com/images/1678982416826270784/a3a13b4d_5631341.jpeg" title="孙-11.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1678982424921728104/b17d1f56_5631341.jpeg" title="孙-12.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1678982435443331382/244bdb1a_5631341.jpeg" title="孙-13.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1678982445675450056/a9a173aa_5631341.jpeg" title="孙-14.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1678982454527456104/b19b3996_5631341.jpeg" title="孙-15.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1678982462898597149/ca0c5cce_5631341.jpeg" title="孙-16.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1678982471962136238/79aedf50_5631341.jpeg" title="孙-17.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1678982480996686414/04018492_5631341.jpeg" title="孙-18.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1678982490009841063/adefff6a_5631341.jpeg" title="孙-19.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1678982499354309293/4a353f37_5631341.jpeg" title="孙-20.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1678982508278016374/626d2cff_5631341.jpeg" title="孙-21.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1678982517318878051/e00fda40_5631341.jpeg" title="孙-22.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1678982527134013897/fbbf109c_5631341.jpeg" title="孙-23.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1678982537490262981/d0ad21fa_5631341.jpeg" title="孙-24.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1678982545780850424/e1d165ea_5631341.jpeg" title="孙-25.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1678982554485699630/2f24429e_5631341.jpeg" title="孙-26.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1678982563818964822/ba20567e_5631341.jpeg" title="孙-27.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1678982574197167109/14f3df27_5631341.jpeg" title="孙-28.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1678982584389312483/43de15f8_5631341.jpeg" title="孙-29.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1678982594272352019/f7b34fbf_5631341.jpeg" title="孙-30.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1678982603113055621/ecaed0ae_5631341.jpeg" title="孙-31.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1678982612416159856/e07be97a_5631341.jpeg" title="孙-32.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1678982620519937558/e3ff9ad8_5631341.jpeg" title="孙-33.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1678982628832040827/39f53227_5631341.jpeg" title="孙-34.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1678982637775683168/99f8e5fd_5631341.jpeg" title="孙-35.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1678982646227034102/c3bb824d_5631341.jpeg" title="孙-36.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1678982655926042159/8e9dec10_5631341.jpeg" title="孙-37.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1678982666147282345/ad37b44e_5631341.jpeg" title="孙-38.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1678982676270470388/914761d6_5631341.jpeg" title="孙-39.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1678982685520565106/686268a3_5631341.jpeg" title="孙-40.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1678982695635889647/15f6c272_5631341.jpeg" title="孙-41.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1678982704886914778/42f9522e_5631341.jpeg" title="孙-42.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1678982714168227135/7556b80c_5631341.jpeg" title="孙-43.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1678982727259135146/5e1924f6_5631341.jpeg" title="孙-44.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1678982740316940140/760a7a15_5631341.jpeg" title="孙-45.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1678982755133507780/79de6b62_5631341.jpeg" title="孙-46.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1678982765192736346/02856757_5631341.jpeg" title="孙-47.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1678982774533148032/8573c0fc_5631341.jpeg" title="孙-48.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1678982784897065007/3be9f73e_5631341.jpeg" title="孙-49.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1678982794207352831/03e214a8_5631341.jpeg" title="孙-50.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1678982802965646679/98802dc5_5631341.jpeg" title="孙-51.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1678982813182131746/40003945_5631341.jpeg" title="孙-52.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1678982823464334510/6d885192_5631341.jpeg" title="孙-53.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1678982833104492347/ff734365_5631341.jpeg" title="孙-54.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1678982842973798322/e3b7fa9d_5631341.jpeg" title="孙-55.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1678982851696910882/fabedba3_5631341.jpeg" title="孙-56.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1678982861050662904/ebcc5c58_5631341.jpeg" title="孙-57.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1678982869794259244/2fa4bf31_5631341.jpeg" title="孙-58.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1678982879215469582/a12d1204_5631341.jpeg" title="孙-59.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1678982890104565703/0be30cdd_5631341.jpeg" title="孙-60.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1678982899257517089/1823aa10_5631341.jpeg" title="孙-61.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1678982908443605193/29a281ce_5631341.jpeg" title="孙-62.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1678982918020045650/e4b2be22_5631341.jpeg" title="孙-63.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1678982927511768220/40fe1938_5631341.jpeg" title="孙-64.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1678982936230749520/e88ea359_5631341.jpeg" title="孙-65.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1678982945237773020/7085a55a_5631341.jpeg" title="孙-66.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1678982955388272699/b5200995_5631341.jpeg" title="孙-67.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1678982965996535765/666edc86_5631341.jpeg" title="孙-68.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1678982976783422194/841c2853_5631341.jpeg" title="孙-69.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1678982989875153174/6f896726_5631341.jpeg" title="孙-70.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1678982999508274554/d2405b1a_5631341.jpeg" title="孙-71.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1678983010463906274/b3d264fb_5631341.jpeg" title="孙-72.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1678983019863962149/2b15dd3a_5631341.jpeg" title="孙-73.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1678983030944386643/a6614867_5631341.jpeg" title="孙-74.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1678983040911164968/3020514e_5631341.jpeg" title="孙-75.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1678983052610337736/1aa925aa_5631341.jpeg" title="孙-76.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1678983111623925053/9376b99b_5631341.jpeg" title="孙-77.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1678983124182582961/65d2a0ee_5631341.jpeg" title="孙-78.jpg"> <img width="23.69%" src="https://foruda.gitee.com/images/1678983135481580060/71ea8ca2_5631341.jpeg" title="孙-79.jpg"></p>

# [ChatGPT 与大模型：启示及思考](http://nlp.csai.tsinghua.edu.cn/news/%E7%B3%BB%E5%88%97%E4%BA%A4%E5%8F%89%E8%AE%BA%E5%9D%9B%E9%A2%84%E5%91%8A-%E5%AD%99%E8%8C%82%E6%9D%BE%E6%95%99%E6%8E%88chatgpt%E4%B8%8E%E5%A4%A7%E6%A8%A1%E5%9E%8B%E5%90%AF%E7%A4%BA%E5%8F%8A%E6%80%9D%E8%80%83/)

—— 清华大学人工智能研究院常务副院长 孙茂松

北京信息科学与技术国家研究中心系列交叉论坛第56期 2023年3月16日

1. 一路急行：从 From Scratch 到大模型直至 ChatGPT 和 GPT-4

   - 深度学习第一重境界：白手起家 + 各家自扫门前雪
   - 深度学习第二重境界：预训练模型 + 大小联调
   - 深度学习第三重境界：预训练大模型 + 一巨托众小
   - 大道至简：语言的生成模型（GPT-3）
   - 大道的方法内涵是什么？自监督学习
     - Superior Performance on Language Understanding
   - 大模型之 “一招鲜吃遍天” ：多任务融合
     - Downstream Tasks
   - 大模型之 “一招鲜吃遍天” ：多模态融合
   - 大模型：性质很奇妙
   - 大模型：性能很奇特
     - TriviaQA
   - 大模型：效果很奇幻
     - OpenAI: ChatGPT
   - 大象无形
     - OpenAI: GPT-4
   - 大象无形
     - Yann LeCun: AI Doesn't Need Our Supervision > Meta's AI chief says self-supervised learning can build the metaverse and maybe even human-level AI
     - **Judea Pearl**  tweeted the caustic question: "What is the scientific principle by which 'Foundation models' can circumvent the theoretical limitations of data-centric methods as we know them...?"

   <p><img height="269px" src="https://foruda.gitee.com/images/1679004343686763923/ce9b9eec_5631341.png"></p>

   从山阴道上行，山川自相映发，使人应接不暇

2. 启示与思考

   - 其一、技术哲学

     - 从万物皆数到万物皆向量
     - 自然语言是人工智能的众妙之门
       - 莱布尼茨：“就一种更广的意义来说，语言的历史也就是一般的人类心灵发展的历史” “语言是人类心灵最好的镜子，而对于语词意义的一种精确分析，将会比任何其他事情都更好地使人认识理智的活动”
       - 近年来的实践表明，人类找到了一种通过对自然语言文本进行穷尽式计算，形式化地建立数字世界基本语义秩序的有效手段
       - 自然语言是实现多模态智能不可或缺的纽带或桥梁
       - 只有通过自然语言，才有可能建立通用人工智能 （OpenAI: GPT-4）
     - 莱布尼兹之梦
       - 1710年，莱布尼茨提出建立一种普遍语言的设想，“这种语言是一种用来代替自然语言的人工语言，它通过字母和符号进行逻辑分析与综合，把一般逻辑推理的规则改变为演算规则，以便更精确更敏捷地进行推理” 他将这种语言称为 alphabet of human thought，认为在这种语言中，一切理性真理都会被还原为一种演算。
       - 程序代码？

     <p><img height="269px" src="https://foruda.gitee.com/images/1679004425066400678/0a5b1391_5631341.png"> <img height="269px" src="https://foruda.gitee.com/images/1679004452649317807/aa35b37a_5631341.png"></p>

   - 其二、科学探索

     - 行到水穷处：自然语言处理下一步该怎么走？（2021/10/16 江苏徐州，16th 全国人机语音通讯学术会议）
     - 关于大模型的思考
     - 大模型的两重性：开发与探索（2021/11/14，智源）
     - 大模型的探索性：不成熟思考（规模 SCALE 复杂世界的简单法则）
     - ChatGPT 之谜（相变 Phase Transitions ？）
       - From "Zero-Shot" To "Chain Of Thoutht" : Prompt Engineering
       - Take the last letters in the words "Zeitgeist" and "AI" and concatenate them.
       - Chain-of-Thought Prompting Elicits Reasoning in Large Language Models
       - MoE _fication_ : Transformer Feed-forward Layers are Mixtures of Experts
       - The neural architecture of language: Integrative modeling converges on predictive processing

     <p><img height="269px" src="https://foruda.gitee.com/images/1679004575018865943/9f71e5a6_5631341.png"></p>

   - 其三、开发利用

     - 深入开发大模型（大模型精调）
     - 应用场景好用是硬道理（“九歌”对对子）
     - 不断拓展应用场景（反向词典 https://wantwords.net/ ）
       - feel very happy
       - 据意查句
     - 积极拓展人工智能+X （融合结构化化学语言与非结构化自然语言的大规模知识计算模型）
       - 研究背景（生医领域材料特点）    
       - 研究方法（分析人类处理问题的逻辑）
       - 研究方法（富知识化的全能处理模型）
       - 实验结果（单模态处理任务）
       - 实验结果（双模态处理任务）
       - 潜在应用（自然->化学语言）
       - 发表成果（富知识化的多功能预训练模型）
     - 大力加强中文评测基准建设
       - BIG-bench (Beyond the Limitation Game Benchmark)
       - CUGE 中文语言理解和生成评测基准

   - 其四、人才培养

     - Deep Unsupervised Learning using Nonequilibrium Thermodynamics
       - Physical Review E, January 1997.
       - Statistics and Computing, January 2001.
       - Langevin, P. Sur la theorie du mouvement brownien. CR Acad. Sci. Paris, 146(530-533), 1908.

   - 其五、科技伦理

     - UN: Towards an Ethics of Artificial Intelligence
     - nature machine intelligence: Towards ethical and socio-legal governance in AI
     - ChatGPT 可能引发的伦理和法律问题
       - 如果不加以限制，互联网上的 AIGC 内容将很可能快速超过人类文明生产的内容
       - 或遭遇五千年未有之大变化
       - 会否发生 “机币” 驱赶 “人币” ？谎言掩盖真相？
       - 对教育的影响
       - 对 copyright 保护提出挑战：从拷贝句子到拷贝句义
       - 大模型的训练过程：
         - 训练数据越大越好。读书破万卷，下笔如有神
         - 需建立资源使用用户许可机制（+隐私保护）
       - 大模型的生成过程：
         - 机器产生的文本是否有版权？两难问题：如果有，可能形成某种形式的内容垄断，发生冲突（人机，机机）；如果没有，则完全忽视了模型设计者的贡献
         - 有效防止生成有害内容，防止算法歧视
       - 应建立广泛、权威的国际对话机制，求同存异

   《竞渡诗》（唐代卢肇）

   石溪久住思端午，馆驿楼前看发机。  
   鼙鼓动时雷隐隐，兽头凌处雪微微。  
   冲破突出人齐譀，跃浪争先鸟退飞。  
   向道是龙刚不信，果然夺得锦标归。

   <p><img height="269px" src="https://foruda.gitee.com/images/1679004624956377952/42b16a16_5631341.png"></p>

   （图：古希腊神话：赫拉克勒斯）

# 2023

### 2023-3

- 2023-3-15	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/Wei_Xin_Tu_Pian__20230314172910.original.jpg"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E7%B3%BB%E5%88%97%E4%BA%A4%E5%8F%89%E8%AE%BA%E5%9D%9B%E9%A2%84%E5%91%8A-%E5%AD%99%E8%8C%82%E6%9D%BE%E6%95%99%E6%8E%88chatgpt%E4%B8%8E%E5%A4%A7%E6%A8%A1%E5%9E%8B%E5%90%AF%E7%A4%BA%E5%8F%8A%E6%80%9D%E8%80%83/" target="_blank">系列交叉论坛预告 | 孙茂松教授：ChatGPT与大模型：启示及思考</a>

    - https://meeting.tencent.com/dm/8rAbsFRtmVAX | 620 652 334 （031673）
    - https://shangzhibo.tv/watch/10954809

    ![输入图片说明](https://foruda.gitee.com/images/1678982232109948440/3c9e6c16_5631341.png)

- 2023-3-8	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/Wei_Xin_Tu_Pian__20230316101451.original.jpg"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E6%96%B0%E9%97%BB-%E6%88%91%E7%BB%84%E5%AD%99%E8%8C%82%E6%9D%BE%E6%95%99%E6%8E%88%E5%9C%A8%E4%BA%BA%E5%B7%A5%E6%99%BA%E8%83%BD%E6%B2%BB%E7%90%86%E5%A4%A7%E8%AE%B2%E5%A0%82%E4%B8%8A%E8%B0%88chatgpt%E7%BA%BF%E4%B8%8A%E8%A7%82%E4%BC%97%E8%B6%85%E7%99%BE%E4%B8%87/" target="_blank">新闻 | 我组孙茂松教授在人工智能治理大讲堂上谈ChatGPT，线上观众超百万</a>

    > 阅读原文：https://mp.weixin.qq.com/s/2R38-2OpTcY_UR5w0_a4qw?scene=25#wechat_redirect

### 2023-1

- 2023-1-19	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/Qi_Fan_Chao__KM8xtnt.original.jpg"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E6%88%91%E7%BB%84%E5%8D%9A%E5%A3%AB%E6%AF%95%E4%B8%9A%E7%94%9F%E5%B2%82%E5%87%A1%E8%B6%85%E5%85%A5%E9%80%89%E4%B8%AD%E5%85%B3%E6%9D%91u30-2022%E5%B9%B4%E5%BA%A6%E4%BC%98%E8%83%9C%E8%80%85%E6%A6%9C%E5%8D%95/" target="_blank">我组博士毕业生岂凡超入选中关村U30 2022年度优胜者榜单</a>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E6%88%91%E7%BB%84%E6%AF%95%E4%B8%9A%E7%94%9F%E5%B2%82%E5%87%A1%E8%B6%85%E8%8D%A3%E8%8E%B7%E4%B8%AD%E5%9B%BD%E4%BA%BA%E5%B7%A5%E6%99%BA%E8%83%BD%E5%AD%A6%E4%BC%9A%E4%BC%98%E7%A7%80%E5%8D%9A%E5%A3%AB%E5%AD%A6%E4%BD%8D%E8%AE%BA%E6%96%87%E5%A5%96/" target="_blank">我组毕业生岂凡超荣获中国人工智能学会优秀博士学位论文奖</a> | 2023-3-16

- 2023-1-18	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/Mian_Bi_Zhi_Neng_.original.png"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E6%88%91%E7%BB%84%E5%88%9B%E4%B8%9A%E5%85%AC%E5%8F%B8%E9%9D%A2%E5%A3%81%E6%99%BA%E8%83%BD%E8%8E%B7ai%E4%B8%AD%E5%9B%BD%E6%9C%BA%E5%99%A8%E4%B9%8B%E5%BF%83%E6%9C%80%E5%85%B7%E6%BD%9C%E5%8A%9B%E5%88%9B%E4%B8%9A%E4%BC%81%E4%B8%9Atop-10%E7%AD%89%E5%A5%96%E9%A1%B9/" target="_blank">我组毕业生创业公司面壁智能获「AI中国」机器之心“最具潜力创业企业TOP 10”等奖项</a>

- 2023-1-16	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/0_pBU9eC5.original.jpg"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/cail-2022%E8%8E%B7%E5%A5%96%E5%90%8D%E5%8D%95%E5%85%AC%E5%B8%83%E5%BF%AB%E6%9D%A5%E7%9C%8B%E7%9C%8B%E6%A6%9C%E4%B8%8A%E9%83%BD%E6%9C%89%E8%B0%81/" target="_blank">CAIL 2022获奖名单公布！快来看看榜上都有谁？</a>

- 2023-1-13	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/Feng_Mian__8dcm2FX.original.jpg"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E8%BD%AC%E8%BD%BD-%E5%88%98%E5%86%B0%E5%A5%96%E5%80%99%E9%80%89%E4%BA%BA%E5%AD%99%E8%8C%82%E6%9D%BE%E5%90%8C%E4%BC%B4%E5%90%8C%E8%A1%8C%E6%8A%95%E8%BA%AB%E5%AD%A6%E7%94%9F%E7%A7%91%E5%88%9B%E5%AD%A6%E7%A7%91%E4%BA%A4%E5%8F%89%E5%9F%B9%E5%85%BB%E6%8B%94%E5%B0%96%E4%BA%BA%E6%89%8D/" target="_blank">转载 | 刘冰奖候选人孙茂松：同伴同行投身学生科创，学科交叉培养拔尖人才</a>

# 2022

### 2022-12

- 2022-12-27	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/Zhao_Pin_Shou_Tu_.original.jpg"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E6%B8%85%E5%8D%8E%E5%A4%A7%E5%AD%A6%E8%AE%A1%E7%AE%97%E6%9C%BA%E7%B3%BBnlp%E5%AE%9E%E9%AA%8C%E5%AE%A4%E6%8B%9B%E8%81%98%E5%8D%9A%E5%A3%AB%E5%90%8E%E9%95%BF%E6%9C%9F%E6%9C%89%E6%95%88/" target="_blank">清华大学计算机系NLP实验室招聘博士后（长期有效）</a>

### 2022-10

- 2022-10-4	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/100.JPG.max-800x600.png"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E6%88%91%E7%BB%84%E6%95%99%E6%8E%88%E6%8C%87%E5%AF%BC%E7%9A%84%E7%A0%94%E7%A9%B6%E7%94%9F%E5%BB%BA%E6%88%90%E5%85%A8%E7%90%83%E9%A6%96%E4%B8%AA%E5%B8%A6%E6%A0%87%E6%B3%A8%E7%9A%84%E5%A4%9A%E6%A8%A1%E6%80%81%E8%83%A1%E7%90%B4%E6%BC%94%E5%A5%8F%E6%95%B0%E6%8D%AE%E5%BA%93/" target="_blank">我组教授指导的研究生建成全球首个带标注的多模态胡琴演奏数据库</a>

    > 近期，中央音乐学院音乐人工智能与音乐信息科技系建成全球首个带标注的多模态胡琴演奏数据库(CCOM-HuQin)，该数据库由中央音乐学院民乐系表演专业的研究生参与录制，采用统一的高精度录制标准和科学可靠的标注体系，可广泛用于音高识别、音频转谱、乐谱跟随以及音视频分析、情感计算、表演生成等研究，填补了音乐科技领域中带标注的多模态中国民族乐器演奏数据库的空白，对弘扬中国民族音乐以及中西方音乐对比研究有重...                 	

### 2022-9

- 2022-9-28	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/1_etmr7Q2.max-800x600.jpg"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E5%BD%93%E8%AF%97%E6%AD%8C%E9%82%82%E9%80%85%E7%AE%97%E6%B3%95/" target="_blank">当诗歌邂逅算法</a>

    > 你是否也有过这样的时刻：  满腔情志汹涌不知如何书写，感于古诗艰深只好望而却步…… 倘在过去，名师教诲或许难得，大家只能独自苦学；而在当下，人工智能却可以“飞入寻常百姓家”，成为诗词爱好者的良师益友。 诞生于清华大学自然语言处理与社会人文计算实验室（THUNLP）的九歌，便是这样一位“饱读诗书善点墨”的“诗词大师”。自诞生以来，九歌以其流畅自然的诗篇、丰富开放的机制收获了一众关注，也吸引着更多人走...                 	

- 2022-9-16	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/1665132228901.max-800x600.png"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E7%9B%BC%E6%98%9F%E6%98%9F%E7%9B%BC%E6%9C%88%E4%BA%AE%E9%A6%96%E6%9C%9F%E6%A8%A1%E5%9E%8Bcpm-ant%E8%AE%AD%E7%BB%83%E5%AE%8C%E6%88%90%E5%95%A6/" target="_blank">盼星星盼月亮：首期模型CPM-Ant训练完成啦！</a>

    > 2022 年 8 月 5 日，CPM-Live 直播训练迎来了它的第一个里程碑： ??&nbsp;&nbsp;首期模型 CPM-Ant 训练完成啦！?? 盼星星盼月亮，经过一个月的评测与打磨，我们很高兴能够在今天发布 CPM-Ant的全部内容。 从大模型训练、微调、推理到应用，不管你是大模型研发者还是大模型技术的爱好者，相信你都能够从 CPM-Ant 的发布内容中有所收获，快来看看吧！ ??&nbsp; 模型概览  CPM-Ant&nbsp;... &lt;THUNLP                 	

- 2022-9-15	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/Wei_Xin_Tu_Pian__20221007104028.max-800x600.jpg"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/omnievent%E4%BA%8B%E4%BB%B6%E6%8A%BD%E5%8F%96%E5%BC%80%E6%BA%90%E5%B7%A5%E5%85%B7%E5%8C%85/" target="_blank">OmniEvent：事件抽取开源工具包</a>

    > 现实世界中每天都发生着海量的事件，如何自动化地处理无结构文本并从中抽取出结构化事件知识一直是自然语言处理领域的重要挑战性任务。清华大学知识工程实验室推出OmniEvent工具包，提供了多种中英文事件抽取算法的实现以及在常用数据集上的评测，旨在为事件抽取领域提供方便快捷的实现代码和统一公平的评测，推动事件抽取领域发展。 OmniEvent开源事件抽取工具包 近年来，越来越多的事件抽取算法出现，涉及分...                 	

### 2022-8

- 2022-8-15	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/40.max-800x600.png"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E6%8B%9B%E8%81%98-%E6%B8%85%E5%8D%8E%E5%A4%A7%E5%AD%A6nlp%E5%AE%9E%E9%AA%8C%E5%AE%A4%E6%8B%9B%E8%81%98%E5%B7%A5%E7%A8%8B%E5%B8%88/" target="_blank">招聘 | 清华大学NLP实验室招聘工程师</a>

    > 清华大学计算机系自然语言处理与社会人文计算实验室成立于 20 世纪七十年代末，是国内开展自然语言处理研究最早、深具影响力的科研单位，也是中国中文信息学会计算语言学专业委员会及中国人工智能学会不确定性人工智能专业委员会的挂靠单位。实验室围绕以中文为核心的自然语言处理，在中文信息处理、机器翻译、社会计算、智慧教育和知识图谱等方面开展系统深入的研究，在国内外具有较大的学术影响。近年来，实验室承担了国家重...                 	

- 2022-8-15	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/40.max-800x600.png"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E6%8B%9B%E8%81%98-%E6%B8%85%E5%8D%8E%E5%A4%A7%E5%AD%A6nlp%E5%AE%9E%E9%AA%8C%E5%AE%A4%E6%8B%9B%E8%81%98%E4%BA%8B%E5%8A%A1%E5%8A%A9%E7%90%86/" target="_blank">招聘 | 清华大学NLP实验室招聘事务助理</a>

    > 清华大学计算机系自然语言处理与社会人文计算实验室成立于 20 世纪七十年代末，是国内开展自然语言处理研究最早、深具影响力的科研单位，也是中国中文信息学会计算语言学专业委员会及中国人工智能学会不确定性人工智能专业委员会的挂靠单位。实验室围绕以中文为核心的自然语言处理，在中文信息处理、机器翻译、社会计算、智慧教育和知识图谱等方面开展系统深入的研究，在国内外具有较大的学术影响。近年来，实验室承担了国家重...                 	

- 2022-8-15	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/40.max-800x600.png"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E6%8B%9B%E8%81%98-%E6%B8%85%E5%8D%8E%E5%A4%A7%E5%AD%A6nlp%E5%AE%9E%E9%AA%8C%E5%AE%A4%E6%8B%9B%E8%81%98%E5%8D%9A%E5%A3%AB%E5%90%8E/" target="_blank">招聘 | 清华大学NLP实验室招聘博士后</a>

    > 01 实验室简介 清华大学计算机系自然语言处理与社会人文计算实验室成立于 20 世纪七十年代末，是国内开展自然语言处理研究最早、深具影响力的科研单位，也是中国中文信息学会计算语言学专业委员会及中国人工智能学会不确定性人工智能专业委员会的挂靠单位。实验室围绕以中文为核心的自然语言处理，在中文信息处理、机器翻译、社会计算、智慧教育和知识图谱等方面开展系统深入的研究，在国内外具有较大的学术影响。近年来，...                 	

- 2022-8-10	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/30.max-800x600.png"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E5%AD%99%E8%8C%82%E6%9D%BE%E6%95%99%E6%8E%88%E6%8E%A5%E5%8F%97%E7%BB%8F%E6%B5%8E%E6%97%A5%E6%8A%A5%E9%87%87%E8%AE%BF%E8%A1%A8%E7%A4%BAai%E6%AD%A3%E5%BD%93%E5%85%B6%E6%97%B6/" target="_blank">孙茂松教授接受经济日报采访表示“AI+”正当其时</a>

    > 孙茂松教授接受经济日报采访表示“AI+”正当其时 新一代人工智能高歌猛进发展，日益成为推动生产力跃升的驱动力量。今年国家发布的《“十四五”数字经济发展规划》提出，以数字技术与实体经济深度融合为主线，不断做强做优做大我国数字经济，为构建数字中国提供有力支撑，经济日报就此展开专题报道，孙茂松教授作为特邀嘉宾对此问题分享了自己的看法。 主持人：作为人工智能专业研究者，您如何看待我国人工智能产业发展与实...                 	

### 2022-7

- 2022-7-31	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/Wei_Xin_Tu_Pian__20221021163607.max-800x600.png"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E6%96%B0%E9%97%BB-%E6%88%91%E7%BB%84%E8%AE%BA%E6%96%87%E8%A2%ABeccv-2022-oral%E5%BD%95%E7%94%A8/" target="_blank">新闻 | 我组论文被ECCV 2022 Oral录用</a>

    > 近日，我组和新加坡国立大学Sea-NExT Joint Lab合作论文“Fine-Grained Scene Graph Generation with Data Transfer”被录用为ECCV 2022 Oral。欧洲计算机视觉国际会议 (European Conference on Computer Vision, 简称ECCV)在世界范围内每两年召开一次，是计算机视觉领域三大顶级会议之一...                 	

- 2022-7-12	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/Wei_Xin_Tu_Pian__209.max-800x600.jpg"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E6%96%B0%E9%97%BB-%E6%88%91%E7%BB%84%E6%9C%AC%E7%A7%91%E7%94%9F%E5%AE%8B%E6%99%A8%E9%98%B3%E8%8D%A3%E8%8E%B7%E6%B8%85%E5%8D%8E%E5%A4%A7%E5%AD%A62022%E5%B1%8A%E7%BB%BC%E5%90%88%E8%AE%BA%E6%96%87%E8%AE%AD%E7%BB%83%E4%BC%98%E7%A7%80%E8%AE%BA%E6%96%87%E5%A5%96/" target="_blank">新闻 | 我组本科生宋晨阳荣获清华大学2022届综合论文训练优秀论文奖</a>

    > 近日 我组本科毕业生宋晨阳  在清华大学2022届综合论文训练中  成绩优秀 撰写的论文 《基于跨模态模型的甲骨文内容补全》 被评为优秀论文！ 论文中以补全甲骨文的残缺内容作为研究背景，提出了一种两阶段的跨模态模型，使其能够充分学习甲骨字图像信息、释文的上下文依赖关系和图文对齐，充分融合两模态的知识，在真实甲骨残缺字的补全任务中取得了令人满意、远超单模态模型的效果，具备辅助人工进行更高效、精准的甲...                 	

- 2022-7-11	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/Wei_Xin_Tu_Pian__20221021120852.max-800x600.png"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E6%96%B0%E9%97%BB-%E6%88%91%E7%BB%84%E5%8D%9A%E5%A3%AB%E7%94%9F%E9%9F%A9%E6%97%AD%E8%8D%A3%E8%8E%B7%E6%B8%85%E5%8D%8E%E5%A4%A7%E5%AD%A62022%E5%B9%B4%E5%BA%A6%E4%BC%98%E7%A7%80%E5%8D%9A%E5%A3%AB%E5%AD%A6%E4%BD%8D%E8%AE%BA%E6%96%87%E5%A5%96/" target="_blank">新闻 | 我组博士生韩旭荣获清华大学2022年度优秀博士学位论文奖</a>

    > 近日，我组博士生韩旭 撰写的论文 《开放域文本的结构化知识获取》 荣获清华大学2022年度优秀博士学位论文 论文以开放域文本的结构化知识获取为研究背景，针对开放域文本标注数据少、长尾数据多、新知识不断涌现、数据多源异构的特点，进行了面向远程监督的降噪学习、面向长尾知识的小样本学习、面向新增关系的持续学习、面向多源异构数据的联合学习四个方面的研究，并基于上述研究成果构建了高效的结构化知识应用系统。 ...                 	

### 2022-6

- 2022-6-27	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/24_lMHTMPJ.max-800x600.png"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E5%9C%A8%E7%BA%BF%E8%AF%BE%E7%A8%8B%E9%80%9A%E7%9F%A5-%E5%A4%A7%E6%A8%A1%E5%9E%8B%E6%8A%80%E6%9C%AF%E4%B8%8E%E4%BA%A4%E5%8F%89%E5%BA%94%E7%94%A8/" target="_blank">在线课程通知 | 大模型技术与交叉应用</a>

    > 近年来，人工智能和自然语言处理进入大模型时代，参数规模指数增长带来了显著的性能提升，为广阔的学科交叉应用提供了新的强大工具。因此，实验室为清华学生全新开设《大模型技术与交叉应用》暑期在线课程，手把手带领同学从深度学习开始，快速上手大模型的方法和实践进行前沿探索。因为本次课程通过在线授课，为了更广泛地普及大模型技术，在此也公开直播，欢迎广大同学报名观看学习。...                 	

- 2022-6-23	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/19.1.max-800x600.png"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E6%96%B0%E9%97%BB-%E6%88%91%E7%BB%84%E5%8D%9A%E5%A3%AB%E5%90%8E%E9%9F%A9%E6%97%AD%E5%85%A5%E9%80%892022%E5%B9%B4%E5%BA%A6%E5%8D%9A%E5%A3%AB%E5%90%8E%E5%88%9B%E6%96%B0%E4%BA%BA%E6%89%8D%E6%94%AF%E6%8C%81%E8%AE%A1%E5%88%92/" target="_blank">新闻 | 我组博士后韩旭入选2022年度博士后创新人才支持计划</a>

    > 近日，全国博士后管委会办公室公布了2022年度博士后创新人才支持计划名单，本次共400名博士后入选，我组博士后韩旭名列其中。 ? 学生风采 ? 韩旭，清华大学计算机系2017级博士生，导师为刘知远副教授，研究方向为自然语言处理、信息抽取、预训练语言模型。博士期间，主要工作落脚于知识指导的自然语言处理，在ACL、EMNLP、AAAI等人工智能及自然语言处理领域国际会议上发表多篇论文，出版专著《知识图...                 	

### 2022-5

- 2022-5-25	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/17.max-800x600.png"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/aacl-ijcnlp-2022-%E5%AD%A6%E7%94%9F%E7%A0%94%E8%AE%A8%E4%BC%9A%E8%AE%BA%E6%96%87%E5%BE%81%E7%A8%BF/" target="_blank">AACL-IJCNLP 2022 学生研讨会论文征稿</a>

    > 征稿链接: https://aacl2022-srw.github.io/cfp 概述： 由ACL亚太分会和亚洲自然语言处理联合会共同组织的AACL-IJCNLP 2022学生研讨会 https://aacl2022-srw.github.io/ 面向全球学生研究者征求覆盖计算语言学和自然语言处理各个方面的大量、原创和未发表的研究论文。该研讨会旨在为研究生提供一个展示自己工作的平台和收获相关领域资...                 	

- 2022-5-24	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/16_RjKGYZb.max-800x600.png"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E6%96%B0%E9%97%BB-%E6%88%91%E7%BB%84%E8%AE%BA%E6%96%87%E8%8E%B7%E5%BE%97acl-2022-best-demo-paper-award/" target="_blank">新闻 | 我组论文获得ACL 2022 Best Demo Paper Award</a>

    > 近日，我组论文获得ACL 2022 Best Demo Paper Award。该奖项是ACL系列会议对System Demonstration论文授予的最佳系统论文奖，每届会议评选出一篇获奖论文，由审稿人提名，领域主席根据系统贡献度、完成度以及影响力进行综合评价。过往的获奖工作包括Huggingface Transformers，Huggingface Datasets，GAIA等研究项目。以下...                 	

- 2022-5-22	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/15.max-800x600_yEgtw3C.jpg"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E6%96%B0%E9%97%BB-%E6%88%91%E7%BB%843%E4%BD%8D%E5%90%8C%E5%AD%A6%E9%A1%BA%E5%88%A9%E9%80%9A%E8%BF%87%E5%8D%9A%E5%A3%AB%E5%AD%A6%E4%BD%8D%E8%AE%BA%E6%96%87%E7%AD%94%E8%BE%A9/" target="_blank">新闻 | 我组3位同学顺利通过博士学位论文答辩</a>

    > 2022年5月17日下午，我组3名博士同学（岂凡超、黄轩成、韩旭）博士学位论文答辩在线上举行。答辩委员会成员包括马少平、孙茂松、周明、孙乐、李涓子、刘洋、刘康、刘知远。 线上答辩合影 答辩会上，岂凡超同学以义原知识与神经网络融合的文本语义计算为研究背景，提出了融入义原的复合词表示学习和基于义原的无监督词义消歧方法，并实现了融入义原的句子表示学习模型和对抗样本生成模型。黄轩成同学面向多源序列到序列生...                 	

- 2022-5-20	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/14_aI5Yil8.max-800x600.png"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E6%95%B0%E5%AD%97%E4%BA%BA%E6%96%87%E4%B8%93%E5%AE%B6%E9%9D%A2%E5%AF%B9%E9%9D%A2-%E7%AC%AC%E4%B8%80%E8%AE%B2%E6%B5%85%E8%B0%88%E4%BA%BA%E5%B7%A5%E6%99%BA%E8%83%BD%E8%83%8C%E6%99%AF%E4%B8%8B%E7%9A%84%E6%95%B0%E5%AD%97%E4%BA%BA%E6%96%87/" target="_blank">《数字人文》专家面对面 | 第一讲：浅谈人工智能背景下的数字人文</a>

    > 在数字化时代应运而生的数字人文，是借助计算机和数据科学等方法和手段进行的人文研究，究其性质是一门交叉学科，也是一种方法论。它将数字技术运用于人文阐释，是由媒介变革引发的知识生产范式的一次转型。 数字化浪潮无所不在，也正在改变着包括人文研究在内的学术领域。数字环境下可以发现前数字时代难以发现的现象，提出前数字时代下难以提出的设想，开展前数字时代难以开展的工作，解决前数字时代难以解决的问题。这些问题往...                 	

- 2022-5-19	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/13_nznaJII.max-800x600.png"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E6%95%B0%E5%AD%97%E8%AE%A9%E4%BA%BA%E6%96%87%E6%9B%B4%E6%96%B0-%E5%88%98%E7%9F%B3-%E5%AD%99%E8%8C%82%E6%9D%BE/" target="_blank">数字让人文更新 | 刘石 孙茂松</a>

    > 刘&nbsp; &nbsp;石 / 清华大学人文学院 孙茂松 /&nbsp;清华大学计算机科学与技术系 日前，国家印发《关于推进新时代古籍工作的意见》，共五个方面18条，内容全面，要求明确，指导性强，鼓舞人心。对于我们来说，其中的第12条：“推进古籍数字化。建立健全国家古籍数字化工作指导协调机制，统筹实施国家古籍数字化工程。积极对接国家文化大数据体系，加强古籍数据流通和协同管理，实现古籍数字化资源汇聚共...                 	

- 2022-5-16	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/11.2.max-800x600.jpg"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E6%96%B0%E9%97%BB-thunlp%E5%AE%9E%E9%AA%8C%E5%AE%A4%E6%9C%AC%E7%A7%91%E7%94%9F%E8%B5%B5%E5%A8%81%E9%9C%96%E5%85%A5%E9%80%89%E6%B8%85%E5%8D%8E%E5%A4%A7%E5%AD%A6%E6%9C%AA%E6%9D%A5%E5%AD%A6%E8%80%85/" target="_blank">新闻 | THUNLP实验室本科生赵威霖入选清华大学“未来学者”</a>

    > 近日，2022年清华大学大学生学术研究推进计划“未来学者”专项入选名单发布，本次全校共有 9 个项目通过现场答辩，我组本科生赵威霖同学入选。赵威霖，清华大学计算机系本科三年级，目前在清华大学自然语言处理实验室从事预训练语言模型、参数高效微调、模型计算加速相关研究。在ACL Demo Track 2022以共同第一作者身份发表两篇论文，相关工作OpenPrompt/Delta、BMInf在Githu...                 	

- 2022-5-10	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/9_wnuFsJK.max-800x600.png"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E6%8B%9B%E8%81%98%E6%B8%85%E5%8D%8Enlp%E5%AE%9E%E9%AA%8C%E5%AE%A4%E5%88%98%E6%B4%8B%E8%80%81%E5%B8%88%E6%8B%9B%E8%81%981-3%E5%90%8D%E5%B7%A5%E7%A8%8B%E5%B8%88/" target="_blank">招聘—清华NLP实验室刘洋老师招聘1-3名工程师</a>

    > 本实验室刘洋老师拟招聘1-3名工程师，负责自然语言处理相关算法的实现、参与相关学术工作，以及网站前端后端开发与维护等。欢迎对自然语言处理感兴趣的计算机专业毕业生加入我们。 清华大学计算机系自然语言处理与社会人文计算实验室成立于 20 世纪七十年代末，是国内开展自然语言处理研究最早、深具影响力的科研单位，也是中国中文信息学会计算语言学专业委员会及中国人工智能学会不确定性人工智能专业委员会的挂靠单位。...                 	

- 2022-5-8	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/10_ms8RDxC.max-800x600.png"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/wantwords%E6%83%B3%E5%BD%A2%E5%AE%B9%E9%82%A3%E4%B8%AA%E5%95%A5%E7%89%B9%E5%88%AB%E9%82%A3%E4%BB%80%E4%B9%88%E6%80%8E%E4%B9%88%E4%B8%AA%E8%AF%8D%E6%9D%A5%E7%9D%80%E7%82%B9%E8%BF%9B%E5%86%85%E6%96%87%E5%B0%B1%E6%9C%89%E7%AD%94%E6%A1%88/" target="_blank">WantWords：想形容那个啥特别那什么，怎么个词来着？点进内文就有答案</a>

    > “怎么形容春天的生机让人觉得很开心？” “如何形容消失在人海之中？” “如何描述夏日暴雨？” 在豆瓣文字失语者互助联盟，超过 30 万人在给自己的语言能力“举哑铃”。越来越多的人患上了这种时代病——不知如何将感受化为文字，或话到嘴边却发现词不达意、言不由衷。在厌倦了网络用语复读机似的轰炸之后，我们如何找回正常的语言表达？ 机器有答案。 输入你想要表达的意思，就能获得对应的词语，名为 WantWor...                 	

### 2022-4

- 2022-4-26	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/9_wnuFsJK.max-800x600.png"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E6%B8%85%E5%8D%8E%E5%A4%A7%E5%AD%A6nlp%E5%AE%9E%E9%AA%8C%E5%AE%A4%E6%8B%9B%E8%81%98%E6%95%B0%E6%8D%AE%E5%B7%A5%E7%A8%8B%E5%B8%88/" target="_blank">清华大学NLP实验室招聘数据工程师</a>

    > 清华大学计算机系自然语言处理与社会人文计算实验室（THUNLP）是国内开展自然语言处理研究最早、深具影响力的科研单位，也是中国中文信息学会计算语言学专业委员会及中国人工智能学会不确定性人工智能专业委员会的挂靠单位。实验室围绕以中文为核心的自然语言处理，在语言信息处理、机器翻译、社会计算、智慧教育和知识图谱等方面开展系统深入的研究，在国内外具有较大的学术影响。近年来，实验室承担了国家重点研发项目、国...                 	

- 2022-4-18	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/8.max-800x600.jpg"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E5%BE%81%E7%A8%BF%E5%90%AF%E4%BA%8B-%E7%AC%AC%E4%BA%8C%E5%8D%81%E4%B8%80%E5%B1%8A%E4%B8%AD%E5%9B%BD%E8%AE%A1%E7%AE%97%E8%AF%AD%E8%A8%80%E5%AD%A6%E5%A4%A7%E4%BC%9Accl-2022%E7%AC%AC%E4%BA%8C%E8%BD%AE%E5%BE%81%E7%A8%BF%E8%BF%9B%E8%A1%8C%E4%B8%AD/" target="_blank">征稿启事 | 第二十一届中国计算语言学大会（CCL 2022）第二轮征稿进行中</a>

    > “第二十一届中国计算语言学大会”（The Twenty-first China National Conference on Computational Linguistics, CCL 2022）将于2022年10月14-16日在江西南昌举行，会议由江西师范大学承办。中国计算语言学大会创办于1991年，由中国中文信息学会计算语言学专业委员会负责组织。经过30余年的发展，中国计算语言学大会已成为国...                 	

- 2022-4-8	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/4_hmlj0Wz.max-800x600.png"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E6%96%B0%E9%97%BB-%E6%88%91%E7%BB%845%E7%AF%87%E8%AE%BA%E6%96%87%E8%A2%ABnaacl-hlt-2022%E5%BD%95%E7%94%A8/" target="_blank">新闻 | 我组5篇论文被NAACL-HLT 2022录用</a>

    > 今日，NAACL-HLT &nbsp;2022录用结果出炉，我组5篇论文被录用，其中主会论文4篇，Findings论文1篇。以下为论文列表及介绍： Fuse It More Deeply! A Variational Transformer with Layer-Wise Latent Variable Inference for Text Generation 作者：胡锦毅，矣晓沅，李文浩，孙茂松，谢幸 ...                 	

### 2022-3

- 2022-3-26	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/3_xarQYZl.max-800x600.png"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E4%B8%8E%E7%BB%9F%E8%AE%A1%E7%9B%B8%E9%81%87%E5%BD%93%E7%BB%9F%E8%AE%A1%E5%AD%A6%E9%81%87%E4%B8%8A%E5%BC%80%E6%94%BE%E5%9F%9F%E4%B8%AD%E6%96%87%E5%88%86%E8%AF%8D/" target="_blank">与统计相遇|当统计学遇上开放域中文分词</a>

    > 近年来，自然语言处理（Natural Language Processing， 简称NLP）领域发展迅猛。在中文自然语言处理（Chinese Natural Language Processing， 简称CNLP）中，词汇之间缺少边界，故文本分词和词汇发现是中文文本分析的首要任务，也是下游分析任务（如文本分类、情感分析等）的基础。本文介绍了通过统计学框架将中文分词和词汇发现有机结合的方法--Top...                 	

- 2022-3-11	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/21_s5b1kGX.max-800x600.png"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/elle%E8%AE%A9%E9%A2%84%E8%AE%AD%E7%BB%83%E8%AF%AD%E8%A8%80%E6%A8%A1%E5%9E%8B%E6%8C%81%E7%BB%AD%E9%AB%98%E6%95%88%E5%90%B8%E6%94%B6%E6%96%B0%E9%A2%86%E5%9F%9F%E7%9F%A5%E8%AF%86-acl-2022-findings/" target="_blank">ELLE：?让预训练语言模型持续高效吸收新领域知识 | ACL 2022 Findings</a>

    > 论文动机 当前的预训练语言模型（PLM）通常使用固定的、不更新的数据进行训练，但在现实世界场景中，各种来源的数据可能会不断增长，如果我们想让PLM同时掌握旧数据和新数据中的知识，就需要让它能够持续地整合各个来源的知识。这个目标固然可以通过对所有新老数据重新大规模训练来实现，但这样的过程太过低效和昂贵。而如果我们只让模型在新数据上进行训练，它又很容易忘记旧数据中的知识，这种现象被称为灾难性遗忘。为此...                 	

- 2022-3-4	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/2_ebDnRS8.max-800x600.png"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E7%89%B9%E7%BA%A6%E4%B8%93%E6%A0%8F%E4%B8%A8%E5%AD%99%E8%8C%82%E6%9D%BE%E6%95%99%E6%8E%88%E8%87%AA%E7%84%B6%E8%AF%AD%E8%A8%80%E5%A4%84%E7%90%86%E4%B8%80%E7%9E%A5%E7%9F%A5%E5%BE%80%E9%89%B4%E4%BB%8A%E7%9E%BB%E6%9C%AA%E6%9D%A5/" target="_blank">特约专栏丨孙茂松教授——自然语言处理一瞥：知往鉴今瞻未来</a>

    > 人类语言（即自然语言）的重要性无论怎么讲都不为过。社会生物学之父爱德华·威尔逊曾说过：“语言是继真核细胞之后最伟大的进化成就”。科普畅销书《信息简史》的作者詹姆斯·格雷克也深刻地指出：“语言本身就是人类有史以来最大的技术发明”。这些断言带有科学哲学的意味，反映了现代人类对语言本质理解的不断深化。 众所周知，语言是人类所独有的，是思维的载体，是人类交流思想、表达情感最自然、最深刻、最方便的工具。其中...                 	

- 2022-3-2	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/Wei_Xin_Jie_Tu__20220225215439.original.png"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E6%96%B0%E9%97%BB-%E6%88%91%E7%BB%842%E7%AF%87%E8%AE%BA%E6%96%87%E8%A2%ABacl-2022-demo%E5%BD%95%E7%94%A8/" target="_blank">新闻 | 我组2篇论文被ACL 2022 Demo录用</a>

### 2022-2

- 2022-2-26	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/Wei_Xin_Jie_Tu__20220226134047.original.png"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E6%88%91%E7%BB%84%E8%AE%BA%E6%96%87%E5%85%A5%E9%80%89nature-communications%E4%BA%AE%E7%82%B9%E6%8E%A8%E8%8D%90%E6%96%87%E7%AB%A0/" target="_blank">我组论文入选Nature Communications亮点推荐文章</a>

- 2022-2-25	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/Wei_Xin_Jie_Tu__20220225215439.original.png"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E6%96%B0%E9%97%BB-%E6%88%91%E7%BB%8418%E7%AF%87%E8%AE%BA%E6%96%87%E8%A2%ABacl-2022%E5%BD%95%E7%94%A8/" target="_blank">新闻 | 我组18篇论文被ACL 2022录用</a>

- 2022-2-16	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/Wei_Xin_Jie_Tu__20220216123516.original.png"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E6%88%91%E7%BB%84%E9%9D%A2%E5%90%91%E7%94%9F%E5%8C%BB%E6%96%87%E7%8C%AE%E7%9A%84%E9%A2%84%E8%AE%AD%E7%BB%83%E6%A8%A1%E5%9E%8B%E6%8A%80%E6%9C%AF%E8%A2%ABnature%E5%AD%90%E5%88%8A%E5%BD%95%E7%94%A8/" target="_blank">我组面向生医文献的预训练模型技术被Nature子刊录用</a>

- 2022-2-12	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/0_1.original.jpg"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E6%B8%85%E5%8D%8E%E5%A4%A7%E5%AD%A6nlp%E5%AE%9E%E9%AA%8C%E5%AE%A4%E6%8B%9B%E8%81%98%E5%8D%9A%E5%A3%AB%E5%90%8E/" target="_blank">清华大学NLP实验室招聘博士后（长期有效）</a>

- 2022-2-8	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/0_1.original.jpg"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E6%B8%85%E5%8D%8E%E5%A4%A7%E5%AD%A6nlp%E5%AE%9E%E9%AA%8C%E5%AE%A4%E6%8B%9B%E8%81%98%E5%8D%9A%E5%A3%AB%E5%90%8E%E9%95%BF%E6%9C%9F%E6%9C%89%E6%95%88/" target="_blank">清华大学NLP实验室&amp;OpenBMB团队招聘工程师和实习生（长期有效）</a>

### 2022-1

- 2022-1-31	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/0_10Wang_.original.jpg"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E6%B8%85%E5%8D%8E%E5%A4%A7%E5%AD%A6nlp%E5%AE%9E%E9%AA%8C%E5%AE%A4%E7%A5%9D%E6%82%A8%E6%96%B0%E6%98%A5%E5%BF%AB%E4%B9%90/" target="_blank">清华大学NLP实验室祝您新春快乐！</a>

- 2022-1-31	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/0_1_3M5P4eR.original.jpg"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E7%BF%BB%E8%BD%AC%E5%AF%B9%E8%81%94%E6%82%A8%E5%87%BA%E4%B8%8B%E8%81%94ai%E5%AF%B9%E4%B8%8A%E8%81%94/" target="_blank">翻转对联：您出下联，AI对上联！</a>

- 2022-1-13	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/0_4.original.jpg"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/2021%E5%B9%B4%E6%88%91%E7%BB%84%E6%8E%88%E6%9D%83%E4%B8%93%E5%88%A9%E4%B8%80%E8%A7%88/" target="_blank">2021年我组授权专利一览</a>

- 2022-1-12	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/0_12.original.jpg"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E4%B8%AD%E5%A4%AE%E9%9F%B3%E4%B9%90%E5%AD%A6%E9%99%A2%E6%9D%8E%E5%B0%8F%E5%85%B5%E6%95%99%E6%8E%88%E4%B8%8E%E6%88%91%E7%BB%84%E5%AD%99%E8%8C%82%E6%9D%BE%E6%95%99%E6%8E%88%E4%BB%BB%E5%85%B1%E5%90%8C%E4%B8%BB%E5%B8%AD%E7%9A%84%E8%AE%A1%E7%AE%97%E8%89%BA%E6%9C%AF%E8%AE%BA%E5%9D%9B%E8%8E%B7cncc2021%E4%BC%98%E7%A7%80%E6%8A%80%E6%9C%AF%E8%AE%BA%E5%9D%9B/" target="_blank">中央音乐学院李小兵教授与我组孙茂松教授任共同主席的“计算艺术”论坛获CNCC2021“优秀技术论坛”</a>

- 2022-1-6	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/Wei_Xin_Jie_Tu__20220106152751.original.png"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/2021%E5%B9%B4ccf%E4%BC%98%E7%A7%80%E5%8D%9A%E5%A3%AB%E5%AD%A6%E4%BD%8D%E8%AE%BA%E6%96%87%E5%A5%96%E8%AF%84%E9%80%89%E7%BB%93%E6%9E%9C%E5%85%AC%E5%91%8A%E6%88%91%E7%BB%84%E7%9F%A3%E6%99%93%E6%B2%85%E5%90%8C%E5%AD%A6%E5%90%8D%E5%88%97%E5%85%B6%E4%B8%AD/" target="_blank">2021年“CCF优秀博士学位论文奖”评选结果公告，我组矣晓沅同学名列其中</a>

- 2022-1-3	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/0_4.original.jpg"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/tsinghuanlp%E5%AE%9E%E9%AA%8C%E5%AE%A42021%E5%B9%B4%E5%BA%A6%E5%A4%A7%E4%BA%8B%E8%AE%B0/" target="_blank">TsinghuaNLP实验室2021年度大事记</a>

# 2021

### 2021-12

- 2021-12-17	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/Wei_Xin_Jie_Tu__20211217130426.original.png"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E8%AE%BA%E5%9D%9B%E9%A2%84%E5%91%8A-%E8%AE%A1%E7%AE%97%E8%89%BA%E6%9C%AF%E8%AE%BA%E5%9D%9B%E6%9C%AA%E6%9D%A5%E5%88%9B%E9%80%A0%E5%8A%9B%E7%9A%84%E4%BC%97%E5%A6%99%E4%B9%8B%E9%97%A8/" target="_blank">论坛预告 | 计算艺术论坛：未来创造力的众妙之门</a>

### 2021-11

- 2021-11-29	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/Wei_Xin_Jie_Tu__20211129090937.original.png"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E7%AC%AC%E4%BA%8C%E5%8D%81%E5%B1%8A%E4%B8%AD%E5%9B%BD%E8%AE%A1%E7%AE%97%E8%AF%AD%E8%A8%80%E5%AD%A6%E5%A4%A7%E4%BC%9Accl-2021%E5%B0%86%E4%BA%8E12%E6%9C%883-5%E6%97%A5%E5%9C%A8%E7%BA%BF%E5%8F%AC%E5%BC%80/" target="_blank">第二十届中国计算语言学大会（CCL 2021）将于12月3-5日在线召开！</a>

- 2021-11-28	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/Tu_Si_Ji_-20211124-26613299.original.jpg"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/cail-2021%E8%8E%B7%E5%A5%96%E5%90%8D%E5%8D%95%E5%85%AC%E5%B8%83%E5%BF%AB%E6%9D%A5%E7%9C%8B%E7%9C%8B%E6%A6%9C%E4%B8%8A%E9%83%BD%E6%9C%89%E8%B0%81/" target="_blank">CAIL 2021获奖名单公布！快来看看榜上都有谁？</a>

- 2021-11-23	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/Tu_Si_Ji_-20211122-26568593.original.png"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E6%B8%85%E5%8D%8E%E4%BA%BA%E5%B7%A5%E6%99%BA%E8%83%BD%E5%85%B4%E8%B6%A3%E8%AE%B2%E5%BA%A7%E5%96%8A%E4%BD%A0%E6%9D%A5-%E7%AC%AC%E4%B8%80%E6%9C%9F%E7%BA%BF%E4%B8%8A%E5%88%86%E4%BA%AB%E9%A2%84%E5%91%8A/" target="_blank">清华人工智能兴趣讲座喊你来 | 第一期线上分享预告</a>

- 2021-11-19	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/Wei_Xin_Tu_Pian__20211119140930.original.jpg"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E8%AE%BA%E5%9D%9B%E9%A2%84%E5%91%8A%E4%B8%A8smp%E5%8D%81%E5%91%A8%E5%B9%B4%E7%B3%BB%E5%88%97%E8%AE%BA%E5%9D%9B%E7%AC%AC%E4%BA%8C%E6%9C%9F%E6%99%BA%E8%83%BD%E6%95%99%E8%82%B2%E8%AE%BA%E5%9D%9B%E9%87%8D%E7%A3%85%E4%B8%8A%E7%BA%BF/" target="_blank">论坛预告丨SMP十周年系列论坛第二期：智能教育论坛重磅上线</a>

- 2021-11-17	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/Wei_Xin_Tu_Pian__20211117091045.original.jpg"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E5%AD%99%E8%8C%82%E6%9D%BE%E6%95%99%E6%8E%88%E5%8F%97%E8%81%98%E6%8B%85%E4%BB%BB%E5%9B%BD%E5%AE%B6%E8%AF%AD%E5%A7%94%E7%AC%AC%E4%B8%89%E5%B1%8A%E7%A7%91%E7%A0%94%E8%A7%84%E5%88%92%E9%A2%86%E5%AF%BC%E5%B0%8F%E7%BB%84%E9%A1%BE%E9%97%AE/" target="_blank">孙茂松教授受聘担任国家语委第三届科研规划领导小组顾问</a>

- 2021-11-15	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/Deng_Deng_.original.jpg"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E5%85%A8%E7%BD%91%E9%A6%96%E4%B8%AAopenprompt%E5%B0%9D%E9%B2%9C%E6%8A%A5%E5%91%8Aprompt%E7%A0%94%E7%A9%B6%E8%80%85%E5%BF%85%E5%A4%87%E5%AE%9E%E9%AA%8C%E5%88%A9%E5%99%A8/" target="_blank">全网首个OpenPrompt尝鲜报告：Prompt研究者必备实验利器</a>

- 2021-11-12	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/Wei_Xin_Tu_Pian__20211112101740.original.jpg"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E5%80%92%E8%AE%A1%E6%97%B61%E5%A4%A9smp%E5%8D%81%E5%91%A8%E5%B9%B4%E7%B3%BB%E5%88%97%E8%AE%BA%E5%9D%9B%E7%AC%AC%E4%B8%80%E6%9C%9F%E7%A4%BE%E4%BA%A4%E6%9C%BA%E5%99%A8%E4%BA%BA%E8%AE%BA%E5%9D%9B%E9%87%8D%E7%A3%85%E4%B8%8A%E7%BA%BF/" target="_blank">倒计时1天！SMP十周年系列论坛第一期：社交机器人论坛重磅上线</a>

- 2021-11-11	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/0_3_YGqx92p.original.jpg"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/openbmb%E5%9B%A2%E9%98%9F%E8%AF%9A%E8%81%98%E8%8B%B1%E6%89%8D-%E5%8A%AA%E5%8A%9B%E5%81%9A%E6%9C%80%E5%A5%BD%E7%94%A8%E7%9A%84%E5%A4%A7%E6%A8%A1%E5%9E%8B%E7%B3%BB%E7%BB%9F/" target="_blank">OpenBMB团队诚聘英才 | 努力做最好用的大模型系统</a>

- 2021-11-9	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/0_sFaMAow.original.jpg"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E6%B8%85%E5%8D%8E%E5%A4%A7%E5%AD%A6%E5%AD%99%E8%8C%82%E6%9D%BE%E6%95%99%E6%8E%88%E5%9F%BA%E7%A1%80%E7%90%86%E8%AE%BA%E7%AA%81%E7%A0%B4%E5%92%8C%E9%A1%B6%E5%B0%96%E4%BA%BA%E6%89%8D%E5%9F%B9%E5%85%BB%E6%98%AF%E6%96%B0%E4%B8%80%E4%BB%A3%E4%BA%BA%E5%B7%A5%E6%99%BA%E8%83%BD%E5%8F%91%E5%B1%95%E7%9A%84%E9%87%8D%E4%B8%AD%E4%B9%8B%E9%87%8D/" target="_blank">清华大学孙茂松教授：基础理论突破和顶尖人才培养是新一代人工智能发展的重中之重</a>

- 2021-11-8	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/Wei_Xin_Tu_Pian__20211108084128.original.jpg"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E6%B7%B1%E5%88%87%E6%80%80%E5%BF%B5%E4%BF%9E%E5%A3%AB%E6%B1%B6%E5%85%88%E7%94%9F/" target="_blank">深切怀念俞士汶先生！</a>

### 2021-10

- 2021-10-27	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/Wei_Xin_Tu_Pian__20211027091641.original.jpg"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E6%B8%85%E5%8D%8E%E5%A4%A7%E5%AD%A6%E5%AD%99%E8%8C%82%E6%9D%BE%E5%9F%B9%E5%85%BB%E7%9C%9F%E6%AD%A3%E5%AF%B9%E5%9B%BD%E5%AE%B6%E7%A4%BE%E4%BC%9A%E5%A0%AA%E5%A4%A7%E7%94%A8%E7%9A%84%E4%BA%BA%E6%89%8D/" target="_blank">清华大学孙茂松：培养真正对国家社会堪大用的人才</a>

- 2021-10-25	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/Mo_Ren_Biao_Ti__Gong_Zhong_Hao_Feng_Mian_Shou_T.original_pntL7na.png"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E7%B4%A7%E6%80%A5%E5%85%B3%E4%BA%8Esmp2021%E5%BB%B6%E6%9C%9F%E7%9A%84%E9%80%9A%E7%9F%A5%E4%B8%8E%E4%B8%BE%E5%8A%9Esmp%E5%8D%81%E5%91%A8%E5%B9%B4%E7%B3%BB%E5%88%97%E7%BA%AA%E5%BF%B5%E6%B4%BB%E5%8A%A8%E7%9A%84%E9%A2%84%E5%91%8A/" target="_blank">紧急：关于SMP2021延期的通知与举办SMP十周年系列纪念活动的预告</a>

- 2021-10-21	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/0_1_jpDY9zc.original.jpg"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E9%A2%84%E5%91%8A-%E4%B8%96%E7%95%8C%E9%9F%B3%E4%B9%90%E4%BA%BA%E5%B7%A5%E6%99%BA%E8%83%BD%E5%B3%B0%E4%BC%9A1n%E8%AE%BA%E5%9D%9B%E7%AC%AC%E4%B8%80%E5%9C%BA/" target="_blank">预告 | 世界音乐人工智能峰会1+N论坛第一场</a>

- 2021-10-19	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/0_TDvElFJ.original.jpg"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E6%97%B6%E9%9A%942%E5%B9%B4%E7%AC%AC%E4%BA%8C%E5%B1%8Aopen-hownet%E5%AD%A6%E6%9C%AF%E7%A0%94%E8%AE%A8%E4%BC%9A%E9%87%8D%E7%A3%85%E6%9D%A5%E8%A2%AD/" target="_blank">时隔2年！第二届Open HowNet学术研讨会重磅来袭！</a>

- 2021-10-15	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/0_3_rVaedLE.original.jpg"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E6%B8%85%E5%8D%8Enlp%E5%AE%9E%E9%AA%8C%E5%AE%A4%E6%8B%9B%E8%81%98%E5%8D%9A%E5%A3%AB%E5%90%8E/" target="_blank">清华NLP实验室招聘博士后</a>

- 2021-10-12	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/0_5.original.jpg"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E7%BA%BF%E4%B8%8B%E6%B4%BB%E5%8A%A8%E4%B8%A8big-model-meetup-%E7%AC%AC1%E6%9C%9F%E5%A4%A7%E6%A8%A1%E5%9E%8Bprompt-tuning%E6%8A%80%E6%9C%AF8%E5%9C%BA%E5%AD%A6%E6%9C%AF%E6%8A%A5%E5%91%8A%E5%92%8Cposter%E5%B1%95%E7%A4%BA/" target="_blank">线下活动丨Big Model Meetup 第1期：大模型Prompt Tuning技术，8场学术报告和Poster展示</a>

- 2021-10-1	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/Wei_Xin_Jie_Tu__20210930204227.original.png"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E7%8C%AE%E7%A4%BC%E5%9B%BD%E5%BA%86-%E6%B8%85%E5%8D%8Enlp%E5%AE%9E%E9%AA%8C%E5%AE%A4%E6%8E%A8%E5%87%BAopenprompt%E5%BC%80%E6%BA%90%E5%B7%A5%E5%85%B7%E5%8C%85/" target="_blank">献礼国庆 | 清华NLP实验室推出OpenPrompt开源工具包</a>

### 2021-9

- 2021-9-28	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/Mo_Ren_Biao_Ti__Gong_Zhong_Hao_Feng_Mian_Shou_T.original.png"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E6%9C%80%E6%96%B0ai-open%E6%9C%9F%E5%88%8Aai-and-law%E4%B8%93%E6%A0%8F%E5%BE%81%E7%A8%BF%E5%90%AF%E4%BA%8B%E6%9D%A5%E4%BA%86/" target="_blank">最新！《AI Open》期刊“AI and Law”专栏征稿启事来了！</a>

- 2021-9-26	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/1112.original.jpg"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E5%8D%83%E5%85%83%E6%98%BE%E5%8D%A1%E7%8E%A9%E8%BD%AC%E7%99%BE%E4%BA%BF%E5%A4%A7%E6%A8%A1%E5%9E%8B-%E6%B8%85%E5%8D%8E%E6%8E%A8%E5%87%BA%E5%B7%A5%E5%85%B7%E5%8C%85bminf%E8%AE%A9%E6%A8%A1%E5%9E%8B%E6%8E%A8%E7%90%86%E8%BD%BB%E8%80%8C%E6%98%93%E4%B8%BE/" target="_blank">千元显卡玩转百亿大模型， 清华推出工具包BMInf让模型推理轻而易举</a>

- 2021-9-26	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/Wei_Xin_Jie_Tu__20210925174217.original.png"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E5%AE%9E%E9%AA%8C%E5%AE%A4openattack%E6%96%87%E6%9C%AC%E5%AF%B9%E6%8A%97%E5%B7%A5%E5%85%B7%E5%8C%85%E9%87%8D%E5%A4%A7%E6%9B%B4%E6%96%B0%E6%94%AF%E6%8C%81%E4%B8%AD%E6%96%87%E5%A4%9A%E8%BF%9B%E7%A8%8B%E5%85%BC%E5%AE%B9huggingface/" target="_blank">实验室OpenAttack文本对抗工具包重大更新：支持中文、多进程、兼容HuggingFace</a>

- 2021-9-21	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/0_1_kbtcSgl.original.jpg"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E5%A4%A9%E6%B6%AF%E5%85%B1%E6%AD%A4%E6%97%B6-%E6%B8%85%E5%8D%8E%E5%A4%A7%E5%AD%A6%E8%87%AA%E7%84%B6%E8%AF%AD%E8%A8%80%E5%A4%84%E7%90%86%E5%AE%9E%E9%AA%8C%E5%AE%A4%E7%A5%9D%E5%A4%A7%E5%AE%B6%E4%B8%AD%E7%A7%8B%E5%BF%AB%E4%B9%90/" target="_blank">天涯共此时 | 清华大学自然语言处理实验室祝大家中秋快乐！</a>

- 2021-9-20	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/Tu_Si_Ji_-20210915-25173468.original.jpg"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E6%96%B0%E5%AD%A6%E6%9C%9F%E6%96%B0%E5%BE%81%E7%A8%8B%E6%AC%A2%E8%BF%8Ethunlp-2021%E7%BA%A7%E6%96%B0%E5%90%8C%E5%AD%A6/" target="_blank">新学期·新征程｜欢迎THUNLP 2021级新同学！</a>

### 2021-8

- 2021-8-10	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/0_4.original.jpg"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E6%8B%9B%E8%81%98-%E6%B8%85%E5%8D%8Enlp%E5%AE%9E%E9%AA%8C%E5%AE%A4%E5%88%98%E6%B4%8B%E8%80%81%E5%B8%88%E6%8B%9B%E8%81%981-3%E5%90%8D%E5%B7%A5%E7%A8%8B%E5%B8%88/" target="_blank">招聘 | 清华NLP实验室刘洋老师招聘1-3名工程师</a>

- 2021-8-4	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/0_1_rdKU2M2.original.jpg"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E9%87%8D%E8%A6%81%E9%80%9A%E7%9F%A5-%E5%85%B3%E4%BA%8E%E5%BB%B6%E6%9C%9F%E5%8F%AC%E5%BC%80%E7%AC%AC%E4%BA%8C%E5%8D%81%E5%B1%8A%E4%B8%AD%E5%9B%BD%E8%AE%A1%E7%AE%97%E8%AF%AD%E8%A8%80%E5%AD%A6%E5%A4%A7%E4%BC%9Accl-2021%E7%9A%84%E9%80%9A%E7%9F%A5/" target="_blank">重要通知 | 关于延期召开第二十届中国计算语言学大会（CCL 2021）的通知</a>

- 2021-8-3	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/0_2.original.jpg"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E4%BF%A1%E6%81%AF%E7%98%9F%E7%96%AB%E4%B9%8B%E6%AE%87%E6%96%B0%E5%86%A0%E7%96%AB%E6%83%85%E7%9B%B8%E5%85%B3%E7%A4%BE%E4%BA%A4%E5%AA%92%E4%BD%93%E8%B0%A3%E8%A8%80%E4%BC%A0%E6%92%AD%E9%87%8F%E5%8C%96%E5%88%86%E6%9E%90/" target="_blank">“信息瘟疫”之殇：新冠疫情相关社交媒体谣言传播量化分析</a>

### 2021-7

- 2021-7-31	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/0_aLmwSaE.original.jpg"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/ccl2021%E5%AD%A6%E7%94%9F%E7%A0%94%E8%AE%A8%E4%BC%9A%E5%A6%82%E4%BD%95%E8%AF%9E%E7%94%9Fidea%E5%A6%82%E4%BD%95%E8%B7%9F%E5%AE%A1%E7%A8%BF%E4%BA%BArebuttal%E5%A6%82%E4%BD%95%E5%86%99%E8%87%AA%E5%B7%B1%E7%9A%84%E7%AC%AC%E4%B8%80%E7%AF%87%E9%A1%B6%E4%BC%9A%E6%96%87%E7%AB%A0%E7%AD%89%E6%8A%A2%E5%85%88%E7%9C%8B/" target="_blank">CCL2021学生研讨会！如何诞生Idea！如何跟审稿人Rebuttal！如何写自己的第一篇顶会文章等抢先看！</a>

- 2021-7-28	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/Dao_Chu_Tu_Pian_Tue_Jul_27_2021_17_52_53_GMT080.original.png"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E7%B2%BE%E5%BD%A9%E6%8A%A2%E5%85%88%E7%9C%8B-%E4%B8%AD%E5%9B%BD%E8%AE%A1%E7%AE%97%E8%AF%AD%E8%A8%80%E5%AD%A6%E5%A4%A7%E4%BC%9Accl-2021%E7%89%B9%E9%82%80%E6%8A%A5%E5%91%8A%E7%AF%87/" target="_blank">精彩抢先看 | 中国计算语言学大会（CCL 2021）特邀报告篇</a>

- 2021-7-27	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/ICCV_2021.original.png"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E6%96%B0%E9%97%BB-%E6%88%91%E7%BB%84%E8%AE%BA%E6%96%87%E8%A2%AB%E8%AE%A1%E7%AE%97%E6%9C%BA%E8%A7%86%E8%A7%89%E9%A1%B6%E4%BC%9Aiccv-2021%E5%BD%95%E7%94%A8/" target="_blank">新闻 | 我组论文被计算机视觉顶会ICCV 2021录用</a>

- 2021-7-26	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/Dao_Chu_Tu_Pian_Thu_Jul_29_2021_09_34_37_GMT080.original.png"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E8%B0%B7%E6%AD%8C%E5%AD%A6%E6%9C%AF2021%E5%88%8A%E7%89%A9%E6%8C%87%E6%A0%87%E5%8F%91%E5%B8%83%E6%88%91%E7%BB%849%E7%AF%87%E8%AE%BA%E6%96%87%E5%85%A5%E9%80%89%E5%90%84%E4%BC%9A%E8%AE%AEtop-100/" target="_blank">谷歌学术2021刊物指标发布：我组9篇论文入选各会议TOP 100</a>

- 2021-7-21	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/image_4.original.jpg"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E6%96%B0%E9%97%BB-%E6%88%91%E7%BB%84%E5%8D%9A%E5%A3%AB%E7%94%9F%E5%A7%9A%E8%BF%9C%E8%8D%A3%E8%8E%B72020%E8%85%BE%E8%AE%AF%E7%8A%80%E7%89%9B%E9%B8%9F%E7%B2%BE%E8%8B%B1%E4%BA%BA%E6%89%8D%E5%9F%B9%E5%85%BB%E8%AE%A1%E5%88%92%E5%B9%B4%E5%BA%A6%E5%A5%96%E5%AD%A6%E9%87%91%E4%B8%80%E7%AD%89%E5%A5%96/" target="_blank">新闻 | 我组博士生姚远荣获「2020腾讯犀牛鸟精英人才培养计划年度奖学金一等奖」</a>

### 2021-6

- 2021-6-30	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/Wei_Xin_Jie_Tu__20210630144858.original.png"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E6%96%B0%E9%97%BB-%E6%88%91%E7%BB%84%E6%9C%AC%E7%A7%91%E7%94%9F%E9%99%88%E6%9A%90%E6%B3%BD%E8%8D%A3%E8%8E%B7%E6%B8%85%E5%8D%8E%E5%A4%A7%E5%AD%A62021%E5%B1%8A%E4%BC%98%E7%A7%80%E8%AE%BA%E6%96%87%E5%A5%96/" target="_blank">新闻 | 我组本科生陈暐泽荣获清华大学2021届优秀论文奖</a>

- 2021-6-28	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/Wei_Xin_Tu_Pian__20210630144538.original.jpg"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E6%96%B0%E9%97%BB-%E6%88%91%E7%BB%84%E5%A4%9A%E5%90%8D%E6%AF%95%E4%B8%9A%E7%94%9F%E9%A1%BA%E5%88%A9%E6%AF%95%E4%B8%9A%E5%B9%B6%E8%8E%B7%E5%A4%9A%E9%A1%B9%E8%8D%A3%E8%AA%89/" target="_blank">新闻 | 我组多名毕业生顺利毕业，并获多项荣誉</a>

- 2021-6-10	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/Wei_Xin_Jie_Tu__20201230124809.original.png"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E5%AD%99%E8%8C%82%E6%9D%BE%E6%9C%BA%E5%99%A8%E8%83%BD%E5%88%9B%E9%80%A0%E5%90%97/" target="_blank">孙茂松：机器能创造吗？</a>

### 2021-5

- 2021-5-25	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/Wei_Xin_Jie_Tu__20201230124809.original.png"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/few-nerd%E4%B8%80%E4%B8%AAfew-shot%E5%9C%BA%E6%99%AF%E7%9A%84%E5%91%BD%E5%90%8D%E5%AE%9E%E4%BD%93%E8%AF%86%E5%88%AB%E6%95%B0%E6%8D%AE%E9%9B%86/" target="_blank">Few-NERD：一个Few-shot场景的命名实体识别数据集</a>

    > 近来，围绕着 "少样本命名实体识别"（few-shot NER）这一主题，出现了大量的工作和文献。“少样本命名实体识别”任务具有实际应用价值，也充满挑战性。但是目前鲜有专门针对该任务的基准数据，之前的大多数研究都是通过重新组织现有的有监督NER数据集，使其成为“少样本”场景下的数据集。这些策略通常旨在通过少量的例子来识别粗粒度的实体类型，而在实践中，大多数实体类型都是细粒度的。本文被ACL-IJC...                 	

- 2021-5-23	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/Wei_Xin_Jie_Tu__20201230124809.original.png"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E7%B1%BB%E4%B9%89%E5%8F%A5%E6%90%9C%E5%A5%87ai%E4%B9%9D%E6%AD%8C%E5%8A%A9%E6%82%A8%E5%9C%A8%E4%B8%AD%E5%9B%BD%E5%8F%A4%E8%AF%97%E8%AF%8D%E8%AF%AD%E4%B9%89%E7%A9%BA%E9%97%B4%E4%B8%AD%E7%B2%BE%E7%A1%AE%E5%AE%9A%E4%BD%8D/" target="_blank">类义句搜奇——AI“九歌”助您在中国古诗词语义空间中精确定位！</a>

- 2021-5-9	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/Wei_Xin_Jie_Tu__20201230124809.original.png"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E6%96%B0%E9%97%BB-%E6%88%91%E7%BB%8414%E7%AF%87%E8%AE%BA%E6%96%87%E8%A2%ABacl-2021%E4%B8%BB%E4%BC%9Afindings%E5%BD%95%E7%94%A8/" target="_blank">新闻 | 我组14篇论文被ACL 2021主会/Findings录用</a>

    > 近日，ACL 2021录用结果出炉，我组 14 篇论文被ACL 2021主会/Findings录用。其中， 8 篇论文被ACL 2021主会录用， 6 篇论文被Findings录用。 下面是论文列表及介绍： 一、ACL-IJCNLP 2021 Mask-Align: Self-Supervised Neural Word Alignment 作者： 陈驰、孙茂松、刘洋 类型： Long Paper...                 	

### 2021-4

- 2021-4-20	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/AIopenZhou_Jie_.original.png"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E5%9B%BE%E7%A5%9E%E7%BB%8F%E7%BD%91%E7%BB%9C%E7%BB%BC%E8%BF%B0%E6%A8%A1%E5%9E%8B%E8%AE%BE%E8%AE%A1%E7%90%86%E8%AE%BA%E5%88%86%E6%9E%90%E4%B8%8E%E5%AE%9E%E9%99%85%E5%BA%94%E7%94%A8/" target="_blank">图神经网络综述：模型设计、理论分析与实际应用</a> <img align="right" height="299px" title="ISSN: 2666-6510" src="https://foruda.gitee.com/images/1679011095427542085/8b15146d_5631341.png">

    > 图神经网络（Graph neural network，GNN）是目前非常火热的研究领域。随着领域的快速发展，越来越多的图神经网络模型与变种被提出。面对众多的模型与变体，研究者如何针对自己的任务与应用选择合适的计算单元，设计高效的图神经网络模型？如何更好利用图的不同特点，从而提升模型精度？现在的图神经网络又有哪些理论支持与模型限制？ 针对这些问题，研究团队中的周界、崔淦渠、胡声鼎等同学将2018年在...

    - https://www.keaipublishing.com/en/journals/ai-open

      > 将 2018 年在 arxiv 上公开的一篇综述《Graph neural networks: A review of methods and applications》进行了相应的更新，并从设计者的角度出发，详细介绍了图神经网络的模型设计、相关分析与具体应用，文章发表在期刊 AI Open 上。

### 2021-3

- 2021-3-12	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/Wei_Xin_Jie_Tu__20201230124809.original.png"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E6%96%B0%E9%97%BB-%E6%88%91%E7%BB%843%E7%AF%87%E8%AE%BA%E6%96%87%E8%A2%ABnaacl-hlt-2021%E5%BD%95%E7%94%A8/" target="_blank">新闻 | 我组3篇论文被NAACL-HLT 2021录用</a>

    > 近日，我组 3 篇论文被 NAACL-HLT 2021 录用，涉及的主题包括语法纠错、预训练语言模型和关系抽取。NAACL-HLT 是自然语言处理领域的国际顶级会议之一，NAACL-HLT 2021 将于 2021 年 6月 6 日至 6 月 11 日在墨西哥首都墨西哥城举办。 NAACL-HLT 2021 官网：https://2021.naacl.org/。 我组被录用的 3 篇论文分别为：...                 	

- 2021-3-5	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/Wei_Xin_Jie_Tu__20210305123027.original.png"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E6%88%91%E7%BB%84%E5%A4%9A%E7%AF%87%E8%AE%BA%E6%96%87%E5%85%A5%E9%80%89paperdigest%E7%BD%91%E7%AB%99%E6%9C%80%E6%9C%89%E5%BD%B1%E5%93%8D%E5%8A%9B%E8%AE%BA%E6%96%87%E5%88%97%E8%A1%A8/" target="_blank">我组多篇论文入选PaperDigest网站最有影响力论文列表</a>

    > 近日，著名论文摘要网站 PaperDigest 发布了各领域最有影响力的论文列表，我组共有 9 篇论文分别入选了 AAAI、ACL、IJCAI、EMNLP 最有影响力论文列表。PaperDigest 通过分析在国际重要会议上发表的所有论文，评选出每年最具影响力的 10 篇论文。 我组入选的 9 篇论文分别为： AAAI Representation Learning Of Knowledge Gr...                 	

### 2021-2

- 2021-2-21	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/4_Ob1ikT4.original.png"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E6%82%A8%E5%87%BA%E4%B8%8A%E8%81%94ai%E5%AF%B9%E4%B8%8B%E8%81%94%E4%B9%9D%E6%AD%8C%E5%AF%B9%E5%AF%B9%E5%AD%90%E5%8A%A9%E6%82%A8%E5%B9%B3%E6%B7%BB%E6%96%87%E5%8C%96%E9%9B%85%E8%B6%A3/" target="_blank">您出上联，AI对下联——“九歌”对对子助您平添文化雅趣！</a>

    > 寒假匆匆将尽，抽空到通州一游。徜徉于标志性的燃灯古塔下，忽想起乾隆爷当年出的著名上联“南通州北通州南北通州通南北”，纪晓岚答以下联“东当铺西当铺东西当铺当东西”，于是乎龙心大悦，并对纪的才学大加赞赏。一时来了雅兴，试着把这个上联送入AI“九歌”对对子（简称九歌对对子）程序，看看是否还能得到新的下联。 九歌对对子给出了一个还算OK的下联： 之后又参观了位于通州的韩美林艺术馆。见到艺术家笔走龙蛇的一幅...                 	

- 2021-2-20	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/0_d2RclQJ.original.jpg"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E6%96%B0%E9%97%BB-%E6%88%91%E7%BB%84%E4%B8%93%E8%91%97%E5%85%A5%E9%80%89springer-nature-2020-highlights%E8%AE%A1%E7%AE%97%E6%9C%BA%E9%A2%86%E5%9F%9F%E4%BB%856%E9%83%A8%E5%85%A5%E9%80%89/" target="_blank">新闻 | 我组专著入选Springer Nature 2020 Highlights，计算机领域仅6部入选</a>

    > 近日，我组2020年出版的英文专著《Representation Learning for Natural Language Processing》入选Springer Nature 2020 Highlights，计算机科学领域仅有6部入选。本书2020年7月出版至今下载量已超过26.1万次。 本书全面介绍了表示学习技术在自然语言处理领域的最新进展，对相关理论、方法和应用进行了深入介绍，并展望了...                 	

### 2021-1

- 2021-1-14	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/0.original.jpg"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/tsinghuanlp%E5%AE%9E%E9%AA%8C%E5%AE%A42020%E5%B9%B4%E5%BA%A6%E4%BA%AE%E7%82%B9%E6%88%90%E6%9E%9C%E5%9B%9E%E9%A1%BE/" target="_blank">TsinghuaNLP实验室2020年度亮点成果回顾</a>

    > 前言 刚刚过去的 2020 年是不平静的一年。贯穿全年的新型冠状病毒肺炎疫情仍未平息，各类偶发事件也层出不穷。即便如此，TsinghuaNLP实验室仍在这一年中做出了许多有价值的成果。 下面，我们将从 古典诗词机器写作研究、复杂场景的知识获取研究、高效文本对抗攻击、法律智能、机器翻译、机器学习、大规模预训练模型、网络表示学习 几大主题回顾 TsinghuaNLP 实验室 2020 年度的亮点成果。...                 	

- 2021-1-4	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/Meng_.original.png"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/%E4%B9%9D%E6%AD%8C%E4%B8%8E%E8%BF%9C%E6%96%B9/" target="_blank">九歌与远方</a>

    > 作者 | 任敏 来源 | 北京日报 诉别离，它说，“离别恨难分，琵琶不忍闻。断肠空有泪，明月已无魂。” 忆荷塘，它说，“荷叶满池塘，月色波光聚。万顷琉璃漾碧漪，秋水澄清露。” 咏春日，它说，“三月初春雪未消，东风吹送柳丝飘。绿杨枝上莺声急，红杏梢头蝶梦遥。” 它是谁？它是“熟读”诗歌90万首，通晓平仄、押韵、对仗等“潜规则”，只需输入关键词句，即可在数秒之内出口成诗的人工智能机器人，名曰九歌。 这...                 	

- 2021-1-1	

  <p><img width="326px" src="http://nlp.csai.tsinghua.edu.cn/media/images/14.original.png"></p>

  - <a href="http://nlp.csai.tsinghua.edu.cn/news/tsinghuanlp%E5%AE%9E%E9%AA%8C%E5%AE%A42020%E5%B9%B4%E5%BA%A6%E5%A4%A7%E4%BA%8B%E8%AE%B0/" target="_blank">TsinghuaNLP实验室2020年度大事记</a>

# [清华大学人工智能国际治理研究院人工智能国际治理大讲堂第四讲成功举办](https://mp.weixin.qq.com/s/2R38-2OpTcY_UR5w0_a4qw)

I-AIIG 清华大学人工智能国际治理研究院 2023-03-03 21:20 发表于北京

![输入图片说明](https://foruda.gitee.com/images/1679009821784103980/1fc3146c_5631341.png "屏幕截图")

- 微信号：清华大学人工智能国际治理研究院 I-AIIG
- 功能介绍：依托清华大学在人工智能与国际治理方面的已有积累和跨学科优势，面向人工智能国际治理重大理论问题及政策需求开展研究。

 **孙茂松**  <img align="right" width="69px" src="https://foruda.gitee.com/images/1679009866439174683/ecf37a28_5631341.png">

> 欧洲科学院外籍院士、清华大学计算机与科学技术系长聘教授、清华大学人工智能研究院常务副院长

2023年3月2日晚，由 **清华大学人工智能国际治理研究院** 主办、清华大学人工智能研究院、中国科技政策研究中心、人工智能治理研究中心共同协办的《人工智能治理大讲堂》第四期在清华大学公共管理学院报告厅顺利举行。本期大讲堂主讲人为欧洲科学院外籍院士、清华大学计算机与科学技术系长聘教授、人工智能研究院常务副院长 **孙茂松** ，采取线上线下相结合的形式，孙教授为大家带来题为“ **ChatGPT热议背后的思考：兼谈人工智能科技创新、产业发展与治理挑战** ”的精彩演讲，并与与会听众进行了热烈交流。清华大学公共管理学院教授、人工智能国际治理研究院副院长梁正主持讲座。

![输入图片说明](https://foruda.gitee.com/images/1679009984225693494/728d0ee6_5631341.png "屏幕截图")

> 梁正教授主持

![输入图片说明](https://foruda.gitee.com/images/1679009995467461739/d23973c7_5631341.png "屏幕截图")

> 孙茂松教授主讲

首先，孙教授介绍了 **ChatGPT及其背后的大模型原理** 。人工智能对话（问答）这一任务可以溯源到1950年的图灵测试，中间经过了一系列发展演变，直至发展到今天的语言生成模型（GPT-3）和语言理解模型(BERT)。孙教授从原理角度解释语言大模型带来的好处是词向量和句子向量以及词、句、篇章能够构成统一的语义空间，而 **ChatGPT则可能在大数据基础上出现了涌现（Emergence）现象** ，从而大大提高了对话与内容生成质量。

![输入图片说明](https://foruda.gitee.com/images/1679010037558364945/8457d061_5631341.png "屏幕截图")

其次，孙教授提出 **ChatGPT及大模型是人工智能科技创新和产业发展的重大机遇** 。他以清华大学自然语言处理实验室在智能写作（创作类）、反向词典、跨模态检索领域的三项代表性技术为例，结合ChatGPT与DALL-E2联合内容生成的案例，详细介绍了 **大模型及AIGC（生成式AI）的创造力** 。从科技创新和产业发展的角度来看，ChatGPT及大模型可能带来内容生产（+辅助内容生产），包括从商业应用到文化创意产业的革命性变化，可望快速扩展辐射到视频、音频、3D、游戏乃至元宇宙技术等产业应用领域，并且有潜力开拓出人类历史上从未有过的巨大创新想象空间以及智能信息处理的新境界。

最后，孙教授强调了 **ChatGPT对人工智能治理提出的前所未有的挑战** 。一是面对人工智能创造的作品可能产生的伦理问题及人类宽容度问题，二是ChatGPT可能引发的伦理和法律问题，以及对教育、知识产权保护等方面可能产生的影响。对此，他提出三点建议，一是在大模型训练过程中需要海量数据，需要建立资源使用用户许可（+隐私保护）机制；二是在大模型的内容生成上，机器产生文本的版权问题仍需深入研讨，要有效防止生成有害内容，防止算法歧视；三是在大模型特别是生成式AI治理上，应建立广泛、权威的国际对话机制，求同存异。

本次讲座吸引了清华大学相关院系师生以及来自相关高校、科研机构、企业和社会各界的听众近200人现场参与，大家就ChatGPT与通用人工智能的关系、ChatGPT对产业发展的影响、ChatGPT的伦理安全问题、新兴技术治理等问题与孙教授进行了热烈讨论，孙教授一一认真解答。讲座同时通过新浪微博、腾讯科技、搜狐科技、BRTV北京时间等平台进行了线上直播，据不完全统计，线上观众超过 **104** 万。

### 人工智能治理大讲堂往期回顾

- [人工智能治理大讲堂第一期：夏华夏](https://mp.weixin.qq.com/s?__biz=MzU4MzYxOTIwOQ==&mid=2247484795&idx=1&sn=516e12760c4063942d2e8272fbf26f0a)
- [人工智能治理大讲堂第二期：龚克](https://mp.weixin.qq.com/s?__biz=MzU4MzYxOTIwOQ==&mid=2247485903&idx=2&sn=d462350fa899fbcb333d449beaf59b08)
- [人工智能治理大讲堂第三期：克里斯托夫·吕特格](https://mp.weixin.qq.com/s?__biz=MzU4MzYxOTIwOQ==&mid=2247486031&idx=2&sn=dcd6b38b3be1d7d3a6b446ac772d81df)

### 关于我们

 **清华大学人工智能国际治理研究院** （Institute for AI International Governance, Tsinghua University，THU I-AIIG）是2020年4月由清华大学成立的校级科研机构。依托清华大学在人工智能与国际治理方面的已有积累和跨学科优势，研究院面向人工智能国际治理重大理论问题及政策需求开展研究，致力于提升清华在该领域的全球学术影响力和政策引领作用，为中国积极参与人工智能国际治理提供智力支撑。

- 新浪微博：@清华大学人工智能国际治理研究院
- 微信视频号：THU-AIIG
- Bilibili：清华大学AIIG

# [刘冰奖候选人孙茂松 | 同伴同行投身学生科创，学科交叉培养拔尖人才](https://mp.weixin.qq.com/s?__biz=MzA3NDQ5NzUzNA==&mid=2652542216&idx=1&sn=1d26796035f31054e0e090a0edba20da)

原创 辅导员工作室 清华辅导员 2023-01-09 18:00 发表于重庆

![输入图片说明](https://foruda.gitee.com/images/1679010497499576936/fca91afc_5631341.png "屏幕截图")

- 微信号：清华辅导员 thufdy
- 功能介绍：清华辅导员的欢乐与幸福之家~

> “对研究兢兢业业，对学生虚怀若谷，毫无保留地为学校拔尖创新人才的培养奉献着光与热。”

### 志存高远，带领学生做有意义的研究

“ **孙老师是计算机学者中少有的具备深厚文化底蕴的老师，他身上有中国知识分子的气息——正直、正义、有情怀。** ”孙茂松老师的学生、清华计算机系副教授刘知远这样形容孙茂松老师。作为一名计算机学者，孙茂松心系天下，以利国利民为担当。他时刻提醒学生“ **做研究不能只为论文、为自身前途，应心怀天下，做对国家对社会有意义的研究，把个人发展融入国家、社会发展的主旋律中** ”。他身体力行奋斗在科研最前线，指导学生的研究，“九歌”的研发就是一个最好的例证。

“九歌”是孙茂松实验室研发的人工智能古典诗词创作系统，取名自屈原的名篇《九歌》，蕴含着孙老师对这个系统的期冀：希望能利用人工智能技术创作出如《九歌》一般有思想、有灵魂的诗作。2017年底，“九歌”系统登陆央视一套大型科学挑战类节目“机智过人”，与三位诗人同台比拼，“九歌”的表现与诗人难分伯仲，这一期节目的网上浏览次数约千万次。此后，“九歌”系统积极开展了推广应用，如曾在清华大学校庆期间，为同学们创作属于基于姓名的藏头诗；与腾讯合作，综合使用“九歌”系统和腾讯图像识别技术，联合《中国国家地理》共同开展了2018年中秋节“月已满·诗故乡”看图作诗活动；并参展了2017年首届中国北京国际语言文化博览会、2019年国际人工智能与教育大会“清华大学人工智能与教育展”等多个重要活动， **为传承与弘扬中国优秀传统文化、推动人工智能技术的研究与落地做出了自己的贡献** 。

![输入图片说明](https://foruda.gitee.com/images/1679010599376681376/cd1fc232_5631341.png "屏幕截图")

> 孙茂松实验室研发的“九歌”计算机诗词创作系统

“九歌”的面世，是研发团队师生攻坚克难的结果。项目开发初期，时间紧、任务重，其中碰到的最大困难，莫过于系统研发和论文发表之间的矛盾。实验室里的其他同学在一些国际通行的问题和数据集上做研究，很快发表论文、获得奖学金。“九歌”团队的学生顶着毕业压力，花费大量时间精力研发系统，却无法见到迅速的成果产出，一时得不到他人的肯定。“ **诗是写给人读的，一定要拿出系统来。只要是好东西，就一定不会被埋没的** ”，孙茂松老师始终坚持着内心的想法，认为性能过硬的实际系统是检验人工智能方法创新的试金石：没有方法创新，系统的性能不可能令人满意；反过来，只在Benchmark数据集上测试方法的有效性存在片面性。对“九歌”而言，两者的结合是必由之路，只有这样，才能真正培养出学生的创新思维和创新能力，发挥并实现AI技术的价值。在孙茂松老师和团队的坚持与不懈努力下，“九歌”系统一步一个脚印地走到了今天，在学术界以及社会大众中产生了较为广泛的影响力。“九歌”研发团队陆续在ACL、EMNLP等人工智能和自然语言处理顶级国际学术会议上发表了一批高水平论文，“九歌”系统应分布在全球各地的广大用户要求，已创作了约3500万首诗词，团队核心成员矣晓沅同学荣获了2021年中国计算机学会优秀博士学位论文奖。

### 亦师亦友，与学生携手同行

在研发“九歌”系统时，学生的疲惫与焦虑在所难免，孙茂松老师看在眼里，疼在心里。他能做的就是尽全力与大家一起共度难关。有一次他从国外出差回来，坐了十几个小时的飞机，但却顾不上吃饭，揣着几袋小零食就风尘仆仆地直奔会议室参加“九歌”团队的组会，了解学生最新进展与研究瓶颈，与大家一起细致探讨，共同解决问题。每周两次，每次四小时，这样的讨论一坚持就是大半年。他的学生，2015年本科生特等奖学金获得者矣晓沅回忆说，“2015年的冬天，那时作为本科毕设内容，我做出了‘九歌’作诗系统的一个早期雏形。12月31号，跨年夜11点左右，忽然收到了孙老师的邮件，说自己在测试时发现了demo系统的很多问题，并在邮件里一一详细列出问题及可能的原因。看到老师这么认真，我也顾不上休息，赶快修复相关bug并回复邮件汇报。零点的钟声就在邮件一来一去中悄然度过了。当时并不觉得累，只是感觉这是自己人生中很有意义的、难忘的一次跨年。”

他的另一个学生陈慧敏说： **孙老师对于我们而言不仅是良师，是益友，更像是一位父亲，一位“倔强”的“慈父”** 。他的“倔强”来自于他对于学术的追求：不是别人做了我们就也要跟着做，不是别人没做我们就不能做，要做对国家社会有意义的研究，哪怕再难也要啃下来；他的“慈父”来自于他对学生的宽容与接纳，孙老师平常生活中是一位可爱的长者，有修养有气度，对学生从不严厉呵责，会以近乎玩笑的语气表达他的建议与想法，学生们与他相处“很舒服”。

![输入图片说明](https://foruda.gitee.com/images/1679010661271857337/74187ec5_5631341.png "屏幕截图")

> 孙茂松和他的学生合影

### 尽职尽责，热心学校创新人才培养

在指导自己课题组同学的同时， **孙茂松老师也时刻关心学校创新人才的培养，积极投身到校内本科生科创工作当中** ，并承担了“星火计划”十四期名誉班主任、“清华大学国强研究院杯”首届建筑机器人技术创新赛专家委员会成员等职责。

在担任“星火计划”十四期名誉班主任期间，孙茂松老师积极参与学生学术活动，在清华大学科创日、全国青少年高校科学营、星火计划年会等活动中进行学术分享和寄语，鼓励同学们在探索学术问题的过程中要敢为天下先，勇于开拓，形成学术思维的碰撞；希望同学们立志做科技之大者，致广大而尽精微，解决有意义、有难度的真实问题。在孙茂松老师的勉励下，星火十四期学员产生了2名本科生特等奖学金获得者，产出了一大批期刊论文和会议论文以及全国挑战杯特等奖、一等奖、三等奖作品各1项。目前他继续担任“星火计划”十六期指导教师。此外，他还担任清华大学致理书院导师、清华大学计算机系“计算机-金融班”指导教师，在不同类型的本科生培养和成长过程中进行积极引导。

![输入图片说明](https://foruda.gitee.com/images/1679010697008325166/7fe5b966_5631341.png "屏幕截图")

> 孙茂松老师参加星火计划活动

![输入图片说明](https://foruda.gitee.com/images/1679010706352209842/dd415ccd_5631341.png "屏幕截图")

> 星火计划学员代表在星火计划年会上向孙茂松老师献花

在积极投身校内本科生科创工作的同时， **孙老师也始终重视第一课堂的教学工作，为同学们夯实理论基础** 。近年来，随着交叉学科、新兴学科的发展，孙老师针对跨学科创新人才的培养，全新设计、开设了面向全校学科交叉的新生研讨课《人文与社会科学计算导论》，还参与开设、讲授人文学院的新课程《数字人文导论》、全校研究生素质课程等。

可喜的是，孙老师在同学们心中播下的学科交叉的种子已经开始破土发芽。如电子系2018级的本科生王芃翰同学，正是在选修了孙老师的《人文与社会科学计算导论》后，对深度学习和自然语言处理产生了非常浓厚的兴趣，随后与计算机系和社科学院经济学的两位同学联合申请立项了一个自然语言处理与经济学交叉的SRT项目，并由孙老师手把手亲自指导。该项目利用全网的经济学相关文本，基于大数据预训练模型进行股情预测，目前已经取得非常积极的进展。

“落红不是无情物，化作春泥更护花”。孙茂松老师是一位 **有人文情怀、有责任担当的计算机学者** ，对研究兢兢业业，对学生虚怀若谷，毫无保留地为学校拔尖创新人才的培养奉献着光与热。

>  **孙茂松** ，计算机系长聘教授，欧洲科学院外籍院士。现任清华大学人工智能研究院常务副院长、教育部在线教育研究中心副主任、清华大学大规模在线开放教育研究中心主任，曾任计算机系主任。主要研究领域为人工智能、社会与人文计算及计算教育学。国家973项目首席科学家、国家社科重大项目首席专家。在国际一流学术会议和重要期刊上发表论文逾200篇，Google Scholar论文引用逾2.8万次。带领团队研制发布了全球第一个中文慕课平台“学堂在线”，为发展我国在线教育事业做出了贡献。曾获“全国优秀科技工作者”、“首都市民学习之星”、“清华大学良师益友”等荣誉。

>  **“刘冰奖”** 于2011年刘冰同志九十岁诞辰之际设立，旨在奖励在学生思想政治工作一线取得突出业绩、即将投身科研或管理工作的教师身份辅导员，以及重视师生互动，在指导和参与学生活动中发挥重要作用的一线教师；每年表彰10人左右。

- 文编丨姜丰
- 美编丨张亦宁

# [当诗歌邂逅算法](https://m.thepaper.cn/newsDetail_forward_20011572)

澎湃新闻特约撰稿 徐清扬 沈星月 翁之颢 2022-09-22 16:49 来源：澎湃新闻

你是否也有过这样的时刻：

满腔情志汹涌不知如何书写，感于古诗艰深只好望而却步……

倘在过去，名师教诲或许难得，大家只能独自苦学；而在当下，人工智能却可以“飞入寻常百姓家”，成为诗词爱好者的良师益友。

诞生于清华大学自然语言处理与社会人文计算实验室（THUNLP）的九歌，便是这样一位“饱读诗书善点墨”的“诗词大师”。自诞生以来，九歌以其流畅自然的诗篇、丰富开放的机制收获了一众关注，也吸引着更多人走近诗歌、走近传统文化。

### 广纳苦读成好诗

提及人工智能写诗，多数了解甚微的人都会持怀疑态度：人工智能可以写诗？

人工智能的语言智能研究起步不过数年，而诗歌是人类语言高度凝练、高度艺术化的体现，古诗更是中华传统文化的璀璨明珠。人工智能虽然具有杰出的储存与计算能力，但在如何写出一首好诗上，还是得参考人的经验。

清代孙洙在《唐诗三百首》序言中曾言：“熟读唐诗三百首,不会作诗也会吟。”九歌系统录入了从魏晋南北朝到近现代所能找到的90万首诗歌，通过对这些诗歌的反复学习，与标注有诗歌情感、韵律、质量数据库的支撑，九歌团队设计算法，建立出诗歌自动生成模型。

诗歌创作不仅需要满足格律的要求，篇章连贯、新颖个性才能成为佳品。这也是九歌团队不懈探索的目标。以往团队大都从数学视角出发，将诗歌生成理解为序列预测问题，九歌并没有止步于此。正如研发者矣晓沅所言，“诗歌是一种高度文学化、艺术化的文本”，这一洞见引领九歌从学科交叉中获取启发——针对诗歌创作的特点和难题，设计出专门的模型结构优化写作。

《文心雕龙》有载：“夫裁文匠笔，篇有小大；离章合句，调有缓急；随变适会，莫见定准……故能外文绮交，内义脉注，跗萼相衔，首尾一体。”矣晓沅解释道，“在写作中，要动态地、灵活地构建出整首诗的骨架主线，以此对上下文的内容和主题进行约束，做到上下紧密相关，意脉连贯。同时又要断续离合、荡开笔墨，允许一定的自由与发挥的空间，不能约束得太死板。”

基于此，研发团队对《文心雕龙》中的“意脉”进行数学建模，设计出了显著性线索机制模型，主题突出的同时使诗歌全篇更为连贯；参考认知心理学中的工作记忆原理，团队提出了基于工作记忆模型的诗歌生成方法，增强与用户输入的关键词的扣题性，实现语句间的连贯表达、合理过渡。

许多人认为，AI写诗最致命的弱点就是缺乏情感。为了使AI更富“人”情，九歌将每一句的情感和内容分别构成序列，不仅能够控制全诗的情感，还可以比较细致地预测每一句的情感，使诗歌内具有起承转合的情感流动。用户可以在“藏头诗”模块选择“喜悦”“较喜悦”至“悲伤”的五种情感基调。此外，背靠模型，九歌可以自动划分风格并实现对诗歌的风格控制，如“萧瑟凄凉”“忆旧感喟”等，模仿人类类型化的风格表达。

![输入图片说明](https://foruda.gitee.com/images/1679011743169198081/a8eb2f22_5631341.jpeg "195.jpg")

> 清华大学自然语言处理与社会人文计算实验室（THUNLP）成员合影。

### 亦师亦友伴诗途

你说“不想开学”，它答“何必读书”；你说“谁在用琵琶弹奏一曲东风破”，它答“我来把玉笛吹开满园春意浓”……一来一回，趣意盎然，传统文化的魅力悄然彰显，让人不自觉沉浸其中。

![输入图片说明](https://foruda.gitee.com/images/1679011777082203095/656afe04_5631341.png "屏幕截图")

> 九歌对对子。

这是九歌于2021年春节期间推出的一项新功能——“九歌对对子”，在目前的2.0版本中，用户还可以“翻转对联”，为下联寻找上联，新颖有趣，不少网友玩得不亦乐乎。而这也正是九歌团队一直在为之努力的一大目标，打破人们与传统文学间的无形之壁，让更多人走近传统文化。为了这一目标，九歌团队持续多方位的探索——

通过文本大数据、评分统计，研发者在Github（GitHub是一个面向开源及私有软件项目的托管平台，因为只支持Git作为唯一的版本库格式进行托管,故名GitHub ）上展列了最受欢迎的300首唐诗，它们既反映了唐诗在现代文化传播中的传承与使用度，又为诗歌初学者提供一个实用、新颖的切入点。

在中国人民大学附属中学、清华大学附属中学、澳门培正中学及中国科技馆、全国中学生奥林匹克语言学竞赛上，九歌举办面向广大中学生的科普学术讲座，开展科学普及活动。

![输入图片说明](https://foruda.gitee.com/images/1679011807602906196/559f2652_5631341.png "屏幕截图")

> 九歌参加清华大学人工智能成就展。

在央视的《机智过人》、与腾讯合作的中秋看图作诗活动以及多项人工智能展中，九歌也均有惊喜的亮相。“作为AI和诗词的结合，我们希望九歌能吸引到原本对诗词不感兴趣的人，也许他们在了解后能对诗词有更深入的了解。”矣晓沅说道。

在吸引更多人走近传统文化之外，九歌另一项重要的意义在于担任诗歌初学者的智能“助教”。

依赖于九歌的评分与交互体系，系统可以对用户生成诗歌的连贯性等方面作出评价。如果不是非常满意，用户可以与AI一起进行局部至整体的修改，如将“秋月满江城，天风昨夜生。寒蛰啼露草，黄雪万家声。”一诗中“天”改为“寒”。在修改后，系统将会对诗歌再次评分，从而体会推敲琢磨的妙处。用户还可以选择与生成的诗歌语义相近的古人诗作，在与古人诗作的比对中，更好地掌握平仄规律，领悟诗歌创作的真味。

![输入图片说明](https://foruda.gitee.com/images/1679011823767451278/8fdc43d2_5631341.png "屏幕截图")

> 九歌所作的诗歌。

### 诗长路漫且畅言

2019年，九歌在Github上开源了多个模型与数据集。谈到为什么做出开源的决定，矣晓沅说：“2015年底开始做九歌时，国内古诗生成领域几乎是一片空白，经过大量的摸索才有了今天的成果。一个团队的力量很微小，把成果分享出来也是希望对诗歌生成感兴趣的研究者可以少走弯路，开发出更先进的AI系统，吸引更多的人关注优秀的中华传统文化。”

如今，九歌的探索仍在继续。清华大学计算机系教授、THUNLP实验室指导老师孙茂松教授对九歌的未来充满期待：“我们会设计更加智能的新算法，使九歌可以做的诗更贴近情境，并能够有意识地运用典故。同时，通过改进评分系统、押韵平仄规律，我们希望九歌可以给人们写古诗、理解古诗提供更大的帮助。我们也会加入如古诗词接龙等益智游戏，创造古诗词的应用环境，以激发大家学习古诗词、学习古代文化的兴趣。”从矣晓沅到刚刚加入实验室的清华特等奖学金得主白钰卓，新鲜优秀血液的不断注入，使九歌持续迸发着新的活力。

![输入图片说明](https://foruda.gitee.com/images/1679011858631137658/d898d951_5631341.png "屏幕截图")

> 反向词典（ https://wantwords.net ）

古诗词之外，THUNLP实验室还开拓着辅助大众书面表达的多种可能。生活中，我们常常会遇见有想说的话，却不知如何表述的问题，“反向词典”正是基于此而诞生。与传统词典输入词汇、查询含义的搜索过程相反，输入内容大意，“反向词典”就能输出相关词汇，为词不达意者提供更丰富准确的选择。

此外，实验室还推出了另一个检索神器——“据意查句”。当你心情烦闷在朋友圈吐槽“太烦了”，它会告诉你“心不怡之长久兮，忧与愁其相接”；当炎炎夏日你只会反复念叨着“太热了”，它会告诉你“火伞高张，炎威如炽”；当你思念一个人说“好想你”，它会告诉你“从别后，忆相逢，几回魂梦与君同”……

![输入图片说明](https://foruda.gitee.com/images/1679011877359802414/30714c9b_5631341.png "屏幕截图")

> 反向词典（ https://wantwords.net ）

只要将你想说的内容输入“据意查句”，便可收获名人学者关于相似含义更优美蕴藉的表达，包含古诗文、歇后语、熟语、影视台词等诸多类别。在每个句子下面，“据意查句”还提供了一键复制、点赞、翻译、反馈等选项，在提升用户体验感的同时，也帮助团队更好地提升程序。

无论何时，人们始终追求更优美、更深远的表达。AI从人类的诗歌中学习创作，在发展到一定程度后又可以给人类带来启发，让人类创作出更好的诗歌，这些诗歌再进一步促进AI的升级，形成良性循环。在科技的助力下，人文、语言、传统，以一种新兴的姿态创造新的惊喜。

- 责任编辑：钟煜豪
- 图片编辑：朱伟辉
- 校对：张亮亮

### 评论

- 万木雨林 2022-09-22

  > 可以精算到精品但绝无绝品，一位绝世诗圣创作不是学习也不是囊括，不是覆辙更不是，是在时空中对天地人瞬间的感悟，运用汉字深邃的内涵和张力，诗者可以突破文字和文法的藩篱，精准地将情感融入属于这特定的时刻和场景的文字中，这是机器和人工智能达不到的境界。

- 离人 2022-09-23

  > 这种“对联机器”应该早有雏形了。 大概在2002/2003年时天涯的对联雅座、诗词比兴很火爆，那会QQ还有自建聊天室，一些BBS论坛，都在玩临屏。偶尔会使用“对联机器”做做提示，拓展拓展思路，但并不能完全依仗这个工具，因为有时工具会脱离上下联的功能、意境生造一个“词”出来。 随着技术发展，如今这类工具的考虑面向应该更丰富精细，除了基本的工整外，应该会在技巧、用词和意境上有突破。

# [TsinghuaNLP实验室2020年度大事记](http://nlp.csai.tsinghua.edu.cn/news/tsinghuanlp%E5%AE%9E%E9%AA%8C%E5%AE%A42020%E5%B9%B4%E5%BA%A6%E5%A4%A7%E4%BA%8B%E8%AE%B0/)

2021-01-01

<p align="center"><img src="https://foruda.gitee.com/images/1679014054178722317/a1c7be41_5631341.png"><img src="https://foruda.gitee.com/images/1679014063728602336/4388d148_5631341.png"><img src="https://foruda.gitee.com/images/1679014071338990408/7fd77e67_5631341.png"><img src="https://foruda.gitee.com/images/1679014078085816800/660253b2_5631341.png"><img src="https://foruda.gitee.com/images/1679014084210773475/c3215f00_5631341.png"><img src="https://foruda.gitee.com/images/1679014091447365211/1349b947_5631341.png"><img src="https://foruda.gitee.com/images/1679014097600325708/b21b3c8f_5631341.png"><img src="https://foruda.gitee.com/images/1679014105375276058/938f57ef_5631341.png"><img src="https://foruda.gitee.com/images/1679014113210776065/db97a45a_5631341.png"><img src="https://foruda.gitee.com/images/1679014121517658057/d7ad40e0_5631341.png"><img src="https://foruda.gitee.com/images/1679014128187958647/8d2d873e_5631341.png"><img src="https://foruda.gitee.com/images/1679014134949776701/f5dffd07_5631341.png"><img src="https://foruda.gitee.com/images/1679014141515367103/445a5f21_5631341.png"><img src="https://foruda.gitee.com/images/1679014148552278062/1b763acb_5631341.png"></p>
<p align="center"><img src="https://foruda.gitee.com/images/1679014158065556031/aeeded12_5631341.png"></p>

### 9个读书计划：

<p><img width="33%" src="https://foruda.gitee.com/images/1679077708986058257/31adbf86_5631341.jpeg" title="601莱布尼茨之梦副本.jpg"> <img width="33%" src="https://foruda.gitee.com/images/1679078729912622381/cfe1a061_5631341.jpeg" title="602规模相变副本.jpg"> <img width="33%" src="https://foruda.gitee.com/images/1679078740375284934/d42dc20f_5631341.jpeg" title="603AIOPEN副本.jpg"> <img width="33%" src="https://foruda.gitee.com/images/1679078749746747339/bf226bf3_5631341.jpeg" title="604大数据智能副本.jpg"> <img width="33%" src="https://foruda.gitee.com/images/1679078758970591729/ec2a1bd2_5631341.jpeg" title="605数字人文副本.jpg"> <img width="33%" src="https://foruda.gitee.com/images/1679078768492503570/73f78658_5631341.jpeg" title="606Introduction to graph neural networks副本.jpg"> <img width="33%" src="https://foruda.gitee.com/images/1679078779267021583/31c60ab6_5631341.jpeg" title="607知识图谱与深度学习副本.jpg"> <img width="33%" src="https://foruda.gitee.com/images/1679078790195475540/5c7433cd_5631341.jpeg" title="608Representation Learning for Natural Language Processing副本.jpg"> <img width="33%" src="https://foruda.gitee.com/images/1679078800719589109/ef1046ad_5631341.jpeg" title="609基于深度学习的自然语言处理副本.jpg"></p>