# [人大新声 | 北京中学许瑞宸：你好，人大江南](https://mp.weixin.qq.com/s/a4U0lmbDr-6uGNIqzPVy6Q)

人大阳光招生 2023-01-03 12:32 发表于北京

![输入图片说明](https://foruda.gitee.com/images/1672789362526496915/95860065_5631341.png "屏幕截图")

- 微信号：人大阳光招生 rdygzs
- 功能介绍：院系信息详解，招考资讯预报，重要信息公开，贴心咨询服务，热点政策解读……阳光高考之路，人大伴你同行！We want you!

 **编者按** 

> 如果说，成长的轨迹总是多样纷繁，那么成长的故事也必定耐人寻味。中国人民大学坚持以学生为本，遵循教育规律，走内涵发展之路，不断探索拔尖人才培养模式，为学生提供了多样的成才机会和广阔的成长空间。选择一所大学，犹如打开一扇窗，开启一个新世界。结缘人大，是怎样的心路历程？对入校后的学习生活以及未来的人生规划，又有什么新的认识？在 【人大新声】栏目，我们邀请2022级新生为你讲述人大人与人大邂逅的故事。

![输入图片说明](https://foruda.gitee.com/images/1672789428004467312/4211c600_5631341.png "屏幕截图")

> 许瑞宸

高中毕业于北京中学，现为中法学院2022级金融三班本科生。入校以来，担任班级班长一职，加入同心圆心理协会、纵横辩论社等学生组织和社团，积极参与新生辩论赛、“一二•九”合唱比赛等活动。

### 好花须买，皓月须赊

当一年前我快步穿梭在高中校园走廊时，向墙上“中国人民大学”宣传牌投去匆匆一瞥的那一刻，我一定没有想过三百天后的我会真正站在人大江南，向前一步就会走进那神圣的学府，开启未来崭新的、长达一千余个日夜甚至更长的新旅途。

成为“人大人”是我一直以来的梦想。背上行囊，披荆斩棘，日日跋涉，直到梦想一日成真。当接过那份沉甸甸的录取通知书时，我畅想中未来那四年甚至更久的奋斗时光第一次化作真实的形状，让我诚惶诚恐，却也爱不释手。

我好似柏拉图“洞穴论”里走出洞穴的人，过往的所有美好想象终于成为了触手可及的现实。在那一刻，我突然有一种“就这样出发吧”的信念。凡是过往，皆为序章，我终于叩响了自己好奇的金融学专业的大门，门内风景，终悉洞见。于是，我暗下决心，一定好好珍惜在人大的学习机会。

### 天高星辰远，苟求其故

如果弗兰克尔所说“在可能的地方创造”是真实的，那么人大江南便是那个给予我们成长的绝佳“可能”。曾与江南有过一面之缘的我，心中一直把这片水乡视作“人杰地灵”最好的诠释，而人大则近乎成功地把这满城风光浓缩进校园里——荷花绽放于湖面，黑天鹅穿梭在簇簇茎叶搭建的水上绿洲之间，鸽群零零散散地在草地上落脚，形态各异的树木肆意向上挺拔。除了这秀丽的江南景色，当走进教室，则又会被浓厚的法国风情所吸引。

<p><img width="49%" src="https://foruda.gitee.com/images/1672789467475720494/5f0a32b9_5631341.png"> <img width="49%" src="https://foruda.gitee.com/images/1672789475757649996/fa710ed9_5631341.png"> <img width="49%" src="https://foruda.gitee.com/images/1672789483673264671/03fbf484_5631341.png"> <img width="49%" src="https://foruda.gitee.com/images/1672789487555644000/00a757a4_5631341.png"></p>

这所立足中国却具有国际情怀的学校拥有着如此多元却和谐的元素，而我目前在人大苏州短暂却充实的学习经历则更丰富了我对它内涵的理解。在这里的每一天都是如此忙碌，原本还担心着一个半小时的课程是否会太过冗长，但是立体化的法语课程设计和各位专业课老师教授的课程让我穿梭于异国语言之美和金融数学的严谨之中。虽说节奏紧张、作息紧凑，难免疲惫，但面对挑战性和魅力性兼具的学习任务，我仍选择毫不犹豫地积极尝试。

我很荣幸能够和来自五湖四海的、如此优秀的同学、学长学姐同行。在班会上听到同学们分享各自取得的成绩和精彩经历，就好像无数来自四面八方的火车一日并轨，而我也有幸得以欣赏每列车厢的琳琅景色。如今终于要和大家一起并驾齐驱，在无形压力外，更是发自内心地赞叹和欣赏他们的优秀。

除此之外，更有教学经验丰富、学识渊博的各位老师。偶然看到蔡海鸥老师的教学手稿，一叠厚厚的A4纸上黑红笔记整齐密集地排布着，为我学习微积分带来了莫大的鼓励——师且如此，学生何为？国庆假期仍然在学校忙碌的杨桦老师，及时回复每封邮件的江艇老师，课堂上永远循循善诱充满活力的傅梦老师、曾子轩老师和Marin、Diana老师以及幽默风趣也格外认真的李海洋教授……每一位老师都像是一轮太阳，向我们散发着由他们人格魅力和专业知识积累而成的光芒，当我真正沐浴其中时，学习已然成为一种神圣而有趣的事。

学习之余，我继续保持了自初中开始的辩论爱好，再次加入了大学的辩论社团。在社团中，我结识到了对于辩论有着相同热情的同学们，我们积极地参与辩论社的破冰、新生辩论赛等活动。在社团里，我感受到严谨治学的大学鲜活的另一面，一切都被涂抹上我们青春独有的热情和奔放，在这样一个朝气蓬勃的青年社群里，我感到自如与释放。

或许这就是各美其美、美美与共的生动画像，无论是别有风情的校园环境、不同地域的语言文化，还是丰富多彩的校园活动，在每一刻、每个角落，都能感受到这种富有张力的融合，产生了1+1远大于2的效果，而正是这样的能量，让人大江南于每位学子而言无可替代。

### 趁我们头脑发热，我们要不顾一切

法国著名诗人波德莱尔在《恶之花》中说：“趁我们头脑发热，我们要不顾一切。”刚刚成年的我们，正是头脑发热、内心燃烧的年龄。如果说在这样学业压力大但成长收获多的学校读书像是冬日雪夜看烟花，那为什么不抓住这种炙热的温度，用它抵御求学路上的酷寒呢？

人大校友王小波曾说，不学习的人生只能活在现世一个狭窄的圈子里。那已经身处如此广大的平台上的我们则更应该抓紧如此难能可贵的学习机会，在这个“可能之地”不负青春韶华。

![输入图片说明](https://foruda.gitee.com/images/1672789604187352969/88b0ecad_5631341.png "屏幕截图")

我写下这段文字的时候，正值苏州的深秋，没刮风也没下雨，只有树叶摩擦沙沙作响的轻微声音。等第二天太阳升起，这世界会在既有的秩序中向前奔忙。这只是个无比寻常的秋天，无比寻常的夜晚，而我相信，我人生的大树将会在这日复一日、年复一年的寻常岁月中蓬勃生长。

转自微信公众号：[人大江南](https://mp.weixin.qq.com/s?__biz=MzIyMzAwNzQzNA==&mid=2652825828&idx=1&sn=c3c8dfe174fabdb1f42f400ca547e13b)

![输入图片说明](https://foruda.gitee.com/images/1672789775412184974/782d9738_5631341.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1672790980562102816/0078baca_5631341.gif "640 (10).gif")

| # | title | date |
|---|---|---|
| 12. | [新生述体验｜连兴：初访江南很温暖](http://mp.weixin.qq.com/s?__biz=MzIyMzAwNzQzNA==&amp;mid=2652825838&amp;idx=1&amp;sn=2fbc6afaf518d4aace18784d8adf16ad) | 1周前 |
| 11. | [新生述体验｜许瑞宸：你好，人大江南](http://mp.weixin.qq.com/s?__biz=MzIyMzAwNzQzNA==&amp;mid=2652825828&amp;idx=1&amp;sn=c3c8dfe174fabdb1f42f400ca547e13b) | 2022-12-17 |
| 10. | [新生述体验｜谌柳廷：未到江南先一笑，人苏薰风初入弦](http://mp.weixin.qq.com/s?__biz=MzIyMzAwNzQzNA==&amp;mid=2652825768&amp;idx=1&amp;sn=6292a35a5d0be26a026498eb11acd923) | 2022-12-13 |
| 9. | [新生述体验｜陈鹤赟：情系中法 梦启姑苏](http://mp.weixin.qq.com/s?__biz=MzIyMzAwNzQzNA==&amp;mid=2652825586&amp;idx=1&amp;sn=b93a89640658a509e42a7510735f7dbb) | 2022-12-5 |
| 8. | [新生述体验｜施皓轩：走啊，走吧](http://mp.weixin.qq.com/s?__biz=MzIyMzAwNzQzNA==&amp;mid=2652825560&amp;idx=2&amp;sn=c1776f1e11d429e6c659f31e684489f6) | 2022-12-2 |
| 7. | [新生述体验｜胡子怡：蝉鸣于夏，风送晚霞——人大江南，你好](http://mp.weixin.qq.com/s?__biz=MzIyMzAwNzQzNA==&amp;mid=2652824375&amp;idx=1&amp;sn=6b2d53d84743206eeff511f5de037848) | 2022-11-9 |
| 6. | [新生述体验｜傅偲函：君到姑苏见：遇见人大江南](http://mp.weixin.qq.com/s?__biz=MzIyMzAwNzQzNA==&amp;mid=2652824336&amp;idx=1&amp;sn=497c24e55a878bc5e2af343fdae8f4f9) | 2022-11-8 |
| 5. | [新生述体验｜李依凝：人大江南，赴一场四年之约](http://mp.weixin.qq.com/s?__biz=MzIyMzAwNzQzNA==&amp;mid=2652824221&amp;idx=1&amp;sn=8da3539318ee58928f9649b0d2f06269) | 2022-11-2 |
| 4. | [新生述体验｜周佳瑞：遇见人大， 书写新章](http://mp.weixin.qq.com/s?__biz=MzIyMzAwNzQzNA==&amp;mid=2652823942&amp;idx=1&amp;sn=a716dedb355feb82777160fa660cd220) | 2022-10-24 |
| 3. | [新生述体验｜印杜洋：如此人大江南，怎能让人不爱！](http://mp.weixin.qq.com/s?__biz=MzIyMzAwNzQzNA==&amp;mid=2652823818&amp;idx=1&amp;sn=d06c5ad8b52c91069e9c6d09601f9a8d) | 2022-10-19 |
| 2. | [新生述体验｜蒋璐忆：适逢中法 是缘是幸](http://mp.weixin.qq.com/s?__biz=MzIyMzAwNzQzNA==&amp;mid=2652823057&amp;idx=1&amp;sn=ba8ea7ce41d8d0d0ff06386577516e1c) | 2022-10-13 |
| 1. | [新生述体验｜钱时佳：于人大江南，观清风匝地](http://mp.weixin.qq.com/s?__biz=MzIyMzAwNzQzNA==&amp;mid=2652823003&amp;idx=1&amp;sn=28ccd81c681b0c590a36c3a46cc43fbb) | 2022-10-10 |


# [五分钟带你认识中国人民大学中法学院](https://mp.weixin.qq.com/s?__biz=MzIyMzAwNzQzNA==&mid=2652819559&idx=1&sn=f0d1e7afe04844e293658762d5a6a94d)

### 3. [多彩的校园活动](https://foruda.gitee.com/images/1672799872021997728/442527c4_5631341.jpeg)

- 学术类：

  - 人文社科前沿系列讲座
  - 浦江金融论坛

- 体育类

  - 趣味运动会、新生篮球赛、
  - 独墅杯足球联赛、
  - 校区羽毛球赛

- 生活类

  - 寝室文明评选、摄影大赛、
  - 校园 “三行诗” 比赛、
  - 主题红酒品鉴活动

- 文艺类

  - “姑苏弦歌” 元旦晚会、
  - “五·四” 青春歌会、
  - “梦想舞台” 歌手大赛、新生嘉年华、
  - 高雅艺术进校园

- 志愿服务类

  - 走进敬老院志愿活动、
  - 生命护航志愿者培训、
  - 暑期支教、
  - 监护缺失儿童线上辅导、
  - “声入你心” 志愿活动

- 心理类

  - “心里乐翻天” 心理健康知识竞赛
  - 心理健康大讲堂、
  - 心理素质拓展活动

- 就业类

  - “职海引航” 计划、
  - face meeting 交流沙龙、
  - “勇往职前” 模拟招聘会、
  - 面试训练营

- 社会实践类

  - “千人百村”、
  - “街巷中国” 调研
  - “初心·筑梦” 寒假社会实践、
  - 人大使者家乡行

### 了解更多中法学院

请点击下方图片

[![输入图片说明](https://foruda.gitee.com/images/1672800000104549500/a3f0b27c_5631341.png "屏幕截图")](https://mp.weixin.qq.com/mp/appmsgalbum?__biz=MzIyMzAwNzQzNA==&action=getalbum&album_id=2192063382197944329&scene=21#wechat_redirect)

| # | title | date |
|---|---|---|
| 16. | [从人大中法到世界名校｜南洋理工大学篇：史一粟](http://mp.weixin.qq.com/s?__biz=MzIyMzAwNzQzNA==&amp;mid=2652818807&amp;idx=1&amp;sn=2b3d92455b8b903ab5981c8f8c011244) | 2022-5-3 |
| 15. | [从人大中法到世界名校｜芝加哥大学：孙艺嘉](http://mp.weixin.qq.com/s?__biz=MzIyMzAwNzQzNA==&amp;mid=2652818558&amp;idx=1&amp;sn=0b9ba0461213a2471fdca97f8a74b70b) | 2022-4-21 |
| 14. | [从人大中法到世界名校｜芝加哥大学篇：曾煦然](http://mp.weixin.qq.com/s?__biz=MzIyMzAwNzQzNA==&amp;mid=2652818176&amp;idx=1&amp;sn=46b8cb55e503cc906e9dc375a2701357) | 2022-4-10 |
| 13. | [从人大中法到世界名校｜新加坡南洋理工大学篇：方佳郅](http://mp.weixin.qq.com/s?__biz=MzIyMzAwNzQzNA==&amp;mid=2652817788&amp;idx=1&amp;sn=00e9b7178fc640cf61d69a9060ff1c56) | 2022-4-3 |
| 12. | [从人大中法到世界名校｜新加坡国立大学：丁渝洲](http://mp.weixin.qq.com/s?__biz=MzIyMzAwNzQzNA==&amp;mid=2652817676&amp;idx=1&amp;sn=67af9628eaa671f2444f689eddea1e76) | 2022-3-26 |
| 11. | [从人大中法到世界名校｜北大汇丰篇：陶雨丰](http://mp.weixin.qq.com/s?__biz=MzIyMzAwNzQzNA==&amp;mid=2652817666&amp;idx=1&amp;sn=7a7d3b5303a42505b123c66dfa2f30a8) | 2022-3-23 |
| 10. | [从人大中法到世界名校｜新加坡国立大学：朱航震](http://mp.weixin.qq.com/s?__biz=MzIyMzAwNzQzNA==&amp;mid=2652817501&amp;idx=1&amp;sn=bc38190533b597cc2a58fadd6b156ca0) | 2022-3-17 |
| 9. | [从人大中法到世界名校｜哥伦比亚篇：李欣然](http://mp.weixin.qq.com/s?__biz=MzIyMzAwNzQzNA==&amp;mid=2652817416&amp;idx=1&amp;sn=0058cfc689a707e9394852d01fefe92e) | 2022-3-11 |
| 8. | [从人大中法到世界名校｜康奈尔：王怡瑄](http://mp.weixin.qq.com/s?__biz=MzIyMzAwNzQzNA==&amp;mid=2652817364&amp;idx=1&amp;sn=0df657592e8b08b63d6d4d0702197e7a) | 2022-2-19 |
| 7. | [从人大中法到世界名校｜耶鲁篇：金昱含](http://mp.weixin.qq.com/s?__biz=MzIyMzAwNzQzNA==&amp;mid=2652817350&amp;idx=1&amp;sn=d310bd3a17988da487f6f05bd90811d8) | 2022-2-12 |
| 6. | [从人大中法到世界名校｜UCL：周子琪](http://mp.weixin.qq.com/s?__biz=MzIyMzAwNzQzNA==&amp;mid=2652817179&amp;idx=1&amp;sn=3f9c1331b053819fdbf979c6b3bbe697) | 2022-1-21 |
| 5. | [从人大中法到世界名校｜剑桥篇：林佳楠](http://mp.weixin.qq.com/s?__biz=MzIyMzAwNzQzNA==&amp;mid=2652816864&amp;idx=1&amp;sn=7136b2f0a1ebb282e4d4fa8919258ae0) | 2021-12-29 |
| 4. | [从人大中法到世界名校｜剑桥篇：李煜婷](http://mp.weixin.qq.com/s?__biz=MzIyMzAwNzQzNA==&amp;mid=2652815552&amp;idx=1&amp;sn=73f1dc9aae8e0f3e7d100d4f7abf0924) | 2021-12-1 |
| 3. | [从人大中法到世界名校｜UCL：王筱晴](http://mp.weixin.qq.com/s?__biz=MzIyMzAwNzQzNA==&amp;mid=2652816124&amp;idx=1&amp;sn=dc335024ea36a5a36a8efc122b146e5f) | 2021-12-10 |
| 2. | [从人大中法到世界名校｜剑桥篇：陈菲](http://mp.weixin.qq.com/s?__biz=MzIyMzAwNzQzNA==&amp;mid=2652816339&amp;idx=1&amp;sn=7119e31504703b571f84fa16aaa8388f) | 2021-12-16 |
| 1. | [从人大中法到世界名校｜剑桥篇：刘臻](http://mp.weixin.qq.com/s?__biz=MzIyMzAwNzQzNA==&amp;mid=2652816375&amp;idx=1&amp;sn=70fca980e1b984f545105ae1923e97e3) | 2021-12-23 |

[![输入图片说明](https://foruda.gitee.com/images/1672800009105449172/1184d84d_5631341.png "屏幕截图")](https://mp.weixin.qq.com/mp/appmsgalbum?__biz=MzIyMzAwNzQzNA==&action=getalbum&album_id=2394619165115744260&scene=21#wechat_redirect)

| # | title | date |
|---|---|---|
| 26. | [学长学姐述体验｜赵静：走进人大江南，走近金融风险管理](http://mp.weixin.qq.com/s?__biz=MzIyMzAwNzQzNA==&amp;mid=2652820788&amp;idx=1&amp;sn=12a9f35b609982f238695badc6fc4c5c) | 2022-6-20 |
| 25. | [学长学姐述体验｜刘丰喆：脚踏实地，保持热爱](http://mp.weixin.qq.com/s?__biz=MzIyMzAwNzQzNA==&amp;mid=2652820691&amp;idx=1&amp;sn=8ee8c403178d3b8c45b489af4239c46d) | 2022-6-17 |
| 24. | [学长学姐述体验｜傅月玥：道阻且长，行则将至——一个非典型法语专业学生的自述](http://mp.weixin.qq.com/s?__biz=MzIyMzAwNzQzNA==&amp;mid=2652820691&amp;idx=2&amp;sn=9c38d5c7c38b61e702722093cc307953) | 2022-6-17 |
| 23. | [学长学姐述体验｜罗靖鸿：享受“可能性”的乐趣](http://mp.weixin.qq.com/s?__biz=MzIyMzAwNzQzNA==&amp;mid=2652820681&amp;idx=1&amp;sn=3ee4508cedf1e12a42967282fc9cbf7b) | 2022-6-16 |
| 22. | [学长学姐述体验｜黄朱昕瑶：心上学，事上炼](http://mp.weixin.qq.com/s?__biz=MzIyMzAwNzQzNA==&amp;mid=2652820681&amp;idx=2&amp;sn=75fcf97334a2ee7990c7b312ed463de6) | 2022-6-16 |
| 21. | [学长学姐述体验｜王瀚林：未知的不仅是有限范围外的世界，还有我们自己](http://mp.weixin.qq.com/s?__biz=MzIyMzAwNzQzNA==&amp;mid=2652820668&amp;idx=1&amp;sn=f05efacb81a4b4177de114d1460e8c50) | 2022-6-15 |
| 20. | [学长学姐述体验｜唐浩玮：找到你自己，成为你自己](http://mp.weixin.qq.com/s?__biz=MzIyMzAwNzQzNA==&amp;mid=2652820668&amp;idx=2&amp;sn=9ebc1dad8da12b27812d26bae1a473e9) | 2022-6-15 |
| 19. | [学长学姐述体验｜夏以尧：在探寻自我的道路上不断前行](http://mp.weixin.qq.com/s?__biz=MzIyMzAwNzQzNA==&amp;mid=2652820659&amp;idx=2&amp;sn=6ebb58f63f482ffa29df7e28aa93bd3e) | 2022-6-14 |
| 18. | [学长学姐述体验｜王亚勋：去平整土地，别焦虑时光](http://mp.weixin.qq.com/s?__biz=MzIyMzAwNzQzNA==&amp;mid=2652820552&amp;idx=2&amp;sn=910b485e4da6ad059f5fc62598acfdf3) | 2022-6-11 |
| 17. | [学长学姐述体验｜牟雅睿：心思澄澈，宠辱不惊](http://mp.weixin.qq.com/s?__biz=MzIyMzAwNzQzNA==&amp;mid=2652819987&amp;idx=2&amp;sn=272fa4b0ffa55b218b34439d70ebdc5a) | 2022-6-9 |
| 16. | [学长学姐述体验｜刘钊：做中法最快乐的人](http://mp.weixin.qq.com/s?__biz=MzIyMzAwNzQzNA==&amp;mid=2652819959&amp;idx=1&amp;sn=38809cf13868ede8ede61797abad0afa) | 2022-6-8 |
| 15. | [学长学姐述体验｜冯丹阳：仰望星空，脚踏实地](http://mp.weixin.qq.com/s?__biz=MzIyMzAwNzQzNA==&amp;mid=2652819959&amp;idx=2&amp;sn=a2629fddaf651f1d72114666cdddc9ac) | 2022-6-8 |
| 14. | [学长学姐述体验｜张盈：当00后在新时代新征程中成长](http://mp.weixin.qq.com/s?__biz=MzIyMzAwNzQzNA==&amp;mid=2652819938&amp;idx=2&amp;sn=cb8bd919d6983b1e55dd0fd7dde48361) | 2022-6-7 |
| 13. | [学长学姐述体验｜韩丽雪：中法四年，也在彷徨，也在成长](http://mp.weixin.qq.com/s?__biz=MzIyMzAwNzQzNA==&amp;mid=2652819862&amp;idx=2&amp;sn=0e4ef474a4cd142d462204c200ddab98) | 2022-6-6 |
| 12. | [学长学姐述体验｜纪昕怡：与其向往，不如启航](http://mp.weixin.qq.com/s?__biz=MzIyMzAwNzQzNA==&amp;mid=2652819851&amp;idx=1&amp;sn=1a80a895fe9b718680fd02ad6685586f) | 2022-6-5 |
| 11. | [学长学姐述体验｜张谋晖：烟雨载道 吟啸徐行](http://mp.weixin.qq.com/s?__biz=MzIyMzAwNzQzNA==&amp;mid=2652819851&amp;idx=2&amp;sn=2e77c92c259a687530ddde94e65651c8) | 2022-6-5 |
| 10. | [学长学姐述体验｜许文嫣：自由行走在东西方文化平台上](http://mp.weixin.qq.com/s?__biz=MzIyMzAwNzQzNA==&amp;mid=2652819840&amp;idx=1&amp;sn=c181ecc305d10493b7ca92981288b1a1) | 2022-6-4 |
| 9. | [学长学姐述体验｜蔡敏：中法之于我](http://mp.weixin.qq.com/s?__biz=MzIyMzAwNzQzNA==&amp;mid=2652819833&amp;idx=2&amp;sn=446f717deb12384f588eb91604ea650e) | 2022-6-3 |
| 8. | [学长学姐述体验｜张晴：慢慢来，比较快](http://mp.weixin.qq.com/s?__biz=MzIyMzAwNzQzNA==&amp;mid=2652819724&amp;idx=2&amp;sn=1273e29c8412c269f9b70dab00317311) | 2022-6-1 |
| 7. | [学长学姐述体验｜徐乐遥：感悟，奋斗，成长——忆我的中法岁月](http://mp.weixin.qq.com/s?__biz=MzIyMzAwNzQzNA==&amp;mid=2652819705&amp;idx=1&amp;sn=24b92676da1b6d27087ef8fe4073b291) | 2022-5-31 |
| 6. | [学长学姐述体验｜董仔涵：江南好风景，来日再逢君](http://mp.weixin.qq.com/s?__biz=MzIyMzAwNzQzNA==&amp;mid=2652819631&amp;idx=1&amp;sn=aad214a293c853793f6baab83dccaa73) | 2022-5-29 |
| 5. | [学长学姐述体验｜于丁一：坚信实事求是的力量](http://mp.weixin.qq.com/s?__biz=MzIyMzAwNzQzNA==&amp;mid=2652819587&amp;idx=1&amp;sn=5ba8c8647e9d405c792cc0d1ec8fb40d) | 2022-5-26 |
| 4. | [学长学姐述体验｜郭凝：在中法，与更好的自己相遇](http://mp.weixin.qq.com/s?__biz=MzIyMzAwNzQzNA==&amp;mid=2652819566&amp;idx=1&amp;sn=58b88aa2e2fa988facfe2064a0bad5d1) | 2022-5-24 |
| 3. | [学长学姐述体验｜周歆逸：落叶归根，不负人民](http://mp.weixin.qq.com/s?__biz=MzIyMzAwNzQzNA==&amp;mid=2652819410&amp;idx=1&amp;sn=4ef3508e24cff4b1816436f3c466bf94) | 2022-5-16 |
| 2. | [学长学姐述体验｜孙雨露：如抱赤子，心诚求之](http://mp.weixin.qq.com/s?__biz=MzIyMzAwNzQzNA==&amp;mid=2652819389&amp;idx=1&amp;sn=24405d76ef433989ac6f5fa120c6970a) | 2022-5-14 |
| 1. | [学长学姐述体验｜陈阳：但行好事，莫问前程](http://mp.weixin.qq.com/s?__biz=MzIyMzAwNzQzNA==&amp;mid=2652819349&amp;idx=1&amp;sn=7d534555c40e0ef47b2114101bac2dd0) | 2022-5-12 |

![输入图片说明](https://foruda.gitee.com/images/1672802372623517431/a9a51802_5631341.png "屏幕截图")

---

【笔记】[你好，人大江南](https://gitee.com/yuandj/siger/issues/I68MGD)

- [人大新声 | 北京中学许瑞宸：你好，人大江南](https://mp.weixin.qq.com/s/a4U0lmbDr-6uGNIqzPVy6Q)

  - [波德莱尔《恶之花》：人生还不如波德莱尔的一行诗](https://zhuanlan.zhihu.com/p/56636763)
  - [趁我们头脑发热，我们要不顾一切——夏尔·波德莱尔](https://zhuanlan.zhihu.com/p/159644667)
  - [波德莱尔 恶之花 英法双语版 英文原版 The Flowers of Evil Les Fleurs du mal Charles Baudelaire 现代派诗人](https://gitee.com/yuandj/siger/issues/I68MGD#note_15543390_link)
  - [The Flowers of Evil: (Les Fleurs du mal) 精装 – 2021年12月7日](https://gitee.com/yuandj/siger/issues/I68MGD#note_15545592_link)
  - sig / 读书 [封面](https://gitee.com/yuandj/siger/issues/I68MGD#note_15548883_link)

  <p><img width="706px" src="https://foruda.gitee.com/images/1672803937980740207/96e31ef7_5631341.jpeg" title="504恶之花副本.jpg"></p>

- [五分钟带你认识中国人民大学中法学院](https://gitee.com/yuandj/siger/issues/I68MGD#note_15546348_link)