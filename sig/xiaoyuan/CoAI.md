- [“中文版GPT-3”来了：会算术、可续写红楼梦，用64张V100训练了3周](https://www.thepaper.cn/newsDetail_forward_10039031)

- [欢迎来到 交互式人工智能（CoAI）课题组](http://coai.cs.tsinghua.edu.cn/)

  - 从高中辍学到获最佳学生论文——清华硕士王义达的非典型AI之路
  - [清华CoAI课题组近期论文速递](http://coai.cs.tsinghua.edu.cn/news/14)
  - [清华CoAI课题组开源1200万对话数据和中文对话预训练模型CDial-GPT](http://coai.cs.tsinghua.edu.cn/news/13)


<p><img width="706px" src="https://foruda.gitee.com/images/1678902204624992841/84e593df_5631341.jpeg" title="597COAI.jpg"></p>

> 没有看到过一个项目对一位主创这么详细报道“履历”的，由于过长，只能忽略掉转载这一步。在项目中多次提到另外一位导师，更坚定了最初的方向。GPT大模型的威力已经显现，生产力提升的时代下，如何激发人类的创造力呢？或者说提升我们发现宇宙的速度呢？暂不讨论。

标题中的 V100 好像已经禁运啦。

# [“中文版GPT-3”来了：会算术、可续写红楼梦，用64张V100训练了3周](https://www.thepaper.cn/newsDetail_forward_10039031)

量子位QbitAI 2020-11-19 06:51 北京 来源：澎湃新闻·澎湃号·湃客

晓查 发自 凹非寺

量子位 报道 | 公众号 QbitAI

今年，OpenAI推出的自然语言模型GPT-3引起了巨大的轰动。

这是迄今为止最大的NLP模型，包含1750亿参数，光是训练就调用上万块GPU，花费了460万美元的成本。

但GPT-3是基于英语语料库进行训练，而且并不开源，业内一直期待着能有一个中文的超大型NLP模型。

现在，它终于来了！

![输入图片说明](https://foruda.gitee.com/images/1678891936836174469/97d29e1f_5631341.png "屏幕截图")

最近，北京智源人工智能研究院和清华大学研究团队，合作开展了一项大规模预训练模型开源计划——清源CPM (Chinese Pretrained Models)。

11 月中旬，CPM将开放第一阶段的26亿参数规模的中文语言模型 (CPM-LM) 和217亿参数规模的结构化知识表示模型 (CPM-KM) 下载，以及相应的Demo。

该项目的源代码和模型已经在GitHub和官网开放下载。

![输入图片说明](https://foruda.gitee.com/images/1678891945280007629/905ca890_5631341.png "屏幕截图")

CPM中文语言模型与GPT-3模型类似，仅需要通过少次、单次学习甚至零次学习，就能完成不同自然语言处理任务，具备一定的常识和认知的泛化能力。
官方表示，清源CPM计划所有模型免费向学术界和产业界开放下载，供研究使用。

### 模型特点

与已有的中文预训练模型相比，本次发布的清源 CPM 大规模预训练模型具有以下特点：

![输入图片说明](https://foruda.gitee.com/images/1678891959429205502/487083a1_5631341.png "屏幕截图")

1、语料丰富多样：收集大量丰富多样的中文语料，包括百科、小说、对话、问答、新闻等类型。
2、模型规模大：本次发布的 CPM-LM 的参数规模为 26 亿，预训练中文数据规模100 GB，使用了 64 块 V100 GPU 训练时间约为 3 周。

3、学习能力强：能够在多种自然语言处理任务上，进行零次学习或少次学习达到较好的效果。

4、行文自然流畅：基于给定上文，模型可以续写出一致性高、可读性强的文本，达到现有中文生成模型的领先效果。

### Demo展示

为了更直观地展示清源CPM预训练模型的效果，官方提供了一些文本生成的Demo。

GPT-3能胜任的常识性问答，CPM预训练模型一样可以应对：

![输入图片说明](https://foruda.gitee.com/images/1678891983149021984/f6e7e385_5631341.png "屏幕截图")

它能够根据真实的天气预报内容，生成天气预报文本模板：

![输入图片说明](https://foruda.gitee.com/images/1678891990246533216/ce7306d6_5631341.png "屏幕截图")

除了生成文字外，清源CPM还具有一定的数理推理，根据之前的规律生成计算结果：

![输入图片说明](https://foruda.gitee.com/images/1678891998454827584/c84afc18_5631341.png "屏幕截图")

甚至可以续写红楼梦片段：

![输入图片说明](https://foruda.gitee.com/images/1678892006440632790/a408dd5c_5631341.png "屏幕截图")

另外，智源和清华团队还在几项基准测试中验证了清源CPM的实际性能。

1、中文成语填空

ChID 是 2019 年清华大学对话交互式人工智能实验室（CoAI）收集的中文成语填空数据集，其目标是对于给定的段落，在 10 个候选项中选择最符合段意的成语进行填空。

![输入图片说明](https://foruda.gitee.com/images/1678892032624499437/5c0f83ce_5631341.png "屏幕截图")

表中汇报了预测的准确率，可以看到，CPM(大) 在无监督的设定下甚至达到了比有监督的 CPM (小) 更好的结果，反应了清源 CPM 强大的中文语言建模能力。

2、对话生成

STC是2015年华为诺亚方舟实验室提出的短文本对话数据集，要求在给定上文多轮对话的条件下预测接下来的回复。

![输入图片说明](https://foruda.gitee.com/images/1678892040796545159/cfd2c602_5631341.png "屏幕截图")

在无监督的设定下，清源 CPM 具有更好的泛化性，在有监督设定下，清源 CPM 能达到比 CDial-GPT 更优的效果，尤其在多样性指标上表现更佳。以下为生成的对话样例。

![输入图片说明](https://foruda.gitee.com/images/1678892047687227729/b4a5f0e6_5631341.png "屏幕截图")

3、文本分类

清源 CPM 使用头条新闻标题分类 (TNEWS，采样为4分类)，IFLYTEK应用介绍分类 (IFLYTEK，采样为4分类)，中文自然语言推断 (OCNLI，3分类) 任务作为文本分类任务的基准。

![输入图片说明](https://foruda.gitee.com/images/1678892059398442668/fb303389_5631341.png "屏幕截图")

可以看出，清源CPM能够在无监督的设定下达到比随机预测 (TNEWS/IFLYTEK/OCNLI 随机预测精确度分别为0.25/0.25/0.33) 好得多的精确度。

4、自动问答

CPM 使用 DuReader 和CMRC2018 作为自动问答任务的基准，要求模型从给定的段落中抽取一个片段作为对题目问题的答案。其中DuReader 由百度搜索和百度知道两部分数据组成。

![输入图片说明](https://foruda.gitee.com/images/1678892070877843516/fad00786_5631341.png "屏幕截图")

在单样本设定下，CPM 能从给定的样本中学习到生成答案的模式，因此效果总是比零样本设定更好。由于模型的输入长度有限，多样本输入的场景将在未来进行探索。

5、实体生成

CPM 采用 XLORE 中的几种常见的关系三元组作为实体生成任务的基准。在少样本设定 (把少量真实样本拼在待预测样本前作为提示) 下，不同规模的 CPM 模型的 BLEU-1 值如下表所示。

![输入图片说明](https://foruda.gitee.com/images/1678892081855159054/6d984892_5631341.png "屏幕截图")

可以看出参数量越大时，模型对于预测实体效果越好。同时，模型在给定 2 个样本时就可以达到不错的效果，大部分时候 N=2 和 N=4 的效果是接近的。

![输入图片说明](https://foruda.gitee.com/images/1678892093977247797/4dc81225_5631341.png "屏幕截图")

64块V100训练3周

智源和清华本次发布的大规模预训练模型，难以在单块GPU上运行，因此需要将模型在多个 GPU之间分配参数，进行并行化训练。

CPM正是基于英伟达的大规模并行计算训练项目Megatron-LM。

CPM模型预训练过程分布在多块 GPU 上，采用层内并行的方法进行训练，并基于当前已有的成熟技术，减少同步提高通讯速率。

本次发布的CPM-LM的参数规模为26亿，预训练中文数据规模100GB，使用64块英伟达V100 GPU，训练时间约为3周。

而CPM-KG的参数规模为217亿，预训练结构化知识图谱为WikiData全量数据，包含近 1300 个关系、8500万实体、4.8 亿个事实三元组，使用了8块英伟达V100 GPU训练时间约为2周。

### 未来计划

今年年底开源的两个项目只是清源NLP研究计划的第一步，据了解，清源 CPM 未来一年的研究和开源计划是：

![输入图片说明](https://foruda.gitee.com/images/1678892112557624323/4ec42001_5631341.png "屏幕截图")

阶段1 (2020年10月-12月)：中文大规模预训练语言模型，含约 30 亿参数，训练数据包括 100GB 中文数据。

阶段2 (2021年01月-06月)：以中文为核心多语言大规模预训练语言模型，含约 200 亿参数，训练数据包括 500GB 以中文为核心的多语言数据。

阶段3 (2021年07月-09月)：知识指导的大规模预训练语言模型，含约 1000 亿参数，训练数据包括 1TB 以中文为核心的多语言数据和亿级实体关系图谱。

清源 CPM 计划将积极配备算力、数据和人力，注重开展原创研究，尽早实现与国际顶尖机构在超大规模预训练模型技术方面并跑，提升中文自然语言的深度理解和生成能力。

与此同时，智源研究院也将积极与产业界合作，在智能客服、个性推荐、文本生成、自动编程等方面，探索新型的人工智能应用和商业模式。

### 关于清源CPM计划

清源CPM计划是以中文为核心的大规模预训练模型。

首期开源内容包括预训练中文语言模型和预训练知识表示模型，可广泛应用于中文自然语言理解、生成任务以及知识计算应用。

清源CPM计划由北京智源人工智能研究院和清华大学研究团队合作开展。“自然语言处理”是智源研究院重点支持的重大研究方向之一。

智源在该方向上集结了大量国内权威学者，这些学者在NLP领域积累了丰富的研究成果。

如清华大学孙茂松、刘知远团队和李涓子、唐杰团队提出了知识指导的预训练模型 ERNIE 和 KEPLER，循环智能杨植麟团队提出了性能显著优于 BERT 的 XLNet 模型，清华大学朱小燕和黄民烈团队提出了面向情感分析的预训练模型 SentiLARE，融合常识知识的预训练语言生成模型 StoryGPT，面向中文对话生成的 CDial-GPT模型，等等。

研究团队将在智源研究院大规模算力平台的支持下，开展以中文为核心的超大规模预训练模型研究，包括跨语言学习、文本生成、知识融合、模型并行和压缩等前沿课题，并将相关模型及时通过智源社区开源共享。

### 传送门

清源CPM项目主页：

https://cpm.baai.ac.cn/

清源CPM源代码主页：

https://github.com/TsinghuaAI/CPM-Generate

— 完 —

本文系网易新闻•网易号特色内容激励计划签约账号【量子位】原创内容，未经账号授权，禁止随意转载。

原标题：《“中文版GPT-3”来了：会算术、可续写红楼梦，用64张V100训练了3周》

[阅读原文](http://mp.weixin.qq.com/s?__biz=MzIzNjc1NzUzMw==&mid=2247560048&idx=3&sn=46e495baebe0a54f4b5d9e4e87f1b8fc)

---

【笔记】 https://gitee.com/yuandj/siger/issues/I6IVZU#note_16801503_link

https://www.thepaper.cn/newsDetail_forward_10039031
“中文版GPT-3”来了：会算术、可续写红楼梦，用64张V100训练了3周

中文版GPT-3”来了：会算术、可续写红楼梦，用64张V100训练了3周
https://mp.weixin.qq.com/s?__biz=MzIzNjc1NzUzMw==&mid=2247560048&idx=3&sn=46e495baebe0a54f4b5d9e4e87f1b8fc

https://openai.com/blog/gpt-3-apps/

> Viable helps companies better understand their customers by using GPT-3 to provide useful insights from customer feedback in easy-to-understand summaries.

> Using GPT-3, Viable identifies themes, emotions, and sentiment from surveys, help desk tickets, live chat logs, reviews, and more. It then pulls insights from this aggregated feedback and provides a summary in seconds.

> For example, if asked, What’s frustrating our customers about the checkout experience?, Viable might provide the insight: Customers are frustrated with the checkout flow because it takes too long to load. They also want a way to edit their address in checkout and save multiple payment methods.

  - “GPT-3’s ability to identify themes from natural language and generate summaries allows Viable to give product, customer experience, and marketing teams at companies across industries a better understanding of their customers’ wants and needs.”

    > — Daniel Erickson, CEO of Viable

- CoAI - 清华大学交互式人工智能课题组
  coai.cs.tsinghua.edu.cn
  > Nov 15, 2020 · 交互式人工智能课题组(Conversational AI, CoAI)隶属于清华大学计算机系、清华大学人工智能研究院。交互式人工智能是指通过对话、问答等语言交互方式体现出 …

  - 科研团队
  - 平台工具
  - 人才招聘
  - 发表文章
  - 最新新闻
  - 内部论坛
  - EXPLORE FURTHER

  EXPLORE FURTHER

  - 清华大学机器学习有哪些不错的博士生导师？ - 知乎 zhihu.com
  - 连续五天，ACL 2020 清华 CoAI 系列解读！ - 知乎 zhuanlan.zhihu.com
  - 清华大学有哪些值得推荐的老师？ - 知乎 zhihu.com
  - Dr. Minlie Huang's Homepage - coai.cs.tsinghua.edu.cn
  - 德勤交互式人工智能白皮书：交互式人工智能正在 ... www2.deloitte.com


- CoAI - 清华大学交互式人工智能课题组
  coai.cs.tsinghua.edu.cn/team
  > 博士研究生. 李道义. 2014年入学，研究方向为智能交互、推荐系统. 张正. 2015年入学，研究方向为任务型对话系统. 柯沛. 2017年入学，研究方向为自然语言生成、对话系统. 朱祺. 2018年入学，研究方向为任务型对话系统.

  发表文章 · 内部论坛 · 最新新闻 · 研究方向为人工智能、自然语言处理、信息获取等领域

- CoAI - 清华大学交互式人工智能课题组
  coai.cs.tsinghua.edu.cn
  > Nov 15, 2020 · 交互式人工智能课题组(Conversational AI, CoAI)隶属于清华大学计算机系、清华大学人工智能研究院。交互式人工智能是指通过对话、问答等语言交互方式体现出 …

  - 科研团队
  - 平台工具
  - 人才招聘
  - 发表文章
  - 最新新闻
  - 内部论坛
  - EXPLORE FURTHER

  EXPLORE FURTHER

  - 清华大学机器学习有哪些不错的博士生导师？ - 知乎 zhihu.com
  - 连续五天，ACL 2020 清华 CoAI 系列解读！ - 知乎 zhuanlan.zhihu.com
  - 清华大学有哪些值得推荐的老师？ - 知乎 zhihu.com
  - Dr. Minlie Huang's Homepage - coai.cs.tsinghua.edu.cn
  - 德勤交互式人工智能白皮书：交互式人工智能正在 ... www2.deloitte.com


- CoAI - 清华大学交互式人工智能课题组
  coai.cs.tsinghua.edu.cn/team
  > 博士研究生. 李道义. 2014年入学，研究方向为智能交互、推荐系统. 张正. 2015年入学，研究方向为任务型对话系统. 柯沛. 2017年入学，研究方向为自然语言生成、对话系统. 朱祺. 2018年入学，研究方向为任务型对话系统.

  发表文章 · 内部论坛 · 最新新闻 · 研究方向为人工智能、自然语言处理、信息获取等领域

- [欢迎来到 交互式人工智能（CoAI）课题组](http://coai.cs.tsinghua.edu.cn/)

  > 交互式人工智能课题组(Conversational AI, CoAI)隶属于清华大学计算机系、清华大学人工智能研究院。交互式人工智能是指通过对话、问答等语言交互方式体现出来的智能行为，是人工智能最具挑战性、最综合性的技术，涵盖了语义理解、知识表示、逻辑与推理、语言生成等各个方面。

  - 最近新闻

    - 从高中辍学到获最佳学生论文——清华硕士王义达的非典型AI之路 | 2020年11月30日
    - 清华CoAI课题组近期论文速递 |  2020年11月15日
    - 清华CoAI课题组开源1200万对话数据和中文对话预训练模型CDial-GPT |  2020年11月15日

  <p><img height="99px" src="https://foruda.gitee.com/images/1678892438021530085/a9228fe5_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1678892456528022420/fca92444_5631341.png"> <img height="99px" src="https://foruda.gitee.com/images/1678892500714717374/ddd481d4_5631341.png"></p>

---

# [清华CoAI课题组近期论文速递](http://coai.cs.tsinghua.edu.cn/news/14)

![输入图片说明](https://foruda.gitee.com/images/1678899963736873455/f0c24357_5631341.png "屏幕截图")

2020年11月15日 17:35

清华大学计算机系对话交互式人工智能课题组（CoAI）隶属于清华大学人工智能研究的知识智能研究中心、智能信息获取研究中心。CoAI小组的研究兴趣主要集中在对话系统、语言生成、泛问答、常识推理等自然语言处理方向。近期，课题组的相关研究成果被自然语言处理领域的国际会议EMNLP 2020、AACL 2020 接收。其中，EMNLP是计算语言学和自然语言处理领域的顶级国际会议，AACL是亚洲ACL分会，2020年是第一届。



### 标题：SentiLARE: Sentiment-aware LAnguage REpresentation Learning with Linguistic Knowledge

- 作者：柯沛，计昊哲，柳思杨，黄民烈，朱小燕
- 关键词：情感分析，语言学知识，预训练语言模型
- 类型：EMNLP 2020, Long Paper
- 论文地址：https://arxiv.org/abs/1911.02493
- GitHub：https://github.com/thu-coai/SentiLARE

 **简介：** 现有的多数预训练语言模型均没有考虑使用语言学知识，而已有工作表明，有效利用语言学知识可以提升模型在自然语言理解任务（例如情感分析）上的性能。本工作将词级别的语言学知识（包括词性和词的情感极性）引入预训练语言模型中，提出了一种适用于情感分析任务的语言表示模型SentiLARE。该模型主要包含两个模块：1) 知识获取：通过上下文感知的情感注意力机制从SentiWordNet上获取词的情感极性；2) 知识融合：以标签感知的掩码语言模型作为预训练任务来构建知识增强的预训练语言模型。实验表明SentiLARE在各类情感分析任务上均能取得当前最佳性能。



### 标题：Language Generation with Multi-hop Reasoning on Commonsense Knowledge Graph

- 作者：计昊哲，柯沛，黄绍晗，韦福如，黄民烈，朱小燕
- 关键词：语言生成，常识推理
- 类型：EMNLP2020, Long Paper
- 论文地址：https://arxiv.org/abs/2009.11692
- GitHub: https://github.com/cdjhz/multigen

 **简介：** 生成式预训练语言模型在常识推理相关的文本生成任务中仍难以很好地利用基础知识生成合理的文本。现有的常识融合的预训练语言模型仅通过在独立的知识三元组上后训练来学习知识库中的关系知识，而忽略了知识图谱的结构化信息。本文提出了GRF模型，在每一步解码时刻模型在图谱的多关系路径上进行动态多跳推理，并选择拷贝关键实体用于生成。我们在常识解释生成、归因常识推理、故事结局生成三个文本生成任务上进行了实验，自动指标和人工评测均表明我们的模型优于现有的基线模型。(注：与微软亚洲研究院合作)



### 标题：UNION: An Unreferenced Metric for Evaluating Open-ended Story Generation

- 作者：关健，黄民烈
- 关键词：开放端语言生成评价，自监督学习
- 类型：EMNLP 2020, Long paper
- 论文地址：https://arxiv.org/abs/2009.07602
- GitHub：https://github.com/thu-coai/UNION

 **简介：** 尽管现有的有参考指标（如BLEU和MoverScore）已经在机器翻译评价等任务上有较好的表现，但是对于开放端语言生成评价（如故事生成、对话生成）等具有一对多特性的任务， 这些指标仍然与人工评价有较低的相关性。其中，一对多是指，对于同一个输入，有许多种合理的输出，这些输出在字面和语义上可能有较大差异，因此有限的参考文本难以完全覆盖这些输出。为了缓解这个问题，我们为开放端故事生成评价提出了一个可学习的无参考指标UNION (UNreferenced metrIc for evaluating Open-eNded story evaluation)，可以不需要任何参考文本即可对生成故事的质量进行评价。UNION建立在BERT的基础上，通过微调来区分人撰写的故事和负样本，同时恢复负样本中的扰动。我们通过模仿在现有的语言生成模型中常被观察到的错误来构建这些负样本，包括重复、矛盾、上下文不一致等。在两个故事数据集上的实验表明，UNION在评价生成故事的质量上是一个更可靠的评价指标，与人工评价有更好的相关性，同时也比现有的SOTA评价指标更好的泛化性。

### 标题：Dialogue Distillation: Open-domain Dialogue Augmentation Using Unpaired Data

- 作者：张荣升，郑银河（共同第一作者），邵建智，毛晓曦，席亚东，黄民烈
- 关键词：对话系统，数据增广，模型蒸馏
- 类型：EMNLP 2020, Long Paper
- 论文地址：https://arxiv.org/abs/2009.09427
- GitHub: https://github.com/njuzrs/dialogue_distillation

 **简介：** 目前开放领域对话模型的训练过程极大地依赖于大规模对话数据，但是，收集高质量的对话数据是非常消耗人力和物力的事情。本文中，我们提出了一个可适用于开放领域对话的数据增广的方法：“对话蒸馏”。我们的方法中使用了非平行数据来增广对话对。具体来说，我们首先设计了一个数据层面的蒸馏过程，在这一过程中，我们根据现有的对话对，在非配对数据中检索相似的句子，从而组成增广后的对话对。我们还提出了一个模型层面的蒸馏过程，这一过程中我们首先使用少量高质量的对话数据训练得到了一个Teacher模型，然后再基于增广后的数据，将这一teacher模型蒸馏到一个student模型中。自动和人工评测均表明，我们的方法可以生成高质量的增广数据，并且我们所提出的对话蒸馏方法可以进一步帮我们提升开放领域对话模型的性能。(注：与网易伏羲实验室合作)

### 标题：Difference-aware Knowledge Selection for Knowledge-grounded Conversation Generation

- 作者：郑楚杰，曹云波，姜大昕，黄民烈
- 关键词：对话生成，知识选择
- 类型：Findings of EMNLP, Long Paper
- 论文地址：https://arxiv.org/abs/2009.09378
- GitHub: https://github.com/chujiezheng/DiffKS

 **简介：** 在基于知识的多轮对话生成任务中，现有的知识选择模型往往忽略了不同轮次所选知识之间的关联。在本文中，我们提出了知识选择模型DiffKS，其显式建模并利用多轮对话所选知识之间的差异信息以促进知识选择。我们进一步设计了两种模型变体，其中差异信息与上下文信息彼此融合或相互解耦。我们在Wizard of Wikipedia和Holl-E两个基准数据集上进行评估，自动评价、人工观测评价和交互评价的结果均表明，DiffKS在知识选择和对话生成方面均显著优于现有的知识选择模型。(注：与腾讯、微软工程院合作)

### 标题：Continual Learning for Natural Language Generation in Task-oriented Dialog Systems

- 作者：糜飞，陈良玮，赵梦杰，黄民烈，Boi Faltings
- 关键词：自然语言生成，连续学习
- 类型：Findings of EMNLP, Long Paper

 **简介：** 现有的自然语言生成模型多数是针对限定领域的离线模型。为了更好的反应现实应用场景，本文提出了连续学习的自然语言生成框架。为了解决其中关键的灾难性遗忘问题，我们提出了一个名为ARPER (Adaptively Regularized Prioritized Exemplar Replay) 的模型。ARPER包括了：(1) 具有优先级的样本回放；(2) 基于EWC的动态的正则项。我们在MultiWoZ-2.0数据集上对自然语言生成任务的灾难性的遗忘问题进行了诊断，并对ARPER和多种连续学习的方法进行了全面评估。结果表明ARPER对于自然语言生成的连续学习任务显著优于现有方法，并且更好的防止灾难性遗忘问题。（注：第一作者为EPFL博士生）

### 标题：Robustness to Modification with Shared Words in Paraphrase Identification

- 作者：施舟行，黄民烈
- 关键词：模型鲁棒性，对抗样本
- 类型：Findings of EMNLP, Short paper
- 论文地址：https://arxiv.org/abs/1909.02560

 **简介：** 自然语言处理模型的鲁棒性对模型在特殊或困难输入上的表现有重要意义。本文从一个新的视角研究同义复述模型的鲁棒性缺陷。对于一个原始样本句子对，我们替换其中一些两个句子的共有词，或是引入新的共有词，由此构建令模型预测出错的合法新样本。我们设计规则限制可修改位置，利用BERT masked language model为指定位置生成替换词，并使用beam search找到替换方案。常规模型在经修改的样本上准确率大幅下降，这揭示了模型面对共有词修改时存在鲁棒性缺陷。我们也使用了对抗训练减轻这一缺陷。

### 标题：Learning Goal-oriented Dialogue Policy with Opposite Agent Awareness

- 作者：张正，廖黎姿，朱小燕，蔡达成，刘子韬，黄琰，黄民烈
- 类型：AACL 2020, Long Paper
- 关键词：任务导向对话，对话策略，非合作式对话，强化学习
- 论文地址：https://arxiv.org/abs/2004.09731

 **简介：** 任务导向对话领域现有的大部分策略学习方法都是基于强化学习，专注于系统策略的训练，简单地将用户策略当作环境的一部分。然而在现实世界中，对手的策略往往可以被推测出来并加以利用，以辅助系统决策。在人类活动中，这种行为很常见。基于这种行为，我们提出了一种考虑对手行为的对话策略学习框架。在这个框架中，我们首先估计对手的策略，然后将这些策略视作系统策略的一部分，辅助系统策略进行决策。我们的方法在合作式和非合作式对话任务上都取得了不错的结果。(注：与好未来、新加坡国立合作)



### 标题：Generating Commonsense Explanation by Extracting Bridge Concepts from Reasoning Paths

- 作者：计昊哲，柯沛，黄绍晗，韦福如，黄民烈
- 关键词：解释生成，常识推理
- 类型：AACL 2020, Long Paper
- 论文地址：https://arxiv.org/abs/2009.11753
- GitHub: https://github.com/cdjhz/CommExpGen

 **简介：** 常识解释生成任务旨在考察模型针对反常识陈述生成合理解释的能力。尽管这项任务对人类来说难度不大，但机器仍然难以生成合理且具有信息量的解释。本工作提出了一个两阶段的方法，首先根据输入陈述从图谱上抽取出实体，然后结合实体生成最终的解释。我们建立的实体抽取模型在 ConceptNet 中提取的路径上进行显式推理，通过对三元组评分并在路径上传播节点分数，最后抽取出相关实体用于解释生成。我们在常识解释生成任务上进行了实验，我们的模型在自动评估和人工评估方面都优于现有的基线模型。(注：与微软亚洲研究院合作)

# [清华CoAI课题组开源1200万对话数据和中文对话预训练模型CDial-GPT](http://coai.cs.tsinghua.edu.cn/news/13)

2020年11月15日 17:28

基于Transformer的大规模预训练语言模型极大地促进了开放领域对话的研究进展。然而目前这一技术在中文对话领域并未被广泛应用，主要原因在于目前缺乏大规模高质量的中文对话开源数据。为了推动中文对话领域的研究，弥补中文对话语料不足这一问题，我们发布了一个包含1200万对话的大规模中文对话数据集LCCC，并开源了在LCCC上预训练的大规模中文对话生成模型CDial-GPT。相关论文获得 NLPCC 2020 best student paper。

开源地址：https://github.com/thu-coai/CDial-GPT

![输入图片说明](https://foruda.gitee.com/images/1678900205177735277/c4cb0231_5631341.png "屏幕截图")

### LCCC数据集的构建

LCCC（Large-scale Cleaned Chinese Conversation）数据集有LCCC-base与LCCC-large两个版本，其中LCCC-base和LCCC-large中各包含6.8M和12M对话。这些数据是从79M原始对话数据中经过严格清洗得到的，也是目前所开源的规模最大、清洗最严格的中文对话数据集。

![输入图片说明](https://foruda.gitee.com/images/1678900226195104172/0068983f_5631341.png "屏幕截图")

> 表1. 被过滤掉的噪音数据



开放领域对话数据的构建通常有三种方式：1、抽取剧本对话；2、人工众包构建对话；3、爬取社交媒体上用户的交流记录。使用第一种方式构建的对话在内容上依赖于特定剧情和场景，与日常对话有较大差异。使用第二种方式构建的对话质量最高，但是由于人力成本过高，无法使用这一方式构建大规模数据集。使用第三种方式可以较为廉价地获取大规模对话数据，因此LCCC数据集中的原始数据主要使用第三种方式收集。

我们同时注意到，来自社交媒体的对话数据中存在各种各样的噪音（表1），为了保证LCCC中对话数据的质量，我们设计了如下数据获取和清洗策略：

1. 数据获取

我们的数据获取流程分为两个阶段。在第一个阶段，我们挑选了微博上由专业媒体团队运营的新闻媒体账号，然后收集了一批在这些新闻媒体下留言互动的活跃用户。在第二个阶段中，我们收集了这些活跃用户微博下的留言互动，并将其作为我们的原始数据。微博下的留言回复一般以一个树形结构展开，我们将这一树形回复结构中每一条从根节点到叶子节点的路径作为一个完整对话，最终共收集到了79M对话数据。

2. 数据清洗

为了保证数据质量，我们对收集到的原始对话数据进行了两个阶段的清洗。

第一阶段的清洗主要基于手工规则。这一阶段的主要目的是为了过滤掉对话数据中的明显噪声，如脏话、特殊符号、病句、复读机句式、广告、违法暴力信息等。在这一阶段中，我们花费了数周时间使用人工排查的方式优化规则。

第二阶段的清洗主要基于分类器过滤。在这一阶段中，我们基于BERT训练了两个文本分类器，第一个分类器主要用于甄别那些无法通过规则检测的噪音，如：1、语义模糊、语法错乱或有严重拼写错误的语句；2、时效性太强的对话；3、与上下文语义不相关的回复。第二个分类器主要用于甄别那些需要依赖额外上下文信息，如图片或视频等，才能理解的对话。这两个分类器均使用人工标注数据训练，我们为其标注了共计11万对话数据，最终的分类器在人工标注的测试集上分别达到了73.76%和77.60%的准确率。我们通过F1-score选择阈值来过滤得到高质量的对话数据。

![输入图片说明](https://foruda.gitee.com/images/1678900269636052861/13398c99_5631341.png "屏幕截图")

> 表2. 数据统计信息，左侧为LCCC-base，右侧为LCCC-large

最终我们基于上述原始对话数据过滤得到了6.8M高质量的对话数据LCCC-base。此外，我们还收集了目前已公开的其他对话数据，并使用同样的清洗流程，结合LCCC-base构造了包含12M对话的数据集LCCC-large。表2展示了这两个数据集中单轮对话和多轮对话的详细统计信息。

### 中文对话预训练模型CDial-GPT

为促进中文对话预训练模型的发展，我们基于LCCC数据集预训练了大规模中文对话生成模型CDial-GPT。该模型的训练过程包含两个阶段，首先，我们在总计5亿字符、包含各类题材的小说数据上训练得到了一个中文小说GPT预训练模型，然后在该模型的基础上，我们使用LCCC中的对话数据继续对模型进行训练，最终得到了中文对话预训练模型CDial-GPT。

![输入图片说明](https://foruda.gitee.com/images/1678900303776924001/20dd7268_5631341.png "屏幕截图")

> 图1. 输入编码示例

该模型拥有12层Transformer结构，我们按字分词，字典大小13088，字向量维度768，最长上下文长度为513。我们沿用TransferTransfo的方式对对话进行建模，即把对话历史拼接为长文本，并使用段分割向量加以区分。具体来说：我们使用[CLS]字符标志文本起始，在段落后使用[SEP]字符表示段落结束，在段落中对相邻轮次对话使用[speaker1]、[speaker2]交替分割，并在segment embedding中使用[speaker1]、[speaker2]进行编码。图1为输入数据示例。

### 模型效果评测

为了评估对话预训练模型的质量，我们在440万规模的中文对话数据集STC上对其进行了评测实验，并对比了现有的中文对话预训练模型和一些经典的非预训练对话模型。我们主要通过PPL这一指标来反映模型的拟合能力，PPL越低表示模型的拟合能力越强。我们通过基于n-gram重合度的指标BLEU和基于Embedding相似度的指标Greedy Matching 和Embedding Average来衡量对话回复与真实回复的相关性，并通过Dist-n指标来衡量生成回复的多样性。实验结果展示在表3中。可以看到我们的模型在绝大多数指标上达到了最好的效果。由于自动指标无法完全反映生成对话的质量，于是我们对各模型生成的对话进行了人工评测。

![输入图片说明](https://foruda.gitee.com/images/1678900329808399644/50eb14fd_5631341.png "屏幕截图")

> 表3. 自动指标评估

![输入图片说明](https://foruda.gitee.com/images/1678900345238622834/06e4b1e2_5631341.png "屏幕截图")

> 表4. 人工评价

在人工评测中我们主要考虑3个方面：1、语法性，也就是生成语句的流畅性；2、相关性，即生成语句与对话上文的相关性；3、信息量，即生成结果自身含有的信息量。具体来说，不符合语法性或与对话上文不相关的生成结果我们给予0分；语句流畅、和对话上文相关但信息量不足的生成结果给予1分；语句流畅、和对话上文相关并且信息量充足的生成结果给予2分，我们将人工评测结果展示在表4中。实验结果显示，我们的预训练模型拥有出色的生成质量，可以在生成信息量充足的回复的同时，保持较高的流畅性与相关性，优于其他基线模型，一些生成例子展示在表5，6中。

![输入图片说明](https://foruda.gitee.com/images/1678900357224316094/029ebafd_5631341.png "屏幕截图")

> 表5. 人机交互示例 （左）和 模型互相交互示例（右）

![输入图片说明](https://foruda.gitee.com/images/1678900369738304110/35670870_5631341.png "屏幕截图")

> 表6. 在STC微调的生成结果示例

目前CDial-GPT模型以及LCCC数据集都已公开，我们提供了训练以及微调代码，可以方便地应用于各种数据和下游任务上。读者可以通过以下链接获取数据集以及模型： https://github.com/thu-coai/CDial-GPT