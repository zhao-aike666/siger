# [Meta公布四款VR原型机，提出“视觉图灵测试”概念](http://www.7tin.cn/news/166778.html)

原创 | [青亭网](http://www.7tin.cn/) | hi188 | 2022-06-21 12:17:10

> 青亭网( ID:[qingtinwang](https://images.gitee.com/uploads/images/2022/0623/134138_a6f8ac05_5631341.png) )--链接科技前沿，服务商业创新!

今年的F8大会虽然被取消，但是Meta仍在不断向外界传递最新的研发进展。在Meta AI部门Inside the Lab活动将定期向媒体公开，最近的一次关于头显原型展示的主题可谓是信息量爆炸，似乎就是F8的重现。

与去年Connect上宣布的Project Cambria不同，它们往往是准量产机型，并即将发售。今天展示的更多是技术原型，在这段约30分钟的视频中共展示四款原型机，可以让我们更全面地了解到Meta在VR头显中采用的最新技术和设计方向。

![meta-reality-labs-research-vr-headset-prototype-3](https://images.gitee.com/uploads/images/2022/0623/134224_1330c13d_5631341.png "屏幕截图.png")

​本次的Inside the Lab公开视频由扎克伯格和Meta首席科学家Michael Abrush共同主讲，最新公布的四款原型机代号分别为：Butterscotch、Starburst、Holocake 2、Mirror Lake。

[![输入图片说明](https://images.gitee.com/uploads/images/2022/0623/134255_fc62b53d_5631341.png "屏幕截图.png")](https://v.qq.com/txp/iframe/player.html?vid=s3344h6v30x)

这四款原型机分别代表着VR产品不同的演进方向，如Butterscotch以高分辨率/高清显示为核心，在产品体积和重量上就会有取舍；Starburst像是更前沿的技术研究，以HDR和超高亮度为核心；Holocake 2更像是轻量化的准量产机，以小巧、轻薄为；Mirror Lake特点则更显示将各种新技术融合在一起的轻量化VR眼镜。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0623/134329_6af26d1c_5631341.png "屏幕截图.png")

Meta VR原型展示墙

### 提出“视觉图灵测试”概念

扎克伯格和Michael Abrush同时表示，Meta的最终目标是打造出可以满足所有视觉要求的VR硬件，让你的视觉系统认为“足够真实”。

扎克伯格表示：未来VR显示技术的发展，将远超过传统2D屏幕，我们不是要研究已经应用在手机、显示器中的屏幕技术，而是要做新的探索，探索人类视觉系统如何感知世界，探索“物理显示”如何与人眼协同工作。

毫无疑问，如今的VR系统在视觉上还有很大进步空间，而为了解决这些问题，Meta Reality Labs提出一个“视觉图灵测试”（Visual Turing Test）的概念。

“图灵测试”由艾伦·麦席森·图灵提出，是通过对话的方式进行机器思考能力的一项测试，通过图灵测试代表测试者不能在机器和真人的回答中分别出来，从而“欺骗”测试者。

![Meta_Reality_Labs_Eye_Chart_Comparison](https://images.gitee.com/uploads/images/2022/0623/134620_365a9699_5631341.png "屏幕截图.png")

而“视觉图灵测试”同理，即看测试者是能够从VR显示的环境和真实世界进行区分。Michael Abrush表示：截至目前，还没有任何VR技术能够通过“视觉图灵测试”。也就是说，让VR在视觉上能够“欺骗”人类视觉，让你完全相信“通过VR看到的视觉是真实可信的”。

而Meta显示系统研究团队（简称：DSR），进一步对视觉图灵测试提出四大硬性要求，必须满足四个条件才能通过测试，分别是：

1. 自然变焦显示系统，类似人眼般的聚焦，支持视力矫正；
2. 达到视网膜级别的分辨率，PPD都足够高；
3. 矫正透镜带来的畸变、色散、鬼影等显示问题；
4. 支持HDR能力，亮度更高，色域更广、对比度更高等。

满足上述条件并可适应人眼视觉的VR显示开启全新的可能，1，增加社交临场感，可以充分感觉到和其他人同处一个虚拟空间；2，带来全新的视觉体验，提供全新的视觉表达方式。

扎克伯格谈到：这将是一个综合的技术进步，除了显示技术自身外，对VR头显的软件、芯片、传感器等硬件来说需要做到无缝协同。

### 变焦显示原型Half Dome

在2018、2019年Meta就多次展示过Half Dome原型机，其中重点提到了自然的变焦显示能力。因为现阶段的VR屏幕显示的都是2D平面图，而一旦和人眼的视觉冲突就会导致潜在的模糊、眼睛不适、甚至眩晕、恶心等情况，也就是所谓的视觉辐辏冲突（VAC）。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0623/134717_6a003290_5631341.png "Half_Dome_Prototypen_Chronik-860x310.jpg_副本")

根据最新消息，初代变焦显示原型创建于2015年，但体积非常大；后在2017年开发Half Dome 0代，外观上类似于Rift。

直到2019年的Half Dome 3有了重大突破，通过多层薄薄的液晶透镜模组，实现64个焦平面的平滑切换，同时体积可以做到更小。

虽然在此之后，并没有Half Dome的进展，但该系列原型机证实了变焦显示可以带来巨大的益处。

### Butterscotch原型机

前面提到视网膜级别的VR显示，那么在Meta眼中到底要达到什么水准呢？

Meta提到，想要达到视觉上的“以假乱真”，VR头显需要达到单眼8K分辨率，以及60PPD的水平。而Quest的单眼在2K分辨率，PPD也仅20左右。

Michael Abrush还谈到：相比当前的笔记本电脑、电视机而言，VR头显对亮度、色域、对比度要求明显更低。因此，从细节和真实感角度看，VR无法真正达到我们对2D显示屏所期待的水平。

![Butterscotch_Prototype_Reality_Labs_Research_副本](https://images.gitee.com/uploads/images/2022/0623/134748_45f723bd_5631341.png "屏幕截图.png")

Butterscotch是目前Meta最新的关于视网膜显示的原型机，也是对外公布的第一款视网膜VR原型机。参数规格方面，Butterscotch的PPD达到了55，虽然较Quest 2有大幅提升，但是同样有所取舍。

比如，为了实现这么高的PPD，Meta采用全新的混合透镜，FOV仅仅是多数VR头显的一半，也就是50度左右的水平。

通过上图也能看出，Butterscotch体积庞大，当前还不足以应用到商用产品中来。不过Michael Abrush表示：它确实体现了高分辨率对VR的积极作用，比如用它可以看清楚树叶上的细节。

### 基于模拟器的预失真研究

光学失真包括：图像畸变、色散、鬼影等等，通常是因为VR的光学透镜导致，对视觉的影响相当大。在现有的VR如Quest 2中，是通过软件算法进行补偿和矫正，也就是说原始图像是经过矫正的图像，通过透镜来模拟正常图像显示效果。

Michael Abrush表示：虽然通过软件算法可以得到一个不错的近似值，但是实际应用的结果并不算理想。因为，人眼是朝着不同方向在看，而图像失真也会因此而变化。完美的失真矫正应该是动态的，根据眼球运动自适应的。

![meta-reality-labs-research-vr-headset-prototype-7](https://images.gitee.com/uploads/images/2022/0623/134946_a7985566_5631341.png "屏幕截图.png")

目前Meta在失真矫正上研发了一个新的模拟器，通过模拟器可以在几分钟内模拟和评估失真情况。而在此之前，往往需要几个月时间来评估，效率大大提升。

![Verzerrungssimulator_Meta_DSR.jpg_副本](https://images.gitee.com/uploads/images/2022/0623/135002_e63de7a9_5631341.png "屏幕截图.png")

Michael Abrush谈到：这个失真模拟器的原理是，通过3D电视显示虚拟图像+眼球追踪技术来重现/模拟不同光学方案的VR中看到的失真效果。它的好处是不需要开发真正的原型机，即可针对不同光学方案的设计进行失真矫正算法设计。

### Starburst：HDR原型机

在2D屏幕下，我认为HDR提升比2K到4K的视觉提升体验更大。而在VR中同样如此，和真实中看到的类似，颜色逼真，明亮环境亮度足够，而黑暗环境则尽可能以黑色显示。

以亮度为例，传统2D显示屏对HDR的要求在1000nit，作为对比Quest 2亮度也就100nit左右。

![meta-reality-labs-research-vr-headset-prototype-5](https://images.gitee.com/uploads/images/2022/0623/135028_dfb40525_5631341.png "屏幕截图.png")

为了实现HDR显示能力，Meta DSR团队开发了一种基于液晶屏+高强背光的显示模组。目前已经应用在Starburst原型中，它的亮度可以达到20000nit，这种亮度足以模拟各种环境。

扎克伯格表示：第一代HDR原型机的缺点也很明显，有线设计，体积庞大、笨重，而且还是手持式的设计，更多用于测试和研究，远达不到商业化的水平。

### Holocake 2原型机

与前面的原型相比，Holocake 2体积更小巧，外观更时尚，也更像是准量产机，外观上和Project Cmabria也有一些相似之处。不同的是，Holocake 2是一款PC VR头显。

Holocake 2的特别之处在于，将全息透镜和Pancake透镜结合，分辨率、FOV、Eyebox和Quest 2基本一致，但是却实现了轻薄的外观，和更小巧的体积。当然，因为是PC VR头显Holocake 2没有内置计算模组和散热模块，否则体积同样也会更大。

![Meta_Reality_Labs_Lens_Comparison](https://images.gitee.com/uploads/images/2022/0623/135141_1b405306_5631341.png "屏幕截图.png")

光学方面，Holocake 2的光学模组可以看作是Pancake的升级。从上图来看，传统透镜、Pancake透镜、以及Holocake透镜在透镜模组的厚度上不断减小，为实现更小的机身提供帮助。

Holocake 2在屏幕上也做了优化，为了保证高亮度和极佳的色彩显示，它采用了激光显示方案，取代LED屏幕。当然，激光模组的一个特点是尚未达到量产阶段，无法大规模展开部署。

![Mark_Zuckerberg_Holocake_2.jpg_副本](https://images.gitee.com/uploads/images/2022/0623/135201_2a835c18_5631341.png "屏幕截图.png")

Michael Abrush表示：在Holocake 2上面还有很多工作要做，比如要确保激光模组足够安全、成本可控，显示效率足够高，并且可以与轻薄的VR头显集成在一起。如果激光显示技术Ready，那么打造一台太阳镜版的VR就不难了。

### Mirror Lake原型机

上面提到的几款原型机，在显示能力方面都有不同的研发倾向，各有特色。而最终，Meta的目标是将这些技术应用到一台VR头显中，并且保证具备轻薄的外形和较低的功耗。

![Meta-Mirror-Lake-concept_副本](https://images.gitee.com/uploads/images/2022/0623/135220_b5fcdca1_5631341.png "屏幕截图.png")

![Meta_Mirror_Lake_concept2](https://images.gitee.com/uploads/images/2022/0623/135230_f922e121_5631341.png "屏幕截图.png")

![Meta-Mirror-Lake-concept-top_副本](https://images.gitee.com/uploads/images/2022/0623/135241_076a6104_5631341.png "屏幕截图.png")

Mirror Lake原型机的目的正是如此，也正因为这是一个多元技术的综合体，本次并没有实际展示，仍然处于设计和论证阶段。

根据说明，Mirror Lake还提供了直通显示能力，即机身外部会显示佩戴者的眼神和面部细节，避免与外部人士交流产生割裂感。类似的技术Meta此前进行过公布，甚至有消息苹果也有类似功能的VR原型机。

看过之后，愈发感觉Meta在厚积薄发，毕竟大量人力、资金投进去，结果也是显而易见的​。这些结果中，主要还是以显示进化为主，同时不断的改进产品体积、功耗等。同时可见，尽管其他科技巨头在AR上加大力度，但VR仍然是Meta持续发力的方向，未来的体验​会越来越好。

 **更多精彩内容，关注青亭网微信号(ID:qingtinwang)，或者来微博@青亭网与我们互动！转载请注明版权和原文链接！** 	

<img width="19%" src="https://images.gitee.com/uploads/images/2022/0623/132819_5c00a23e_5631341.png">

微信扫码关注青亭网

<img width="19%" src="https://images.gitee.com/uploads/images/2022/0623/132826_60c23f06_5631341.png">

青亭 | 前沿科技交流群01

> 责任编辑：hi188

TAGs:  #[Butterscotch](http://www.7tin.cn/news/tag/butterscotch/)  #[Holocake 2](http://www.7tin.cn/news/tag/holocake-2/)  #[Meta](http://www.7tin.cn/news/tag/meta/)  #[Michael Abrush](http://www.7tin.cn/news/tag/michael-abrush/)  #[Mirror Lake](http://www.7tin.cn/news/tag/mirror-lake/)  #[Starburst](http://www.7tin.cn/news/tag/starburst/)  #[扎克伯格](http://www.7tin.cn/news/tag/zerkberg/)  #[视觉图灵测试](http://www.7tin.cn/news/tag/%e8%a7%86%e8%a7%89%e5%9b%be%e7%81%b5%e6%b5%8b%e8%af%95/)