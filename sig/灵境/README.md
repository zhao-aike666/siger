# [没想到，这些名字都出自钱学森！](https://mp.weixin.qq.com/s?__biz=MzI3NDI5MjI4OQ==&mid=2247666747&idx=1&sn=6ac5811556893f566e475842cc898ff5)

科技日报 2021-12-10 22:07 发表于北京
◎ 杜名馨 科技日报记者 付毅飞

钱学森是中国航天事业奠基人、许多人想不到的是，他还是位“起名大师”。

2021年12月11日是钱学森诞辰110周年，在缅怀钱老的同时，咱们一起来看看，他起过哪些名字。

### 灵境

“元宇宙”是目前最火的科技概念之一。不过你知道吗，早在30多年前，钱学森就对虚拟现实与“元宇宙”有过展望，并为其起了个颇有意境的名字——“灵境”。

信息领域专家汪成为院士长期从事电子计算机及人工智能研究工作。1990年，时任国防科工委科技委常委的汪成为收到钱学森的信，信中说，“ Virtual Reality（注：即如今所说“虚拟现实技术”）” 中译可以是“人为景境”或是“灵境”。钱学森还特别强调：“ **我特别喜欢‘灵境’，中国味特浓** ”。

<p><img width="39.9%" src="https://images.gitee.com/uploads/images/2022/0529/130001_394e9317_5631341.png"> <img width="39.9%" src="https://images.gitee.com/uploads/images/2022/0529/130025_3dd5cb2d_5631341.png"></p>

> 中国航天科技集团一院供图

20世纪90年代初，钱学森开始了解到虚拟现实技术，立刻想到将其应用于人机结合和人脑开发领域。

如今虚拟现实技术已应用于诸多领域，但仍未臻成熟，对于“元宇宙”，除了热炒概念，各界尚未形成清晰、确切的定义。

然而，钱学森在当时就已透过虚拟现实技术的产生和发展，预见到人机深度结合将对人类社会带来的深层变革。他在1993年7月3日写给汪成为的信中表示：“ **我对灵境技术及多媒体的兴趣在于，它能大大拓展人脑的知觉，使人进入前所未有的新天地。新的历史时代要开始了！** ”

在近日举行的2021元宇宙产业论坛上，中国移动通信联合会执行会长、元宇宙产业委倪健中表示：“ **钱学森多年前对虚拟现实技术定名‘灵境’‘大成智慧’的科技范畴，就是代表今天大家所说的元宇宙。** ”

1997年，汪成为的著作《灵境（虚拟现实）技术的理论、实现及应用》出版，对虚拟现实技术的发展史、理论、系统构成原理与设计方法，以及典型应用等作了全面介绍。当然，“灵境”这个名字，也被用在书中。

[more ...](https://gitee.com/yuandj/siger/issues/I4VF35#note_10583486_link)

---

# 【SIGer 灵境】频道 首刊 发布

- [第74届威尼斯电影节](http://ent.sina.com.cn/zt_d/venice74) : [新浪娱乐](http://ent.sina.com.cn/)
- [一半是惊喜一半是工伤 VR电影质检报告](一半是惊喜一半是工伤威尼斯VR电影质检报告.md)
- [四部中国VR入围 “VR元年”后前景何在](中国VR弄潮儿登陆威尼斯“VR元年”后前景何在.md)
- [钱学森图书馆：试水VR展览「窗」越书海](【展览】「窗」越书海，钱学森图书馆联手Jaunt中国首度试水VR展览.md)
- [VR 就是钱老称之为“灵境”的技术, 今儿开幕](「窗」越书海：VR就是那个钱老称之为“灵境”的技术，今天有个展览在钱馆开幕了.md)

<p><img width="706px" src="https://images.gitee.com/uploads/images/2022/0616/032935_8aa3e15c_5631341.jpeg" title="veniceDA副本.jpg"></p>

> 信息技术还会改变人接受客观世界的途径，大大扩展了，出现所谓 Virtual reality，我称之为 “灵境”。“巡天远看一千河！”

> 近日读了一些报道，看来用virtrual reality来管理生产、组织飞机起飞降落于繁忙的机场，甚至于艺术音乐现场演出都大有可为。这“灵境技术”可能是比较现实的人·机综合智能系统，21世纪会有大发展。你们（戴汝为）专题领导小组似应讨论。（1991年的7月）

还是去年元宇宙概念席卷全球时，一篇热搜将 “灵境” 推入了大众视野，当时我也只是一撇，必定早几年神州大地已出现过 VR 元年，此般热潮总会退去的。直到 星际航行 的 阿婆主同我讲，公号名是致敬中国航天之父，在30年后才追粉 “钱老” 的我，重温了他的生平和故事，这才感叹，我们学的太少了。

> 灵境技术 —— 引发、推动 —— 科学革命、文化革命 出现在钱老对 “灵境技术” 的评判  _**它将引发一系列震撼全世界的变革，一定是人类历史中的大事！**_ 当时我还在泡大学的图书馆实验室，抱着 CP/M 操作系统的英文手册操作点阵打印机画小画。如同阅读《星际航行》带给我们的科技视野，让我们敬仰之情油然而生。他的传记记述了 风靡全球的科幻作家对他的致敬，前文中的《神经漫游者》的插图，才是 SIGer 今年初才定的选题啊。:pray:

选用这张电影海报，寓意星辰大海触手可及，也是一份致敬。新浪记者采访的 威尼斯电影节 VR 专题，透出一个细节 星球大战的制作催生了 PS 技术的发展，VR/AR 促进了专用创作工具的诞生。这就是钱老论述的 “灵境技术” 促进科技文化发展，反馈技术进一步发展，赋能人类的创造力的过程。这是人类文明的脚步，不可阻挡，星辰大海和灵境，在这一刻高度统一了。前不久有朋友刷屏，人类的选择是 VR/AR 还是 火星，答案在此不言自明啦。再次致敬 “钱老” :pray: 

> 本次专题，腹稿时间想对长，也是反复推敲的过程，最后选择了，钱馆的 VR 展，以及 新浪娱乐制作的 第74届 威尼斯电影节 VR 专题，共4篇文章，从侧面记录了 前面提到的 VR 元年，至少它记录的是我们自己的脚步。

  - 下面的笔记摘要《[灵境&遥望比邻](https://gitee.com/yuandj/siger/issues/I5CHY4)》，记录了灵境专题的学习过程，也标志了两个频道同时推出的时间点，皆缘自致敬，其中 遥望比邻的笔记，已经由《[钱学森](https://gitee.com/yuandj/siger/issues/I4VF35)》和《星际航行》两个专题涵盖，不再分别叙述了。

【[笔记](https://gitee.com/yuandj/siger/issues/I5CHY4)】灵境 & 遥望比邻

- 39/217 篇 [钱学森图书馆](https://images.gitee.com/uploads/images/2022/0616/002647_da8ace73_5631341.png) 公众号的文章目录，与 “灵境&遥望比邻” 相关

  <p><img width="39.9%" src="https://images.gitee.com/uploads/images/2022/0615/234007_e1baa5e7_5631341.png"> <img width="39.9%" src="https://images.gitee.com/uploads/images/2022/0615/234023_203f715a_5631341.png"> <img width="39.9%" src="https://images.gitee.com/uploads/images/2022/0615/234041_e3ca8e3c_5631341.png"> <img width="39.9%" src="https://images.gitee.com/uploads/images/2022/0615/234114_d1e1f0ff_5631341.png"></p>

  - 1. [冲上热搜！钱学森30年前给虚拟现实取名“灵境”](https://gitee.com/yuandj/siger/issues/I5CHY4#note_10961446_link)
  - 15. [展览 | 「窗」越书海，钱学森图书馆联手Jaunt中国首度试水VR展览](https://gitee.com/yuandj/siger/issues/I5CHY4#note_10961752_link)
  - 16. [「窗」越书海 | VR就是那个钱老称之为“灵境”的技术，今天有个展览在钱馆开幕了](https://gitee.com/yuandj/siger/issues/I5CHY4#note_10961806_link)
  - 17. [Four Chinese VR movies nominated at Venice Film Festival](http://www.chinadaily.com.cn/culture/2017-09/01/content_31409044.htm) 【[笔记](https://gitee.com/yuandj/siger/issues/I5CHY4#note_10961843_link)】
  
- [四部中国VR电影入围威尼斯电影节](http://www.chinadaily.com.cn/culture/2017-09/01/content_31409044.htm) 【[笔记](https://gitee.com/yuandj/siger/issues/I5CHY4#note_10961871_link)】

  - [中国VR纪录片入围威尼斯国际电影节](http://art.ifeng.com/2020/0803/3505299.shtml) 【[笔记](https://gitee.com/yuandj/siger/issues/I5CHY4#note_10961902_link)】
  - [【视频】威尼斯电影节上，中国VR团队作品《自游》真给力](https://www.jiemian.com/article/1508793.html) 【[笔记](https://gitee.com/yuandj/siger/issues/I5CHY4#note_10961970_link)】

- [四部华语VR电影入围威尼斯，我们听主创聊了聊VR电影的“钱景”](https://www.tmtpost.com/2755622.html) 【[笔记](https://gitee.com/yuandj/siger/issues/I5CHY4#note_10962007_link)】

  - [VR电影走进主流 四部中国VR作品竞赛威尼斯电影节](https://vr.pconline.com.cn/991/9917424.html) 【[笔记](https://gitee.com/yuandj/siger/issues/I5CHY4#note_10962034_link)】

- [中国VR弄潮儿登陆威尼斯 “VR元年”后前景何在](https://ent.sina.com.cn/m/c/2017-09-10/doc-ifykuffc4801890.shtml) 【[笔记](https://gitee.com/yuandj/siger/issues/I5CHY4#note_10962081_link)】

  - [一半是惊喜一半是工伤 威尼斯VR电影质检报告](http://ent.sina.com.cn/m/c/2017-09-07/doc-ifykuffc4074117.shtml) 【[笔记](https://gitee.com/yuandj/siger/issues/I5CHY4#note_10962109_link)】
  - [封面素材](https://gitee.com/yuandj/siger/issues/I5CHY4#note_10962113_link) | [正文草稿](https://gitee.com/yuandj/siger/issues/I5CHY4#note_10962155_link) | [“灵境” 钱老手书文字版](https://gitee.com/yuandj/siger/issues/I5CHY4#note_11133745_link)

---

### 频道 优秀专题

<table width="100%">
<tr>
<td width="25%"><img width="100%" src="https://images.gitee.com/uploads/images/2022/0616/032935_8aa3e15c_5631341.jpeg"></td><td width="25%">
<p><i>人类文明的脚步，不可阻挡，星辰大海和灵境，高度统一，致敬 “钱老” :pray: </i></p>
<p align="center"><b>《SIGer 灵境》频道 首刊</b></p>
▪ <a href="一半是惊喜一半是工伤威尼斯VR电影质检报告.md" target="_blank">第74届威尼斯电影节</a><br>
▪ <a href="中国VR弄潮儿登陆威尼斯“VR元年”后前景何在.md" target="_blank">四部中国VR入围</a><br>
▪ <a href="【展览】「窗」越书海，钱学森图书馆联手Jaunt中国首度试水VR展览.md" target="_blank">「窗」越书海</a><br>
▪ <a href="「窗」越书海：VR就是那个钱老称之为“灵境”的技术，今天有个展览在钱馆开幕了.md" target="_blank">钱老所称的“灵境”技术</a><br>
</td>
<td width="25%"><img width="100%" src="https://images.gitee.com/uploads/images/2022/0623/011524_3e4f9ed5_5631341.jpeg"></td><td width="25%">
<p><i><a href="https://gitee.com/yuandj/siger/issues/I5DFVX#note_11132352_link" target="_blank">花絮：</a>1.倍感亲切的 LOGO（<a href="https://gitee.com/yuandj/siger/issues/I4UFQY#note_10905350_link" target="_blank">FlameOI</a>封面是BYD六角大楼）2. 元宇宙棋盘就是我心中酝酿已久的5米巨棋（火焰棋<a href="https://gitee.com/yuandj/siger/issues/I508GE#note_10993728_link" target="_blank">塔</a>）触手可及。</i></p>
<p align="center"><a href="7tin.cn" target="_blank" title="7tin.cn"><b>青亭网</b> 前沿科技新媒体</a></p><p>
▪ <a href="Meta公布四款VR原型机，提出“视觉图灵测试”概念.md" target="_blank">Meta公布四款VR原型机，提出“视觉图灵测试”概念</a><br>
▪ <a href="Meta长篇幅阐释：元宇宙必将到来，互联互通是必然.md" target="_blank">元宇宙必将到来，互联互通</a></p>
</td>
</tr>
</table>