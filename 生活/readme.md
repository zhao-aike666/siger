# 2022 [摄影频道](../sig/PICTURE/) [向往的假期](../sig/PICTURE/README.md#2022-摄影频道-向往的假期-专题) 专题

---

[<img width="369px" src="https://images.gitee.com/uploads/images/2022/0601/000118_23c030ce_5631341.jpeg" title="知音妈妈创刊号">](知音妈妈-大妞爱你.md)

（ [知音妈妈创刊号](知音妈妈-大妞爱你.md)。用心倾听孩子们的声音！）

# [我与SIGer](https://gitee.com/huang-zixin001/siger/issues/I546N4)

@[黄子昕](https://gitee.com/huang-zixin001) 2022-04-22 19:10

> 看近期SIGer家庭多了些新成员，我分享下自己的SIGer活动经历，我的参与度不高，只是SIGer里面的一个小小的面，希望能给大家启发。

### 特别有意思的志愿活动--SIGer兴趣小组

在我的学习之余，我参加了一些志愿活动，其中SIGer兴趣小组是我最喜欢的活动 ，这个小组自从我第一次加入到现在大概已经有1个多月了，活动不是很多，就是自由的分享自己平时的一些想法，什么的。时间上自由安排，而且形式也是多样的。我也看了其他小伙伴们的分享，有的擅长摄影，分享他的[摄影大作](建国路上的春秋朝暮.md)，甚至如何挑选摄影设备，有的擅长绘画，于是就分享了自己的绘画作品，还给自己的主题设计了logo，有的甚至把自己的[手工制作的小玩意](https://gitee.com/liu-aru/siger/issues/I53K7Z)，对制作过程进行了分享。分享的形式真的是多种多样！

我觉得这里面最有意思的是，每次分享主题，老师大都给做个很抢眼的海报，我就是想每次看到老师精心制作的海报，才忍不住一直写点东西。当然海报只是一个方面，更多的是，负责老师对我们每个人各个方面的引领，其中最重要的，我觉得是对探索，求知欲的鼓励。

这个活动，从刚开始的布置任务，都是有点意思。有个链接，还有个到现在我也没太明白的词“开源”，点进去，还有着代码等等，看着就像是个专门给编程人员用的网站，怎么看都不像我这个中学生能明白的。我也是好奇，就按照步骤操作，终于做了自己的第一次分享——我的兴趣爱好。我完成后，把链接发给了老师。老师很快给我回复，说他无法查看，让我检查分享的内容是不是只是发在了自己的仓内。哦，什么是“仓”？我一点都不明白，但我还是沉下心来去看，正好老师也在线，顺便一步一步的问了。我觉得自己是费了很大劲才算搞定，我轻松的出了口气。再去看自己的发文处，老师给回复了下：终于看到小黄的分享了，很不错，加油！嗯，看到这句话，我已经觉得是很受鼓舞。没想到，后面看到的更是精彩，老师同时把其他小伙伴类似的兴趣爱好的分享也发给我，科幻系类的，图文并茂，还有很吸引人的海报，被称为[“创生之柱”的鹰状星云](../第8期%20星辰大海II%20-%20宇宙.md)，配着SIGer开源期刊，看着真的是吸引人。当然这些是我之前的那些大哥哥大姐姐们的合创结果。

后来我在科幻主题下写下了自己的第一篇文章，我和弟弟的魔幻之旅。我记得我刚分享不久，虽然那篇文章只是开了个头，但是很快就收到老师发过的科幻大片，各种大片似的图片，还有相关的图书推荐，还有科幻作家等，更重要的是，老师还贴心的专门给我的这篇文章做了个惹眼海报，封面充满了幻想，就像欧美的科幻大片那样。就这样，我的名字也第一次上了SIGer的期刊封面，还是主编。老师顺便布置了任务，我可以继续这一主题的写作。这个太鼓舞人了，我还从没觉得如此被重视过呢。我本来就喜欢写作，这个确实又再次的激起我写作的欲望。接下来的一段时间，我也是有空就发一些自己的写作，把这里就当做自己的写作展示平台。因为自己的参与度比较高吧，还不时的收到老师的鼓励，还有几篇文章，也有了个专门的海报封面，成了一个独立的主题。同时老师还给我做了个自己的专辑，感觉简直太有意思了。

这是个线上的志愿活动，主旨是为了引导年轻人，拥抱互联网，拥抱科技。我觉得通过这个活动，我是再次喜欢上了写作，同时也能室我更加深入的思考。因为我也想在学习之余，把自己想到的，而且老师觉得也还不错的主题做好，这样我就算偶尔用电脑，用手机，我觉得也大都是搜素资料，与之前老想着玩的那种状态相比，自己都觉得改变了很多。

希望自己能坚持做，把[自己的小集子](https://gitee.com/huang-zixin001/siger/issues/I50O3Z)做好，把[和弟弟的魔幻之旅](https://gitee.com/huang-zixin001/siger/issues/I4Y343)做好。尽力坚持。感谢机缘巧合，加入这个活动，感谢老师一直以来的鼓励关怀，也希望我的感受能给小伙伴们带来激励。

> 附一篇国际社工日任务时写下的，刚加入SIGer时  
  《[拥抱网络，拥抱科技，改善学习 ——不让任何人掉队的SIGer兴趣小组](世界社工日.md#拥抱网络拥抱科技改善学习)》