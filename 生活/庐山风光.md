<p><img width="706px" src="https://images.gitee.com/uploads/images/2022/0805/003641_f1e7975c_5631341.jpeg" title="208庐山瀑布副本.jpg"></p>

# [庐山风光](https://gitee.com/LHZ_CUC/siger/issues/I5K3CX)

李浩泽 2022-08-02 10:59

前段时间去庐山旅游了一趟，给读者们分享几张我认为拍得不错的风光照片。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0802/105125_6e68ab37_10096166.jpeg "在这里输入图片标题")

这里是锦绣谷一块突出的巨石，伸出山体，颇有险峻之势。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0802/105311_38ee6053_10096166.jpeg "在这里输入图片标题")

锦绣谷山体上一块刻字巨石。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0802/105357_a885faf1_10096166.jpeg "在这里输入图片标题")

在锦绣谷的一个角落能够看到下方庐山市和鄱阳湖的一角。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0802/105445_dd03ea9b_10096166.jpeg "在这里输入图片标题")

吃完晚饭，在街心公园散步，俯瞰别墅区。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0802/105548_f4affdd8_10096166.jpeg "在这里输入图片标题")

第二天去了三叠泉，即著名的“飞流直下三千尺，疑是银河落九天”。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0802/105648_ff97a549_10096166.jpeg "在这里输入图片标题")

大天池近景特写。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0802/105712_2bd1463a_10096166.jpeg "在这里输入图片标题")

俯瞰九江市全景。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0802/105735_f013357e_10096166.jpeg "在这里输入图片标题")

这里是著名的龙首崖。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0802/105753_d6170a1c_10096166.jpeg "在这里输入图片标题")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0802/105802_aee67b2e_10096166.jpeg "在这里输入图片标题")

往下看就是九江市。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0802/105834_e24f65f6_10096166.jpeg "在这里输入图片标题")

山谷中的一座桥。

夏日季节去庐山旅游是非常不错的选择，堪称避暑胜地，希望读者们今后有机会都能够去庐山一探美景！

@袁德俊 邀请袁老师看一组我拍摄的庐山风光照片，使用的是索尼A7M4设备，我决定再写一篇使用体验。